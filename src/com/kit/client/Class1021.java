package com.kit.client;

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;
import java.net.URL;

public abstract class Class1021 extends Applet implements Runnable, FocusListener, WindowListener {
    static String aString_0223 = "com.mysql.jdbc.Driver";
    private boolean aBoolean1 = false;
    static int anInt3 = 0;

    static boolean aBoolean6 = false;
    static Class1008 aClass94_8 = Class943.create("");
    static Class1008 aClass94_9 = Class943.create(")3)3)3");
    static boolean initOpenGL = false;
    public static int anInt12;
    public static boolean aBoolean13;
    static Class1008 aClass94_4 = Class943.create(" from your ignore list first)3");
    ;

    public final void focusLost(FocusEvent var1) {
        Class163_Sub2_Sub1.focusIn = false;
    }

    abstract void mainLoop();

    public final void windowClosing(WindowEvent var1) {
        this.destroy();
    }

    public final void windowIconified(WindowEvent var1) {
    }

    public static void method26() {
        aClass94_8 = null;
        // aClass94_10 = null;
        aClass94_9 = null;
        aClass94_4 = null;
    }

    public final void windowDeactivated(WindowEvent var1) {
    }

    static final Class1008 method27(Class1008 var0) {
        int var2 = Class10117.method1602(var0);
        return var2 != -1 ? Class119.aClass131_1624.aClass94Array1721[var2].method1560(Class3_Sub13_Sub16.aClass94_3192, true, Class932.aClass94_4066) : Class1225.aClass94_4049;
    }

    public final AppletContext getAppletContext() {
        return null != Class3_Sub13_Sub7.resizableFrame ? null : (Class38.gameClass942 != null && this != Class38.gameClass942.thisApplet ? Class38.gameClass942.thisApplet.getAppletContext() : super.getAppletContext());
    }

    public final void focusGained(FocusEvent var1) {
        Class163_Sub2_Sub1.focusIn = true;
        Class3_Sub13_Sub10.fullRedraw = true;
    }

    static final void method28() {
        Class143.aClass93_1874.clearAll();
    }

    public final void windowClosed(WindowEvent var1) {
    }

    protected final boolean method29() {
       /*String var2 = this.getDocumentBase().getHost().toLowerCase();
	   if(!var2.equals("jagex.com") && !var2.endsWith(".jagex.com")) {
		   if(!var2.equals("runescape.com") && !var2.endsWith(".runescape.com")) {
			   if(var2.endsWith("127.0.0.1")) {
				   return true;
			   } else {
				   while(-1 > ~var2.length() && 48 <= var2.charAt(-1 + var2.length()) && 57 >= var2.charAt(-1 + var2.length())) {
					   var2 = var2.substring(0, -1 + var2.length());
				   }

				   if(!var2.endsWith("192.168.1.")) {
					   this.method31("invalidhost", var1 + -27544);
					   return false;
				   } else {
					   return true;
				   }
			   }
		   } else {
			   return true;
		   }
	   } else {
		   return true;
	   }*/
        return true;
    }

    public final synchronized void addCanvas() {
        if (Class1143.canvas != null) {
            Class1143.canvas.removeFocusListener(this);
            Class1143.canvas.getParent().remove(Class1143.canvas);
        }

        Container container;
        if (Class3_Sub13_Sub10.fullscreenFrame != null) {
            container = Class3_Sub13_Sub10.fullscreenFrame;
        } else if (null != Class3_Sub13_Sub7.resizableFrame) {
            container = Class3_Sub13_Sub7.resizableFrame;
        } else {
            container = Class38.gameClass942.thisApplet;
        }

        container.setLayout(null);
        Class1143.canvas = new Class970(this);
        container.add(Class1143.canvas);
        Class1143.canvas.setSize(Class23.canvasWid, Class1013.canvasHei);
        Class1143.canvas.setVisible(true);
        if (container != Class3_Sub13_Sub7.resizableFrame) {
            Class1143.canvas.setLocation(Class84.canvasDrawX, Class989.canvasDrawY);
        } else {
            Insets var3 = Class3_Sub13_Sub7.resizableFrame.getInsets();
            Class1143.canvas.setLocation(Class84.canvasDrawX + var3.left, var3.top + Class989.canvasDrawY);
        }

        Class1143.canvas.addFocusListener(this);
        Class1143.canvas.requestFocus();
        Class163_Sub2_Sub1.focusIn = true;
        Class3_Sub13_Sub10.fullRedraw = true;
        Class3_Sub13_Sub6.focus = true;
        Class955.canvasReplaceRecommended = false;
        Class954.lastCanvasReplace = Class1219.currentTimeMillis();
    }

    public final void destroy() {
        if (this == Class1030.currentScreen && !Class1221.shutdown) {
            System.out.println("Destroying applet");
            Class3_Sub9.killtime = Class1219.currentTimeMillis();
            Class3_Sub13_Sub34.sleep(5000L);
            Class3_Sub13_Sub10.errorClass942 = null;
            this.destroyApplet(false);
        }
    }

    public final void update(Graphics var1) {
        this.paint(var1);
    }

    final void error(String var1) {
        if (!this.aBoolean1) {
            this.aBoolean1 = true;
            System.out.println("error_game_" + var1);

            try {
                this.getAppletContext().showDocument(new URL(this.getCodeBase(), "error_game_" + var1 + ".ws"), "_top");
            } catch (Exception var4) {
                ;
            }

        }
    }

    abstract void method32(byte var1);

    abstract void method33(int var1);

    public final URL getDocumentBase() {
        return null != Class3_Sub13_Sub7.resizableFrame ? null : (Class38.gameClass942 != null && this != Class38.gameClass942.thisApplet ? Class38.gameClass942.thisApplet.getDocumentBase() : super.getDocumentBase());
    }

    public final synchronized void paint(Graphics var1) {
        if (this == Class1030.currentScreen && !Class1221.shutdown) {
            Class3_Sub13_Sub10.fullRedraw = true;
            if (Class137.safeRedraw && !Class1012.aBoolean_617 && (Class1219.currentTimeMillis() - Class954.lastCanvasReplace) > 1000L) {
                Rectangle var2 = var1.getClipBounds();
                if (var2 == null || ~var2.width <= ~Class3_Sub9.anInt2334 && ~Class70.anInt1047 >= ~var2.height) {
                    Class955.canvasReplaceRecommended = true;
                }
            }
        }
    }

    public final void windowDeiconified(WindowEvent var1) {
    }

    static final void method34() {
        if (null != Class1228.aClass155_2627) {
            Class1228.aClass155_2627.method2163(false);
        }

        if (Class3_Sub21.aClass155_2491 != null) {
            Class3_Sub21.aClass155_2491.method2163(false);
        }

        Class140_Sub3.method1959(256, 2, 22050, Class3_Sub13_Sub15.isStereo);
        Class1228.aClass155_2627 = Class58.method1195(22050, Class38.gameClass942, Class1143.canvas, 0, 14);
        Class1228.aClass155_2627.method2154(114, Class930.aClass3_Sub24_Sub4_1193);
        Class3_Sub21.aClass155_2491 = Class58.method1195(2048, Class38.gameClass942, Class1143.canvas, 1, 14);
        Class3_Sub21.aClass155_2491.method2154(-126, Class3_Sub26.aClass3_Sub24_Sub2_2563);
    }

    private final void destroyApplet(boolean var2) {
        synchronized (this) {
            if (Class1221.shutdown) {
                return;
            }

            Class1221.shutdown = true;
        }

        if (Class38.gameClass942.thisApplet != null) {
            Class38.gameClass942.thisApplet.destroy();
        }

        try {
            this.method32((byte) 23);
        } catch (Exception var8) {
            ;
        }

        if (Class1143.canvas != null) {
            try {
                Class1143.canvas.removeFocusListener(this);
                Class1143.canvas.getParent().remove(Class1143.canvas);
            } catch (Exception var7) {
                ;
            }
        }

        if (null != Class38.gameClass942) {
            try {
                Class38.gameClass942.method1445();
            } catch (Exception var6) {
                ;
            }
        }

        this.method33(126);

        if (null != Class3_Sub13_Sub7.resizableFrame) {
            try {
                System.exit(0);
            } catch (Throwable var5) {
                ;
            }
        }

        System.out.println("Shutdown complete - clean:" + var2);
    }

    public final void windowActivated(WindowEvent var1) {
    }

    private final void method36() {
        long var2 = Class1219.currentTimeMillis();
        long var4 = Class134.aLongArray1766[Class1027.anInt1953];
        Class134.aLongArray1766[Class1027.anInt1953] = var2;
        Class1027.anInt1953 = 31 & Class1027.anInt1953 - -1;
        synchronized (this) {
            Class3_Sub13_Sub6.focus = Class163_Sub2_Sub1.focusIn;
        }

        this.mainLoop();
        if (0L != var4 && var2 <= var4) {
            ;
        }
    }

    public static final void providesignlink(Class942 var0) {
        Class38.gameClass942 = var0;
        Class3_Sub13_Sub10.errorClass942 = var0;
    }

    private final void method37() {
        long var2 = Class1219.currentTimeMillis();
        long var4 = Class163_Sub1.aLongArray2986[Class62.anInt950];
        Class163_Sub1.aLongArray2986[Class62.anInt950] = var2;
        Class62.anInt950 = 31 & Class62.anInt950 + 1;
        if (~var4 != -1L && var2 > var4) {
            int var6 = (int) (var2 + -var4);
            Class954.anInt1862 = (32000 + (var6 >> 1)) / var6;
        }

        if (50 < Class3_Sub13_Sub25.anInt3313++) {
            Class3_Sub13_Sub10.fullRedraw = true;
            Class3_Sub13_Sub25.anInt3313 -= 50;
            Class1143.canvas.setSize(Class23.canvasWid, Class1013.canvasHei);
            Class1143.canvas.setVisible(true);
            if (Class3_Sub13_Sub7.resizableFrame != null && null == Class3_Sub13_Sub10.fullscreenFrame) {
                Insets var8 = Class3_Sub13_Sub7.resizableFrame.getInsets();
                Class1143.canvas.setLocation(var8.left + Class84.canvasDrawX, Class989.canvasDrawY + var8.top);
            } else {
                Class1143.canvas.setLocation(Class84.canvasDrawX, Class989.canvasDrawY);
            }
        }

        this.method_754(40);
    }

    abstract void method_754(int var1);

    public final URL getCodeBase() {
        return Class3_Sub13_Sub7.resizableFrame == null ? (null != Class38.gameClass942 && this != Class38.gameClass942.thisApplet ? Class38.gameClass942.thisApplet.getCodeBase() : super.getCodeBase()) : null;
    }

    public final void run() {
        try {
            if (null != Class942.javaVendor) {
                String var1 = Class942.javaVendor.toLowerCase();
                if (var1.indexOf("sun") == -1 && -1 == var1.indexOf("apple")) {
                    if (0 != ~var1.indexOf("ibm") && (Class942.javaVersion == null || Class942.javaVersion.equals("1.4.2"))) {
                        this.error("wrongjava");
                        return;
                    }
                } else {
                    String var2 = Class942.javaVersion;
                    if (var2.equals("1.1") || var2.startsWith("1.1.") || var2.equals("1.2") || var2.startsWith("1.2.")) {
                        this.error("wrongjava");
                        return;
                    }

                    Class132.anInt1737 = 5;
                }
            }

            int var7;
            if (null != Class942.javaVersion && Class942.javaVersion.startsWith("1.")) {
                var7 = 2;

                int var9;
                for (var9 = 0; var7 < Class942.javaVersion.length(); ++var7) {
                    char var3 = Class942.javaVersion.charAt(var7);
                    if (var3 < 48 || 57 < var3) {
                        break;
                    }

                    var9 = var9 * 10 - (48 - var3);
                }

                if (~var9 <= -6) {
                    Class137.safeRedraw = true;
                }
            }

            if (null != Class38.gameClass942.thisApplet) {
                Method var8 = Class942.cycleRootMethod;
                if (null != var8) {
                    try {
                        var8.invoke(Class38.gameClass942.thisApplet, new Object[]{Boolean.TRUE});
                    } catch (Throwable var4) {
                        ;
                    }
                }
            }

            Class3_Sub28_Sub18.calculateMaxMemory();
            this.addCanvas();
            Class164_Sub1.aClass158_3009 = Class3_Sub13_Sub23_Sub1.createGraphicsBuffer(Class1143.canvas, /*Class23.canvasWid, Class1013.canvasHei*/Class922.resizeWidth, Class922.resizeHeight);
            this.method39();
            Class1042_4.aClass129_2552 = Class1017.method1012();

            while (-1L == ~Class3_Sub9.killtime || Class3_Sub9.killtime > Class1219.currentTimeMillis()) {
                Class974.anInt1754 = Class1042_4.aClass129_2552.method1767(-1, Class132.anInt1737, Class1228.anInt2626);

                for (var7 = 0; var7 < Class974.anInt1754; ++var7) {
                    this.method36();
                }

                this.method37();
                Class81.processEventQueue(Class38.gameClass942, Class1143.canvas);
            }
        } catch (Exception var5) {
            Class49.method1125((String) null, var5);
            this.error("crash");
        }

        this.destroyApplet(true);
    }

    public final String getParameter(String var1) {
        return Class3_Sub13_Sub7.resizableFrame == null ? (Class38.gameClass942 != null && this != Class38.gameClass942.thisApplet ? Class38.gameClass942.thisApplet.getParameter(var1) : super.getParameter(var1)) : null;
    }

    abstract void method39();

    public final void stop() {
        if (Class1030.currentScreen == this && !Class1221.shutdown) {
            Class3_Sub9.killtime = 4000L + Class1219.currentTimeMillis();
        }
    }

    public abstract void init();

    final void createResizableFrame(int modeWhat, int gameMode, boolean var3, int width, String var5, int height, int var8) {
        try {
            Class1013.canvasHei = height;
            Class70.anInt1047 = height;
            Class84.canvasDrawX = 0;
            Class3_Sub13_Sub23_Sub1.anInt4033 = gameMode;
            Class23.canvasWid = width;
            Class3_Sub9.anInt2334 = width;
            Class989.canvasDrawY = 0;
            Class1030.currentScreen = this;
            Class3_Sub13_Sub7.resizableFrame = new Frame();
            Class3_Sub13_Sub7.resizableFrame.setTitle("Jagex");
            Class3_Sub13_Sub7.resizableFrame.setResizable(true);
            Class3_Sub13_Sub7.resizableFrame.addWindowListener(this);
            Class3_Sub13_Sub7.resizableFrame.setVisible(true);
            Class3_Sub13_Sub7.resizableFrame.toFront();
            Insets var9 = Class3_Sub13_Sub7.resizableFrame.getInsets();
            Class3_Sub13_Sub7.resizableFrame.setSize(var9.left + Class3_Sub9.anInt2334 + var9.right, var9.top + Class70.anInt1047 + var9.bottom);
            Class3_Sub13_Sub10.errorClass942 = Class38.gameClass942 = new Class942((Applet) null, modeWhat, var5, var8);
            Class1124 var10 = Class38.gameClass942.startThread(this, 1);

            while (0 == var10.status) {
                Class3_Sub13_Sub34.sleep(10L);
            }

            Class17.aThread409 = (Thread) var10.value;
        } catch (Exception var11) {
            Class49.method1125((String) null, var11);
        }
    }

    public final void windowOpened(WindowEvent var1) {
    }

    public final void start() {
        if (Class1030.currentScreen == this && !Class1221.shutdown) {
            Class3_Sub9.killtime = 0L;
        }
    }

    final void createFixedFrame(int width, int var3, int var4, int height) {
        try {
            if (Class1030.currentScreen != null) {
                ++Class1017.anInt639;
                if (~Class1017.anInt639 <= -4) {
                    this.error("alreadyloaded");
                    return;
                }

                this.getAppletContext().showDocument(this.getDocumentBase(), "_self");
                return;
            }

            Class1030.currentScreen = this;
            Class989.canvasDrawY = 0;
            Class3_Sub13_Sub23_Sub1.anInt4033 = var4;
            Class23.canvasWid = width;
            Class3_Sub9.anInt2334 = width;
            Class84.canvasDrawX = 0;
            Class1013.canvasHei = height;
            Class70.anInt1047 = height;
            String var6 = this.getParameter("openwinjs");
            if (var6 != null && var6.equals("1")) {
                Class1042_3.aBoolean3594 = true;
            } else {
                Class1042_3.aBoolean3594 = false;
            }

            if (null == Class38.gameClass942) {
                Class3_Sub13_Sub10.errorClass942 = Class38.gameClass942 = new Class942(this, var3, (String) null, Class922.aInteger_512);
            }

            Class1124 var7 = Class38.gameClass942.startThread(this, 1);

            while (~var7.status == -1) {
                Class3_Sub13_Sub34.sleep(10L);
            }

            Class17.aThread409 = (Thread) var7.value;
        } catch (Exception var8) {
            Class49.method1125((String) null, var8);
            this.error("crash");
        }
    }

}

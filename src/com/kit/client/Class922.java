package com.kit.client;

import java.awt.Color;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.GregorianCalendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.zip.CRC32;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;

public final class Class922 extends Class1021 {

    protected static final long serialVersionUID = 819707645271950974L;

    protected static boolean aBoolean_609 = true;
    protected static boolean aBoolean_610 = false;
    protected static boolean aBoolean_611 = false;

    static int aInteger_546;
    protected static int aInteger_514;
    protected static int aInteger_513;
    protected static int aInteger_512 = 26;
    protected static int anInt1465 = 0;
    protected static int anInt2195;
    protected static int mapbackId = -1;
    protected static int mapbackIdNew = -1;
    protected static int[] playerIndices = new int[2048];
    protected static volatile int mouseY = -1;
    protected static volatile int mouseX = -1;
    protected static int aInteger_544 = 0;
    protected static int mapbackFsId = -1;
    protected static int chatbuttonFsId = -1;
    protected static int tabstonesFsId = -1;
    protected static int mapscenesId = -1;
    protected static int infinitySymbolId = -1;
    protected static int orbBgId;
    protected static int hpFillId;
    protected static int hpIconId;
    protected static int prayFillId;
    protected static int prayIconId;
    protected static int runFillId;
    protected static int runIconId;
    protected static int orbDrainId;
    protected static int runFillOnId;
    protected static int runOnIconId;
    protected static int poisonFillId;
    protected static int orb_drain_hd_id;
    protected static int redStone2Id;
    protected static int redStoneId;
    protected static int chatbuttonselectedId;
    protected static int redStone1Id;
    protected static int redStone4Id;
    protected static int redStone3Id;
    protected static int redStone5Id;
    protected static int aboveCompassId;
    protected static int attackIconId;
    protected static int skillsIconId;
    protected static int questIconId;
    protected static int inventoryIconId;
    protected static int equipmentIconId;
    protected static int prayerIconId;
    protected static int magicIconId;
    protected static int clanChatIconId;
    protected static int friendsIconId;
    protected static int ignoreIconId;
    protected static int logoutIconId;
    protected static int settingsIconId;
    protected static int emotesIconId;
    protected static int musicIconId;
    protected static int bountyTeleSpriteId;
    protected static int worldmap0Id;
    protected static int worldmap1Id;
    protected static int worldmap2Id;
    protected static int orbBgIdNew;
    protected static int orbBgOnIdNew;
    protected static int hpFillIdNew;
    protected static int osrsChannelBackgroundID;
    protected static int prayIconIdNew;
    protected static int runFillIdNew;
    protected static int orbDrainIdNew;
    protected static int orbBgOSHoverId;
    protected static int xpButtonId = -1;
    protected static int restIconId = -1;

    protected final String aString2003 = Class929.aString1089;

    protected static Object anObject821 = new Object();
    protected static Class927 xpCounterHover;
    protected static Class927 mapbackSprte;
    protected final static Class1008 mapbackNew = Class943.create("525mapback");
    protected final static Class1008 CYAN_CLASS_1008 = Class943.create("cyan:");
    protected final static Class1008 mapback = Class943.create("mapback");
    protected static Class1017_2 class1017_2 = new Class1017_2(16);
    protected static Class975 aClass60_2164 = new Class975();
    protected static ByteBuffer aClass3_Sub12_1667;
    protected static Class927 compassBg;
    protected final static Class1008 BLANK_CLASS_1008 = Class943.create("");
    protected final static Class1008 spaceString = Class943.create(" ");
    protected final static Class1008 FLASH_CLASS_1008 = Class943.create("*");
    protected static Class946[] class946List = new Class946[2048];
    protected static Class1008 password = BLANK_CLASS_1008;
    protected static Class1008 username = BLANK_CLASS_1008;
    protected static Class1008 amountModifier = BLANK_CLASS_1008;
    protected final static Class1008 mapbackFs = Class943.create("mapbackfs");
    protected static Class927 mapbackFsSprite;
    protected final static Class1008 chatbuttonFs = Class943.create("osrschannelbuttons");
    protected static Class927 chatbuttonFsSprite;
    protected final static Class1008 tabstonesFs = Class943.create("tabstonesfs");
    protected static Class927 tabstonesFsSprite;
    protected final static Class1008 mapscene = Class943.create("mapscene");
    protected static Class957[] mapscenes;
    protected static Class957 infinitySymbol;
    protected final static Class1008 infinitySymbolString = Class943.create("infinity");
    protected static Class927 orbBg;
    protected static Class927 hpFill;
    protected static Class927 hpIcon;
    protected static Class927 prayFill;
    protected static Class927 prayIcon;
    protected static Class927 runFill;
    protected static Class927 runIcon;
    protected static Class927 orbDrain;
    protected static Class927 runFillOn;
    protected static Class927 runOnIcon;
    protected static Class927 poisonFill;
    protected final static Class1008 orbBgString = Class943.create("orbbg");
    protected final static Class1008 hpFillString = Class943.create("hpfill");
    protected final static Class1008 hpIconString = Class943.create("hp_icon");
    protected final static Class1008 prayFillString = Class943.create("prayfill");
    protected final static Class1008 prayIconString = Class943.create("pray_icon");
    protected final static Class1008 runFillString = Class943.create("run_fill");
    protected final static Class1008 runIconString = Class943.create("run_icon");
    protected final static Class1008 orbDrainString = Class943.create("orbdrain");
    protected final static Class1008 runFillOnString = Class943.create("runfillon");
    protected final static Class1008 runOnIconString = Class943.create("runonicon");
    protected final static Class1008 poisonFillString = Class943.create("poisonfill");
    protected static Class957[] orb_drain_hd;
    protected static Class957[] randomSprite;
    protected static Class927 chatbuttonselected;
    protected static Class927 redStone0;
    protected static Class927 redStone1;
    protected static Class927 redStone2;
    protected static Class927 redStone3;
    protected static Class927 redStone4;
    protected static Class927 redStone5;
    protected static Class927 aboveCompass;
    protected static Class927 attackIcon;
    protected static Class927 skillsIcon;
    protected static Class927 questIcon;
    protected static Class927 inventoryIcon;
    protected static Class927 equipmentIcon;
    protected static Class927 prayerIcon;
    protected static Class927 magicIcon;
    protected static Class927 clanChatIcon;
    protected static Class927 friendsIcon;
    protected static Class927 ignoreIcon;
    protected static Class927 logoutIcon;
    protected static Class927 settingsIcon;
    protected static Class927 emotesIcon;
    protected static Class927 musicIcon;
    protected static Class957[] bountyTeleSprite;
    protected static Class927 worldmap0;
    protected static Class927 worldmap1;
    protected static Class927 worldmap2;
    protected static Class927 hpFillNew;
    protected static Class927 osrsChannelBackground;
    protected static Class927 runFillNew;
    protected static Class927 prayIconNew;
    protected static Class927 orbDrainNew;
    static Class927 runFillOnNew;
    protected static Class927 orbBgOSHover;
    protected final static Class1008 orbBgStringNew = Class943.create("orbbgnew");
    protected final static Class1008 orbBgOnStringNew = Class943.create("orbbgglow");
    protected final static Class1008 orbBgOSHoverString = Class943.create("orbbghover");
    protected final static Class1008 hpFillStringNew = Class943.create("hpfillnew");
    protected final static Class1008 osrsChannelBackgroundString = Class943.create("1071");
    protected final static Class1008 prayIconStringNew = Class943.create("prayiconnew");
    protected final static Class1008 runFillStringNew = Class943.create("runfillnew");
    protected final static Class1008 orbDrainStringNew = Class943.create("orbdrainnew");
    protected static Class927 xpButton;
    protected final static Class1008 xpButtonString = Class943.create("1009");
    protected static Class927 restIcon;
    protected final static Class1008 restIconString = Class943.create("resticon");

    /**
     * start of osrs login screen
     **/
    protected static Class927 forgottenPasswordLogin;
    protected static Class927 rememberUsernameLogin;
    protected static Class927 hideUsernamelogin;

    protected static Class927 rememberToggleOff;
    protected static Class927 rememberToggleOffHover;
    protected static Class927 blockIcon;

    protected static int
            forgottenPasswordLoginId,
            rememberUsernameLoginId,
            hideUsernameLoginId,

    rememberToggleOffId,
            rememberToggleOffHoverId,
            blockIconId = -1;

    protected final static Class1008 forgottenPasswordLoginString = Class943.create("forgottenpassword");
    protected final static Class1008 rememberUsernameLoginString = Class943.create("rememberusername");
    protected final static Class1008 hideUsernameLoginString = Class943.create("hideusername");
    protected final static Class1008 rememberToggleOffString = Class943.create("cannonicon");
    protected final static Class1008 rememberToggleOffHoverString = Class943.create("deflecticon");
    protected final static Class1008 blockIconString = Class943.create("blockicon");

    protected static Class927 xp_counter;
    protected static Class927 xp_counter_2;
    protected static Class927 xpCounterActive;
    protected static Class957 compassSprite;
    protected static Class927 xpCounterActive_2;
    protected static int xp_counter_id;
    protected static int xp_counter_2_id;
    protected static int xpCounterActiveId;
    protected static int xpCounterActive2Id;
    protected static int xpCounterHoverId;
    protected static int compassBgId;
    protected static Class957[] musicToggle;
    protected static Class957[] hd_sprites;
    protected static Class957[] sd_sprites;
    protected static boolean playMusic = false;
    protected static int musicToggleId;
    protected static int hd_sprites_id;
    protected static int sd_sprites_id;
    protected static int[] mapbackDest;
    protected static int[] mapbackSource;
    protected static Class975 updateServerList = new Class975();
    protected static int[] compassDest;
    protected static int[] compassSource;
    protected static Class3_Sub11[][] aClass3_Sub11ArrayArray2199;
    protected static int[] mapsArray;
    protected static Class1019 smallClass1019;
    protected static Class1019 boldClass1019;
    protected static Class1019 regularClass1019;
    protected static final boolean snow = false;
    protected static boolean hintFlash = false;
    protected static Class1009 modelsCache602, skeletons602, skins602;
    protected static final int CLAN_MEMBERS_SIZE = 50;
    protected static final int CLAN_MEMBERS_START = 40108065;
    protected static boolean roofsOff = true;
    protected static boolean hdOnLogin = false; //fullscreen = false, removedFrame = false;
    protected static int clientSize = 0;
    protected static int log_view_dist = 9; // for LD fullscreen - not done.
    protected static int cameraZoom = 3; // default - 3. Higher = zoomed out
    protected static int displayMode = 2; // 1 fixed - 3 fs
    protected static int resizeWidth = 1600;
    protected static int resizeHeight = 800;
    protected static final int GAME_AREA = 35913790;
    protected static final int GAME_AREA_1337 = 35913791;// 40697856
    protected static final int GAME_AREA_INTERFACE_CONTAINER = 35913792;
    protected static final int INVY_CONTAINER = 35913810;
    protected static final int INVY_CONTAINER_SPRITE = 35913811;
    protected static final int MINIMAP_AREA = 35913897;
    protected static final int CHATBOX_CONTAINER = 35913801;
    protected static final int CHATBOX_CONTAINER_SPRITE = 35913802;
    protected final int BANK_BG_SPRITE = 786432;
    protected static final int UPPER_TABS = 35913755;
    protected static final int UPPER_TABS_SPRITE = 35913756;
    protected static final int LOWER_TABS = 35913738;
    protected static final int LOWER_TABS_SPRITE = 35913739;
    protected static final int CHAT_BUTTONS_CONTAINER = 35913888;
    protected static final int NEW_FRAME_CONTAINER = 35913836;
    protected static final int TOP_OLDBUTTONS = 35913841;
    protected static final int BOTTOM_OLDBUTTONS = 35913864;
    protected static final int TOP_NEWBUTTONS = 35913898; // 562
    protected static final int BOTTOM_NEWBUTTONS = 35913920; // 562
    protected static final int XP_BUTTON = 35913950;
    protected static final int PRAY_BUTTON = 35913951;
    protected static final int RUN_BUTTON = 35913953;
    protected static final int PRAY_BUTTON2 = 35913957;
    protected static final int RUN_BUTTON2 = 35913955;
    protected static final int XP_COUNTER = 35913830;
    protected static final int NEW_BOTTOM_CLICKAREA = 35913969;
    protected static final int NEW_TOP_CLICKAREA = 35913976;
    protected static final int UPPER_TAB_SPRITE = 35913756;
    protected static final int LOWER_TAB_SPRITE = 35913739;
    protected static final int INVY_FRAME = 35913984;
    protected static final int CHATBOX_FRAME = 35913986;
    protected static final int OLD_CHAT_BUTTONS = 35913728;
    protected static final int NEW_TOP_BUTTONS = 35913988;
    protected static final int NEW_BOTTOM_BUTTONS = 35914012;
    protected static final int FS_CHATBUTTONS = 35914036;
    protected static final int FIXED_CHATBUTTONS = 35913888;
    protected static final int MULTI_SPRITE = 35913793;
    protected static final int WILDY_CONTAINER = 42598407;//24969218;
    protected static final int DUEL_SPRITE = 6881280;
    protected static final int SKILL_GUIDE_BG = 32702464;

    @SuppressWarnings("ucd")
    protected static final int[] ALL_IDS = new int[]{FIXED_CHATBUTTONS, FS_CHATBUTTONS,
            NEW_BOTTOM_BUTTONS, NEW_TOP_BUTTONS, OLD_CHAT_BUTTONS, INVY_FRAME,
            CHATBOX_FRAME, UPPER_TAB_SPRITE, LOWER_TAB_SPRITE,
            NEW_BOTTOM_CLICKAREA, NEW_TOP_CLICKAREA, XP_COUNTER, RUN_BUTTON2,
            PRAY_BUTTON2, RUN_BUTTON, PRAY_BUTTON, XP_BUTTON, TOP_NEWBUTTONS,
            BOTTOM_NEWBUTTONS, TOP_OLDBUTTONS, BOTTOM_OLDBUTTONS,
            NEW_FRAME_CONTAINER, CHAT_BUTTONS_CONTAINER, LOWER_TABS_SPRITE,
            LOWER_TABS, UPPER_TABS_SPRITE, UPPER_TABS, GAME_AREA,
            GAME_AREA_1337, GAME_AREA_INTERFACE_CONTAINER, INVY_CONTAINER,
            INVY_CONTAINER_SPRITE, MINIMAP_AREA, CHATBOX_CONTAINER,
            CHATBOX_CONTAINER_SPRITE,
            SKILL_GUIDE_BG, 20971661};

    public final static void setDefaults() {
        // Only used for fullscreen atm
        for (int x = 0; x < ALL_IDS.length - 1; x++) {
            Class1034 i = Class7.getInterface(ALL_IDS[x]);
            if (i != null) {
                i.x = i.originalX;
                i.y = x == 602 && i.parent == -1 ? i.originalY - 10 : i.originalY;
                i.width = i.originalWidth;
                i.height = i.originalHeight;
                i.hidden = i.originalShow;
            }
        }


        /** XP counter **/
        Class7.getInterface(35913830).hidden = true; // xp bar container
        Class7.getInterface(32702469).mediaIdDisabled = -1;

        for (int i = 0; i < interfacesToCenter.length; i++) {
            Class922.method_902(interfacesToCenter[i][0],
                    interfacesToCenter[i][1]);
        }
        for (int i = 622; i <= 649; i++) { // clues
            Class922.method_902(i, -1);
        }
    }

    protected static boolean setGameframe = false;
    protected static boolean resizeEvent = false;

    public final static void sendInterfaceConfig(int i, int c, boolean b) {
        if (i == 548) {
            if (Class7.getInterface(OLD_CHAT_BUTTONS + c) != null)
                Class7.getInterface(OLD_CHAT_BUTTONS + c).hidden = b == false;
            else
                System.out.println("null container: " + c + ", "
                        + (OLD_CHAT_BUTTONS + c));

        }
    }

    public final static void modifyCanvas() {
        Class989.canvasDrawY = 0;
        Class3_Sub13_Sub23_Sub1.anInt4033 = 464;
        Class23.canvasWid = Class922.resizeWidth;
        Class3_Sub9.anInt2334 = Class922.resizeWidth;
        Class84.canvasDrawX = 0;
        Class1013.canvasHei = Class922.resizeHeight;
        Class70.anInt1047 = Class922.resizeHeight;
    }

    public final static void revertToFixed() {
        Class922.cameraZoom = 3;
        Class922.log_view_dist = 9;
        Class922.resizeWidth = 765;
        Class922.resizeHeight = 503;
        modifyCanvas();
        Class122.getFrame().setResizable(false);
        Class122.changeFrameToFixed();
        if (Class1012.aBoolean_617) {
            Class922.justChangedSize = true;
        }
        if (Class929.aInteger_510 == 474) {
            Class7.getInterface(Class922.XP_COUNTER).y = Class7.getInterface(Class922.XP_COUNTER).originalY - 37;
        }
        Class932.method598(false, 2, false, 0, Class922.resizeWidth,
                Class922.resizeHeight);
    }

    public final static void forceRefresh() {
        //Sets interface back to invy
        Class3_Sub13_Sub13.method232(Class1143.mainScreenInterface);
        Class124.method1746(false);
        Class3_Sub13_Sub12.executeOnLaunchScript(Class1143.mainScreenInterface);
        Class922.previousFullscreenTab = 35913980; //invy
        for (int var5 = 0; -101 < ~var5; ++var5) {
            Class921.interfacesToRefresh[var5] = true;
        }
        // Class1005.runScript(11); //set back to options tab
    }

    static final ScheduledExecutorService worker = Executors
            .newSingleThreadScheduledExecutor();

    protected static boolean justChangedSize = false;

    public final static void modZoom() {
        if (Class1012.aBoolean_617) {
            Class922.cameraZoom = 3;
        } else {
            Class922.cameraZoom = 4;
            Class922.log_view_dist = 10;
        }
    }

    public static void changeSize(int size) {
        System.out.println("Setting size: " + size + ", last size: " + clientSize);
        //size = size == 2 ? 1 : size;
        if (size > 0) {
            if (size == 1)
                Class922.prepareForResizable();
            else if (size == 2)
                Class922.prepareForFullscreen();

            Class922.reloadFullscreenInterfaces();
            Class922.clientSize = size;
            Class164_Sub1.aClass158_3009 = Class3_Sub13_Sub23_Sub1.createGraphicsBuffer(Class1143.canvas, /*765*/Class922.resizeWidth, Class922.resizeHeight);
            Class922.set = false;
        } else {
            Class922.clientSize = 0;
            Class922.revertToFixed();
            Class922.set = true;
            Class922.setDefaults();
            if (size == 2)
                Class122.rebuildFrame();
        }

        if (size == 1 && clientSize == 2)
            Class122.rebuildFrame();


        Class955.canvasReplaceRecommended = true;
        Class922.forceRefresh();
        System.gc();

    }

    public final static void prepareForResizable() {
        modZoom();
        //Class122.getFrame().setExtendedState( Class122.getFrame().getExtendedState() | JFrame.MAXIMIZED_VERT | Frame.MAXIMIZED_HORIZ);
        Class922.resizeWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        Class922.resizeHeight = Toolkit.getDefaultToolkit().getScreenSize().height - 50;
        //client.resizeWidth = (int) Class122.getFrame().getSize().getWidth();
        //client.resizeWidth = (int)Class122.getFrame().getSize().getHeight();
        modifyCanvas();

        Class122.changeFrameToResizable();
        if (Class1012.aBoolean_617) {
            Class1012.changeCanvasHeight(Class23.canvasWid, Class1013.canvasHei);
            // Class920.method_974(0, 0, client.resizeWidth,
            // client.resizeHeight);
            Class922.justChangedSize = true;
        }
        Class122.getFrame().setResizable(true);
        // Class1012.canvasHeight = resizeHeight;
        // Class1012.canvasWidth = resizeWidth;
        // Class955.canvasReplaceRecommended = true;
        if (Class59.hdEnabled) {
            Class932.method598(true, 2, true, 0, Class922.resizeWidth,
                    Class922.resizeHeight);
        } else {
            Class922.dontEnterGL = true;
            Class932.method598(false, 2, true, 0, Class922.resizeWidth,
                    Class922.resizeHeight);
            Class922.set = true;
            Class955.canvasReplaceRecommended = true;
        }
        Class922.forceRefresh();
        Class3_Sub13_Sub10.fullRedraw = true;
    }


    public final static void prepareForFullscreen() {
        System.out.println("Preparing for true fullscreen");
        modZoom();
        Class922.resizeWidth = Toolkit.getDefaultToolkit().getScreenSize().width + 16;
        Class922.resizeHeight = Toolkit.getDefaultToolkit().getScreenSize().height + 38;
        modifyCanvas();

        Class122.goFullscreen();
        if (Class1012.aBoolean_617) {
            Class1012.changeCanvasHeight(Class23.canvasWid, Class1013.canvasHei);
            Class922.justChangedSize = true;
        }
        if (Class59.hdEnabled) {
            Class932.method598(true, 2, true, 0, Class922.resizeWidth,
                    Class922.resizeHeight);
        } else {
            Class922.dontEnterGL = true;
            Class932.method598(false, 2, true, 0, Class922.resizeWidth,
                    Class922.resizeHeight);
            Class922.set = true;
            Class955.canvasReplaceRecommended = true;
        }
        Class922.forceRefresh();
        Class3_Sub13_Sub10.fullRedraw = true;
    }

    protected static boolean dontEnterGL = false;
    protected static int startSize = 0;

    public final static void reloadFullscreenInterfaces() {
        try {

            Class7.getInterface(UPPER_TABS).hidden = true;
            Class7.getInterface(LOWER_TABS).hidden = true;
            // Above added dec 31

            Class7.getInterface(FIXED_CHATBUTTONS).hidden = true;
            Class7.getInterface(FS_CHATBUTTONS).hidden = false;
            Class7.getInterface(FS_CHATBUTTONS).x = Class7
                    .getInterface(FS_CHATBUTTONS).originalX + 4;
            Class7.getInterface(FS_CHATBUTTONS).y = Class922.resizeHeight - 65;

            Class7.getInterface(NEW_TOP_BUTTONS).hidden = false;

            Class7.getInterface(OLD_CHAT_BUTTONS).hidden = false;

            if (!Class922.inventoryHidden) {
                Class7.getInterface(INVY_FRAME).hidden = false;
            }
            Class7.getInterface(INVY_FRAME).x = Class922.resizeWidth - 196 - 25;
            Class7.getInterface(INVY_FRAME).y = Class922.resizeHeight - 269 - 75
                    - 5 - (Class922.resizeWidth < 1000 ? 36 : 0);


            if (Class922.clientSize > 0 && (Class929.aInteger_510 >= 525 || chatboxHidden))
                Class7.getInterface(CHATBOX_FRAME).hidden = true;
            else
                Class7.getInterface(CHATBOX_FRAME).hidden = false;

            Class7.getInterface(CHATBOX_FRAME).x = Class7
                    .getInterface(CHATBOX_FRAME).originalX - 7 + 1;
            Class7.getInterface(CHATBOX_FRAME).y = Class922.resizeHeight - 158 - 30 - 15;


            Class7.getInterface(XP_BUTTON).hidden = false;
            Class7.getInterface(XP_BUTTON).x = Class922.resizeWidth - 774;
            Class7.getInterface(XP_BUTTON).y = -40;
            Class7.getInterface(XP_COUNTER).x = Class922.resizeWidth - 220 - 111 - 77;
            Class7.getInterface(XP_COUNTER).y = Class7.getInterface(XP_COUNTER).originalY - 38;


            Class7.getInterface(RUN_BUTTON).x = Class922.resizeWidth - 224;
            Class7.getInterface(RUN_BUTTON).y = Class7.getInterface(RUN_BUTTON).originalY + 5;

            Class7.getInterface(PRAY_BUTTON).x = Class922.resizeWidth - 238;
            Class7.getInterface(PRAY_BUTTON).y = Class7
                    .getInterface(PRAY_BUTTON).originalY + 5;

            Class7.getInterface(RUN_BUTTON2).hidden = true;
            Class7.getInterface(PRAY_BUTTON2).hidden = true;

            Class7.getInterface(NEW_FRAME_CONTAINER).hidden = false;
            Class7.getInterface(NEW_FRAME_CONTAINER).width = Class922.resizeWidth - 10;
            Class7.getInterface(NEW_FRAME_CONTAINER).height = Class922.resizeHeight - 10;
            Class7.getInterface(INVY_CONTAINER_SPRITE).alpha = 255;
            Class7.getInterface(CHATBOX_CONTAINER_SPRITE).alpha = 255;
            Class7.getInterface(NEW_TOP_CLICKAREA).hidden = false;
            Class7.getInterface(NEW_TOP_CLICKAREA).x = Class922.resizeWidth - 10
                    - 30 - (249 * 2) + 15 + 30 + 9
                    + (Class922.resizeWidth < 1000 ? 228 : -4);
            Class7.getInterface(NEW_TOP_CLICKAREA).y = Class922.resizeHeight - 75
                    - 5 - (Class922.resizeWidth < 1000 ? 34 : 0);

            // Clickareas - bottom
            Class7.getInterface(NEW_BOTTOM_CLICKAREA).hidden = false;
            Class7.getInterface(NEW_BOTTOM_CLICKAREA).x = Class922.resizeWidth
                    - 10 - 30 - 249 + 30 + 2
                    + (Class922.resizeWidth < 1000 ? -1 : -2);
            Class7.getInterface(NEW_BOTTOM_CLICKAREA).y = Class922.resizeHeight - 75 - 5;

            Class7.getInterface(NEW_TOP_BUTTONS).x = Class922.resizeWidth - 10
                    - 30 - (249 * (Class922.resizeWidth < 1000 ? 1 : 2))
                    + (Class922.resizeWidth < 1000 ? 38 : 30 + 18 + 11 - 2);
            Class7.getInterface(NEW_TOP_BUTTONS).y = Class922.resizeHeight - 75
                    - (Class922.resizeWidth < 1000 ? 36 : 0) - 5;
            Class7.getInterface(NEW_TOP_BUTTONS).hidden = false;

            Class7.getInterface(TOP_NEWBUTTONS).hidden = true;

            Class7.getInterface(NEW_BOTTOM_BUTTONS).x = Class922.resizeWidth
                    - 249 + 30 - 30 /* + 10 */
                    + (Class922.resizeWidth < 1000 ? -2 : -2);
            Class7.getInterface(NEW_BOTTOM_BUTTONS).y = Class922.resizeHeight - 75 - 5;
            Class7.getInterface(NEW_BOTTOM_BUTTONS).hidden = false;

            Class7.getInterface(BOTTOM_NEWBUTTONS).hidden = true;

            Class7.getInterface(TOP_OLDBUTTONS).hidden = true;
            Class7.getInterface(BOTTOM_OLDBUTTONS).hidden = true;

            Class7.getInterface(CHAT_BUTTONS_CONTAINER).y = Class922.resizeHeight - 60;

            Class7.getInterface(MINIMAP_AREA).x = Class922.resizeWidth - 198;
            Class7.getInterface(MINIMAP_AREA).y = Class7
                    .getInterface(MINIMAP_AREA).originalY + 2;
            Class7.getInterface(MINIMAP_AREA).hidden = false;

            Class7.getInterface(GAME_AREA).width = Class922.resizeWidth - 10;
            Class7.getInterface(GAME_AREA).height = Class922.resizeHeight - 30 - 6;
            Class7.getInterface(GAME_AREA).x = Class7.getInterface(GAME_AREA).originalX - 2;
            Class7.getInterface(GAME_AREA).y = Class7.getInterface(GAME_AREA).originalY - 2;

            Class7.getInterface(GAME_AREA_INTERFACE_CONTAINER).width = Class922.resizeWidth /*/ 2*/;
            Class7.getInterface(GAME_AREA_INTERFACE_CONTAINER).height = Class922.resizeHeight /*/ 2*/;

            Class7.getInterface(GAME_AREA_1337).width = Class922.resizeWidth - 10;
            Class7.getInterface(GAME_AREA_1337).height = Class922.resizeHeight - 30 - 6;
            Class7.getInterface(GAME_AREA_1337).x = Class7
                    .getInterface(GAME_AREA_1337).originalX - 2;
            Class7.getInterface(GAME_AREA_1337).y = Class7
                    .getInterface(GAME_AREA_1337).originalY - 2;

            Class7.getInterface(INVY_CONTAINER).x = Class922.resizeWidth - 206 - 9;
            Class7.getInterface(INVY_CONTAINER).y = Class922.resizeHeight - 269
                    - 37 - 30 - 7 - (Class922.resizeWidth < 1000 ? 36 : 0);

            Class7.getInterface(CHATBOX_CONTAINER).y = Class922.resizeHeight - 192 - 5;
            Class7.getInterface(CHATBOX_CONTAINER).x = Class7
                    .getInterface(CHATBOX_CONTAINER).originalX - 1 + 2;
            Class7.getInterface(CHATBOX_CONTAINER).height = 128 + 2;
            Class7.getInterface(CHATBOX_CONTAINER).width = 505 + 1;

            for (int i = CHATBOX_CONTAINER; i < CHATBOX_CONTAINER + 7; i++) {
                Class7.getInterface(i).height = 128 + 2;
                Class7.getInterface(i).width = 505 + 2;
            }

            Class7.getInterface(SKILL_GUIDE_BG).x = 0;
            Class7.getInterface(SKILL_GUIDE_BG).y = 0;
            Class7.getInterface(SKILL_GUIDE_BG).width = Class922.resizeWidth;
            Class7.getInterface(SKILL_GUIDE_BG).height = Class922.resizeHeight;

            Class7.getInterface(30474364).disabledSprite = -1;

            Class7.getInterface(32702469).mediaIdDisabled = -1;

            for (int i = 0; i < interfacesToCenter.length; i++) {
                Class922.method_902(interfacesToCenter[i][0],
                        interfacesToCenter[i][1]);
            }
            for (int i = 622; i <= 649; i++) { // clues
                Class922.method_902(i, -1);
            }
        } catch (Exception e) {
            System.out.println("Exception while resising interfaces: " + e);
        }
    }

    public final static int[][] interfacesToCenter = {
            // -1 means only children without buffer parent, 0 means any children
            // within.
            {606, 0}, {269, 0}/*, { 378, 0 }*/, {499, 0},
            /*{ 17, 0 },*/ {334, 0}, {335, 0}, {553, 0}, {652, 0},

            {615, -1}, {609, -1}, {610, -1}, {611, -1}, {613, -1},
            {614, -1}, {617, -1}, {618, -1}, {619, -1}, {604, -1},
            {603, -1}, {583, -1}, {275, -1}, {276, -1}, {300, -1},
            {102, -1}, {465, -1}, {12, -1}, {106, -1}, {107, -1},
            {108, -1}, {109, -1}, {110, -1}, {345, -1}, {364, -1}, {598, -1}, {599, -1},
            {312, -1}, {650, -1}, {651, -1}, {602, -1},

    };

    public static final void method_902(int actualInter, int parent) {
        Class1034[] var3 = Class1031.interfaceCache[actualInter];
        if (var3 == null)
            return;
        boolean noParent = parent == -1 ? true : false;

        for (int i = 0; i < var3.length; i++) {
            Class1034 child = var3[i];
            if (noParent) {
                if (child.parent != -1)
                    continue;
            }
            if (Class922.clientSize > 0 || Class922.startSize > 0) {
                int negativeXAdjust = actualInter == 378 || actualInter == 17 ? 130
                        : 0;
                int positiveXAdjust = actualInter == 599 ? 97 : actualInter == 650 ? 20 : 0;
                if (Class922.resizeWidth < 1000) {
                    child.x = child.originalX
                            + Class922.resizeWidth
                            / 2
                            - (275)
                            - negativeXAdjust - 100
                            + positiveXAdjust;
                } else {
                    child.x = child.originalX
                            + Class922.resizeWidth
                            / 2
                            - (275)
                            - negativeXAdjust
                            + positiveXAdjust;
                }
                child.y = actualInter != 650 && actualInter != 651 && actualInter != 652 ?
                        (actualInter != 606 ? (child.uid == 18087940 ? Class922.resizeHeight / 2
                                : actualInter == 599 ? 10 : child.originalY + Class922.resizeHeight / 2 - (275))
                                : child.originalY)
                        : child.originalY;
            } else {
                child.x = child.originalX;
                child.y = child.originalY;
            }

        }
    }

    public static int getAnimationLengths() {
        return 7814;
    }

    public static int getAnimationConfig(int id) {
        return 12;
    }

    public static int getGraphicConfig(int id) {
        return 13;
    }

    public static int getItemsConfig(int id) {
        return 10;
    }

    public static Class1027 getModelJs5(int id) {
        return Class159.cacheIndex7;
    }

    public static int getNPCConfig(int id) {
        return 9;
    }

    public static int getModelsIDX() {
        return 7;
    }

    public static int getSkinsIDX() {
        return 1;
    }

    public static int getSkeletonsIDX() {
        return 0;
    }

    protected static int anInt396;
    protected static int anInt384 = 0;
    protected static Class1008 nullString = null;
    protected static Class1008 dashjString = Class943.create(")2");
    protected static Class1008 discardjString = Class943.create("Discard");
    protected static CRC32 aCRC32_377 = new CRC32();
    protected static volatile int anInt362 = 0;
    protected static int anInt377 = 0;
    protected static int headIconsId;
    protected static int[] anIntArray356 = new int[]{1, 0, -1, 0};
    protected static Class1008 lastPmFrom = null;

    protected static void drawRedstones() {
        if (aInteger_514 > 520 && aInteger_514 < 1558 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 0;
        }
        if (aInteger_514 > 525 + 33 && aInteger_514 < 1560 + 133 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 1;
        }
        if (aInteger_514 > 525 + 66 && aInteger_514 < 1560 + 66 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 2;
        }
        if (aInteger_514 > 525 + 99 && aInteger_514 < 1560 + 99 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 3;
        }
        if (aInteger_514 > 525 + 132 && aInteger_514 < 1560 + 132 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 4;
        }
        if (aInteger_514 > 525 + 165 && aInteger_514 < 1560 + 165 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 5;
        }
        if (aInteger_514 > 525 + 198 && aInteger_514 < 1560 + 198 && aInteger_513 > 168 && aInteger_513 < 208) {
            tabID = 6;
        }
        if (aInteger_514 > 525 && aInteger_514 < 1560 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 7;
        }
        if (aInteger_514 > 525 + 33 && aInteger_514 < 1560 + 33 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 8;
        }
        if (aInteger_514 > 525 + 66 && aInteger_514 < 1560 + 66 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 9;
        }
        if (aInteger_514 > 525 + 99 && aInteger_514 < 1560 + 99 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 10;
        }
        if (aInteger_514 > 525 + 132 && aInteger_514 < 1560 + 132 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 11;
        }
        if (aInteger_514 > 525 + 165 && aInteger_514 < 1560 + 165 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 12;
        }
        if (aInteger_514 > 525 + 198 && aInteger_514 < 1560 + 198 && aInteger_513 > 466 && aInteger_513 < 506) {
            tabID = 13;
        }
        chatbuttonselected.drawSprite(5, 480);
        for (int i = 0; i < 5; i++) {
            redStone1.drawSprite(560 + (33 * i), 168);

        }
        for (int i = 0; i < 5; i++) {
            redStone1.drawSprite(560 + (33 * i), 466);
        }
        if (tabID < 7) {
            if (tabID == 0) {
                redStone2.drawSprite(555 + (33 * (tabID - 1)), 168);
            } else {
                if (tabID == 6) {
                    redStone3.drawSprite(560 + (33 * (tabID - 1)), 168);
                } else {
                    redStone0.drawSprite(560 + (33 * (tabID - 1)), 168);
                }
            }
        } else {
            if (tabID == 7) {
                redStone4.drawSprite(324 + (33 * (tabID - 1)), 466);
            } else {
                if (tabID == 13) {
                    redStone5.drawSprite(329 + (33 * (tabID - 1)), 466);
                } else {
                    redStone0.drawSprite(329 + (33 * (tabID - 1)), 466);
                }
            }
        }
    }

    final void method_754(int var1) {
        try {
            if (~Class922.aInteger_544 != -1001) {
                boolean var2 = Class1001.method1988();
                if (var2 && Class83.aBoolean1158
                        && Class1228.aClass155_2627 != null) {
                    Class1228.aClass155_2627.method2158();
                }

                if ((30 == Class922.aInteger_544 || 10 == Class922.aInteger_544)
                        && (Class955.canvasReplaceRecommended || ~Class53.aLong866 != -1L
                        && Class53.aLong866 < Class1219
                        .currentTimeMillis())) {
                    // System.out.println("setLowDefinition - fsdraw");
                    Class1031.setLowDefinition(
                            Class955.canvasReplaceRecommended,
                            Class83.getDisplayMode(), /*
                             * Texture.
                             * fullscreenFrameWidth
                             */Class922.resizeWidth, /*
                             * Class3_Sub13_Sub5
                             * .
                             * fullscreenFrameHeight
                             */
                            Class922.resizeHeight);
                }

                int var4;
                int var5;
                if (null == Class3_Sub13_Sub10.fullscreenFrame) {
                    Object var3;
                    if (Class3_Sub13_Sub10.fullscreenFrame != null) {
                        var3 = Class3_Sub13_Sub10.fullscreenFrame;
                    } else if (Class3_Sub13_Sub7.resizableFrame != null) {
                        var3 = Class3_Sub13_Sub7.resizableFrame;
                    } else {
                        var3 = Class38.gameClass942.thisApplet;
                    }

                    var4 = ((Container) var3).getSize().width;
                    var5 = ((Container) var3).getSize().height;
                    if (var3 == Class3_Sub13_Sub7.resizableFrame) {
                        Insets var6 = Class3_Sub13_Sub7.resizableFrame
                                .getInsets();
                        var4 -= var6.right + var6.left;
                        var5 -= var6.top + var6.bottom;
                    }

                    if (var4 != Class3_Sub9.anInt2334
                            || ~var5 != ~Class70.anInt1047) {
                        if (Class942.osName.startsWith("mac")) {
                            Class3_Sub9.anInt2334 = var4;
                            Class70.anInt1047 = var5;
                        } else {
                            Class119.method1729();
                        }

                        Class53.aLong866 = Class1219.currentTimeMillis() + 500L;
                    }
                }

                if (Class3_Sub13_Sub10.fullscreenFrame != null
                        && !Class3_Sub13_Sub6.focus
                        && (30 == Class922.aInteger_544 || 10 == Class922.aInteger_544)) {
                    // System.out.println("setLowDefinition - in method_754 #2");
                    Class1031.setLowDefinition(false, Class1002.anInt2577, -1, -1);
                }

                boolean var10 = false;
                if (Class3_Sub13_Sub10.fullRedraw) {
                    var10 = true;
                    Class3_Sub13_Sub10.fullRedraw = false;
                }

                if (var10) {
                    Class80.method1396();
                }

                if (Class1012.aBoolean_617) {
                    for (var4 = 0; ~var4 > -101; ++var4) {
                        Class921.interfacesToRefresh[var4] = true;
                    }
                }
                /*
                 * System.out.println("gamestate: " + Class143.aInteger_544); if
                 * (!closed_revision_choosing && Class143.aInteger_544 == 5) {
                 * Class80.drawScreen(); } else
                 */
                if (!Class929.aBoolean_606 && Class1025.loadingStage == 30) {
                    drawScreen();
                    return;
                }

                if (Class922.aInteger_544 == 0) {
                    Class3_Sub28_Sub1.method_454(new Color(Class929.aInteger_509),
                            var10, Class1227.loading_completion,
                            Class951.anInt3684);
                    return;
                } else if (5 == Class922.aInteger_544) {
                    Class922.secondLoadingBar(false, getBoldClass1019());
                } else if (10 != Class922.aInteger_544) {
                    if (25 != Class922.aInteger_544 && 28 != Class922.aInteger_544) {
                        if (Class922.aInteger_544 == GameState.LOGGED_IN) {
                            Class49.method1127();
                        } else if (GameState.RECONNECTING == Class922.aInteger_544) {
                            Class922.drawTextOnScreen(
                                    Class922.combinejStrings(new Class1008[]{
                                            Class136.aClass94_1773,
                                            ByteBuffer.newLinejString,
                                            Class154.aClass94_1959}), false);
                        }
                    } else if (Class163_Sub2_Sub1.loadingScreenType != 1) {
                        if (Class163_Sub2_Sub1.loadingScreenType == 2) {
                            if (Class3_Sub5.anInt2275 < Class162.modelFetchCount) {
                                Class3_Sub5.anInt2275 = Class162.modelFetchCount;
                            }

                            var4 = (-Class162.modelFetchCount + Class3_Sub5.anInt2275)
                                    * 50 / Class3_Sub5.anInt2275 + 50;
                            Class922.drawTextOnScreen(
                                    Class922.combinejStrings(new Class1008[]{
                                            Class3_Sub13_Sub23.loadingString,
                                            Class3_Sub13_Sub33.aClass94_3399,
                                            Class72.createInt(var4),
                                            Class10.aClass94_148}), false);
                        } else {
                            Class922.drawTextOnScreen(
                                    Class3_Sub13_Sub23.loadingString, false);
                        }
                    } else {
                        if (~Class1030.anInt2579 > ~Class3_Sub13_Sub24.anInt3293) {
                            Class1030.anInt2579 = Class3_Sub13_Sub24.anInt3293;
                        }

                        var4 = 50
                                * (Class1030.anInt2579 + -Class3_Sub13_Sub24.anInt3293)
                                / Class1030.anInt2579;
                        Class922.drawTextOnScreen(
                                Class922.combinejStrings(new Class1008[]{
                                        Class3_Sub13_Sub23.loadingString,
                                        Class3_Sub13_Sub33.aClass94_3399,
                                        Class72.createInt(var4),
                                        Class10.aClass94_148}), false);
                    }
                } else {
                    Class1227.method381();
                }

                if (Class1012.aBoolean_617 && -1 != ~Class922.aInteger_544) {
                    Class1012.method1826();

                    for (var4 = 0; ~var4 > ~Class3_Sub28_Sub3.anInt3557; ++var4) {
                        Class163_Sub1_Sub1.aBooleanArray4008[var4] = false;
                    }
                } else {
                    Graphics var11;
                    if ((~Class922.aInteger_544 == -31 || GameState.LOGIN_SCREEN == Class922.aInteger_544)
                            && ~Class951.anInt3689 == -1 && !var10) {
                        try {
                            var11 = Class1143.canvas.getGraphics();

                            for (var5 = 0; Class3_Sub28_Sub3.anInt3557 > var5; ++var5) {
                                if (Class163_Sub1_Sub1.aBooleanArray4008[var5]) {
                                    Class164_Sub1.aClass158_3009
                                            .clip(var11,
                                                    Class155.anIntArray1969[var5],
                                                    Class946.anIntArray3954[var5],
                                                    Class3_Sub28_Sub18.anIntArray3768[var5],
                                                    Class991.anIntArray2794[var5]);
                                    Class163_Sub1_Sub1.aBooleanArray4008[var5] = false;
                                }
                            }
                        } catch (Exception var8) {
                            Class1143.canvas.repaint();
                        }
                    } else if (0 != Class922.aInteger_544) {
                        try {
                            var11 = Class1143.canvas.getGraphics();
                            Class164_Sub1.aClass158_3009.drawGraphics(var11, 0,
                                    0);

                            for (var5 = 0; var5 < Class3_Sub28_Sub3.anInt3557; ++var5) {
                                Class163_Sub1_Sub1.aBooleanArray4008[var5] = false;
                            }
                        } catch (Exception var7) {
                            Class1143.canvas.repaint();
                        }
                    }
                }

                if (Class58.aBoolean913) {
                    Class75_Sub3.method1346(26211);
                }

                if (Class1008.safeMode
                        && Class922.aInteger_544 == GameState.LOGIN_SCREEN
                        && 0 != ~Class1143.mainScreenInterface) {
                    Class1008.safeMode = false;
                    Class119.writePreferences(Class38.gameClass942);
                }

                if (!Class1012.aBoolean_617 && Class922.aInteger_544 == 10
                        && (System.currentTimeMillis() - lastLdRefresh >= 100)) {
                    for (int i = 39387136; i < (39387136 + 17); i++) {
                        Class20.refreshInterface(Class7.getInterface(i));
                    }
                    Class922.lastLdRefresh = System.currentTimeMillis();
                }
                if (Class922.clientSize > 0
                        && Class922.aInteger_544 == GameState.LOGGED_IN) {
                    if (!set) {
                        System.out.println("Setting fullscreen");
                        Class922.reloadFullscreenInterfaces();
                        if (Class922.clientSize == 2) {
                            Class122.goFullscreen();
                        }
                        Class932.method598(false, 2, false, 0,
                                Class922.resizeWidth, Class922.resizeHeight);
                        Class122.setCursor(0);
                        set = true;
                    }

                    if (Class922.resizeEvent && (System.currentTimeMillis() - Class922.lastResize) > 500) {
                        Class922.lastResize = System.currentTimeMillis();
                        Class922.resizeEvent = false;
                        Class122.resize();
                    }

                    if (!Class1012.aBoolean_617
                            && (System.currentTimeMillis() - lastLdRefresh >= 9)) {
                        Class20.refreshInterface(Class7
                                .getInterface(INVY_CONTAINER));
                        Class20.refreshInterface(Class7
                                .getInterface(INVY_CONTAINER_SPRITE));
                        Class20.refreshInterface(Class7
                                .getInterface(CHATBOX_CONTAINER));
                        Class20.refreshInterface(Class7
                                .getInterface(CHATBOX_CONTAINER_SPRITE));
                        Class20.refreshInterface(Class7
                                .getInterface(Class922.CHATBOX_FRAME));
                        Class20.refreshInterface(Class7
                                .getInterface(Class922.INVY_FRAME));
                        Class20.refreshInterface(Class7
                                .getInterface(Class922.CHAT_BUTTONS_CONTAINER));
                        Class20.refreshInterface(Class7.getInterface(XP_BUTTON));
                        Class20.refreshInterface(Class7
                                .getInterface(Class922.NEW_BOTTOM_BUTTONS));
                        Class20.refreshInterface(Class7
                                .getInterface(Class922.NEW_TOP_BUTTONS));
                        Class922.lastLdRefresh = System.currentTimeMillis();
                    }
                } else if (!Class1012.aBoolean_617 && Class922.clientSize == 0) {
                    if (System.currentTimeMillis() - lastLdRefresh >= 180) {
                        Class20.refreshInterface(Class7
                                .getInterface(35913809));
                        Class20.refreshInterface(Class7
                                .getInterface(35913808));
                        if (Class929.aInteger_510 <= 474) {
                            Class20.refreshInterface(Class7
                                    .getInterface(35913784));
                            Class20.refreshInterface(Class7
                                    .getInterface(35913778));
                        }
                        Class20.refreshInterface(Class7
                                .getInterface(INVY_CONTAINER));
                        Class20.refreshInterface(Class7
                                .getInterface(8978433));
                        Class20.refreshInterface(Class7
                                .getInterface(35913839));
                        Class922.lastLdRefresh = System.currentTimeMillis();
                    }
                }
            }
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "client.K(" + var1 + ')');
        }
    }

    protected static int LAST_STATE = -1;

    public final static class GameState {
        protected static final int RECONNECTING = 40;
        protected static final int LOGIN_SCREEN = 10;
        protected static final int WELCOME = 25;
        protected static final int LOGGED_IN = 30;
    }

    protected static long lastLdRefresh = 0, lastResize = 0;
    protected static boolean resizeLD = false;
    protected static boolean set = false;

    public static final Class1034 method42(Class1034 var0) {
        int var1 = Class1034.getInterfaceClickMask(var0).method94();
        if (var1 == 0) {
            return null;
        } else {
            for (int var2 = 0; var2 < var1; ++var2) {
                var0 = Class7.getInterface(var0.parent);
                if (var0 == null) {
                    return null;
                }
            }

            return var0;
        }
    }

    public static void method43(boolean var0) {
        try {
            if (!var0) {
                aClass3_Sub11ArrayArray2199 = (Class3_Sub11[][]) ((Class3_Sub11[][]) null);
            }

            mapsArray = null;
            class1017_2 = null;
            aClass3_Sub11ArrayArray2199 = (Class3_Sub11[][]) null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "client.O(" + var0 + ')');
        }
    }

    final void method32(byte var1) {
        try {
            if (Class1012.aBoolean_617) {
                Class1012.close();
            }

            if (null != Class3_Sub13_Sub10.fullscreenFrame) {
                Class1028.revertToResizable(
                        Class3_Sub13_Sub10.fullscreenFrame,
                        Class38.gameClass942);
                Class3_Sub13_Sub10.fullscreenFrame = null;
            }

            if (null != Class38.gameClass942) {
                Class38.gameClass942.method1442(getClass(), 0);
            }

            if (null != Class989.aClass67_1443) {
                Class989.aClass67_1443.aBoolean1015 = false;
            }

            Class989.aClass67_1443 = null;
            if (Class3_Sub15.worldConnection != null) {
                Class3_Sub15.worldConnection.close();
                Class3_Sub15.worldConnection = null;
            }

            Class163_Sub1_Sub1.method2215(Class1143.canvas, -9320);
            Class1017_2.method1783(Class1143.canvas);
            if (null != Class38.mouseWheelHandler) {
                Class38.mouseWheelHandler.removeMouseWheel(Class1143.canvas);
            }

            Class3_Sub13_Sub1.method167(0);
            Class980.method2090(8);
            Class38.mouseWheelHandler = null;
            if (null != Class1228.aClass155_2627) {
                Class1228.aClass155_2627.method2163(false);
            }

            if (null != Class3_Sub21.aClass155_2491) {
                Class3_Sub21.aClass155_2491.method2163(false);
            }

            Class58.class1006.method1254(false);
            Class3_Sub13_Sub14.aClass73_3159.method1304(3208);

            try {
                if (Class101.cacheFile != null) {
                    Class101.cacheFile.method980(false);
                }

                if (var1 <= 20) {
                    return;
                }

                if (Class163_Sub2.indexFiles != null) {
                    for (int var2 = 0; var2 < Class163_Sub2.indexFiles.length; ++var2) {
                        if (null != Class163_Sub2.indexFiles[var2]) {
                            Class163_Sub2.indexFiles[var2].method980(false);
                        }
                    }
                }

                if (null != Class114.aClass30_1572) {
                    Class114.aClass30_1572.method980(false);
                }

                if (null != Class1043.aClass30_1039) {
                    Class1043.aClass30_1039.method980(false);
                }
            } catch (IOException var3) {
                ;
            }

        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "client.F(" + var1 + ')');
        }
    }

    public final void init() {
        try {
            if (method29()) {
                ObjectDefinition.worldId = Integer
                        .parseInt(getParameter("worldid"));
                Class1134.modeWhere = Integer.parseInt(getParameter("modewhere"));
                if (0 > Class1134.modeWhere || 1 < Class1134.modeWhere) {
                    Class1134.modeWhere = 0;
                }

                Class3_Sub13_Sub13.modeWhat = Integer
                        .parseInt(getParameter("modewhat"));
                if (~Class3_Sub13_Sub13.modeWhat > -1
                        || Class3_Sub13_Sub13.modeWhat > 2) {
                    Class3_Sub13_Sub13.modeWhat = 0;
                }

                /*
                 * String var1 = getParameter("advertsuppressed"); if(var1 !=
                 * null && var1.equals("1")) {
                 * Class3_Sub28_Sub19.advertSuppressed = true; } else {
                 * Class3_Sub28_Sub19.advertSuppressed = false; }
                 *
                 * /*try { Class3_Sub20.language =
                 * Integer.parseInt(getParameter("lang")); } catch (Exception
                 * var10) { Class3_Sub20.language = 0; }
                 */

                /*
                 * String var2 = getParameter("objecttag"); if(var2 != null &&
                 * var2.equals("1")) { Class163_Sub2_Sub1.objectTag = true; }
                 * else { Class163_Sub2_Sub1.objectTag = false; }
                 *
                 * String var3 = getParameter("js"); if(null != var3 &&
                 * var3.equals("1")) { Class3_Sub28_Sub11.aBoolean3641 = true; }
                 * else { Class3_Sub28_Sub11.aBoolean3641 = false; }
                 *
                 * /*try { Class3_Sub26.anInt2554 =
                 * Integer.parseInt(getParameter("affid")); } catch (Exception
                 * var9) { Class3_Sub26.anInt2554 = 0; }
                 */

                /*
                 * Class163_Sub2.aClass94_2996 =
                 * Class974.aClass94_1745.method1573((byte)126, this);
                 * if(Class163_Sub2.aClass94_2996 == null) {
                 * Class163_Sub2.aClass94_2996 = Class921.aClass94_3672; }
                 */

                /*
                 * String var5 = getParameter("country"); if(var5 != null) { try
                 * { Class1207.anInt2607 = Integer.parseInt(var5); }
                 * catch (Exception var8) { Class1207.anInt2607 = 0; }
                 * }
                 */

                /*
                 * String var6 = getParameter("haveie6"); if(null != var6 &&
                 * var6.equals("1")) { Class106.aBoolean1451 = true; } else {
                 * Class106.aBoolean1451 = false; }
                 */

                Class1015.aClass9221671 = this;
                /*
                 * if (client.clientSize > 0) { createFixedFrame(client.resizeWidth,
                 * 32 + Class3_Sub13_Sub13.modeWhat, 464, client.resizeHeight);
                 * } else { createFixedFrame(765, 32 +
                 * Class3_Sub13_Sub13.modeWhat, 464, 503); }
                 */
                Class922.resizeWidth = (int) Toolkit.getDefaultToolkit()
                        .getScreenSize().getWidth();
                Class922.resizeHeight = (int) Toolkit.getDefaultToolkit()
                        .getScreenSize().getHeight();
                /*
                 * createFixedFrame(client.resizeWidth, 32 +
                 * Class3_Sub13_Sub13.modeWhat, 464, client.resizeHeight);
                 */
                createFixedFrame(765, 32 + Class3_Sub13_Sub13.modeWhat, 464,
                        503);

                // We don't use the method below because it creates an entirely
                // new frame, when we can simply resize current one.
                // createResizableFrame(32 + Class3_Sub13_Sub13.modeWhat, 464,
                // true, 1000, "stringer", 800, client.aInteger_512);

            }
        } catch (RuntimeException var11) {
            throw Class1134.method1067(var11, "client.init()");
        }
    }

    public void resizeLD() {
        // if(Class1143.canvas != null) {
        // Class1143.canvas.removeFocusListener(this);
        // Class1143.canvas.getParent().remove(Class1143.canvas);
        // }
        System.out.println("resizing LD frame");
        Class989.canvasDrawY = 0;
        Class3_Sub13_Sub23_Sub1.anInt4033 = 464;
        Class23.canvasWid = Class922.resizeWidth;
        Class3_Sub9.anInt2334 = Class922.resizeWidth;
        Class84.canvasDrawX = 0;
        Class1013.canvasHei = Class922.resizeHeight;
        Class70.anInt1047 = Class922.resizeHeight;
        // createFixedFrame(client.resizeWidth, 32 +
        // Class3_Sub13_Sub13.modeWhat, 464, client.resizeHeight);
    }

    final void method39() {
        Class119.method1729();
        Class3_Sub13_Sub14.aClass73_3159 = new Class943();
        Class58.class1006 = new Class1006();
        if (Class3_Sub13_Sub13.modeWhat != 0) {
            Class3_Sub6.aByteArrayArray2287 = new byte[50][];
        }

        Class1048.anInt2451 = ObjectDefinition.worldId;
        Class956.method564(Class38.gameClass942, 0);
        /*
         * if(Class1134.modeWhere != 0) { if(Class1134.modeWhere == 1) {
         * Class1244.aString2121 = getCodeBase().getHost();
         * Class53.anInt867 = Class984.worldId + 50000;
         * Class3_Sub28_Sub19.anInt3773 = 40000 + Class984.worldId; }
         * else if(Class1134.modeWhere == 2) { Class1244.aString2121 =
         * "66.150.121.228"; Class53.anInt867 = Class984.worldId +
         * '\uc350'; Class3_Sub28_Sub19.anInt3773 = Class984.worldId +
         * '\u9c40'; } } else { Class1244.aString2121 =
         * getCodeBase().getHost(); Class53.anInt867 = 443;
         * Class3_Sub28_Sub19.anInt3773 = '\uaa4a'; }
         */

        // TODO: fix cursors(either load them all at once or include them in the
        // jar
        // TODO: add jframe and bar for screenshot n shit
        // change ip
        // obfuscate

        Class1244.aString2121 = aString2003;
        Class53.anInt867 = 43594;
        Class3_Sub28_Sub19.anInt3773 = 43594;

        // if(1 != Class158.anInt2014) {
        Class15.aShortArrayArray344 = Class1143.aShortArrayArray3654;
        Class972.aShortArray1311 = Class3_Sub13_Sub28.aShortArray3349;
        Class101.aShortArrayArray1429 = Class20.aShortArrayArray435;
        Class1042_4.aShortArray2548 = Class164_Sub1.aShortArray3011;
        /*
         * } else { Class101.aBoolean1419 = true; Class92.anInt1322 = 16777215;
         * Class92.fogColor = 0; Class15.aShortArrayArray344 =
         * Class118.aShortArrayArray1619; Class101.aShortArrayArray1429 =
         * Class75_Sub1.aShortArrayArray2634; Class3_Sub25.aShortArray2548 =
         * Class2.aShortArray63; Class972.aShortArray1311 =
         * Class949.aShortArray2219; }
         */

        Class26.anInt506 = Class53.anInt867;
        Class162.anInt2036 = Class3_Sub28_Sub19.anInt3773;
        Class38_Sub1.aString2611 = Class1244.aString2121;
        Class123.anInt1658 = Class3_Sub28_Sub19.anInt3773;
        Class3_Sub13_Sub38.aShortArray3455 = Class3_Sub13_Sub9.aShortArray3110 = Class136.aShortArray1779 = Class3_Sub13_Sub38.aShortArray3453 = new short[256];
        Class140_Sub6.anInt2894 = Class123.anInt1658;
        if (Class942.anInt1214 == 3 && 2 != Class1134.modeWhere) {
            Class1048.anInt2451 = ObjectDefinition.worldId;
        }

        Class3_Sub22.synchronizeKeyCodes();
        Class3_Sub13_Sub4.addKeyboardHandler(Class1143.canvas);
        ItemDefinition.addMouseHandler(Class1143.canvas);
        Class38.mouseWheelHandler = Class21.createMouseWheel();
        if (null != Class38.mouseWheelHandler) {
            Class38.mouseWheelHandler.addMouseWheel(Class1143.canvas);
        }

        Class163_Sub1.anInt2994 = Class942.anInt1214;

        try {
            if (Class38.gameClass942.aClass122_1198 != null) {
                Class101.cacheFile = new Class1046(
                        Class38.gameClass942.aClass122_1198, 5200, 0);

                for (int var2 = 0; var2 < Class922.aInteger_512; ++var2) {
                    Class163_Sub2.indexFiles[var2] = new Class1046(
                            Class38.gameClass942.aClass122Array1197[var2],
                            6000, 0);
                }

                Class114.aClass30_1572 = new Class1046(
                        Class38.gameClass942.aClass122_1204, 6000, 0);
                Class930.referenceCache = new Class988(255,
                        Class101.cacheFile, Class114.aClass30_1572, 500000);
                Class1043.aClass30_1039 = new Class1046(
                        Class38.gameClass942.aClass122_1207, 24, 0);
                Class38.gameClass942.aClass122Array1197 = null;
                Class38.gameClass942.aClass122_1204 = null;
                Class38.gameClass942.aClass122_1207 = null;
                Class38.gameClass942.aClass122_1198 = null;
            }
        } catch (IOException var3) {
            Class1043.aClass30_1039 = null;
            Class101.cacheFile = null;
            Class114.aClass30_1572 = null;
            Class930.referenceCache = null;
        }

        Class167.aClass94_2083 = Class25.aClass94_485;
    }

    final void method33(int var1) {
        try {
            method43(true);
            Class1008.method1541(-8635);
            Class38.method1024();
            Class3_Sub28_Sub3.method542((byte) -46);
            Class131.method1792(0);
            Class1021.method26();
            Class1012.method1838();
            Class963.method1766(24241);
            Class923.method2181(false);
            Class67.method1257(25951);
            Class1025.method1588((byte) 106);
            ByteBuffer.method767(0);
            Class1007.method1463(0);
            Class1006.method1242();
            Class943.method1306(-16222);
            Class8.method836(-114);
            Class1009.method2105(false);
            Class1027.method2119(100);
            Class1046.method974(true);
            Class988.method1049(true);
            Class93.method1521(3101);
            Class1001.method1983(-3);
            Class966_2.method808(1);
            Class972.method1491((byte) -124);
            Class1034.method860(126);
            Class989.method1644((byte) 121);
            Class946.method1982((byte) 121);
            Class975.method1217(0);
            Class957.nullLoader();
            Class1017_2.method1774(103);
            Class961.method2081(0);
            Class0.method387(103);
            Class974.method1802(25);
            Class3_Sub24_Sub4.method491((byte) 85);
            Class155.method2165(0);
            Class157.method2175();
            Class1098.method1168(8160);
            Class954.method2057((byte) -108);
            Class991.method1974((byte) -116);
            Class949.method102(3353893);
            ObjectDefinition.method1687(-11);
            Class990.method109();
            Class981.method1473((byte) 103);
            Class1207.method821(26971);
            Class971.method91((byte) 120);
            Class1042.method83();
            Class1245.method875((byte) 106);
            Class1039.method1231(119);
            Class95.method1582(3);
            Class955.method558(-29679);
            Class145.method2071((byte) 59);
            Class1004.method1085(-1);
            Class132.method1800((byte) 104);
            Class14.method886((byte) 35);
            Class119.method1728(-14256);
            Class1126.method1754(-79);
            Class17.method905(-24912);
            Class128.method1761((byte) -55);
            Class1244.method2288(false);
            Class20.method908();
            Class167.method2262((byte) 126);
            Class1028.method592((byte) 38);
            Class932.method597((byte) 108);
            Class99.method1598(-126);
            Class84.method1422((byte) 24);
            Class953.nullLoader();
            Class40.method1042(true);
            Model.method1990();
            Class136.method1815((byte) -45);
            Class948.method1915();
            ItemDefinition.method1111(3327);
            Class1049.method1948();
            Class1005.method2085(118);
            Class980.method2088(true);
            Class23.method937(0);
            Class83.method1414();
            Class31.method987();
            Class15.method892(100);
            Class969.method53(0);
            Class162.method2205(-17413);
            Class1019.method689();
            Class85.method1426(-25247);
            Class3_Sub22.method399();
            Class78.method1369();
            Class3_Sub24_Sub3.method463(-28918);
            Class3_Sub15.method372(true);
            Class38_Sub1.method1032(false);
            Class33.method999();
            Class977.method1274();
            Class983.method1059();
            Class151.method2093(1);
            Class62.method1223(0);
            Class3_Sub28_Sub10.method588();
            Class143.method2063(0);
            Class1023.method1333();
            Class979.method828(-90);
            Class49.method1130(99);
            Class3_Sub28_Sub9.method584(0);
            Class1002.method521(-3);
            Class47.method1096((byte) 89);
            Class1223.method2276(-2);
            Class139.method1858(-17124);
            Class10117.method1606();
            Class1217.method943(-9893);
            Class3_Sub28_Sub11.method605(221301966);
            Class117.method1721();
            Class115.method1712();
            Class922.method896(true);
            Class1211.method964(6);
            Class1220.method1388(true);
            Class1221.method973((byte) 62);
            Class1143.method613();
            Class3_Sub28_Sub4.method547(-2951);
            Class7.method833((byte) 126);
            Class3_Sub28_Sub1.method528(-1667);
            Class57.method1192();
            Class973.method1181((byte) -118);
            Class2.method80();
            Class945.method1612(-11565);
            Class934.method721(20413);
            Class3_Sub28_Sub18.method711(1);
            Rasterizer.nullLoader();
            Class1017.method1016((byte) 127);
            Class1.method71((byte) -124);
            Class101.method1608((byte) 110);
            Class53.method1169(false);
            Class1228.method1077(0);
            Class26.method960(31);
            Class88.method1457();
            Class1045.method1178((byte) -93);
            Class10.method853(0);
            Class1048.method376(false);
            Class1031.method1860(0);
            Class930.method1429((byte) 53);
            Class3_Sub11.method147();
            Class25.method954(128);
            Class113.method1703();
            Class70.method1284((byte) -87);
            Class19.method906((byte) 112);
            Class12.method871((byte) 115);
            Class72.method1296(1);
            Class1015.method1751();
            Class997.method1011();
            Class1227.method380(-29113);
            Class56.method1187(30351);
            Class58.method1193(-26723);
            Class965.method1595();
            Class123.method1743(false);
            Class3_Sub28_Sub21.method726();
            Class121.method1733(-17148);
            Class141.method2045();
            Class169.method2283();
            Class77.method1365(119);
            Class110.method1682(-82);
            Class419.method120(1000);
            Class1042_3.f((int) 3);
            Class1042_4.method510(-128);
            Class3_Sub9.method136(-3);
            Class951.method632(-30497);
            Class116.method1715();
            Class161.method2202(-196);
            Class81.method1402();
            Class920.method923();
            Class45.method1081((byte) 81);
            Class140_Sub6.method2019(true);
            Class1212.method1954(24);
            Class107.method1646();
            Class1013.method2030((byte) 83);
            Class3_Sub5.method113((byte) -120);
            Class140_Sub3.method1958(2);
            Class124.method1744(true);
            Class80.method1394();
            Class1030.method735();
            Class134.method1806(3846);
            Class1042_2.method382();
            Class3_Sub21.method396(0);
            Class970.method59();
            Class108.method1660(13123);
            Class1210.method1771(14635);
            Class999.method2187(27316);
            Class996.method1731(12881);
            Class1209.method1133((byte) 81);
            Class1043.method1283((byte) 122);
            Class144.method2070((byte) 67);
            Class105.method1641();
            Class9.method849(2);
            Class956.method563(3);
            Class1225.method570(-119);
            Class118.method1726();
            Class3_Sub6.method118(2);
            Class166.method2255((byte) -128);
            Class155_Sub1.method2166();
            Class103.method1623();
            Class21.method911(26);
            Class154.method2145((byte) -69);
            Class125.method1748();
            Class112.method1700();
            Class104.method1630((byte) -113);
            Class65.method1238(-112);
            Class3_Sub14.method361();
            Class59.method1204();
            Class3_Sub13_Sub4.method187(false);
            Class159.method2197(true);
            smallClass1019 = null;
            boldClass1019 = null;
            regularClass1019 = null;
            CanvasBuffer.method156(2);
            Class164.method2235(4);
            Class97.method1592((byte) 102);
            Class114.method1704(65536);
            Class3_Sub10.method143();
            SpriteDefinition.method1409(false);
            Class1134.method1071((byte) -115);
            Class164_Sub2.method2245((byte) -74);
            Class164_Sub1.method2240(128);
            Class27.method962((byte) -67);
            Class1222.method131(-109);
            Class32.method994('\u93bd');
            Class60.method1206((byte) 26);
            // int var2 = 87 % ((68 - var1) / 49);
            Class120_Sub30_Sub1.method274(-2);
            Class3_Sub13_Sub11.method217(1);
            Class3_Sub13_Sub31.method317(7759444);
            Class3_Sub13_Sub29.method309(true);
            Class3_Sub13_Sub19.method261(-125);
            Class3_Sub13_Sub24.method288((byte) 110);
            Class3_Sub13_Sub2.method172(11597);
            Class3_Sub13_Sub27.method296((byte) -107);
            Class3_Sub13_Sub39.method357(false);
            Class3_Sub13_Sub37.method348(48);
            Class3_Sub13_Sub20.method266(-1443422260);
            Class3_Sub13_Sub1.method168(-1771542303);
            Class3_Sub13_Sub30.method315(-15028);
            Class3_Sub13_Sub32.method321(-21136);
            Class3_Sub13_Sub16.method245(0);
            Class3_Sub13_Sub9.method209((byte) 79);
            Class3_Sub13_Sub15.method241((byte) 74);
            Class3_Sub13_Sub23_Sub1.method287(false);
            Class3_Sub13_Sub23.method277((byte) -41);
            Class933.method258(-97);
            Class3_Sub13_Sub13.method234();
            Class3_Sub13_Sub35.method337(2);
            Class3_Sub13_Sub17.method249();
            Class3_Sub13_Sub12.resetVariables(true);
            Class3_Sub13_Sub34.method333((byte) -54);
            Class3_Sub13_Sub6.method197(1);
            Class3_Sub13_Sub7.method200((byte) 122);
            Class3_Sub13_Sub25.method290(-9);
            Class3_Sub13_Sub33.method325(0);
            Class3_Sub13_Sub10.method211(1024);
            Class3_Sub13_Sub14.method238(9423);
            Class3_Sub13_Sub28.method300();
            Class3_Sub13_Sub3.method177((byte) 119);
            Class3_Sub13_Sub26.method294((byte) 30);
            Class3_Sub13_Sub36.method341((byte) 85);
            Class3_Sub13_Sub21.method268((byte) -91);
            Class3_Sub13_Sub38.method351(-1);
            Class163_Sub2.method2218((byte) -83);
            Class163.method2208();
            Class163_Sub2_Sub1.method2225((byte) -120);
            Class163_Sub3.method2227((byte) 37);
            Class163_Sub1_Sub1.method2213((byte) 104);
            Class163_Sub1.method2212(false);
            Class921.method627((byte) -122);
            Class3_Sub28_Sub19.method717(109);
            Class3_Sub28_Sub2.method534(99);
            Class3_Sub23.method405(true);
            Class1016.method61();
            Class986.method67(true);
            Class3_Sub28_Sub8.method573(-11346);
            Class3_Sub20.method391();
            Class3_Sub26.method511((byte) 121);
            Class75.method1334((byte) -115);
            Class75_Sub1.method1343(false);
            Class75_Sub3.method1348((byte) 100);
            Class75_Sub4.method1350((byte) 75);
            Class75_Sub2.method1345(-71);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "client.C(" + var1 + ')');
        }

        //if (Class1021.anInt12 != 0) {
        //aBoolean2201 = true;
        //}

    }

    /*
     * private final void method46(boolean var1, int var2) { try {
     * ++Class58.class1006.failAttemps; Class17.js5ConnectionNode = null;
     * if (!var1) { aClass3_Sub11ArrayArray2199 = (Class3_Sub11[][])
     * ((Class3_Sub11[][]) null); }
     *
     * Class58.class1006.anInt1010 = var2; Class1016.js5Connection =
     * null; Class979.js5Stage = 0; } catch (RuntimeException var4) { throw
     * Class1134.method1067(var4, "client.P(" + var1 + ',' + var2 + ')'); } }
     */

    private final void method47(byte var1) {
        try {
            for (Class3_Sub23.anInt2537 = 0; Class1028.method591(83)
                    && ~Class3_Sub23.anInt2537 > -129; ++Class3_Sub23.anInt2537) {
                Class974.anIntArray1755[Class3_Sub23.anInt2537] = Class3_Sub28_Sub9.anInt3624;
                Class996.anIntArray1638[Class3_Sub23.anInt2537] = Class3_Sub13_Sub27.anInt3342;
            }

            ++Class989.anInt1446;
            if (-1 != Class1143.mainScreenInterface) {
                Class1211.method967(0, 0, 0, Class23.canvasWid,
                        Class1143.mainScreenInterface, 0, Class1013.canvasHei);
            }

            ++Class3_Sub13_Sub17.anInt3213;
            if (Class1012.aBoolean_617) {
                int var2 = 19137023;

                label191:
                for (int var3 = 0; ~var3 > -32769; ++var3) {
                    Class1001 var4 = Class3_Sub13_Sub24.class1001List[var3];
                    if (null != var4) {
                        byte var5 = var4.aClass90_3976.aByte1267;
                        if ((var5 & 2) > 0
                                && -1 == ~var4.walkQueueLocationIndex
                                && 10.0D > Math.random() * 1000.0D) {
                            int var6 = (int) Math.round(-1.0D + 2.0D
                                    * Math.random());
                            int var7 = (int) Math
                                    .round(Math.random() * 2.0D - 1.0D);
                            if (var6 != 0 || 0 != var7) {
                                var4.aByteArray2795[0] = 1;
                                var4.anIntArray2767[0] = var6
                                        + (var4.y >> -1913236345);
                                var4.anIntArray2755[0] = var7
                                        + (var4.x >> -173151257);
                                Class930.class972[Class26.plane].method1502(
                                        var1 + 20850,
                                        var4.y >> -649292601,
                                        var4.getSize(), false, 0,
                                        var4.getSize(),
                                        var4.x >> 1442151015);
                                if (0 <= var4.anIntArray2767[0]
                                        && var4.anIntArray2767[0] <= 104 + -var4
                                        .getSize()
                                        && 0 <= var4.anIntArray2755[0]
                                        && var4.anIntArray2755[0] <= 104 - var4
                                        .getSize()
                                        && Class930.class972[Class26.plane]
                                        .method1500(
                                                -2,
                                                var4.x >> 2135388679,
                                                var4.anIntArray2755[0],
                                                var4.anIntArray2767[0],
                                                var4.y >> 627928135)) {
                                    if (var4.getSize() > 1) {
                                        for (int var8 = var4.anIntArray2767[0]; ~(var4.anIntArray2767[0] - -var4
                                                .getSize()) < ~var8; ++var8) {
                                            for (int var9 = var4.anIntArray2755[0]; var4.anIntArray2755[0]
                                                    + var4.getSize() > var9; ++var9) {
                                                if ((var2 & Class930.class972[Class26.plane].aBoolean_4222[var8][var9]) != 0) {
                                                    continue label191;
                                                }
                                            }
                                        }
                                    }

                                    var4.walkQueueLocationIndex = 1;
                                }
                            }
                        }

                        Class973.method1180((byte) -122, var4);
                        Class17.method904(65536, var4);
                        Class922.method900(var4, var1 ^ -11974);
                        Class930.class972[Class26.plane].method1489(
                                var4.y >> -375465785, false, (byte) 85,
                                var4.x >> 1678486439, var4.getSize(),
                                var4.getSize());
                    }
                }
            }

            //if (var1 != 1) {
            //	aClass94_2196 = (Class1008) null;
            //}

            if (!Class1012.aBoolean_617) {
                ByteBuffer.method744(true);
            } else if (0 == Class3_Sub13_Sub25.loginStage
                    && 0 == Class969.anInt23) {
                // if(Class974.anInt1753 != 2) {
                // InterfaceChangeNode.d();
                // } else {
                Class1048.method379(var1 ^ 1025);
                // }

                if (14 > Class1001.renderX >> -1377844697
                        || Class1001.renderX >> 2015386375 >= 90
                        || 14 > Class77.renderY >> -944239097
                        || -91 >= ~(Class77.renderY >> -1325288249)) {
                    Class3_Sub13_Sub6.method195();
                }
            }

            while (true) {
                Class1048 var11 = (Class1048) Class979.aClass61_82
                        .popFront();
                Class1034 var12;
                Class1034 var13;
                if (var11 == null) {
                    while (true) {
                        var11 = (Class1048) Class65.aClass61_983
                                .popFront();
                        if (null == var11) {
                            while (true) {
                                var11 = (Class1048) Class110.aClass61_1471
                                        .popFront();
                                if (null == var11) {
                                    if (Class56.aClass11_886 != null) {
                                        Class979.method829(-1);
                                    }

                                    if (null != Class15.aClass64_351
                                            && Class15.aClass64_351.status == 1) {
                                        // if(null !=
                                        // Class15.aClass64_351.value) {
                                        // Class99.method1596(Class3_Sub13_Sub24.aClass94_3295,
                                        // Class1008.aBoolean2154);
                                        // }

                                        Class1008.aBoolean2154 = false;
                                        Class3_Sub13_Sub24.aClass94_3295 = null;
                                        Class15.aClass64_351 = null;
                                    }

                                    // if(Class1134.loopCycle % 1500 == 0) {
                                    // Class72.method1293();
                                    // }

                                    return;
                                }

                                var12 = var11.aClass11_2449;
                                if (0 <= var12.anInt191) {
                                    var13 = Class7.getInterface(var12.parent);
                                    if (var13 == null
                                            || null == var13.aClass11Array262
                                            || ~var13.aClass11Array262.length >= ~var12.anInt191
                                            || var12 != var13.aClass11Array262[var12.anInt191]) {
                                        continue;
                                    }
                                }

                                Class983.method1065(var11);
                            }
                        }

                        var12 = var11.aClass11_2449;
                        if (~var12.anInt191 <= -1) {
                            var13 = Class7.getInterface(var12.parent);
                            if (null == var13
                                    || var13.aClass11Array262 == null
                                    || ~var12.anInt191 <= ~var13.aClass11Array262.length
                                    || var12 != var13.aClass11Array262[var12.anInt191]) {
                                continue;
                            }
                        }

                        Class983.method1065(var11);
                    }
                }

                var12 = var11.aClass11_2449;
                if (var12.anInt191 >= 0) {
                    var13 = Class7.getInterface(var12.parent);
                    if (null == var13
                            || null == var13.aClass11Array262
                            || ~var13.aClass11Array262.length >= ~var12.anInt191
                            || var12 != var13.aClass11Array262[var12.anInt191]) {
                        continue;
                    }
                }

                Class983.method1065(var11);
            }
        } catch (RuntimeException var10) {
            throw Class1134.method1067(var10, "client.I(" + var1 + ')');
        }
    }

    private final void handleUpdateServer() {
        if (1000 != Class922.aInteger_544) {
            boolean var2 = Class1006.processUpdateServer();

            if (!var2) {
                js5Connect(-31379);
            }
        }
    }

    private final void js5Connect(int var1) {
        if (Class1006.failAttemps >= 4) {
            if (Class922.aInteger_544 <= 5) {
                error("error_io");
                Class922.aInteger_544 = 1000;
                return;
            }
            Class3_Sub13_Sub5.retryDelay = 3000;
            Class1006.failAttemps = 3;
        }
        if (Class3_Sub13_Sub5.retryDelay-- <= 0) {
            try {
                if (Class979.js5Stage == 0) {
                    Class17.js5ConnectionNode = Class38.gameClass942
                            .startConnection(Class38_Sub1.aString2611,
                                    Class140_Sub6.anInt2894);
                    //Update server - + 1 for 43595
                    Class979.js5Stage++;
                }
                if (Class979.js5Stage == 1) {
                    if (Class17.js5ConnectionNode.status == 2) {
                        js5Error(-1);
                        return;
                    }
                    if (Class17.js5ConnectionNode.status == 1)
                        Class979.js5Stage++;
                }
                if (Class979.js5Stage == 2) {
                    Class1016.js5Connection = new Class1007(
                            (Socket) Class17.js5ConnectionNode.value,
                            Class38.gameClass942);
                    ByteBuffer class966 = new ByteBuffer(5);
                    class966.aBlowMe(15);
                    class966.method_211(464);
                    Class1016.js5Connection.write(class966.buffer, 0, 5);
                    Class979.js5Stage++;
                    Class3_Sub13_Sub30.lastJs5ConnectionTime = Class1219
                            .currentTimeMillis();
                }
                if (Class979.js5Stage == 3) {
                    if (Class922.aInteger_544 == 0 || Class922.aInteger_544 == 5
                            || Class1016.js5Connection.available() > 0) {
                        int responce = Class1016.js5Connection.read();
                        if (responce != 0) {
                            js5Error(responce);
                            return;
                        }
                        Class979.js5Stage++;
                    } else if ((Class1219.currentTimeMillis() - Class3_Sub13_Sub30.lastJs5ConnectionTime) > 30000L) {
                        js5Error(-2);
                        return;
                    }
                }
                if (Class979.js5Stage == 4) {
                    boolean var6 = ~Class922.aInteger_544 == -6
                            || -11 == ~Class922.aInteger_544
                            || Class922.aInteger_544 == 28;
                    Class1006.method1249(var6,
                            Class1016.js5Connection);
                    Class1016.js5Connection = null;
                    Class17.js5ConnectionNode = null;
                    Class979.js5Stage = 0;
                    Class922.aInteger_546 = 0;
                }
            } catch (IOException ioexception) {
                js5Error(-3);
            }
        }
        /*
         * if(~Class163_Sub2_Sub1.anInt4026 >
         * ~Class58.class1006.failAttemps) { Class3_Sub13_Sub5.retryDelay =
         * 5 * 50 * (Class58.class1006.failAttemps + -1);
         * if(Class162.anInt2036 != Class140_Sub6.anInt2894) {
         * Class140_Sub6.anInt2894 = Class162.anInt2036; } else {
         * Class140_Sub6.anInt2894 = Class26.anInt506; }
         *
         * if(Class3_Sub13_Sub5.retryDelay > 3000) {
         * Class3_Sub13_Sub5.retryDelay = 3000; }
         *
         * if(~Class58.class1006.failAttemps <= -3 &&
         * Class58.class1006.anInt1010 == 6) {
         * error("js5connect_outofdate"); Class143.aInteger_544 = 1000; return; }
         *
         * if(Class58.class1006.failAttemps >= 4 &&
         * ~Class58.class1006.anInt1010 == 0) { error("js5crc");
         * Class143.aInteger_544 = 1000; return; }
         *
         * if(Class58.class1006.failAttemps >= 4 && (Class143.aInteger_544 ==
         * 0 || -6 == ~Class143.aInteger_544)) {
         * if(~Class58.class1006.anInt1010 != -8 && -10 !=
         * ~Class58.class1006.anInt1010) {
         * if(Class58.class1006.anInt1010 > 0) { error("js5connect"); }
         * else { error("js5io"); } } else { error("js5connect_full"); }
         *
         * Class143.aInteger_544 = 1000; return; } }
         *
         * Class163_Sub2_Sub1.anInt4026 = Class58.class1006.failAttemps;
         * if(~Class3_Sub13_Sub5.retryDelay < -1) {
         * --Class3_Sub13_Sub5.retryDelay; } else { try {
         * if(~Class979.js5Stage == -1) { Class17.js5ConnectionNode =
         * Class38.gameClass942.startConnection(Class38_Sub1.aString2611,
         * Class140_Sub6.anInt2894); ++Class979.js5Stage; }
         *
         * if(Class979.js5Stage == 1) { if(2 ==
         * Class17.js5ConnectionNode.status) { method46(true, 1000); return; }
         *
         * if(~Class17.js5ConnectionNode.status == -2) {
         * ++Class979.js5Stage; } }
         *
         * if(2 == Class979.js5Stage) { Class1016.js5Connection = new
         * Class1007((Socket)Class17.js5ConnectionNode.value,
         * Class38.gameClass942); ByteBuffer var2 = new ByteBuffer(5); var2.aBlowMe(15);
         * var2.method_211(464); Class1016.js5Connection.write(var2.buffer, 0,
         * 5); ++Class979.js5Stage; Class3_Sub13_Sub30.lastJs5ConnectionTime
         * = Class1219.currentTimeMillis(); }
         *
         * if(3 == Class979.js5Stage) { if(-1 != ~Class143.aInteger_544 &&
         * ~Class143.aInteger_544 != -6 && 0 >=
         * Class1016.js5Connection.available()) {
         * if(~(Class1219.currentTimeMillis() +
         * -Class3_Sub13_Sub30.lastJs5ConnectionTime) < -30001L) {
         * method46(true, 1001); return; } } else { int var5 =
         * Class1016.js5Connection.read(); if(-1 != ~var5) {
         * method46(true, var5); return; }
         *
         * ++Class979.js5Stage; } }
         *
         * if(-5 == ~Class979.js5Stage) { boolean var6 = ~Class143.aInteger_544
         * == -6 || -11 == ~Class143.aInteger_544 || Class143.aInteger_544 == 28;
         * Class58.class1006.method1249(!var6,
         * Class1016.js5Connection); Class1016.js5Connection =
         * null; Class17.js5ConnectionNode = null; Class979.js5Stage = 0; }
         * } catch (IOException var3) { method46(true, 1002); }
         *
         * }
         */
    }

    private final void js5Error(int responce) {
        Class979.js5Stage = 0;
        Class17.js5ConnectionNode = null;
        Class1016.js5Connection = null;
        Class922.aInteger_546++;
        if (Class922.aInteger_546 < 2 || responce != 7 && responce != 9) {
            if (Class922.aInteger_546 >= 2 && responce == 6) {
                error("server_out_of_date");
                Class922.aInteger_544 = 1000;
            } else if (Class922.aInteger_546 >= 4) {
                if (Class922.aInteger_544 > 5)
                    Class3_Sub13_Sub5.retryDelay = 3000;
                else {
                    error("server_offline");
                    Class922.aInteger_544 = 1000;
                }
            }
        } else if (Class922.aInteger_544 > 5)
            Class3_Sub13_Sub5.retryDelay = 3000;
        else {
            error("server_full");
            Class922.aInteger_544 = 1000;
        }
    }

    /*
     * public static final void main(String[] var0) { try { try { if(4 !=
     * var0.length) { Class3_Sub13_Sub23_Sub1.method283("argument method_713",
     * (byte)38); }
     *
     * int var1 = -1; Class984.worldId = Integer.parseInt(var0[0]);
     * Class1134.modeWhere = 2; if(!var0[1].equals("aBoolean_605")) {
     * if(var0[1].equals("rc")) { Class3_Sub13_Sub13.modeWhat = 1; } else
     * if(!var0[1].equals("wip")) {
     * Class3_Sub13_Sub23_Sub1.method283("modewhat", (byte)38); } else {
     * Class3_Sub13_Sub13.modeWhat = 2; } } else { Class3_Sub13_Sub13.modeWhat =
     * 0; }
     *
     * Class3_Sub28_Sub19.advertSuppressed = false;
     *
     * try { byte[] var2 = var0[2].getOutOurCode("ISO-8859-1"); var1 =
     * Class3_Sub13_Sub16.method243(Class3_Sub13_Sub3.bufferToString(var2, 0,
     * var2.length), (byte)13); } catch (Exception var3) { ; }
     *
     * if(-1 == var1) { if(!var0[2].equals("english")) {
     * if(var0[2].equals("german")) { Class3_Sub20.language = 1; } else {
     * Class3_Sub13_Sub23_Sub1.method283("language", (byte)38); } } else {
     * Class3_Sub20.language = 0; } } else { Class3_Sub20.language = var1; }
     *
     * Node.method87(Class3_Sub20.language); Class163_Sub2_Sub1.objectTag =
     * false; Class3_Sub28_Sub11.aBoolean3641 = false;
     *
     * Class1207.anInt2607 = 0; Class106.aBoolean1451 = false;
     * Class3_Sub26.anInt2554 = 0; Class163_Sub2.aClass94_2996 =
     * Class3_Sub28_Sub14.aClass94_3672; client var6 = new client();
     * Class1131.aClass9221671 = var6; var6.method40(32 -
     * -Class3_Sub13_Sub13.modeWhat, 464, false, 1024, "runescape", 768,
     * client.aInteger_512); Class3_Sub13_Sub7.aFrame3092.setLocation(40, 40); }
     * catch (Exception var4) { Class49.method1125((String)null, var4); }
     *
     * } catch (RuntimeException var5) { throw Class1134.method1067(var5,
     * "client.main(" + (var0 != null?"{...}":"null") + ')'); } }
     */

    public static final void drawInterface(Class1034[] inter, int uid, int var0,
                                           int var1, int width, int height, int x, int y) {

        if (aInteger_544 == GameState.LOGGED_IN && clientSize == 0) {
            if (Class929.aInteger_510 == 474) {
                Class7.getInterface(Class922.XP_COUNTER).y = Class7.getInterface(Class922.XP_COUNTER).originalY - 37;
            } else {
                Class7.getInterface(Class922.XP_COUNTER).y = Class7.getInterface(Class922.XP_COUNTER).originalY;
            }

            /** OSRS XP counter modifications @author Technotik **/
            Class7.getInterface(35913832).y = -1200;
            Class7.getInterface(35913832).x = +48;
            Class7.getInterface(35913831).y = -4;
            Class7.getInterface(35913831).x = +22;
            Class7.getInterface(35913833).y = -60;

        }

        if (aInteger_544 == GameState.LOGGED_IN && clientSize != 0) {
            Class7.getInterface(35913832).y = -1200;
        }

        for (int var8 = 0; var8 < inter.length; ++var8) {
            Class1034 var9 = inter[var8];
            if (var9 != null
                    && var9.parent == uid
                    && (!var9.scriptedInterface
                    || var9.type == 0
                    || var9.aBoolean195
                    || Class1034.getInterfaceClickMask(var9).clickMask != 0
                    || var9 == Class979.aClass11_88 || var9.clientCode == 1338)
                    && (!var9.scriptedInterface || !Class1034
                    .isComponentHidden(var9))) {
                int var10 = var9.anInt306 + x;
                int var11 = var9.newScrollerPos + y;
                int var12;
                int var13;
                int var14;
                int var15;
                if (var9.type == 2) {
                    var12 = var0;
                    var13 = var1;
                    var14 = width;
                    var15 = height;
                } else {

                    int var16 = var10 + var9.scrollbarWidth;
                    int var17 = var11 + var9.scrollbarHeight;
                    // System.out.println("var16: "+ var16 + " var17: "+ var17);
                    if (uid == 40697856) {
                        var16 = width;
                        var17 = height;
                    }
                    if (var9.type == 9) {
                        ++var16;
                        ++var17;
                    }

                    var12 = var10 > var0 ? var10 : var0;
                    var13 = var11 > var1 ? var11 : var1;
                    var14 = var16 < width ? var16 : width;
                    var15 = var17 < height ? var17 : height;
                }

                if (var9 == Class56.aClass11_886) {
                    Class21.aBoolean440 = true;
                    Class3_Sub15.anInt2421 = var10;
                    Class949.anInt2218 = var11;
                }

                if (!var9.scriptedInterface || var12 < var14 && var13 < var15) {
                    if (var9.type == 0) {
                        if (!var9.scriptedInterface
                                && Class1034.isComponentHidden(var9)
                                && Class107.aClass11_1453 != var9) {
                            continue;
                        }

                        if (var9.aBoolean219 && Class1015.mouseX2 >= var12
                                && Class1017_2.anInt1709 >= var13
                                && Class1015.mouseX2 < var14
                                && Class1017_2.anInt1709 < var15) {
                            for (Class1048 var27 = (Class1048) Class110.aClass61_1471
                                    .getFirst(); var27 != null; var27 = (Class1048) Class110.aClass61_1471
                                    .getNext()) {
                                if (var27.aBoolean2446) {
                                    var27.unlink();
                                    var27.aClass11_2449.aBoolean163 = false;
                                }
                            }

                            if (Class75_Sub3.anInt2658 == 0) {
                                Class56.aClass11_886 = null;
                                Class979.aClass11_88 = null;
                            }

                            // ClanMember.anInt2475 = 0;
                        }
                    }

                    if (var9.scriptedInterface) {
                        boolean var26;
                        if (Class1015.mouseX2 >= var12
                                && Class1017_2.anInt1709 >= var13
                                && Class1015.mouseX2 < var14
                                && Class1017_2.anInt1709 < var15) {
                            var26 = true;
                        } else {
                            var26 = false;
                        }

                        boolean var25 = false;
                        if (Class3_Sub13_Sub5.anInt3069 == 1 && var26) {
                            var25 = true;
                        }

                        boolean var18 = false;
                        if (Class3_Sub28_Sub11.anInt3644 == 1
                                && Class163_Sub1.clickX2 >= var12
                                && Class38_Sub1.clickY2 >= var13
                                && Class163_Sub1.clickX2 < var14
                                && Class38_Sub1.clickY2 < var15) {
                            var18 = true;
                        }

                        int var19;
                        int var21;
                        if (var9.aByteArray263 != null) {
                            for (var19 = 0; var19 < var9.aByteArray263.length; ++var19) {
                                if (!ObjectDefinition.isKeyDown[var9.aByteArray263[var19]]) {
                                    if (var9.anIntArray310 != null) {
                                        var9.anIntArray310[var19] = 0;
                                    }
                                } else if (var9.anIntArray310 == null
                                        || Class1134.loopCycle >= var9.anIntArray310[var19]) {
                                    byte var20 = var9.aByteArray231[var19];
                                    if (var20 == 0
                                            || ((var20 & 2) == 0 || ObjectDefinition.isKeyDown[86])
                                            && ((var20 & 1) == 0 || ObjectDefinition.isKeyDown[82])
                                            && ((var20 & 4) == 0 || ObjectDefinition.isKeyDown[81])) {
                                        Class986.method66(
                                                Class922.BLANK_CLASS_1008, -1,
                                                var19 + 1, var9.uid);
                                        var21 = var9.anIntArray299[var19];
                                        if (var9.anIntArray310 == null) {
                                            var9.anIntArray310 = new int[var9.aByteArray263.length];
                                        }

                                        if (var21 != 0) {
                                            var9.anIntArray310[var19] = Class1134.loopCycle
                                                    + var21;
                                        } else {
                                            var9.anIntArray310[var19] = Integer.MAX_VALUE;
                                        }
                                    }
                                }
                            }
                        }

                        if (var18) {
                            Class1042_3.a(Class38_Sub1.clickY2 - var11,
                                    Class163_Sub1.clickX2 - var10, 97, var9);
                        }

                        if (Class56.aClass11_886 != null
                                && Class56.aClass11_886 != var9
                                && var26
                                && Class1034.getInterfaceClickMask(var9)
                                .method98(false)) {
                            Class27.aClass11_526 = var9;
                        }

                        if (var9 == Class979.aClass11_88) {
                            Class85.aBoolean1167 = true;
                            Class3_Sub13_Sub13.anInt3156 = var10;
                            Class134.anInt1761 = var11;
                        }

                        if (var9.aBoolean195 || var9.clientCode != 0) {
                            Class1048 var30;
                            if (var26 && Class1221.anInt561 != 0
                                    && var9.anObjectArray183 != null) {
                                var30 = new Class1048();
                                var30.aBoolean2446 = true;
                                var30.aClass11_2449 = var9;
                                var30.anInt2441 = Class1221.anInt561;
                                var30.objectData = var9.anObjectArray183;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (Class56.aClass11_886 != null
                                    || Class67.aClass11_1017 != null
                                    || Class38_Sub1.drawContextMenu) {
                                var18 = false;
                                var25 = false;
                                var26 = false;
                            }

                            int var29;
                            if (var9.clientCode != 0) {
                                if (var9.clientCode == 1337) {
                                    // TODO:
                                    // Here is where we set fullscreen, after
                                    // this is visible(after play button)
                                    Class1223.aClass11_2091 = var9;
                                    Class20.refreshInterface(var9);
                                    continue;
                                }

                                if (var9.clientCode == 1338) {
                                    if (var18) {
                                        Class1.anInt56 = Class163_Sub1.clickX2
                                                - var10;
                                        Class58.anInt916 = Class38_Sub1.clickY2
                                                - var11;
                                    }
                                    continue;
                                }

                                /*
                                 * if(var9.clientCode == 1400) {
                                 * Class3_Sub28_Sub3.aClass11_3551 = var9;
                                 * if(var18) {
                                 * if(Class984.aBooleanArray1490[82] &&
                                 * Class3_Sub13_Sub26.anInt3320 >= 2) { var19 =
                                 * (int)((double)(Class163_Sub1.anInt2993 -
                                 * var10 - var9.scrollbarWidth / 2) * 2.0D /
                                 * (double)Class1134.aFloat727); var29 =
                                 * (int)((double)(Class38_Sub1.anInt2614 - var11
                                 * - var9.anInt193 / 2) * 2.0D /
                                 * (double)Class1134.aFloat727); var21 =
                                 * Class3_Sub28_Sub1.anInt3536 + var19; int
                                 * var32 = Class3_Sub4.anInt2251 + var29; int
                                 * var23 = var21 + Class3_Sub13_Sub21.anInt3256;
                                 * int var24 = Class108.anInt1460 - 1 - var32 +
                                 * Class2.anInt65; Class30.method979(var23,
                                 * var24, 0); Class3_Sub13_Sub19.method264();
                                 * continue; }
                                 *
                                 * ClanMember.anInt2475 = 1; Class144.anInt1881
                                 * = Class1131.anInt1676; Class95.anInt1336 =
                                 * Class1017_2.anInt1709; continue; }
                                 *
                                 * if(var25 && ClanMember.anInt2475 > 0) {
                                 * if(ClanMember.anInt2475 == 1 &&
                                 * (Class144.anInt1881 != Class1131.anInt1676
                                 * || Class95.anInt1336 != Class1017_2.anInt1709))
                                 * { Class932.anInt4073 =
                                 * Class3_Sub28_Sub1.anInt3536; Class38.anInt660
                                 * = Class3_Sub4.anInt2251; ClanMember.anInt2475
                                 * = 2; }
                                 *
                                 * if(ClanMember.anInt2475 == 2) {
                                 * SubScript.method1175
                                 * (Class932.anInt4073 +
                                 * (int)((double)(Class144.anInt1881 -
                                 * Class1131.anInt1676) * 2.0D /
                                 * (double)Class1001.aFloat3979));
                                 * Class3_Sub13_Sub39.method354(Class38.anInt660
                                 * + (int)((double)(Class95.anInt1336 -
                                 * Class1017_2.anInt1709) * 2.0D /
                                 * (double)Class1001.aFloat3979)); } continue; }
                                 *
                                 * ClanMember.anInt2475 = 0; continue; }
                                 */

                                /*
                                 * if(var9.clientCode == 1401) { if(var25) {
                                 * Class3_Sub13_Sub17.method253(var9.scrollbarWidth,
                                 * Class1017_2.anInt1709 - var11,
                                 * Class1131.anInt1676 - var10,
                                 * var9.anInt193); } continue; }
                                 */

                                if (var9.clientCode == 1402) {
                                    if (!Class1012.aBoolean_617) {
                                        Class20.refreshInterface(var9);
                                    }
                                    continue;
                                }
                            }

                            if (!var9.aBoolean188 && var18) {
                                var9.aBoolean188 = true;
                                if (var9.anObjectArray165 != null) {
                                    var30 = new Class1048();
                                    var30.aBoolean2446 = true;
                                    var30.aClass11_2449 = var9;
                                    var30.anInt2447 = Class163_Sub1.clickX2
                                            - var10;
                                    var30.anInt2441 = Class38_Sub1.clickY2
                                            - var11;
                                    var30.objectData = var9.anObjectArray165;
                                    Class110.aClass61_1471.insertBack(var30);
                                }
                            }

                            if (var9.aBoolean188 && var25
                                    && var9.anObjectArray170 != null) {
                                var30 = new Class1048();
                                var30.aBoolean2446 = true;
                                var30.aClass11_2449 = var9;
                                var30.anInt2447 = Class1015.mouseX2 - var10;
                                var30.anInt2441 = Class1017_2.anInt1709 - var11;
                                var30.objectData = var9.anObjectArray170;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (var9.aBoolean188 && !var25) {
                                var9.aBoolean188 = false;
                                if (var9.anObjectArray239 != null) {
                                    var30 = new Class1048();
                                    var30.aBoolean2446 = true;
                                    var30.aClass11_2449 = var9;
                                    var30.anInt2447 = Class1015.mouseX2
                                            - var10;
                                    var30.anInt2441 = Class1017_2.anInt1709
                                            - var11;
                                    var30.objectData = var9.anObjectArray239;
                                    Class65.aClass61_983.insertBack(var30);
                                }
                            }

                            if (var25 && var9.anObjectArray180 != null) {
                                var30 = new Class1048();
                                var30.aBoolean2446 = true;
                                var30.aClass11_2449 = var9;
                                var30.anInt2447 = Class1015.mouseX2 - var10;
                                var30.anInt2441 = Class1017_2.anInt1709 - var11;
                                var30.objectData = var9.anObjectArray180;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (!var9.aBoolean163 && var26) {
                                var9.aBoolean163 = true;
                                if (var9.anObjectArray248 != null) {
                                    var30 = new Class1048();
                                    var30.aBoolean2446 = true;
                                    var30.aClass11_2449 = var9;
                                    var30.anInt2447 = Class1015.mouseX2
                                            - var10;
                                    var30.anInt2441 = Class1017_2.anInt1709
                                            - var11;
                                    var30.objectData = var9.anObjectArray248;
                                    Class110.aClass61_1471.insertBack(var30);
                                }
                            }

                            if (var9.aBoolean163 && var26
                                    && var9.anObjectArray276 != null) {
                                var30 = new Class1048();
                                var30.aBoolean2446 = true;
                                var30.aClass11_2449 = var9;
                                var30.anInt2447 = Class1015.mouseX2 - var10;
                                var30.anInt2441 = Class1017_2.anInt1709 - var11;
                                var30.objectData = var9.anObjectArray276;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (var9.aBoolean163 && !var26) {
                                var9.aBoolean163 = false;
                                if (var9.anObjectArray281 != null) {
                                    var30 = new Class1048();
                                    var30.aBoolean2446 = true;
                                    var30.aClass11_2449 = var9;
                                    var30.anInt2447 = Class1015.mouseX2
                                            - var10;
                                    var30.anInt2441 = Class1017_2.anInt1709
                                            - var11;
                                    var30.objectData = var9.anObjectArray281;
                                    Class65.aClass61_983.insertBack(var30);
                                }
                            }

                            if (var9.anObjectArray269 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray269;
                                Class979.aClass61_82.insertBack(var30);
                            }

                            Class1048 var22;
                            if (var9.anObjectArray161 != null
                                    && Class979.anInt87 > var9.anInt284) {
                                if (var9.anIntArray211 != null
                                        && Class979.anInt87 - var9.anInt284 <= 32) {
                                    label531:
                                    for (var19 = var9.anInt284; var19 < Class979.anInt87; ++var19) {
                                        var29 = Class1001.anIntArray3986[var19 & 31];

                                        for (var21 = 0; var21 < var9.anIntArray211.length; ++var21) {
                                            if (var9.anIntArray211[var21] == var29) {
                                                var22 = new Class1048();
                                                var22.aClass11_2449 = var9;
                                                var22.objectData = var9.anObjectArray161;
                                                Class110.aClass61_1471
                                                        .insertBack(var22);
                                                break label531;
                                            }
                                        }
                                    }
                                } else {
                                    var30 = new Class1048();
                                    var30.aClass11_2449 = var9;
                                    var30.objectData = var9.anObjectArray161;
                                    Class110.aClass61_1471.insertBack(var30);
                                }

                                var9.anInt284 = Class979.anInt87;
                            }

                            if (var9.anObjectArray221 != null
                                    && Class3_Sub9.anInt2317 > var9.anInt242) {
                                if (var9.anIntArray185 != null
                                        && Class3_Sub9.anInt2317
                                        - var9.anInt242 <= 32) {
                                    label512:
                                    for (var19 = var9.anInt242; var19 < Class3_Sub9.anInt2317; ++var19) {
                                        var29 = Class163_Sub2_Sub1.anIntArray4025[var19 & 31];

                                        for (var21 = 0; var21 < var9.anIntArray185.length; ++var21) {
                                            if (var9.anIntArray185[var21] == var29) {
                                                var22 = new Class1048();
                                                var22.aClass11_2449 = var9;
                                                var22.objectData = var9.anObjectArray221;
                                                Class110.aClass61_1471
                                                        .insertBack(var22);
                                                break label512;
                                            }
                                        }
                                    }
                                } else {
                                    var30 = new Class1048();
                                    var30.aClass11_2449 = var9;
                                    var30.objectData = var9.anObjectArray221;
                                    Class110.aClass61_1471.insertBack(var30);
                                }

                                var9.anInt242 = Class3_Sub9.anInt2317;
                            }

                            if (var9.anObjectArray282 != null
                                    && Class1017.anInt641 > var9.anInt213) {
                                if (var9.anIntArray286 != null
                                        && Class1017.anInt641 - var9.anInt213 <= 32) {
                                    label493:
                                    for (var19 = var9.anInt213; var19 < Class1017.anInt641; ++var19) {
                                        var29 = Class1134.anIntArray726[var19 & 31];

                                        for (var21 = 0; var21 < var9.anIntArray286.length; ++var21) {
                                            if (var9.anIntArray286[var21] == var29) {
                                                var22 = new Class1048();
                                                var22.aClass11_2449 = var9;
                                                var22.objectData = var9.anObjectArray282;
                                                Class110.aClass61_1471
                                                        .insertBack(var22);
                                                break label493;
                                            }
                                        }
                                    }
                                } else {
                                    var30 = new Class1048();
                                    var30.aClass11_2449 = var9;
                                    var30.objectData = var9.anObjectArray282;
                                    Class110.aClass61_1471.insertBack(var30);
                                }

                                var9.anInt213 = Class1017.anInt641;
                            }

                            if (var9.anObjectArray174 != null
                                    && Class62.anInt944 > var9.anInt255) {
                                if (var9.anIntArray175 != null
                                        && Class62.anInt944 - var9.anInt255 <= 32) {
                                    label474:
                                    for (var19 = var9.anInt255; var19 < Class62.anInt944; ++var19) {
                                        var29 = Class3_Sub28_Sub4.anIntArray3565[var19 & 31];

                                        for (var21 = 0; var21 < var9.anIntArray175.length; ++var21) {
                                            if (var9.anIntArray175[var21] == var29) {
                                                var22 = new Class1048();
                                                var22.aClass11_2449 = var9;
                                                var22.objectData = var9.anObjectArray174;
                                                Class110.aClass61_1471
                                                        .insertBack(var22);
                                                break label474;
                                            }
                                        }
                                    }
                                } else {
                                    var30 = new Class1048();
                                    var30.aClass11_2449 = var9;
                                    var30.objectData = var9.anObjectArray174;
                                    Class110.aClass61_1471.insertBack(var30);
                                }

                                var9.anInt255 = Class62.anInt944;
                            }

                            if (var9.anObjectArray158 != null
                                    && Class49.anInt815 > var9.anInt311) {
                                if (var9.anIntArray274 != null
                                        && Class49.anInt815 - var9.anInt311 <= 32) {
                                    label455:
                                    for (var19 = var9.anInt311; var19 < Class49.anInt815; ++var19) {
                                        var29 = Class3_Sub28_Sub19.anIntArray3780[var19 & 31];

                                        for (var21 = 0; var21 < var9.anIntArray274.length; ++var21) {
                                            if (var9.anIntArray274[var21] == var29) {
                                                var22 = new Class1048();
                                                var22.aClass11_2449 = var9;
                                                var22.objectData = var9.anObjectArray158;
                                                Class110.aClass61_1471
                                                        .insertBack(var22);
                                                break label455;
                                            }
                                        }
                                    }
                                } else {
                                    var30 = new Class1048();
                                    var30.aClass11_2449 = var9;
                                    var30.objectData = var9.anObjectArray158;
                                    Class110.aClass61_1471.insertBack(var30);
                                }

                                var9.anInt311 = Class49.anInt815;
                            }

                            if (Class1217.anInt472 > var9.anInt234
                                    && var9.anObjectArray256 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray256;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (Class110.anInt1472 > var9.anInt234
                                    && var9.anObjectArray156 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray156;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (Class167.anInt2087 > var9.anInt234
                                    && var9.anObjectArray313 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray313;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (Class121.anInt1642 > var9.anInt234
                                    && var9.anObjectArray268 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray268;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            if (Class140_Sub6.anInt2905 > var9.anInt234
                                    && var9.anObjectArray315 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray315;
                                Class110.aClass61_1471.insertBack(var30);
                            }

                            var9.anInt234 = Class3_Sub13_Sub17.anInt3213;
                            if (var9.keyPressedListener != null) {
                                for (var19 = 0; var19 < Class3_Sub23.anInt2537; ++var19) {
                                    Class1048 var31 = new Class1048();
                                    var31.aClass11_2449 = var9;
                                    var31.anInt2444 = Class974.anIntArray1755[var19];
                                    var31.anInt2443 = Class996.anIntArray1638[var19];
                                    var31.objectData = var9.keyPressedListener;
                                    Class110.aClass61_1471.insertBack(var31);
                                }
                            }

                            if (Class3_Sub28_Sub1.aBoolean3531
                                    && var9.anObjectArray217 != null) {
                                var30 = new Class1048();
                                var30.aClass11_2449 = var9;
                                var30.objectData = var9.anObjectArray217;
                                Class110.aClass61_1471.insertBack(var30);
                            }
                        }
                    }

                    if (!var9.scriptedInterface && Class56.aClass11_886 == null
                            && Class67.aClass11_1017 == null
                            && !Class38_Sub1.drawContextMenu) {
                        if ((var9.mouseOverId >= 0 || var9.disabledMouseOverColor != 0)
                                && Class1015.mouseX2 >= var12
                                && Class1017_2.anInt1709 >= var13
                                && Class1015.mouseX2 < var14
                                && Class1017_2.anInt1709 < var15) {
                            if (var9.mouseOverId >= 0) {
                                Class107.aClass11_1453 = inter[var9.mouseOverId];
                            } else {
                                Class107.aClass11_1453 = var9;
                            }
                        }

                        if (var9.type == 8 && Class1015.mouseX2 >= var12
                                && Class1017_2.anInt1709 >= var13
                                && Class1015.mouseX2 < var14
                                && Class1017_2.anInt1709 < var15) {
                            Class20.aClass11_439 = var9;
                        }

                        if (var9.maxScrollVertical > var9.scrollbarHeight) {
                            Class137.method1819(Class1017_2.anInt1709,
                                    var9.scrollbarHeight, var9, (byte) -101,
                                    Class1015.mouseX2, var10 + var9.scrollbarWidth,
                                    var11, var9.maxScrollVertical);
                        }
                    }

                    if (var9.type == 0) {
                        drawInterface(inter, var9.uid, var12, var13, var14,
                                var15, var10 - var9.anInt247, var11
                                        - var9.scrollPosition);
                        if (var9.aClass11Array262 != null) {
                            drawInterface(var9.aClass11Array262, var9.uid,
                                    var12, var13, var14, var15, var10
                                            - var9.anInt247, var11
                                            - var9.scrollPosition);
                        }

                        Class1207 var28 = (Class1207) Class3_Sub13_Sub17.aClass130_3208
                                .get((long) var9.uid);
                        if (var28 != null) {
                            Class1211.method967(var10, var13, var11, var14,
                                    var28.uid, var12, var15);
                        }
                    }
                }
            }
        }

    }

    public static void printAllGuaranteedProperties() {
        printAProperty("java.version", "Java version number");
        printAProperty("java.vendor", "Java vendor specific string");
        printAProperty("java.vendor.url", "Java vendor URL");
        printAProperty("java.home", "Java installation directory");
        printAProperty("java.class.version", "Java class version number");
        printAProperty("java.class.path", "Java classpath");
        printAProperty("os.integer_34", "Operating System Name");
        printAProperty("os.arch", "Operating System Architecture");
        printAProperty("os.version", "Operating System Version");
        printAProperty("file.separator", "File separator");
        printAProperty("path.separator", "Path separator");
        printAProperty("line.separator", "Line separator");
        printAProperty("user.integer_34", "User account integer_34");
        printAProperty("user.home", "User home directory");
        printAProperty("user.dir", "User's current working directory");
    }

    public static String indexLocation(int cacheIndex, int index) {
        return Class929.aString1086 + "/index" + cacheIndex + "/" + (index != -1 ? index + ".gz" : "");
    }

    public static void repackCacheIndex(int cacheIndex) {
        System.out.println("Started repacking index " + cacheIndex + ".");
        int indexLength = new File(indexLocation(cacheIndex, -1)).listFiles().length;
        File[] file = new File(indexLocation(cacheIndex, -1)).listFiles();
        try {
            for (int index = 0; index < indexLength; index++) {
                int fileIndex = Integer.parseInt(getFileNameWithoutExtension(file[index].toString()));
                byte[] data = fileToByteArray(cacheIndex, fileIndex);
                if (data != null && data.length > 0) {
                    //mapsArray[var2].method2129(data.length, fileIndex);
                    //[cacheIndex].method234(data.length, data, fileIndex);
                    //Class988.method1053(fileIndex, Class3_Sub13_Sub6.cacheIndex5 );
                    //Class988.method1050(0);
                    new Class988(index, Class101.cacheFile, Class163_Sub2.indexFiles[index], 1000000);
                    Class3_Sub13_Sub6.cacheIndex5 = Class8.method842(false, true, false,
                            5);
                    System.out.println("Repacked " + fileIndex + ".");
                } else {
                    System.out.println("Unable to locate index " + fileIndex + ".");
                }
            }
        } catch (Exception e) {
            System.out.println("Error packing cache index " + cacheIndex + ".");
        }
        System.out.println("Finished repacking " + cacheIndex + ".");
    }

    public static String getFileNameWithoutExtension(String fileName) {
        File tmpFile = new File(fileName);
        tmpFile.getName();
        int whereDot = tmpFile.getName().lastIndexOf('.');
        if (0 < whereDot && whereDot <= tmpFile.getName().length() - 2) {
            return tmpFile.getName().substring(0, whereDot);
        }
        return "";
    }

    public static byte[] fileToByteArray(int cacheIndex, int index) {
        try {
            if (indexLocation(cacheIndex, index).length() <= 0 || indexLocation(cacheIndex, index) == null) {
                return null;
            }
            File file = new File(indexLocation(cacheIndex, index));
            byte[] fileData = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(fileData);
            fis.close();
            return fileData;
        } catch (Exception e) {
            return null;
        }
    }

    public static void printAProperty(String propName, String desc) {
        System.out.println("Value for '" + desc + "' is '"
                + System.getProperty(propName) + "'.");
    }

    public final void load() {
        /*
         * if(!Class1008.safeMode) { while(Class3_Sub28_Sub10_Sub1.method591(107))
         * { if(~Class3_Sub13_Sub27.anInt3342 == -116 ||
         * ~Class3_Sub13_Sub27.anInt3342 == -84) { Class1008.safeMode = true; } }
         * }
         */
        // if (!closed_revision_choosing && Class143.aInteger_544 == 5) {
        // Class108.load_title_screen(Class3_Sub13_Sub25.cacheIndex10);
        // return;
        // }
        int var2;
        int var3;
        if (Class1025.loadingStage == 0) {
            Runtime var10 = Runtime.getRuntime();
            var3 = (int) ((var10.totalMemory() - var10.freeMemory()) / 1024L);
            long var4 = Class1219.currentTimeMillis();
            if (-1L == ~Class3_Sub13_Sub24.aLong3296) {
                Class3_Sub13_Sub24.aLong3296 = var4;
            }

            if (var3 > 16384 && 5000L > -Class3_Sub13_Sub24.aLong3296 + var4) {
                if (-Class972.aLong1310 + var4 > 1000L) {
                    System.gc();
                    Class972.aLong1310 = var4;
                }

                Class951.anInt3684 = 5;
                Class1227.loading_completion = Class3_Sub13_Sub23_Sub1.aClass94_4040;

            } else {
                Class1227.loading_completion = Class1008.aClass94_2151;
                Class1025.loadingStage = 10;
                Class951.anInt3684 = 5;
            }
        } else if (Class1025.loadingStage == 10) {
            Class977.method1267(4, 104, 104);

            for (var2 = 0; -5 < ~var2; ++var2) {
                Class930.class972[var2] = new Class972(104, 104);
            }
            Class951.anInt3684 = 10;
            Class1025.loadingStage = 30;
            Class1227.loading_completion = Class3_Sub28_Sub10.aClass94_3629;
            //Class929.clientLoad = (Integer)Class944.getSetting("client_load");
            Class922.hdOnLogin = (Boolean) Class944.getSetting("hd_enabled");
            Class922.startSize = (Integer) Class944.getSetting("start_size");
            Class3_Sub26.forceTweeningEnabled = (Boolean) Class944.getSetting("tweening_enabled");
            Class929.aInteger_511 = (Integer) Class944.getSetting("hitmarkers");
            Class1005.showGroundDecorations = !((Boolean) Class944.getSetting("lowmem"));
            Class25.lowMemoryTextures = !Class1005.showGroundDecorations;
            Class922.aBoolean_611 = !Class1005.showGroundDecorations;
            Class1228.highDetailLights = Class1005.showGroundDecorations;
            Class929.aInteger_510 = (Integer) Class944.getSetting("gameframe");
            Class929.newHealthbars = (Boolean) Class944.getSetting("new_health_bars");
            Class929.orbsToggled = (Boolean) Class944.getSetting("orbs_toggled");
            Class922.leftClickAttack = (Boolean) Class944.getSetting("left_click_attack");
            Class929.newHits = (Boolean) Class944.getSetting("new_hits");
            Class929.newMenus = (Boolean) Class944.getSetting("new_menus");
            Class929.hdMinimap = (Boolean) Class944.getSetting("hd_minimap");
            Class929.newCursors = (Boolean) Class944.getSetting("new_cursors");
            Class922.censorOn = (Boolean) Class944.getSetting("censor");
        } else if (Class1025.loadingStage == 30) {
            if (!Class929.aBoolean_606) {
                return;
            }
            Class75_Sub3.cacheIndex0 = Class8.method842(false, true, false,
                    getSkeletonsIDX()); //skeletons
            Class3_Sub28_Sub19.cacheIndex1 = Class8.method842(false, true,
                    false, getSkinsIDX()); //skins
            Class164.cacheIndex2 = Class8.method842(true, true, false, 2);
            Class140_Sub3.cacheIndex3 = Class8.method842(false, true, false, 3);
            Class961.cacheIndex4 = Class8.method842(false, true,
                    false, 4);
            Class3_Sub13_Sub6.cacheIndex5 = Class8.method842(false, true,
                    false, 5);
            Class75_Sub2.cacheIndex6 = Class8.method842(/* false */true, false, /* false */
                    true, 6); // music
            Class159.cacheIndex7 = Class8.method842(false, true, false,
                    getModelsIDX());
            Class140_Sub6.cacheIndex8 = Class8.method842(false, true, false, 8);
            Class3_Sub13_Sub28.cacheIndex9 = Class8.method842(false, true,
                    false, 9);
            Class3_Sub13_Sub25.cacheIndex10 = Class8.method842(false, true,
                    false, 10);
            Class1002.cacheIndex11 = Class8.method842(false, true, false, 11);
            Class971.cacheIndex12 = Class8.method842(false, true, false,
                    12);
            Class1027.cacheIndex13 = Class8.method842(false, true, false, 13);
            Class0.cacheIndex14 = Class8.method842(false, false, false, 14);
            Class1001.cacheIndex15 = Class8.method842(false, true, false, 15);
            /*
             * skeletons562 = Class8.method842(false, true, true, 16, true);
             * skins562 = Class8.method842(false, true, true, 17, true);
             * modelsCache562 = Class8.method842(false, true, true, 18, true);
             * skeletons530 = Class8.method842(false, true, true, 19, true);
             * skins530 = Class8.method842(false, true, true, 20, true);
             * modelsCache530 = Class8.method842(false, true, true, 21, true);
             */
            //if (Class929.clientLoad != 602) {
            skeletons602 = Class8.method842(false, true, false, 22);
            skins602 = Class8.method842(false, true, false, 23);
            modelsCache602 = Class8.method842(false, true, false, 24);
            //}
            // modelsCache666 = Class8.method842(false, true, true, 28, true);
            Class97.aClass153_1376 = Class8.method842(true, true, false, 25);// clientLoad
            // ==
            // 666
            // ?
            // 29
            // :
            // 25,
            // true);
            /*
             * Class168.aClass153_2097 = Class8.method842(false, true, true, 16,
             * true); Class1001.aClass153_3993 = Class8.method842(false, true, true,
             * 17, true); Class101.aClass153_1428 = Class8.method842(false,
             * true, true, 18, true); Class100.aClass153_1410 =
             * Class8.method842(false, true, true, 19, true);
             * Class3_Sub13_Sub36.aClass153_3429 = Class8.method842(false, true,
             * true, 20, true); Class70.aClass153_1058 = Class8.method842(false,
             * true, true, 21, true); Class3_Sub22.aClass153_2528 =
             * Class8.method842(false, true, true, 22, true);
             * Class133.aClass153_1751 = Class8.method842(true, true, true, 23,
             * true); Class140_Sub7.aClass153_2939 = Class8.method842(false,
             * true, true, 24, true); Class3_Sub4.aClass153_2258 =
             * Class8.method842(false, true, true, 25, true);
             * Class132.aClass153_1735 = Class8.method842(false, true, true, 27,
             * true);
             */
            Class951.anInt3684 = 15;
            Class1227.loading_completion = Class157.aClass94_1995;
            Class1025.loadingStage = 40;
        } else if (Class1025.loadingStage == 40) {
            int percent = 0;

            percent += Class75_Sub3.cacheIndex0.getReferenceTableCompletion() * 4 / 100;
            percent += Class3_Sub28_Sub19.cacheIndex1
                    .getReferenceTableCompletion() * 4 / 100;
            percent += Class164.cacheIndex2.getReferenceTableCompletion() * 2 / 100;
            percent += Class140_Sub3.cacheIndex3.getReferenceTableCompletion() * 2 / 100;
            percent += Class961.cacheIndex4
                    .getReferenceTableCompletion() * 6 / 100;
            percent += Class3_Sub13_Sub6.cacheIndex5
                    .getReferenceTableCompletion() * 4 / 100;
            percent += Class75_Sub2.cacheIndex6.getReferenceTableCompletion() * 2 / 100;
            percent += Class159.cacheIndex7.getReferenceTableCompletion() * 60 / 100;
            percent += Class140_Sub6.cacheIndex8.getReferenceTableCompletion() * 2 / 100;
            percent += Class3_Sub13_Sub28.cacheIndex9
                    .getReferenceTableCompletion() * 2 / 100;
            percent += Class3_Sub13_Sub25.cacheIndex10
                    .getReferenceTableCompletion() * 2 / 100;
            percent += Class1002.cacheIndex11.getReferenceTableCompletion() * 2 / 100;
            percent += Class971.cacheIndex12.getReferenceTableCompletion() * 2 / 100;
            percent += Class1027.cacheIndex13.getReferenceTableCompletion() * 2 / 100;
            percent += Class0.cacheIndex14.getReferenceTableCompletion() * 2 / 100;
            percent += Class1001.cacheIndex15.getReferenceTableCompletion() * 2 / 100;

            // for(var3 = 0; var3 < client.aInteger_512; ++var3) {
            // var2 +=
            // ByteBuffer.aClass151_Sub1Array2601[var3].getReferenceTableCompletion(-61)
            // * Class3_Sub13_Sub23.anIntArray3288[var3] / 100;
            // }
            // var2 = 100;

            if (percent != 100) {
                if (0 != percent) {
                    Class1227.loading_completion = Class922
                            .combinejStrings(new Class1008[]{
                                    Class12.aClass94_327,
                                    Class72.createInt(percent),
                                    Class1217.aClass94_468});
                }

                Class951.anInt3684 = 20;
            } else {
                Class951.anInt3684 = 20;
                Class1227.loading_completion = Class1228.aClass94_2624;
                Class39.loadMiscSprites(Class140_Sub6.cacheIndex8);
                Class97.loadTitleScreen(Class3_Sub13_Sub25.cacheIndex10);
                // Class3_Sub13_Sub13.loadRunes(Class140_Sub6.cacheIndex8);
                Class1025.loadingStage = 45;
            }
        }
        if (Class1025.loadingStage == 45) {
            Class140_Sub3
                    .method1959(256, 2, 22050, Class3_Sub13_Sub15.isStereo);
            Class930.aClass3_Sub24_Sub4_1193 = new Class3_Sub24_Sub4();
            Class930.aClass3_Sub24_Sub4_1193.method479((byte) 98, 9, 128);
            Class1228.aClass155_2627 = Class58.method1195(22050,
                    Class38.gameClass942, Class1143.canvas, 0, 14);
            Class1228.aClass155_2627.method2154(-116,
                    Class930.aClass3_Sub24_Sub4_1193);
            Class922.method897(Class930.aClass3_Sub24_Sub4_1193, Class1001.cacheIndex15,
                    Class0.cacheIndex14, Class961.cacheIndex4);
            Class3_Sub21.aClass155_2491 = Class58.method1195(2048,
                    Class38.gameClass942, Class1143.canvas, 1, 14);
            Class3_Sub26.aClass3_Sub24_Sub2_2563 = new Class3_Sub24_Sub2();
            Class3_Sub21.aClass155_2491.method2154(-128,
                    Class3_Sub26.aClass3_Sub24_Sub2_2563);
            Class27.aClass157_524 = new Class157(22050, Class21.anInt443);
            //Class75_Sub2.cacheIndex6.getSpriteGroupId(Class1.scape_main);
            //System.out.println("Class1.aClass94_53: " + Class1.scape_main);
            Class1005.anInt1912 = Class75_Sub2.cacheIndex6
                    .getSpriteGroupId(Class1.scape_main);
            // Below deals with music
            // Class1005.anInt1912 = -1;//
            // Class75_Sub2.cacheIndex6.method2120(Class1.aClass94_53);
            Class951.anInt3684 = 30;
            Class1025.loadingStage = 50;
            Class1227.loading_completion = Class131.aClass94_1731;
        } else if (Class1025.loadingStage == 50) {
            var2 = Class3_Sub13_Sub12.method228(Class140_Sub6.cacheIndex8,
                    Class1027.cacheIndex13);
            var3 = Class1244.method2286();
            if (~var2 <= ~var3) {
                Class1227.loading_completion = Class143.aClass94_1879;
                Class951.anInt3684 = 35;
                Class1025.loadingStage = 60;
                Class922.loadFonts(Class1027.cacheIndex13, 0, Class140_Sub6.cacheIndex8);
            } else {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{
                                Class3_Sub28_Sub11.aClass94_3643,
                                Class72.createInt(100 * var2 / var3),
                                Class1217.aClass94_468});
                Class951.anInt3684 = 35;
            }
        } else if (Class1025.loadingStage == 60) {
            var2 = Class3_Sub28_Sub11.method599(-20916,
                    Class140_Sub6.cacheIndex8);
            var3 = Class973.method1185();
            if (var3 <= var2) {
                Class1227.loading_completion = Class3_Sub28_Sub4.aClass94_3575;
                Class1025.loadingStage = 65;
                Class951.anInt3684 = 40;
            } else {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{
                                Class1217.aClass94_461,
                                Class72.createInt(100 * var2 / var3),
                                Class1217.aClass94_468});
                Class951.anInt3684 = 40;
            }
        } else if (Class1025.loadingStage == 65) {
            Class951.anInt3684 = 45;
            Class1227.loading_completion = Class23.aClass94_459;
            Class922.setaInteger_544(5);
            Class1025.loadingStage = 70;
        } else if (Class1025.loadingStage == 70) {
            if (Class164.cacheIndex2.allGroupsExist()) {
                Class132.method1799((byte) 96, Class164.cacheIndex2);
                Class951.method631(false, Class164.cacheIndex2);
                Class3_Sub28_Sub8.method575(Class164.cacheIndex2, -1);
                Class1048.method375(3, Class159.cacheIndex7,
                        Class164.cacheIndex2);
                Class108.method1661(2, Class164.cacheIndex2,
                        Class159.cacheIndex7, true);
                ItemDefinition.method1103(Class159.cacheIndex7,
                        Class164.cacheIndex2, false);
                Class1031.method1864(true, (byte) -126,
                        Class10117.aClass153_1410,
                        Class157.aClass3_Sub28_Sub17_Sub1_2000,
                        Class159.cacheIndex7);
                Class1221.method969(Class164.cacheIndex2, 59);
                Class3_Sub20.method392(Class3_Sub28_Sub19.cacheIndex1,
                        Class164.cacheIndex2, -77, Class75_Sub3.cacheIndex0);
                Class988.method1053((byte) -117, Class164.cacheIndex2);
                Class923.method2180(Class159.cacheIndex7,
                        Class70.aClass153_1058);
                Class107.method1648(Class164.cacheIndex2, 255);
                Class1030.setupVarpJs5(Class164.cacheIndex2);
                Class1042.method89(Class1027.cacheIndex13, Class140_Sub6.cacheIndex8,
                        Class140_Sub3.cacheIndex3, Class159.cacheIndex7);
                Class3_Sub13_Sub17.method250(2048, Class164.cacheIndex2);
                Class1004.method1086(Class164.cacheIndex2, -6);
                Class3_Sub13_Sub8.method205(Class990.aClass153_2258,
                        Class1013.aClass153_2939, new Class7());
                Class65.method1236(Class990.aClass153_2258,
                        Class1013.aClass153_2939, -117);
                Class58.method1197(Class164.cacheIndex2, (byte) 69);
                Class144.method2065((byte) -125, Class164.cacheIndex2,
                        Class140_Sub6.cacheIndex8);
                Class107.method1645(Class164.cacheIndex2,
                        Class140_Sub6.cacheIndex8, (byte) -67);
                Class951.anInt3684 = 50;
                Class1227.loading_completion = Class3_Sub13_Sub12.aClass94_3142;
                Class1221.method968(128);
                Class1025.loadingStage = 80;
            } else {
                //LOAD ALL MODELS BEFORE MOVING ON
                //if(Class159.cacheIndex7.allGroupsExist()) {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{
                                Class3_Sub28_Sub2.aClass94_3546,
                                Class72.createInt(Class164.cacheIndex2
                                        .getTotalCompletion()),
                                Class1217.aClass94_468});
                Class951.anInt3684 = 50;
                //} else {
                //System.out.println("" + Class159.cacheIndex7.getGroupCompletion() + ", " + Class159.cacheIndex7.getTotalCount());
                    /*Class1227.loading_completion = client
                            .combinejStrings(new Class1008[] {
									Class3_Sub28_Sub2.loadingModelsString,
										Class1008.create(""+Class159.cacheIndex7.getTotalCompletion() * 60 / 100),
											Class1217.aClass94_468 });
				}*/
            }
        } else if (Class1025.loadingStage == 80) {
            var2 = Class990.customSpritesAmount(Class140_Sub6.cacheIndex8, false);
            var3 = Class990.customSpritesAmount(Class140_Sub6.cacheIndex8, true);
            //System.out.println(hint icon var2 + ";" + var3);
            //	System.out.println("var2: " + var2 + ", var3: " + var3);
            if (var2 < var3) {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{
                                Class3_Sub13_Sub38.aClass94_3445,
                                Class72.createInt(var2 * 100 / var3),
                                Class1217.aClass94_468});
                Class951.anInt3684 = 60;
            } else {
                //if(Class140_Sub6.cacheIndex8.allGroupsExist()) {
                //FORCE LOAD SPRITES BEFORE STARTING
                Class1025.loadingStage = 90;
                Class951.anInt3684 = 60;
                Class1227.loading_completion = Class1017_2.aClass94_1707;
                //}
            }
        } else if (Class1025.loadingStage == 90) {
            if (!Class97.aClass153_1376.allGroupsExist()) {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{
                                Class145.aClass94_1892,
                                Class72.createInt((byte) 9),
                                Class1217.aClass94_468});
                Class951.anInt3684 = 70;
            } else {
                Class14.loadExtraSprites(21, Class140_Sub6.cacheIndex8);
                Class945 var8 = new Class945(
                        Class3_Sub13_Sub28.cacheIndex9, Class97.aClass153_1376,
                        Class140_Sub6.cacheIndex8, 20,
                        !Class25.lowMemoryTextures);
                Rasterizer.method1140(var8);
                if (~Class3_Sub28_Sub10.brightness == -2) {
                    Rasterizer.method1137(0.9F);
                }

                if (2 == Class3_Sub28_Sub10.brightness) {
                    Rasterizer.method1137(0.8F);
                }

                if (Class3_Sub28_Sub10.brightness == 3) {
                    Rasterizer.method1137(0.7F);
                }

                if (~Class3_Sub28_Sub10.brightness == -5) {
                    Rasterizer.method1137(0.6F);
                }

                Class1227.loading_completion = Class3_Sub13_Sub14.aClass94_3167;
                Class1025.loadingStage = 100;
                Class951.anInt3684 = 70;
            }
        } else if (Class1025.loadingStage == 100) {
            if (Class3_Sub13_Sub25.cacheIndex10.allGroupsExist()) {
                downloadDlls();
                downloadCursors();
                Class1025.loadingStage = 110;
            }
        } else if (Class1025.loadingStage == 110) {
            Class989.aClass67_1443 = new Class67();
            Class38.gameClass942.startThread(
                    Class989.aClass67_1443, 10);
            Class1227.loading_completion = Class10117.aClass94_1409;
            Class951.anInt3684 = 75;
            Class1025.loadingStage = 120;
        } else if (Class1025.loadingStage == 120) {
            if (Class3_Sub13_Sub25.cacheIndex10.method2125(Class922.BLANK_CLASS_1008,
                    Class934.aClass94_3792)) {
                Class1017 var9 = new Class1017(
                        Class3_Sub13_Sub25.cacheIndex10.method2123(
                                Class922.BLANK_CLASS_1008, Class934.aClass94_3792));
                Class1.method69(var9, 104);
                Class1227.loading_completion = Class117.aClass94_1615;
                Class1025.loadingStage = 130;
                Class951.anInt3684 = 80;
            } else {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{Class930.aClass94_1183,
                                Class1016.aClass94_37});
                Class951.anInt3684 = 80;
            }
        } else if (Class1025.loadingStage == 130) {
            if (Class140_Sub3.cacheIndex3.allGroupsExist()) {
                if (Class971.cacheIndex12.allGroupsExist()) {
                    if (Class1027.cacheIndex13.allGroupsExist()) {
                        Class951.anInt3684 = 95;
                        Class1227.loading_completion = ByteBuffer.aClass94_2597;
                        Class1025.loadingStage = 135;
                    } else {
                        Class1227.loading_completion = Class922
                                .combinejStrings(new Class1008[]{
                                        Class1009.aClass94_2961,
                                        Class72.createInt(85 + (Class1027.cacheIndex13
                                                .getTotalCompletion() / 20)),
                                        Class1217.aClass94_468});
                        Class951.anInt3684 = 85;
                    }
                } else {
                    Class1227.loading_completion = Class922
                            .combinejStrings(new Class1008[]{
                                    Class1009.aClass94_2961,
                                    Class72.createInt(75 + (Class971.cacheIndex12
                                            .getTotalCompletion() / 10)),
                                    Class1217.aClass94_468});
                    Class951.anInt3684 = 85;
                }
            } else {
                Class1227.loading_completion = Class922
                        .combinejStrings(new Class1008[]{
                                Class1009.aClass94_2961,
                                Class72.createInt(Class140_Sub3.cacheIndex3
                                        .getTotalCompletion() * 3 / 4),
                                Class1217.aClass94_468});
                Class951.anInt3684 = 85;
            }
        } else if (135 == Class1025.loadingStage) {
            Class1025.loadingStage = 140;
            Class951.anInt3684 = 96;
        } else if (Class1025.loadingStage == 140) {
            Class3_Sub22.loginScreenInterfaceid = 601;// Class140_Sub3.aClass153_2727.method2120(Class1001.aClass94_3992,
            // (byte)-30);
            Class3_Sub13_Sub6.cacheIndex5.method2115(false, true);
            Class75_Sub2.cacheIndex6.method2115(true, true);
            Class140_Sub6.cacheIndex8.method2115(true, true);
            Class1027.cacheIndex13.method2115(true, true);
            Class3_Sub13_Sub25.cacheIndex10.method2115(true, true);
            Class140_Sub3.cacheIndex3.method2115(true, true);
            Class951.anInt3684 = 97;
            Class1227.loading_completion = Class3_Sub5.aClass94_2267;
            Class1025.loadingStage = 150;
            Class58.aBoolean913 = true;
        } else if (Class1025.loadingStage == 150) {
            Class88.method1454();
            /*
             * if(Class1008.safeMode) { Class3_Sub28_Sub9.anInt3622 = 0;
             * Class3_Sub28_Sub14.antiAliasing = 0; Class1002.anInt2577 = 0;
             * Class3_Sub20.anInt2488 = 0; }
             */

            // Class1008.safeMode = true;
            Class119.writePreferences(Class38.gameClass942);
            // System.out.println("setLowDefinition - in loadingstage");
            Class1031.setLowDefinition(false, Class1002.anInt2577, -1, -1);
            Class951.anInt3684 = 100;
            Class1025.loadingStage = 160;
            Class1227.loading_completion = Class1002.aClass94_2576;
        } else if (Class1025.loadingStage == 160) {
            /*
             * if (!closed_revision_choosing && client.aInteger_544 == 5) { return;
             * }
             */
            Class3_Sub13_Sub11.constructLoginScreen(true);
            if (Class59.hdEnabled) {
                Class1031.setLowDefinition(false, 1, -1, -1);
            }
        }
    }

    public static void launchUrl(String url) {
        if (Desktop.isDesktopSupported())
            try {
                System.out.println("Launching url: " + url);
                Desktop.getDesktop().browse(new URI(url));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        else {
            System.out.println("Desktop not supported -- could not launch URL: " + url);
            Class966_2.sendMessage((Class1008) null, Class943.create("Failed to launch URL: " + url), 0);
        }
    }

    public static void downloadDlls() {
        String bit = System.getProperty("os.arch");
        File zipFile = new File(Class942.getCacheDir() + File.separator
                + "jogl_awt.dll");
        System.out.println(bit.contains("64") ? "64 bit OS" : "32 bit OS");
        //  printAllGuaranteedProperties();
        if (zipFile.exists()) {
            System.out.println("OpenGL dlls exist.");
        } else {
            String url = bit.contains("64") ? "http://smite.io/dlls/Win64.zip"
                    : "http://smite.io/dlls/Win32.zip";
            try {
                System.out.println("OpenGL dlls do not exist.");
                Class950.downloadFile(url, Class942.getCacheDir());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void downloadCursors() {
        File zipFile = new File(Class942.getCacheDir() + File.separator
                + "cursors/Cursor_115.png");
        // printAllGuaranteedProperties();

    }

    protected static int previousTab, previousFullscreenTab = 35913980; // invy
    protected static boolean inventoryHidden;
    protected static boolean chatboxHidden;

    protected static int inputBox = 1;

    final void mainLoop() {

        Class1134.loopCycle++;// TODO mainloop
        if (Class1134.loopCycle % 1000 == 1) {
            GregorianCalendar var2 = new GregorianCalendar();
            Class38_Sub1.randomSeed = var2.get(11) * 600
                    - (-(var2.get(12) * 10) + -(var2.get(13) / 6));
            Class3_Sub13_Sub7.tooltipRandomGenerator
                    .setSeed((long) Class38_Sub1.randomSeed);
        }

        handleUpdateServer();
        handleRequests();

        Class1030.method728();
        Class58.method1194();
        Class32.method996();
        Class62.method1225();
        if (Class1012.aBoolean_617) {
            Class31.method990();
        }

        /*
         * if(Class1143.mainScreenInterface ==
         * Class3_Sub22.loginScreenInterfaceid) { if (Class1012.aBoolean_617) {
         * Class920.method_475(Class23.canvasWid / 2 - 152, 250, 304, 34,
         * 9179409); Class920.method_475(-151 + Class23.canvasWid / 2, 250 - -1,
         * 302, 32, 0); Class920.method_574(Class23.canvasWid / 2 - 150, 250 +
         * 2, Class951.anInt3684 * 3, 30, 9179409);
         * Class920.method_574(Class23.canvasWid / 2 + -150 - -(3 *
         * Class951.anInt3684), 2 + 250, 300 + -(3 *
         * Class951.anInt3684), 30, 0); } else {
         * Class1023.method_475(Class23.canvasWid / 2 + -152, 250, 304, 34,
         * 9179409); Class1023.method_475(-151 + Class23.canvasWid / 2, 250 + 1,
         * 302, 32, 0); Class1023.method_574(Class23.canvasWid / 2 + -150, 250 +
         * 2, Class951.anInt3684 * 3, 30, 9179409); Class1023.method_574(3 *
         * Class951.anInt3684 + -150 + Class23.canvasWid / 2, 2 + 250, -(3 *
         * Class951.anInt3684) + 300, 30, 0); } char[] stars = new
         * char[Class1005.characterCount]; Arrays.fill(stars, '*');
         * client.getSmallClass1019().method699(Class1008.createJString(new
         * String(stars)), Class23.canvasWid / 2, 260, 16777215, 0); }
         */

        int var4;
        if (Class38.mouseWheelHandler != null) {
            var4 = Class38.mouseWheelHandler.getRotation();
            Class1221.anInt561 = var4;
        }
        if (Class922.aInteger_544 != 0) {
            if (Class922.aInteger_544 == 5) {
                load();
                Class75_Sub4.method1355();
            } else if (Class922.aInteger_544 == 25 || Class922.aInteger_544 == 28) {
                Class40.rebuildMap();
            }
        } else {
            load();
            Class75_Sub4.method1355();
        }
        if (10 == Class922.aInteger_544) {
            method47((byte) 1);
            Class3_Sub13_Sub21.method267((byte) 36);
            Class163_Sub1_Sub1.method2216((byte) 81);
            Class1126.login((byte) -9);
        } else if (Class922.aInteger_544 == 30) {
            Class3_Sub13_Sub13.handleMainGame(true);
        } else if (Class922.aInteger_544 == 40) {
            Class1126.login((byte) -9);
            if (~Class923.returnCode != 2) {
                if (Class923.returnCode == 15) {
                    Class21.method912(false);
                } else if (Class923.returnCode != 2) {
                    Class167.processLogout();
                }
            }
        }

        if (!Class929.aBoolean_606 && Class1025.loadingStage == 30) {
            handleSelectingScreen();
        }
    }

    /**
     * Gets the percentage of total CPU used running this program.
     *
     * @return Numeric (double) value of CPU usage in percentage.
     */
    public static double getCPUUsage() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName name = ObjectName.getInstance("java.lang:type=OperatingSystem");
            AttributeList list = mbs.getAttributes(name, new String[]{"ProcessCpuLoad"});

            if (list.isEmpty())
                return Double.NaN;

            Attribute att = (Attribute) list.get(0);
            Double value = (Double) att.getValue();

            if (value == -1.0)
                return Double.NaN;

            return ((int) (value * 1000) / 10.0);
        } catch (Exception e) {
            return -1;
        }
    }

    public static final void setaInteger_544(int newState) {
        if (aInteger_544 != newState) {
            if (0 == aInteger_544) {
                Class93.method1517((byte) -118);
            }

            if (newState == 40) { //reconnecting
                Class1217.initLoginConnection(0);
            }

            boolean var2 = newState == 5 || newState == 10 || newState == 28;
            if (40 != newState && null != Class163_Sub2_Sub1.worldConnection2) {
                Class163_Sub2_Sub1.worldConnection2.close();
                Class163_Sub2_Sub1.worldConnection2 = null;
            }

            if (newState == 25 || newState == 28) {
                Class162.modelFetchCount = 0;
                Class1030.anInt2579 = 1;
                Class163_Sub2_Sub1.loadingScreenType = 0;
                Class3_Sub5.anInt2275 = 1;
                Class3_Sub13_Sub24.anInt3293 = 0;
                //Class1006.method1250(true);
            }

            //if(25 == var0 || var0 == 10) {
            //   Class72.method1293();
            //}

            if (newState == 5 || !Class929.aBoolean_606) {
                load_title_screen(Class3_Sub13_Sub25.cacheIndex10);
            } else {
                Class3_Sub13_Sub17.method247((byte) -121);
            }

            boolean var3 = 5 == aInteger_544 || aInteger_544 == 10 || 28 == aInteger_544;
            if (var3 == !var2) {
                if (var2) {
                    Class963.currentSound = Class1005.anInt1912;
                    Class9.musicVolume = 255;
                    Class3_Sub13_Sub15.isStereo = true;
                    Class1048.soundEffectsVolume = 127;
                    Class14.areaSoundsVolume = 255;
                    if (Class9.musicVolume != 0) {
                        //Plays login music - anInt1912
                        //	  System.out.println("level: " + Class9.musicVolume + ", anint1912: "+ Class1005.anInt1912);
                        //  	  Class151.playMusic(true, Class1005.anInt1912, 0, Class75_Sub2.cacheIndex6, false, 255, 2);
                        //	  Class167.handleMusicEffect(
                        //				1,
                        //				1,
                        //				(byte) -1);
                    } else {
                        Class1245.method882(-1, 2);
                    }

                    Class58.class1006.method1247(false);
                } else {
                    Class1245.method882(-1, 2);
                    Class58.class1006.method1247(true);
                }
            }

            if (Class1012.aBoolean_617 && (25 == newState || newState == 28 || 40 == newState)) {
                Class1012.method1833();
            }

            if (Class1012.aBoolean_617 && (newState == 30)) {
                Class1134.forceRefresh = true;
            }

            aInteger_544 = newState;
            //System.out.println("state:" + newState);

			/*  if(var0 == 30 || var0 == 25) {
	            	//TODO: move this to an earlier location
	           	if(client.clientSize > 0) {
	            		client.reloadFullscreenInterfaces();
	            	} else {
	            		client.sendGameframe(client.aInteger_510, false);
	            	}
	            	System.out.println("sent gf");
	            	client.setGameframe = true;
	            } else {
	            	client.setGameframe = false;
	            	System.out.println("gf not set now");
	            }*/

			/* if(client.clientSize > 0 && var0 == 10) {
	            	Class122.getFrame().setBackground(Color.BLACK);
	            	Class122.getPanel().setBackground(Color.BLACK);
	            	Class955.canvasReplaceRecommended = true;
	            }
			 */
            if (newState == 5 || newState == 10 || newState == 25) {
                //TODO: centre interface 601(login screen)
                if (Class929.newCursors)
                    Class122.setCursor(0);
            }
        }

        //TODO: if it isn't the 1st time the client is started and we are relogging, set the player to fixed screen

		/*if(client.LAST_STATE == 25 && var0 == 30
	        		  && client.clientSize > 0) {
	        	 client.prepareForFullscreen();
					client.reloadFullscreenInterfaces();
					client.clientSize = true;
					client.set = false;
					//Class955.canvasReplaceRecommended = true;
					client.forceRefresh();
					System.gc();
	        	// client.set = false;
	        	// Class955.canvasReplaceRecommended = true;
	         }*/

        if (LAST_STATE == GameState.RECONNECTING && newState == GameState.WELCOME) {
            if (Class1012.aBoolean_617)
                Class920.resetPixels();
            else
                Class1023.resetPixels();

            //Class122.getFrame().setBackground(Color.BLACK);
            //Class122.getPanel().setBackground(Color.BLACK);
        }

        if ((LAST_STATE == GameState.RECONNECTING || LAST_STATE == GameState.WELCOME
                || LAST_STATE == GameState.LOGIN_SCREEN) && newState == GameState.LOGGED_IN) {
            if (clientSize > 0) {
                reloadFullscreenInterfaces();
            } else {
                //client.sendGameframe(client.aInteger_510, false);
            }
        }

        if (/*(client.LAST_STATE == 30 || client.LAST_STATE == 40)*/
                LAST_STATE != 5 && newState == 10) {
            if (clientSize > 0) {
                //System.out.println("Setting reverted size(logout)");

                //client.rearrangeLoginScreen();
                if (clientSize == 2) {
                    //TODO: recreate frame
                    startSize = 2;
                }
                clientSize = 0;
                startSize = 1;
                revertToFixed();
                Class955.canvasReplaceRecommended = true;
                set = true;

				/*				if (!Class1012.aBoolean_617) {
						Class1023.resetPixels();
					} else {
						Class920.resetPixels();
					}*/
                //client.setDefaults();
            }
        }

        if (LAST_STATE == 25 && newState == 30) {
            if (startSize > 0) {
                //System.out.println("Initial fullscreen set");
                if (Class922.startSize == 1)
                    prepareForResizable();
                else
                    prepareForFullscreen();
                reloadFullscreenInterfaces();
                clientSize = 1;
                set = false;
                //Class955.canvasReplaceRecommended = true;
                forceRefresh();
                System.gc();
                startSize = 0;
            } else {
                if (Class1012.aBoolean_617)
                    Class920.resetPixels();
                else
                    Class1023.resetPixels();
            }
        }

        if (newState == 10) {
            //Reset all vars
            inputBox = 1;
            inventoryHidden = false;
            chatboxHidden = false;
            Class21.reHideChatbox = false;
            previousFullscreenTab = 35913980;
            amountModifier = BLANK_CLASS_1008;
            username = BLANK_CLASS_1008;
            password = BLANK_CLASS_1008;
            dialogueUp = false;
            hideFSTabs = false;
        }

        LAST_STATE = newState;
    }

    static final void drawScreen() {
        try {
            // Class1210.anImage2695.getGraphics().clearRect(0, 0, 765, 503);

            int mouseX = Class922.mouseX;
            int mouseY = Class922.mouseY;

            boolean inBox1 = false, inBox2 = false, inBox3 = false, inBox4 = false, inBox5 = false;
            if (mouseX >= 179 && mouseY >= 75 && mouseX <= 579 && mouseY <= 110) {
                inBox1 = true;
            }
            if (mouseX >= 179 && mouseY >= 125 && mouseX <= 579 && mouseY <= 160) {
                inBox2 = true;
            }
            if (mouseX >= 179 && mouseY >= 175 && mouseX <= 579 && mouseY <= 210) {
                inBox3 = true;
            }
            if (mouseX >= 179 && mouseY >= 225 && mouseX <= 579 && mouseY <= 260) {
                inBox4 = true;
            }
            if (mouseX >= 80 && mouseY >= 400 && mouseX <= 680 && mouseY <= 435) {
                inBox5 = true;
            }

            //System.out.println("mouseX: " + mouseX + ", mouseY: " + mouseY);

            Graphics var5 = Class1143.canvas.getGraphics();

            if (Class3_Sub13_Sub32.aFont3384 == null) {
                Class3_Sub13_Sub32.aFont3384 = new java.awt.Font("Helvetica",
                        1, 20);
            }

            if (null == Class3_Sub14.selectionImage) {
                Class3_Sub14.selectionImage = Class1143.canvas.createImage(765,
                        503);
            }

            Graphics var6 = Class3_Sub14.selectionImage.getGraphics();

            var6.setColor(Color.black);
            var6.fillRect(0, 0, 765, 503);

            var6.setColor(new Color(1f, 1f, 1f, .1f));
            var6.fillRoundRect(50, 40, 660, 428, 5, 5); //Background

            var6.setColor(Color.gray);
            var6.drawRoundRect(49, 39, 661, 429, 5, 5); //Background


            var6.setColor(inBox1 ? Color.red : Color.gray);
            var6.drawRect(179, 77, 400, 35); //Rev
            var6.setColor(inBox2 ? Color.red : Color.gray);
            var6.drawRect(179, 125, 400, 35); //HD
            var6.setColor(inBox3 ? Color.red : Color.gray);
            var6.drawRect(179, 175, 400, 35); //FS
            var6.setColor(inBox4 ? Color.red : Color.gray);
            var6.drawRect(179, 225, 400, 35); //Low memory
            var6.setColor(Color.RED);
            var6.drawRect(80, 400, 600, 35);

            var6.setColor(new Color(1f, 0f, 0f, (inBox5 ? .4f : .3f)));
            var6.fillRect(81, 401, 599, 34);

            var6.setColor(Color.white);
            var6.setFont(Class3_Sub13_Sub32.aFont3384);

            var6.drawString("Revision:", 254, 100);
            var6.drawString("OpenGL HD:", 254, 148);
            var6.drawString("Class922 size:", 254, 198);
            var6.drawString("Low memory:", 254, 248);

            var6.drawString("464", 476, 100);
            var6.drawString("" + Class922.hdOnLogin, 476, 148);
            var6.drawString("" + (Class922.startSize == 0 ? "Fixed" : Class922.startSize == 1 ? "Resizable" : "Fullscreen"), 476, 198);
            var6.drawString("" + !(Class1005.showGroundDecorations), 476, 248);

            var6.drawString("Continue", 350, 424);

            var5.drawImage(Class3_Sub14.selectionImage, 0, 0, null);

            /*
             * Graphics var11; var11 = Class1143.canvas.getGraphics();
             * Class1143.canvas.repaint();
             *
             * var11.clearRect(0, 0, 765, 503);
             *
             * if (Class1012.aBoolean_617) { Class920.resetPixels();
             * //Class920.method_576(0, 0, 765, 503); // x, y, width, height }
             * else { Class1023.resetPixels(); // Class1023.method_576(0, 0,
             * 765, 503); }
             */

            // client.revision_images[0].drawSprite(0, 0);

            /*
             * revision_images[1].drawFlippedSprite(263, 15);
             * revision_images[2].drawFlippedSprite(263, 440);
             *
             * set_464[clientLoad == 464 ? 1 : 0].drawFlippedSprite(263, 60);
             * set_530[clientLoad == 530 ? 1 : 0].drawFlippedSprite(327, 60);
             * set_562[clientLoad == 562 ? 1 : 0].drawFlippedSprite(391, 60);
             * set_602[clientLoad == 602 ? 1 : 0].drawFlippedSprite(455, 60);
             *
             * hd_sprites[client.hdOnLogin ? 1 : 0].drawFlippedSprite(340, 380);
             * sd_sprites[client.hdOnLogin ? 0 : 1].drawFlippedSprite(391, 380);
             */

            // Class164_Sub1.aClass158_3009.drawGraphics(var11, 0, 0);
        } catch (Exception var10) {
            Class1143.canvas.repaint();
        }

    }

    static final void handleSelectingScreen() {
        int x = Class163_Sub1.clickX2;
        int y = Class38_Sub1.clickY2;

        //System.out.println("x: " + x + ", y: " + y);
        if (x >= 179 && y >= 75 && x <= 579 && y <= 110
                && Class3_Sub28_Sub11.anInt3644 == 1) {
            //Class929.clientLoad = Class929.clientLoad == 464 ? 562 : Class929.clientLoad == 562 ? 602 : 464;
			/*if (clientLoad == 464) {
				clientLoad = 530;
			} else if (clientLoad == 530) {
				clientLoad = 562;
			} else if (clientLoad == 562) {
				clientLoad = 602;
			} else if (clientLoad == 602) {
				clientLoad = 464;
			}*/
        }
        if (x >= 179 && y >= 125 && x <= 579 && y <= 160
                && Class3_Sub28_Sub11.anInt3644 == 1) {
            Class922.hdOnLogin = !Class922.hdOnLogin;
            System.out.println("OpenGL HD "
                    + (Class922.hdOnLogin ? "enabled" : "disabled") + ".");
        }
        if (x >= 179 && y >= 175 && x <= 579 && y <= 210
                && Class3_Sub28_Sub11.anInt3644 == 1) {
            //startSize = startSize == 2 ? 0 : startSize + 1;
            startSize = startSize == 0 ? 1 : 0;
            System.out
                    .println("Frame size: "
                            + (startSize == 0 ? "Fixed" :
                            startSize == 1 ? "Resizable" : "Fullscreen") + ".");
        }
        if (x >= 179 && y >= 225 && x <= 579 && y <= 260
                && Class3_Sub28_Sub11.anInt3644 == 1) {
            Class1005.showGroundDecorations = !Class1005.showGroundDecorations;

            System.out
                    .println("Low memory "
                            + (!Class1005.showGroundDecorations ? "enabled"
                            : "disabled") + ".");
        }
        if (x >= 80 && y >= 400 && x <= 680 && y <= 435
                && Class3_Sub28_Sub11.anInt3644 == 1) {
            closeSelect();
        }
    }

    public static final void closeSelect() {
        if (Class1012.aBoolean_617) {
            Class920.resetPixels();
        } else {
            Class1023.resetPixels();
        }
        Class929.aInteger_510 = 464;//(Integer) Class944.getSetting("gameframe") != -1 ? (Integer) Class944.getSetting("gameframe") : Class929.clientLoad == 464 ? 474 : Class929.clientLoad == 530 ? 530 : 562;
        Class929.aInteger_511 = 464;//(Integer) Class944.getSetting("hitmarkers") != -1 ? (Integer) Class944.getSetting("hitmarkers") : (Class929.clientLoad == 464 || Class929.clientLoad == 530) ? 464 : Class929.clientLoad == 562 ? 554 : 634;

        boolean firstStart = false;
        if ((Integer) Class944.getSetting("gameframe") == -1 || (Integer) Class944.getSetting("hitmarkers") == -1)
            firstStart = true;

        if ((Integer) Class944.getSetting("gameframe") == -1) {
            Class944.changeSetting("gameframe", 464);
        }

        Class929.orbsToggled = firstStart ? true : (Boolean) Class944.getSetting("orbs_toggled");
        Class922.leftClickAttack = firstStart ? true : (Boolean) Class944.getSetting("left_click_attack");
        Class3_Sub26.forceTweeningEnabled = firstStart ? false : (Boolean) Class944.getSetting("tweening_enabled");
        Class929.newMenus = false;
        Class929.newHealthbars = false;
        Class929.newHits = false;
        Class929.newCursors = false;
        Class59.hdEnabled = Class922.hdOnLogin;
        Class922.censorOn = firstStart ? false : (Boolean) Class944.getSetting("censor");
        Class929.aInteger_510 = 464;

        Class929.hdMinimap = false;
        Class929.aBoolean_606 = true;

        Class25.lowMemoryTextures = !Class1005.showGroundDecorations;
        Class922.aBoolean_611 = !Class1005.showGroundDecorations;
        Class1228.highDetailLights = Class1005.showGroundDecorations;

        //Class956.removeRoofs = true;
        //Class943.aBoolean1084 = true;
        Class992.visibleLevels = Class1005.showGroundDecorations;


        Class944.changeSetting("client_load", 464);
        Class944.changeSetting("hd_enabled", Class922.hdOnLogin);
        Class944.changeSetting("start_size", Class922.startSize);
        Class944.changeSetting("orbs_toggled", Class929.orbsToggled);
        Class944.changeSetting("tweening_enabled", Class3_Sub26.forceTweeningEnabled);
        Class944.changeSetting("new_menus", Class929.newMenus);
        Class944.changeSetting("new_health_bars", Class929.newHealthbars);
        Class944.changeSetting("new_hits", Class929.newHits);
        Class944.changeSetting("new_cursors", Class929.newCursors);
        Class944.changeSetting("censor", Class922.censorOn);
        Class944.changeSetting("gameframe", Class929.aInteger_510);
        Class944.changeSetting("hitmarkers", Class929.aInteger_511);
        Class944.changeSetting("lowmem", !Class1005.showGroundDecorations);
        Class944.changeSetting("left_click_attack", Class922.leftClickAttack);
        Class944.save();
        //System.out.println("Saved settings.");
    }

    public static final void secondLoadingBar(boolean var1, Class1019 var2) {
        int var3;
        if (Class1012.aBoolean_617 || var1) {
            var3 = Class1013.canvasHei;
        }
        var2.drawText(Class25.aClass94_485, Class23.canvasWid / 2,
                Class1013.canvasHei / 2 - 26, 16777215, -1);
        var3 = Class1013.canvasHei / 2 + -18;
        if (Class1012.aBoolean_617) {
            Class920.method_475(Class23.canvasWid / 2 - 152, var3, 304, 34,
                    Class929.aInteger_509);
            Class920.method_475(-151 + Class23.canvasWid / 2, var3 - -1, 302,
                    32, 0);
            Class920.method_574(Class23.canvasWid / 2 - 150, var3 + 2,
                    Class951.anInt3684 * 3, 30, Class929.aInteger_509);
            Class920.method_574(Class23.canvasWid / 2 + -150
                            - -(3 * Class951.anInt3684), 2 + var3,
                    300 + -(3 * Class951.anInt3684), 30, 0);
        } else {
            Class1023.drawRect(Class23.canvasWid / 2 + -152, var3, 304, 34,
                    0x980000);
            Class1023.drawRect(-151 + Class23.canvasWid / 2, var3 + 1, 302,
                    32, 0);
            Class1023.fillRect(Class23.canvasWid / 2 + -150, var3 + 2,
                    Class951.anInt3684 * 3, 30, 0x980000); // Inner rect
            Class1023.fillRect(3 * Class951.anInt3684 + -150
                            + Class23.canvasWid / 2, 2 + var3,
                    -(3 * Class951.anInt3684) + 300, 30, 0);

            // Below is wider bar, unfinished
            /*
             * Class1023.method_475(Class23.canvasWid / 2 + -1522,
             * var3ItemPile.canvasHei - 36, 304Class23.canvasWid - 2, 34,
             * 6908265); Class1023.method_475(-151 + Class23.canvasWid / 22, var3
             * + 1ItemPile.canvasHei - 34, 302Class23.canvasWid - 4, 32, 0);
             * Class1023.method_574(Class23.canvasWid / 2 + -1503, var3 +
             * 2ItemPile.canvasHei- 32, Class951.anInt3684 * 3, 30,
             * 6908265); //Inner rect Class1023.method_574(3 *
             * Class951.anInt3684 + -150 + Class23.canvasWid / 23, 2 +
             * var3ItemPile.canvasHei - 32, -(3 * Class951.anInt3684) + 300,
             * 30, 0);
             */
        }

        var2.drawText(Class1227.loading_completion, Class23.canvasWid / 2,
                4 + Class1013.canvasHei / 2, 16777215, -1);
    }

    public static final void load_title_screen(Class1027 var0) {
        if (!Class1212.titleScreenLoaded) {
            if (!Class1012.aBoolean_617) {
                Class1023.resetPixels();
            } else {
                Class920.resetPixels();
            }
            // Below is background image
            /*
             * if
             * (Class3_Sub13_Sub25.cacheIndex10.method2144(Class154.anInt1966))
             * { Class1206_2 tempSprite = new Class1206_2(
             * Class3_Sub13_Sub25.cacheIndex10.getFile(0, Class154.anInt1966),
             * Class1143.canvas); if (Class1012.aBoolean_617) {
             * Class40.aClass3_Sub28_Sub16_680 = new Class1011(tempSprite); }
             * else Class40.aClass3_Sub28_Sub16_680 = tempSprite;
             *
             * // Class40.aClass3_Sub28_Sub16_680 = //
             * Class75_Sub2.method1344(117, var0, Class154.anInt1966); int var2
             * = Class1013.canvasHei; int var3 = var2 * 956 / 503;
             * Class40.aClass3_Sub28_Sub16_680.method639( (Class23.canvasWid +
             * -var3) / 2, 0, var3, var2); }
             */
            // Class954.aClass109_1856 =
            // Class3_Sub28_Sub6.buffer(Class79.anInt1124, var0, true);//TODO logo
            // Class954.aClass109_1856.method1667(Class23.canvasWid /
            // 2 + -(Class954.aClass109_1856.anInt1461 / 2), 18);
            Class1212.titleScreenLoaded = true;
        }
    }

    public static void method896(boolean var0) {
        try {
            //	client.aClass94_388 = null;
            // client.aClass94_374 = null;
            //	client.aClass94_392 = null;
            //	client.aClass94_385 = null;
            //	client.aClass94_376 = null;
            //	client.aClass94_397 = null;
            //	client.anIntArray356 = null;
            // client.aClass94_366 = null;
            //	client.aClass94Array358 = null;
            //	client.nullString = null;
            //	client.aClass94_365 = null;
            //	client.aClass94_394 = null;
            if (!var0) {
                Class1211.list(-109);
            }

            //	client.aClass94_361 = null;
            //	client.aClass94_391 = null;
            //	client.aClass94_401 = null;
            //	client.aClass94_405 = null;
            //	client.aClass94_363 = null;
            // client.aByteArrayArrayArray383 = (byte[][][])null;
            //	client.aClass94_404 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ck.A(" + var0 + ')');
        }
    }

    static final boolean method897(Class3_Sub24_Sub4 var1, Class1027 var2, Class1027 var3,
                                   Class1027 var4) {
        Class124.aClass153_1661 = var2;
        Class40.aClass153_679 = var4;
        Class934.aClass153_3786 = var3;
        Class101.aClass3_Sub24_Sub4_1421 = var1;
        return true;
    }

    public static final void method900(Class991 var0, int var1) {
        try {
            var0.aBoolean2810 = false;
            if (-1 != var0.currentMoveAnimation) {
                Class954 var2 = Class954.list(
                        var0.currentMoveAnimation);
                if (null != var2 && null != var2.frames) {
                    ++var0.anInt2802;
                    if (~var2.frames.length < ~var0.anInt2813
                            && var0.anInt2802 > var2.cycles[var0.anInt2813]) {
                        var0.anInt2802 = 1;
                        ++var0.anInt2813;
                        ++var0.anInt2793;
                        Class1007.method1470(var0.x, var2, 183921384,
                                var0.y,
                                var0 == Class945.thisClass946,
                                var0.anInt2813);
                    }

                    if (~var0.anInt2813 <= ~var2.frames.length) {
                        var0.anInt2813 = 0;
                        /*
                         * if (arg1.aBoolean5198) { arg1.anInt5225 =
                         * arg1.method1326(4).method2454( (byte) 109); if
                         * (arg1.anInt5225 == -1) { arg1.aBoolean5198 = false;
                         * break; } class95 = Class125.method1404(-8192,
                         * arg1.anInt5225); }//TODO this
                         */
                        Class1007.method1470(var0.x, var2, 183921384,
                                var0.y,
                                Class945.thisClass946 == var0,
                                var0.anInt2813);
                    }

                    var0.anInt2793 = var0.anInt2813 + 1;
                    if (~var0.anInt2793 <= ~var2.frames.length) {
                        var0.anInt2793 = 0;
                    }
                } else {
                    // TODO missing boolean here to add aBoolean5198 in entity
                    var0.currentMoveAnimation = -1;
                }
            }

            int var6;
            if (var0.anInt2842 != -1 && ~Class1134.loopCycle <= ~var0.anInt2759) {
                var6 = Class1211.list(var0.anInt2842).anInt542;
                if (0 != ~var6) {
                    Class954 var3 = Class954.list(var6);
                    if (var3 != null && var3.frames != null) {
                        if (0 > var0.anInt2805) {
                            var0.anInt2805 = 0;
                            Class1007.method1470(var0.x, var3, 183921384,
                                    var0.y,
                                    Class945.thisClass946 == var0, 0);
                        }

                        ++var0.anInt2761;
                        if (var0.anInt2805 < var3.frames.length
                                && ~var3.cycles[var0.anInt2805] > ~var0.anInt2761) {
                            ++var0.anInt2805;
                            var0.anInt2761 = 1;
                            Class1007.method1470(var0.x, var3, var1
                                            ^ -183911469, var0.y,
                                    Class945.thisClass946 == var0,
                                    var0.anInt2805);
                        }

                        if (var0.anInt2805 >= var3.frames.length) {
                            var0.anInt2842 = -1;
                        }

                        var0.anInt2826 = var0.anInt2805 + 1;
                        if (~var3.frames.length >= ~var0.anInt2826) {
                            var0.anInt2826 = -1;// TODO convert this graphics
                            // part class153_sub37 on 569
                        }
                    } else {
                        var0.anInt2842 = -1;
                    }
                } else {
                    var0.anInt2842 = -1;
                }
            }

            if (~var0.currentAnimationId != 0 && -2 <= ~var0.animationDelay) {
                Class954 var2 = Class954.list(
                        var0.currentAnimationId);
                if (~var2.anInt1866 == -2 && var0.anInt2811 > 0
                        && ~Class1134.loopCycle <= ~var0.anInt2800
                        && Class1134.loopCycle > var0.anInt2790) {
                    var0.animationDelay = 1;
                    return;
                }
            }

            if (var1 == -11973) {
                if (~var0.currentAnimationId != 0 && -1 == ~var0.animationDelay) {
                    Class954 var2 = Class954.list(
                            var0.currentAnimationId);
                    if (var2 != null && var2.frames != null) {
                        ++var0.anInt2760;
                        if (~var0.anInt2832 > ~var2.frames.length
                                && var0.anInt2760 > var2.cycles[var0.anInt2832]) {
                            var0.anInt2760 = 1;
                            ++var0.anInt2832;
                            Class1007.method1470(var0.x, var2, 183921384,
                                    var0.y,
                                    var0 == Class945.thisClass946,
                                    var0.anInt2832);
                        }

                        if (var2.frames.length <= var0.anInt2832) {
                            var0.anInt2832 -= var2.anInt1865;
                            ++var0.anInt2773;
                            if (~var0.anInt2773 > ~var2.anInt1861) {
                                if (var0.anInt2832 >= 0
                                        && ~var2.frames.length < ~var0.anInt2832) {
                                    Class1007.method1470(var0.x, var2,
                                            var1 ^ -183911469, var0.y,
                                            Class945.thisClass946 == var0,
                                            var0.anInt2832);
                                } else {
                                    var0.currentAnimationId = -1;
                                }
                            } else {
                                var0.currentAnimationId = -1;
                            }
                        }

                        var0.anInt2776 = var0.anInt2832 + 1;
                        if (var0.anInt2776 >= var2.frames.length) {
                            var0.anInt2776 -= var2.anInt1865;
                            if (var2.anInt1861 > var0.anInt2773 + 1) {
                                if (0 > var0.anInt2776
                                        || var0.anInt2776 >= var2.frames.length) {
                                    var0.anInt2776 = -1;
                                }
                            } else {
                                var0.anInt2776 = -1;
                            }
                        }

                        var0.aBoolean2810 = var2.aBoolean1859;
                    } else {
                        var0.currentAnimationId = -1;
                    }
                }

                if (0 < var0.animationDelay) {
                    --var0.animationDelay;
                }

                for (var6 = 0; var0.aClass145Array2809.length > var6; ++var6) {
                    Class145 var7 = var0.aClass145Array2809[var6];
                    if (null != var7) {
                        if (~var7.anInt1900 >= -1) {
                            Class954 var4 = Class954
                                    .list(var7.anInt1890);
                            if (null != var4 && var4.frames != null) {
                                ++var7.anInt1897;
                                if (var7.anInt1893 < var4.frames.length
                                        && var7.anInt1897 > var4.cycles[var7.anInt1893]) {
                                    ++var7.anInt1893;
                                    var7.anInt1897 = 1;
                                    Class1007.method1470(var0.x, var4,
                                            183921384, var0.y,
                                            var0 == Class945.thisClass946,
                                            var7.anInt1893);
                                }

                                if (~var4.frames.length >= ~var7.anInt1893) {
                                    ++var7.anInt1894;
                                    var7.anInt1893 -= var4.anInt1865;
                                    if (var4.anInt1861 > var7.anInt1894) {
                                        if (-1 >= ~var7.anInt1893
                                                && ~var7.anInt1893 > ~var4.frames.length) {
                                            Class1007.method1470(
                                                    var0.x,
                                                    var4,
                                                    183921384,
                                                    var0.y,
                                                    Class945.thisClass946 == var0,
                                                    var7.anInt1893);
                                        } else {
                                            var0.aClass145Array2809[var6] = null;
                                        }
                                    } else {
                                        var0.aClass145Array2809[var6] = null;
                                    }
                                }

                                var7.anInt1891 = 1 + var7.anInt1893;
                                if (var4.frames.length <= var7.anInt1891) {
                                    var7.anInt1891 -= var4.anInt1865;
                                    if (1 + var7.anInt1894 < var4.anInt1861) {
                                        if (-1 < ~var7.anInt1891
                                                || var4.frames.length <= var7.anInt1891) {
                                            var7.anInt1891 = -1;
                                        }
                                    } else {
                                        var7.anInt1891 = -1;
                                    }
                                }
                            } else {
                                var0.aClass145Array2809[var6] = null;
                            }
                        } else {
                            --var7.anInt1900;
                        }
                    }
                }

            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "ck.F("
                    + (var0 != null ? "{...}" : "null") + ',' + var1 + ')');
        }
    }

    public static final void method816(int i, Class988 class28,
                                       Class1009 class18_sub1, int i_5_) {
        Class1028 node = new Class1028();
        node.Class1009 = class18_sub1;
        node.class988 = class28;
        node.anInt1246 = i_5_;
        node.hash = (long) i;
        synchronized (Class922.updateServerList) {
            Class922.updateServerList.insertBack(node);
        }
        Class922.method568();
    }

    public static final void method37(Class988 class28, byte[] is, int i) {
        Class1028 node = new Class1028();
        node.hash = (long) i;
        node.buffer = is;
        node.anInt1246 = 0;
        node.class988 = class28;
        synchronized (Class922.updateServerList) {
            Class922.updateServerList.insertBack(node);
        }
        Class922.method568();
    }

    public static final Class1008 combinejStrings(Class1008[] var0) {
        if (-3 >= ~var0.length) {

            for (int i = 0; i < var0.length; i++) {
                String old = var0[i].toString();
                //System.out.println(old);
                if (old.contains("Impact")) {
                    old = old.replace("Impact", Class929.aString1088);
                    var0[i] = Class943.create(old);
                }
            }

            return Class67.method1261(0, var0.length, var0, 2774);
        } else {
            throw new IllegalArgumentException();
        }
    }

    protected static String cursorInfo[] = {
            "Walk-to",
            "Take",
            "Use",
            "Talk-to",
            "Pointless",
            "Net",
            "Bait",
            "Cage",
            "Harpo",
            "Chop down",
            "Bury",
            "Pray-at",
            "Mine",
            "Eat",
            "Drink",
            "Wield",
            "Wear",
            "aweeeeeeeeeeee",
            // "Remove",
            "Pointless",
            "Enter",
            "Logoutdisabled",
            "Climb-up",
            "Climb-down",
            "Search",
            "Steal",
            "Smelt",
            "Clean",
            // clean is 26
            "Back", "Deposit Bank", "Inspect", "Pickpocket", "Zoom",
            "Pointless", "Class944", "Pointless", "Pointless", "Accept",
            "Decline", "Cast Ice Barrage", "Cast Blood Barrage",
            "Cast Shadow Barrage", "Cast Smoke Barrage", "Cast Ice Blitz",
            "Cast Blood Blitz", "Cast Shadow Blitz", "Cast Smoke Blitz",
            "Cast Ice Burst", "Cast Blood Burst", "Cast Shadow Burst",
            "Cast Smoke Burst", "Cast Ice Rush", "Cast Blood Rush",
            "Cast Shadow Rush", "Cast Smoke Rush", "Link", "Split Private",
            "Graphics", "Audio", "Pointless", "Pointless", "Choose",
            "Informati", "Cast High Level Alchemy", "Cast Low Level Alchemy",
            "Pointless", "Select Starter", "Craft-rune", "World Map",
            "Withdraw", "Slash Web", "Pull", "Cast Superheat Item",
            "Cast Wind strike", "Cast Earth strike", "Cast Fire strike",
            "Cast Water strike", "Cast Earth bolt", "Cast Wind bolt",
            "Cast Water bolt", "Cast Fire bolt", "Cast Wind blast",
            "Cast Water blast", "Cast Earth blast", "Cast Fire blast",
            "Cast Wind wave", "Cast Water wave", "Cast Earth wave",
            "Cast Fire wave", "Cast Magic Dart", "Cast Flames of Zamorak",
            "Cast Claws of Guthix", "Cast Saradomin strike", "Cast Tele Block",
            "Cast Teleother Lumbridge", "Cast Teleother Falador",
            "Cast Teleother Camelot", "Cast Crumble undead",
            "Cast Charge water orb", "Cast Charge earth orb",
            "Cast Charge fire orb", "Cast Charge air orb", "Dig",
            "Cast Enchant Lvl-1 Jewelry", "Cast Enchant Lvl-2 Jewelry",
            "Cast Enchant Lvl-3 Jewelry", "Cast Enchant Lvl-4 Jewelry",
            "Cast Enchant Lvl-5 Jewelry", "Cast Enchant Lvl-6 Jewelry",
            "Cast Telekinetic grab", "Cast Bind", "Cast Snare",
            "Cast Entangle", "aw123",
            // "Cast Confuse",
            "Attack", "Close",};

    protected static int hoverTab = -1;

    protected static String lastAction;

    public static boolean detectCursor(String action) {
        int mouseX = Class922.mouseX - 3;
        int mouseY = Class922.mouseY;
        if (mouseX == -1 || mouseY == -1) //TODO: set default?
            return false;
        if (!Class929.newCursors) {
            Class122.setDefaultCursor();
            if (Class929.aInteger_510 == 562 && clientSize == 0)
                checkTabHover(action);
            return false;
        }

        boolean found = false;

        action = action.replaceAll("\\<.*?\\>", "");

        if (action.equals(lastAction)) {
            return false;
        }


        if (action.contains("Walk here") || action == "") {
            lastAction = action;
            Class122.setCursor(0);
            return false;
        }
        //	System.out.println("action: " + action);
        /*
         * if(action.startsWith("Cast")) useAction2 = true;
         *
         * if(useAction2) { String noHtml1 = action.replaceAll("\\<.*?\\>", "");
         * String noHtml2 = action2.replaceAll("\\<.*?\\>", ""); String noHtml3
         * = action3.replaceAll("\\<.*?\\>", ""); action = noHtml1 + noHtml2 +
         * noHtml3; System.out.println("action: " + action); }
         */

        for (int i2 = 0; i2 < cursorInfo.length; i2++) {
            if (found)
                continue;
            if (action.startsWith(cursorInfo[i2])) {
                found = true;
                Class122.setCursor(i2);
            }
        }
        if (!found)
            if (action.startsWith("Cast")) {
                Class122.setCursor(115);
                found = true;
            } else if (action.startsWith("Close")) {
                Class122.setCursor(29);
                found = true;
            }
        if (!found) {
            Class122.setCursor(0);
            lastAction = "";
            if (Class929.aInteger_510 == 562 && clientSize == 0)
                checkTabHover(action);
        }
        if (found) {
            Class922.lastAction = action;
			/*client.drawingCursor = true;
			client.setCursor = false;*/
/*			client.lastCursorX = client.mouseX;
			client.lastCursorY = client.mouseY;
			client.lastDraw = System.currentTimeMillis();*/
            // if (!Class1012.aBoolean_617) {
            // return false;
            // jInterface invy = Class7.getInterface(35913810);
            // if (invy != null)
            // Class20.refreshInterface(invy);
            // else
            // return false;
            // }
            return true;
        }
        return false;
    }

    private static boolean checkTabHover(String action) {
        if (action.equals("Combat Options")) {
            hoverTab = 1;
            return false;
        } else if (action.equals("Stats")) {
            hoverTab = 2;
            return false;
        } else if (action.equals("Quest List")) {
            hoverTab = 3;
            return false;
        } else if (action.equals("Inventory")) {
            hoverTab = 4;
            return false;
        } else if (action.equals("Worn Equipment")) {
            hoverTab = 5;
            return false;
        } else if (action.equals("Prayer")) {
            hoverTab = 6;
            return false;
        } else if (action.equals("Magic")) {
            hoverTab = 7;
            return false;
        } else if (action.equals("Clan Chat")) {
            hoverTab = 8;
            return false;
        } else if (action.toLowerCase().contains("friend list")) {
            hoverTab = 9;
            return false;
        } else if (action.toLowerCase().contains("ignore list")) {
            hoverTab = 10;
            return false;
        } else if (action.equals("Logout")) {
            hoverTab = 11;
            return false;
        } else if (action.equals("Options")) {
            hoverTab = 12;
            return false;
        } else if (action.equals("Emotes")) {
            hoverTab = 13;
            return false;
        } else if (hoverTab != -1)
            hoverTab = -1;
        if (hoverTab == -1)
            return false;
        return true;
    }

    protected static boolean resetBankScroller;
    protected static final int BANK_ITEMS = 786439;
    protected static final int BANK_TAB_HEADER_ITEMS = 786545;
    protected static int newScrollerToReset = -1;

    protected static boolean censorOn;
    protected static boolean dialogueUp, hideFSTabs;
    protected static boolean leftClickAttack = true;

    protected static final boolean debug_object_models = true;

    public static final Class957 method602(int var0, int var1, Class1027 var3) {
        if (spriteExists(var3, var0, var1)) {
            return method1062();
        } else {
            return null;
        }
    }

    public static final Class957 method1062() {
        byte[] var2 = Class163_Sub1.spritePaletteIndicators[0];
        int var1 = Class1013.spriteWidths[0]
                * Class3_Sub13_Sub6.spriteHeights[0];
        int[] var3 = new int[var1];
        for (int var4 = 0; ~var1 < ~var4; ++var4) {
            var3[var4] = Class3_Sub13_Sub38.spritePalette[Class951
                    .method633(var2[var4], 255)];
        }
        Object var6;
        if (!Class1012.aBoolean_617) {
            var6 = new Class1206_2(Class3_Sub15.spriteTrimWidth,
                    Class974.spriteTrimHeight,
                    Class164.spriteXOffsets[0], ByteBuffer.aInteger1259[0],
                    Class1013.spriteWidths[0],
                    Class3_Sub13_Sub6.spriteHeights[0], var3);
        } else {
            var6 = new Class1011(Class3_Sub15.spriteTrimWidth,
                    Class974.spriteTrimHeight,
                    Class164.spriteXOffsets[0], ByteBuffer.aInteger1259[0],
                    Class1013.spriteWidths[0],
                    Class3_Sub13_Sub6.spriteHeights[0], var3);
        }
        resetSprites();
        return (Class957) var6;
    }

    static final Class1019 constructFont(byte[] var1) {
        if (null != var1) {
            Object var2;
            if (!Class1012.aBoolean_617) {
                var2 = new Class1019_3(var1, Class164.spriteXOffsets,
                        ByteBuffer.aInteger1259, Class1013.spriteWidths,
                        Class3_Sub13_Sub6.spriteHeights,
                        Class163_Sub1.spritePaletteIndicators);
            } else {
                var2 = new Class1024(var1, Class164.spriteXOffsets,
                        ByteBuffer.aInteger1259, Class1013.spriteWidths,
                        Class3_Sub13_Sub6.spriteHeights,
                        Class163_Sub1.spritePaletteIndicators);
            }
            resetSprites();
            return (Class1019) var2;
        } else {
            return null;
        }
    }

    /*
     * static final Class1019 constructNewFont(byte[] var1) { if (null != var1) {
     * Object var2; if (!Class1012.aBoolean_617) { var2 = new Class1019_3(var1,
     * Class164.spriteXOffsets, ByteBuffer.aInteger1259, Class1013.spriteWidths,
     * Class3_Sub13_Sub6.spriteHeights, Class163_Sub1.spritePaletteIndicators);
     * } else { var2 = new Class1024(var1, Class164.spriteXOffsets,
     * ByteBuffer.aInteger1259, Class1013.spriteWidths,
     * Class3_Sub13_Sub6.spriteHeights, Class163_Sub1.spritePaletteIndicators);
     * } resetSprites(); return (Class1019) var2; } else { return null; } }
     */

    public static final void method75(int i, Class1009 class18_sub1, Class988 class28) {
        byte[] is = null;
        synchronized (updateServerList) {
            for (Class1028 class3_sub10 = (Class1028) updateServerList
                    .getFirst(); class3_sub10 != null; class3_sub10 = (Class1028) updateServerList
                    .getNext()) {
                if ((long) i == class3_sub10.hash
                        && class28 == class3_sub10.class988
                        && class3_sub10.anInt1246 == 0) {
                    is = class3_sub10.buffer;
                    break;
                }
            }
        }
        if (is != null)
            class18_sub1.method596(is, true, i, class28);
        else {
            byte[] is_28_ = class28.get(i);
            class18_sub1.method596(is_28_, true, i, class28);
        }
    }

    public static final void resetSprites() {
        Class1013.spriteWidths = null;
        ByteBuffer.aInteger1259 = null;
        Class3_Sub13_Sub6.spriteHeights = null;
        Class163_Sub1.spritePaletteIndicators = (byte[][]) null;
        Class164.spriteXOffsets = null;
        Class3_Sub13_Sub38.spritePalette = null;
    }

    public static final int method117(int i_0_, int i_1_) {
        long l = (long) ((i_0_ << 16) + i_1_);
        if (Class1006.aClass3_Sub3_Sub13_128 == null
                || l != Class1006.aClass3_Sub3_Sub13_128.hash)
            return 0;
        return 1 + ((Class1006.aClass3_Sub12_1448.offset) * 99 / (-(Class1006.aClass3_Sub3_Sub13_128.padding) + Class1006.aClass3_Sub12_1448.buffer.length));
    }

    static final void method568() {
        synchronized (anObject821) {
            if (anInt1465 == 0)
                Class38.gameClass942.startThread(new Class1044(), 5);
            anInt1465 = 600;
        }
    }

    public static final boolean spriteExists(Class1027 var0, int sub_id, int id) {
        byte[] var4 = var0.getFile(id, 0);
        if (var4 != null) {
            Class45.decodeSprites(var4);
            return true;
        } else {
            return false;
        }
    }

    public static final void loadFonts(Class1027 var0, int var1, Class1027 sprite_cache) {
        try {
            setSmallClass1019(fetchFont(0, Class1025.fontP11Id, sprite_cache, var0));

            if (!Class1012.aBoolean_617) {
                Class157.aClass3_Sub28_Sub17_Sub1_2000 = (Class1019_3) getSmallClass1019();
            } else {
                Class157.aClass3_Sub28_Sub17_Sub1_2000 = Class70.method1287(
                        Class1025.fontP11Id, 0, var0, sprite_cache, -1);
            }
            setRegularClass1019(fetchFont(var1, Class75_Sub2.fontP12Id,
                    sprite_cache, var0));

            setBoldClass1019(fetchFont(0, Class3_Sub13_Sub11.fontB12Id,
                    sprite_cache, var0));

        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "hn.D("
                    + (var0 != null ? "{...}" : "null") + ',' + var1 + ','
                    + (sprite_cache != null ? "{...}" : "null") + ')');
        }
    }

    public static final Class1019 fetchFont(int index, int file, Class1027 var3, Class1027 var4) {
        if (!Class922.spriteExists(var3, 0, file)) {
            return null;
        }
        return Class922.constructFont(var4.getFile(file, 0));
    }

    /*
     * static final Class1019 fetchFontNew(int index, int file) { return
     * client.constructFont
     * (Class140_Sub6.cacheIndex8.getFile(Class140_Sub6.cacheIndex8
     * .getSpriteGroupId(Class1008.create("newsmallfont")), 0)); }
     */

    /*
     * static final Class1019 fetchNewFont(int index, int file, Class1027 var3, Class1027 var4) {
     * //TODO: if the file is for hit or crit font, length is 58 not 256 if
     * (!client.spriteExists(var3, 0, file)) { return null; } return
     * client.constructFont(var4.getFile(file, 0), 58); //58 length }
     */

    public static final void drawTextOnScreen(Class1008 var2, boolean var1) {
        // if(var2 == Class3_Sub13_Sub23.loadingString) {
        // var2 = client.combinejStrings(new
        // Class1008[]{Class3_Sub13_Sub23.loadingString,
        // Class1008.create("If this is ur first time loading region..")});
        // }
        // TODO: make box bigger 4 dat and add modelInfoCount or whatever
        byte var3 = 4;
        int var4 = var3 + 6;
        int var5 = var3 + 6;
        int var6 = getRegularClass1019().method680(var2, 250);
        int var7 = getRegularClass1019().method684(var2, 250) * 13;
        if (!Class1012.aBoolean_617) {
            Class1023.fillRect(var4 - var3, -var3 + var5, var3 + var6 - -var3,
                    var3 + var3 + var7, 0);
            Class1023.drawRect(var4 + -var3, var5 - var3, var3 + var3 + var6,
                    var3 + var3 + var7, 16777215);
        } else {
            Class920.method_574(var4 - var3, -var3 + var5, var3 + var6 - -var3,
                    var3 + var3 + var7, 0);
            Class920.method_475(-var3 + var4, -var3 + var5,
                    var6 + var3 - -var3, var3 + var7 + var3, 16777215);
        }

        getRegularClass1019().method676(var2, var4, var5, var6, var7, 16777215, -1,
                1, 1, 0);
        Class75.method1340(var4 + -var3, var6 + (var3 - -var3), -var3 + var5,
                var3 + var7 + var3);
        if (var1) {
            if (Class1012.aBoolean_617) {
                Class1012.method1826();
            } else {
                try {
                    Graphics var8 = Class1143.canvas.getGraphics();
                    Class164_Sub1.aClass158_3009.drawGraphics(var8, 0, 0);
                } catch (Exception var9) {
                    Class1143.canvas.repaint();
                }
            }
        } else {
            Class1043.method1282(var4, var5, var7, var6);
        }
    }

    public static Class1019 getSmallClass1019() {
        return smallClass1019;
    }

    static final void method665() {
        synchronized (anObject821) {
            if (anInt1465 != 0) {
                anInt1465 = 1;
                try {
                    anObject821.wait();
                } catch (InterruptedException interruptedexception) {
                    /* empty */
                }
            }
        }
    }

    public static void setSmallClass1019(Class1019 class1019) {
        smallClass1019 = class1019;
    }

    public static Class1019 getBoldClass1019() {
        return boldClass1019;
    }

    public static void setBoldClass1019(Class1019 class1019) {
        boldClass1019 = class1019;
    }

    public static Class1019 getRegularClass1019() {
        return regularClass1019;
    }

    public static void setRegularClass1019(Class1019 class1019) {
        regularClass1019 = class1019;
    }

    // 947 - lowercase 562 font
    // 948 - uppercase 562 font
    // 949 - bold
    // 950 - fancy
    static final void handleRequests() {
        for (; ; ) {
            Class1028 node;
            synchronized (Class922.updateServerList) {
                node = (Class1028) Class922.aClass60_2164.popFront();
            }
            if (node == null)
                break;
            node.Class1009.method596(node.buffer, false, (int) node.hash,
                    node.class988);
        }
    }

    public static int getCompletion() {
        int percent = 0;

        percent += Class75_Sub3.cacheIndex0.getTotalCompletion() * 4 / 100;
        percent += Class3_Sub28_Sub19.cacheIndex1.getTotalCompletion() * 4 / 100;
        percent += Class164.cacheIndex2.getTotalCompletion() * 2 / 100;
        percent += Class140_Sub3.cacheIndex3.getTotalCompletion() * 2 / 100;
        percent += Class961.cacheIndex4.getTotalCompletion() * 6 / 100;
        percent += Class3_Sub13_Sub6.cacheIndex5.getTotalCompletion() * 4 / 100;
        percent += Class75_Sub2.cacheIndex6.getTotalCompletion() * 2 / 100;
        percent += Class159.cacheIndex7.getTotalCompletion() * 60 / 100;
        percent += Class140_Sub6.cacheIndex8.getTotalCompletion() * 2 / 100;
        percent += Class3_Sub13_Sub28.cacheIndex9.getTotalCompletion() * 2 / 100;
        percent += Class3_Sub13_Sub25.cacheIndex10.getTotalCompletion() * 2 / 100;
        percent += Class1002.cacheIndex11.getTotalCompletion() * 2 / 100;
        percent += Class971.cacheIndex12.getTotalCompletion() * 2 / 100;
        percent += Class1027.cacheIndex13.getTotalCompletion() * 2 / 100;
        percent += Class0.cacheIndex14.getTotalCompletion() * 2 / 100;
        percent += Class1001.cacheIndex15.getTotalCompletion() * 2 / 100;
        return percent;
    }

    public static int tabID = 3;

    public static final void updateGroundTexturing() {
        aBoolean_609 = !aBoolean_609;
        Class1134.aClass93_725.clearAll();
        setaInteger_544(25);
        closeSelect();
    }

}

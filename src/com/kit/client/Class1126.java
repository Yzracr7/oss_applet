package com.kit.client;

import java.io.IOException;
import java.net.Socket;

class Class1126 {

    protected static int[] anIntArray1679 = new int[14];
    protected static Class1027 aClass153_1680;
    protected static int[] anIntArray1681;

    static final void login(byte var0) {
        try {
            if (0 != Class3_Sub13_Sub25.loginStage && 5 != Class3_Sub13_Sub25.loginStage) {
                try {
                    if (~(++Class1209.anInt820) < -2001) {
                        System.out.println("Error login stage -2001 | Possible timeout");
                        if (Class3_Sub15.worldConnection != null) {
                            Class3_Sub15.worldConnection.close();
                            Class3_Sub15.worldConnection = null;
                        }

                        if (-2 >= ~Class166.anInt2079) {
                            Class923.returnCode = -5;
                            Class3_Sub13_Sub25.loginStage = 0;
                            return;
                        }

                        Class1209.anInt820 = 0;
                        if (Class140_Sub6.anInt2894 != Class162.anInt2036) {
                            Class140_Sub6.anInt2894 = Class162.anInt2036;
                        } else {
                            Class140_Sub6.anInt2894 = Class26.anInt506;
                        }

                        Class3_Sub13_Sub25.loginStage = 1;
                        ++Class166.anInt2079;
                    }

                    if (Class3_Sub13_Sub25.loginStage == 1) {
                        Class3_Sub9.aClass64_2318 = Class38.gameClass942.startConnection(Class38_Sub1.aString2611, Class140_Sub6.anInt2894);
                        Class3_Sub13_Sub25.loginStage = 2;
                    }

                    if (-3 == ~Class3_Sub13_Sub25.loginStage) {
                        if (~Class3_Sub9.aClass64_2318.status == -3) {
                            throw new IOException();
                        }

                        if (1 != Class3_Sub9.aClass64_2318.status) {
                            return;
                        }

                        Class3_Sub15.worldConnection = new Class1007((Socket) Class3_Sub9.aClass64_2318.value, Class38.gameClass942);
                        Class3_Sub9.aClass64_2318 = null;
                        long var1 = Class3_Sub13_Sub16.aLong3202 = Class922.username.toLong();
                        Class3_Sub13_Sub1.outputStream.offset = 0;
                        Class3_Sub13_Sub1.outputStream.aBlowMe(14);
                        int var3 = (int) (var1 >> 16 & 31L);
                        Class3_Sub13_Sub1.outputStream.aBlowMe(var3);
                        Class3_Sub15.worldConnection.write(Class3_Sub13_Sub1.outputStream.buffer, 0, 2);
                        if (Class1228.aClass155_2627 != null) {
                            Class1228.aClass155_2627.method2159();
                        }

                        if (Class3_Sub21.aClass155_2491 != null) {
                            Class3_Sub21.aClass155_2491.method2159();
                        }

                        int var4 = Class3_Sub15.worldConnection.read();
                        if (Class1228.aClass155_2627 != null) {
                            Class1228.aClass155_2627.method2159();
                        }

                        if (null != Class3_Sub21.aClass155_2491) {
                            Class3_Sub21.aClass155_2491.method2159();
                        }

                        if (~var4 != -1) {
                            Class923.returnCode = var4;
                            Class3_Sub13_Sub25.loginStage = 0;
                            Class3_Sub15.worldConnection.close();
                            Class3_Sub15.worldConnection = null;
                            return;
                        }

                        Class3_Sub13_Sub25.loginStage = 3;
                    }

                    if (Class3_Sub13_Sub25.loginStage == 3) {
                        if (~Class3_Sub15.worldConnection.available() > -9) {
                            return;
                        }

                        Class3_Sub15.worldConnection.read(Class1211.incomingPackets.buffer, 0, 8);
                        Class1211.incomingPackets.offset = 0;
                        Class3_Sub13_Sub27.aLong3338 = Class1211.incomingPackets.aVar100();
                        int[] var9 = new int[4];
                        Class3_Sub13_Sub1.outputStream.offset = 0;
                        var9[2] = (int) (Class3_Sub13_Sub27.aLong3338 >> 32);
                        var9[3] = (int) Class3_Sub13_Sub27.aLong3338;
                        var9[1] = (int) (Math.random() * 9.9999999E7D);
                        var9[0] = (int) (Math.random() * 9.9999999E7D);
                        Class3_Sub13_Sub1.outputStream.aBlowMe(10);
                        Class3_Sub13_Sub1.outputStream.method_211(var9[0]);
                        Class3_Sub13_Sub1.outputStream.method_211(var9[1]);
                        Class3_Sub13_Sub1.outputStream.method_211(var9[2]);
                        Class3_Sub13_Sub1.outputStream.method_211(var9[3]);
                        Class3_Sub13_Sub1.outputStream.method_211(0);
                        Class3_Sub13_Sub1.outputStream.aInt_322(Class922.username);
                        Class3_Sub13_Sub1.outputStream.aInt_322(Class922.password);
                        Class3_Sub13_Sub1.outputStream.aBoolean1056(Class3_Sub13_Sub14.aBigInteger3162, Class3_Sub13_Sub37.aBigInteger3441, -296);
                        Class1009.aClass3_Sub30_Sub1_2942.offset = 0;
                        if (40 == Class922.aInteger_544) {
                            Class1009.aClass3_Sub30_Sub1_2942.aBlowMe(18);
                        } else {
                            Class1009.aClass3_Sub30_Sub1_2942.aBlowMe(16);
                        }
                        Class1009.aClass3_Sub30_Sub1_2942.aBlowMe(Class3_Sub13_Sub1.outputStream.offset + 69);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(464);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class929.aInteger_510);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(464);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class929.orbsToggled ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class929.newHealthbars ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class929.newCursors ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class929.newMenus ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class929.newHits ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class3_Sub26.forceTweeningEnabled ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class59.hdEnabled ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class922.hdOnLogin ? 1 : 0);
                        int output = Class922.clientSize > 0 ? Class922.clientSize : Class922.startSize > 0 ? Class922.startSize : Class922.clientSize;
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(output);
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class922.censorOn ? 1 : 0);
                        Class1009.aClass3_Sub30_Sub1_2942.aBlowMe(1);
                        Class1212.aBoolean2705 = true;
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class75_Sub3.cacheIndex0.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class3_Sub28_Sub19.cacheIndex1.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class164.cacheIndex2.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class140_Sub3.cacheIndex3.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class961.cacheIndex4.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class3_Sub13_Sub6.cacheIndex5.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class75_Sub2.cacheIndex6.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class159.cacheIndex7.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class140_Sub6.cacheIndex8.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class3_Sub13_Sub28.cacheIndex9.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class3_Sub13_Sub25.cacheIndex10.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class1002.cacheIndex11.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class971.cacheIndex12.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class1027.cacheIndex13.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class0.cacheIndex14.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method_211(Class1001.cacheIndex15.method2118());
                        Class1009.aClass3_Sub30_Sub1_2942.method753(Class3_Sub13_Sub1.outputStream.buffer, 0, Class3_Sub13_Sub1.outputStream.offset, var0 + 117);
                        Class3_Sub15.worldConnection.write(Class1009.aClass3_Sub30_Sub1_2942.buffer, 0, Class1009.aClass3_Sub30_Sub1_2942.offset);
                        Class3_Sub13_Sub1.outputStream.initIsaac(var9);
                        for (int var2 = 0; ~var2 > -5; ++var2) {
                            var9[var2] += 50;
                        }
                        Class1211.incomingPackets.initIsaac(var9);
                        Class3_Sub13_Sub25.loginStage = 4;
                    }
                    if (-5 == ~Class3_Sub13_Sub25.loginStage) {
                        if (~Class3_Sub15.worldConnection.available() > -2) {
                            return;
                        }
                        int var10 = Class3_Sub15.worldConnection.read();
                        if (~var10 != -22) {
                            if (var10 != 29) {
                                if (var10 == 1) {
                                    Class3_Sub13_Sub25.loginStage = 5;
                                    Class923.returnCode = var10;
                                    return;
                                }

                                if (2 != var10) {
                                    if (~var10 != -16) {
                                        if (23 == var10 && ~Class166.anInt2079 > -2) {
                                            Class3_Sub13_Sub25.loginStage = 1;
                                            ++Class166.anInt2079;
                                            Class1209.anInt820 = 0;
                                            Class3_Sub15.worldConnection.close();
                                            Class3_Sub15.worldConnection = null;
                                            return;
                                        }

                                        Class923.returnCode = var10;
                                        Class3_Sub13_Sub25.loginStage = 0;
                                        Class3_Sub15.worldConnection.close();
                                        Class3_Sub15.worldConnection = null;
                                        return;
                                    }

                                    Class3_Sub13_Sub25.loginStage = 0;
                                    Class923.returnCode = var10;
                                    return;
                                }

                                Class3_Sub13_Sub25.loginStage = 8;
                            } else {
                                Class3_Sub13_Sub25.loginStage = 10;
                            }
                        } else {
                            Class3_Sub13_Sub25.loginStage = 7;
                        }
                    }

                    if (6 == Class3_Sub13_Sub25.loginStage) {
                        //Class3_Sub13_Sub1.outputStream.offset = 0;
                        //Class3_Sub13_Sub1.outputStream.putPacket(17);
                        //Class3_Sub15.worldConnection.write(Class3_Sub13_Sub1.outputStream.buffer, 0, Class3_Sub13_Sub1.outputStream.offset);
                        Class3_Sub13_Sub25.loginStage = 4;
                        return;
                    }

                    if (Class3_Sub13_Sub25.loginStage == 7) {
                        if (-2 >= ~Class3_Sub15.worldConnection.available()) {
                            Class3_Sub13_Sub34.anInt3413 = 60 * (3 + Class3_Sub15.worldConnection.read());
                            Class3_Sub13_Sub25.loginStage = 0;
                            Class923.returnCode = 21;
                            Class3_Sub15.worldConnection.close();
                            Class3_Sub15.worldConnection = null;
                            return;
                        }

                        return;
                    }

                    if (-11 == ~Class3_Sub13_Sub25.loginStage) {
                        if (1 <= Class3_Sub15.worldConnection.available()) {
                            Class3_Sub26.anInt2561 = Class3_Sub15.worldConnection.read();
                            Class3_Sub13_Sub25.loginStage = 0;
                            Class923.returnCode = 29;
                            Class3_Sub15.worldConnection.close();
                            Class3_Sub15.worldConnection = null;
                            return;
                        }

                        return;
                    }

                    if (Class3_Sub13_Sub25.loginStage == 8) {
                        if (~Class3_Sub15.worldConnection.available() > -15) {
                            return;
                        }

                        Class3_Sub15.worldConnection.read(Class1211.incomingPackets.buffer, 0, 8);
                        Class1211.incomingPackets.offset = 0;
                        Class3_Sub13_Sub26.anInt3320 = Class1211.incomingPackets.readUnsignedByte();
                        Class3_Sub28_Sub19.anInt3775 = Class1211.incomingPackets.readUnsignedByte();
                        //Class3_Sub15.aBoolean2433 = Class1211.aClass3_Sub30_Sub1_532.readUnsignedByte() == 1;
                        //Class121.aBoolean1641 = 1 == Class1211.aClass3_Sub30_Sub1_532.readUnsignedByte();
                        //Class3_Sub28_Sub10_Sub1.aBoolean4063 = ~Class1211.aClass3_Sub30_Sub1_532.readUnsignedByte() == -2;
                        //Class3_Sub13_Sub14.aBoolean3166 = 1 == Class1211.aClass3_Sub30_Sub1_532.readUnsignedByte();
                        //Class970.aBoolean29 = Class1211.aClass3_Sub30_Sub1_532.readUnsignedByte() == 1;
                        Class971.anInt2211 = Class1211.incomingPackets.aInteger233();
                        Class3_Sub13_Sub29.aBoolean3358 = Class1211.incomingPackets.readUnsignedByte() == 1;
                        Class2.allowMembers = true;
                        Class113.method1702(Class2.allowMembers);
                        Class8.method845(Class2.allowMembers);
                  /*if(!Class3_Sub28_Sub19.advertSuppressed) {
                     if((!Class3_Sub15.aBoolean2433 || Class1028.aBoolean4063) && !Class3_Sub13_Sub29.aBoolean3358) {
                        try {
                           Class27.aClass94_516.method1577(Class38.gameClass942.thisApplet);
                        } catch (Throwable var5) {
                           ;
                        }
                     } else {
                        try {
                           Class97.aClass94_1374.method1577(Class38.gameClass942.thisApplet);
                        } catch (Throwable var6) {
                           ;
                        }
                     }
                  }*/

                        Class1008.incomingPacket = Class1211.incomingPackets.getPacket();
                        Class1017_2.anInt1704 = Class1211.incomingPackets.aInteger233();
                        Class3_Sub13_Sub25.loginStage = 9;
                    }

                    if (-10 == ~Class3_Sub13_Sub25.loginStage) {
                        if (~Class3_Sub15.worldConnection.available() > ~Class1017_2.anInt1704) {
                            return;
                        }
                        Class1211.incomingPackets.offset = 0;
                        Class3_Sub15.worldConnection.read(Class1211.incomingPackets.buffer, 0, Class1017_2.anInt1704);
                        Class923.returnCode = 2;
                        Class3_Sub13_Sub25.loginStage = 0;
                        Class954.resetLocalVariables(true);
                        Class956.anInt3606 = -1;
                        Class39.createRegion(false);
                        Class1008.incomingPacket = -1;
                        return;
                    }

                    if (var0 != -9) {
                        aClass153_1680 = (Class1027) null;
                    }
                } catch (IOException var7) {
                    if (null != Class3_Sub15.worldConnection) {
                        Class3_Sub15.worldConnection.close();
                        Class3_Sub15.worldConnection = null;
                    }

                    if (Class166.anInt2079 >= 1) {
                        Class3_Sub13_Sub25.loginStage = 0;
                        Class923.returnCode = -4;
                    } else {
                        Class3_Sub13_Sub25.loginStage = 1;
                        Class1209.anInt820 = 0;
                        ++Class166.anInt2079;
                        if (~Class162.anInt2036 == ~Class140_Sub6.anInt2894) {
                            Class140_Sub6.anInt2894 = Class26.anInt506;
                        } else {
                            Class140_Sub6.anInt2894 = Class162.anInt2036;
                        }
                    }
                }

            }
        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "ri.A(" + var0 + ')');
        }
    }

    static final int method935(int var0, int var1) {
        var1 = var1 * (var0 & 127) >> 7;
        if (var1 < 2) {
            var1 = 2;
        } else if (var1 > 126) {
            var1 = 126;
        }

        return (var0 & '\uff80') + var1;
    }

    public static void method1754(int var0) {
        try {
            aClass153_1680 = null;
            anIntArray1679 = null;
            if (var0 >= -49) {
                login((byte) 102);
            }

            anIntArray1681 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ri.B(" + var0 + ')');
        }
    }

}

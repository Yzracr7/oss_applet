package com.kit.client;

class Class38 {

    protected static int anInt660;
    protected static boolean aBoolean661 = true;
    protected static Class1008 aClass94_662 = Class943.create("Please remove ");
    protected static int[][] anIntArrayArray663;
    protected static int[] anIntArray664 = new int[14];
    protected static Class942 gameClass942;
    protected static Class1008 aClass94_666 = Class943.create("Please remove ");
    protected static Class961 mouseWheelHandler;

    public static void method1024() {
        anIntArray664 = null;
        anIntArrayArray663 = (int[][]) null;
        mouseWheelHandler = null;
        gameClass942 = null;
        aClass94_662 = null;
        aClass94_666 = null;
    }

    static final void method1025() {
        Class1207.aClass93_2604.clearSoftReference();
        Class27.aClass93_511.clearSoftReference();
    }

    static final int method1026(byte[] var0, int var1, boolean var2) {
        try {
            return var2 ? -3 : Class99.method1599(0, var1, var0);
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "fk.H(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ',' + var2 + ')');
        }
    }

    static final void method1027(int var0, byte var1) {
        try {
            Class1134.aClass93_725.method1522(var0);
            int var2 = -38 % ((var1 - 29) / 40);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "fk.J(" + var0 + ',' + var1 + ')');
        }
    }

    static final void method1028(int var0) {
        try {
            for (int var1 = -1; ~var1 > ~Class159.anInt2022; ++var1) {
                int var2;
                if (0 == ~var1) {
                    var2 = 2047;
                } else {
                    var2 = Class922.playerIndices[var1];
                }

                Class946 var3 = Class922.class946List[var2];
                if (var3 != null) {
                    Class986.method68(var3.getSize(), 2279, var3);
                }
            }

            if (var0 >= -3) {
                aClass94_666 = (Class1008) null;
            }

        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "fk.G(" + var0 + ')');
        }
    }

}

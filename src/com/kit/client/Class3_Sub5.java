package com.kit.client;

final class Class3_Sub5 extends Class1042 {

    protected int anInt2266;
    protected int anInt2268;
    protected int anInt2270;
    protected int anInt2271;
    protected int anInt2272;
    protected int anInt2273;
    protected static int anInt2275 = 1;
    protected int anInt2277;
    protected int anInt2278;
    protected int anInt2279;
    protected static int anInt2281 = 0;
    protected int anInt2282;
    protected int anInt2283;
    protected int anInt2284;
    protected static Class1008 aClass94_2267 = Class943.create("Starting 3d Library");

    static final void method112(byte var0, byte var1) {
        try {
            if (null == Class923.aByteArrayArrayArray2008) {
                Class923.aByteArrayArrayArray2008 = new byte[4][104][104];
            }

            if (var1 != 55) {
                anInt2281 = -87;
            }

            for (int var2 = 0; var2 < 4; ++var2) {
                for (int var3 = 0; ~var3 > -105; ++var3) {
                    for (int var4 = 0; 104 > var4; ++var4) {
                        Class923.aByteArrayArrayArray2008[var2][var3][var4] = var0;
                    }
                }
            }

        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "cn.A(" + var0 + ',' + var1 + ')');
        }
    }

    public static void method113(byte var0) {
        try {
            aClass94_2267 = null;
            int var1 = -100 % ((var0 - -43) / 46);
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "cn.D(" + var0 + ')');
        }
    }

    static final void method114(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        if (~var9 == ~var8 && var3 == var7 && var5 == var4 && var1 == var6) {
            Class1031.method1869((byte) 84, var2, var6, var7, var5, var9);
        } else {
            int var11 = var7;
            int var10 = var9;
            int var12 = var9 * 3;
            int var13 = 3 * var7;
            int var14 = var8 * 3;
            int var15 = var3 * 3;
            int var16 = var4 * 3;
            int var17 = var1 * 3;
            int var18 = var5 + -var16 + var14 + -var9;
            int var19 = -var7 + var6 - (var17 + -var15);
            int var20 = var12 + -var14 + -var14 + var16;
            int var21 = var13 + -var15 + var17 + -var15;
            int var22 = -var12 + var14;
            int var23 = var15 + -var13;

            for (int var24 = 128; ~var24 >= -4097; var24 += 128) {
                int var25 = var24 * var24 >> 12;
                int var26 = var24 * var25 >> 12;
                int var28 = var19 * var26;
                int var29 = var25 * var20;
                int var27 = var18 * var26;
                int var30 = var25 * var21;
                int var31 = var22 * var24;
                int var32 = var23 * var24;
                int var33 = (var31 + (var27 - -var29) >> 12) + var9;
                int var34 = var7 + (var32 + var28 + var30 >> 12);
                Class1031.method1869((byte) -119, var2, var34, var11, var33, var10);
                var10 = var33;
                var11 = var34;
            }
        }
    }

    static final int method115(boolean var0) {
        long var2 = Class1219.currentTimeMillis();
        for (Class419 var4 = !var0 ? (Class419) Class951.aClass130_3679.getNext() : (Class419) Class951.aClass130_3679.getFirst(); var4 != null; var4 = (Class419) Class951.aClass130_3679.getNext()) {
            if ((4611686018427387903L & var4.value) < var2) {
                if (~(4611686018427387904L & var4.value) != -1L) {
                    int var5 = (int) var4.hash;
                    Class163_Sub1.variousSettings[var5] = Class57.anIntArray898[var5];
                    var4.unlink();
                    return var5;
                }
                var4.unlink();
            }
        }
        return -1;
    }

    static final void method116(boolean var0, int var1) {
        try {
            int var2 = Class159.anInt2022;
            if (Class65.mapFlagX == Class945.thisClass946.y >> 7 && ~(Class945.thisClass946.x >> 7) == ~Class45.mapFlagY) {
                Class65.mapFlagX = 0;
            }

            if (var0) {
                var2 = 1;
            }

            int var3;
            Class946 var4;
            int var7;
            int var8;
            int var9;
            int var10;
            int var11;
            for (var3 = 0; var3 < var2; ++var3) {
                if (!var0) {
                    var4 = Class922.class946List[Class922.playerIndices[var3]];
                } else {
                    var4 = Class945.thisClass946;
                }

                if (null != var4 && var4.method1966((byte) 17)) {
                    int var5 = var4.getSize();
                    int var6;
                    if (-2 == ~var5) {
                        if ((127 & var4.y) == 64 && 64 == (127 & var4.x)) {
                            var6 = var4.y >> 7;
                            var7 = var4.x >> 7;
                            if (~var6 <= -1 && var6 < 104 && ~var7 <= -1 && -105 < ~var7) {
                                ++Class163_Sub1_Sub1.anIntArrayArray4010[var6][var7];
                            }
                        }
                    } else if (((1 & var5) != 0 || ~(var4.y & 127) == -1 && ~(127 & var4.x) == -1) && (~(1 & var5) != -2 || -65 == ~(var4.y & 127) && -65 == ~(127 & var4.x))) {
                        var6 = var4.y + -(var5 * 64) >> 7;
                        var7 = var4.x + -(var5 * 64) >> 7;
                        var8 = var4.getSize() + var6;
                        if (104 < var8) {
                            var8 = 104;
                        }

                        if (-1 < ~var6) {
                            var6 = 0;
                        }

                        var9 = var7 + var4.getSize();
                        if (var7 < 0) {
                            var7 = 0;
                        }

                        if (~var9 < -105) {
                            var9 = 104;
                        }

                        for (var10 = var6; ~var10 > ~var8; ++var10) {
                            for (var11 = var7; var11 < var9; ++var11) {
                                ++Class163_Sub1_Sub1.anIntArrayArray4010[var10][var11];
                            }
                        }
                    }
                }
            }
            if (var1 == 670232012) {
                label226:
                for (var3 = 0; ~var2 < ~var3; ++var3) {
                    long var16;
                    if (!var0) {
                        var4 = Class922.class946List[Class922.playerIndices[var3]];
                        var16 = (long) Class922.playerIndices[var3] << 32;
                    } else {
                        var4 = Class945.thisClass946;
                        var16 = 8791798054912L;
                    }

                    if (var4 != null && var4.method1966((byte) 17)) {
                        var4.aBoolean3968 = false;
                        if ((Class1034.manyIdleAnimations && -201 > ~Class159.anInt2022 || 50 < Class159.anInt2022) && !var0 && var4.currentMoveAnimation == var4.idleAnimation) {
                            var4.aBoolean3968 = true;
                        }

                        var7 = var4.getSize();
                        if (-2 == ~var7) {
                            if (64 == (127 & var4.y) && (127 & var4.x) == 64) {
                                var8 = var4.y >> 7;
                                var9 = var4.x >> 7;
                                if (~var8 > -1 || var8 >= 104 || ~var9 > -1 || 104 <= var9) {
                                    continue;
                                }

                                if (~Class163_Sub1_Sub1.anIntArrayArray4010[var8][var9] < -2) {
                                    --Class163_Sub1_Sub1.anIntArrayArray4010[var8][var9];
                                    continue;
                                }
                            }
                        } else if (~(1 & var7) == -1 && ~(127 & var4.y) == -1 && (var4.x & 127) == 0 || 1 == (1 & var7) && -65 == ~(127 & var4.y) && (var4.x & 127) == 0) {
                            var8 = var4.y + -(64 * var7) >> 7;
                            var10 = var7 + var8;
                            var9 = -(var7 * 64) + var4.x >> 7;
                            if (var10 > 104) {
                                var10 = 104;
                            }

                            if (0 > var8) {
                                var8 = 0;
                            }

                            var11 = var7 + var9;
                            if (~var9 > -1) {
                                var9 = 0;
                            }

                            boolean var12 = true;
                            if (var11 > 104) {
                                var11 = 104;
                            }
                            int var13;
                            int var14;
                            for (var13 = var8; ~var13 > ~var10; ++var13) {
                                for (var14 = var9; ~var11 < ~var14; ++var14) {
                                    if (-2 <= ~Class163_Sub1_Sub1.anIntArrayArray4010[var13][var14]) {
                                        var12 = false;
                                        break;
                                    }
                                }
                            }
                            if (var12) {
                                var13 = var8;

                                while (true) {
                                    if (~var13 <= ~var10) {
                                        continue label226;
                                    }

                                    for (var14 = var9; ~var11 < ~var14; ++var14) {
                                        --Class163_Sub1_Sub1.anIntArrayArray4010[var13][var14];
                                    }

                                    ++var13;
                                }
                            }
                        }
                        if (null != var4.anObject2796 && ~Class1134.loopCycle <= ~var4.anInt2797 && ~var4.anInt2778 < ~Class1134.loopCycle) {
                            var4.aBoolean3968 = false;
                            var4.anInt2831 = Class121.method1736(Class26.plane, 1, var4.y, var4.x);
                            Class3_Sub13_Sub25.method292(Class26.plane, var4.y, var4.x, var4.anInt2831, var4, var4.directionDegrees, var16, var4.anInt2788, var4.anInt2777, var4.anInt2818, var4.anInt2817);
                        } else {
                            var4.anInt2831 = Class121.method1736(Class26.plane, 1, var4.y, var4.x);
                            Class20.method907(Class26.plane, var4.y, var4.x, var4.anInt2831, 64 * (var7 - 1) + 60, var4, var4.directionDegrees, var16, var4.aBoolean2810);
                        }
                    }
                }
            }
        } catch (RuntimeException var15) {
            throw Class1134.method1067(var15, "cn.C(" + var0 + ',' + var1 + ')');
        }
    }
}

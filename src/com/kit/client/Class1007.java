package com.kit.client;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

final class Class1007 implements Runnable {

    private int tnum = 0;
    private OutputStream outputStream;
    private InputStream inputStream;
    private byte[] buffer;
    protected   static int anInt1234;
    private Socket socket;
    private int tcycle = 0;
    private Class1124 socketThread;
    static Class33 aClass33_1238;
    private Class942 class942;
    private boolean closed = false;
    protected   static Class975 aClass61_1242 = new Class975();
    private boolean IOError = false;
    protected  static int anInt1244 = -1;

    public final void run() {
        try {
            for (; ; ) {
                int off;
                int len;
                synchronized (this) {
                    if (tcycle == tnum) {
                        if (closed) {
                            break;
                        }
                        try {
                            this.wait();
                        } catch (InterruptedException var9) {

                        }
                    }

                    off = tcycle;
                    if (tnum >= tcycle) {
                        len = tnum - tcycle;
                    } else {
                        len = 5000 - tcycle;
                    }
                }

                if (len > 0) {
                    try {
                        outputStream.write(buffer, off, len);
                    } catch (IOException var8) {
                        IOError = true;
                    }

                    tcycle = (len + tcycle) % 5000;

                    try {
                        if (tcycle == tnum) {
                            outputStream.flush();
                        }
                    } catch (IOException var7) {
                        IOError = true;
                    }
                }
            }

            try {
                if (null != inputStream) {
                    inputStream.close();
                }

                if (null != outputStream) {
                    outputStream.close();
                }

                if (socket != null) {
                    socket.close();
                }
            } catch (IOException var6) {
                ;
            }

            buffer = null;
        } catch (Exception var11) {
            Class49.method1125((String) null, var11);
        }
    }

    static final void method1460(int var0, int var1, byte var2, int var3, int var4, int var5, int var6) {
        try {
            if (~(var5 - var4) <= ~Class101.anInt1425 && Class3_Sub28_Sub18.anInt3765 >= var5 - -var4 && Class159.anInt2020 <= -var4 + var1 && Class57.anInt902 >= var4 + var1) {
                Class3_Sub13_Sub2.method175(var6, var0, var1, true, var3, var4, var5);
            } else {
                Class1223.method2275(var3, (byte) 109, var1, var4, var6, var0, var5);
            }

            if (var2 > -107) {
                anInt1244 = 89;
            }

        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "ma.A(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ')');
        }
    }

    final void read(byte[] buf, int off, int len) throws IOException {
        if (!closed) {
            while (len > 0) {
                int avail = inputStream.read(buf, off, len);
                if (avail <= 0)
                    throw new EOFException();
                len -= avail;
                off += avail;
            }
        }
    }

    final int read() throws IOException {
        return !closed ? inputStream.read() : 0;
    }

    public static void method1463(int var0) {
        aClass61_1242 = null;
        aClass33_1238 = null;
    }

    final void write(byte[] buf, int off, int len) throws IOException {
        if (!closed) {
            if (IOError) {
                IOError = false;
                throw new IOException();
            }
            if (buffer == null) {
                buffer = new byte[5000];
            }

            synchronized (this) {
                for (int id = 0; id < len; ++id) {
                    buffer[tnum] = buf[off + id];
                    tnum = (tnum + 1) % 5000;
                    if (tnum == (tcycle + 4900) % 5000)
                        throw new IOException();
                }

                if (null == socketThread) {
                    socketThread = class942.startThread(this, 3);
                }

                this.notifyAll();
            }
        }
    }

    protected final void finalize() {
        close();
    }

    final int available() throws IOException {
        return closed ? 0 : inputStream.available();
    }

    final void checkIOError() throws IOException {
        if (!closed) {
            if (IOError) {
                IOError = false;
                throw new IOException();
            }
        }
    }

    final void close() {
        if (!closed) {
            synchronized (this) {
                closed = true;
                notifyAll();
            }

            if (socketThread != null) {
                while (socketThread.status == 0) {
                    Class3_Sub13_Sub34.sleep(1L);
                }

                if (-2 == ~socketThread.status) {
                    try {
                        ((Thread) socketThread.value).join();
                    } catch (InterruptedException var4) {
                        ;
                    }
                }
            }

            socketThread = null;
        }
    }


    public static void pushMinimapPixelsHD(int pixels[], int pixel, int step, int z, int x, int y) {
        Class949 class30_sub3 = Class75_Sub2.class949s[z][x][y];
        if (class30_sub3 == null)
            return;
        Class1015 class43 = class30_sub3.class1015;
        if (class43 != null) {
            if (class43.color1 != 12345678) {
                if (class43.minimapColor == 0) {
                    return;
                }
                int hs = class43.color1 & ~0x7f;
                int l1 = class43.color4 & 0x7f;
                int l2 = class43.color3 & 0x7f;
                int l3 = (class43.color1 & 0x7f) - l1;
                int l4 = (class43.color2 & 0x7f) - l2;
                l1 <<= 2;
                l2 <<= 2;
                for (int k1 = 0; k1 < 4; k1++) {
                    if (!class43.textured) {
                        pixels[pixel] = hsl2rgb[hs | (l1 >> 2)];
                        pixels[pixel + 1] = hsl2rgb[hs | (l1 * 3 + l2 >> 4)];
                        pixels[pixel + 2] = hsl2rgb[hs | (l1 + l2 >> 3)];
                        pixels[pixel + 3] = hsl2rgb[hs | (l1 + l2 * 3 >> 4)];
                    } else {
                        int j1 = class43.minimapColor;
                        int lig = 0xff - ((l1 >> 1) * (l1 >> 1) >> 8);
                        pixels[pixel] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                        lig = 0xff - ((l1 * 3 + l2 >> 3) * (l1 * 3 + l2 >> 3) >> 8);
                        pixels[pixel + 1] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                        lig = 0xff - ((l1 + l2 >> 2) * (l1 + l2 >> 2) >> 8);
                        pixels[pixel + 2] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                        lig = 0xff - ((l1 + l2 * 3 >> 3) * (l1 + l2 * 3 >> 3) >> 8);
                        pixels[pixel + 3] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                    }
                    l1 += l3;
                    l2 += l4;
                    pixel += step;
                }
                return;
            }
            int j1 = class43.minimapColor;
            if (j1 == 0)
                return;
            for (int k1 = 0; k1 < 4; k1++) {
                pixels[pixel] = j1;
                pixels[pixel + 1] = j1;
                pixels[pixel + 2] = j1;
                pixels[pixel + 3] = j1;
                pixel += step;
            }

            return;
        }
        Class997 class40 = class30_sub3.class997;
        if (class40 == null)
            return;
        int l1 = class40.shape;
        int i2 = class40.rotation;
        int j2 = class40.overlayMinimapColor;
        int k2 = class40.underlayMinimapColor;
        int ai1[] = Class933.anIntArrayArray3215[l1];
        int ai2[] = Class162.anIntArrayArray2039[i2];
        int l2 = 0;
        if (class40.color62 != 12345678) {
            int hs1 = class40.color62 & ~0x7f;
            int l11 = class40.color92 & 0x7f;
            int l21 = class40.color82 & 0x7f;
            int l31 = (class40.color62 & 0x7f) - l11;
            int l41 = (class40.color72 & 0x7f) - l21;
            l11 <<= 2;
            l21 <<= 2;
            for (int k1 = 0; k1 < 4; k1++) {
                if (!class40.textured) {
                    if (ai1[ai2[l2++]] != 0)
                        pixels[pixel] = hsl2rgb[hs1 | (l11 >> 2)];
                    if (ai1[ai2[l2++]] != 0)
                        pixels[pixel + 1] = hsl2rgb[hs1 | (l11 * 3 + l21 >> 4)];
                    if (ai1[ai2[l2++]] != 0)
                        pixels[pixel + 2] = hsl2rgb[hs1 | (l11 + l21 >> 3)];
                    if (ai1[ai2[l2++]] != 0)
                        pixels[pixel + 3] = hsl2rgb[hs1 | (l11 + l21 * 3 >> 4)];
                } else {
                    int j1 = k2;
                    if (ai1[ai2[l2++]] != 0) {
                        int lig = 0xff - ((l11 >> 1) * (l11 >> 1) >> 8);
                        pixels[pixel] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                    }
                    if (ai1[ai2[l2++]] != 0) {
                        int lig = 0xff - ((l11 * 3 + l21 >> 3) * (l11 * 3 + l21 >> 3) >> 8);
                        pixels[pixel + 1] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                    }
                    if (ai1[ai2[l2++]] != 0) {
                        int lig = 0xff - ((l11 + l21 >> 2) * (l11 + l21 >> 2) >> 8);
                        pixels[pixel + 2] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                    }
                    if (ai1[ai2[l2++]] != 0) {
                        int lig = 0xff - ((l11 + l21 * 3 >> 3) * (l11 + l21 * 3 >> 3) >> 8);
                        pixels[pixel + 3] = ((j1 & 0xff00ff) * lig & ~0xff00ff) + ((j1 & 0xff00) * lig & 0xff0000) >> 8;
                    }
                }
                l11 += l31;
                l21 += l41;
                pixel += step;
            }
            if (j2 != 0 && class40.color61 != 12345678) {
                pixel -= step << 2;
                l2 -= 16;
                hs1 = class40.color61 & ~0x7f;
                l11 = class40.color91 & 0x7f;
                l21 = class40.color81 & 0x7f;
                l31 = (class40.color61 & 0x7f) - l11;
                l41 = (class40.color71 & 0x7f) - l21;
                l11 <<= 2;
                l21 <<= 2;
                for (int k1 = 0; k1 < 4; k1++) {
                    if (ai1[ai2[l2++]] == 0)
                        pixels[pixel] = hsl2rgb[hs1 | (l11 >> 2)];
                    if (ai1[ai2[l2++]] == 0)
                        pixels[pixel + 1] = hsl2rgb[hs1 | (l11 * 3 + l21 >> 4)];
                    if (ai1[ai2[l2++]] == 0)
                        pixels[pixel + 2] = hsl2rgb[hs1 | (l11 + l21 >> 3)];
                    if (ai1[ai2[l2++]] == 0)
                        pixels[pixel + 3] = hsl2rgb[hs1 | (l11 + l21 * 3 >> 4)];
                    l11 += l31;
                    l21 += l41;
                    pixel += step;
                }
            }
            return;
        }
        if (j2 != 0) {
            for (int i3 = 0; i3 < 4; i3++) {
                pixels[pixel] = ai1[ai2[l2++]] != 0 ? k2 : j2;
                pixels[pixel + 1] = ai1[ai2[l2++]] != 0 ? k2 : j2;
                pixels[pixel + 2] = ai1[ai2[l2++]] != 0 ? k2 : j2;
                pixels[pixel + 3] = ai1[ai2[l2++]] != 0 ? k2 : j2;
                pixel += step;
            }

            return;
        }
        for (int j3 = 0; j3 < 4; j3++) {
            if (ai1[ai2[l2++]] != 0)
                pixels[pixel] = k2;
            if (ai1[ai2[l2++]] != 0)
                pixels[pixel + 1] = k2;
            if (ai1[ai2[l2++]] != 0)
                pixels[pixel + 2] = k2;
            if (ai1[ai2[l2++]] != 0)
                pixels[pixel + 3] = k2;
            pixel += step;
        }
    }

    static final void pushMinimapPixels(int[] pixels, int pixel, int step, int z, int x, int y, int dummy) {
        Class949 class949 = Class75_Sub2.class949s[z][x][y];
        if (class949 != null) {
            Class1015 tile1 = class949.class1015;
            if (tile1 != null) {
                int color = tile1.minimapColor;
                if (color != 0) {
                    for (int loops = 0; loops < 4; ++loops) {
                        pixels[pixel] = color;
                        pixels[pixel + 1] = color;
                        pixels[pixel + 2] = color;
                        pixels[pixel + 3] = color;
                        pixel += step;
                    }

                }
            } else {
                Class997 tile2 = class949.class997;
                if (tile2 != null) {
                    int var9 = tile2.shape;
                    int var10 = tile2.rotation;
                    int var11 = tile2.overlayMinimapColor;
                    int var12 = tile2.underlayMinimapColor;
                    int[] var13 = Class933.anIntArrayArray3215[var9];
                    int[] var14 = Class162.anIntArrayArray2039[var10];
                    int var15 = 0;
                    if (var11 != 0) {
                        for (int var16 = 0; var16 < 4; ++var16) {
                            pixels[pixel] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixels[pixel + 1] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixels[pixel + 2] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixels[pixel + 3] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixel += step;
                        }
                    } else {
                        for (int var16 = 0; var16 < 4; ++var16) {
                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel] = var12;
                            }

                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel + 1] = var12;
                            }

                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel + 2] = var12;
                            }

                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel + 3] = var12;
                            }

                            pixel += step;
                        }
                    }

                }
            }
        }
    }


    static final void pushMinimapPixelsLD(int[] pixels, int pixel, int step, int z, int x, int y) {
        Class949 class949 = Class75_Sub2.class949s[z][x][y];
        if (class949 != null) {
            Class1015 tile1 = class949.class1015;
            if (tile1 != null) {
                int color = tile1.minimapColor;
                if (color != 0) {
                    for (int loops = 0; loops < 4; ++loops) {
                        pixels[pixel] = color;
                        pixels[pixel + 1] = color;
                        pixels[pixel + 2] = color;
                        pixels[pixel + 3] = color;
                        pixel += step;
                    }

                }
            } else {
                Class997 tile2 = class949.class997;
                if (tile2 != null) {
                    int var9 = tile2.shape;
                    int var10 = tile2.rotation;
                    int var11 = tile2.overlayMinimapColor;
                    int var12 = tile2.underlayMinimapColor;
                    int[] var13 = Class933.anIntArrayArray3215[var9];
                    int[] var14 = Class162.anIntArrayArray2039[var10];
                    int var15 = 0;
                    if (var11 != 0) {
                        for (int var16 = 0; var16 < 4; ++var16) {
                            pixels[pixel] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixels[pixel + 1] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixels[pixel + 2] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixels[pixel + 3] = var13[var14[var15++]] == 0 ? var11 : var12;
                            pixel += step;
                        }
                    } else {
                        for (int var16 = 0; var16 < 4; ++var16) {
                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel] = var12;
                            }

                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel + 1] = var12;
                            }

                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel + 2] = var12;
                            }

                            if (var13[var14[var15++]] != 0) {
                                pixels[pixel + 3] = var12;
                            }

                            pixel += step;
                        }
                    }

                }
            }
        }
    }

    Class1007(Socket s, Class942 sl) throws IOException {
        class942 = sl;
        socket = s;
        socket.setSoTimeout(30000);
        socket.setTcpNoDelay(true);
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
    }

    static final void method1470(int var0, Class954 var1, int var2, int var3, boolean var4, int var5) {
        try {
            if (-51 < ~Class113.anInt1552) {
                if (var1.anIntArrayArray1867 != null && ~var5 > ~var1.anIntArrayArray1867.length && null != var1.anIntArrayArray1867[var5]) {
                    int var6 = var1.anIntArrayArray1867[var5][0];
                    int var7 = var6 >> 8;
                    int var10;
                    if (1 < var1.anIntArrayArray1867[var5].length) {
                        var10 = (int) ((double) var1.anIntArrayArray1867[var5].length * Math.random());
                        if (0 < var10) {
                            var7 = var1.anIntArrayArray1867[var5][var10];
                        }
                    }

                    int var8 = var6 >> 5 & 7;
                    int var9 = var6 & 31;
                    if (~var9 != -1) {
                        if (0 != Class14.areaSoundsVolume) {
                            Class1042_4.anIntArray2550[Class113.anInt1552] = var7;
                            Class166.anIntArray2068[Class113.anInt1552] = var8;
                            int var11 = (-64 + var0) / 128;
                            var10 = (var3 + -64) / 128;
                            Class1008.anIntArray2157[Class113.anInt1552] = 0;
                            Class945.aClass135Array2131[Class113.anInt1552] = null;
                            Class3_Sub13_Sub6.anIntArray3083[Class113.anInt1552] = var9 + (var10 << 16) + (var11 << 8);
                            if (var2 != 183921384) {
                                aClass33_1238 = (Class33) null;
                            }

                            ++Class113.anInt1552;
                        }
                    } else {
                        if (var4) {
                            Class3_Sub13_Sub6.method199(var8, var7, 0, -799);
                        }

                    }
                }
            }
        } catch (RuntimeException var12) {
            throw Class1134.method1067(var12, "ma.C(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
        }
    }

    public static int hsl2rgb[] = new int[0x10000];

    static {
        float brightness = 0.7F;
        int hsl = 0;
        for (int k = 0; k < 512; k++) {
            float d1 = (float) (k / 8) / 64F + 0.0078125F;
            float d2 = (float) (k & 7) / 8F + 0.0625F;
            for (int k1 = 0; k1 < 128; k1++) {
                float d3 = (float) k1 / 128F;
                float r = d3;
                float g = d3;
                float b = d3;
                if (d2 != 0.0F) {
                    float d7;
                    if (d3 < 0.5F) {
                        d7 = d3 * (1.0F + d2);
                    } else {
                        d7 = (d3 + d2) - d3 * d2;
                    }
                    float d8 = 2F * d3 - d7;
                    float d9 = d1 + 1F / 3F;
                    if (d9 > 1.0F) {
                        d9--;
                    }
                    float d10 = d1;
                    float d11 = d1 - 1F / 3F;
                    if (d11 < 0.0F) {
                        d11++;
                    }
                    if (6F * d9 < 1.0F) {
                        r = d8 + (d7 - d8) * 6F * d9;
                    } else if (2F * d9 < 1.0F) {
                        r = d7;
                    } else if (3F * d9 < 2F) {
                        r = d8 + (d7 - d8) * ((2F / 3F) - d9) * 6F;
                    } else {
                        r = d8;
                    }
                    if (6F * d10 < 1.0F) {
                        g = d8 + (d7 - d8) * 6F * d10;
                    } else if (2F * d10 < 1.0F) {
                        g = d7;
                    } else if (3F * d10 < 2F) {
                        g = d8 + (d7 - d8) * ((2F / 3F) - d10) * 6F;
                    } else {
                        g = d8;
                    }
                    if (6F * d11 < 1.0F) {
                        b = d8 + (d7 - d8) * 6F * d11;
                    } else if (2F * d11 < 1.0F) {
                        b = d7;
                    } else if (3F * d11 < 2F) {
                        b = d8 + (d7 - d8) * ((2F / 3F) - d11) * 6F;
                    } else {
                        b = d8;
                    }
                }
                int rgb = ((int) ((float) Math.pow((double) r, (double) brightness) * 256F) << 16) + ((int) ((float) Math.pow((double) g, (double) brightness) * 256F) << 8) + (int) ((float) Math.pow((double) b, (double) brightness) * 256F);
                if (rgb == 0)
                    rgb = 1;

                hsl2rgb[hsl++] = rgb;
            }

        }
    }

}
/*import java.awt.Color;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

final class Class1007 implements Runnable {

   private int tnum = 0;
   private OutputStream outputStream;
   private InputStream inputStream;
   private byte[] class966;
   static int anInt1234;
   private Socket socket;
   private int tcycle = 0;
   private Class1124 socketThread;
   static Class33 aClass33_1238;
   private Class942 class942;
   private boolean closed = false;
   static Class975 aClass61_1242 = new Class975();
   private boolean IOError = false;
   static int anInt1244 = -1;

	public final void run() {
		try {
			for(;;) {
				int off;
				int len;
				synchronized (this) {
					if (tcycle == tnum) {
						if (closed) {
							break;
						}
						try {
							this.wait();
						} catch (InterruptedException var9) {
							System.out.println("dis error1");
						}
					}

					off = tcycle;
					if (tnum >= tcycle) {
						len = tnum - tcycle;
					} else {
						len = 5000 - tcycle;
					}
				}

				if(len > 0) {
					try {
						outputStream.write(class966, off, len);
					} catch (IOException var8) {
						IOError = true;
					}
	
					tcycle = (len + tcycle) % 5000;
	
					try {
						if (tcycle == tnum) {
							outputStream.flush();
						}
					} catch (IOException var7) {
						IOError = true;
					}
				}
			}

			try {
				if (null != inputStream) {
					inputStream.close();
				}

				if (null != outputStream) {
					outputStream.close();
				}

				if (socket != null) {
					socket.close();
				}
			} catch (IOException var6) {
				;
			}

			class966 = null;
		} catch (Exception var11) {
			 System.out.println("dis error");
			Class49.method1125((String) null, var11);
		}
	}

   static final void method1460(int var0, int var1, byte var2, int var3, int var4, int var5, int var6) {
      try {
         if(~(var5 - var4) <= ~Class101.anInt1425 && Class3_Sub28_Sub18.anInt3765 >= var5 - -var4 && Class159.anInt2020 <= -var4 + var1 && Class57.anInt902 >= var4 + var1) {
            Class3_Sub13_Sub2.method175(var6, var0, var1, true, var3, var4, var5);
         } else {
            Class1223.method2275(var3, (byte)109, var1, var4, var6, var0, var5);
         }

         if(var2 > -107) {
            anInt1244 = 89;
         }

      } catch (RuntimeException var8) {
         throw Class1134.method1067(var8, "ma.A(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ')');
      }
   }

	final void read(byte[] buffer, int off, int len) throws IOException {
		if (!closed) {
			while (len > 0) {
				try {
				int avail = inputStream.read(buffer, off, len);
				if (avail <= 0)
					throw new EOFException();
				len -= avail;
				off += avail;
				} catch (Exception e) {
					//idk, error on login sometimes
					System.out.println("Caught login exception @ Class1007.read");

					Class167.processLogout();
			  		  if (null != Class3_Sub15.worldConnection) {
			  				Class3_Sub15.worldConnection.close();
			  				Class3_Sub15.worldConnection = null;
			  			}
						this.close();
					throw new RuntimeException("RTE");
					//Class167.processLogout();
				}
			}
		}
	}
	

   final int read() throws IOException {
	   return !closed?inputStream.read():0;
   }

   public static void method1463(int var0) {
         aClass61_1242 = null;
         aClass33_1238 = null;
   }

	final void write(byte[] buffer, int off, int len) throws IOException {
		 try {
		if (!closed) {
			if (IOError) {
				System.out.println("errorwritejs");
				IOError = false;
				throw new IOException();
			}
			if (class966 == null) {
				class966 = new byte[5000];
			}

			synchronized (this) {
				for (int id = 0; id < len; ++id) {
					if(off + id < buffer.length)
						class966[tnum] = buffer[off + id];
					else
						class966[tnum] = buffer[buffer.length-1];
					tnum = (tnum + 1) % 5000;
					if (tnum == (tcycle + 4900) % 5000)
						throw new IOException();
				}

				if (null == socketThread) {
					socketThread = class942.startThread(this, 3);
				}

				this.notifyAll();
			}
		}
		 } catch (Exception e) {
			 System.out.println("dis error99");
		 }
	}

	protected final void finalize() {
		close();
	}

	final int available() throws IOException {
		return closed ? 0 : inputStream.available();
	}

	final void checkIOError() throws IOException {
		if (!closed) {
			if (IOError) {
				IOError = false;
				System.out.println("dis error59");
				throw new IOException();
			}
		}
	}

	final void close() {
		if (!closed) {
			synchronized (this) {
				closed = true;
				notifyAll();
			}

			if (socketThread != null) {
				while (socketThread.status == 0) {
					Class3_Sub13_Sub34.sleep(1L);
				}

				if (-2 == ~socketThread.status) {
					try {
						((Thread) socketThread.value).join();
					} catch (InterruptedException var4) {
						;
					}
				}
			}

			socketThread = null;
		}
	}
    

    static final void pushMinimapPixelsHD(int pixels[], int mapPixel, int scanLength, int z, int x, int y) {
 	   try {
 	   Class949 ground = Class75_Sub2.class949s[z][x][y];
 	    if (ground == null) {
 	        return;
 	    }
 	    Class1015 class43 = ground.class1015;

 
 	    if (class43 != null) {
 	    	int texture = class43.texture;
 	    	if(texture == 1 || texture == 137 || texture == 31|| texture == 24 || texture == 22) {
 	    		pushMinimapPixelsLD(pixels, mapPixel, 512, z, x, y);
 	    		return;
 	    	}
 	    	//print out texture for murky water
 		    if(x == 49 && y == 50) {
 	 	    	System.out.println("43: " + class43.color1 + ", " + class43.color2 + ", " + class43.color3 + ", " + class43.color4
 	 	    			+ ", texture: " + class43.texture);
 	 	    }
 		    if(class43.color1== 13074 && class43.color2 == 13073 && class43.color3 == 13075 && class43.color4 == 13074)  {
 		    	pushMinimapPixelsLD(pixels, mapPixel, 512, z, x, y);
	                return;
 		    }
 	        if (class43.color1 != 0xbc614e) {
 	            if (class43.minimapColor == 0) {
 	            	pushMinimapPixelsLD(pixels, mapPixel, 512, z, x, y);
 	                return;
 	            }
 	         
	            int hs = class43.color1 & ~0x7f; //green everywhere
	            int l1 = class43.color4 & 0x7f;
	            int l2 = class43.color3 & 0x7f;
	            int l3 = (class43.color1 & 0x7f) - l1;
	            int l4 = (class43.color2 & 0x7f) - l2;
 	            l1 <<= 2;
 	            l2 <<= 2;
 	            for (int k1 = 0; k1 < 4; k1++) {
 	                pixels[mapPixel] = Class978.hsl2rgb[hs | (l1 >> 2)];
 	                pixels[mapPixel + 1] = Class978.hsl2rgb[hs | (l1 * 3 + l2 >> 4)];
 	                pixels[mapPixel + 2] = Class978.hsl2rgb[hs | (l1 + l2 >> 3)];
 	                pixels[mapPixel + 3] = Class978.hsl2rgb[hs | (l1 + l2 * 3 >> 4)];
 	                l1 += l3;
 	                l2 += l4;
 	                mapPixel += scanLength;
 	            }
 	            return;
 	        }
 	        int mapRGB = class43.minimapColor;
 	        if (mapRGB == 0) {
 	            return;
 	        }
 	        for (int k1 = 0; k1 < 4; k1++) {
 	            pixels[mapPixel] = mapRGB;
 	            pixels[mapPixel + 1] = mapRGB;
 	            pixels[mapPixel + 2] = mapRGB;
 	            pixels[mapPixel + 3] = mapRGB;
 	            mapPixel += scanLength;
 	        }
 	        return;
 	    }
 	    Class997 class40 = ground.class997;
 	    if (class40 == null) {
 	        return;
 	    }
	   if(x == 55 && y == 56) {
	    	 System.out.println("class40: " + class40.overlayMinimapColor  + ", " + class40.underlayMinimapColor + ", " + class40.shape
                     + ", " + class40.rotation + ", " + ((class40.color62&~0x7f) - 111)  + ", " + (class40.color92& 0x7f) + ", " + (class40.color82& 0x7f) +
                     ", " + (class40.color72& 0x7f) + ", " + ((class40.color72& 0x7f) - 121));
 	    }
	    
	    boolean aroundWater = false;
	    Class949 ground1 = null;
	    Class1015 class44 = null;
	    //N-E-S-W
	    for(int i = 0; i < 4; i ++) {
	    	ground1 = Class75_Sub2.class949s[z][i == 0 ?
	    			x + 1 : i == 2 ? x - 1 : x][i == 3 ? y + 1 : i == 4 ? y - 1 : y];
	    	if(ground!= null) {
	    		class44 = ground.class1015;
	    		if(class44 != null) {
	    			if (class44.texture == 1 || class44.texture == 137 || class44.texture == 31)
	    				aroundWater = true;
	    		}
	    	}
	    		
	    }
	    //NE-SE-SW-NW
	    for(int i = 0; i < 3; i ++) {
	    	ground1 = Class75_Sub2.class949s[z][i == 0 ? x + 1
	    			: i == 1 ? x - 1 :
	    				i == 2 ? x + 1 :
	    					i == 3 ? x + 1
	    							: x]
	    							
	    							[i == 0 ? y + 1 :
	    								i == 1 ? y - 1 :
	    									i == 2 ? y - 1 :
	    										i == 3 ? y - 1
	    												: y];
	    	if(ground!= null) {
	    		class44 = ground.class1015;
	    		if(class44 != null) {
	    			if (class44.texture == 1 || class44.texture == 137 || class44.texture == 31)
	    				aroundWater = true;
	    		}
	    	}
	    		
	    }

	    if((class40.color82& 0x7f) == 86
	    		|| (class40.color72& 0x7f) == 86
	    	|| (class40.color92& 0x7f) == 86
	    			||		
	    			(((class40.color92& 0x7f) >= 0 && (class40.color92& 0x7f) <= 125) && ((class40.color62&~0x7f) - 111) == -111) 
	    	/*&& aroundWater//) {
 	   if(
 			  (class40.color82& 0x7f) == 86
	    		|| (class40.color72& 0x7f) == 86
	    	|| (class40.color92& 0x7f) == 86
	    			||		
	    			(((class40.color92& 0x7f) >= 0 && (class40.color92& 0x7f) <= 125) && ((class40.color62&~0x7f) - 111) == -111)  
	    			
	    			&& (class40.texture == 1 || class40.texture == 137 || class40.texture == 31)){
   		 pushMinimapPixelsLD(pixels, mapPixel, 512, z, x, y);
         return;
  	   }
  	   boolean disableShapedTiles = false;
  	   if(disableShapedTiles == true){
    		 pushMinimapPixelsLD(pixels, mapPixel, 512, z, x, y);
             return;
      	   }
  	   
 	    int shape = class40.shape;
 	    int rotation = class40.rotation;
 	    int underlayRGB = class40.overlayMinimapColor;
 	    int overlayRGB = class40.underlayMinimapColor;
 	    int shadePoints[] = Class3_Sub13_Sub18.anIntArrayArray3215[shape];
 	    int shapeIndices[] = Class162.anIntArrayArray2039[rotation];
 	    int shapePtr = 0;

 	    if (class40.color62 != 0xbc614e) {
 	        int hs1 = class40.color62 & ~0x7f;
 	        int l11 = class40.color92 & 0x7f;
 	        int l21 = class40.color82 & 0x7f;
 	        int l31 = (class40.color62 & 0x7f) - l11;
 	        int l41 = (class40.color72 & 0x7f) - l21;
 	        l11 <<= 2;
 	        l21 <<= 2;
 	        for (int k1 = 0; k1 < 4; k1++) {
 	            if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	                pixels[mapPixel] = Class978.hsl2rgb[hs1 | (l11 >> 2)];
 	            }
 	            if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	                pixels[mapPixel + 1] = Class978.hsl2rgb[hs1 | (l11 * 3 + l21 >> 4)];
 	            }
 	            if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	                pixels[mapPixel + 2] = Class978.hsl2rgb[hs1 | (l11 + l21 >> 3)];
 	            }
 	            if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	                pixels[mapPixel + 3] = Class978.hsl2rgb[hs1 | (l11 + l21 * 3 >> 4)];
 	            }
 	            l11 += l31;
 	            l21 += l41;
 	            mapPixel += scanLength;
 	        }
 	        if (underlayRGB != 0 && class40.color61 != 0xbc614e) {
 	            mapPixel -= scanLength << 2;
 	            shapePtr -= 16;
 	            hs1 = class40.color61 & ~0x7f;
 	            l11 = class40.color91 & 0x7f;
 	            l21 = class40.color81 & 0x7f;
 	            l31 = (class40.color61 & 0x7f) - l11;
 	            l41 = (class40.color71 & 0x7f) - l21;
 	            l11 <<= 2;
 	            l21 <<= 2;
 	            for (int k1 = 0; k1 < 4; k1++) {
 	                if (shadePoints[shapeIndices[shapePtr++]] == 0) {
 	                    pixels[mapPixel] = Class978.hsl2rgb[hs1 | (l11 >> 2)];
 	                }
 	                if (shadePoints[shapeIndices[shapePtr++]] == 0) {
 	                    pixels[mapPixel + 1] = Class978.hsl2rgb[hs1 | (l11 * 3 + l21 >> 4)];
 	                }
 	                if (shadePoints[shapeIndices[shapePtr++]] == 0) {
 	                    pixels[mapPixel + 2] = Class978.hsl2rgb[hs1 | (l11 + l21 >> 3)];
 	                }
 	                if (shadePoints[shapeIndices[shapePtr++]] == 0) {
 	                    pixels[mapPixel + 3] = Class978.hsl2rgb[hs1 | (l11 + l21 * 3 >> 4)];
 	                }
 	                l11 += l31;
 	                l21 += l41;
 	                mapPixel += scanLength;
 	            }
 	        }
 	        return;
 	    }
 	    if (underlayRGB != 0) {
 	        for (int i3 = 0; i3 < 4; i3++) {
 	            pixels[mapPixel] = shadePoints[shapeIndices[shapePtr++]] != 0 ? overlayRGB : underlayRGB;
 	            pixels[mapPixel + 1] = shadePoints[shapeIndices[shapePtr++]] != 0 ? overlayRGB : underlayRGB;
 	            pixels[mapPixel + 2] = shadePoints[shapeIndices[shapePtr++]] != 0 ? overlayRGB : underlayRGB;
 	            pixels[mapPixel + 3] = shadePoints[shapeIndices[shapePtr++]] != 0 ? overlayRGB : underlayRGB;
 	            mapPixel += scanLength;
 	        }
 	        return;
 	    }
 	    for (int j3 = 0; j3 < 4; j3++) {
 	        if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	            pixels[mapPixel] = overlayRGB;
 	        }
 	        if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	            pixels[mapPixel + 1] = overlayRGB;
 	        }
 	        if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	            pixels[mapPixel + 2] = overlayRGB;
 	        }
 	        if (shadePoints[shapeIndices[shapePtr++]] != 0) {
 	            pixels[mapPixel + 3] = overlayRGB;
 	        }
 	        mapPixel += scanLength;
 	    }
 	   } catch(Exception e) {
 		   
 	   }
 	}

    public static boolean betweenExclusive(int x, int min, int max)
    {
        return x>min && x<max;    
    }
    
   static final void pushMinimapPixelsLD(int[] pixels, int pixel, int step, int z, int x, int y) {
      Class949 groundTile = Class75_Sub2.class949s[z][x][y];
      if(groundTile != null) {
         Class1015 tile1 = groundTile.class1015;
         if(tile1 != null) {
            int color = tile1.minimapColor;
            if(color != 0) {
               for(int loops = 0; loops < 4; ++loops) {
                  pixels[pixel] = color;
                  pixels[pixel + 1] = color;
                  pixels[pixel + 2] = color;
                  pixels[pixel + 3] = color;
                  pixel += step;
               }

            }
         } else {
            Class997 tile2 = groundTile.class997;
            if(tile2 != null) {
               int var9 = tile2.shape;
               int var10 = tile2.rotation;
               int var11 = tile2.overlayMinimapColor;
               int var12 = tile2.underlayMinimapColor;
               int[] var13 = Class3_Sub13_Sub18.anIntArrayArray3215[var9];
               int[] var14 = Class162.anIntArrayArray2039[var10];
               int var15 = 0;
               if(var11 != 0) {
                  for(int var16 = 0; var16 < 4; ++var16) {
                     pixels[pixel] = var13[var14[var15++]] == 0?var11:var12;
                     pixels[pixel + 1] = var13[var14[var15++]] == 0?var11:var12;
                     pixels[pixel + 2] = var13[var14[var15++]] == 0?var11:var12;
                     pixels[pixel + 3] = var13[var14[var15++]] == 0?var11:var12;
                     pixel += step;
                  }
               } else {
                  for(int var16 = 0; var16 < 4; ++var16) {
                     if(var13[var14[var15++]] != 0) {
                        pixels[pixel] = var12;
                     }

                     if(var13[var14[var15++]] != 0) {
                        pixels[pixel + 1] = var12;
                     }

                     if(var13[var14[var15++]] != 0) {
                        pixels[pixel + 2] = var12;
                     }

                     if(var13[var14[var15++]] != 0) {
                        pixels[pixel + 3] = var12;
                     }

                     pixel += step;
                  }
               }

            }
         }
      }
   }

	Class1007(java.net.Socket s, Class942 sl) throws IOException {
		class942 = sl;
		socket = s;
		socket.setSoTimeout(30000);
		socket.setTcpNoDelay(true);
		inputStream = socket.getInputStream();
		outputStream = socket.getOutputStream();
	}

   static final void method1470(int var0, Class954 var1, int var2, int var3, boolean var4, int var5) {
      try {
         if(-51 < ~Class113.anInt1552) {
            if(var1.anIntArrayArray1867 != null && ~var5 > ~var1.anIntArrayArray1867.length && null != var1.anIntArrayArray1867[var5]) {
               int var6 = var1.anIntArrayArray1867[var5][0];
               int var7 = var6 >> 8;
               int var10;
               if(1 < var1.anIntArrayArray1867[var5].length) {
                  var10 = (int)((double)var1.anIntArrayArray1867[var5].length * Math.random());
                  if(0 < var10) {
                     var7 = var1.anIntArrayArray1867[var5][var10];
                  }
               }

               int var8 = var6 >> 5 & 7;
               int var9 = var6 & 31;
               if(~var9 != -1) {
                  if(0 != Class14.areaSoundsVolume) {
                     InventoryNode.anIntArray2550[Class113.anInt1552] = var7;
                     Class166.anIntArray2068[Class113.anInt1552] = var8;
                     int var11 = (-64 + var0) / 128;
                     var10 = (var3 + -64) / 128;
                     Class1008.anIntArray2157[Class113.anInt1552] = 0;
                     Class945.aClass135Array2131[Class113.anInt1552] = null;
                     Class3_Sub13_Sub6.anIntArray3083[Class113.anInt1552] = var9 + (var10 << 16) + (var11 << 8);
                     if(var2 != 183921384) {
                        aClass33_1238 = (Class33)null;
                     }

                     ++Class113.anInt1552;
                  }
               } else {
                  if(var4) {
                     Class3_Sub13_Sub6.method199(var8, var7, 0, -799);
                  }

               }
            }
         }
      } catch (RuntimeException var12) {
         throw Class1134.method1067(var12, "ma.C(" + var0 + ',' + (var1 != null?"{...}":"null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

}
*/
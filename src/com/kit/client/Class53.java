package com.kit.client;

import java.io.*;

final class Class53 {

   static int anInt865 = -1;
   static long aLong866 = 0L;
   static int anInt867;

   public static void method1169(boolean var0) {
      try {
         if(var0) {
            method1170((byte)25, 28);
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "hi.C(" + var0 + ')');
      }
   }

   static final int method1170(byte var0, int var1) {
      try {
         int var2 = -77 / ((-34 - var0) / 52);
         return var1 >>> 8;
      } catch (RuntimeException var3) {
         throw Class1134.method1067(var3, "hi.E(" + var0 + ',' + var1 + ')');
      }
   }

   static final String method1172(int var0, Throwable var1) throws IOException {
      String var2;
      if(var1 instanceof Class1244) {
         Class1244 var3 = (Class1244)var1;
         var1 = var3.aThrowable2118;
         var2 = var3.aString2117 + " | ";
      } else {
         var2 = "";
      }

      StringWriter var13 = new StringWriter();
      PrintWriter var4 = new PrintWriter(var13);
      var1.printStackTrace(var4);
      var4.close();
      String var5 = var13.toString();
      BufferedReader var6 = new BufferedReader(new StringReader(var5));
      String var7 = var6.readLine();

      while(true) {
         String var8 = var6.readLine();
         if(var8 == null) {
            int var14 = -107 % ((var0 - 31) / 34);
            var2 = var2 + "| " + var7;
            return var2;
         }

         int var9 = var8.indexOf(40);
         int var10 = var8.indexOf(41, 1 + var9);
         String var11;
         if(0 == ~var9) {
            var11 = var8;
         } else {
            var11 = var8.substring(0, var9);
         }

         var11 = var11.trim();
         var11 = var11.substring(1 + var11.lastIndexOf(32));
         var11 = var11.substring(var11.lastIndexOf(9) + 1);
         var2 = var2 + var11;
         if(-1 != var9 && var10 != -1) {
            int var12 = var8.indexOf(".java:", var9);
            if(var12 >= 0) {
               var2 = var2 + var8.substring(5 + var12, var10);
            }
         }

         var2 = var2 + ' ';
      }
   }

   static final void method1173(ByteBuffer var0, int var1) {
      try {
         int var2 = var0.aNon145();
         Class119.aClass26Array1627 = new Class26[var2];

         int var3;
         for(var3 = 0; var3 < var2; ++var3) {
            Class119.aClass26Array1627[var3] = new Class26();
            Class119.aClass26Array1627[var3].anInt507 = var0.aNon145();
            Class119.aClass26Array1627[var3].aClass94_508 = var0.method761(105);
         }

         if(var1 > -10) {
            method1174((Class1034)null);
         }

         Class3_Sub13_Sub4.anInt3054 = var0.aNon145();
         Class10117.anInt1416 = var0.aNon145();
         Class57.anInt906 = var0.aNon145();
         Class117.aClass44_Sub1Array1609 = new Class1228[-Class3_Sub13_Sub4.anInt3054 + Class10117.anInt1416 + 1];

         for(var3 = 0; var3 < Class57.anInt906; ++var3) {
            int var4 = var0.aNon145();
            Class1228 var5 = Class117.aClass44_Sub1Array1609[var4] = new Class1228();
            var5.anInt721 = var0.readUnsignedByte();
            var5.anInt724 = var0.getInt();
            var5.anInt2621 = var4 - -Class3_Sub13_Sub4.anInt3054;
            var5.aClass94_2620 = var0.method761(98);
            var5.aClass94_2625 = var0.method761(79);
         }

         Class956.anInt3608 = var0.getInt();
         Class1046.aBoolean579 = true;
      } catch (RuntimeException var6) {
         throw Class1134.method1067(var6, "hi.B(" + (var0 != null?"{...}":"null") + ',' + var1 + ')');
      }
   }

   static final Class1008 method1174(Class1034 var0) {
	   return ~Class1034.getInterfaceClickMask(var0).method101() != -1?(null != var0.selectedActionName && var0.selectedActionName.method1564().getLength() != 0?var0.selectedActionName:(Class1043.qaoptestEnabled? Class1244.aClass94_2116:null)):null;
   }

}

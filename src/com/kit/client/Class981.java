package com.kit.client;

final class Class981 implements Cloneable {

    int size = 1;
    private short[] aShortArray1246;
    private byte[] aByteArray1247;
    private short[] originalColors;
    boolean aBoolean1249;
    private int[] anIntArray1250;
    private int anInt1251;
    static int anInt1252 = -1;
    int anInt1253;
    private short[] modifiedColors;
    boolean aBoolean1255 = true;
    short aShort1256 = 0;
    private int anInt1257;
    private int[][] anIntArrayArray1258;
    Class1008[] actions;
    int combatLevel;
    private int[][] anIntArrayArray1261;
    int anInt1262 = -1;
    boolean aBoolean1263;
    private int sizeX;
    int anInt1265;
    private int sizeY;
    byte aByte1267;
    byte aByte1268;
    int headIconId;
    boolean aBoolean1270 = true;
    private short[] aShortArray1271;
    private Class1017_2 aClass130_1272;
    Class1008 name;
    int anInt1274;
    byte aByte1275;
    int anInt1276 = -1;
    static int[] anIntArray1277 = new int[2000];
    int anInt1278;
    int anInt1279;
    int anInt1280;
    private int anInt1282;
    int anInt1283;
    int npcID;
    boolean aBoolean1285;
    short aShort1286;
    byte aByte1287;
    private int[] modelIds;
    int anInt1289;
    int anInt1290;
    int anInt1291;
    int[] anIntArray1292;
    int anInt1293;
    private int anInt1295;
    int anInt1296;
    static int clickX;
    protected  int anInt1298;
    protected int idleAnim, walkAnim, turn180Animation, turn90CWAnimation,
            turn90CCAnimation = -1;

    final Class981 method1471(byte var1) {
        try {
            int var2 = -1;
            if (~anInt1257 == 0) {
                if (anInt1295 != -1) {
                    var2 = Class163_Sub1.variousSettings[anInt1295];
                }
            } else {
                var2 = method1484(64835055, anInt1257);
            }

            int var3;
            if (0 <= var2 && ~var2 > ~(-1 + anIntArray1292.length)
                    && anIntArray1292[var2] != -1) {
                var3 = -24 % ((-46 - var1) / 41);
                return Class981.list(anIntArray1292[var2]);
            } else {
                var3 = anIntArray1292[-1 + anIntArray1292.length];
                return ~var3 == 0 ? null : Class981.list(var3);
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "me.G(" + var1 + ')');
        }
    }

    final boolean method1472(byte var1) {
        try {
            if (var1 != 74) {
                return true;
            } else if (null == anIntArray1292) {
                return true;
            } else {
                int var2 = -1;
                if (-1 == anInt1257) {
                    if (~anInt1295 != 0) {
                        var2 = Class163_Sub1.variousSettings[anInt1295];
                    }
                } else {
                    var2 = method1484(64835055, anInt1257);
                }

                if (var2 >= 0 && var2 < -1 + anIntArray1292.length
                        && -1 != anIntArray1292[var2]) {
                    return true;
                } else {
                    int var3 = anIntArray1292[-1 + anIntArray1292.length];
                    return 0 != ~var3;
                }
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "me.L(" + var1 + ')');
        }
    }

    public static void method1473(byte var0) {
        try {
            anIntArray1277 = null;
            if (var0 != 103) {
                clickX = -20;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "me.K(" + var0 + ')');
        }
    }

    final boolean method1474(int var1) {
        try {
            if (var1 != -1) {
                method1480(false, (Class1008) null, -57);
            }

            if (anIntArray1292 != null) {
                for (int var2 = 0; ~anIntArray1292.length < ~var2; ++var2) {
                    if (0 != ~anIntArray1292[var2]) {
                        Class981 var3 = Class981
                                .list(anIntArray1292[var2]);
                        if (0 != ~var3.anInt1262 || 0 != ~var3.anInt1293
                                || var3.anInt1276 != -1) {
                            return true;
                        }
                    }
                }

                return false;
            } else {
                return -1 != anInt1262 || anInt1293 != -1 || anInt1276 != -1;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "me.E(" + var1 + ')');
        }
    }

    final int method1475(int var1, int var3) {
        if (null != aClass130_1272) {
            Class1042_2 var4 = (Class1042_2) aClass130_1272.get((long) var1);
            return var4 != null ? var4.value : var3;
        } else {
            return var3;
        }
    }

    final Class960 method1476(Class145[] var1, int var2, byte var3,
                              int var4, int var5, int var6, int var7, Class954 var8,
                              int var9, Class954 var10) {
        try {
            if (anIntArray1292 != null) {
                Class981 var33 = method1471((byte) 32);
                return null != var33 ? var33.method1476(var1, var2,
                        (byte) -102, var4, var5, var6, var7, var8, var9, var10)
                        : null;
            } else {
                Class960 var11 = (Class960) Class1048.aClass93_2442
                        .get((long) npcID);
                boolean var12;
                int var17;
                int var16;
                int var19;
                int var18;
                int var21;
                int var20;
                int var22;
                int var24;
                int var27;
                int var29;
                int var28;
                if (null == var11) {
                    var12 = false;

                    for (int var13 = 0; var13 < modelIds.length; ++var13) {
                        if (modelIds[var13] != -1
                                && !Class3_Sub13_Sub33.getModelJs5()
                                .method2129(0, modelIds[var13])) {
                            var12 = true;
                        }
                    }

                    if (var12) {
                        return null;
                    }

                    Model[] var14 = new Model[modelIds.length];

                    for (int var15 = 0; ~modelIds.length < ~var15; ++var15) {
                        if (0 != ~modelIds[var15]) {
                            var14[var15] = Model.get(
                                    Class3_Sub13_Sub33.getModelJs5(),
                                    modelIds[var15], 0);
                            if (null != anIntArrayArray1261
                                    && anIntArrayArray1261[var15] != null
                                    && var14[var15] != null) {
                                var14[var15].method2001(
                                        anIntArrayArray1261[var15][0],
                                        anIntArrayArray1261[var15][1],
                                        anIntArrayArray1261[var15][2]);
                            }
                        }
                    }

                    Model var34;
                    if (var14.length == 1) {
                        var34 = var14[0];
                    } else {
                        var34 = new Model(var14, var14.length);
                    }

                    if (originalColors != null) {
                        for (var16 = 0; ~originalColors.length < ~var16; ++var16) {
                            if (null != aByteArray1247
                                    && ~aByteArray1247.length < ~var16) {
                                var34.swapColors(
                                        originalColors[var16],
                                        Class136.aShortArray1779[aByteArray1247[var16] & 255]);
                            } else {
                                var34.swapColors(originalColors[var16],
                                        modifiedColors[var16]);
                            }
                        }
                    }

                    if (null != aShortArray1271) {
                        for (var16 = 0; aShortArray1271.length > var16; ++var16) {
                            var34.method1998(aShortArray1271[var16],
                                    aShortArray1246[var16]);
                        }
                    }

                    var11 = var34.convert(anInt1251 + 64, anInt1282 + 850, -30,
                            -50, -30);
                    if (Class1012.aBoolean_617) {
                        ((Class948) var11).method1920(false, false, false, true,
                                false, false, true);
                    }

                    Class1048.aClass93_2442.put(var11, (long) npcID);
                }

                var12 = false;
                boolean var37 = false;
                boolean var35 = false;
                boolean var36 = false;
                var16 = null != var1 ? var1.length : 0;

                for (var17 = 0; ~var16 < ~var17; ++var17) {
                    if (var1[var17] != null) {
                        Class954 var39 = Class954.list(
                                var1[var17].anInt1890);
                        if (null != var39.frames) {
                            Class85.aClass142Array1168[var17] = var39;
                            var20 = var1[var17].anInt1891;
                            var12 = true;
                            var19 = var1[var17].anInt1893;
                            var21 = var39.frames[var19];
                            Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var17] = Class955
                                    .list(var21 >>> 16, 0, true);
                            var21 &= '\uffff';
                            Class58.anIntArray912[var17] = var21;
                            if (Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var17] != null) {
                                var35 |= Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var17]
                                        .method561(var21);
                                var37 |= Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var17]
                                        .method559(var21);
                                var36 |= var39.aBoolean1848;
                            }

                            if ((var39.tween || Class3_Sub26.forceTweeningEnabled)
                                    && 0 != ~var20
                                    && ~var39.frames.length < ~var20) {
                                Class38.anIntArray664[var17] = var39.cycles[var19];
                                Class1002.anIntArray2574[var17] = var1[var17].anInt1897;
                                var22 = var39.frames[var20];
                                Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var17] = Class955
                                        .list(var22 >>> 16, 0, false);
                                var22 &= '\uffff';
                                Class1046.anIntArray574[var17] = var22;
                                if (null != Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var17]) {
                                    var35 |= Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var17]
                                            .method561(var22);
                                    var37 |= Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var17]
                                            .method559(var22);
                                }
                            } else {
                                Class38.anIntArray664[var17] = 0;
                                Class1002.anIntArray2574[var17] = 0;
                                Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var17] = null;
                                Class1046.anIntArray574[var17] = -1;
                            }
                        }
                    }
                }

                if (!var12 && null == var10 && var8 == null) {
                    Class960 var41 = var11.method1894(true, true, true);
                    if (sizeX != 128 || -129 != ~sizeY) {
                        var41.scale(sizeX, sizeY, sizeX);
                    }

                    return var41;
                } else {
                    var18 = -1;
                    var17 = -1;
                    var19 = 0;
                    Class955 var40 = null;
                    Class955 var43 = null;
                    int var42;
                    if (var10 != null) {
                        var17 = var10.frames[var7];
                        var22 = var17 >>> 16;
                        var17 &= '\uffff';
                        var40 = Class955.list(var22, 0, true);
                        if (null != var40) {
                            var35 |= var40.method561(var17);
                            var37 |= var40.method559(var17);
                            var36 |= var10.aBoolean1848;
                        }

                        if ((var10.tween || Class3_Sub26.forceTweeningEnabled)
                                && 0 != ~var5 && ~var10.frames.length < ~var5) {
                            var19 = var10.cycles[var7];
                            var18 = var10.frames[var5];
                            var42 = var18 >>> 16;
                            var18 &= '\uffff';
                            if (var22 != var42) {
                                var43 = Class955.list(var18 >>> 16, 0,
                                        true);
                            } else {
                                var43 = var40;
                            }

                            if (var43 != null) {
                                var35 |= var43.method561(var18);
                                var37 |= var43.method559(var18);
                            }
                        }
                    }

                    var22 = -1;
                    var42 = -1;
                    Class955 var44 = null;
                    var24 = 0;
                    Class955 var46 = null;
                    if (var8 != null) {
                        var22 = var8.frames[var4];
                        var27 = var22 >>> 16;
                        var22 &= '\uffff';
                        var44 = Class955.list(var27, 0, true);
                        if (var44 != null) {
                            var35 |= var44.method561(var22);
                            var37 |= var44.method559(var22);
                            var36 |= var8.aBoolean1848;
                        }

                        if ((var8.tween || Class3_Sub26.forceTweeningEnabled)
                                && 0 != ~var2 && var2 < var8.frames.length) {
                            var24 = var8.cycles[var4];
                            var42 = var8.frames[var2];
                            var28 = var42 >>> 16;
                            var42 &= '\uffff';
                            if (~var28 == ~var27) {
                                var46 = var44;
                            } else {
                                var46 = Class955.list(var42 >>> 16, 0,
                                        true);
                            }

                            if (null != var46) {
                                var35 |= var46.method561(var42);
                                var37 |= var46.method559(var42);
                            }
                        }
                    }

                    Class960 var45 = var11.method1894(!var37, !var35,
                            !var36);
                    var29 = 1;

                    for (var28 = 0; var28 < var16; ++var28) {
                        if (Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var28] != null) {
                            var45.method1887(
                                    Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var28],
                                    Class58.anIntArray912[var28],
                                    Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var28],
                                    Class1046.anIntArray574[var28],
                                    -1 + Class1002.anIntArray2574[var28],
                                    Class38.anIntArray664[var28],
                                    var29,
                                    Class85.aClass142Array1168[var28].aBoolean1848,
                                    anIntArrayArray1258[var28]);
                        }

                        var29 <<= 1;
                    }

                    if (var40 != null && var44 != null) {
                        var45.method1892(var40, var17, var43, var18, var6 + -1,
                                var19, var44, var22, var46, var42, var9 + -1,
                                var24, var10.aBooleanArray1855,
                                var10.aBoolean1848 | var8.aBoolean1848);
                    } else if (var40 == null) {
                        if (null != var44) {
                            var45.method1880(var44, var22, var46, var42, -1
                                    + var9, var24, var8.aBoolean1848);
                        }
                    } else {
                        var45.method1880(var40, var17, var43, var18, var6 + -1,
                                var19, var10.aBoolean1848);
                    }

                    for (var28 = 0; ~var28 > ~var16; ++var28) {
                        Class3_Sub13_Sub1.aClass3_Sub28_Sub5Array3041[var28] = null;
                        Class3_Sub13_Sub23_Sub1.aClass3_Sub28_Sub5Array4031[var28] = null;
                        Class85.aClass142Array1168[var28] = null;
                    }

                    if (~sizeX != -129 || -129 != ~sizeY) {
                        var45.scale(sizeX, sizeY, sizeX);
                    }

                    return var45;
                }
            }
        } catch (RuntimeException var32) {
            throw Class1134.method1067(var32, "me.M("
                    + (var1 != null ? "{...}" : "null") + ',' + var2 + ','
                    + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7
                    + ',' + (var8 != null ? "{...}" : "null") + ',' + var9
                    + ',' + (var10 != null ? "{...}" : "null") + ')');
        }
    }

    final Class1008 method1477(int var1, Class1008 var2, boolean var3) {
        try {
            if (null != aClass130_1272) {
                Class1030 var4 = (Class1030) aClass130_1272.get((long) var1);
                return !var3 ? (Class1008) null : (null == var4 ? var2
                        : var4.value);
            } else {
                return var2;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "me.I(" + var1 + ','
                    + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    final void method1478(ByteBuffer var1, boolean new_header) {
        while (true) {
            int var3 = var1.readUnsignedByte();
            if (-1 == ~var3) {
                return;
            }
            decode_525(var3, var1);
        }
    }

	/*
     * static final void method1479(int var0) { Class3_Sub13_Sub30.anInt3362 =
	 * -1;
	 * 
	 * if (-38 == ~var0) { Class1001.aFloat3979 = 3.0F; } else if (50 != var0) { if
	 * (var0 == 75) { Class1001.aFloat3979 = 6.0F; } else if (var0 != 100) { if (var0
	 * == 200) { Class1001.aFloat3979 = 16.0F; } } else { Class1001.aFloat3979 = 8.0F; } }
	 * else { Class1001.aFloat3979 = 4.0F; }
	 * 
	 * Class3_Sub13_Sub30.anInt3362 = -1; }
	 */

    static final void method1480(boolean var0, Class1008 var1, int var2) {
        try {
            short[] var3 = new short[16];
            var1 = var1.toLowerCase();
            int var4 = 0;

            for (int var5 = 0; ~var5 > ~Class3_Sub13_Sub23.anInt3287; ++var5) {
                ItemDefinition var6 = ItemDefinition.getDefinition(var5);
                if ((!var0 || var6.aBoolean807) && var6.anInt791 == -1
                        && -1 == var6.anInt762 && -1 == ~var6.anInt800
                        && var6.name.toLowerCase().method1551(var1) != -1) {
                    if (~var4 <= -251) {
                        Class99.aShortArray1398 = null;
                        Class62.anInt952 = -1;
                        return;
                    }

                    if (~var4 <= ~var3.length) {
                        short[] var7 = new short[2 * var3.length];

                        for (int var8 = 0; var8 < var4; ++var8) {
                            var7[var8] = var3[var8];
                        }

                        var3 = var7;
                    }

                    var3[var4++] = (short) var5;
                }
            }

            Class99.aShortArray1398 = var3;
            Class991.anInt2756 = 0;
            Class62.anInt952 = var4;
            Class1008[] var10 = new Class1008[Class62.anInt952];

            for (int var11 = 0; Class62.anInt952 > var11; ++var11) {
                var10[var11] = ItemDefinition.getDefinition(var3[var11]).name;
            }

            int var12 = -44 / ((45 - var2) / 33);
            Class3_Sub13_Sub29.method307(var10, Class99.aShortArray1398, 77);
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "me.J(" + var0 + ','
                    + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    final void method1481(int var1) {
        try {
            int var2 = 36 % ((12 - var1) / 41);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "me.D(" + var1 + ')');
        }
    }

    final Class960 method1482(Class954 var1, int var2,
                              int var3, int var4, int var5) {
        if (anIntArray1292 == null) {
            if (null == anIntArray1250) {
                return null;
            } else {
                Class960 var12 = (Class960) Class154.aClass93_1964
                        .get((long) npcID);
                if (var12 == null) {
                    boolean var7 = false;

                    for (int var8 = 0; ~var8 > ~anIntArray1250.length; ++var8) {
                        if (!Class3_Sub13_Sub33.getModelJs5().method2129(0,
                                anIntArray1250[var8])) {
                            var7 = true;
                        }
                    }

                    if (var7) {
                        return null;
                    }

                    Model[] var14 = new Model[anIntArray1250.length];

                    for (int var9 = 0; ~var9 > ~anIntArray1250.length; ++var9) {
                        var14[var9] = Model.get(
                                Class3_Sub13_Sub33.getModelJs5(),
                                anIntArray1250[var9], 0);
                    }

                    Model var15;
                    if (-2 != ~var14.length) {
                        var15 = new Model(var14, var14.length);
                    } else {
                        var15 = var14[0];
                    }

                    int var10;
                    if (null != originalColors) {
                        for (var10 = 0; ~originalColors.length < ~var10; ++var10) {
                            if (aByteArray1247 != null
                                    && ~var10 > ~aByteArray1247.length) {
                                var15.swapColors(
                                        originalColors[var10],
                                        Class136.aShortArray1779[255 & aByteArray1247[var10]]);
                            } else {
                                var15.swapColors(originalColors[var10],
                                        modifiedColors[var10]);
                            }
                        }
                    }

                    if (aShortArray1271 != null) {
                        for (var10 = 0; ~var10 > ~aShortArray1271.length; ++var10) {
                            var15.method1998(aShortArray1271[var10],
                                    aShortArray1246[var10]);
                        }
                    }

                    var12 = var15.convert(64, 768, -50, -10, -50);
                    Class154.aClass93_1964.put(var12, (long) npcID);
                }

                if (null != var1) {
                    var12 = var1
                            .method2055(var12, (byte) -75, var3, var2, var5);
                }

                return var12;
            }
        } else {
            Class981 var6 = method1471((byte) -100);
            return null == var6 ? null : var6.method1482(var1, var2, var3, 54,
                    var5);
        }
    }

    private final void decode_525(int opcode, ByteBuffer class966) {
        if (opcode == 1) {
            int var4 = class966.readUnsignedByte();
            modelIds = new int[var4];
            for (int var5 = 0; var4 > var5; ++var5) {
                modelIds[var5] = class966.aInteger233();
                if (modelIds[var5] == '\uffff') {
                    modelIds[var5] = -1;
                }
            }
        } else if (opcode == 2) {
            name = class966.class_91033();
        } else if (opcode == 12) {
            size = class966.readUnsignedByte();
        } else if (opcode == 13) {
            idleAnim = class966.aInteger233();
        } else if (opcode == 14) {
            walkAnim = class966.aInteger233();
        } else if (opcode == 15) {
            class966.aInteger233();// TODO - ??
        } else if (opcode == 16) {
            class966.aInteger233();// TODO - ??
        } else if (opcode == 17) {
            walkAnim = class966.aInteger233();
            turn180Animation = class966.aInteger233();
            turn90CWAnimation = class966.aInteger233();
            turn90CCAnimation = class966.aInteger233();
        } else if (opcode >= 30 && opcode < 35) {
            actions[-30 + opcode] = class966.class_91033();
            if (actions[opcode - 30].method102(Class3_Sub13_Sub3.aClass94_3051))
                actions[opcode - 30] = null;
        } else if (opcode == 40) {
            int var4 = class966.readUnsignedByte();
            modifiedColors = new short[var4];
            originalColors = new short[var4];
            for (int var5 = 0; var4 > var5; ++var5) {
                originalColors[var5] = (short) class966.aInteger233();
                modifiedColors[var5] = (short) class966.aInteger233();
            }
        } else if (opcode == 41) {
            int var4 = class966.readUnsignedByte();
            aShortArray1246 = new short[var4];
            aShortArray1271 = new short[var4];
            for (int var5 = 0; ~var4 < ~var5; ++var5) {
                aShortArray1271[var5] = (short) class966.aInteger233();
                aShortArray1246[var5] = (short) class966.aInteger233();
            }
        } else if (opcode == 42) {
            int var4 = class966.readUnsignedByte();
            aByteArray1247 = new byte[var4];
            for (int var5 = 0; var4 > var5; ++var5) {
                aByteArray1247[var5] = class966.getByte();
            }
        } else if (opcode == 60) {
            int i = class966.readUnsignedByte();
            anIntArray1250 = new int[i];
            for (int i_3_ = 0; i_3_ < i; i_3_++)
                anIntArray1250[i_3_] = class966.aInteger233();
        } else if (opcode == 93) {
            aBoolean1285 = false;
        } else if (opcode == 95) {
            combatLevel = class966.aInteger233();
        } else if (opcode == 97) {
            sizeX = class966.aInteger233();
        } else if (opcode == 98) {
            sizeY = class966.aInteger233();
        } else if (opcode == 99) {
            aBoolean1263 = true;
        } else if (opcode == 100) {
            anInt1251 = class966.getByte();
        } else if (opcode == 101) {
            anInt1282 = class966.getByte() * 5;
        } else if (opcode == 102) {
            headIconId = class966.aInteger233();
        } else if (opcode == 103) {
            anInt1274 = class966.aInteger233();
        } else if (opcode == 106 || opcode == 118) {
            anInt1257 = class966.aInteger233();
            int var4 = -1;
            if (-65536 == ~anInt1257) {
                anInt1257 = -1;
            }
            anInt1295 = class966.aInteger233();
            if (~anInt1295 == -65536) {
                anInt1295 = -1;
            }
            if (opcode == 118) {
                var4 = class966.aInteger233();
                if (-65536 == ~var4) {
                    var4 = -1;
                }
            }
            int var5 = class966.readUnsignedByte();
            anIntArray1292 = new int[2 + var5];
            for (int var6 = 0; ~var5 <= ~var6; ++var6) {
                anIntArray1292[var6] = class966.aInteger233();
                if (~anIntArray1292[var6] == -65536) {
                    anIntArray1292[var6] = -1;
                }
            }
            anIntArray1292[1 + var5] = var4;
        } else if (opcode == 107) {
            aBoolean1270 = false;
        } else if (opcode == 109) {
            aBoolean1255 = false;
        } else if (opcode == 111) {
            aBoolean1249 = false;
        } else if (opcode == 113) {
            aShort1286 = (short) class966.aInteger233();
            aShort1256 = (short) class966.aInteger233();
        } else if (opcode == 114) {
            aByte1287 = class966.getByte();
            aByte1275 = class966.getByte();
        } else if (opcode == 115) {
            class966.readUnsignedByte();
            class966.readUnsignedByte();
        } else if (opcode == 119) {
            aByte1267 = class966.getByte();
        } else if (opcode == 120) {
            class966.aInteger233();
            class966.aInteger233();
            class966.aInteger233();
            class966.readUnsignedByte();
        } else if (opcode == 121) {
            anIntArrayArray1261 = new int[modelIds.length][];
            int var4 = class966.readUnsignedByte();
            for (int var5 = 0; var5 < var4; ++var5) {
                int var6 = class966.readUnsignedByte();
                int[] var7 = anIntArrayArray1261[var6] = new int[3];
                var7[0] = class966.getByte();
                var7[1] = class966.getByte();
                var7[2] = class966.getByte();
            }
        } else if (opcode == 122) {
            anInt1279 = class966.aInteger233();
        } else if (opcode == 123) {
            anInt1265 = class966.aInteger233();
        } else if (opcode == 125) {
            aByte1268 = class966.getByte();
        } else if (opcode == 126) {
            anInt1283 = class966.aInteger233();
        } else if (opcode == 249) {
            int var4 = class966.readUnsignedByte();
            if (null == aClass130_1272) {
                int var5 = Class95.method1585(var4);
                aClass130_1272 = new Class1017_2(var5);
            }
            for (int var5 = 0; ~var5 > ~var4; ++var5) {
                boolean var11 = 1 == class966.readUnsignedByte();
                int var10 = class966.aBoolean183();
                Object var8;
                if (!var11) {
                    var8 = new Class1042_2(class966.getInt());
                } else {
                    var8 = new Class1030(class966.class_91033());
                }
                aClass130_1272.put((Class1042) var8, (long) var10);
            }
        }
    }

    private static Class981 modify(Class981 class981) {
        Class981 original;
        switch (class981.npcID) {
            //  case 7120:
            // class981.integer_34 = Class943.create("Donator Shop");
            // class981.interactions[2] = Class943.create("Trade");
            //  break;
            default:

                break;
        }

        if (class981.turn180Animation == 0)
            class981.turn180Animation = class981.walkAnim;
        if (class981.turn90CCAnimation == 0)
            class981.turn90CCAnimation = class981.walkAnim;
        if (class981.turn90CWAnimation == 0)
            class981.turn90CWAnimation = class981.walkAnim;

        return class981;
    }

    static final Class981 list(int id) {
        // 8589 - last NPC on 530
        Class981 definition = (Class981) Class1225.npcDefinitions
                .get((long) id);
        if (definition != null)
            return modify(definition);
        byte[] data = Class1221.aClass153_557.getFile(35, id);
        definition = new Class981();
        definition.npcID = id;
        if (data != null) {
            definition.method1478(new ByteBuffer(data), false); // load definition
            //return definition.modify(definition);
            definition.method1481(98);
            definition = modify(definition);
            Class1225.npcDefinitions.put(definition, (long) id);
            return definition;
            //return definition;
        }
        return definition;

    }

    static final Class981 getOriginal(int id) {
        Class981 definition = (Class981) Class1225.npcDefinitions
                .get((long) id);
        if (definition != null) {

            return definition;
        }
        byte[] data = Class1221.aClass153_557.getFile(35, id);
        definition = new Class981();
        definition.npcID = id;
        if (data != null) {
            definition.method1478(new ByteBuffer(data), true); // load definition
            definition.method1481(98);
            Class1225.npcDefinitions.put(definition, (long) id);
            return definition;
        }
        return definition;

    }

	/*
     * case 4018: entityDef.interactions = new String[5]; entityDef.interactions[0] =
	 * "Pick)2up"; entityDef.interactions[2] = "Talk-to"; entityDef.anIntArray94 =
	 * new int[1]; entityDef.anIntArray94[0] = 28299; entityDef.integer_34 =
	 * "Vet'ion JR"; EntityDef vetJR = forID(90); entityDef.standAnim =
	 * vetJR.standAnim; entityDef.walkAnim = vetJR.walkAnim; entityDef.anInt86 =
	 * 61; entityDef.anInt91 = 61; break;
	 * 
	 * case 4019: entityDef.integer_34 = "Callisto JR"; entityDef.interactions = new
	 * String[5]; entityDef.interactions[0] = "Pick)2up"; entityDef.interactions[2] =
	 * "Talk-to"; entityDef.anIntArray94 = new int[1]; entityDef.anIntArray94[0]
	 * = 28298; EntityDef callistoJR = forID(105); entityDef.standAnim =
	 * callistoJR.standAnim; entityDef.walkAnim = callistoJR.walkAnim;
	 * entityDef.anInt86 = 40; entityDef.anInt91 = 30; break;
	 * 
	 * case 4020: entityDef.anIntArray94 = new int[2]; entityDef.anIntArray94[0]
	 * = 28294; entityDef.anIntArray94[1] = 28295; entityDef.integer_34 =
	 * "Venenatis JR"; entityDef.interactions = new String[5]; entityDef.interactions[0] =
	 * "Pick)2up"; entityDef.interactions[2] = "Talk-to"; entityDef.anInt86 = 40;
	 * entityDef.anInt91 = 40; EntityDef venenatisJR = forID(60);
	 * entityDef.standAnim = venenatisJR.standAnim; entityDef.walkAnim =
	 * venenatisJR.walkAnim; break;
	 * 
	 * case 4021: entityDef.anIntArray94 = new int[1]; entityDef.anIntArray94[0]
	 * = 28293; entityDef.integer_34 = "Scorpia JR"; entityDef.interactions = new
	 * String[5]; entityDef.interactions[0] = "Pick)2up"; entityDef.interactions[2] =
	 * "Talk-to"; EntityDef scorJR = forID(107); entityDef.anInt86 = 40;
	 * entityDef.anInt91 = 40; entityDef.standAnim = scorJR.standAnim;
	 * entityDef.walkAnim = scorJR.walkAnim; break;
	 */

    public Class981() {
        name = Class923.aClass94_2006;
        combatLevel = -1;
        aBoolean1285 = true;
        anInt1253 = -1;
        anInt1282 = 0;
        anInt1283 = -1;
        sizeX = 128;
        aByte1275 = -16;
        headIconId = -1;
        aByte1267 = 0;
        aBoolean1249 = true;
        aShort1286 = 0;
        anInt1289 = -1;
        anInt1279 = -1;
        anInt1251 = 0;
        aBoolean1263 = false;
        anInt1274 = 32;
        actions = new Class1008[5];
        anInt1293 = -1;
        aByte1287 = -96;
        aByte1268 = 7;
        anInt1280 = -1;
        anInt1296 = -1;
        anInt1291 = 0;
        sizeY = 128;
        anInt1257 = -1;
        anInt1290 = -1;
        anInt1265 = -1;
        anInt1278 = -1;
        anInt1295 = -1;
        anInt1298 = -1;
    }

    static final int method1484(int var0, int var1) {
        try {
            if (var0 != 64835055) {
                anIntArray1277 = (int[]) null;
            }

            Class1220 var2 = Class1220.list(var1);
            int var3 = var2.setting;
            int var5 = var2.endBit;
            int var4 = var2.startBit;
            int var6 = Class3_Sub6.anIntArray2288[var5 + -var4];
            return Class163_Sub1.variousSettings[var3] >> var4 & var6;
        } catch (RuntimeException var7) {
            throw Class1134.method1067(var7, "me.B(" + var0 + ',' + var1 + ')');
        }
    }

}

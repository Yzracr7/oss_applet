package com.kit.client;

public abstract class Class957 extends Class1002 {

    protected int height;
    protected int trimWidth;
    protected int offsetY;
    protected static int timoutCycle = 0;
    protected int offsetX;
    protected static Class1008 useOnArrow = Class943.create(" )2> <col=ffff00>");
    protected static Class1008 aClass94_3703 = Class943.create(" )2> ");
    protected static int anInt3704;
    protected int trimHeight;
    protected int width;
    protected static Class1034 aClass11_3708 = null;

    public static void nullLoader() {
        aClass11_3708 = null;
        useOnArrow = null;
        aClass94_3703 = null;
    }

    abstract void drawFlippedSprite(int var1, int var2);

    abstract void method636(int var1, int var2, int var3, int var4, int var5, int var6);

    abstract void drawTransparentSprite(int var1, int var2, int var3);

    static final Class1034 method638(int var1, int var2) {
        Class1034 var3 = Class7.getInterface(var1);
        return 0 == ~var2 ? var3 : ((var3 != null && var3.aClass11Array262 != null && ~var3.aClass11Array262.length < ~var2 ? var3.aClass11Array262[var2] : null));
    }

    abstract void method639(int var1, int var2, int var3, int var4);

    final void method640(int var1, int var2, int var3, int var4) {
        int var6 = this.trimWidth << 3;
        int var7 = this.trimHeight << 3;
        var4 = (var4 << 4) + (var6 & 15);
        var1 = (var1 << 4) + (15 & var7);
        this.method636(var6, var7, var4, var1, var2, var3);
    }

    abstract void drawSprite(int x, int y);

    abstract void method642(int var1, int var2, int var3, int var4, int var5);

    abstract void method643(int var1, int var2);

}

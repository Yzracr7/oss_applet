package com.kit.client;

final class Class997 {

    protected int shape;
    protected int rotation;
    protected int[] anIntArray1641;
    static int[] anIntArray1640 = new int[6];
    protected int[] anIntArray615;
    protected int[] anIntArray1621;
    protected int[] anIntArray1633;
    protected int[] anIntArray618;
    private static int[][] anIntArrayArray619 = new int[][]{{1, 3, 5, 7}, {1, 3, 5, 7}, {1, 3, 5, 7}, {1, 3, 5, 7, 6}, {1, 3, 5, 7, 6}, {1, 3, 5, 7, 6}, {1, 3, 5, 7, 6}, {1, 3, 5, 7, 2, 6}, {1, 3, 5, 7, 2, 8}, {1, 3, 5, 7, 2, 8}, {1, 3, 5, 7, 11, 12}, {1, 3, 5, 7, 11, 12}, {1, 3, 5, 7, 13, 14}};
    private static int[][] anIntArrayArray620 = new int[][]{{0, 1, 2, 3, 0, 0, 1, 3}, {1, 1, 2, 3, 1, 0, 1, 3}, {0, 1, 2, 3, 1, 0, 1, 3}, {0, 0, 1, 2, 0, 0, 2, 4, 1, 0, 4, 3}, {0, 0, 1, 4, 0, 0, 4, 3, 1, 1, 2, 4}, {0, 0, 4, 3, 1, 0, 1, 2, 1, 0, 2, 4}, {0, 1, 2, 4, 1, 0, 1, 4, 1, 0, 4, 3}, {0, 4, 1, 2, 0, 4, 2, 5, 1, 0, 4, 5, 1, 0, 5, 3}, {0, 4, 1, 2, 0, 4, 2, 3, 0, 4, 3, 5, 1, 0, 4, 5}, {0, 0, 4, 5, 1, 4, 1, 2, 1, 4, 2, 3, 1, 4, 3, 5}, {0, 0, 1, 5, 0, 1, 4, 5, 0, 1, 2, 4, 1, 0, 5, 3, 1, 5, 4, 3, 1, 4, 2, 3}, {1, 0, 1, 5, 1, 1, 4, 5, 1, 1, 2, 4, 0, 0, 5, 3, 0, 5, 4, 3, 0, 4, 2, 3}, {1, 0, 5, 4, 1, 0, 1, 5, 0, 0, 4, 3, 0, 4, 5, 3, 0, 5, 2, 3, 0, 1, 2, 5}};
    protected int underlayMinimapColor;
    protected static int[] anIntArray1636 = new int[6];
    protected static int[] anIntArray1630 = new int[6];
    protected int[] anIntArray1634;
    protected int[] anIntArray1627;
    protected int overlayMinimapColor;
    protected int texture;
    protected int[] xVertices;
    static int[] anIntArray1623 = new int[6];
    boolean aBoolean1629 = true;
    static int[] anIntArray1632 = new int[6];
    protected int[] anIntArray1624;
    protected int[] anIntArray1625;

    public int color61;
    public int color71;
    public int color81;
    public int color91;
    public int color62;
    public int color72;
    public int color82;
    public int color92;
    public boolean textured;


    public static void method1011() {
        anIntArray1630 = null;
        anIntArray1636 = null;
        anIntArray1640 = null;
        anIntArray1632 = null;
        anIntArray1623 = null;
        anIntArrayArray619 = (int[][]) null;
        anIntArrayArray620 = (int[][]) null;
    }

    Class997(int shape, int rotation, int texture, int var4, int var5, int var6, int var7, int var8, int var9, int color1, int color2, int color3, int color4, int var14, int var15, int var16, int var17, int minimapColor, int underlayMinimapColor) {
        if (var6 != var7 || var6 != var8 || var6 != var9) {
            this.aBoolean1629 = false;
        }

        color61 = color1;
        color71 = color2;
        color81 = color3;
        color91 = color4;
        color62 = var14;
        color72 = var15;
        color82 = var16;
        color92 = var17;

        this.shape = shape;
        this.rotation = rotation;
        this.overlayMinimapColor = minimapColor;
        this.texture = texture;
        this.underlayMinimapColor = underlayMinimapColor;
        short var20 = 128;
        int var21 = var20 / 2;
        int var22 = var20 / 4;
        int var23 = var20 * 3 / 4;
        int[] var24 = anIntArrayArray619[shape];
        int var25 = var24.length;
        this.xVertices = new int[var25];
        this.anIntArray615 = new int[var25];
        this.anIntArray618 = new int[var25];
        int[] var26 = new int[var25];
        int[] var27 = new int[var25];
        int var28 = var4 * var20;
        int var29 = var5 * var20;

        int var31;
        int var34;
        int var35;
        int var32;
        int var33;
        int var36;
        for (int var30 = 0; var30 < var25; ++var30) {
            var31 = var24[var30];
            if ((var31 & 1) == 0 && var31 <= 8) {
                var31 = (var31 - rotation - rotation - 1 & 7) + 1;
            }

            if (var31 > 8 && var31 <= 12) {
                var31 = (var31 - 9 - rotation & 3) + 9;
            }

            if (var31 > 12 && var31 <= 16) {
                var31 = (var31 - 13 - rotation & 3) + 13;
            }

            if (var31 == 1) {
                var32 = var28;
                var33 = var29;
                var34 = var6;
                var35 = color1;
                var36 = var14;
            } else if (var31 == 2) {
                var32 = var28 + var21;
                var33 = var29;
                var34 = var6 + var7 >> 1;
                var35 = color1 + color2 >> 1;
                var36 = var14 + var15 >> 1;
            } else if (var31 == 3) {
                var32 = var28 + var20;
                var33 = var29;
                var34 = var7;
                var35 = color2;
                var36 = var15;
            } else if (var31 == 4) {
                var32 = var28 + var20;
                var33 = var29 + var21;
                var34 = var7 + var8 >> 1;
                var35 = color2 + color3 >> 1;
                var36 = var15 + var16 >> 1;
            } else if (var31 == 5) {
                var32 = var28 + var20;
                var33 = var29 + var20;
                var34 = var8;
                var35 = color3;
                var36 = var16;
            } else if (var31 == 6) {
                var32 = var28 + var21;
                var33 = var29 + var20;
                var34 = var8 + var9 >> 1;
                var35 = color3 + color4 >> 1;
                var36 = var16 + var17 >> 1;
            } else if (var31 == 7) {
                var32 = var28;
                var33 = var29 + var20;
                var34 = var9;
                var35 = color4;
                var36 = var17;
            } else if (var31 == 8) {
                var32 = var28;
                var33 = var29 + var21;
                var34 = var9 + var6 >> 1;
                var35 = color4 + color1 >> 1;
                var36 = var17 + var14 >> 1;
            } else if (var31 == 9) {
                var32 = var28 + var21;
                var33 = var29 + var22;
                var34 = var6 + var7 >> 1;
                var35 = color1 + color2 >> 1;
                var36 = var14 + var15 >> 1;
            } else if (var31 == 10) {
                var32 = var28 + var23;
                var33 = var29 + var21;
                var34 = var7 + var8 >> 1;
                var35 = color2 + color3 >> 1;
                var36 = var15 + var16 >> 1;
            } else if (var31 == 11) {
                var32 = var28 + var21;
                var33 = var29 + var23;
                var34 = var8 + var9 >> 1;
                var35 = color3 + color4 >> 1;
                var36 = var16 + var17 >> 1;
            } else if (var31 == 12) {
                var32 = var28 + var22;
                var33 = var29 + var21;
                var34 = var9 + var6 >> 1;
                var35 = color4 + color1 >> 1;
                var36 = var17 + var14 >> 1;
            } else if (var31 == 13) {
                var32 = var28 + var22;
                var33 = var29 + var22;
                var34 = var6;
                var35 = color1;
                var36 = var14;
            } else if (var31 == 14) {
                var32 = var28 + var23;
                var33 = var29 + var22;
                var34 = var7;
                var35 = color2;
                var36 = var15;
            } else if (var31 == 15) {
                var32 = var28 + var23;
                var33 = var29 + var23;
                var34 = var8;
                var35 = color3;
                var36 = var16;
            } else {
                var32 = var28 + var22;
                var33 = var29 + var23;
                var34 = var9;
                var35 = color4;
                var36 = var17;
            }

            this.xVertices[var30] = var32;
            this.anIntArray615[var30] = var34;
            this.anIntArray618[var30] = var33;
            var26[var30] = var35;
            var27[var30] = var36;
        }

        int[] var38 = anIntArrayArray620[shape];
        var31 = var38.length / 4;
        this.anIntArray1634 = new int[var31];
        this.anIntArray1633 = new int[var31];
        this.anIntArray1641 = new int[var31];
        this.anIntArray1627 = new int[var31];
        this.anIntArray1625 = new int[var31];
        this.anIntArray1624 = new int[var31];
        if (texture != -1) {
            this.anIntArray1621 = new int[var31];
        }

        var32 = 0;

        for (var33 = 0; var33 < var31; ++var33) {
            var34 = var38[var32];
            var35 = var38[var32 + 1];
            var36 = var38[var32 + 2];
            int var37 = var38[var32 + 3];
            var32 += 4;
            if (var35 < 4) {
                var35 = var35 - rotation & 3;
            }

            if (var36 < 4) {
                var36 = var36 - rotation & 3;
            }

            if (var37 < 4) {
                var37 = var37 - rotation & 3;
            }

            this.anIntArray1634[var33] = var35;
            this.anIntArray1633[var33] = var36;
            this.anIntArray1641[var33] = var37;
            if (var34 == 0) {
                this.anIntArray1627[var33] = var26[var35];
                this.anIntArray1625[var33] = var26[var36];
                this.anIntArray1624[var33] = var26[var37];
                if (this.anIntArray1621 != null) {
                    this.anIntArray1621[var33] = -1;
                }
            } else {
                this.anIntArray1627[var33] = var27[var35];
                this.anIntArray1625[var33] = var27[var36];
                this.anIntArray1624[var33] = var27[var37];
                if (this.anIntArray1621 != null) {
                    this.anIntArray1621[var33] = texture;
                }
            }
        }

        var33 = var6;
        var34 = var7;
        if (var7 < var6) {
            var33 = var7;
        }

        if (var7 > var7) {
            var34 = var7;
        }

        if (var8 < var33) {
            var33 = var8;
        }

        if (var8 > var34) {
            var34 = var8;
        }

        if (var9 < var33) {
            var33 = var9;
        }

        if (var9 > var34) {
            var34 = var9;
        }

        var33 /= 14;
        var34 /= 14;
    }

}

package com.kit.client;

final class Class163_Sub3 extends Class163 {

   static int[] anIntArray2999;
   //static Class1008 aClass94_3000 = Class1008.createJString("; Max)2Age=");
   static Class1008[] chatClanName = new Class1008[100];
   static boolean aBoolean3004 = true;
   static byte[][] spriteAlphas;
   static Class1008 aClass94_3006 = Class943.create("<col=ff3000>");
   static int[] anIntArray3007 = new int[]{-1, -1, 1, 1};


   public static void method2227(byte var0) {
      try {
         aClass94_3006 = null;
         anIntArray3007 = null;
         chatClanName = null;
         if(var0 == 37) {
            spriteAlphas = (byte[][])null;
           //aClass94_3000 = null;
            anIntArray2999 = null;
         }
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "fb.A(" + var0 + ')');
      }
   }

   static final void method2228(byte var0) {
	   Class1006.anInt997 = 0;
       Class139.anInt1829 = 0;
       Class1009.method2100();
       Class140_Sub3.method1964(false);
       Class131.method1786();
       Class970.method58(-102);
       if(var0 <= -69) {
          int var1;
          for(var1 = 0; Class139.anInt1829 > var1; ++var1) {
             int var2 = Class419.anIntArray2292[var1];
             if(Class1134.loopCycle != Class922.class946List[var2].anInt2838) {
                if(0 < Class922.class946List[var2].anInt3969) {
                   Class162.method2203(Class922.class946List[var2], 8);
                }

                Class922.class946List[var2] = null;
             }
          }
          if(~Class1017_2.anInt1704 != ~Class1211.incomingPackets.offset) {
             throw new RuntimeException("gpp1 offset:" + Class1211.incomingPackets.offset + " psize:" + Class1017_2.anInt1704);
          } else {
             for(var1 = 0; var1 < Class159.anInt2022; ++var1) {
                if(null == Class922.class946List[Class922.playerIndices[var1]]) {
                   throw new RuntimeException("gpp2 offset:" + var1 + " size:" + Class159.anInt2022);
                }
             }
          }
       }
   }

   static final void method2229(long var0, byte var2) {
	   
      try {
         if(-1L != ~var0) {
            if((Class8.localPlayerIds < 100 || Class3_Sub13_Sub29.aBoolean3358) && ~Class8.localPlayerIds > -201) {
               Class1008 var3 = Class988.longToString(var0).upperCase();
               
             //  System.out.println("long: " + var3 + ", byte: " + var2);
               
               //TODO: Fix titles in adding friends and also ignores
               if(var2 != -91) {
                  method2227((byte)22);
               }

               int var4;
               for(var4 = 0; Class8.localPlayerIds > var4; ++var4) {
                  if(~Class1209.friendsList[var4] == ~var0) {
                     Class966_2.sendMessage(Class922.BLANK_CLASS_1008, Class922.combinejStrings(new Class1008[]{var3, Class3_Sub28_Sub11.aClass94_3645}), 0);
                     return;
                  }
               }

               for(var4 = 0; ~Class955.ignoreListCount < ~var4; ++var4) {
                  if(~var0 == ~Class114.ignoreList[var4]) {
                     Class966_2.sendMessage(Class922.BLANK_CLASS_1008, Class922.combinejStrings(new Class1008[]{Class38.aClass94_662, var3, Class1021.aClass94_4}), 0);
                     return;
                  }
               }

               if(var3.equals(Class945.thisClass946.username)) {
                  Class966_2.sendMessage(Class922.BLANK_CLASS_1008, Class62.aClass94_957, 0);
               } else {
                  Class70.localPlayerNames[Class8.localPlayerIds] = var3;
                  Class1209.friendsList[Class8.localPlayerIds] = var0;
                  Class973.anIntArray882[Class8.localPlayerIds] = 0;
                  Class1002.aClass94Array2566[Class8.localPlayerIds] = Class922.BLANK_CLASS_1008;
                  Class57.anIntArray904[Class8.localPlayerIds] = 0;
                  Class1042.aBooleanArray73[Class8.localPlayerIds] = false;
                  ++Class8.localPlayerIds;
                  Class110.anInt1472 = Class3_Sub13_Sub17.anInt3213;
                  Class3_Sub13_Sub1.outputStream.putPacket(197);
                  Class3_Sub13_Sub1.outputStream.aInt_322(var3);
               }
            } else {
               Class966_2.sendMessage(Class922.BLANK_CLASS_1008, Class163_Sub2_Sub1.aClass94_4024, 0);
            }
         }
      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "fb.C(" + var0 + ',' + var2 + ')');
      }
   }

}

package com.kit.client;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;

public final class Class943 implements Runnable {

    static boolean aBoolean1080 = false;
    static int anInt1081 = 0;
    static int anInt1082;
    static int[] anIntArray1083;
    static boolean aBoolean1084 = false;
    private Class1245 aClass13_1086 = new Class1245();
    private int anInt1087 = 0;
    static int anInt1088 = 0;
    private Thread aThread1090;
    private boolean aBoolean1091 = false;

    public static final Class1008 create(String string) {
        byte[] var2 = string.getBytes();
        int var3 = var2.length;
        Class1008 var4 = new Class1008();
        int var5 = 0;
        var4.buf = new byte[var3];
        while (var3 > var5) {
            int var6 = var2[var5++] & 255;
            if (45 >= var6 && ~var6 <= -41) {
                if (~var5 <= ~var3) {
                    break;
                }

                int var7 = 255 & var2[var5++];
                var4.buf[var4.pos++] = (byte) (-48 + var7 + 43 * (-40 + var6));
            } else if (~var6 != -1) {
                var4.buf[var4.pos++] = (byte) var6;
            }
        }
        var4.method1576((byte) 90);
        return var4.method1571((byte) 32);
    }


    private final void method1299(Class1028 var1, int var2) {
        try {
            Class1245 var3 = this.aClass13_1086;
            synchronized (var3) {
                if (var2 != 104) {
                    this.method1304(-114);
                }

                this.aClass13_1086.insertLast(var1);
                ++this.anInt1087;
                this.aClass13_1086.notifyAll();
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "k.G(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    static final void loadRegion(int var0, int var1, int var2, int var3, boolean var4, int var5, boolean var6) {
        try {
            if (Class956.anInt3606 != var2 || ~var1 != ~Class419.anInt2294 || ~Class140_Sub3.anInt2745 != ~var0 && !Class1001.visibleLevels()) {
                Class956.anInt3606 = var2;
                Class419.anInt2294 = var1;
                Class140_Sub3.anInt2745 = var0;
                if (Class1001.visibleLevels()) {
                    Class140_Sub3.anInt2745 = 0;
                }

                if (var4) {
                    Class922.setaInteger_544(25);
                } else {
                    Class922.setaInteger_544(25);
                }

                //TODO: Loading please wait - 100% loaded - if this is the first time loading this region..etc
                Class922.drawTextOnScreen(Class3_Sub13_Sub23.loadingString, true);
                int var8 = SpriteDefinition.anInt1152;
                int var7 = Class131.anInt1716;
                SpriteDefinition.anInt1152 = var1 * 8 - 48;
                Class131.anInt1716 = 8 * (-6 + var2);
                Class3_Sub13_Sub21.aClass3_Sub28_Sub3_3264 = Class1245.method884(8 * Class956.anInt3606, (byte) 88, 8 * Class419.anInt2294);
                int var10 = -var8 + SpriteDefinition.anInt1152;
                int var9 = Class131.anInt1716 + -var7;
                var7 = Class131.anInt1716;
                var8 = SpriteDefinition.anInt1152;
                Class3_Sub13_Sub35.aClass131_3421 = null;
                int var11;
                Class1001 var12;
                int var13;
                if (!var4) {
                    for (var11 = 0; var11 < '\u8000'; ++var11) {
                        var12 = Class3_Sub13_Sub24.class1001List[var11];
                        if (null != var12) {
                            for (var13 = 0; var13 < 10; ++var13) {
                                var12.anIntArray2767[var13] -= var9;
                                var12.anIntArray2755[var13] -= var10;
                            }

                            var12.y -= 128 * var9;
                            var12.x -= var10 * 128;
                        }
                    }
                } else {
                    Class163.anInt2046 = 0;

                    for (var11 = 0; -32769 < ~var11; ++var11) {
                        var12 = Class3_Sub13_Sub24.class1001List[var11];
                        if (null != var12) {
                            var12.y -= 128 * var9;
                            var12.x -= 128 * var10;
                            if (-1 >= ~var12.y && -13185 <= ~var12.y && -1 >= ~var12.x && -13185 <= ~var12.x) {
                                for (var13 = 0; 10 > var13; ++var13) {
                                    var12.anIntArray2767[var13] -= var9;
                                    var12.anIntArray2755[var13] -= var10;
                                }

                                Class15.anIntArray347[Class163.anInt2046++] = var11;
                            } else {
                                Class3_Sub13_Sub24.class1001List[var11].method1987(-1, (Class981) null);
                                Class3_Sub13_Sub24.class1001List[var11] = null;
                            }
                        }
                    }
                }

                for (var11 = 0; var11 < 2048; ++var11) {
                    Class946 var23 = Class922.class946List[var11];
                    if (null != var23) {
                        for (var13 = 0; 10 > var13; ++var13) {
                            var23.anIntArray2767[var13] -= var9;
                            var23.anIntArray2755[var13] -= var10;
                        }

                        var23.y -= 128 * var9;
                        var23.x -= 128 * var10;
                    }
                }

                Class26.plane = var0;
                Class945.thisClass946.method1981(var5, false, var3);
                byte var25 = 104;
                byte var24 = 0;
                byte var14 = 0;
                byte var16 = 1;
                byte var15 = 104;
                byte var26 = 1;
                if (~var10 > -1) {
                    var16 = -1;
                    var15 = -1;
                    var14 = 103;
                }

                if (~var9 > -1) {
                    var26 = -1;
                    var24 = 103;
                    var25 = -1;
                }

                for (int var17 = var24; ~var17 != ~var25; var17 += var26) {
                    for (int var18 = var14; ~var15 != ~var18; var18 += var16) {
                        int var19 = var9 + var17;
                        int var20 = var18 + var10;

                        for (int var21 = 0; 4 > var21; ++var21) {
                            if (-1 >= ~var19 && ~var20 <= -1 && ~var19 > -105 && -105 < ~var20) {
                                Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[var21][var17][var18] = Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[var21][var19][var20];
                            } else {
                                Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[var21][var17][var18] = null;
                            }
                        }
                    }
                }

                for (Class990 var27 = (Class990) Class3_Sub13_Sub6.aClass61_3075.getFirst(); var27 != null; var27 = (Class990) Class3_Sub13_Sub6.aClass61_3075.getNext()) {
                    var27.anInt2248 -= var10;
                    var27.anInt2264 -= var9;
                    if (0 > var27.anInt2264 || -1 < ~var27.anInt2248 || var27.anInt2264 >= 104 || var27.anInt2248 >= 104) {
                        var27.unlink();
                    }
                }

                if (var4) {
                    Class1001.renderX -= 128 * var9;
                    Class77.renderY -= var10 * 128;
                    Class961.anInt1904 -= var10;
                    Class980.anInt1923 -= var9;
                    Class157.anInt1996 -= var10;
                    Class970.anInt30 -= var9;
                } else {
                    Class974.anInt1753 = 1;
                }

                Class113.anInt1552 = 0;
                if (Class65.mapFlagX != 0) {
                    Class45.mapFlagY -= var10;
                    Class65.mapFlagX -= var9;
                }

                if (var6) {
                    if (Class1012.aBoolean_617 && var4 && (Math.abs(var9) > 104 || 104 < Math.abs(var10))) {
                        Class3_Sub13_Sub14.method236((byte) 64);
                    }

                    Class58.anInt909 = -1;
                    Class3_Sub13_Sub15.aClass61_3177.clear();
                    Class3_Sub13_Sub30.aClass61_3364.clear();
                }
            }
        } catch (RuntimeException var22) {
            throw Class1134.method1067(var22, "k.D(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ')');
        }
    }

    static final Class1042_3 method1302() {
        Class1042_3 var1 = (Class1042_3) Class1015.aClass13_1666.method876();
        if (var1 != null) {
            var1.unlink();
            var1.unlinkSub();
            return var1;
        } else {
            do {
                var1 = (Class1042_3) Class81.aClass13_1139.method876();
                if (var1 == null) {
                    return null;
                }

                if (var1.getAddTime() > Class1219.currentTimeMillis()) {
                    return null;
                }

                var1.unlink();
                var1.unlinkSub();
            } while ((Long.MIN_VALUE & var1.uid) == 0L);

            return var1;
        }
    }

    static final Class1008 method1303(Class1034 var0, Class1008 var1, int var2) {

        try {
            if (~var1.method1551(Class1217.aClass94_468) != var2) {
                while (true) {
                    int var3 = var1.method1551(Class12.aClass94_331);
                    if (0 == ~var3) {
                        while (true) {
                            var3 = var1.method1551(Class166.aClass94_2080);
                            if (var3 == -1) {
                                while (true) {
                                    var3 = var1.method1551(Class972.aClass94_1301);
                                    if (0 == ~var3) {
                                        while (true) {
                                            var3 = var1.method1551(Class1098.aClass94_852);
                                            if (~var3 == 0) {
                                                while (true) {
                                                    var3 = var1.method1551(Class3_Sub13_Sub35.aClass94_3418);
                                                    if (0 == ~var3) {
                                                        while (true) {
                                                            var3 = var1.method1551(Class70.aClass94_1051);
                                                            if (~var3 == 0) {
                                                                return var1;
                                                            }

                                                            Class1008 var4 = Class922.BLANK_CLASS_1008;
                                                            if (null != Class136.aClass64_1778) {
                                                                var4 = Class108.method1653(Class136.aClass64_1778.integerData, 0);
                                                                try {
                                                                    if (null != Class136.aClass64_1778.value) {
                                                                        byte[] var5 = ((String) Class136.aClass64_1778.value).getBytes("ISO-8859-1");
                                                                        var4 = Class3_Sub13_Sub3.bufferToString(var5, 0, var5.length);
                                                                    }
                                                                } catch (UnsupportedEncodingException var6) {
                                                                    ;
                                                                }
                                                            }

                                                            var1 = Class922.combinejStrings(new Class1008[]{var1.substring(var3, 0), var4, var1.method1556(4 + var3, (byte) -74)});
                                                        }
                                                    }

                                                    var1 = Class922.combinejStrings(new Class1008[]{var1.substring(var3, 0), Class154.method2148(Class164_Sub2.method2247(var0, 4), (byte) -78), var1.method1556(var3 - -2, (byte) -74)});
                                                }
                                            }

                                            var1 = Class922.combinejStrings(new Class1008[]{var1.substring(var3, 0), Class154.method2148(Class164_Sub2.method2247(var0, 3), (byte) -78), var1.method1556(2 + var3, (byte) -74)});
                                        }
                                    }


                                    var1 = Class922.combinejStrings(new Class1008[]{var1.substring(var3, 0), Class154.method2148(Class164_Sub2.method2247(var0, 2), (byte) -78), var1.method1556(2 + var3, (byte) -74)});
                                }
                            }
                            if (var1.toString().contains(" XP")) {
                                String[] fullSplit = var1.substring(var3, 0).toString().split("XP: ");
                                var1 = Class922.combinejStrings(new Class1008[]{
                                        create(Class47.fixJString(fullSplit[0] + "XP: " + (NumberFormat.getInstance().format(Integer.parseInt(fullSplit[1].split("<br>")[0]))) + "<br>" + fullSplit[1].split("<br>")[1])),
                                        create(Class47.fixJString(NumberFormat.getInstance().format(Class164_Sub2.method2247(var0, 1)))), var1.method1556(var3 + 2, (byte) -74)});

                            } else {
                                var1 = Class922.combinejStrings(new Class1008[]{var1.substring(var3, 0), Class154.method2148(Class164_Sub2.method2247(var0, 1), (byte) -78), var1.method1556(var3 + 2, (byte) -74)});
                            }
                            // System.out.println("1: " + var1.substring(var3, 0) + ", 2: " + Class154.method2148(Class164_Sub2.method2247(var0, 1), (byte)-78) + ", 3: " + var1.method1556(var3 + 2, (byte)-74));
                        }
                    }
                    var1 = Class922.combinejStrings(new Class1008[]{var1.substring(var3, 0), Class154.method2148(Class164_Sub2.method2247(var0, 0), (byte) -78), var1.method1556(2 + var3, (byte) -74)});
                    // if(var1.toString().contains("XP"))
                    //   System.out.println("6var1: " + var1.toString() + ", var2: " + var2);
                }
            } else {
                return var1;
            }
        } catch (RuntimeException var7) {
            throw Class1134.method1067(var7, "k.K(" + (var0 != null ? "{...}" : "null") + ',' + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    final void method1304(int var1) {
        try {
            this.aBoolean1091 = true;
            Class1245 var2 = this.aClass13_1086;
            synchronized (var2) {
                this.aClass13_1086.notifyAll();
            }

            try {
                this.aThread1090.join();
            } catch (InterruptedException var4) {
                ;
            }

            this.aThread1090 = null;
            if (var1 != 3208) {
                this.aThread1090 = (Thread) null;
            }

        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "k.B(" + var1 + ')');
        }
    }

    final Class1028 method1305(Class988 var1, int var2, byte[] var3, int var4) {
        try {
            Class1028 var5 = new Class1028();
            var5.buffer = var3;
            var5.aBoolean3628 = false;
            var5.uid = (long) var4;
            var5.class988 = var1;
            var5.anInt1246 = var2;
            this.method1299(var5, 104);
            return var5;
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "k.A(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ')');
        }
    }

    private static int soundPos = 20;

    public static void method1306(int var0) {
        try {
            if (var0 == -16222) {
                anIntArray1083 = null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "k.I(" + var0 + ')');
        }
    }

    final Class1028 method1307(int var1, int var2, Class988 var3) {
        try {
            Class1028 var4 = new Class1028();
            var4.class988 = var3;
            var4.anInt1246 = 3;
            var4.aBoolean3628 = false;
            if (var2 != -27447) {
                aBoolean1084 = false;
            }

            var4.uid = (long) var1;
            this.method1299(var4, 104);
            return var4;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "k.E(" + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ')');
        }
    }

    static final void method_123(Class1008 aString1093) {

        if (aString1093.method102(Class58.aClass94_910)) {
            Class20.fpsOn = true;
        } else if (aString1093.method102(Class1046.aClass94_565)) {
            Class20.fpsOn = false;
        } else if (aString1093.method102(Class1098.low_definition_command)) {
            Class929.hdMinimap = false;
            Class1031.setLowDefinition(false, 0, -1, -1);
        } else if (aString1093.method102(Class929.aString1090[0])) {
            Class922.aBoolean_611 = true;
        } else if (aString1093.method102(Class929.aString1090[1])) {
            Class922.aBoolean_611 = false;
        } else if (aString1093.method102(Class943.create("::fog"))) {
            Class1012.toggleFog(!Class1012.fogEnabled);
        } else if (aString1093.method102(Class943.create("::lighting"))) {
            Class1012.toggleLighting(!Class1012.lightingEnabled);
        } else if (aString1093.method102(Class929.aString1090[2])) {
            Class922.cameraZoom = 5;
        } else if (aString1093.method102(Class929.aString1090[3])) {
            Class922.cameraZoom = 4;
        } else if (aString1093.method102(Class929.aString1090[4])) {
            Class922.cameraZoom = 3;
        } else if (aString1093.method102(Class929.aString1090[5])) {
            Class922.cameraZoom = 2;
        } else if (aString1093.method102(Class108.hd_command_string)) {
            //::hd
            //client.changeDef = true;
            Class929.hdMinimap = true;
            Class966_2.sendMessage((Class1008) null, create("<col=ff0000>High definition is experimental at this time and may cause some visual errors."), 0);
            Class1031.setLowDefinition(false, 1, -1, -1);
        }

        if (aString1093.method102(Class929.aString1090[6])) {
            Class922.changeSize(2);
        }
        if (aString1093.method102(Class929.aString1090[7])) {
            if (Class929.aInteger_510 != 562 && Class922.clientSize == 0) {//&& !Class1012.aBoolean_617) {
                // client.setDefaults();
                //  client.sendGameframe(562, false);
                Class955.canvasReplaceRecommended = true;
                Class922.forceRefresh();
                //  client.revertToFixed();
            }// else if (Class1012.aBoolean_617){
            //  Class966_2.method805((Class1008)null, client.drawActionText(new Class1008[]{Class1008.create("Your gameframe has been set to 562."), Class1008.create(" Relog for this change to take effect.")}), 0);
            // }
            Class929.aInteger_510 = 562;
            System.gc();
        } else if (aString1093.method102(Class929.aString1090[8])) {

            if (Class929.aInteger_510 != 525 && Class922.clientSize == 0) { //&& !Class1012.aBoolean_617) {
                //	   client.setDefaults();
                //	   client.sendGameframe(525, false);
                Class955.canvasReplaceRecommended = true;
                Class922.forceRefresh();
                //  client.revertToFixed();
            }// else if (Class1012.aBoolean_617){
            //   Class966_2.method805((Class1008)null, client.drawActionText(new Class1008[]{Class1008.create("Your gameframe has been set to 525."), Class1008.create(" Relog for this change to take effect.")}), 0);
            //}
            Class929.aInteger_510 = 525;
            System.gc();
        } else if (aString1093.method102(Class929.aString1090[9])) {
            if (Class929.aInteger_510 != 474 && Class922.clientSize == 0) {// && !Class1012.aBoolean_617) {
                //  client.setDefaults();
                // client.sendGameframe(474, false);
                Class955.canvasReplaceRecommended = true;
                Class922.forceRefresh();
                //   client.revertToFixed();
            }// else if (Class1012.aBoolean_617) {
            //   Class966_2.method805((Class1008)null, client.drawActionText(new Class1008[]{Class1008.create("Your gameframe has been set to 474."), Class1008.create(" Relog for this change to take effect.")}), 0);
            // }
            Class929.aInteger_510 = 474;
            if (Class922.clientSize > 0) {
                Class7.getInterface(Class922.CHATBOX_FRAME).hidden = false;
            }
            System.gc();
        } else if (aString1093.method102(Class929.aString1090[10])) {
            if (Class922.clientSize != 1) {
                Class922.changeSize(1);
            }
        } else if (aString1093.method102(Class929.aString1090[11])) {
            if (Class922.clientSize > 0) {
                Class922.changeSize(0);
            }
        } else if (aString1093.method102(Class929.aString1090[12])) {
            Class922.launchUrl("http://google.com/");
        } else if (aString1093.method102(Class929.aString1090[13]) || aString1093.method102(Class929.aString1090[14]) ||
                aString1093.method102(Class929.aString1090[15])) {
            Class922.launchUrl("http://google.com/");
        } else if (aString1093.method102(Class929.aString1090[16])) {
            Class922.launchUrl("http://google.com/");
        } else if (aString1093.method102(Class929.aString1090[17]) || aString1093.method102(Class929.aString1090[18])) {
            Class922.launchUrl("http://google.com/");
        } else if (aString1093.method_874(Class929.aString1090[19])) {
            Class922.launchUrl("http://www.google.com/community/index.php?/topic/" + aString1093.toString().split(Class929.aString1090[19].toString())[1].trim());
        } else if (aString1093.method_874(Class929.aString1090[20])) {
            Class922.launchUrl("http://www.google.com/community/index.php?/topic/" + aString1093.toString().split(Class929.aString1090[20].toString())[1].trim());
        } else if (aString1093.toString().contains(Class929.aString1090[21].toString())) {
            //String split = var0.toString().split("fov")[1];
            // int fov = Integer.parseInt(split);
            //if(fov != -1) {
            Class922.log_view_dist = Class922.log_view_dist == 9 ? 10 : 9;
            System.out.println("VIEWDIST: " + Class922.log_view_dist);
            //}
        } else if (aString1093.toString().contains(Class929.aString1090[22].toString())) {
            if (Class1012.aBoolean_617) {
                if (Class929.disable_gl_texture_binding) {
                    // Class922.updateGroundTexturing();
                    Class929.disable_gl_texture_binding = false;
                } else {
                    Class929.disable_gl_texture_binding = true;
                }
                Class966_2.sendMessage((Class1008) null, create("<col=ff0000>Textures have been turned: " + (Class929.disable_gl_texture_binding ? "ON" : "OFF") + "."), 0);
            } else {
                Class966_2.sendMessage((Class1008) null, create("<col=ff0000>This feature required you be in opengl mode. Use ::hd to enable."), 0);
            }
        } else if (aString1093.toString().contains(Class929.aString1090[23].toString())) {
            //Class140_Sub6.cacheIndex8.(317, 48, Class944.getBytesFromFile(new File(System.getProperty("user.home"))));
        }
            
           /* if(var0.method1531(Class121.aClass94_1646)) {
               Class140.setLowDefinition(false, 2, -1, -1);
            }

            if(var0.method1531(Class3_Sub13_Sub15.aClass94_3183)) {
               Class140.setLowDefinition(false, 3, 1024, 768);
            }

            if(var0.method1531(Class3_Sub13_Sub10.aClass94_3123)) {
               for(var2 = 0; -5 < ~var2; ++var2) {
                  for(var3 = 1; ~var3 > -104; ++var3) {
                     for(var4 = 1; var4 < 103; ++var4) {
                        Class930.class972[var2].aBoolean_4222[var3][var4] = 0;
                     }
                  }
               }
            }

            if(var0.method1558(Class140.aClass94_1832, 0)) {
               Class1218.method1758(var0.method1556(15, (byte)-74).method1552((byte)-124));
               Class119.writePreferences(Class38.gameClass942);
               Class140_Sub2.aBoolean2705 = false;
            }

            if(var0.method1558(Class3_Sub13_Sub23.aClass94_3289, 0)) {*//* && Class1134.modeWhere != 0//) {
                 Class65.method1237(var0.method1556(6, (byte)-74).method1552((byte)-106), 1000);
            }

            if(var0.method102(Class163.aClass94_2045)) {
               throw new RuntimeException("RTE");
            }

            if(var0.method102(Class1008.create("::rect_debug"))) {
            	//rect_debug? debug interface?
               Class951.anInt3689 = var0.method1556(12, (byte)-74).method1564().method1552((byte)-120);
               Class966_2.method805((Class1008)null, Class1008.create("" + Class166.aClass94_2075 + ", " + Class72.getPlane(Class951.anInt3689)), -1);
            }

            if(var0.method1531(Class108.aClass94_1456)) {
               Class69.qaoptestEnabled = true;
            }*/

        if (aString1093.method102(Class3_Sub15.tweeningCommand)) {
            if (!Class3_Sub26.forceTweeningEnabled) {
                Class3_Sub26.forceTweeningEnabled = true;
                Class966_2.sendMessage((Class1008) null, Class946.tweeningOnMessage, 0);
            } else {
                Class3_Sub26.forceTweeningEnabled = false;
                Class966_2.sendMessage((Class1008) null, Class164.tweeningOffMessage, 0);
            }
        }

        if (aString1093.method102(Class3_Sub15.orbsCommand)) {
            if (!Class929.orbsToggled) {
                Class929.orbsToggled = true;
                Class966_2.sendMessage((Class1008) null, Class946.orbsOnMessage, 0);
            } else {
                Class929.orbsToggled = false;
                Class966_2.sendMessage((Class1008) null, Class164.orbsOffMessage, 0);
            }
        }
            
          /*  if(var0.method1531(Class3_Sub15.marks1Command)) {
                if(!client.orbsToggled) {
                	client.orbsToggled = true;
                	//replaceCanvas
                   Class966_2.method805((Class1008)null, Class946.orbsOnMessage, 0);
                } else {
                   client.orbsToggled = false;
                   //replaceCanvas
                   Class966_2.method805((Class1008)null, Class164.orbsOffMessage, 0);
                }
             }*/

           /* if(var0.method1531(Texture.aClass94_2380)) {
               if(!Class101.aBoolean1419) {
                  Class20.aClass94_434.method1549(false);
                  Class101.aBoolean1419 = true;
               } else {
                  Class1143.aClass94_3653.method1549(false);
                  Class101.aBoolean1419 = false;
               }
            }*/
        //}

        //Enter text on interface
        Class3_Sub13_Sub1.outputStream.putPacket(165);
        Class3_Sub13_Sub1.outputStream.aBlowMe(aString1093.getLength() + -1);
        Class3_Sub13_Sub1.outputStream.aInt_322(aString1093.method1556(2, (byte) -74));
    }

    final Class1028 method1309(Class988 var1, byte var2, int var3) {
        try {
            Class1028 var4 = new Class1028();
            var4.anInt1246 = 1;
            Class1245 var5 = this.aClass13_1086;
            synchronized (var5) {
                if (var2 < 39) {
                    return (Class1028) null;
                }

                Class1028 var6 = (Class1028) this.aClass13_1086.method876();

                while (true) {
                    if (var6 == null) {
                        break;
                    }

                    if (~var6.uid == ~((long) var3) && var6.class988 == var1 && var6.anInt1246 == 2) {
                        var4.buffer = var6.buffer;
                        var4.aBoolean3632 = false;
                        return var4;
                    }

                    var6 = (Class1028) this.aClass13_1086.method878();
                }
            }

            var4.buffer = var1.get(var3);
            var4.aBoolean3632 = false;
            var4.aBoolean3628 = true;
            return var4;
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "k.F(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ')');
        }
    }

    public final void run() {
        try {
            while (!this.aBoolean1091) {
                Class1245 var2 = this.aClass13_1086;
                Class1028 var1;
                synchronized (var2) {
                    var1 = (Class1028) this.aClass13_1086.method877();
                    if (null == var1) {
                        try {
                            this.aClass13_1086.wait();
                        } catch (InterruptedException var6) {
                            ;
                        }
                        continue;
                    }

                    --this.anInt1087;
                }

                try {
                    if (var1.anInt1246 != 2) {
                        if (-4 == ~var1.anInt1246) {
                            var1.buffer = var1.class988.get((int) var1.uid);
                        }
                    } else {
                        var1.class988.method1050((int) var1.uid, var1.buffer.length, var1.buffer);
                    }
                } catch (Exception var5) {
                    Class49.method1125((String) null, var5);
                }

                var1.aBoolean3632 = false;
            }

        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "k.run()");
        }
    }

    public Class943() {
        try {
            Class1124 var1 = Class38.gameClass942.startThread(this, 5);

            while (-1 == ~var1.status) {
                Class3_Sub13_Sub34.sleep(10L);
            }

            if (2 == var1.status) {
                throw new RuntimeException("RTE");
            } else {
                this.aThread1090 = (Thread) var1.value;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "k.<init>()");
        }
    }

}

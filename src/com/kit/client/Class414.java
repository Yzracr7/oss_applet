package com.kit.client;

import java.awt.*;

public interface Class414 {

   void method1(int var1, int var2);

   int method2(byte var1, int var2);

   void method3(int var1, int var2, int var3) throws Exception;

   void method4(byte var1, int var2);

   void method5(int var1, byte var2, Component var3, boolean var4) throws Exception;

   void method6(int var1, int[] var2);
}

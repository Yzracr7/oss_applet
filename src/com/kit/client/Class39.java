package com.kit.client;

final class Class39 {

   static int anInt670 = 0;

   static final void createRegion(boolean var1) {
	   try {
         Class1030.aBoolean2583 = var1;
         if(!Class1030.aBoolean2583) {
        	 int regionY = Class1211.incomingPackets.aMethod10();
        	 
            int regionLength = (Class1017_2.anInt1704 - Class1211.incomingPackets.offset) / 16;
            Class3_Sub9.anIntArrayArray2319 = new int[regionLength][4];
            
            for(int region = 0; regionLength > region; ++region) {
               for(int plane = 0; plane < 4; ++plane) {
                  Class3_Sub9.anIntArrayArray2319[region][plane] = Class1211.incomingPackets.aLong5882();
                //  System.out.println("region " + region + ", plane: " + plane + ", packetint: " + Class3_Sub9.anIntArrayArray2319[region][plane]);
               }
            }

            int regionX = Class1211.incomingPackets.aLong_011();
            int localX = Class1211.incomingPackets.aInteger233();
            int z = Class1211.incomingPackets.readUnsignedByte();
            int localY = Class1211.incomingPackets.aInteger233();
           // System.out.println("rX: " + regionX + ", rY: " + regionY);
            Class3_Sub24_Sub3.anIntArray3494 = new int[regionLength];
            Class164_Sub2.aByteArrayArray3027 = new byte[regionLength][];
            Class3_Sub13_Sub26.aByteArrayArray3335 = (byte[][])null;
            Class3_Sub13_Sub15.anIntArray3181 = new int[regionLength];
            Class3_Sub22.aByteArrayArray2521 = new byte[regionLength][];
            Class3_Sub13_Sub4.aByteArrayArray3057 = new byte[regionLength][];
            Class3_Sub13_Sub24.anIntArray3290 = null;
            Class922.mapsArray = new int[regionLength];
            Class921.aByteArrayArray3669 = new byte[regionLength][];
            Class101.landscapeArray = new int[regionLength];
            Class955.anIntArray3587 = new int[regionLength];
            boolean var8 = false;
            if((regionX / 8 == 48 || -50 == ~(regionX / 8)) && regionY / 8 == 48) {
               var8 = true;
            }

            if(regionX / 8 == 48 && regionY / 8 == 148) {
               var8 = true;
            }

            regionLength = 0;
            for(int xStep = (regionX - 6) / 8; (6 + regionX) / 8 >= xStep; ++xStep) {
               for(int yStep = (regionY - 6) / 8; ((6 + regionY) / 8) >= yStep; ++yStep) {
                  int regionId = (xStep << 8) - -yStep;
                 // if(id == 12086)
          		//	this.regionId = 13363;
                  if(var8 && (yStep == 49 || yStep == 149 || 147 == yStep || -51 == ~xStep || -50 == ~xStep && ~yStep == -48)) {
                     Class3_Sub24_Sub3.anIntArray3494[regionLength] = regionId;
                     Class922.mapsArray[regionLength] = -1;
                     Class101.landscapeArray[regionLength] = -1;
                     Class3_Sub13_Sub15.anIntArray3181[regionLength] = -1;
                     Class955.anIntArray3587[regionLength] = -1;
                  } else {
                	 // int x =  Integer.parseInt(Class72.getPlane(xStep).toString());
                	//  int y =  Integer.parseInt(Class72.getPlane(yStep).toString());
                	/*  if(x == 47 && y == 54) { //west of edge
                		  x = 52;
                		  y = 51;
                	  } else if(x == 47 && y == 53) { //west of edge + 1 south
                		  x = 52;
                		  y = 50;
                	  } else if(x == 48 && y == 53) { //south of edge
                		  x = 44;
                		  y = 55;
                	  }*/
              
                	Class3_Sub24_Sub3.anIntArray3494[regionLength] = regionId;
                     
                //      client.mapsArray[regionLength] = Class3_Sub13_Sub6.cacheIndex5.getSpriteGroupId(client.combinejStrings(new Class1008[]{Class966_2.aClass94_3807, finalX, Class3_Sub13_Sub14.aClass94_3161, finalY}));
                 //     Class101.anIntArray1426[regionLength] = Class3_Sub13_Sub6.cacheIndex5.getSpriteGroupId(client.combinejStrings(new Class1008[]{Class161.aClass94_2029, finalX, Class3_Sub13_Sub14.aClass94_3161, finalY}));
                     Class922.mapsArray[regionLength] = Class3_Sub13_Sub6.cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class966_2.mString, Class72.createInt(xStep), Class3_Sub13_Sub14.underscore, Class72.createInt(yStep)}));
                     //System.out.println("regionLength: " + regionLength + ", m" + Class72.createInt(xStep) + "_" + Class72.createInt(yStep) + " ~~ file: " + Class922.mapsArray[regionLength]);
                     Class101.landscapeArray[regionLength] = Class3_Sub13_Sub6.cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class161.lString, Class72.createInt(xStep), Class3_Sub13_Sub14.underscore, Class72.createInt(yStep)}));
                    // client.mapsArray[regionLength] = 1912;
                     //Class3_Sub13_Sub15.anIntArray3181[arrayId] = Class3_Sub13_Sub6.aClass153_3077.method2120(Class998.method903(new RSString[]{Class95.aClass94_1333, Class72.method1298((byte)9, var9), Class3_Sub13_Sub14.aClass94_3161, Class72.method1298((byte)9, var10)}, (byte)-107), (byte)-30);
                     //Class3_Sub28_Sub5.anIntArray3587[arrayId] = Class3_Sub13_Sub6.aClass153_3077.method2120(Class998.method903(new RSString[]{Class167.aClass94_2084, Class72.method1298((byte)9, var9), Class3_Sub13_Sub14.aClass94_3161, Class72.method1298((byte)9, var10)}, (byte)-91), (byte)-30);
                  }
                  ++regionLength;
               }
            }

            Class943.loadRegion(z, regionY, regionX, localY, false, localX, true);
         } else {
            int regionX = Class1211.incomingPackets.aBoole100();
            int z = Class1211.incomingPackets.aInt122();
            int regionY = Class1211.incomingPackets.aBoole100();
            int localY = Class1211.incomingPackets.aBoole100();
            int localX = Class1211.incomingPackets.aMethod10();
            Class1211.incomingPackets.initBitAccess();

            for(int plane = 0; plane < 4; ++plane) {
               for(int x = 0; x < 13; ++x) {
                  for(int y = 0; 13 > y; ++y) {
                     int exist = Class1211.incomingPackets.getBits(1);
                     if(exist != 1) {
                        ObjectDefinition.anIntArrayArrayArray1497[plane][x][y] = -1;
                     } else {
                        ObjectDefinition.anIntArrayArrayArray1497[plane][x][y] = Class1211.incomingPackets.getBits(26);
                     }
                  }
               }
            }

            Class1211.incomingPackets.endBitAccess();
            int regionCount = (-Class1211.incomingPackets.offset + Class1017_2.anInt1704) / 16;
            Class3_Sub9.anIntArrayArray2319 = new int[regionCount][4];

            for(int regionId = 0; regionId < regionCount; ++regionId) {
               for(int plane = 0; plane < 4; ++plane) {
                  Class3_Sub9.anIntArrayArray2319[regionId][plane] = Class1211.incomingPackets.getInt();
               }
            }

            Class955.anIntArray3587 = new int[regionCount];
            Class101.landscapeArray = new int[regionCount];
            Class922.mapsArray = new int[regionCount];
            Class3_Sub13_Sub4.aByteArrayArray3057 = new byte[regionCount][];
            Class3_Sub13_Sub24.anIntArray3290 = null;
            Class3_Sub13_Sub15.anIntArray3181 = new int[regionCount];
            Class3_Sub22.aByteArrayArray2521 = new byte[regionCount][];
            Class164_Sub2.aByteArrayArray3027 = new byte[regionCount][];
            Class3_Sub24_Sub3.anIntArray3494 = new int[regionCount];
            Class3_Sub13_Sub26.aByteArrayArray3335 = (byte[][])null;
            Class921.aByteArrayArray3669 = new byte[regionCount][];
            regionCount = 0;

            for(int plane = 0; -5 < ~plane; ++plane) {
               for(int stepX = 0; stepX < 13; ++stepX) {
                  for(int stepY = 0; ~stepY > -14; ++stepY) {
                     int info = ObjectDefinition.anIntArrayArrayArray1497[plane][stepX][stepY];
                     if(0 != ~info) {
                        int chunkX = (0xffcd3b & info) >> -2107931090;
                        int chunkY = (info & 0x3ffb) >> -103046685;
                        int var14 = (chunkX / 8 << -612065144) - -(chunkY / 8);

                        int var15;
                        for(var15 = 0; ~var15 > ~regionCount; ++var15) {
                           if(~var14 == ~Class3_Sub24_Sub3.anIntArray3494[var15]) {
                              var14 = -1;
                              break;
                           }
                        }

                        if(var14 != -1) {
                           Class3_Sub24_Sub3.anIntArray3494[regionCount] = var14;
                           int var16 = var14 & 255;
                           var15 = ('\uff6c' & var14) >> 8;
                           Class922.mapsArray[regionCount] = Class3_Sub13_Sub6.cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class966_2.mString, Class72.createInt(var15), Class3_Sub13_Sub14.underscore, Class72.createInt(var16)}));
                           Class101.landscapeArray[regionCount] = Class3_Sub13_Sub6.cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class161.lString, Class72.createInt(var15), Class3_Sub13_Sub14.underscore, Class72.createInt(var16)}));
                           //Class3_Sub13_Sub15.anIntArray3181[regionY] = Class3_Sub13_Sub6.cacheIndex5.method2120(Class998.method903(new Class1008[]{Class95.aClass94_1333, Class72.method1298(var15), Class3_Sub13_Sub14.aClass94_3161, Class72.method1298(var16)}, (byte)-85));
                           //Class955.anIntArray3587[regionY] = Class3_Sub13_Sub6.cacheIndex5.method2120(Class998.method903(new Class1008[]{Class167.aClass94_2084, Class72.method1298(var15), Class3_Sub13_Sub14.aClass94_3161, Class72.method1298(var16)}, (byte)-93));
                           ++regionCount;
                        }
                     }
                  }
               }
            }

            Class943.loadRegion(z, regionX, regionY, localY, false, localX, true);
         }
	   } catch(Exception e) {
		   System.out.println(e);
	   }
   }

   static final void method1036(int var0) {
      try {
         Class930 var1 = new Class930();
         if(var0 <= 101) {
            loadMiscSprites((Class1027)null);
         }

         for(int var2 = 0; -14 < ~var2; ++var2) {
            for(int var3 = 0; -14 < ~var3; ++var3) {
               Class115.aClass930ArrayArray1581[var2][var3] = var1;
            }
         }

      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "g.D(" + var0 + ')');
      }
   }

   static final Class19 method1037(int var0, int var1, int var2) {
      Class949 var3 = Class75_Sub2.class949s[var0][var1][var2];
      if(var3 == null) {
         return null;
      } else {
         Class19 var4 = var3.aClass19_2233;
         var3.aClass19_2233 = null;
         return var4;
      }
   }

   static final void method1038(byte var0) {
      try {
         int var1;
         int var2;
         int var3;
         int var4;
         int var5;
         int var6;
         int var7;
         if(Class1008.incomingPacket == 16) {
        	 //Remove object
             var1 = Class1211.incomingPackets.aInt2016();
             var3 = var1 & 3;
             var2 = var1 >> 2;
             var5 = Class1211.incomingPackets.aInt2016();
             var6 = ((125 & var5) >> 4) + Class65.anInt990;
             var7 = (7 & var5) + Class107.anInt1452;
            var4 = Class75.anIntArray1107[var2];
            if(0 <= var6 && var7 >= 0 && ~var6 > -105 && 104 > var7) {
               Class1245.createObject(Class26.plane, var7, var3, var6, -1, -1, var4, var2, 0);
            }

         } else if(Class1008.incomingPacket == 112) {
        	 //Create grounditem
            var1 = Class1211.incomingPackets.aInteger233();
            var5 = Class1211.incomingPackets.aLong_011();
            var2 = Class1211.incomingPackets.aInt122();
            var4 = (7 & var2) + Class107.anInt1452;
            var3 = ((120 & var2) >> 4) + Class65.anInt990;
            if(~var3 <= -1 && ~var4 <= -1 && 104 > var3 && var4 < 104) {
               Class1013 var31 = new Class1013();
               var31.amount = var5;
               var31.id = var1;
               if(Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var3][var4] == null) {
                  Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var3][var4] = new Class975();
               }

               Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var3][var4].insertBack(new Class921(var31));
               Class128.spawnGroundItem(var4, var3);
            }

         } else {
            int var8;
            int var10;
            int var11;
            int var13;
            int var28;
            int var35;
            Class140_Sub6 var36;
            if(-122 == ~Class1008.incomingPacket) {
               var1 = Class1211.incomingPackets.readUnsignedByte();
               var2 = 2 * Class65.anInt990 + (15 & var1 >> 4);
               var3 = (15 & var1) + 2 * Class107.anInt1452;
               var4 = var2 - -Class1211.incomingPackets.getByte();
               var5 = Class1211.incomingPackets.getByte() + var3;
               var6 = Class1211.incomingPackets.aLong_1884();
               var7 = Class1211.incomingPackets.aInteger233();
               var8 = Class1211.incomingPackets.readUnsignedByte() * 4;
               var28 = Class1211.incomingPackets.readUnsignedByte() * 4;
               var10 = Class1211.incomingPackets.aInteger233();
               var11 = Class1211.incomingPackets.aInteger233();
               var35 = Class1211.incomingPackets.readUnsignedByte();
               if(-256 == ~var35) {
                  var35 = -1;
               }

               var13 = Class1211.incomingPackets.readUnsignedByte();
               if(0 <= var2 && 0 <= var3 && 208 > var2 && 208 > var3 && var4 >= 0 && 0 <= var5 && var4 < 208 && -209 < ~var5 && var7 != '\uffff') {
                  var5 *= 64;
                  var4 = 64 * var4;
                  var3 = 64 * var3;
                  var2 = 64 * var2;
                  var36 = new Class140_Sub6(var7, Class26.plane, var2, var3, Class121.method1736(Class26.plane, 1, var2, var3) + -var8, Class1134.loopCycle + var10, var11 + Class1134.loopCycle, var35, var13, var6, var28);
                  var36.method2024(var5, 1, Class1134.loopCycle + var10, -var28 + Class121.method1736(Class26.plane, 1, var4, var5), var4);
                  Class3_Sub13_Sub30.aClass61_3364.insertBack(new Class3_Sub28_Sub19(var36));
               }

            } else if(Class1008.incomingPacket == 186) {
            	//Still gfx(gfx at buffer location)
               var1 = Class1211.incomingPackets.readUnsignedByte();
               var2 = Class65.anInt990 + (var1 >> 4 & 7);
               var3 = Class107.anInt1452 - -(var1 & 7);
               var4 = Class1211.incomingPackets.aInteger233(); //id of gfx
               var5 = Class1211.incomingPackets.readUnsignedByte();
               var6 = Class1211.incomingPackets.aInteger233();
               if(-1 >= ~var2 && ~var3 <= -1 && ~var2 > -105 && -105 < ~var3) {
                  var2 = var2 * 128 - -64;
                  var3 = var3 * 128 - -64;
                  Class1212 tileGfx = new Class1212(var4, Class26.plane, var2, var3, -var5 + Class121.method1736(Class26.plane, 1, var2, var3), var6, Class1134.loopCycle);
                  Class3_Sub13_Sub15.aClass61_3177.insertBack(new Class3_Sub28_Sub2(tileGfx));
               }

            } else if(Class1008.incomingPacket == 17) {
            	//Create object
                var5 = Class1211.incomingPackets.aInt2016();
                var6 = Class65.anInt990 - -((var5 & 125) >> 4);
                var7 = (7 & var5) + Class107.anInt1452;
               var8 = Class1211.incomingPackets.aLong_011();
               var1 = Class1211.incomingPackets.aInt2016();
               var2 = var1 >> 2;
               var3 = 3 & var1;
               var4 = Class75.anIntArray1107[var2];
               if(~var6 <= -1 && var7 >= 0 && var6 < 104 && ~var7 > -105) {
                  Class1245.createObject(Class26.plane, var7, var3, var6, -1, var8, var4, var2, 0);
                  
                /*  int new1 = 10368;
                  int new2 = ((new1 & 125) >> 4) + Class65.anInt990; //X
                  int new3 = Class107.anInt1452 + (7 & new1); //Y
                  int new4 = 0;
                  int new5 = new4 >> 2;
                  int new6 = 3 & new4;
                  int new7 = Class75.anIntArray1107[new5]; //important
                  int new8 = ((10 << 2) + (0 & 0x3));//Class1211.incomingPackets.aLong_011();
                  if('\uffff' == new8) {
                      new8 = -1;
                   }
                  //System.out.println("Sending object anim: " + new1 + ", " + new2 + ", " + new3);
                  Class1209.method1131(Class26.plane, 125, new6, new5, new3, new7, new2, new8);*/
               }

            } else if(~Class1008.incomingPacket != -21) {
               int var14;
               if(202 == Class1008.incomingPacket) {
                  var1 = Class1211.incomingPackets.readUnsignedByte();
                  var2 = var1 >> 2;
                  var3 = var1 & 3;
                  var4 = Class1211.incomingPackets.readUnsignedByte();
                  var5 = (var4 >> 4 & 7) + Class65.anInt990;
                  var6 = (7 & var4) + Class107.anInt1452;
                  byte var25 = Class1211.incomingPackets.aLong1003();
                  byte var30 = Class1211.incomingPackets.aLong1003();
                  byte var9 = Class1211.incomingPackets.aReturn111();
                  var10 = Class1211.incomingPackets.aMethod10();
                  var11 = Class1211.incomingPackets.aLong_011();
                  byte var12 = Class1211.incomingPackets.getByte();
                  var13 = Class1211.incomingPackets.aInteger233();
                  var14 = Class1211.incomingPackets.method788(-1741292848);
                  if(!Class1012.aBoolean_617) {
                     Class3_Sub13_Sub23.method280(var12, var13, var14, var11, var6, var9, var3, var25, var5, var2, -745213428, var30, var10);
                  }
               }

               if(-15 == ~Class1008.incomingPacket) {
                  var1 = Class1211.incomingPackets.readUnsignedByte();
                  var3 = Class107.anInt1452 + (var1 & 7);
                  var2 = ((var1 & 119) >> 4) + Class65.anInt990;
                  var4 = Class1211.incomingPackets.aInteger233();
                  var5 = Class1211.incomingPackets.aInteger233();
                  var6 = Class1211.incomingPackets.aInteger233();
                  if(0 <= var2 && ~var3 <= -1 && var2 < 104 && ~var3 > -105) {
                     Class975 var29 = Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var2][var3];
                     if(var29 != null) {
                        for(Class921 var34 = (Class921)var29.getFirst(); var34 != null; var34 = (Class921)var29.getNext()) {
                           Class1013 var33 = var34.aClass140_Sub7_3676;
                           if(~(var4 & 32767) == ~var33.id && var5 == var33.amount) {
                              var33.amount = var6;
                              break;
                           }
                        }

                        Class128.spawnGroundItem(var3, var2);
                     }
                  }

               } else if(135 == Class1008.incomingPacket) {
                  var1 = Class1211.incomingPackets.aBoole100();
                  var2 = Class1211.incomingPackets.gea100();
                  var4 = Class107.anInt1452 + (7 & var2);
                  var3 = (7 & var2 >> 4) + Class65.anInt990;
                  var5 = Class1211.incomingPackets.aLong_011();
                  var6 = Class1211.incomingPackets.aLong_011();
                  if(0 <= var3 && ~var4 <= -1 && var3 < 104 && -105 < ~var4 && Class971.anInt2211 != var1) {
                     Class1013 var27 = new Class1013();
                     var27.amount = var5;
                     var27.id = var6;
                     if(null == Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var3][var4]) {
                        Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var3][var4] = new Class975();
                     }

                     Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var3][var4].insertBack(new Class921(var27));
                     Class128.spawnGroundItem(var4, var3);
                  }

               } else if(var0 <= -67) {
                  if(218 != Class1008.incomingPacket) {
                     if(Class1008.incomingPacket == 104) {
                        var1 = Class1211.incomingPackets.readUnsignedByte();
                        var3 = 2 * Class107.anInt1452 + (var1 & 15);
                        var2 = 2 * Class65.anInt990 - -(var1 >> 4 & 15);
                        var4 = Class1211.incomingPackets.getByte() + var2;
                        var5 = Class1211.incomingPackets.getByte() + var3;
                        var6 = Class1211.incomingPackets.aLong_1884();
                        var7 = Class1211.incomingPackets.aLong_1884();
                        var8 = Class1211.incomingPackets.aInteger233();
                        var28 = Class1211.incomingPackets.getByte();
                        var10 = 4 * Class1211.incomingPackets.readUnsignedByte();
                        var11 = Class1211.incomingPackets.aInteger233();
                        var35 = Class1211.incomingPackets.aInteger233();
                        var13 = Class1211.incomingPackets.readUnsignedByte();
                        var14 = Class1211.incomingPackets.readUnsignedByte();
                        if(255 == var13) {
                           var13 = -1;
                        }

                        if(var2 >= 0 && -1 >= ~var3 && 208 > var2 && var3 < 208 && 0 <= var4 && ~var5 <= -1 && 208 > var4 && 208 > var5 && var8 != '\uffff') {
                           var4 = 64 * var4;
                           var2 *= 64;
                           var5 *= 64;
                           var3 *= 64;
                           if(~var6 != -1) {
                              int var15;
                              int var17;
                              Object var16;
                              int var18;
                              if(0 <= var6) {
                                 var17 = var6 - 1;
                                 var18 = 2047 & var17;
                                 var15 = 15 & var17 >> 11;
                                 var16 = Class3_Sub13_Sub24.class1001List[var18];
                              } else {
                                 var17 = -1 + -var6;
                                 var15 = (31085 & var17) >> 11;
                                 var18 = 2047 & var17;
                                 if(Class971.anInt2211 != var18) {
                                    var16 = Class922.class946List[var18];
                                 } else {
                                    var16 = Class945.thisClass946;
                                 }
                              }
                           }

                           Class140_Sub6 var37 = new Class140_Sub6(var8, Class26.plane, var2, var3, -var28 + Class121.method1736(Class26.plane, 1, var2, var3), var11 + Class1134.loopCycle, var35 + Class1134.loopCycle, var13, var14, var7, var10);
                           var37.method2024(var5, 1, var11 + Class1134.loopCycle, -var10 + Class121.method1736(Class26.plane, 1, var4, var5), var4);
                           Class3_Sub13_Sub30.aClass61_3364.insertBack(new Class3_Sub28_Sub19(var37));
                        }

                     } else if(97 == Class1008.incomingPacket) {
                        var1 = Class1211.incomingPackets.readUnsignedByte();
                        var2 = Class65.anInt990 + (7 & var1 >> 4);
                        var3 = Class107.anInt1452 + (var1 & 7);
                        var4 = Class1211.incomingPackets.aInteger233();
                        if(~var4 == -65536) {
                           var4 = -1;
                        }

                        var5 = Class1211.incomingPackets.readUnsignedByte();
                        var6 = (242 & var5) >> 4;
                        var8 = Class1211.incomingPackets.readUnsignedByte();
                        var7 = 7 & var5;
                        if(-1 >= ~var2 && -1 >= ~var3 && var2 < 104 && var3 < 104) {
                           var28 = 1 + var6;
                           if(~Class945.thisClass946.anIntArray2767[0] <= ~(var2 + -var28) && ~(var28 + var2) <= ~Class945.thisClass946.anIntArray2767[0] && Class945.thisClass946.anIntArray2755[0] >= -var28 + var3 && Class945.thisClass946.anIntArray2755[0] <= var28 + var3 && 0 != Class14.areaSoundsVolume && var7 > 0 && 50 > Class113.anInt1552 && ~var4 != 0) {
                              Class1042_4.anIntArray2550[Class113.anInt1552] = var4;
                              Class166.anIntArray2068[Class113.anInt1552] = var7;
                              Class1008.anIntArray2157[Class113.anInt1552] = var8;
                              Class945.aClass135Array2131[Class113.anInt1552] = null;
                              Class3_Sub13_Sub6.anIntArray3083[Class113.anInt1552] = var6 + ((var2 << 16) - -(var3 << 8));
                              ++Class113.anInt1552;
                           }
                        }

                     } else if(Class1008.incomingPacket == 39) {
                         var4 = Class1211.incomingPackets.aMethod10();
                        var1 = Class1211.incomingPackets.aInt122();
                        var3 = Class107.anInt1452 + (var1 & 7);
                        var2 = ((113 & var1) >> 4) + Class65.anInt990;
                        if(-1 >= ~var2 && var3 >= 0 && 104 > var2 && 104 > var3) {
                           Class975 var24 = Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var2][var3];
                           if(var24 != null) {
                              for(Class921 var26 = (Class921)var24.getFirst(); var26 != null; var26 = (Class921)var24.getNext()) {
                                 if(var26.aClass140_Sub7_3676.id == (var4 & 32767)) {
                                    var26.unlink();
                                    break;
                                 }
                              }

                              if(var24.getFirst() == null) {
                                 Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var2][var3] = null;
                              }

                              Class128.spawnGroundItem(var3, var2);
                           }
                        }

                     }
                  } else {
                     var1 = Class1211.incomingPackets.readUnsignedByte();
                     var2 = Class65.anInt990 - -(var1 >> 4 & 7);
                     var3 = (var1 & 7) + Class107.anInt1452;
                     var4 = var2 + Class1211.incomingPackets.getByte();
                     var5 = Class1211.incomingPackets.getByte() + var3;
                     var6 = Class1211.incomingPackets.aLong_1884();
                     var7 = Class1211.incomingPackets.aInteger233();
                     var8 = 4 * Class1211.incomingPackets.readUnsignedByte();
                     var28 = Class1211.incomingPackets.readUnsignedByte() * 4;
                     var10 = Class1211.incomingPackets.aInteger233();
                     var11 = Class1211.incomingPackets.aInteger233();
                     var35 = Class1211.incomingPackets.readUnsignedByte();
                     var13 = Class1211.incomingPackets.readUnsignedByte();
                     if(255 == var35) {
                        var35 = -1;
                     }

                     if(~var2 <= -1 && var3 >= 0 && -105 < ~var2 && 104 > var3 && ~var4 <= -1 && var5 >= 0 && -105 < ~var4 && 104 > var5 && -65536 != ~var7) {
                        var5 = var5 * 128 + 64;
                        var3 = 128 * var3 + 64;
                        var2 = 128 * var2 + 64;
                        var4 = 128 * var4 + 64;
                        var36 = new Class140_Sub6(var7, Class26.plane, var2, var3, Class121.method1736(Class26.plane, 1, var2, var3) + -var8, var10 + Class1134.loopCycle, var11 + Class1134.loopCycle, var35, var13, var6, var28);
                        var36.method2024(var5, 1, Class1134.loopCycle + var10, Class121.method1736(Class26.plane, 1, var4, var5) - var28, var4);
                        Class3_Sub13_Sub30.aClass61_3364.insertBack(new Class3_Sub28_Sub19(var36));
                     }

                  }
               }
            } else {
            	//Send object animation
            	//~Class1008.incomingPacket != -21
               var1 = Class1211.incomingPackets.aInt122();
               var2 = ((var1 & 125) >> 4) + Class65.anInt990;
               var3 = Class107.anInt1452 + (7 & var1);
               var4 = Class1211.incomingPackets.aInt122();
               var5 = var4 >> 2;
               var6 = 3 & var4;
               var7 = Class75.anIntArray1107[var5];
               var8 = Class1211.incomingPackets.aLong_011();
               if('\uffff' == var8) {
                  var8 = -1;
               }

               Class1209.method1131(Class26.plane, 125, var6, var5, var3, var7, var2, var8);
            }
         }
      } catch (RuntimeException var23) {
         throw Class1134.method1067(var23, "g.G(" + var0 + ')');
      }
   }

   static final void loadMiscSprites(Class1027 var1) {
	   //P11 - smallFont
	   //P12 - regularFont
	   //B12 - boldFont
	   //Hit - hitMarkFont
	   //Crit - critMarkFont
         Class1025.fontP11Id = var1.getSpriteGroupId(Class1212.fontP11String);
         Class75_Sub2.fontP12Id = var1.getSpriteGroupId(Class26.fontP12String);
         Class3_Sub13_Sub11.fontB12Id = var1.getSpriteGroupId(Class3_Sub13_Sub11.fontB12String);
         Class1223.map_function_sprite_group = var1.getSpriteGroupId(Class1009.mapFunctions);

         Class3_Sub13_Sub23_Sub1.old_markers_id = var1.getSpriteGroupId(Class3_Sub13_Sub31.old_markers_string);
         Class3_Sub13_Sub23_Sub1.new_markers_id = var1.getSpriteGroupId(Class3_Sub13_Sub31.new_markers_string);
         Class3_Sub13_Sub23_Sub1.new_markers_id2dark = var1.getSpriteGroupId(Class3_Sub13_Sub31.new_markers_string2dark);
         Class3_Sub13_Sub23_Sub1.new_markers_id2hq = var1.getSpriteGroupId(Class3_Sub13_Sub31.new_markers_string2hq);
         Class3_Sub13_Sub23_Sub1.soakingId = var1.getSpriteGroupId(Class3_Sub13_Sub31.soaking_string);
         Class3_Sub13_Sub23_Sub1.healmarkId = var1.getSpriteGroupId(Class3_Sub13_Sub31.healmarkString);
         Class3_Sub13_Sub23_Sub1.hit_font_id = var1.getSpriteGroupId(Class3_Sub13_Sub31.hit_font_string);
         Class3_Sub13_Sub23_Sub1.crit_font_id = var1.getSpriteGroupId(Class3_Sub13_Sub31.crit_font_string);
   		
         Class922.infinitySymbolId = var1.getSpriteGroupId(Class922.infinitySymbolString);

       /*  client.fishCursorId = var1.getSpriteGroupId(Class1008.create("fishcursor"));
         client.treeCursorId = var1.getSpriteGroupId(Class1008.create("treecursor"));
         client.prayCursorId = var1.getSpriteGroupId(Class1008.create("praycursor"));
         client.mineCursorId = var1.getSpriteGroupId(Class1008.create("minecursor"));
         client.eatCursorId = var1.getSpriteGroupId(Class1008.create("eatcursor"));
         client.drinkCursorId = var1.getSpriteGroupId(Class1008.create("drinkcursor"));
         client.wearCursorId = var1.getSpriteGroupId(Class1008.create("wearcursor"));
         client.talkCursorId = var1.getSpriteGroupId(Class1008.create("talkcursor"));
         client.useCursorId = var1.getSpriteGroupId(Class1008.create("usecursor"));
         client.takeCursorId = var1.getSpriteGroupId(Class1008.create("takecursor"));
         client.pointerId = var1.getSpriteGroupId(Class1008.create("pointer"));
         
         
         
         
    	 client.attackCursorId = var1.getSpriteGroupId(Class1008.create("attackcursor"));
    	 client.climbUpCursorId = var1.getSpriteGroupId(Class1008.create("climbupcursor"));
    	 client.climbDownCursorId = var1.getSpriteGroupId(Class1008.create("climbdowncursor"));
    	 client.stealCursorId = var1.getSpriteGroupId(Class1008.create("stealcursor"));
    	 client.doorCursorId = var1.getSpriteGroupId(Class1008.create("doorcursor"));
    	 client.enterCursorId = var1.getSpriteGroupId(Class1008.create("entercursor"));
    	 client.magicCursorId = var1.getSpriteGroupId(Class1008.create("magiccursor"));
    	 client.searchCursorId = var1.getSpriteGroupId(Class1008.create("searchcursor"));
    	 client.chooseCursorId = var1.getSpriteGroupId(Class1008.create("choosecursor"));
    	 client.withdrawCursorId = var1.getSpriteGroupId(Class1008.create("withdrawcursor"));
    	 client.slashCursorId = var1.getSpriteGroupId(Class1008.create("slashcursor"));
    	 client.craftCursorId = var1.getSpriteGroupId(Class1008.create("craftcursor"));
    	 client.acceptCursorId = var1.getSpriteGroupId(Class1008.create("acceptcursor"));
    	 client.declineCursorId = var1.getSpriteGroupId(Class1008.create("declinecursor"));
    	 client.smeltCursorId = var1.getSpriteGroupId(Class1008.create("smeltcursor"));
    	 client.cleanCursorId = var1.getSpriteGroupId(Class1008.create("cleancursor"));
    	 */
         
         Class922.worldmap0Id = var1.getSpriteGroupId(Class943.create("worldmap0"));
         Class922.worldmap1Id = var1.getSpriteGroupId(Class943.create("worldmap1"));
         Class922.worldmap2Id = var1.getSpriteGroupId(Class943.create("1362"));

         //Orbs
         Class922.orbBgId = var1.getSpriteGroupId(Class922.orbBgString);
         Class922.hpFillId = var1.getSpriteGroupId(Class922.hpFillString);
         Class922.poisonFillId = var1.getSpriteGroupId(Class922.poisonFillString);
         Class922.hpIconId = var1.getSpriteGroupId(Class922.hpIconString);
         Class922.prayFillId = var1.getSpriteGroupId(Class922.prayFillString);
         Class922.prayIconId = var1.getSpriteGroupId(Class922.prayIconString);
         Class922.runFillId = var1.getSpriteGroupId(Class922.runFillString);
         Class922.runIconId = var1.getSpriteGroupId(Class922.runIconString);
         Class922.orbDrainId = var1.getSpriteGroupId(Class922.orbDrainString);
         Class922.runOnIconId = var1.getSpriteGroupId(Class922.runOnIconString);
         Class922.orbBgIdNew = var1.getSpriteGroupId(Class922.orbBgStringNew);
         Class922.orbBgOnIdNew = var1.getSpriteGroupId(Class922.orbBgOnStringNew);
         Class922.orbBgOSHoverId = var1.getSpriteGroupId(Class922.orbBgOSHoverString);
         Class922.hpFillIdNew = var1.getSpriteGroupId(Class922.hpFillStringNew);
         Class922.osrsChannelBackgroundID = var1.getSpriteGroupId(Class922.osrsChannelBackgroundString);
         Class922.prayIconIdNew = var1.getSpriteGroupId(Class922.prayIconStringNew);
         Class922.runFillIdNew = var1.getSpriteGroupId(Class922.runFillStringNew);
         Class922.orbDrainIdNew = var1.getSpriteGroupId(Class922.orbDrainStringNew);
         Class922.orb_drain_hd_id = var1.getSpriteGroupId(Class943.create("orbdrain"));
         Class922.chatbuttonselectedId = var1.getSpriteGroupId(Class943.create("1022"));
         Class922.redStoneId = var1.getSpriteGroupId(Class943.create("1335"));
         Class922.redStone1Id = var1.getSpriteGroupId(Class943.create("1206"));
         Class922.redStone2Id = var1.getSpriteGroupId(Class943.create("1162"));
         Class922.redStone3Id = var1.getSpriteGroupId(Class943.create("1163"));
         Class922.redStone4Id = var1.getSpriteGroupId(Class943.create("1164"));
         Class922.redStone5Id = var1.getSpriteGroupId(Class943.create("1165"));
         Class922.aboveCompassId = var1.getSpriteGroupId(Class943.create("1363"));
         Class922.attackIconId = var1.getSpriteGroupId(Class943.create("attackicon"));
         Class922.skillsIconId = var1.getSpriteGroupId(Class943.create("skillsicon"));
         Class922.questIconId = var1.getSpriteGroupId(Class943.create("questicon"));
         Class922.inventoryIconId = var1.getSpriteGroupId(Class943.create("inventoryicon"));
         Class922.equipmentIconId = var1.getSpriteGroupId(Class943.create("equipmenticon"));
         Class922.prayerIconId = var1.getSpriteGroupId(Class943.create("prayericon"));
         Class922.magicIconId = var1.getSpriteGroupId(Class943.create("magicicon"));
         Class922.clanChatIconId = var1.getSpriteGroupId(Class943.create("clanchaticon"));
         Class922.friendsIconId = var1.getSpriteGroupId(Class943.create("friendsicon"));
         Class922.ignoreIconId = var1.getSpriteGroupId(Class943.create("ignoreicon"));
         Class922.logoutIconId = var1.getSpriteGroupId(Class943.create("logouticon"));
         Class922.settingsIconId = var1.getSpriteGroupId(Class943.create("settingsicon"));
         Class922.emotesIconId = var1.getSpriteGroupId(Class943.create("emotesicon"));
         Class922.musicIconId = var1.getSpriteGroupId(Class943.create("musicicon"));

         Class922.bountyTeleSpriteId = var1.getSpriteGroupId(Class943.create("bountyteleport"));
         
     	//horizontal top, horizontal bottom, 
     	//vertical left, vertical right
     	//top left, top right
     	//bottom left, bottom right
      /*   client.borderSpriteIds[0] = var1.getSpriteGroupId(Class1008.create("newborderhorizontal2"));
         client.borderSpriteIds[1] = var1.getSpriteGroupId(Class1008.create("newborderhorizontal"));
         client.borderSpriteIds[2] = var1.getSpriteGroupId(Class1008.create("newbordervert"));
         client.borderSpriteIds[3] = var1.getSpriteGroupId(Class1008.create("newbordervert2"));
         client.borderSpriteIds[4] = var1.getSpriteGroupId(Class1008.create("nbtopleft"));
         client.borderSpriteIds[5] = var1.getSpriteGroupId(Class1008.create("nbtopright"));
         client.borderSpriteIds[6] = var1.getSpriteGroupId(Class1008.create("nbbottomleft"));
         client.borderSpriteIds[7] = var1.getSpriteGroupId(Class1008.create("nbbottomright"));
         client.borderSpriteIds[8] = var1.getSpriteGroupId(Class1008.create("leftconnector"));
         client.borderSpriteIds[9] = var1.getSpriteGroupId(Class1008.create("rightconnector"));*/
         //1232 - vertical left - newbordervert
         //1233 - horizontal bottom - newborderhorizontal
         //1234 - top left - nbtopleft
         //1235 - top right - nbtopright
         //1236 - bottom left - nbbottomleft
         //1237 - vertical right - newbordervert2
         //1238 - horizontal top - newborderhorizontal2
         
         Class922.xpButtonId = var1.getSpriteGroupId(Class922.xpButtonString);
         Class922.restIconId = var1.getSpriteGroupId(Class922.restIconString);
         
         Class922.forgottenPasswordLoginId = var1.getSpriteGroupId(Class922.forgottenPasswordLoginString);
         Class922.rememberUsernameLoginId = var1.getSpriteGroupId(Class922.rememberUsernameLoginString);
         Class922.hideUsernameLoginId = var1.getSpriteGroupId(Class922.hideUsernameLoginString);
         Class922.rememberToggleOffId = var1.getSpriteGroupId(Class922.rememberToggleOffString);
         Class922.rememberToggleOffHoverId = var1.getSpriteGroupId(Class922.rememberToggleOffHoverString);
         Class922.blockIconId = var1.getSpriteGroupId(Class922.blockIconString);
         
         Class922.mapscenesId = var1.getSpriteGroupId(Class922.mapscene);
         Class922.mapbackId = var1.getSpriteGroupId(Class922.mapback);
         Class922.mapbackIdNew = var1.getSpriteGroupId(Class922.mapbackNew);
         
         Class922.mapbackFsId = var1.getSpriteGroupId(Class922.mapbackFs);
         Class922.chatbuttonFsId = var1.getSpriteGroupId(Class922.chatbuttonFs);
         Class922.tabstonesFsId = var1.getSpriteGroupId(Class922.tabstonesFs);
         Class922.xpCounterHoverId = var1.getSpriteGroupId(Class943.create("1010"));
         Class922.compassBgId = var1.getSpriteGroupId(Class943.create("compassbg"));
         Class922.runFillOnId = var1.getSpriteGroupId(Class922.runFillOnString);
         Class922.xp_counter_id = var1.getSpriteGroupId(Class943.create("464sprites"));
         Class922.xp_counter_2_id = var1.getSpriteGroupId(Class943.create("530sprites"));
         Class922.xpCounterActiveId = var1.getSpriteGroupId(Class943.create("1076"));
         Class922.xpCounterActive2Id = var1.getSpriteGroupId(Class943.create("1115"));
         Class922.musicToggleId = var1.getSpriteGroupId(Class943.create("musictoggle"));
         Class922.hd_sprites_id = var1.getSpriteGroupId(Class943.create("hdsprites"));
         Class922.sd_sprites_id = var1.getSpriteGroupId(Class943.create("sdsprites"));
         Class922.anInt2195 = var1.getSpriteGroupId(Class164_Sub1.aClass94_3008);
         Class1002.pkIconsId = var1.getSpriteGroupId(Class72.aClass94_1070);
         Class922.headIconsId = var1.getSpriteGroupId(Class134.aClass94_1764);
         Class3_Sub13_Sub29.hintIconsId = var1.getSpriteGroupId(Class1045.aClass94_871);
         Class1210.hintMapmarkersId = var1.getSpriteGroupId(Class113.aClass94_1556);
         Class3_Sub13_Sub4.mapmarkersId = var1.getSpriteGroupId(Class3_Sub13_Sub38.aClass94_3452);
         Class75_Sub1.anInt2633 = var1.getSpriteGroupId(Class3_Sub13_Sub14.aClass94_3168); //cross?
         Class40.mapIconIds = var1.getSpriteGroupId(Class113.mapIconStrings); //map dots
         Class3_Sub15.scrollbarSpriteId = var1.getSpriteGroupId(Class1005.scrollbarjString);
         Class3_Sub15.scrollbarFsSpriteId = var1.getSpriteGroupId(Class1005.scrollbarFsjString);
         Class3_Sub28_Sub18.anInt3757 = var1.getSpriteGroupId(Class3_Sub28_Sub4.aClass94_3576); //mod icons
         Class45.anInt735 = var1.getSpriteGroupId(Class1212.floorShadows); //floor shadows
         Class93.compassId = var1.getSpriteGroupId(Class1223.compassString);
         Class1042_2.mapEdgeId = var1.getSpriteGroupId(Class7.aClass94_2164);
   }

}

package com.kit.client;

final class Class3_Sub13_Sub6 extends CanvasBuffer {

   private int anInt3073 = 0;
   private int anInt3074 = 4096;
   protected static Class975 aClass61_3075 = new Class975();
   protected  static int[] spriteHeights;
   protected  static Class1009 cacheIndex5;
   protected   static boolean focus;
   protected   static Class1008 aClass94_3080 = Class95.method1586(23161, 160);
   protected   static int anInt3081 = 0;
   protected   static int[] chatTypes = new int[100];
   protected  static int[] anIntArray3083 = new int[50];

   static final void method195() {
         int var1 = (Class1001.renderX >> 10) - -(Class131.anInt1716 >> 3);
         int var2 = (Class77.renderY >> 10) - -(SpriteDefinition.anInt1152 >> 3);
            byte var3 = 0;
            byte var4 = 8;
            byte var6 = 18;
            Class3_Sub22.aByteArrayArray2521 = new byte[var6][];
            Class955.anIntArray3587 = new int[var6];
            Class3_Sub13_Sub26.aByteArrayArray3335 = new byte[var6][];
            Class922.mapsArray = new int[var6];
            Class3_Sub9.anIntArrayArray2319 = new int[var6][4];
            Class921.aByteArrayArray3669 = new byte[var6][];
            Class3_Sub24_Sub3.anIntArray3494 = new int[var6];
            Class164_Sub2.aByteArrayArray3027 = new byte[var6][];
            Class3_Sub13_Sub24.anIntArray3290 = new int[var6];
            Class3_Sub13_Sub15.anIntArray3181 = new int[var6];
            Class101.landscapeArray = new int[var6];
            byte var5 = 8;
            Class3_Sub13_Sub4.aByteArrayArray3057 = new byte[var6][];
            int var11 = 0;

            int var7;
            for(var7 = (-6 + var1) / 8; ~var7 >= ~((6 + var1) / 8); ++var7) {
               for(int var8 = (-6 + var2) / 8; ~((var2 + 6) / 8) <= ~var8; ++var8) {
                  int var9 = (var7 << 8) - -var8;
                  Class3_Sub24_Sub3.anIntArray3494[var11] = var9;
                  Class922.mapsArray[var11] = cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class966_2.mString, Class72.createInt(var7), Class3_Sub13_Sub14.underscore, Class72.createInt(var8)}));
                  Class101.landscapeArray[var11] = cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class161.lString, Class72.createInt(var7), Class3_Sub13_Sub14.underscore, Class72.createInt(var8)}));
                  Class3_Sub13_Sub24.anIntArray3290[var11] = cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class969.aClass94_26, Class72.createInt(var7), Class3_Sub13_Sub14.underscore, Class72.createInt(var8)}));
                  Class3_Sub13_Sub15.anIntArray3181[var11] = cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class95.aClass94_1333, Class72.createInt(var7), Class3_Sub13_Sub14.underscore, Class72.createInt(var8)}));
                  Class955.anIntArray3587[var11] = cacheIndex5.getSpriteGroupId(Class922.combinejStrings(new Class1008[]{Class167.aClass94_2084, Class72.createInt(var7), Class3_Sub13_Sub14.underscore, Class72.createInt(var8)}));
                  if(~Class3_Sub13_Sub24.anIntArray3290[var11] == 0) {
                     Class922.mapsArray[var11] = -1;
                     Class101.landscapeArray[var11] = -1;
                     Class3_Sub13_Sub15.anIntArray3181[var11] = -1;
                     Class955.anIntArray3587[var11] = -1;
                  }

                  ++var11;
               }
            }

            for(var7 = var11; ~var7 > ~Class3_Sub13_Sub24.anIntArray3290.length; ++var7) {
               Class3_Sub13_Sub24.anIntArray3290[var7] = -1;
               Class922.mapsArray[var7] = -1;
               Class101.landscapeArray[var7] = -1;
               Class3_Sub13_Sub15.anIntArray3181[var7] = -1;
               Class955.anIntArray3587[var7] = -1;
            }

            Class943.loadRegion(var3, var2, var1, var5, true, var4, true);
   }

   final int[] getMonochromeOutput(int var1, byte var2) {
      try {
         int var3 = 69 / ((var2 - 30) / 36);
         int[] var4 = this.aClass114_2382.method1709(-16409, var1);
         if(this.aClass114_2382.aBoolean1580) {
            int[] var5 = this.method152(0, var1, 32755);

            for(int var6 = 0; var6 < Class113.anInt1559; ++var6) {
               int var7 = var5[var6];
               var4[var6] = ~this.anInt3073 >= ~var7 && ~var7 >= ~this.anInt3074?4096:0;
            }
         }

         return var4;
      } catch (RuntimeException var8) {
         throw Class1134.method1067(var8, "ca.D(" + var1 + ',' + var2 + ')');
      }
   }

   static final void method196(boolean var0) {
      try {
         Class988.aClass93_684.clearSoftReference();
         Class163_Sub1.cursorCache.clearSoftReference();
         if(var0) {
            anIntArray3083 = (int[])null;
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "ca.B(" + var0 + ')');
      }
   }

   final void decode(int var1, ByteBuffer var2, boolean var3) {
      try {
         if(!var3) {
            method196(true);
         }

         if(~var1 != -1) {
            if(1 == var1) {
               this.anInt3074 = var2.aInteger233();
            }
         } else {
            this.anInt3073 = var2.aInteger233();
         }

      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "ca.A(" + var1 + ',' + (var2 != null?"{...}":"null") + ',' + var3 + ')');
      }
   }

   public Class3_Sub13_Sub6() {
      super(1, true);
   }

   public static void method197(int var0) {
      try {
         spriteHeights = null;
         aClass61_3075 = null;
         chatTypes = null;
         aClass94_3080 = null;
         if(var0 == 1) {
            anIntArray3083 = null;
            cacheIndex5 = null;
         }
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "ca.O(" + var0 + ')');
      }
   }

	static final void method198(boolean var0) {
		int var3 = Class164_Sub2.aByteArrayArray3027.length;
		byte[][] var2;
		if (Class1012.aBoolean_617 && var0) {
			var2 = Class3_Sub13_Sub4.aByteArrayArray3057;
		} else {
			var2 = Class3_Sub22.aByteArrayArray2521;
		}

		for (int var4 = 0; var4 < var3; ++var4) {
			byte[] var5 = var2[var4];
			if (var5 != null) {
				int var6 = -Class131.anInt1716 + 64 * (Class3_Sub24_Sub3.anIntArray3494[var4] >> 8);
				int var7 = (Class3_Sub24_Sub3.anIntArray3494[var4] & 255) * 64 + -SpriteDefinition.anInt1152;
				Class58.method1194();
				Class3_Sub15.method374(var6, var0, var5, var7, Class930.class972);
			}
		}
	}

   static final void method199(int var0, int var1, int var2, int var3) {
      try {
         if(var3 != -799) {
            focus = true;
         }

         if(-1 != ~Class1048.soundEffectsVolume && var0 != 0 && ~Class113.anInt1552 > -51 && 0 != ~var1) {
            Class1042_4.anIntArray2550[Class113.anInt1552] = var1;
            Class166.anIntArray2068[Class113.anInt1552] = var0;
            Class1008.anIntArray2157[Class113.anInt1552] = var2;
            Class945.aClass135Array2131[Class113.anInt1552] = null;
            anIntArray3083[Class113.anInt1552] = 0;
            ++Class113.anInt1552;
         }

      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "ca.C(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ')');
      }
   }

}

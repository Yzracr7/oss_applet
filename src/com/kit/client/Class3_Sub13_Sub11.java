package com.kit.client;

public final class Class3_Sub13_Sub11 extends CanvasBuffer {

    private int anInt3129;
    protected static Class93 aClass93_3130 = new Class93(4);
    public static int fontB12Id/*, fontCritId, fontHitId*/;
    protected static Class1008 aClass94_3133 = Class943.create(")2");
    private int anInt3134;
    private int anInt3135;
    protected static Class47 aClass47_3137 = new Class47(64);
    protected static Class1008 fontB12String = Class943.create("b12_full");
    protected static int[] anIntArray3139 = new int[14];
    protected static Class1008 aClass94_3140 = Class943.create("overlay2");

    private Class3_Sub13_Sub11(int var1) {
        super(0, false);
        try {
            this.method218((byte) 75, var1);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "fm.<init>(" + var1 + ')');
        }
    }

    public static void method217(int var0) {
        try {
            fontB12String = null;
            aClass94_3133 = null;
            aClass93_3130 = null;
            anIntArray3139 = null;
            if (var0 != 1) {
                fontB12String = (Class1008) null;
            }

            aClass47_3137 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "fm.B(" + var0 + ')');
        }
    }

    public Class3_Sub13_Sub11() {
        this(0);
    }

    private final void method218(byte var1, int var2) {
        try {
            this.anInt3134 = 4080 & var2 >> 4;
            this.anInt3135 = var2 << 4 & 4080;
            if (var1 == 75) {
                this.anInt3129 = (var2 & 16711680) >> 12;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "fm.Q(" + var1 + ',' + var2 + ')');
        }
    }

    final int[][] getColorOutput(int var1, int var2) {
        try {
            if (var1 != -1) {
                method222(-87, 26, 75, -56, 22, -68);
            }
            int[][] var3 = this.aClass97_2376.method1594((byte) -123, var2);
            if (this.aClass97_2376.aBoolean1379) {
                int[] var4 = var3[0];
                int[] var5 = var3[1];
                int[] var6 = var3[2];

                for (int var7 = 0; ~var7 > ~Class113.anInt1559; ++var7) {
                    var4[var7] = this.anInt3129;
                    var5[var7] = this.anInt3134;
                    var6[var7] = this.anInt3135;
                }
            }
            return var3;
        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "fm.T(" + var1 + ',' + var2 + ')');
        }
    }

    static final void constructLoginScreen(boolean var0) {
        if (var0) {
            if (-1 != Class1143.mainScreenInterface) {
                Class60.discardInterface(Class1143.mainScreenInterface);
            }
            for (Class1207 var2 = (Class1207) Class3_Sub13_Sub17.aClass130_3208.getFirst(); null != var2; var2 = (Class1207) Class3_Sub13_Sub17.aClass130_3208.getNext()) {
                Class21.removeOverrideInterface(var2, true);
            }
            Class1143.mainScreenInterface = -1;
            Class3_Sub13_Sub17.aClass130_3208 = new Class1017_2(8);
            Class419.method122();
            Class1143.mainScreenInterface = Class3_Sub22.loginScreenInterfaceid;
            Class124.method1746(false);
            Class47.method1093();
            Class3_Sub13_Sub12.executeOnLaunchScript(Class1143.mainScreenInterface);
        }
        Class955.anInt3590 = -1;
        Class922.setaInteger_544(10);
    }

    static final void method220(int var1, int var2) {
        try {
            Class1004.anInt741 = Class115.aClass930ArrayArray1581[var2][var1].lightY;
            Class120_Sub30_Sub1.anInt3274 = Class115.aClass930ArrayArray1581[var2][var1].lightZ;
            Class930.anInt1191 = Class115.aClass930ArrayArray1581[var2][var1].lightX;
            Class953.setLightPosition((float) Class1004.anInt741, (float) Class120_Sub30_Sub1.anInt3274, (float) Class930.anInt1191);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static final void method221(int var0, Class1008 var1, Class1008 var2, Class1008 var3, int var4/*, Class1008 chatTitles*/) {
        try {
            Class1143.appendChatMessage(var4, var1, var3, var2/*, chatTitles*/);
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "fm.F(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ',' + (var2 != null ? "{...}" : "null") + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ')');
        }
    }

    final void decode(int var1, ByteBuffer var2, boolean var3) {
        try {
            if (!var3) {
                method221(-64, (Class1008) null, (Class1008) null, (Class1008) null, 34);
            }
            if (-1 == ~var1) {
                this.method218((byte) 75, var2.aBoolean183());
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "fm.A(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    static final boolean method222(int var0, int var1, int var2, int var3, int var4, int var5) {
        int var6;
        int var7;
        if (var1 == var2 && var3 == var4) {
            if (!Class8.method846(var0, var1, var3)) {
                return false;
            } else {
                var6 = var1 << 7;
                var7 = var3 << 7;
                return Class3_Sub13_Sub37.method349(var6 + 1, Class1134.activeTileHeightMap[var0][var1][var3] + var5, var7 + 1) && Class3_Sub13_Sub37.method349(var6 + 128 - 1, Class1134.activeTileHeightMap[var0][var1 + 1][var3] + var5, var7 + 1) && Class3_Sub13_Sub37.method349(var6 + 128 - 1, Class1134.activeTileHeightMap[var0][var1 + 1][var3 + 1] + var5, var7 + 128 - 1) && Class3_Sub13_Sub37.method349(var6 + 1, Class1134.activeTileHeightMap[var0][var1][var3 + 1] + var5, var7 + 128 - 1);
            }
        } else {
            for (var6 = var1; var6 <= var2; ++var6) {
                for (var7 = var3; var7 <= var4; ++var7) {
                    if (Class81.anIntArrayArrayArray1142[var0][var6][var7] == -Class3_Sub28_Sub1.anInt3539) {
                        return false;
                    }
                }
            }
            var6 = (var1 << 7) + 1;
            var7 = (var3 << 7) + 2;
            int var8 = Class1134.activeTileHeightMap[var0][var1][var3] + var5;
            if (!Class3_Sub13_Sub37.method349(var6, var8, var7)) {
                return false;
            } else {
                int var9 = (var2 << 7) - 1;
                if (!Class3_Sub13_Sub37.method349(var9, var8, var7)) {
                    return false;
                } else {
                    int var10 = (var4 << 7) - 1;
                    if (!Class3_Sub13_Sub37.method349(var6, var8, var10)) {
                        return false;
                    } else if (!Class3_Sub13_Sub37.method349(var9, var8, var10)) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
    }
}

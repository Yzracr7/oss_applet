package com.kit.client;

class Class163_Sub2 extends Class163 {

    protected static Class1008 aClass94_2996 = null;
    protected static Class1046[] indexFiles = new Class1046[Class922.aInteger_512];

    static final Class25 method2217(int var0, int var1, int var2) {
        Class949 var3 = Class75_Sub2.class949s[var0][var1][var2];
        if (var3 == null) {
            return null;
        } else {
            for (int var4 = 0; var4 < var3.anInt2223; ++var4) {
                Class25 var5 = var3.aClass25Array2221[var4];
                if ((var5.aLong498 >> 29 & 3L) == 2L && var5.anInt483 == var1 && var5.anInt478 == var2) {
                    Class923.method2186(var5);
                    return var5;
                }
            }
            return null;
        }
    }

    public static void method2218(byte var0) {
        try {
            aClass94_2996 = null;
            if (var0 != -83) {
                method2218((byte) -9);
            }

            indexFiles = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "dk.B(" + var0 + ')');
        }
    }
}

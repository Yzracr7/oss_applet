package com.kit.client;

final class SpriteDefinition {

    private int[] anIntArray1144;
    private CanvasBuffer aClass3_Sub13_1145;
    protected static Class93 aClass93_1146 = new Class93(64);
    private CanvasBuffer[] aClass3_Sub13Array1147;
    private CanvasBuffer aClass3_Sub13_1148;
    private int[] anIntArray1149;
    protected static int anInt1152;

    final int[] method1404(int var1, boolean var2, int var3, double var4, Class1027 var7, Class1226 var8, boolean var9) {
        Class1031.method1859(var4);
        Class17.anInterface2_408 = var8;
        Class104.aClass153_2172 = var7;
        Class3_Sub13_Sub3.method180(-1, var1, var3);
        int var11;
        for (var11 = 0; ~this.aClass3_Sub13Array1147.length < ~var11; ++var11) {
            this.aClass3_Sub13Array1147[var11].method160(var1, var3, 250);
        }
        int[] var10 = new int[var1 * var3];
        int var12;
        byte var13;
        if (!var9) {
            var13 = 1;
            var11 = 0;
            var12 = var3;
        } else {
            var13 = -1;
            var12 = -1;
            var11 = var3 - 1;
        }
        int var14 = 0;
        int var15;
        for (var15 = 0; ~var15 > ~var1; ++var15) {
            if (var2) {
                var14 = var15;
            }
            int[] var17;
            int[] var16;
            int[] var18;
            if (this.aClass3_Sub13_1145.aBoolean2375) {
                int[] var19 = this.aClass3_Sub13_1145.getMonochromeOutput(var15, (byte) 109);
                var16 = var19;
                var17 = var19;
                var18 = var19;
            } else {
                int[][] var24 = this.aClass3_Sub13_1145.getColorOutput(-1, var15);
                var16 = var24[0];
                var18 = var24[2];
                var17 = var24[1];
            }
            for (int var25 = var11; var25 != var12; var25 += var13) {
                int var20 = var16[var25] >> 4;
                if (var20 > 255) {
                    var20 = 255;
                }
                if (~var20 > -1) {
                    var20 = 0;
                }
                var20 = Class966_2.anIntArray3804[var20];
                int var22 = var18[var25] >> 4;
                int var21 = var17[var25] >> 4;
                if (var21 > 255) {
                    var21 = 255;
                }
                if (0 > var21) {
                    var21 = 0;
                }
                if (-256 > ~var22) {
                    var22 = 255;
                }
                var21 = Class966_2.anIntArray3804[var21];
                if (-1 < ~var22) {
                    var22 = 0;
                }
                var22 = Class966_2.anIntArray3804[var22];
                var10[var14++] = (var20 << 16) - -(var21 << 8) + var22;
                if (var2) {
                    var14 += var3 + -1;
                }
            }
        }
        for (var15 = 0; var15 < this.aClass3_Sub13Array1147.length; ++var15) {
            this.aClass3_Sub13Array1147[var15].method161();
        }
        return var10;
    }

    static final void handleEntitySprites(int startY, int endX, int startX, int var3, int endY, int var5) {
        Class3_Sub13_Sub39.anInt3464 = 0;
        int var7;
        int var15;
        int var19;
        int var21;
        int var22;
        int var29;
        int var32;
        for (var7 = -1; var7 < Class159.anInt2022 + Class163.anInt2046; ++var7) {
            Object var8;
            if (0 == ~var7) {
                var8 = Class945.thisClass946;
            } else if (~Class159.anInt2022 < ~var7) {
                var8 = Class922.class946List[Class922.playerIndices[var7]];
            } else {
                var8 = Class3_Sub13_Sub24.class1001List[Class15.anIntArray347[-Class159.anInt2022 + var7]];
            }
            if (null != var8 && ((Class991) var8).method1966((byte) 17)) {
                Class981 var9;
                if (var8 instanceof Class1001) {
                    var9 = ((Class1001) var8).aClass90_3976;
                    if (null != var9.anIntArray1292) {
                        var9 = var9.method1471((byte) -93);
                    }

                    if (var9 == null) {
                        continue;
                    }
                }
                int i;
                if (var7 < Class159.anInt2022) {
                    var19 = 30;
                    Class946 var10 = (Class946) var8;
                    if (var10.pkIconId != -1 || -1 != var10.headIconId) {
                        Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, ((Class991) var8).method1975() - -15, endX >> 1);
                        if (-1 < Class32.anInt590) {
                            if (0 != ~var10.pkIconId) {
                                Class3_Sub13_Sub31.pkIconSprites[var10.pkIconId].method643(-12 + Class32.anInt590 + startX, -var19 + startY + Class971.anInt2208);
                                var19 += 25;
                            }
                            if (var10.headIconId != -1) {
                                Class1001.headIconSprites[var10.headIconId].method643(-12 + startX + Class32.anInt590, startY - (-Class971.anInt2208 + var19));
                                var19 += 25;
                            }
                        }
                    }
                    if (~var7 <= -1) {
                        Class1025[] hintsList = Class1244.hintsList;
                        for (i = 0; ~hintsList.length < ~i; ++i) {
                            Class1025 hint = hintsList[i];
                            if (null != hint
                                    && -11 == ~hint.type
                                    && Class922.playerIndices[var7] == hint.index) {
                                Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, ((Class991) var8).method1975() - -15, endX >> 1);
                                if (Class32.anInt590 > -1 && Class166.hintIconSprites != null) {
                                    Class166.hintIconSprites[hint.iconIndex].method643(startX - (-Class32.anInt590 + 12), startY + (Class971.anInt2208 - var19));
                                }
                            }
                        }
                    }
                } else {
                    var9 = ((Class1001) var8).aClass90_3976;
                    if (var9.anIntArray1292 != null) {
                        var9 = var9.method1471((byte) 102);
                    }
                    if (~var9.headIconId <= -1 && Class1001.headIconSprites.length > var9.headIconId) {
                        if (0 == ~var9.anInt1265) {
                            var22 = 15 + ((Class991) var8).method1975();
                        } else {
                            var22 = 15 + var9.anInt1265;
                        }
                        Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, var22, endX >> 1);
                        if (~Class32.anInt590 < 0 && Class1001.headIconSprites != null) {
                            Class1001.headIconSprites[var9.headIconId].method643(startX - -Class32.anInt590 - 12, -30 + startY - -Class971.anInt2208);
                        }
                    }
                    Class1025[] var20 = Class1244.hintsList;
                    for (var21 = 0; ~var21 > ~var20.length; ++var21) {
                        Class1025 var24 = var20[var21];
                        if (null != var24 && var24.type == 1 && ~var24.index == ~Class15.anIntArray347[-Class159.anInt2022 + var7] && -11 < ~(Class1134.loopCycle % 20)) {
                            if (-1 != var9.anInt1265) {
                                var29 = 15 + var9.anInt1265;
                            } else {
                                var29 = 15 + ((Class991) var8).method1975();
                            }
                            Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, var29, endX >> 1);
                            if (Class166.hintIconSprites != null && 0 > ~Class32.anInt590 && ((-11 < ~(Class1134.loopCycle % 20) && Class922.hintFlash) || (!Class922.hintFlash))) {
                                Class166.hintIconSprites[var24.iconIndex].method643(-12 + startX + Class32.anInt590, -28 + Class971.anInt2208 + startY);
                            }
                        }
                    }
                }
                if (((Class991) var8).aClass94_2825 != null && (var7 >= Class159.anInt2022 || ~Class3_Sub13_Sub8.publicChatStatus == -1 || 3 == Class3_Sub13_Sub8.publicChatStatus || 1 == Class3_Sub13_Sub8.publicChatStatus && Class1045.checkIfSelf(((Class946) var8).username, (byte) -82))) {
                    Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, ((Class991) var8).method1975(), endX >> 1);
                    if (-1 < Class32.anInt590 && Class3_Sub13_Sub39.anInt3464 < Class3_Sub13_Sub26.anInt3332) {
                        Class3_Sub13_Sub26.anIntArray3329[Class3_Sub13_Sub39.anInt3464] = Class922.getBoldClass1019().method682(((Class991) var8).aClass94_2825) / 2;
                        Class3_Sub13_Sub26.anIntArray3327[Class3_Sub13_Sub39.anInt3464] = Class922.getBoldClass1019().anInt3727;
                        Class3_Sub13_Sub26.anIntArray3319[Class3_Sub13_Sub39.anInt3464] = Class32.anInt590;
                        Class3_Sub13_Sub26.anIntArray3337[Class3_Sub13_Sub39.anInt3464] = Class971.anInt2208;
                        Class3_Sub13_Sub26.anIntArray3331[Class3_Sub13_Sub39.anInt3464] = ((Class991) var8).anInt2837;
                        Class3_Sub13_Sub26.anIntArray3336[Class3_Sub13_Sub39.anInt3464] = ((Class991) var8).anInt2753;
                        Class3_Sub13_Sub26.anIntArray3318[Class3_Sub13_Sub39.anInt3464] = ((Class991) var8).anInt2814;
                        Class3_Sub13_Sub26.aClass94Array3317[Class3_Sub13_Sub39.anInt3464] = ((Class991) var8).aClass94_2825;
                        ++Class3_Sub13_Sub39.anInt3464;
                    }
                }
                if (~((Class991) var8).anInt2781 < ~Class1134.loopCycle) {
                    Class957 var23 = Class1006.aClass3_Sub28_Sub16Array996[Class929.newHealthbars ? 1 : 3];
                    Class957 var25 = Class1006.aClass3_Sub28_Sub16Array996[Class929.newHealthbars ? 0 : 2];
                    if (var8 instanceof Class1001) {
                        Class1001 var28 = (Class1001) var8;
                        Class957[] var31 = (Class957[]) ((Class957[]) Class3_Sub13_Sub11.aClass93_3130.get((long) var28.aClass90_3976.anInt1279));
                        if (var31 == null) {
                            var31 = Class140_Sub6.method2027(0, var28.aClass90_3976.anInt1279, Class140_Sub6.cacheIndex8);
                            if (null != var31) {
                                Class3_Sub13_Sub11.aClass93_3130.put(var31, (long) var28.aClass90_3976.anInt1279);
                            }
                        }
                        if (null != var31 && ~var31.length == -3) {
                            var25 = var31[1];
                            var23 = var31[0];
                        }
                        Class981 var14 = var28.aClass90_3976;
                        if (-1 == var14.anInt1265) {
                            var21 = ((Class991) var8).method1975();
                        } else {
                            var21 = var14.anInt1265;
                        }
                    } else {
                        var21 = ((Class991) var8).method1975();
                    }
                    Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, var23.height + 10 + var21, endX >> 1);
                    if (-1 < Class32.anInt590) {
                        i = -(var23.width >> 1) + Class32.anInt590 + startX;
                        var29 = Class971.anInt2208 + startY + -3;
                        var23.method643(i, var29);
                        var32 = var23.width * ((Class991) var8).hpRatio / 255;
                        var15 = var23.height;
                        if (!Class1012.aBoolean_617) {
                            Class1023.method1326(i, var29, i + var32, var15 + var29);
                        } else {
                            Class920.method_974(i, var29, i + var32, var29 + var15);
                        }
                        var25.method643(i, var29);
                        if (Class1012.aBoolean_617) {
                            Class920.method_576(startX, startY, endX + startX, startY - -endY);
                        } else {
                            Class1023.clipRect(startX, startY - (Class929.aInteger_511 == 634 ? 5 : 0), endX + startX, endY + startY);
                        }
                    }
                }
                for (var19 = 0; ~var19 > -5; ++var19) {
                    if (~((Class991) var8).hitsLoopCycle[var19] < ~Class1134.loopCycle) {
                        if (!(var8 instanceof Class1001)) {
                            var22 = ((Class991) var8).method1975() / 2;
                        } else {
                            Class1001 var30 = (Class1001) var8;
                            Class981 var26 = var30.aClass90_3976;
                            if (~var26.anInt1265 == 0) {
                                var22 = ((Class991) var8).method1975() / 2;
                            } else {
                                var22 = var26.anInt1265 / 2;
                            }
                        }
                        Class107.method1647((byte) 122, endY >> 1, var3, (Class991) var8, var5, var22, endX >> 1);
                        if (-1 < Class32.anInt590) {
                            if (Class929.aInteger_511 != 634) {
                                if (var19 == 1) {
                                    Class971.anInt2208 -= 20;
                                }
                                if (-3 == ~var19) {
                                    Class971.anInt2208 -= 10;
                                    Class32.anInt590 -= 15;
                                }

                                if (3 == var19) {
                                    Class971.anInt2208 -= 10;
                                    Class32.anInt590 += 15;
                                }
                            }
                            int index = (((Class991) var8).hitmarkTypes[var19]);
                            int damage = (((Class991) var8).hitArray[var19]);
                            if (Class929.aInteger_511 == 554) { // 554
                                if (index == 1) {
                                    if (damage > 99) {
                                        index = 2;
                                    }
                                } else if (index == 2) {
                                    if (damage > 99) {
                                        index = 4;
                                    } else {
                                        index = 3;
                                    }
                                }
                                int xIncrease = (index == 2 || index == 4) ? 20 : 12;
                                int yIncrease = (index == 2 || index == 4) ? 13 : 12;
                                Class75_Sub3.new_hitmarkers[index].method643(-xIncrease + startX + Class32.anInt590, startY + Class971.anInt2208 - yIncrease);
                                Class922.getSmallClass1019().drawText(Class72.createInt(damage), -1 + Class32.anInt590 + startX, 3 + Class971.anInt2208 + startY, 16777215, 0);
                            } else if (Class929.aInteger_511 == 634) {
                                int spriteDrawX = Class32.anInt590;
                                int spriteDrawY = Class971.anInt2208;
                                int j1 = var19;
                                if (spriteDrawX > -1) {
                                    switch (j1) {
                                        case 1:
                                            spriteDrawY += 20;
                                            break;
                                        case 2:
                                            spriteDrawY += 40;
                                            break;
                                        case 3:
                                            spriteDrawY += 60;
                                            break;
                                        case 4:
                                            spriteDrawY += 80;
                                            break;
                                        case 5:
                                            spriteDrawY += 100;
                                            break;
                                        case 6:
                                            spriteDrawY += 120;
                                            break;
                                    }
                                    Class991 e = ((Class991) (var8));
                                    if (((Class991) var8).hitsLoopCycle[var19] < Class1134.loopCycle + 1000)
                                        e.hitmarkMove[j1]--;
                                    if (e.hitmarkMove[j1] < -26)
                                        e.hitmarkTrans[j1] -= 5;
                                    newHitmarkDraw(spriteDrawX, spriteDrawY, String.valueOf(e.hitArray[j1]).length(), e.hitmarkTypes[j1], e.hitIcon[j1], e.hitArray[j1], e.hitmarkMove[j1], e.hitmarkTrans[j1], e.hitmarkFocus[j1]);
                                }
                            } else {
                                Class75_Sub3.old_hitmarkers[index].method643(-12 + startX + Class32.anInt590, startY + Class971.anInt2208 - 12);
                                Class922.getSmallClass1019().drawText(Class72.createInt(damage), -1 + Class32.anInt590 + startX, 3 + Class971.anInt2208 + startY, 16777215, 0);
                            }
                        }
                    }
                }
            }
        }
        var7 = 0;
        for (; Class3_Sub13_Sub39.anInt3464 > var7; ++var7) {
            var19 = Class3_Sub13_Sub26.anIntArray3337[var7];
            int var18 = Class3_Sub13_Sub26.anIntArray3319[var7];
            var21 = Class3_Sub13_Sub26.anIntArray3327[var7];
            var22 = Class3_Sub13_Sub26.anIntArray3329[var7];
            boolean var27 = true;
            while (var27) {
                var27 = false;

                for (var29 = 0; var7 > var29; ++var29) {
                    if (Class3_Sub13_Sub26.anIntArray3337[var29] - Class3_Sub13_Sub26.anIntArray3327[var29] < 2 + var19 && -var21 + var19 < Class3_Sub13_Sub26.anIntArray3337[var29] - -2 && -var22 + var18 < Class3_Sub13_Sub26.anIntArray3319[var29] + Class3_Sub13_Sub26.anIntArray3329[var29] && Class3_Sub13_Sub26.anIntArray3319[var29] - Class3_Sub13_Sub26.anIntArray3329[var29] < var22 + var18 && -Class3_Sub13_Sub26.anIntArray3327[var29] + Class3_Sub13_Sub26.anIntArray3337[var29] < var19) {
                        var19 = Class3_Sub13_Sub26.anIntArray3337[var29] - Class3_Sub13_Sub26.anIntArray3327[var29];
                        var27 = true;
                    }
                }
            }
            Class32.anInt590 = Class3_Sub13_Sub26.anIntArray3319[var7];
            Class971.anInt2208 = Class3_Sub13_Sub26.anIntArray3337[var7] = var19;
            Class1008 var33 = Class3_Sub13_Sub26.aClass94Array3317[var7];
            if (~Class988.anInt688 == -1) {
                var32 = 16776960;
                if (-7 < ~Class3_Sub13_Sub26.anIntArray3331[var7]) {
                    var32 = Class971.anIntArray2213[Class3_Sub13_Sub26.anIntArray3331[var7]];
                }
                if (6 == Class3_Sub13_Sub26.anIntArray3331[var7]) {
                    var32 = 10 <= Class1220.anInt1127 % 20 ? 16776960 : 16711680;
                }
                if (~Class3_Sub13_Sub26.anIntArray3331[var7] == -8) {
                    var32 = ~(Class1220.anInt1127 % 20) > -11 ? 255 : '\uffff';
                }
                if (8 == Class3_Sub13_Sub26.anIntArray3331[var7]) {
                    var32 = ~(Class1220.anInt1127 % 20) <= -11 ? 8454016 : '\ub000';
                }
                if (9 == Class3_Sub13_Sub26.anIntArray3331[var7]) {
                    var15 = -Class3_Sub13_Sub26.anIntArray3318[var7] + 150;
                    if (var15 >= 50) {
                        if (var15 >= 100) {
                            if (150 > var15) {
                                var32 = -500 - (-(5 * var15) - '\uff00');
                            }
                        } else {
                            var32 = 16776960 + 16384000 + -(327680 * var15);
                        }
                    } else {
                        var32 = var15 * 1280 + 16711680;
                    }
                }
                if (10 == Class3_Sub13_Sub26.anIntArray3331[var7]) {
                    var15 = -Class3_Sub13_Sub26.anIntArray3318[var7] + 150;
                    if (50 <= var15) {
                        if (~var15 > -101) {
                            var32 = -(327680 * (-50 + var15)) + 16711935;
                        } else if (150 > var15) {
                            var32 = 327680 * var15 - (32768000 - (255 + -(5 * var15) + 500));
                        }
                    } else {
                        var32 = 16711680 + var15 * 5;
                    }
                }
                if (Class3_Sub13_Sub26.anIntArray3331[var7] == 11) {
                    var15 = 150 + -Class3_Sub13_Sub26.anIntArray3318[var7];
                    if (var15 >= 50) {
                        if (-101 >= ~var15) {
                            if (var15 < 150) {
                                var32 = 16777215 - var15 * 327680 + 32768000;
                            }
                        } else {
                            var32 = '\uff00' - (-(327685 * var15) - -16384250);
                        }
                    } else {
                        var32 = 16777215 - 327685 * var15;
                    }
                }
                if (0 == Class3_Sub13_Sub26.anIntArray3336[var7]) {
                    Class922.getBoldClass1019().drawText(var33, Class32.anInt590 + startX, startY + Class971.anInt2208, var32, 0);
                }
                if (1 == Class3_Sub13_Sub26.anIntArray3336[var7]) {
                    Class922.getBoldClass1019().method696(var33, startX - -Class32.anInt590, Class971.anInt2208 + startY, var32, 0, Class1220.anInt1127);
                }
                if (Class3_Sub13_Sub26.anIntArray3336[var7] == 2) {
                    Class922.getBoldClass1019().method695(var33, startX - -Class32.anInt590, startY - -Class971.anInt2208, var32, 0, Class1220.anInt1127);
                }
                if (-4 == ~Class3_Sub13_Sub26.anIntArray3336[var7]) {
                    Class922.getBoldClass1019().method692(var33, startX + Class32.anInt590, Class971.anInt2208 + startY, var32, 0, Class1220.anInt1127, 150 - Class3_Sub13_Sub26.anIntArray3318[var7]);
                }
                if (4 == Class3_Sub13_Sub26.anIntArray3336[var7]) {
                    var15 = (-Class3_Sub13_Sub26.anIntArray3318[var7] + 150) * (Class922.getBoldClass1019().method682(var33) - -100) / 150;
                    if (!Class1012.aBoolean_617) {
                        Class1023.method1326(-50 + (startX - -Class32.anInt590), startY, 50 + Class32.anInt590 + startX, endY + startY);
                    } else {
                        Class920.method_974(Class32.anInt590 + startX + -50, startY, Class32.anInt590 + startX - -50, endY + startY);
                    }
                    Class922.getBoldClass1019().method681(var33, startX - (-Class32.anInt590 + -50) + -var15, startY + Class971.anInt2208, var32, 0);
                    if (Class1012.aBoolean_617) {
                        Class920.method_576(startX, startY, endX + startX, endY + startY);
                    } else {
                        Class1023.clipRect(startX, startY, startX - -endX, startY + endY);
                    }
                }
                if (Class3_Sub13_Sub26.anIntArray3336[var7] == 5) {
                    int var16 = 0;
                    var15 = -Class3_Sub13_Sub26.anIntArray3318[var7] + 150;
                    if (Class1012.aBoolean_617) {
                        Class920.method_974(startX, -1 + -Class922.getBoldClass1019().anInt3727 + Class971.anInt2208 + startY, endX + startX, 5 + startY - -Class971.anInt2208);
                    } else {
                        Class1023.method1326(startX, -1 + -Class922.getBoldClass1019().anInt3727 + Class971.anInt2208 + startY, startX + endX, 5 + Class971.anInt2208 + startY);
                    }
                    if (25 > var15) {
                        var16 = var15 + -25;
                    } else if (var15 > 125) {
                        var16 = var15 - 125;
                    }
                    Class922.getBoldClass1019().drawText(var33, Class32.anInt590 + startX, var16 + startY + Class971.anInt2208, var32, 0);
                    if (Class1012.aBoolean_617) {
                        Class920.method_576(startX, startY, startX - -endX, startY + endY);
                    } else {
                        Class1023.clipRect(startX, startY, startX + endX, startY + endY);
                    }
                }
            } else {
                Class922.getBoldClass1019().drawText(var33, startX - -Class32.anInt590, startY + Class971.anInt2208, 16776960, 0);
            }
        }
    }

    static final int method1406(byte var0) {
        try {
            int var1 = -21 % ((63 - var0) / 49);
            return Class1225.anInt4045;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "lc.E(" + var0 + ')');
        }
    }

    final byte[] method1407(int var1, int var2, boolean var3, Class1226 var4, double var5, int var7, Class1027 var8) {
        try {
            byte[] var9 = new byte[4 * var2 * var1];
            if (var7 != 8839) {
                return (byte[]) null;
            } else {
                Class1031.method1859(var5);
                Class104.aClass153_2172 = var8;
                Class17.anInterface2_408 = var4;
                Class3_Sub13_Sub3.method180(-32, var1, var2);

                int var10;
                for (var10 = 0; this.aClass3_Sub13Array1147.length > var10; ++var10) {
                    this.aClass3_Sub13Array1147[var10].method160(var1, var2, var7 + -8589);
                }

                var10 = 0;

                int var11;
                for (var11 = 0; ~var1 < ~var11; ++var11) {
                    if (var3) {
                        var10 = var11 << 2;
                    }

                    int[] var12;
                    int[] var13;
                    int[] var14;
                    int[] var15;
                    if (this.aClass3_Sub13_1145.aBoolean2375) {
                        var15 = this.aClass3_Sub13_1145.getMonochromeOutput(var11, (byte) -98);
                        var12 = var15;
                        var13 = var15;
                        var14 = var15;
                    } else {
                        int[][] var22 = this.aClass3_Sub13_1145.getColorOutput(-1, var11);
                        var12 = var22[0];
                        var13 = var22[1];
                        var14 = var22[2];
                    }

                    if (this.aClass3_Sub13_1148.aBoolean2375) {
                        var15 = this.aClass3_Sub13_1148.getMonochromeOutput(var11, (byte) -103);
                    } else {
                        var15 = this.aClass3_Sub13_1148.getColorOutput(-1, var11)[0];
                    }

                    for (int var16 = var2 - 1; ~var16 <= -1; --var16) {
                        int var17 = var12[var16] >> 4;
                        if (var17 > 255) {
                            var17 = 255;
                        }

                        if (var17 < 0) {
                            var17 = 0;
                        }

                        int var18 = var13[var16] >> 4;
                        if (-256 > ~var18) {
                            var18 = 255;
                        }

                        int var19 = var14[var16] >> 4;
                        if (var19 > 255) {
                            var19 = 255;
                        }

                        var17 = Class966_2.anIntArray3804[var17];
                        if (~var19 > -1) {
                            var19 = 0;
                        }

                        if (~var18 > -1) {
                            var18 = 0;
                        }

                        var18 = Class966_2.anIntArray3804[var18];
                        var19 = Class966_2.anIntArray3804[var19];
                        int var20;
                        if (~var17 == -1 && -1 == ~var18 && -1 == ~var19) {
                            var20 = 0;
                        } else {
                            var20 = var15[var16] >> 4;
                            if (255 < var20) {
                                var20 = 255;
                            }

                            if (-1 < ~var20) {
                                var20 = 0;
                            }
                        }

                        var9[var10++] = (byte) var17;
                        var9[var10++] = (byte) var18;
                        var9[var10++] = (byte) var19;
                        var9[var10++] = (byte) var20;
                        if (var3) {
                            var10 += -4 + (var2 << 2);
                        }
                    }
                }

                for (var11 = 0; ~var11 > ~this.aClass3_Sub13Array1147.length; ++var11) {
                    this.aClass3_Sub13Array1147[var11].method161();
                }

                return var9;
            }
        } catch (RuntimeException var21) {
            throw Class1134.method1067(var21, "lc.F(" + var1 + ',' + var2 + ',' + var3 + ',' + (var4 != null ? "{...}" : "null") + ',' + var5 + ',' + var7 + ',' + (var8 != null ? "{...}" : "null") + ')');
        }
    }

    public static void newHitmarkDraw(int spriteDrawX, int spriteDrawY, int hitLength, int type, int icon, int damage, int move, int opacity, int focus) {
        // spriteDrawY = spriteDrawY - 10; //RS has these sprites higher
        spriteDrawX = spriteDrawX + 5;

        //TODO tomorrow:


        //Make client save buffer preferences file in user.home with settings
        //Obfuscate


        Class927 hitIcon = null;

        if (damage > 0) {
            Class957 end1 = null, middle = null, end2 = null;

            switch (icon) {
                case 0:
                    hitIcon = Class922.forgottenPasswordLogin;
                    break;
                case 1:
                    hitIcon = Class922.rememberUsernameLogin;
                    break;
                case 2:
                    hitIcon = Class922.hideUsernamelogin;
                    break;
                case 3:
                    hitIcon = Class922.rememberToggleOff;
                    break;
                case 4:
                    //hitIcon = client.deflectIcon;
                    break;
            }

            int x = 0;
            switch (hitLength) {
                case 1:
                    x = 8;
                    break;
                case 2:
                    x = 4;
                    break;
                case 3:
                    x = 1;
                    break;
            }

            Class957 num1 = null, num2 = null, num3 = null;

            int target = damage;
            int[] ara = new int[hitLength];

            for (int i = 0; i < ara.length; i++) {
                ara[i] = target % 10;
                target = target / 10;
            }

            boolean crit = type == 4;
            if (hitLength == 3) {
                num1 = (crit ? Class75_Sub3.crit_font : Class75_Sub3.hit_font)[ara[2]];
                num2 = (crit ? Class75_Sub3.crit_font : Class75_Sub3.hit_font)[ara[1]];
                num3 = (crit ? Class75_Sub3.crit_font : Class75_Sub3.hit_font)[ara[0]];
            } else if (hitLength == 2) {
                num1 = (crit ? Class75_Sub3.crit_font : Class75_Sub3.hit_font)[ara[1]];
                num2 = (crit ? Class75_Sub3.crit_font : Class75_Sub3.hit_font)[ara[0]];
            } else if (hitLength == 1) {
                num1 = (crit ? Class75_Sub3.crit_font : Class75_Sub3.hit_font)[ara[0]];
            }

            if (focus == 0) {
                switch (type) {
                    case 4: // critical
                        end1 = Class75_Sub3.new_hitmarkers2hq[6];
                        middle = Class75_Sub3.new_hitmarkers2hq[7];
                        end2 = Class75_Sub3.new_hitmarkers2hq[8];
                        break;
                    case 1: // reg
                        end1 = Class75_Sub3.new_hitmarkers2hq[0];
                        //	end1 = Class75_Sub3.new_hitmarkers2[1];
                        middle = Class75_Sub3.new_hitmarkers2hq[1];
                        end2 = Class75_Sub3.new_hitmarkers2hq[2];
                        break;
                    case 2: // poison
                        end1 = Class75_Sub3.new_hitmarkers2hq[3];
                        middle = Class75_Sub3.new_hitmarkers2hq[4];
                        end2 = Class75_Sub3.new_hitmarkers2hq[5];
                        break;
                    case 3: // heal(purple)
                        end1 = Class75_Sub3.healmark[0];
                        middle = Class75_Sub3.healmark[1];
                        end2 = Class75_Sub3.healmark[2];
                        break;
                    case 5: //recoil(uses dark red)
                        end1 = Class75_Sub3.new_hitmarkers2dark[0];
                        middle = Class75_Sub3.new_hitmarkers2dark[1];
                        end2 = Class75_Sub3.new_hitmarkers2dark[2];
                        break;
                }
            } else {
                switch (type) {
                    case 4: // critical
                        end1 = Class75_Sub3.new_hitmarkers2dark[6];
                        middle = Class75_Sub3.new_hitmarkers2dark[7];
                        end2 = Class75_Sub3.new_hitmarkers2dark[8];
                        break;
                    case 1: // reg
                        end1 = Class75_Sub3.new_hitmarkers2dark[0];
                        middle = Class75_Sub3.new_hitmarkers2dark[1];
                        end2 = Class75_Sub3.new_hitmarkers2dark[2];
                        break;
                    case 2: // poison
                        end1 = Class75_Sub3.new_hitmarkers2dark[3];
                        middle = Class75_Sub3.new_hitmarkers2dark[4];
                        end2 = Class75_Sub3.new_hitmarkers2dark[5];
                        break;
                    case 3: // heal(purple)
                        end1 = Class75_Sub3.healmark[3];
                        middle = Class75_Sub3.healmark[4];
                        end2 = Class75_Sub3.healmark[5];
                        break;
                    case 5: //recoil(uses dark red)
                        end1 = Class75_Sub3.new_hitmarkers2dark[0];
                        middle = Class75_Sub3.new_hitmarkers2dark[1];
                        end2 = Class75_Sub3.new_hitmarkers2dark[2];
                }
            }
            if (type <= 1 || icon != -1)
                if (hitIcon != null)
                    hitIcon.drawTransparentSprite(spriteDrawX - 34 + x, spriteDrawY - 14 + move + (crit ? 0 : 1), opacity);
            end1.drawTransparentSprite(spriteDrawX - 12 + x, spriteDrawY - 12 + move, opacity);
            x += 4;
            for (int i = 0; i < hitLength * 2; i++) {
                middle.drawTransparentSprite(spriteDrawX - 12 + x, spriteDrawY - 12 + move, opacity);
                x += 4;
            }
            end2.drawTransparentSprite(spriteDrawX - 12 + x, spriteDrawY - 12 + move, opacity);
            int yyMove = 0;
            if (!crit)
                yyMove -= 20;
            else
                yyMove += 4;

            int xx = crit ? (hitLength == 3 ? 7 : hitLength == 2 ? 9 : 10) : (hitLength == 3 ? 6 : hitLength == 1 ? 12 : 9);
            if (num1 != null) {
                num1.drawTransparentSprite(spriteDrawX - 12 + xx, spriteDrawY - 12 + move + yyMove, opacity);
            }
            xx += 7;
            if (num2 != null) {
                num2.drawTransparentSprite(spriteDrawX - 12 + xx, spriteDrawY - 12 + move + yyMove, opacity);
            }
            xx += 7;
            if (num3 != null) {
                num3.drawTransparentSprite(spriteDrawX - 12 + xx, spriteDrawY - 12 + move + yyMove, opacity);
            }
        } else {
            hitIcon = Class922.blockIcon;
            hitIcon.drawTransparentSprite(spriteDrawX - 12, spriteDrawY - 14 + move, opacity);
        }
    }

    final boolean method1408(boolean var1, Class1226 var2, Class1027 var3) {
        try {
            int var4;
            if (0 < Class1015.anInt1668) {
                for (var4 = 0; ~var4 > ~this.anIntArray1144.length; ++var4) {
                    if (!var3.method2129(this.anIntArray1144[var4], Class1015.anInt1668)) {
                        return false;
                    }
                }
            } else {
                for (var4 = 0; ~var4 > ~this.anIntArray1144.length; ++var4) {
                    if (!var3.method2144(this.anIntArray1144[var4])) {
                        return false;
                    }
                }
            }
            if (!var1) {
                this.anIntArray1144 = (int[]) null;
            }

            for (var4 = 0; ~this.anIntArray1149.length < ~var4; ++var4) {
                if (!var2.method11(21, this.anIntArray1149[var4])) {
                    return false;
                }
            }
            return true;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "lc.B(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + (var3 != null ? "{...}" : "null") + ')');
        }
    }

    public static void method1409(boolean var0) {
        aClass93_1146 = null;
    }

    public SpriteDefinition() {
        try {
            this.anIntArray1149 = new int[0];
            this.anIntArray1144 = new int[0];
            this.aClass3_Sub13_1145 = new Class120_Sub30_Sub1();
            this.aClass3_Sub13_1145.anInt2381 = 1;
            this.aClass3_Sub13_1148 = new Class120_Sub30_Sub1();
            this.aClass3_Sub13Array1147 = new CanvasBuffer[]{this.aClass3_Sub13_1145, this.aClass3_Sub13_1148};
            this.aClass3_Sub13_1148.anInt2381 = 1;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "lc.<init>()");
        }
    }

    SpriteDefinition(ByteBuffer var1) {
        //TODO LOADING TEXTURES HERE
        try {
            int var2 = var1.readUnsignedByte();
            this.aClass3_Sub13Array1147 = new CanvasBuffer[var2];
            int[][] var5 = new int[var2][];
            int var4 = 0;
            int var3 = 0;

            int var6;
            CanvasBuffer var7;
            int var8;
            int var9;
            for (var6 = 0; var2 > var6; ++var6) {
                var7 = Class1016.create((byte) -67, var1);

                if (0 <= var7.method159(4)) {
                    ++var3;
                }

                if (~var7.method155((byte) 19) <= -1) {
                    ++var4;
                }

                var8 = var7.aClass3_Sub13Array2377.length;
                var5[var6] = new int[var8];

                for (var9 = 0; ~var8 < ~var9; ++var9) {
                    var5[var6][var9] = var1.readUnsignedByte();
                }

                this.aClass3_Sub13Array1147[var6] = var7;
            }

            this.anIntArray1144 = new int[var3];
            this.anIntArray1149 = new int[var4];
            var3 = 0;
            var4 = 0;

            for (var6 = 0; var6 < var2; ++var6) {
                var7 = this.aClass3_Sub13Array1147[var6];
                var8 = var7.aClass3_Sub13Array2377.length;

                for (var9 = 0; ~var9 > ~var8; ++var9) {
                    var7.aClass3_Sub13Array2377[var9] = this.aClass3_Sub13Array1147[var5[var6][var9]];
                }

                var9 = var7.method159(4);
                int var10 = var7.method155((byte) 19);
                if (-1 > ~var9) {
                    this.anIntArray1144[var3++] = var9;
                }

                if (~var10 < -1) {
                    this.anIntArray1149[var4++] = var10;
                }

                var5[var6] = null;
            }

            this.aClass3_Sub13_1145 = this.aClass3_Sub13Array1147[var1.readUnsignedByte()];
            var5 = (int[][]) null;
            this.aClass3_Sub13_1148 = this.aClass3_Sub13Array1147[var1.readUnsignedByte()];
        } catch (RuntimeException var11) {
            throw Class1134.method1067(var11, "lc.<init>(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }
}

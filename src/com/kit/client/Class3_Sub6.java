package com.kit.client;

final class Class3_Sub6 extends Class1042 {

    protected static Class1008 duelTextString = Class943.create("wishes to duel with you)3");
    protected static Class1008 clanWarTextString = Class943.create("challenges you to buffer war)3");
    protected static byte[][] aByteArrayArray2287;
    protected static int[] anIntArray2288 = new int[32];
    protected byte[] aByteArray2289;
    protected static int anInt2291;
    protected static Class1008 tradeTextString = Class943.create("wishes to trade with you)3");

    public static void method118(int var0) {
        try {
            anIntArray2288 = null;
            aByteArrayArray2287 = (byte[][]) null;
            tradeTextString = null;
            if (var0 != 2) {
                method119((float[]) null, 91);
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ea.A(" + var0 + ')');
        }
    }

    static final float[] method119(float[] var0, int var1) {
        try {
            if (var0 != null) {
                float[] var2 = new float[var0.length];
                Class967.arrayCopy(var0, var1, var2, 0, var0.length);
                return var2;
            } else {
                return null;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ea.B(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ')');
        }
    }

    Class3_Sub6(byte[] var1) {
        try {
            this.aByteArray2289 = var1;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ea.<init>(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    static {
        int var0 = 2;

        for (int var1 = 0; var1 < 32; ++var1) {
            anIntArray2288[var1] = -1 + var0;
            var0 += var0;
        }

        anInt2291 = 1;
    }
}

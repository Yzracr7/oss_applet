package com.kit.client;

import java.awt.*;

final class Class119 {

   static Class131 aClass131_1624;
   static Class1008 aClass94_1625 = Class943.create("Memory before cleanup=");
   static Class33 aClass33_1626;
   static Class26[] aClass26Array1627;
   static Class1027 aClass153_1628;
   static Class1008 aClass94_1630 = Class943.create("Mem:");

   public static void method1728(int var0) {
      try {
         aClass33_1626 = null;
         aClass131_1624 = null;
         aClass26Array1627 = null;
         aClass94_1630 = null;
         aClass153_1628 = null;
         aClass94_1625 = null;
         if(var0 != -14256) {
            method1728(46);
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "qh.B(" + var0 + ')');
      }
   }

   static final void method1729() {
	   Object var1;
	   if(null == Class3_Sub13_Sub10.fullscreenFrame) {
		   if(Class3_Sub13_Sub7.resizableFrame != null) {
			   var1 = Class3_Sub13_Sub7.resizableFrame;
		   } else {
			   var1 = Class38.gameClass942.thisApplet;
		   }
	   } else {
		   var1 = Class3_Sub13_Sub10.fullscreenFrame;
	   }

         Class3_Sub9.anInt2334 = ((Container)var1).getSize().width;
         Class70.anInt1047 = ((Container)var1).getSize().height;
         Insets var2;
         if(var1 == Class3_Sub13_Sub7.resizableFrame) {
            var2 = Class3_Sub13_Sub7.resizableFrame.getInsets();
            Class70.anInt1047 -= var2.bottom + var2.top;
            Class3_Sub9.anInt2334 -= var2.right + var2.left;
         }

         if(~Class83.getDisplayMode() <= -3) {
            Class23.canvasWid = Class3_Sub9.anInt2334;
            Class84.canvasDrawX = 0;
            Class989.canvasDrawY = 0;
            Class1013.canvasHei = Class70.anInt1047;
         } else {
            Class989.canvasDrawY = 0;
            Class84.canvasDrawX = (-765 + Class3_Sub9.anInt2334) / 2;
            Class1013.canvasHei = 503;
            Class23.canvasWid = 765;
         }

         if(Class1012.aBoolean_617) {
            Class1012.changeCanvasHeight(Class23.canvasWid, Class1013.canvasHei);
         }

         Class1143.canvas.setSize(Class23.canvasWid, Class1013.canvasHei);
         if(var1 != Class3_Sub13_Sub7.resizableFrame) {
            Class1143.canvas.setLocation(Class84.canvasDrawX, Class989.canvasDrawY);
         } else {
            var2 = Class3_Sub13_Sub7.resizableFrame.getInsets();
            Class1143.canvas.setLocation(var2.left + Class84.canvasDrawX, Class989.canvasDrawY + var2.top);
         }

         if(~Class1143.mainScreenInterface != 0) {
            Class124.method1746(true);
         }

         Class80.method1396();
   }

   static final void writePreferences(Class942 var0) {
	   Class995 var2 = null;

	   try {
		   Class1124 var3 = var0.getPreferences("runescape");

		   while(0 == var3.status) {
			   Class3_Sub13_Sub34.sleep(1L);
		   }

		   if(var3.status == 1) {
			   var2 = (Class995)var3.value;
			   ByteBuffer var4 = Class23.getPreferencesBuffer();
			   var2.write(var4.buffer, 0, var4.offset);
		   }
	   } catch (Exception var6) {
		   ;
	   }

	   try {
		   if(var2 != null) {
			   var2.close();
		   }
	   } catch (Exception var5) {
		   ;
	   }
   }

}

package com.kit.client;

final class Class128 {

   static int anInt1682 = 1;
   static Class93 aClass93_1683 = new Class93(64);
   private Class1002[] aClass3_Sub28Array1684;
   static boolean aBoolean1685 = true;
   private static Class1008 aClass94_1686 = Class943.create("shake:");
   static Class1008 aClass94_1687 = Class943.create("(Z");
   static Class1008 aClass94_1688 = aClass94_1686;
   static Class1008 aClass94_1689 = aClass94_1686;


   static final void spawnGroundItem(int y, int x) {
	   if(Class26.plane > Class120_Sub30_Sub1.aClass61ArrayArrayArray3273.length)
		   return;
         Class975 var3 = Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][x][y];
         try {
         if(var3 != null) {
               int var4 = -99999999;
               Class921 var5 = null;

               Class921 var6;
               for(var6 = (Class921)var3.getFirst(); null != var6; var6 = (Class921)var3.getNext()) {
                  ItemDefinition var7 = ItemDefinition.getDefinition(var6.aClass140_Sub7_3676.id);
                  int var8 = var7.cost;
                  if(var7.stackable == 1) {
                     var8 *= 1 + var6.aClass140_Sub7_3676.amount;
                  }

                  if(var4 < var8) {
                     var4 = var8;
                     var5 = var6;
                  }
               }

               if(null != var5) {
                  var3.insertFront(var5);
                  Class1013 var12 = null;
                  Class1013 var14 = null;

                  for(var6 = (Class921)var3.getFirst(); var6 != null; var6 = (Class921)var3.getNext()) {
                     Class1013 var9 = var6.aClass140_Sub7_3676;
                     if(~var9.id != ~var5.aClass140_Sub7_3676.id) {
                        if(null == var12) {
                           var12 = var9;
                        }

                        if(~var9.id != ~var12.id && null == var14) {
                           var14 = var9;
                        }
                     }
                  }

                  long var13 = (long)(1610612736 + (y << 7) + x);
                  Class3_Sub13_Sub10.method213(Class26.plane, x, y, Class121.method1736(Class26.plane, 1, 64 + 128 * x, 64 + y * 128), var5.aClass140_Sub7_3676, var13, var12, var14);
               } else {
                  ObjectDefinition.method1688(Class26.plane, x, y);
               }
         } else {
            ObjectDefinition.method1688(Class26.plane, x, y);
         }
         } catch (Exception e) {
        	 System.out.println(e);
         }
   }

   public static void method1761(byte var0) {
      try {
         aClass94_1688 = null;
         aClass94_1686 = null;
         aClass94_1689 = null;
         aClass94_1687 = null;
         if(var0 < -46) {
            aClass93_1683 = null;
         }
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "rm.C(" + var0 + ')');
      }
   }

   static final void sleep(long var0) {
	   try {
		   Thread.sleep(var0);
	   } catch (InterruptedException var4) {
		   ;
	   }
   }

   static final Class960 method1763(boolean var0, int var1, int var2, int var3, int var4, Class960 var5, int var6) {
      try {
         long var7 = (long)var3;
         Class960 var9 = (Class960) Class975.aClass93_939.get(var7);
         if(var9 == null) {
            Model var10 = Model.get(Class922.getModelJs5(var3), var3, 0);
            if(var10 == null) {
               return null;
            }

            var9 = var10.convert(64, 768, -50, -10, -50);
            Class975.aClass93_939.put(var9, var7);
         }

         int var17 = var5.getMaxX();
         int var11 = var5.method1883();
         int var12 = var5.method1898();
         int var13 = var5.method1872();
         var9 = var9.method1882(var0, true, true);
         if(var1 != 0) {
            var9.method1876(var1);
         }

         int var15;
         if(Class1012.aBoolean_617) {
            Class948 var14 = (Class948)var9;
            if(var6 != Class121.method1736(Class26.plane, 1, var4 + var17, var2 + var12) || var6 != Class121.method1736(Class26.plane, 1, var4 - -var11, var13 + var2)) {
               for(var15 = 0; ~var15 > ~var14.vertexCount; ++var15) {
                  var14.yVertices[var15] += Class121.method1736(Class26.plane, 1, var14.xVertices[var15] + var4, var14.zVertices[var15] + var2) - var6;
               }

               var14.aClass121_3839.aBoolean1640 = false;
               var14.modelBounds.calculated = false;
            }
         } else {
            Class1049 var18 = (Class1049)var9;
            if(var6 != Class121.method1736(Class26.plane, 1, var17 + var4, var12 + var2) || var6 != Class121.method1736(Class26.plane, 1, var4 - -var11, var13 + var2)) {
               for(var15 = 0; var18.anInt3891 > var15; ++var15) {
                  var18.anIntArray3883[var15] += Class121.method1736(Class26.plane, 1, var4 + var18.anIntArray3885[var15], var18.anIntArray3895[var15] + var2) - var6;
               }

               var18.aBoolean3897 = false;
            }
         }

         return var9;
      } catch (RuntimeException var16) {
         throw Class1134.method1067(var16, "rm.D(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + (var5 != null?"{...}":"null") + ',' + var6 + ')');
      }
   }

   static final void method1764(int var0, int var1, int var2) {
      for(int var3 = 0; var3 < Class1227.anInt2456; ++var3) {
         for(int var4 = 0; var4 < Class1007.anInt1234; ++var4) {
            for(int var5 = 0; var5 < Class3_Sub13_Sub15.anInt3179; ++var5) {
               Class949 var6 = Class75_Sub2.class949s[var3][var4][var5];
               if(var6 != null) {
                  Class70 var7 = var6.aClass70_2234;
                  if(var7 != null && var7.aClass140_1049.method1865()) {
                     Class3_Sub13_Sub10.method214(var7.aClass140_1049, var3, var4, var5, 1, 1);
                     if(var7.aClass140_1052 != null && var7.aClass140_1052.method1865()) {
                        Class3_Sub13_Sub10.method214(var7.aClass140_1052, var3, var4, var5, 1, 1);
                        var7.aClass140_1049.method1866(var7.aClass140_1052, 0, 0, 0, false);
                        var7.aClass140_1052 = var7.aClass140_1052.method1861(var0, var1, var2);
                     }

                     var7.aClass140_1049 = var7.aClass140_1049.method1861(var0, var1, var2);
                  }

                  for(int var8 = 0; var8 < var6.anInt2223; ++var8) {
                     Class25 var9 = var6.aClass25Array2221[var8];
                     if(var9 != null && var9.aClass140_479.method1865()) {
                        Class3_Sub13_Sub10.method214(var9.aClass140_479, var3, var4, var5, var9.anInt495 - var9.anInt483 + 1, var9.anInt481 - var9.anInt478 + 1);
                        var9.aClass140_479 = var9.aClass140_479.method1861(var0, var1, var2);
                     }
                  }

                  Class12 var10 = var6.aClass12_2230;
                  if(var10 != null && var10.aClass140_320.method1865()) {
                     Class155.method2162(var10.aClass140_320, var3, var4, var5);
                     var10.aClass140_320 = var10.aClass140_320.method1861(var0, var1, var2);
                  }
               }
            }
         }
      }

   }

   Class128(int var1) {
      try {
         this.aClass3_Sub28Array1684 = new Class1002[var1];

         for(int var2 = 0; ~var2 > ~var1; ++var2) {
            Class1002 var3 = this.aClass3_Sub28Array1684[var2] = new Class1002();
            var3.previousSub = var3;
            var3.nextSub = var3;
         }

      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "rm.<init>(" + var1 + ')');
      }
   }

}

package com.kit.client;

final class Class990 extends Class1042 {

    protected  int anInt2248;
    protected   static int anInt2249;
    protected   int plane;
   protected int anInt2253;
    protected  int anInt2254;
    protected  int anInt2256;
    protected  int anInt2257;
    protected static Class1027 aClass153_2258;
    protected  int anInt2259 = -1;
    protected  int anInt2261 = 0;
    protected  int anInt2262;
    protected  int anInt2263;
    protected  int anInt2264;
    protected  int id;


   static final int customSpritesAmount(Class1027 var0, boolean count) {
         int var2 = 0;
         if(var0.method2144(Class1223.map_function_sprite_group) || count) {
            ++var2;
         }

         if(var0.method2144(Class3_Sub13_Sub23_Sub1.old_markers_id) || count) {
            ++var2;
         }
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.new_markers_id) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.new_markers_id2hq) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.new_markers_id2dark) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.soakingId) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.healmarkId) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.crit_font_id) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class3_Sub13_Sub23_Sub1.hit_font_id) || count) {
        	 ++var2;
         }

         if(var0.method2144(Class922.anInt2195) || count) {
            ++var2;
         }

         if(var0.method2144(Class1002.pkIconsId) || count) {
            ++var2;
         }

         if(var0.method2144(Class922.headIconsId) || count) {
            ++var2;
         }

         if(var0.method2144(Class75_Sub1.anInt2633) || count) {
            ++var2;
         }

         if(var0.method2144(Class40.mapIconIds) || count) {
            ++var2;
         }
         
         if(var0.method2144(Class40.mapIconIds) || count) {
             ++var2;
          }
         
        // if(var0.method2144(Class1210.hintMapmarkersId) || method_713)
        //	 ++var2;
         
         if(var0.method2144(Class75_Sub1.anInt2633) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class3_Sub13_Sub4.mapmarkersId) || count) {
        	 ++var2;
         }

         if(var0.method2144(Class3_Sub15.scrollbarSpriteId) || count) {
            ++var2;
         }
         
         if(var0.method2144(Class3_Sub15.scrollbarFsSpriteId) || count) {
             ++var2;
          }

         if(var0.method2144(Class45.anInt735) || count) {
            ++var2;
         }

         if(var0.method2144(Class93.compassId) || count) {
            ++var2;
         }

        // if(var0.method2144(IntegerNode.anInt2471) || method_713) {
         //   ++var2;
         //}
         
         if(var0.method2144(Class922.mapbackId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.mapbackFsId) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class922.chatbuttonFsId) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class922.tabstonesFsId) || count) {
        	 ++var2;
         }
         
         
         
         
         if (var0.method2144(Class922.musicToggleId) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class922.xpCounterHoverId) || count) {
        	 ++var2;
         }
         if(var0.method2144(Class922.xp_counter_2_id) || count) {
        	 ++var2;
         }

         if (var0.method2144(Class922.sd_sprites_id) || count) {
        	 ++var2;
         }
         if (var0.method2144(Class922.redStone1Id) || count) {
        	 ++var2;
         }
        
         if(var0.method2144(Class922.orb_drain_hd_id) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.chatbuttonselectedId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.redStoneId) || count) {
        	 ++var2;
         }
        
         if(var0.method2144(Class922.bountyTeleSpriteId) || count) {
        	 ++var2;
         }
         
        /* for(int i = 0; i < client.borderSpriteIds.length; i++) {
        	 if(var0.method2144(client.borderSpriteIds[i]) || method_713) {
            	 ++var2;
             }
         }*/
         
         if(var0.method2144(Class922.infinitySymbolId) || count) {
        	 ++var2;
         }
         
       /*  if(var0.method2144(client.fishCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.treeCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.prayCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.mineCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.eatCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.drinkCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.wearCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.talkCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.useCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.takeCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.pointerId) || method_713) {
        	 ++var2;
         }
         
         if(var0.method2144(client.attackCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.climbUpCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.climbDownCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.stealCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.doorCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.enterCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.magicCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.searchCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.chooseCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.withdrawCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.slashCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.craftCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.acceptCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.declineCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.smeltCursorId) || method_713) {
        	 ++var2;
         }
         if(var0.method2144(client.cleanCursorId) || method_713) {
        	 ++var2;
         }
         */
         if(var0.method2144(Class922.orbBgId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.hpFillId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.poisonFillId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.hpIconId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.prayFillId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.prayIconId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.runFillOnId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.runIconId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.orbDrainId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.runOnIconId) || count) {
        	 ++var2;
         }
         
//new orbs

         if(var0.method2144(Class922.orbBgOSHoverId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.hpFillIdNew) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.osrsChannelBackgroundID) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.prayIconIdNew) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.orbDrainIdNew) || count) {
        	 ++var2;
         }
         
      
         if(var0.method2144(Class922.xpButtonId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.restIconId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.forgottenPasswordLoginId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.rememberUsernameLoginId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.hideUsernameLoginId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.rememberToggleOffId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.rememberToggleOffHoverId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.blockIconId) || count) {
        	 ++var2;
         }
         
         if(var0.method2144(Class922.mapscenesId) || count) {
        	 ++var2;
         }
         for (int i = 1242; i < 1332; i++) {
        	 if(var0.method2144(i) || count) {
            	 ++var2;
             }
         }
         return var2;
   }

   public static void method109() {
	   aClass153_2258 = null;
   }

   static final void method110(int var0, int var1, int var2, int var3, boolean var4) {
      Class1007.anInt1234 = var1;
      Class3_Sub13_Sub15.anInt3179 = var2;
      Class3_Sub13_Sub39.anInt3466 = var3;
      Class932.aClass3_Sub2ArrayArrayArray4070 = new Class949[var0][Class1007.anInt1234][Class3_Sub13_Sub15.anInt3179];
      Class58.anIntArrayArrayArray914 = new int[var0][Class1007.anInt1234 + 1][Class3_Sub13_Sub15.anInt3179 + 1];
      if(Class1012.aBoolean_617) {
         Class922.aClass3_Sub11ArrayArray2199 = new Class3_Sub11[4][];
      }

      if(var4) {
         Class166.aClass3_Sub2ArrayArrayArray2065 = new Class949[1][Class1007.anInt1234][Class3_Sub13_Sub15.anInt3179];
         Class3_Sub13_Sub9.anIntArrayArray3115 = new int[Class1007.anInt1234][Class3_Sub13_Sub15.anInt3179];
         Class956.anIntArrayArrayArray3605 = new int[1][Class1007.anInt1234 + 1][Class3_Sub13_Sub15.anInt3179 + 1];
         if(Class1012.aBoolean_617) {
            Class3_Sub13_Sub28.aClass3_Sub11ArrayArray3346 = new Class3_Sub11[1][];
         }
      } else {
         Class166.aClass3_Sub2ArrayArrayArray2065 = (Class949[][][])null;
         Class3_Sub13_Sub9.anIntArrayArray3115 = (int[][])null;
         Class956.anIntArrayArrayArray3605 = (int[][][])null;
         Class3_Sub13_Sub28.aClass3_Sub11ArrayArray3346 = (Class3_Sub11[][])null;
      }

      Class167.method2264(false);
      Class3_Sub28_Sub8.aClass113Array3610 = new Class113[500];
      anInt2249 = 0;
      Class145.aClass113Array1895 = new Class113[500];
      Class1015.anInt1672 = 0;
      Class81.anIntArrayArrayArray1142 = new int[var0][Class1007.anInt1234 + 1][Class3_Sub13_Sub15.anInt3179 + 1];
      Class954.aClass25Array1868 = new Class25[5000];
      Class3_Sub13_Sub5.anInt3070 = 0;
      Class1028.aClass25Array4060 = new Class25[100];
      Class23.aBooleanArrayArray457 = new boolean[Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466 + 1][Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466 + 1];
      Class49.aBooleanArrayArray814 = new boolean[Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466 + 2][Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466 + 2];
      Class136.aByteArrayArrayArray1774 = new byte[var0][Class1007.anInt1234][Class3_Sub13_Sub15.anInt3179];
   }

}

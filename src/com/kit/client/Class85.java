package com.kit.client;

final class Class85 {

   static boolean aBoolean1167 = false;
   static Class954[] aClass142Array1168 = new Class954[14];
   static float aFloat1169;
   static Class1027 aClass153_1171;
   static int anInt1174 = 99;

   static final int method1423(boolean var0, ByteBuffer var1, Class1008 var2) {
      try {
         if(var0) {
            method1426(17);
         }

         int var3 = var1.offset;
         byte[] var4 = var2.method1568(0);
         var1.aLong_1017(var4.length);
         var1.offset += Class3_Sub13_Sub9.aClass36_3112.method1015(var4.length, -81, var1.buffer, var4, 0, var1.offset);
         return var1.offset + -var3;
      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "lg.A(" + var0 + ',' + (var1 != null?"{...}":"null") + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

   static final Class927[] method1424(Class1027 var0, byte var1, int var2, int var3) {
      try {
         if(var1 != -12) {
            anInt1174 = 37;
         }

         return Class922.spriteExists(var0, var2, var3)?Class3_Sub13_Sub36.method343():null;
      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "lg.C(" + (var0 != null?"{...}":"null") + ',' + var1 + ',' + var2 + ',' + var3 + ')');
      }
   }

   static final void method1425(int var0) {
      Class3_Sub13_Sub35.anInt3419 = var0;

      for(int var1 = 0; var1 < Class1007.anInt1234; ++var1) {
         for(int var2 = 0; var2 < Class3_Sub13_Sub15.anInt3179; ++var2) {
            if(Class75_Sub2.class949s[var0][var1][var2] == null) {
               Class75_Sub2.class949s[var0][var1][var2] = new Class949(var0, var1, var2);
            }
         }
      }

   }

   public static void method1426(int var0) {
      try {
         aClass153_1171 = null;
         if(var0 != -25247) {
            aClass142Array1168 = (Class954[])null;
         }

         aClass142Array1168 = null;
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "lg.B(" + var0 + ')');
      }
   }

}

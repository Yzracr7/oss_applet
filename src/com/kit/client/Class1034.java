package com.kit.client;

public final class Class1034 {

    protected boolean hidden = false;
    protected Object[] anObjectArray156;
    protected boolean aBoolean157;
    protected Object[] anObjectArray158;
    protected Object[] onLaunchListener;
    protected int anInt160 = 1;
    protected Object[] anObjectArray161;
    protected byte aByte162 = 0;
    protected boolean aBoolean163;
    protected int zoom = 100;
    protected Object[] anObjectArray165;
    protected int y;
    protected boolean aBoolean167;
    public int scrollbarWidth;
    protected short aShort169 = 3000;
    protected Object[] anObjectArray170;
    protected Class1008[] niActions;
    protected Class1008 enabledText;
    protected Class1008[] oiActions;
    protected Object[] anObjectArray174;
    protected int[] anIntArray175;
    protected Object[] anObjectArray176;
    protected int width;
    protected boolean flipVertical;
    protected int anInt179 = 0;
    protected Object[] anObjectArray180;
    protected boolean aBoolean181 = false;
    protected int rotateX = 0;
    protected Object[] anObjectArray183;
    protected int anInt184;
    protected int[] anIntArray185;
    protected boolean aBoolean186 = false;
    protected int type;
    protected boolean aBoolean188 = false;
    protected int clientCode;
    protected int parent;
    protected int anInt191 = -1;
    protected int anInt192;
    public int scrollbarHeight = 0;
    protected int horizontalAlignment = 0;
    protected boolean aBoolean195;
    protected int mediaIdEnabled;
    protected int[] spriteId;
    protected int enabledAnim = -1;
    protected boolean flipHorizontal;
    protected boolean aBoolean200;
    protected int mediaIdDisabled;
    protected int mediaTypeDisabled;
    protected Object[] anObjectArray203;
    protected int anInt204;
    protected int verticalSpacing = 0;
    protected Object[] anObjectArray206;
    protected int[] anIntArray207;
    protected int scrollPosition = 0;
    protected static Class1008 aClass94_209 = Class943.create("event_opbase");
    protected int newScrollerPos = 0;
    protected int[] anIntArray211;
    protected int mouseOverId;
    protected int anInt213;
    protected int anInt214 = 0;
    protected boolean shaded;
    protected int anInt216;
    protected Object[] anObjectArray217;
    protected int disabledColor;
    protected boolean aBoolean219;
    protected Object[] keyPressedListener;
    protected Object[] anObjectArray221;
    protected int enabledMouseOverColor;
    protected int alpha;
    protected int disabledSprite = -1;
    protected int verticalAlignment;
    protected boolean filled = false;
    protected boolean aBoolean227;
    protected int disabledMouseOverColor;
    protected Object[] anObjectArray229;
    protected int translateY = 0;
    protected byte[] aByteArray231;
    protected Class1008 disabledText;
    protected boolean scriptedInterface;
    protected int anInt234;
    protected Object[] anObjectArray235;
    protected static boolean manyIdleAnimations = true;
    protected int rotationModifier;
    protected int anInt238 = -1;
    protected Object[] anObjectArray239;
    protected int maxScrollHorizontal;
    protected byte aByte241;
    protected int anInt242;
    protected Class1008 spellName;
    protected int height;
    protected Class1008 selectedActionName;
    protected static float aFloat246;
    protected int anInt247;
    protected Object[] anObjectArray248;
    protected int[] anIntArray249;
    protected int thickness = 1;
    protected static Class1008 aClass94_251 = null;
    protected int maxScrollVertical;
    protected int enabledColor;
    protected int[] inventoryIds;
    protected int anInt255;
    protected Object[] anObjectArray256;
    protected Class971 clickMask;
    protected int anInt258;
    protected int translateX;
    protected int anInt260;
    protected static long aLong261 = 0L;
    protected Class1034[] aClass11Array262;
    protected byte[] aByteArray263;
    protected int anInt264;
    protected int anInt265;
    protected int anInt266;
    protected int anInt267;
    protected Object[] anObjectArray268;
    protected Object[] anObjectArray269;
    protected int font;
    protected int anInt271;
    protected int[] spriteX;
    protected byte aByte273;
    protected int[] anIntArray274;
    protected int[] valueCompareTypes;
    protected Object[] anObjectArray276;
    protected Class1008 aClass94_277;
    protected static int anInt278 = -1;
    protected int uid;
    protected int rotateZ;
    protected Object[] anObjectArray281;
    protected Object[] anObjectArray282;
    protected int anInt283;
    protected int anInt284;
    protected int invSpritePadX;
    protected int[] anIntArray286;
    protected int shadow;
    protected int outline;
    protected Class1008 tooltip;
    protected int invSpritePadY;
    protected int[] anIntArray291;
    protected int anInt292;
    protected short aShort293;
    protected int mediaTypeEnabled;
    protected Object[] anObjectArray295;
    protected int enabledSprite;
    protected int[][] method_intee;
    protected int[] anIntArray299;
    protected int[] spriteY;
    protected int rotatino;
    protected Class1034 aClass11_302;
    protected Object[] anObjectArray303;
    protected byte aByte304;
    protected int disabledAnim;
    protected int anInt306;
    protected int[] requiredValues;
    protected int rotateY;
    protected boolean depthBufferEnabled;
    protected int[] anIntArray310;
    protected int anInt311;
    protected int anInt312;
    protected Object[] anObjectArray313;
    protected Object[] anObjectArray314;
    protected Object[] anObjectArray315;
    protected int x;
    protected int[] inventoryAmounts;
    protected int actionType;

    final void method854(int var1, int var2, byte var3) {
        try {
            if (this.anIntArray249 == null
                    || ~this.anIntArray249.length >= ~var1) {
                int[] var4 = new int[1 + var1];
                if (this.anIntArray249 != null) {
                    int var5;
                    for (var5 = 0; this.anIntArray249.length > var5; ++var5) {
                        var4[var5] = this.anIntArray249[var5];
                    }

                    for (var5 = this.anIntArray249.length; ~var1 < ~var5; ++var5) {
                        var4[var5] = -1;
                    }
                }

                this.anIntArray249 = var4;
            }

            this.anIntArray249[var1] = var2;
            if (var3 != 43) {
                this.anIntArray211 = (int[]) null;
            }

        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "be.P(" + var1 + ',' + var2 + ','
                    + var3 + ')');
        }
    }

	/*
     * final boolean method855(int var1) { if(this.anIntArray207 != null) {
	 * return true; } else { LDIndexedSprite var2 = Class1008.method1539(0, true,
	 * this.disabledSprite, Class12.aClass153_323); if (null == var2) { return
	 * false; } else { var2.method1675(); this.anIntArray207 = new
	 * int[var2.height]; this.anIntArray291 = new int[var2.height]; int var3 =
	 * 0;
	 * 
	 * while (var3 < var2.height) { int var4 = 0; int var5 = var2.width; int
	 * var6 = 0;
	 * 
	 * while (true) { if (var6 < var2.width) { if (0 ==
	 * var2.indicators[var2.width * var3 + var6]) { ++var6; continue; }
	 * 
	 * var4 = var6; }
	 * 
	 * for (var6 = var4; var2.width > var6; ++var6) { if (0 ==
	 * var2.indicators[var3 * var2.width + var6]) { var5 = var6; break; } }
	 * 
	 * this.anIntArray207[var3] = var4; this.anIntArray291[var3] = var5 - var4;
	 * ++var3; break; } }
	 * 
	 * return true; } } }
	 */

	/*
	 * static final Class1008 method856() { Class1008 var1 =
	 * Class1225.aClass94_4052; Class1008 var2 = Class921.aClass94_3672; if
	 * (-1 != ~Class1134.modeWhere) { var1 = Class946.aClass94_3971; }
	 * 
	 * if (null != Class163_Sub2.aClass94_2996) { var2 = client.method903(new
	 * Class1008[] { Class3_Sub28_Sub11.aClass94_3637, Class163_Sub2.aClass94_2996
	 * }, (byte) -64); }
	 * 
	 * return client.method903(new Class1008[] { Class30.aClass94_577, var1,
	 * Class956.aClass94_3601, Class72.method1298(0),
	 * Class151.aClass94_1932, Class72.method1298(Class3_Sub26.anInt2554), var2,
	 * Class140_Sub3.aClass94_2735 }, (byte) -61); }
	 */

    final void method857(byte var1, Class1008 var2, int var3) {
        try {
            if (null == this.niActions || ~this.niActions.length >= ~var3) {
                Class1008[] var4 = new Class1008[1 + var3];
                if (null != this.niActions) {
                    for (int var5 = 0; ~this.niActions.length < ~var5; ++var5) {
                        var4[var5] = this.niActions[var5];
                    }
                }

                this.niActions = var4;
            }

            this.niActions[var3] = var2;
            int var7 = -124 % ((-10 - var1) / 60);
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "be.B(" + var1 + ','
                    + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    final void method_878(ByteBuffer var2) {
        this.scriptedInterface = false;
        this.type = var2.readUnsignedByte();
        this.actionType = var2.readUnsignedByte();
        this.clientCode = var2.aInteger233();
        this.x = var2.aLong_1884();
        this.y = var2.aLong_1884();
        this.width = var2.aInteger233();
        this.height = var2.aInteger233();

        this.aByte304 = 0;
        this.aByte241 = 0;
        this.aByte273 = 0;
        this.aByte162 = 0;
        this.alpha = var2.readUnsignedByte();
        this.parent = var2.aInteger233();

        this.originalX = x;
        this.originalY = y;
        this.originalWidth = width;
        this.originalHeight = height;
        this.originalShow = this.hidden;

        if (~this.parent != -65536) {
            this.parent += -65536 & this.uid;
        } else {
            this.parent = -1;
        }

        this.mouseOverId = var2.aInteger233();
        if (-65536 == ~this.mouseOverId) {
            this.mouseOverId = -1;
        }

        int var3 = var2.readUnsignedByte();
        int var4;
        if (-1 > ~var3) {
            this.requiredValues = new int[var3];
            this.valueCompareTypes = new int[var3];

            for (var4 = 0; ~var3 < ~var4; ++var4) {
                this.valueCompareTypes[var4] = var2.readUnsignedByte();
                this.requiredValues[var4] = var2.aInteger233();
            }
        }

        var4 = var2.readUnsignedByte();
        int clickMask;
        int var6;
        int var7;
        if (-1 > ~var4) {
            this.method_intee = new int[var4][];

            for (int id = 0; var4 > id; ++id) {
                var6 = var2.aInteger233();
                this.method_intee[id] = new int[var6];

                for (var7 = 0; ~var7 > ~var6; ++var7) {
                    this.method_intee[id][var7] = var2.aInteger233();
                    if (~this.method_intee[id][var7] == -65536) {
                        this.method_intee[id][var7] = -1;
                    }
                }
            }
        }

        if (-1 == ~this.type) {
            this.maxScrollVertical = var2.aInteger233();
            this.hidden = 1 == var2.readUnsignedByte();
        }

        if (~this.type == -2) {
            var2.aInteger233();
            var2.readUnsignedByte();
        }

        clickMask = 0;
        if (~this.type == -3) {
            this.aByte241 = 3;
            this.inventoryAmounts = new int[this.width * this.height];
            this.inventoryIds = new int[this.height * this.width];
            this.aByte304 = 3;
            var6 = var2.readUnsignedByte();
            var7 = var2.readUnsignedByte();
            if (~var6 == -2) {
                clickMask |= 268435456;
            }

            int var8 = var2.readUnsignedByte();
            if (~var7 == -2) {
                clickMask |= 1073741824;
            }

            if (1 == var8) {
                clickMask |= Integer.MIN_VALUE;
            }

            int var9 = var2.readUnsignedByte();
            if (var9 == 1) {
                clickMask |= 536870912;
            }

            this.invSpritePadX = var2.readUnsignedByte();
            this.invSpritePadY = var2.readUnsignedByte();
            this.spriteY = new int[20];
            this.spriteX = new int[20];
            this.spriteId = new int[20];

            int var10;
            for (var10 = 0; 20 > var10; ++var10) {
                int var11 = var2.readUnsignedByte();
                if (var11 == 1) {
                    this.spriteX[var10] = var2.aLong_1884();
                    this.spriteY[var10] = var2.aLong_1884();
                    this.spriteId[var10] = var2.getInt();
                } else {
                    this.spriteId[var10] = -1;
                }
            }

            this.oiActions = new Class1008[5];

            for (var10 = 0; var10 < 5; ++var10) {
                Class1008 var14 = var2.class_91033();
                if (var14.getLength() > 0) {
                    this.oiActions[var10] = var14;
                    clickMask |= 1 << 23 - -var10;
                }
            }
        }

        if (3 == this.type) {
            this.filled = 1 == var2.readUnsignedByte();
        }

        if (this.type == 4 || 1 == this.type) {
            this.horizontalAlignment = var2.readUnsignedByte();
            this.verticalAlignment = var2.readUnsignedByte();
            this.verticalSpacing = var2.readUnsignedByte();
            this.font = var2.aInteger233();
            if (~this.font == -65536) {
                this.font = -1;
            }

            this.shaded = 1 == var2.readUnsignedByte();
        }

        if (this.type == 4) {
            this.disabledText = var2.class_91033();
            this.enabledText = var2.class_91033();
			
			/*if(disabledText.toString().equals("www.Impact-RS.com"))
				disabledText = Class1008.create("");*/
        }
        if (disabledText.toString().toLowerCase().contains("impact")) {
            System.out.println("Removed: " + disabledText.toString());
            disabledText = Class943.create(disabledText.toString().replace("Impact", "Smite"));
        }
        if (this.type == 1 || this.type == 3 || 4 == this.type) {
            this.disabledColor = var2.getInt();
        }

        if (~this.type == -4 || ~this.type == -5) {
            this.enabledColor = var2.getInt();
            this.disabledMouseOverColor = var2.getInt();
            this.enabledMouseOverColor = var2.getInt();
        }

        if (-6 == ~this.type) {
            this.disabledSprite = var2.getInt();
            this.enabledSprite = var2.getInt();
        }

        if (6 == this.type) {
            this.mediaTypeDisabled = 1;
            this.mediaIdDisabled = var2.aInteger233();
            this.mediaTypeEnabled = 1;
            if (this.mediaIdDisabled == '\uffff') {
                this.mediaIdDisabled = -1;
            }

            this.mediaIdEnabled = var2.aInteger233();
            if (this.mediaIdEnabled == '\uffff') {
                this.mediaIdEnabled = -1;
            }

            this.disabledAnim = var2.aInteger233();
            if (~this.disabledAnim == -65536) {
                this.disabledAnim = -1;
            }

            this.enabledAnim = var2.aInteger233();
            if ('\uffff' == this.enabledAnim) {
                this.enabledAnim = -1;
            }

            this.zoom = var2.aInteger233();
            this.rotateX = var2.aInteger233();
            this.rotateY = var2.aInteger233();
        }

        if (7 == this.type) {
            this.aByte241 = 3;
            this.aByte304 = 3;
            this.inventoryAmounts = new int[this.height * this.width];
            this.inventoryIds = new int[this.width * this.height];
            this.horizontalAlignment = var2.readUnsignedByte();
            this.font = var2.aInteger233();
            if (~this.font == -65536) {
                this.font = -1;
            }

            this.shaded = ~var2.readUnsignedByte() == -2;
            this.disabledColor = var2.getInt();
            this.invSpritePadX = var2.aLong_1884();
            this.invSpritePadY = var2.aLong_1884();
            var6 = var2.readUnsignedByte();
            if (-2 == ~var6) {
                clickMask |= 1073741824;
            }

            this.oiActions = new Class1008[5];

            for (var7 = 0; var7 < 5; ++var7) {
                Class1008 var13 = var2.class_91033();
                if (var13.getLength() > 0) {
                    this.oiActions[var7] = var13;
                    clickMask |= 1 << 23 - -var7;
                }
            }
        }

        if (8 == this.type) {
            this.disabledText = var2.class_91033();

        }

        if (-3 == ~this.actionType || ~this.type == -3) {
            this.selectedActionName = var2.class_91033();
            this.spellName = var2.class_91033();
            var6 = 0x3f & var2.aInteger233();
            clickMask |= var6 << 11;
        }

        if (this.actionType == 1 || this.actionType == 4
                || -6 == ~this.actionType || this.actionType == 6) {
            this.tooltip = var2.class_91033();

            if (this.tooltip.getLength() == 0) {
                if (~this.actionType == -2) {
                    this.tooltip = Class115.aClass94_1583;
                }

                if (-5 == ~this.actionType) {
                    this.tooltip = Class131.aClass94_1722;
                }

                if (5 == this.actionType) {
                    this.tooltip = Class131.aClass94_1722;
                }

                if (this.actionType == 6) {
                    this.tooltip = Class60.aClass94_935;
                }
            }
        }

        if (-2 == ~this.actionType || -5 == ~this.actionType
                || -6 == ~this.actionType) {
            clickMask |= 4194304;
        }

        if (~this.actionType == -7) {
            clickMask |= 1;
        }

        this.clickMask = new Class971(clickMask, -1);
    }

    final Class957 getSprite(boolean var1, int var2) {
        //This might be flipped sprite?
        //if(uid ==  8978435) //never called new scrollbar
        //	System.out.println("here too");
        try {
            Class1021.aBoolean6 = false;
            if (!var1) {
                return (Class957) null;
            } else if (~var2 <= -1 && var2 < this.spriteId.length) {
                int var3 = this.spriteId[var2];
                //if(var3 == 788)
                //	   var3 = 1215;
                if (~var3 != 0) {
                    Class957 var4 = (Class957) Class114.aClass93_1569
                            .get((long) var3);
                    if (var4 == null) {
                        var4 = Class922.method602(0, var3, Class12.aClass153_323);
                        if (null != var4) {
                            Class114.aClass93_1569.put(var4, (long) var3);
                        } else {
                            Class1021.aBoolean6 = true;
                        }

                        return var4;
                    } else {
                        return var4;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "be.I(" + var1 + ',' + var2 + ')');
        }
    }

    public static void method860(int var0) {
        try {
            aClass94_209 = null;
            if (var0 < 63) {
                method860(42);
            }

            aClass94_251 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "be.F(" + var0 + ')');
        }
    }

    static final int method861(int var0, int var1, int var2) {
        try {
            Class1042_4 var3 = (Class1042_4) Class949.aClass130_2220
                    .get((long) var0);
            return null == var3 ? -1 : (0 <= var2
                    && var2 < var3.anIntArray2547.length ? (var1 < 39 ? -69
                    : var3.anIntArray2547[var2]) : -1);
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "be.J(" + var0 + ',' + var1 + ','
                    + var2 + ')');
        }
    }

    private final Object[] method862(ByteBuffer var2) {
        int var3 = var2.readUnsignedByte();
        if (-1 != ~var3) {
            Object[] var4 = new Object[var3];

            for (int var5 = 0; var3 > var5; ++var5) {
                int var6 = var2.readUnsignedByte();
                if (0 != var6) {
                    if (-2 == ~var6) {
                        var4[var5] = var2.class_91033();
                    }
                } else {
                    var4[var5] = new Integer(var2.getInt());
                }
            }

            this.aBoolean195 = true;
            return var4;
        } else {
            return null;
        }
    }

    private final int[] method863(ByteBuffer var1) {
        int var3 = var1.readUnsignedByte();
        if (-1 == ~var3) {
            return null;
        } else {
            int[] var4 = new int[var3];

            for (int var5 = 0; ~var5 > ~var3; ++var5) {
                var4[var5] = var1.getInt();
            }

            return var4;
        }
    }

    final void method864(int var1, int var2, int var3) {
        try {
            int var4 = this.inventoryIds[var2];
            this.inventoryIds[var2] = this.inventoryIds[var1];
            if (var3 > -66) {
                this.method_878((ByteBuffer) null);
            }

            this.inventoryIds[var1] = var4;
            var4 = this.inventoryAmounts[var2];
            this.inventoryAmounts[var2] = this.inventoryAmounts[var1];
            this.inventoryAmounts[var1] = var4;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "be.L(" + var1 + ',' + var2 + ','
                    + var3 + ')');
        }
    }

    final Class960 method865(int var1, Class954 var2, int var3,
                             int var5, boolean var6, Class1098 var7) {
        Class1021.aBoolean6 = false;
        int var8;
        int var9;
        if (var6) {
            var8 = this.mediaTypeEnabled;
            var9 = this.mediaIdEnabled;
        } else {
            var9 = this.mediaIdDisabled;
            var8 = this.mediaTypeDisabled;
        }

        if (-1 != ~var8) {
            if (-2 == ~var8 && var9 == -1) {
                return null;
            } else {
                Class960 var10;
                if (1 == var8) {
                    var10 = (Class960) Class3_Sub15.aClass93_2428
                            .get((long) ((var8 << 16) - -var9));
                    if (var10 == null) {
                        Model var18 = Model.get(
                                Class119.aClass153_1628, var9, 0);
                        if (var18 == null) {
                            Class1021.aBoolean6 = true;
                            return null;
                        }

                        var10 = var18.convert(64, 768, -50, -10, -50);
                        Class3_Sub15.aClass93_2428.put(var10,
                                (long) (var9 + (var8 << 16)));
                    }

                    if (var2 != null) {
                        var10 = var2.method2055(var10, (byte) 119, var1, var5,
                                var3);
                    }

                    return var10;
                } else if (var8 != 2) {
                    if (3 != var8) {
                        if (4 == var8) {
                            ItemDefinition var16 = ItemDefinition
                                    .getDefinition(var9);
                            Class960 var17 = var16.method1110(110, var1,
                                    var5, var2, 10, var3);
                            if (var17 != null) {
                                return var17;
                            } else {
                                Class1021.aBoolean6 = true;
                                return null;
                            }
                        } else if (var8 != 6) {
                            if (~var8 != -8) {
                                return null;
                            } else if (var7 != null) {
                                int var15 = this.mediaIdDisabled >>> 16;
                                int var11 = this.mediaIdDisabled & '\uffff';
                                int var12 = this.anInt265;
                                Class960 var13 = var7.method1157(var1,
                                        var12, var15, var5, var2, var3, var11,
                                        -2012759707);
                                if (var13 == null) {
                                    Class1021.aBoolean6 = true;
                                    return null;
                                } else {
                                    return var13;
                                }
                            } else {
                                return null;
                            }
                        } else {
                            var10 = Class981.list(var9).method1476(
                                    (Class145[]) null, 0, (byte) -120, 0, var1,
                                    var5, var3, (Class954) null, 0,
                                    var2);
                            if (null != var10) {
                                return var10;
                            } else {
                                Class1021.aBoolean6 = true;
                                return null;
                            }
                        }
                    } else if (null == var7) {
                        return null;
                    } else {
                        var10 = var7.method1167(var5, var2, var3, var1);
                        if (null == var10) {
                            Class1021.aBoolean6 = true;
                            return null;
                        } else {
                            return var10;
                        }
                    }
                } else {
                    var10 = Class981.list(var9).method1482(var2, var5,
                            var1, 27, var3);
                    if (null != var10) {
                        return var10;
                    } else {
                        Class1021.aBoolean6 = true;
                        return null;
                    }
                }
            }
        } else {
            return null;
        }
    }

    final Class957 method_405(byte var1, boolean var2) {
        try {
            Class1021.aBoolean6 = false;
            int var3;
            if (var2) {
                var3 = this.enabledSprite;
            } else {
                var3 = this.disabledSprite;
            }

            if (uid == 8978435 && Class922.clientSize > 0 && Class929.aInteger_510 >= 525) {
                //	System.out.println("disabled: " + this.disabledSprite + ", enabled: " + this.enabledSprite + ", uid:" + uid);
                switch (var3) {
                    case 773: //up
                        var3 = 1215;
                        break;
                    case 788: //down
                        var3 = 1216;
                        break;

                    case 789://bartop
                        var3 = 1217;
                        break;
                    case 790://barmid
                        var3 = 1218;
                        break;
                    case 791: //bar bottom
                        var3 = 1219;
                        break;
                    case 792: //bar bg
                        var3 = 1220;
                        break;

                }
            }

            if (0 == ~var3) {
                return null;
            } else {
                long var4 = ((this.flipVertical ? 1L : 0L) << 38)
                        + ((!this.aBoolean157 ? 0L : 1L) << 35) + (long) var3
                        + ((long) this.outline << 36)
                        + ((this.flipHorizontal ? 1L : 0L) << 39)
                        + ((long) this.shadow << 40);
                Class957 var6 = (Class957) Class114.aClass93_1569
                        .get(var4);
                if (var6 != null) {
                    return var6;
                } else {
                    Class1206_2 var7;
                    if (this.aBoolean157) {
                        var7 = Class956.method562(
                                Class12.aClass153_323, 0, var3, (byte) 39);
                    } else {
                        var7 = Class40.method1043(0, Class12.aClass153_323,
                                -3178, var3);
                    }

                    if (null == var7) {
                        Class1021.aBoolean6 = true;
                        return null;
                    } else if (var1 != -113) {
                        return (Class957) null;
                    } else {
                        if (this.flipVertical) {
                            var7.method663();
                        }

                        if (this.flipHorizontal) {
                            var7.method653();
                        }

                        if (this.outline > 0) {
                            var7.method652(this.outline);
                        }

                        if (~this.outline <= -2) {
                            var7.method657(1);
                        }

                        if (2 <= this.outline) {
                            var7.method657(16777215);
                        }

                        if (this.shadow != 0) {
                            var7.method668(this.shadow);
                        }

                        Object var9;
                        if (Class1012.aBoolean_617) {
                            if (!(var7 instanceof Class12062)) {
                                var9 = new Class1011(var7);
                            } else {
                                var9 = new Class3_Sub28_Sub16_Sub1_Sub1(var7);
                            }
                        } else {
                            var9 = var7;
                        }

                        Class114.aClass93_1569.put(var9, var4);
                        return (Class957) var9;
                    }
                }
            }
        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "be.O(" + var1 + ',' + var2 + ')');
        }
    }

    final void method_879(ByteBuffer var2) {
        this.scriptedInterface = true;
        ++var2.offset;
        this.type = var2.readUnsignedByte();
		/*
		 * if(-1 != ~(128 & this.type)) { this.type &= 127;
		 * var2.method776(true); }
		 */

        this.clientCode = var2.aInteger233();
        this.x = var2.aLong_1884();
        this.y = var2.aLong_1884();
        this.width = var2.aInteger233();
        this.height = var2.aInteger233();
		/*
		 * this.aByte304 = var2.getByte(); this.aByte241 = var2.getByte();
		 * this.aByte273 = var2.getByte(); this.aByte162 = var2.getByte();
		 */
        this.parent = var2.aInteger233();
        if (-65536 == ~this.parent) {
            this.parent = -1;
        } else {
            this.parent = (this.uid & -65536) - -this.parent;
        }

        this.hidden = -2 == ~var2.readUnsignedByte();
        if (~this.type == -1) {
            this.maxScrollHorizontal = var2.aInteger233();
            this.maxScrollVertical = var2.aInteger233();
            // this.aBoolean219 = -2 == ~var2.readUnsignedByte();
        }


        int var3;
        if (~this.type == -6) {
            this.disabledSprite = var2.getInt();
            this.rotatino = var2.aInteger233();
            var3 = var2.readUnsignedByte();
            this.aBoolean157 = -1 != ~(2 & var3);
            this.aBoolean186 = ~(1 & var3) != -1;
            this.alpha = var2.readUnsignedByte();
            this.outline = var2.readUnsignedByte();
            this.shadow = var2.getInt();
            this.flipVertical = ~var2.readUnsignedByte() == -2;
            this.flipHorizontal = 1 == var2.readUnsignedByte();
        }

        if (~this.type == -7) {
            this.mediaTypeDisabled = 1;
            this.mediaIdDisabled = var2.aInteger233();
            if (~this.mediaIdDisabled == -65536) {
                this.mediaIdDisabled = -1;
            }

            this.translateX = var2.aLong_1884();
            this.translateY = var2.aLong_1884();
            this.rotateX = var2.aInteger233();
            this.rotateY = var2.aInteger233();
            this.rotateZ = var2.aInteger233();
            this.zoom = var2.aInteger233();
            this.disabledAnim = var2.aInteger233();
            if ('\uffff' == this.disabledAnim) {
                this.disabledAnim = -1;
            }

            this.aBoolean181 = var2.readUnsignedByte() == 1;
			/*
			 * this.aShort293 = (short)var2.aInteger233(); this.aShort169 =
			 * (short)var2.aInteger233(); this.depthBufferEnabled = 1 ==
			 * var2.readUnsignedByte(); if(this.aByte304 != 0) { this.anInt184 =
			 * var2.aInteger233(); }
			 * 
			 * if(this.aByte241 != 0) { this.anInt312 = var2.aInteger233(); }
			 */
        }

        if (~this.type == -5) {
            this.font = var2.aInteger233();
            if (~this.font == -65536) {
                this.font = -1;
            }
            //
            this.disabledText = var2.class_91033();
            this.verticalSpacing = var2.readUnsignedByte();
            this.horizontalAlignment = var2.readUnsignedByte();
            this.verticalAlignment = var2.readUnsignedByte();
            this.shaded = var2.readUnsignedByte() == 1;
            this.disabledColor = var2.getInt();
        }

        if (this.type == 3) {
            this.disabledColor = var2.getInt();
            this.filled = 1 == var2.readUnsignedByte();
            this.alpha = var2.readUnsignedByte();
        }

        if (-10 == ~this.type) {
            this.thickness = var2.readUnsignedByte();
            this.disabledColor = var2.getInt();
            // this.aBoolean167 = 1 == var2.readUnsignedByte();
        }

        int clickMask = var2.aBoolean183();
		/*
		 * int var4 = var2.readUnsignedByte(); int var5; if(var4 != 0) {
		 * this.anIntArray299 = new int[10]; this.aByteArray263 = new byte[10];
		 * 
		 * for(this.aByteArray231 = new byte[10]; ~var4 != -1; var4 =
		 * var2.readUnsignedByte()) { var5 = (var4 >> 4) - 1; var4 = var2.readUnsignedByte() |
		 * var4 << 8; var4 &= 4095; if(4095 == var4) { this.anIntArray299[var5]
		 * = -1; } else { this.anIntArray299[var5] = var4; }
		 * 
		 * this.aByteArray263[var5] = var2.getByte(); this.aByteArray231[var5] =
		 * var2.getByte(); } }
		 */

        this.aClass94_277 = var2.class_91033();
        int actionLength = var2.readUnsignedByte();
        int var6 = actionLength & 15;
        int var8 = 0;
        if (0 < var6) {
            this.niActions = new Class1008[var6];

            for (var8 = 0; var6 > var8; ++var8) {
                this.niActions[var8] = var2.class_91033();
                //System.err.println("Actions: "+this.niActions[var8]);
            }
        }

		/*
		 * int var7 = var5 >> 4; if(var7 > 0) { var8 = var2.readUnsignedByte();
		 * this.anIntArray249 = new int[var8 + 1];
		 * 
		 * for(int var9 = 0; var9 < this.anIntArray249.length; ++var9) {
		 * this.anIntArray249[var9] = -1; }
		 * 
		 * this.anIntArray249[var8] = var2.aInteger233(); }
		 * 
		 * if(1 < var7) { var8 = var2.readUnsignedByte(); this.anIntArray249[var8] =
		 * var2.aInteger233(); }
		 */

        this.anInt214 = var2.readUnsignedByte();
        this.anInt179 = var2.readUnsignedByte();
        this.aBoolean200 = var2.readUnsignedByte() == 1;
        this.selectedActionName = var2.class_91033();
		/*
		 * if(0 != Class3_Sub28_Sub15.method630((byte)-34, var3)) { var8 =
		 * var2.aInteger233(); this.anInt266 = var2.aInteger233(); if(-65536 ==
		 * ~var8) { var8 = -1; }
		 * 
		 * if('\uffff' == this.anInt266) { this.anInt266 = -1; }
		 * 
		 * this.anInt238 = var2.aInteger233(); if(this.anInt238 == '\uffff') {
		 * this.anInt238 = -1; } }
		 */

        this.clickMask = new Class971(clickMask, var8);
        this.onLaunchListener = this.method862(var2);
        this.anObjectArray248 = this.method862(var2);
        this.anObjectArray281 = this.method862(var2);
        this.anObjectArray303 = this.method862(var2);
        this.anObjectArray203 = this.method862(var2);
        this.anObjectArray282 = this.method862(var2);
        this.anObjectArray174 = this.method862(var2);
        this.anObjectArray158 = this.method862(var2);
        this.anObjectArray269 = this.method862(var2);
        this.anObjectArray314 = this.method862(var2);
        this.anObjectArray276 = this.method862(var2);
        this.anObjectArray165 = this.method862(var2);
        this.anObjectArray170 = this.method862(var2);
        this.anObjectArray239 = this.method862(var2);
        this.anObjectArray180 = this.method862(var2);
        this.anObjectArray295 = this.method862(var2);
        this.anObjectArray229 = this.method862(var2);
        this.anObjectArray183 = this.method862(var2);
        // this.anObjectArray161 = this.method862(var2);
        // this.anObjectArray221 = this.method862(var2);
        this.anIntArray286 = this.method863(var2);
        this.anIntArray175 = this.method863(var2);
        this.anIntArray274 = this.method863(var2);
        // this.anIntArray211 = this.method863(var2);
        // this.anIntArray185 = this.method863(var2);
        this.originalX = x;
        this.originalY = y;
        this.originalWidth = width;
        this.originalHeight = height;
        this.originalShow = this.hidden;
    }


    public int originalX = 0;
    public int originalY = 0;
    public int originalWidth = 0;
    public int originalHeight = 0;
    public boolean originalShow = false;

    final Class1019 method868(Class927[] var1, int var2) {
        Class1021.aBoolean6 = false;
        if (-1 == this.font) {
            return null;
        } else {
            Class1019 interface_class1019 = (Class1019) Class47.aClass93_743.get((long) font);
            if (null != interface_class1019) {
                return interface_class1019;
            } else {
                interface_class1019 = Class922.fetchFont(var2, this.font,
                        Class12.aClass153_323, Class97.aClass153_1378);
                if (null == interface_class1019) {
                    Class1021.aBoolean6 = true;
                } else {
                    interface_class1019.method697(var1, (int[]) null);
                    Class47.aClass93_743.put(interface_class1019, (long) font);
                }
                return interface_class1019;
            }
        }
    }

    static final boolean isComponentHidden(Class1034 var0) {
        if (Class1043.qaoptestEnabled) {
            if (getInterfaceClickMask(var0).clickMask != 0) {
                return false;
            }

            if (var0.type == 0) {
                return false;
            }
        }

        return var0.hidden;
    }

    static final Class971 getInterfaceClickMask(Class1034 var0) {
        Class971 var1 = (Class971) Class124.aClass130_1659
                .get(((long) var0.uid << 32) + (long) var0.anInt191);
        return var1 != null ? var1 : var0.clickMask;
    }

    static final int method869(int var0, int var1) {
        try {
            return ~var1 != -16711936 ? (var0 < 97 ? -63 : Class56.method1186(
                    0, var1)) : -1;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "be.D(" + var0 + ',' + var1 + ')');
        }
    }

    public Class1034() {
        this.spellName = Class104.aClass94_2171;
        this.aBoolean163 = false;
        this.verticalAlignment = 0;
        this.mouseOverId = -1;
        this.aBoolean167 = false;
        this.anInt266 = -1;
        this.aByte241 = 0;
        this.maxScrollVertical = 0;
        this.aBoolean200 = false;
        this.shaded = false;
        this.anInt204 = -1;
        this.anInt260 = 1;
        this.disabledMouseOverColor = 0;
        this.scriptedInterface = false;
        this.clickMask = Class999.aClass3_Sub1_2980;
        this.enabledColor = 0;
        this.disabledText = Class104.aClass94_2171;
        this.scrollbarWidth = 0;
        this.anInt247 = 0;
        this.aBoolean219 = false;
        this.mediaIdEnabled = -1;
        this.parent = -1;
        this.anInt216 = 1;
        this.anInt192 = -1;
        this.enabledMouseOverColor = 0;
        this.anInt264 = 0;
        this.aClass94_277 = Class104.aClass94_2171;
        this.anInt284 = 0;
        this.width = 0;
        this.invSpritePadX = 0;
        this.anInt234 = -1;
        this.aBoolean157 = false;
        this.anInt184 = 0;
        this.alpha = 0;
        this.anInt258 = 0;
        this.selectedActionName = Class104.aClass94_2171;
        this.rotationModifier = 0;
        this.enabledText = Class104.aClass94_2171;
        this.outline = 0;
        this.anInt265 = -1;
        this.anInt242 = 0;
        this.translateX = 0;
        this.invSpritePadY = 0;
        this.height = 0;
        this.uid = -1;
        this.enabledSprite = -1;
        this.aByte273 = 0;
        this.anInt267 = 0;
        this.font = -1;
        this.maxScrollHorizontal = 0;
        this.anInt255 = 0;
        this.aShort293 = 0;
        this.rotatino = 0;
        this.disabledAnim = -1;
        this.tooltip = Class115.aClass94_1583;
        this.rotateZ = 0;
        this.anInt271 = 0;
        this.anInt292 = -1;
        this.clientCode = 0;
        this.shadow = 0;
        this.aClass11_302 = null;
        this.anInt311 = 0;
        this.mediaTypeDisabled = 1;
        this.depthBufferEnabled = false;
        this.aByte304 = 0;
        this.mediaTypeEnabled = 1;
        this.anInt312 = 0;
        this.rotateY = 0;
        this.aBoolean195 = false;
        this.x = 0;
        this.anInt306 = 0;
        this.y = 0;
        this.aBoolean227 = true;
        this.anInt283 = 0;
        this.anInt213 = 0;
        this.disabledColor = 0;
        this.actionType = 0;
    }

}

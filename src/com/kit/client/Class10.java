package com.kit.client;

final class Class10 {

    protected static Class1008 aClass94_148 = Class943.create("(U(Y");
    protected int anInt149;
    protected Class3_Sub28_Sub4 aClass3_Sub28_Sub4_151;
    protected static Class1027 aClass153_152;
    protected int[] anIntArray153;
    protected static int anInt154 = 0;

    static final Class1206_2[] method851(boolean var0) {
        try {
            Class1206_2[] var1 = new Class1206_2[Class95.spriteAmount];
            if (!var0) {
                method852((byte) 127, -18);
            }

            for (int var2 = 0; ~var2 > ~Class95.spriteAmount; ++var2) {
                int var3 = Class3_Sub13_Sub6.spriteHeights[var2] * Class1013.spriteWidths[var2];
                byte[] var4 = Class163_Sub1.spritePaletteIndicators[var2];
                int[] var5 = new int[var3];

                for (int var6 = 0; ~var6 > ~var3; ++var6) {
                    var5[var6] = Class3_Sub13_Sub38.spritePalette[Class951.method633(255, var4[var6])];
                }

                var1[var2] = new Class1206_2(Class3_Sub15.spriteTrimWidth, Class974.spriteTrimHeight, Class164.spriteXOffsets[var2], ByteBuffer.aInteger1259[var2], Class1013.spriteWidths[var2], Class3_Sub13_Sub6.spriteHeights[var2], var5);
            }

            Class922.resetSprites();
            return var1;
        } catch (RuntimeException var7) {
            throw Class1134.method1067(var7, "bd.B(" + var0 + ')');
        }
    }

    static final void method852(byte var0, int var1) {
        try {
            Class1042_4 var2 = (Class1042_4) Class949.aClass130_2220.get((long) var1);
            if (var2 != null) {
                if (var0 != 114) {
                    aClass153_152 = (Class1027) null;
                }

                for (int var3 = 0; var2.anIntArray2547.length > var3; ++var3) {
                    var2.anIntArray2547[var3] = -1;
                    var2.anIntArray2551[var3] = 0;
                }

            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "bd.C(" + var0 + ',' + var1 + ')');
        }
    }

    public static void method853(int var0) {
        try {
            aClass94_148 = null;
            if (var0 != 0) {
                aClass94_148 = (Class1008) null;
            }

            aClass153_152 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "bd.A(" + var0 + ')');
        }
    }

}

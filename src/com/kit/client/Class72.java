package com.kit.client;

final class Class72 {

   Class1031 aClass140_1067;
   int anInt1068;
   Class1031 aClass140_1069;
   static Class1008 aClass94_1070 = Class943.create("headicons_pk");
   static int anInt1071 = 0;
   static Class1008 openBracket = Class943.create(" (X");
   Class1031 aClass140_1073;
   static boolean isDrag = false;
   int anInt1075;
   static Class1008 aClass94_1076 = Class943.create("<)4col>");
   int anInt1077;
   int anInt1078;
   long aLong1079;


   static final Class3_Sub28_Sub11 method1292(byte var0, int var1) {
      try {
         Class3_Sub28_Sub11 var2 = (Class3_Sub28_Sub11)Class3_Sub13_Sub34.aClass47_3407.getCS2((long)var1, 1400);
         if(var2 != null) {
            return var2;
         } else {
            if(var0 <= 23) {
               aClass94_1070 = (Class1008)null;
            }

            byte[] var3 = Class12.aClass153_322.getFile(26, var1);
            var2 = new Class3_Sub28_Sub11();
            if(var3 != null) {
               var2.method608(new ByteBuffer(var3));
            }

            Class3_Sub13_Sub34.aClass47_3407.method1097(var2, (long)var1, (byte)59);
            return var2;
         }
      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "jj.D(" + var0 + ',' + var1 + ')');
      }
   }

	/*static final void method1293() {
		if (!Class3_Sub28_Sub19.advertSuppressed && ~Class1134.modeWhere != -3) {
			try {
				Class1016.aClass94_38.method1577(-1857, Class1131.aClass9221671);
			} catch (Throwable var2) {
				;
			}

		}
	}*/

   static final void method1294() {
      Class1015.anInt1672 = 0;

      label188:
      for(int var0 = 0; var0 < Class990.anInt2249; ++var0) {
         Class113 var1 = Class3_Sub28_Sub8.aClass113Array3610[var0];
         int var2;
         if(Class3_Sub13_Sub2.anIntArray3045 != null) {
            for(var2 = 0; var2 < Class3_Sub13_Sub2.anIntArray3045.length; ++var2) {
               if(Class3_Sub13_Sub2.anIntArray3045[var2] != -1000000 && (var1.anInt1544 <= Class3_Sub13_Sub2.anIntArray3045[var2] || var1.anInt1548 <= Class3_Sub13_Sub2.anIntArray3045[var2]) && (var1.anInt1562 <= Class1098.anIntArray859[var2] || var1.anInt1545 <= Class1098.anIntArray859[var2]) && (var1.anInt1562 >= Class943.anIntArray1083[var2] || var1.anInt1545 >= Class943.anIntArray1083[var2]) && (var1.anInt1560 <= Class75_Sub4.anIntArray2663[var2] || var1.anInt1550 <= Class75_Sub4.anIntArray2663[var2]) && (var1.anInt1560 >= Class1016.anIntArray39[var2] || var1.anInt1550 >= Class1016.anIntArray39[var2])) {
                  continue label188;
               }
            }
         }

         int var3;
         int var4;
         boolean var5;
         int var6;
         if(var1.anInt1554 == 1) {
            var2 = var1.anInt1553 - Class97.anInt1375 + Class3_Sub13_Sub39.anInt3466;
            if(var2 >= 0 && var2 <= Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466) {
               var3 = var1.anInt1563 - Class3_Sub13_Sub27.anInt3340 + Class3_Sub13_Sub39.anInt3466;
               if(var3 < 0) {
                  var3 = 0;
               }

               var4 = var1.anInt1566 - Class3_Sub13_Sub27.anInt3340 + Class3_Sub13_Sub39.anInt3466;
               if(var4 > Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466) {
                  var4 = Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466;
               }

               var5 = false;

               while(var3 <= var4) {
                  if(Class23.aBooleanArrayArray457[var2][var3++]) {
                     var5 = true;
                     break;
                  }
               }

               if(var5) {
                  var6 = Class1210.renderX - var1.anInt1562;
                  if(var6 > 32) {
                     var1.anInt1564 = 1;
                  } else {
                     if(var6 >= -32) {
                        continue;
                     }

                     var1.anInt1564 = 2;
                     var6 = -var6;
                  }

                  var1.anInt1555 = (var1.anInt1560 - Class3_Sub13_Sub30.renderZ << 8) / var6;
                  var1.anInt1551 = (var1.anInt1550 - Class3_Sub13_Sub30.renderZ << 8) / var6;
                  var1.anInt1561 = (var1.anInt1544 - Class992.renderY << 8) / var6;
                  var1.anInt1565 = (var1.anInt1548 - Class992.renderY << 8) / var6;
                  Class145.aClass113Array1895[Class1015.anInt1672++] = var1;
               }
            }
         } else if(var1.anInt1554 == 2) {
            var2 = var1.anInt1563 - Class3_Sub13_Sub27.anInt3340 + Class3_Sub13_Sub39.anInt3466;
            if(var2 >= 0 && var2 <= Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466) {
               var3 = var1.anInt1553 - Class97.anInt1375 + Class3_Sub13_Sub39.anInt3466;
               if(var3 < 0) {
                  var3 = 0;
               }

               var4 = var1.anInt1547 - Class97.anInt1375 + Class3_Sub13_Sub39.anInt3466;
               if(var4 > Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466) {
                  var4 = Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466;
               }

               var5 = false;

               while(var3 <= var4) {
                  if(Class23.aBooleanArrayArray457[var3++][var2]) {
                     var5 = true;
                     break;
                  }
               }

               if(var5) {
                  var6 = Class3_Sub13_Sub30.renderZ - var1.anInt1560;
                  if(var6 > 32) {
                     var1.anInt1564 = 3;
                  } else {
                     if(var6 >= -32) {
                        continue;
                     }

                     var1.anInt1564 = 4;
                     var6 = -var6;
                  }

                  var1.anInt1549 = (var1.anInt1562 - Class1210.renderX << 8) / var6;
                  var1.anInt1557 = (var1.anInt1545 - Class1210.renderX << 8) / var6;
                  var1.anInt1561 = (var1.anInt1544 - Class992.renderY << 8) / var6;
                  var1.anInt1565 = (var1.anInt1548 - Class992.renderY << 8) / var6;
                  Class145.aClass113Array1895[Class1015.anInt1672++] = var1;
               }
            }
         } else if(var1.anInt1554 == 4) {
            var2 = var1.anInt1544 - Class992.renderY;
            if(var2 > 128) {
               var3 = var1.anInt1563 - Class3_Sub13_Sub27.anInt3340 + Class3_Sub13_Sub39.anInt3466;
               if(var3 < 0) {
                  var3 = 0;
               }

               var4 = var1.anInt1566 - Class3_Sub13_Sub27.anInt3340 + Class3_Sub13_Sub39.anInt3466;
               if(var4 > Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466) {
                  var4 = Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466;
               }

               if(var3 <= var4) {
                  int var10 = var1.anInt1553 - Class97.anInt1375 + Class3_Sub13_Sub39.anInt3466;
                  if(var10 < 0) {
                     var10 = 0;
                  }

                  var6 = var1.anInt1547 - Class97.anInt1375 + Class3_Sub13_Sub39.anInt3466;
                  if(var6 > Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466) {
                     var6 = Class3_Sub13_Sub39.anInt3466 + Class3_Sub13_Sub39.anInt3466;
                  }

                  boolean var7 = false;

                  label114:
                  for(int var8 = var10; var8 <= var6; ++var8) {
                     for(int var9 = var3; var9 <= var4; ++var9) {
                        if(Class23.aBooleanArrayArray457[var8][var9]) {
                           var7 = true;
                           break label114;
                        }
                     }
                  }

                  if(var7) {
                     var1.anInt1564 = 5;
                     var1.anInt1549 = (var1.anInt1562 - Class1210.renderX << 8) / var2;
                     var1.anInt1557 = (var1.anInt1545 - Class1210.renderX << 8) / var2;
                     var1.anInt1555 = (var1.anInt1560 - Class3_Sub13_Sub30.renderZ << 8) / var2;
                     var1.anInt1551 = (var1.anInt1550 - Class3_Sub13_Sub30.renderZ << 8) / var2;
                     Class145.aClass113Array1895[Class1015.anInt1672++] = var1;
                  }
               }
            }
         }
      }

   }

   static final Class1008 method1295(int var0, byte var1, int var2) {
      try {
         int var3 = -var0 + var2;
         return 8 >= ~var3?(5 >= ~var3?(~var3 <= 2?(0 > var3?Class3_Sub13_Sub33.aClass94_3394:(var1 > -52?(Class1008)null:(var3 <= 9?(var3 > 6?Class3_Sub13_Sub1.aClass94_3040:(~var3 >= -4?(0 < var3?Class140_Sub3.aClass94_2723:Class132.yellowColor): Class986.GREENJ_STRING)):Class19.aClass94_431))): Class1017_2.peachjString):Class163_Sub3.aClass94_3006):Class3_Sub13_Sub24.aClass94_3298;
      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "jj.E(" + var0 + ',' + var1 + ',' + var2 + ')');
      }
   }

   public static void method1296(int var0) {
      try {
         aClass94_1076 = null;
         if(var0 == 1) {
            openBracket = null;
            aClass94_1070 = null;
         }
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "jj.F(" + var0 + ')');
      }
   }

   static final float[] method1297() {
	   float var1 = Class953.method1514() + Class953.getLightColor();
	   int var2 = Class953.method1510();
	   float var3 = (float)(255 & var2 >> 16) / 255.0F;
	   Class980.aFloatArray1919[3] = 1.0F;
	   float var4 = (float)(('\uff59' & var2) >> 8) / 255.0F;
	   float var6 = 0.58823526F;
	   float var5 = (float)(255 & var2) / 255.0F;
	   Class980.aFloatArray1919[2] = Class151.aFloatArray1934[2] * var5 * var6 * var1;
	   Class980.aFloatArray1919[0] = Class151.aFloatArray1934[0] * var3 * var6 * var1;
	   Class980.aFloatArray1919[1] = var1 * var6 * var4 * Class151.aFloatArray1934[1];
	   return Class980.aFloatArray1919;
   }

   //not actually getPlane
   //I think it creates buffer j string
   static final Class1008 createInt(int var1) {
	   return Class118.method1723(false, 10, var1);
   }

}

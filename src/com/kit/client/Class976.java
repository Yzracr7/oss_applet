package com.kit.client;

import java.text.NumberFormat;

final class Class976 extends Class1042 {

    protected int anInt2296;
    protected  Class1124[] aClass64Array2298;
    protected int[] anIntArray2299;
    protected int[] anIntArray2300;
    protected int[] anIntArray2301;
    protected byte[][][] aByteArrayArrayArray2302;
    protected  Class1124[] aClass64Array2303;
    protected static Class1008 aClass94_2304 = Class943.create("details");
    protected int anInt2305;
    protected static Class1008 aClass94_2306 = Class943.create("<)4col> x");

    public static void drawSideIcons() {
        Class922.attackIcon.drawSprite(529, 169);
        Class922.skillsIcon.drawSprite(527 + 33, 168);
        Class922.questIcon.drawSprite(529 + 66, 169);
        Class922.inventoryIcon.drawSprite(531 + 99, 173);
        Class922.equipmentIcon.drawSprite(527 + 132, 170);
        Class922.prayerIcon.drawSprite(527 + 165, 168);
        Class922.magicIcon.drawSprite(528 + 198, 169);
        Class922.clanChatIcon.drawSprite(525, 467);
        Class922.friendsIcon.drawSprite(527 + 33, 468);
        Class922.ignoreIcon.drawSprite(528 + 66, 468);
        Class922.logoutIcon.drawSprite(529 + 99, 470);
        Class922.settingsIcon.drawSprite(530 + 132, 468);
        Class922.emotesIcon.drawSprite(528 + 165, 469);
        Class922.musicIcon.drawSprite(529 + 198, 469);
    }

    static final void method124(int var1, int var2) {
        if (Class970.method_948(var2)) {
            Class2.method75(Class1031.interfaceCache[var2], true, var1);

        }
    }

    protected static boolean xpCounterActive() {
        return !Class7.getInterface(35913830).hidden;
    }

    static final void method_946(Class1034 var4, int var0, int var2, int var3) {
        if (Class922.clientSize > 0) {
            method_944(var0, var2, var3, var4);
            return;
        }
        method_942(var4, var0, var2, var3);
    }

    protected static int xpCounter;

    static void method_943(int var3, int var2) {
        if (Class922.xpButton != null)
            if (Class922.mouseX >= 518 && Class922.mouseX <= 544 && Class922.mouseY >= 25 && Class922.mouseY <= 50) {
                if (xpCounterActive()) {
                    Class922.xpCounterHover.drawSprite(var3 - 34, var2 + 56);
                } else {
                    Class922.xpCounterActive.drawSprite(var3 - 34, var2 + 56);
                }
            } else {
                if (xpCounterActive()) {
                    Class922.xpButton.drawSprite(var3 - 34, var2 + 56);
                } else {
                    Class922.xpCounterActive_2.drawSprite(var3 - 34, var2 + 56);
                }
            }
        if (xpCounterActive() && Class922.clientSize == 0) {
            int x = Class922.clientSize == 0 ? 507 : Class922.WIDTH - 200;
            int y = Class922.clientSize == 0 ? 45 : 50;
            digits = xpCounter == 0 ? 1 : 1 + (int) Math.floor(Math.log10(xpCounter));
            int i = xpCounter >= Integer.MAX_VALUE
                    ? Class922.getSmallClass1019().getTextWidth("Lots!") - Class922.getSmallClass1019().getTextWidth("Lots!") / 2
                    : Class922.getSmallClass1019().getTextWidth(Integer.toString(xpCounter))
                    - Class922.getSmallClass1019().getTextWidth(Integer.toString(xpCounter)) / 2;
            Class922.xp_counter.drawTransparentSprite(var3 - 156, var2 + 41, 70);
            Class922.xp_counter_2.drawSprite(var3 - 153, var2 + 44);
            Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("" + NumberFormat.getInstance().format(xpCounter))), xpCounter == 0 ? Class922.clientSize == 0 ? x + 3 - i - digits : x - Class922.WIDTH / 2 + (x / 2) - i - digits : Class922.clientSize == 0 ? x + 5 - i - digits : x - Class922.WIDTH / 2 + (x / 2) - i - digits, y - (Class922.clientSize == 0 ? 21 : 29), 16777215, 0);
        }
    }

    protected static int digits;

    static final void method_944(int var0,/* byte var1, */int var2, int var3, Class1034 var4) {
        try {
            Class58.method1194();
            var3 = var3 + 12;
            if (Class1012.aBoolean_617) {
                Class920.method_576(var3 - 50, var2 - 3, var3 + var4.scrollbarWidth, var2 + var4.scrollbarHeight + 6);
            } else {
                Class1023.clipRect(var3 - 50, var2 - 3, var3 - -var4.scrollbarWidth, var2 + var4.scrollbarHeight + 6);
            }

            if (2 != Class161.anInt2028 && 5 != Class161.anInt2028 && Class49.minimapLandscape != null) {
                int var19 = Class3_Sub13_Sub8.anInt3102 + Class1211.cameraYaw & 2047;
                int var6 = 0 + Class945.thisClass946.y / 32 + 48;
                int var7 = 0 + -(Class945.thisClass946.x / 32) + 464;
                if (!Class1012.aBoolean_617) {
                    ((Class1206_2) Class49.minimapLandscape).drawSprite(var3 + 16, var2 + 2, 151, 151, var6/* - stepsToMove*/, var7, var19, 256 + Class164_Sub2.anInt3020, /*client.mapbackSource*/Class14.dest, /*client.mapbackDest*/Class14.src);
                } else {
                    ((Class1011) Class49.minimapLandscape).method647(var3 + 16, var2 + 2, var4.scrollbarWidth - 27, var4.scrollbarHeight + 10, var6, var7, var19, Class164_Sub2.anInt3020 + 256, (Class1011) Class922.randomSprite[0]);
                }
                if (!Class1012.aBoolean_617) {
                    ((Class1206_2) Class922.compassSprite).drawSprite(var3, var2, 35, 35, 25, 25, Class1211.cameraYaw, 256 + Class164_Sub2.anInt3020, Class922.compassSource, Class922.compassDest);
                } else {
                    ((Class1011) Class922.compassSprite).method647(var3 - 26, var2 + 3, 37, 37, 25, 25, var19, Class164_Sub2.anInt3020 + 256, (Class1011) Class922.randomSprite[0]);// Class1211.cameraYaw, Class164_Sub2.anInt3020 + 256, (Class1011)Class57.compassSprite);
                }
                var3 = var3 - 14;
                var2 = var2 + 2;
                int var9;
                int var10;
                int var11;
                int var12;
                int var13;
                int var14;
                int var17;
                int var16;
                for (var9 = 0; Class980.anInt1924 > var9; ++var9) {
                    var10 = -(Class945.thisClass946.y / 32) + 2 + 4 * Class84.anIntArray1163[var9] + 0;
                    var11 = -(Class945.thisClass946.x / 32) + 2 + (Class1225.anIntArray4050[var9] * 4 - 0);
                    ObjectDefinition var20 = ObjectDefinition.getDefinition(Class951.anIntArray3693[var9]);
                    int minimapSprite = var20.minimapSprite;
                    if (minimapSprite == 13) {
                        minimapSprite = 12;
                    }
                    if (minimapSprite >= 15 && minimapSprite <= 67) {
                        minimapSprite -= 2;
                    } else if (minimapSprite >= 68 && minimapSprite <= 84) {
                        minimapSprite -= 1;
                    }
                    if (minimapSprite > 86)
                        continue;
                    if (null != var20.anIntArray1524) {
                        var20 = var20.method1685();
                        if (null == var20 || 0 == ~var20.minimapSprite) {
                            continue;
                        }
                    }
                    if (Class1012.aBoolean_617) {
                        Class920.method_576(var3 + 29, var2 + 2, var3 + var4.scrollbarWidth - 5, var2 + var4.scrollbarHeight - 15); //x, y, width, height
                    } else {
                        Class1023.clipRect(var3 + 29, var2 + 2, var3 + var4.scrollbarWidth - 5, var2 + var4.scrollbarHeight - 15);
                    }
                    Class38_Sub1.markMinimap(var4, Class945.aClass3_Sub28_Sub16_Sub2Array2140[minimapSprite], var11, var10, var2, var3);
                }
                if (Class1012.aBoolean_617) {
                    Class920.method_576(var3 - 50, var2 - 3, var3 + var4.scrollbarWidth, var2 + var4.scrollbarHeight + 6);
                } else {
                    Class1023.clipRect(var3 - 50, var2 - 3, var3 - -var4.scrollbarWidth, var2 + var4.scrollbarHeight + 6);
                }
                for (var9 = 0; 104 > var9; ++var9) {
                    for (var10 = 0; -105 < ~var10; ++var10) {
                        Class975 var25 = Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var9][var10];
                        if (null != var25) {
                            var12 = 2 + var9 * 4 + -(Class945.thisClass946.y / 32);
                            var13 = -(Class945.thisClass946.x / 32) + 2 + 4 * var10;
                            Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[0], var13, var12, var2, var3);
                        }
                    }
                }
                for (var9 = 0; ~Class163.anInt2046 < ~var9; ++var9) {
                    Class1001 var21 = Class3_Sub13_Sub24.class1001List[Class15.anIntArray347[var9]];
                    if (var21 != null && var21.method1966((byte) 17)) {
                        Class981 var22 = var21.aClass90_3976;
                        if (null != var22 && null != var22.anIntArray1292) {
                            var22 = var22.method1471((byte) -3);
                        }
                        if (var22 != null && var22.aBoolean1285 && var22.aBoolean1270) {
                            var12 = var21.y / 32 - Class945.thisClass946.y / 32;
                            var13 = var21.x / 32 + -(Class945.thisClass946.x / 32);
                            if (~var22.anInt1283 != 0) {
                                Class38_Sub1.markMinimap(var4, Class945.aClass3_Sub28_Sub16_Sub2Array2140[var22.anInt1283], var13, var12, var2, var3);
                            } else {
                                Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[1], var13, var12, var2, var3);
                            }
                        }
                    }
                }
                for (var9 = 0; var9 < Class159.anInt2022; ++var9) {
                    Class946 var23 = Class922.class946List[Class922.playerIndices[var9]];
                    if (null != var23 && var23.method1966((byte) 17)) {
                        var12 = var23.x / 32 - Class945.thisClass946.x / 32;
                        var11 = -(Class945.thisClass946.y / 32) + var23.y / 32;
                        long var29 = var23.username.toLong();
                        boolean var28 = false;
                        for (var16 = 0; ~Class8.localPlayerIds < ~var16; ++var16) {
                            if (~var29 == ~Class1209.friendsList[var16] && 0 != Class973.anIntArray882[var16]) {
                                var28 = true;
                                break;
                            }
                        }
                        boolean var31 = false;
                        String stringName = var23.username.toString().trim();
                        for (var17 = 0; var17 < Class922.CLAN_MEMBERS_SIZE - 1; var17++) {
                            if (Class1209.clanMembersList[var17] != null && stringName.equalsIgnoreCase(Class1209.clanMembersList[var17])) {
                                var31 = true;
                                break;
                            }
                        }
                        boolean var32 = false;
                        if (-1 != ~Class945.thisClass946.team && 0 != var23.team && var23.team == Class945.thisClass946.team) {
                            var32 = true;
                        }
                        if (Class80.mapIconsArray != null) {
                            if (var28) {
                                Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[3], var12, var11, var2, var3);
                            } else if (!var31) {
                                if (var32) {
                                    Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[4], var12, var11, var2, var3);
                                } else {
                                    Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[2], var12, var11, var2, var3);
                                }
                            } else {
                                Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[5], var12, var11, var2, var3);
                            }
                        }
                    }
                }
                Class1025[] var24 = Class1244.hintsList;
                for (var10 = 0; ~var10 > ~var24.length; ++var10) {
                    Class1025 var26 = var24[var10];
                    if (null != var26 && -1 != ~var26.type && Class1134.loopCycle % 20 < 10) {
                        if (~var26.type == -2 && var26.index >= 0 && var26.index < Class3_Sub13_Sub24.class1001List.length) {
                            Class1001 var27 = Class3_Sub13_Sub24.class1001List[var26.index];
                            if (null != var27) {
                                var13 = -(Class945.thisClass946.y / 32) + var27.y / 32;
                                var14 = var27.x / 32 + -(Class945.thisClass946.x / 32);
                                markMinimap(var26.iconIndex, var2, var3, var13, var14, var4, false);
                            }
                        }
                        if (-3 == ~var26.type) {
                            var12 = (-Class131.anInt1716 + var26.anInt1356) * 4 + 2 - Class945.thisClass946.y / 32;
                            var13 = -(Class945.thisClass946.x / 32) + 2 + (-SpriteDefinition.anInt1152 + var26.anInt1347) * 4;
                            markMinimap(var26.iconIndex, var2, var3, var12, var13, var4, false);
                        }
                        if (var26.type == 10 && -1 >= ~var26.index && Class922.class946List.length > var26.index) {
                            Class946 var30 = Class922.class946List[var26.index];
                            if (null != var30) {
                                var14 = var30.x / 32 + -(Class945.thisClass946.x / 32);
                                var13 = var30.y / 32 + -(Class945.thisClass946.y / 32);
                                markMinimap(var26.iconIndex, var2, var3, var13, var14, var4, false);
                            }
                        }
                    }
                }
                if (Class65.mapFlagX != 0) {
                    var9 = 4 * Class65.mapFlagX + (2 - Class945.thisClass946.y / 32);
                    var10 = 2 + 4 * Class45.mapFlagY - Class945.thisClass946.x / 32;
                    if (Class45.mapmarkerSprites != null)
                        Class38_Sub1.markMinimap(var4, Class45.mapmarkerSprites[0], var10, var9, var2, var3);
                }
                if (!Class1012.aBoolean_617) {
                    Class1023.fillRect(var3 + 106, var2 + 75, 3, 3, 16777215);
                } else {
                    Class920.method_574(var3 + 97, var2 + 82 - 4, 3, 3, 16777215);
                }
            } else if (!Class1012.aBoolean_617) {
                Class1023.method1332(var3 + 25, var2 + 5, 0, Class922.mapbackSource, Class922.mapbackDest);
            }


            Class163_Sub1_Sub1.aBooleanArray4008[var0] = true;
            method_944(var3 + 12, var2 - 3);
            method_943(var3 + 12, var2 - 38);
        } catch (RuntimeException var18) {
        }
    }

    static final void method_942(Class1034 var4, int var0, int var2, int var3) {
        Class58.method1194();
        if (Class1012.aBoolean_617) {
            Class920.method_576(var3 - 7, var2, var3 + var4.scrollbarWidth, var2 + var4.scrollbarHeight); // x, y, width,
        } else {
            Class1023.clipRect(var3, var2, var3 + var4.scrollbarWidth, var2 + var4.scrollbarHeight);
        }
        if (2 != Class161.anInt2028 && 5 != Class161.anInt2028 && Class49.minimapLandscape != null) {
            int var19 = Class3_Sub13_Sub8.anInt3102 + Class1211.cameraYaw & 2047;
            int player_y = 0 + Class945.thisClass946.y / 32 + 48;
            int player_x = 0 + -(Class945.thisClass946.x / 32) + 464;
            if (!Class1012.aBoolean_617) {
                ((Class1206_2) Class49.minimapLandscape).drawSprite(var3 + 25, var2 + 5, 135, 152, player_y, player_x, var19,
                        256 + Class164_Sub2.anInt3020, Class922.mapbackSource, Class922.mapbackDest);
            } else {
                ((Class1011) Class49.minimapLandscape).drawHDSprite(var3 + 18, var2 + 5, 155, 152, player_y, player_x, var19,
                        Class164_Sub2.anInt3020 + 256);
            }
            if (!Class1012.aBoolean_617) {
                ((Class1206_2) Class922.compassSprite).drawSprite(550, 4, 20, 33, 25, 25, Class1211.cameraYaw,
                        256 + Class164_Sub2.anInt3020, Class922.compassSource, Class922.compassDest);
            } else {
                ((Class1011) Class922.compassSprite).method647(var3 - 7, var2, 35, 35, 25, 25, var19,
                        /*sprite size*/Class164_Sub2.anInt3020 + 256, (Class1011) Class922.randomSprite[0]);// Class1211.cameraYaw,
            }

            int var9;
            int var10;
            int y;
            int x;
            int var13;
            int var14;
            int var17;
            int var16;
            for (var9 = 0; Class980.anInt1924 > var9; ++var9) {
                var10 = -(Class945.thisClass946.y / 32) + 2 + 4 * Class84.anIntArray1163[var9] + 0;
                y = -(Class945.thisClass946.x / 32) + 2 + (Class1225.anIntArray4050[var9] * 4 - 0);
                ObjectDefinition var20 = ObjectDefinition.getDefinition(Class951.anIntArray3693[var9]);
                int minimapSprite = var20.minimapSprite;
                if (minimapSprite == 13) {
                    minimapSprite = 12;
                }
                if (minimapSprite >= 15 && minimapSprite <= 67) {
                    minimapSprite -= 2;
                } else if (minimapSprite >= 68 && minimapSprite <= 84) {
                    minimapSprite -= 1;
                }
                //System.out.println("map sprite: " + var20.minimapSprite + ", max: " +
                        //Class945.aClass3_Sub28_Sub16_Sub2Array2140.length);
                if (minimapSprite > 86)
                    continue;
                if (null != var20.anIntArray1524) {
                    var20 = var20.method1685();
                    if (null == var20 || 0 == ~var20.minimapSprite) {
                        continue;
                    }
                }
                Class38_Sub1.markMinimap(var4, Class945.aClass3_Sub28_Sub16_Sub2Array2140[minimapSprite], y, var10,
                        var2, var3);
            }
            for (var9 = 0; 104 > var9; ++var9) {
                for (var10 = 0; -105 < ~var10; ++var10) {
                    Class975 var25 = Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var9][var10];
                    if (null != var25) {
                        x = 2 + var9 * 4 + -(Class945.thisClass946.y / 32);
                        var13 = -(Class945.thisClass946.x / 32) + 2 + 4 * var10;
                        if (Class80.mapIconsArray != null)
                            Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[0], var13, x, var2, var3);
                    }
                }
            }
            for (var9 = 0; ~Class163.anInt2046 < ~var9; ++var9) {
                Class1001 var21 = Class3_Sub13_Sub24.class1001List[Class15.anIntArray347[var9]];
                if (var21 != null && var21.method1966((byte) 17)) {
                    Class981 var22 = var21.aClass90_3976;
                    if (null != var22 && null != var22.anIntArray1292) {
                        var22 = var22.method1471((byte) -3);
                    }

                    if (var22 != null && var22.aBoolean1285 && var22.aBoolean1270) {
                        x = var21.y / 32 - Class945.thisClass946.y / 32;
                        var13 = var21.x / 32 + -(Class945.thisClass946.x / 32);
                        if (~var22.anInt1283 != 0) {
                            Class38_Sub1.markMinimap(var4,
                                    Class945.aClass3_Sub28_Sub16_Sub2Array2140[var22.anInt1283], var13, x, var2,
                                    var3);
                        } else {
                            if (Class80.mapIconsArray != null)
                                Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[1], var13, x, var2 + 1, var3);
                        }
                    }
                }
            }
            for (var9 = 0; var9 < Class159.anInt2022; ++var9) {
                Class946 class946 = Class922.class946List[Class922.playerIndices[var9]];
                if (null != class946 && class946.method1966((byte) 17)) {
                    x = class946.x / 32 - Class945.thisClass946.x / 32;
                    y = -(Class945.thisClass946.y / 32) + class946.y / 32;
                    long otherName = class946.username.toLong();
                    String stringName = class946.username.toString().trim();
                    boolean isFriend = false;

                    for (var16 = 0; ~Class8.localPlayerIds < ~var16; ++var16) {
                        if (~otherName == ~Class1209.friendsList[var16] && 0 != Class973.anIntArray882[var16]) {
                            isFriend = true;
                            break;
                        }
                    }
                    boolean isClan = false;
                    for (var17 = 0; var17 < Class922.CLAN_MEMBERS_SIZE - 1; var17++) {
                        if (Class1209.clanMembersList[var17] != null
                                && stringName.equalsIgnoreCase(Class1209.clanMembersList[var17])) {
                            isClan = true;
                            break;
                        }
                    }
                    boolean isTeamCape = false;
                    if (-1 != ~Class945.thisClass946.team && 0 != class946.team
                            && class946.team == Class945.thisClass946.team) {
                        isTeamCape = true;
                    }
                    if (Class80.mapIconsArray != null) {
                        if (isFriend) {
                            Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[3], x, y, var2, var3);
                        } else if (!isClan) {
                            if (isTeamCape) {
                                Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[4], x, y, var2, var3);
                            } else {
                                Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[2], x, y, var2, var3);
                            }
                        } else {
                            Class38_Sub1.markMinimap(var4, Class80.mapIconsArray[5], x, y, var2, var3);
                        }
                    }
                }
            }
            Class1025[] var24 = Class1244.hintsList;
            for (var10 = 0; ~var10 > ~var24.length; ++var10) {
                Class1025 var26 = var24[var10];
                if (null != var26 && -1 != ~var26.type && Class1134.loopCycle % 20 < 10) {
                    if (~var26.type == -2 && var26.index >= 0 && var26.index < Class3_Sub13_Sub24.class1001List.length) {
                        Class1001 var27 = Class3_Sub13_Sub24.class1001List[var26.index];
                        if (null != var27) {
                            var13 = -(Class945.thisClass946.y / 32) + var27.y / 32;
                            var14 = var27.x / 32 + -(Class945.thisClass946.x / 32);
                            markMinimap(var26.iconIndex, var2, var3, var13, var14, var4, false);
                        }
                    }
                    if (-3 == ~var26.type) {
                        x = (-Class131.anInt1716 + var26.anInt1356) * 4 + 2 - Class945.thisClass946.y / 32;
                        var13 = -(Class945.thisClass946.x / 32) + 2 + (-SpriteDefinition.anInt1152 + var26.anInt1347) * 4;
                        markMinimap(var26.iconIndex, var2, var3, x, var13, var4, false);
                    }
                    if (var26.type == 10 && -1 >= ~var26.index && Class922.class946List.length > var26.index) {
                        Class946 var30 = Class922.class946List[var26.index];
                        if (null != var30) {
                            var14 = var30.x / 32 + -(Class945.thisClass946.x / 32);
                            var13 = var30.y / 32 + -(Class945.thisClass946.y / 32);
                            markMinimap(var26.iconIndex, var2, var3, var13, var14, var4, false);
                        }
                    }
                }
            }
            if (Class65.mapFlagX != 0) {
                var9 = 4 * Class65.mapFlagX + (2 - Class945.thisClass946.y / 32);
                var10 = 2 + 4 * Class45.mapFlagY - Class945.thisClass946.x / 32;
                if (Class45.mapmarkerSprites != null)
                    Class38_Sub1.markMinimap(var4, Class45.mapmarkerSprites[0], var10, var9, var2, var3);
            }
            if (!Class1012.aBoolean_617) {
                Class1023.fillRect(var3 + 91, var2 + 79, 3, 3, 16777215);
            } else {
                Class920.method_574(var3 + 92, var2 + 82, 3, 3, 16777215);
            }
        } else if (!Class1012.aBoolean_617) {
            Class1023.method1332(var3 + 25, var2 + 5, 0, Class922.mapbackSource, Class922.mapbackDest);
        }
        Class922.mapbackSprte.drawSprite(var3, var2);
        Class163_Sub1_Sub1.aBooleanArray4008[var0] = true;
    }

    private static int orbOpacity = 255;
    private static boolean reverseOrbOpacty = false;

    static void method_944(int var3, int var2) {
        int xMinus = 3;
        int xMinus2 = 2;
        int yPlus = -4;
        int yPlus2 = 4;
        int xPlus = Class922.clientSize == 0 ? -8 : 1;
        int yPlus3 = 3;
        if (!Class929.orbsToggled)
            return;
        boolean runToggled = Class163_Sub1.variousSettings[173] == 1 ? true : false;
        boolean isPoisoned = Class163_Sub1.variousSettings[428] == 1 ? true : false;
        int runEnergy = Class9.energy;
        int prayLvl = Class3_Sub13_Sub15.currentStats[5];
        int prayMax = Class3_Sub20.maxStats[5];
        int prayRatio = (int) (((double) prayLvl / (double) prayMax) * 100D);
        int hpLvl = Class3_Sub13_Sub15.currentStats[3];
        int hpMax = Class3_Sub20.maxStats[3];
        int hpRatio = (int) (((double) hpLvl / (double) hpMax) * 100D);
        int runRatio = (int) (((double) runEnergy / (double) 100) * 100D);
        boolean orbsOn = Class929.orbsToggled;

        if (Class929.aInteger_510 == 474)
            var3 = var3 + 1;

        if (Class929.newHits) {
            prayLvl = prayLvl * 10;
            hpLvl = hpLvl * 10;
        }

        Class922.orbBg.height = orbsOn ? 36 : 0;

        int x = Class922.mouseX;
        int y = Class922.mouseY;

        Class922.orbBg.drawSprite(var3 - 33 - xMinus, var2 + 36 - yPlus);
        if ((Class922.clientSize == 0 && x > 522 && x < 579 && y >= 84 && y <= 119)
                || (Class922.clientSize > 0 && x > Class922.resizeWidth - 223 && x <= Class922.resizeWidth - 223 + 55
                && y >= 93 && y <= 126)) {
            Class922.orbBgOSHover.drawSprite(var3 - 33 - xMinus2, var2 + 80 + yPlus2);
        } else {
            Class922.orbBg.drawSprite(var3 - 33 - xMinus2, var2 + 80 + yPlus2);
        }
        if ((Class922.clientSize == 0 && x > 550 && x < 605 && y >= 122 && y <= 154)
                || (Class922.clientSize > 0 && x > Class922.resizeWidth - 207 && x <= Class922.resizeWidth - 207 + 55
                && y >= 130 && y <= 163)) {
            Class922.orbBgOSHover.drawSprite(var3 - 3 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 117 + yPlus3);
        } else {
            Class922.orbBg.drawSprite(var3 - 3 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 117 + yPlus3);
        }


        if (hpRatio <= 25 || prayRatio <= 25 || runRatio <= 25) {
            orbOpacity += reverseOrbOpacty ? 5 : -5;

            if (orbOpacity <= 5)
                reverseOrbOpacty = true;
            else if (orbOpacity >= 250)
                reverseOrbOpacty = false;
        }

        Class922.hpIcon.height = orbsOn ? 16 : 0;
        Class922.prayIcon.height = orbsOn ? 22 : 0;

        if (isPoisoned) {
            Class922.poisonFill.height = orbsOn ? 26 : 0;
            Class922.poisonFill.drawSprite(var3 - 33 + 27 - xMinus, var2 + 45);
        } else {
            Class922.hpFill.height = orbsOn ? 26 : 0;
            Class922.hpFill.drawSprite(var3 - 33 + 28 - xMinus, var2 + 41 - yPlus);
        }

        Class922.prayFill.height = orbsOn ? 26 : 0;
        Class922.prayFill.drawSprite(var3 - 33 + 28 - xMinus2, var2 + 85 + yPlus2);

        Class922.orbDrain.height = Class946.resting ? 26 : 0;
        if (runToggled || Class946.resting) {
            Class922.orbDrain.drawSprite(var3 - 6 + 30 - (Class922.clientSize > 0 ? /*RESIZE*/(Class1012.aBoolean_617 ? 20 : -18) : /*FIXED*/(Class1012.aBoolean_617 ? 8 : -28)), var2 + 121 - (Class1012.aBoolean_617 ? -2 : 0));
            Class922.runFillOn.height = orbsOn ? 26 : 0;
            if (Class946.resting)
                Class922.runFillOn.drawTransparentSprite(var3 - 6 + 31 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 122 + yPlus3, orbOpacity);
            else
                /*YELLOW*/
                Class922.runFillOn.drawSprite(var3 - 6 + 31 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 122 + yPlus3);
        } else {
            Class922.runFill.height = orbsOn ? 26 : 0;
            Class922.runFill.drawSprite(var3 - 6 + 31 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 122 + yPlus3);
        }

        Class922.orb_drain_hd[0].height = getOrbFill(hpRatio);
        Class922.orb_drain_hd[0].drawSprite(var3 - 33 + 27 - xMinus + (Class922.clientSize > 0 ? (Class1012.aBoolean_617 ? 0 : 28) : (Class1012.aBoolean_617 ? 0 : 28)), var2 + 40 - yPlus);
        Class922.getSmallClass1019().drawText(Class943.create(orbsOn ? (getOrbColor(hpRatio) + hpLvl) : ""), var3 - 17 - xMinus, var2 + 63 - yPlus, 16777215, 0);

        Class922.orb_drain_hd[0].height = getOrbFill(prayRatio);
        Class922.orb_drain_hd[0].drawSprite(var3 - 33 + 27 - xMinus2 + (Class922.clientSize > 0 ? (Class1012.aBoolean_617 ? 0 : 28) : (Class1012.aBoolean_617 ? 0 : 28)), var2 + 84 + yPlus2);
        Class922.getSmallClass1019().drawText(Class943.create(orbsOn ? (getOrbColor(prayRatio) + prayLvl) : ""), var3 - 17 - xMinus2, var2 + 107 + yPlus2, 16777215, 0);

        Class922.orb_drain_hd[0].height = getOrbFill(runRatio);
        Class922.orb_drain_hd[0].drawSprite(var3 - 11 + 27
                        - (Class922.clientSize > 0 ? (Class1012.aBoolean_617 ? 10 : -18) : (Class1012.aBoolean_617 ? 0 : -28)),
                var2 + 120 + yPlus3 + 1);
        Class922.getSmallClass1019().drawText(Class943.create(orbsOn ? (getOrbColor(runRatio) + runEnergy) : ""),
                var3 + 13 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 144 + yPlus3, 16777215, 0);

		/* client.hpIcon.height = orbsOn ? 16 : 0;
       client.hpIcon.method1667(var3, var2 + 47);
       client.prayIcon.height = orbsOn ? 22 : 0;
       client.prayIcon.method1667(var3, var2 + 87);*/
        if (hpRatio > 25)
            Class922.hpIcon.drawSprite(var3 - xMinus, var2 + 46 - yPlus);
        else
            Class922.hpIcon.drawTransparentSprite(var3 - xMinus, var2 + 46 - yPlus, orbOpacity);
        if (prayRatio > 25)
            Class922.prayIcon.drawSprite(var3 - xMinus2 - 3, var2 + 87 + yPlus2);
        else
            Class922.prayIcon.drawTransparentSprite(var3 - xMinus2 - 3, var2 + 87 + yPlus2, orbOpacity);

        if (runToggled) {
            Class922.runOnIcon.height = orbsOn ? 20 : 0;
            if (runRatio > 25) {
                Class922.runOnIcon.drawSprite(var3 + 30 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 125 + yPlus3);
            } else {
                Class922.runOnIcon.drawTransparentSprite(var3 + 30 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 125 + yPlus3, orbOpacity);
            }
        } else {
            Class922.runIcon.height = orbsOn ? 20 : 0;
            if (runRatio > 25)
                Class922.runIcon.drawSprite(var3 + 30 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 125 + yPlus3);
            else
                Class922.runIcon.drawTransparentSprite(var3 + 30 + xPlus - (Class922.clientSize > 0 ? 10 : 0), var2 + 125 + yPlus3, orbOpacity);
        }
    }


    private static int getOrbFill(int statusInt) {
        if (statusInt <= 100 && statusInt >= 97) {
            return 0;
        } else if (statusInt <= 96 && statusInt >= 93) {
            return 3;
        } else if (statusInt <= 92 && statusInt >= 89) {
            return 4;
        } else if (statusInt <= 88 && statusInt >= 85) {
            return 5;
        } else if (statusInt <= 84 && statusInt >= 81) {
            return 6;
        } else if (statusInt <= 80 && statusInt >= 77) {
            return 7;
        } else if (statusInt <= 76 && statusInt >= 73) {
            return 8;
        } else if (statusInt <= 72 && statusInt >= 69) {
            return 9;
        } else if (statusInt <= 68 && statusInt >= 65) {
            return 10;
        } else if (statusInt <= 64 && statusInt >= 61) {
            return 11;
        } else if (statusInt <= 60 && statusInt >= 57) {
            return 12;
        } else if (statusInt <= 56 && statusInt >= 53) {
            return 13;
        } else if (statusInt <= 52 && statusInt >= 49) {
            return 14;
        } else if (statusInt <= 48 && statusInt >= 45) {
            return 15;
        } else if (statusInt <= 44 && statusInt >= 41) {
            return 16;
        } else if (statusInt <= 40 && statusInt >= 37) {
            return 17;
        } else if (statusInt <= 36 && statusInt >= 33) {
            return 18;
        } else if (statusInt <= 32 && statusInt >= 29) {
            return 19;
        } else if (statusInt <= 28 && statusInt >= 25) {
            return 20;
        } else if (statusInt <= 24 && statusInt >= 21) {
            return 21;
        } else if (statusInt <= 20 && statusInt >= 17) {
            return 22;
        } else if (statusInt <= 16 && statusInt >= 13) {
            return 23;
        } else if (statusInt <= 12 && statusInt >= 9) {
            return 24;
        } else if (statusInt <= 8 && statusInt >= 7) {
            return 25;
        } else if (statusInt <= 6 && statusInt >= 5) {
            return 26;
        } else if (statusInt <= 4 && statusInt >= 3) {
            return 27;
        } else if (statusInt <= 2 && statusInt >= 1) {
            return 27;
        } else if (statusInt <= 0) {
            return 28;
        }
        return 0;
    }

    private static String getOrbColor(int energy) {
        if (energy > 100) {
            return "<col=00ff00>";
        }
        if (energy <= 100 && energy >= 75) {
            return "<col=00ff00>";
        } else if (energy <= 74 && energy >= 50) {
            return "<col=ffff00>";
        } else if (energy <= 49 && energy >= 25) {
            return "<col=fca607>";
        } else if (energy <= 24 && energy >= 0) {
            return "<col=f50d0d>";
        }
        return "<col=00ff00>";
    }

    static final void method126(boolean var0, int var1, int var2, int var3, int var4) {
        try {
            if (!var0) {
                if (~var4 <= ~Class101.anInt1425 && var4 <= Class3_Sub28_Sub18.anInt3765) {
                    var2 = Class40.method1040(Class57.anInt902, var2, (byte) 0, Class159.anInt2020);
                    var1 = Class40.method1040(Class57.anInt902, var1, (byte) 0, Class159.anInt2020);
                    Class3_Sub13_Sub16.method244(2, var2, var4, var1, var3);
                }

            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "ed.F(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
        }
    }

    static final void method127(short[] var0, int var1, Class1008[] var2, int var3, int var4) {
        try {
            if (~var1 < ~var4) {
                int var6 = var4;
                int var5 = (var4 - -var1) / 2;
                Class1008 var7 = var2[var5];
                var2[var5] = var2[var1];
                var2[var1] = var7;
                short var8 = var0[var5];
                var0[var5] = var0[var1];
                var0[var1] = var8;

                for (int var9 = var4; ~var1 < ~var9; ++var9) {
                    if (var7 == null || null != var2[var9] && var2[var9].contains(var7) < (var9 & 1)) {
                        Class1008 var10 = var2[var9];
                        var2[var9] = var2[var6];
                        var2[var6] = var10;
                        short var11 = var0[var9];
                        var0[var9] = var0[var6];
                        var0[var6++] = var11;
                    }
                }

                var2[var1] = var2[var6];
                var2[var6] = var7;
                var0[var1] = var0[var6];
                var0[var6] = var8;
                method127(var0, -1 + var6, var2, -909, var4);
                method127(var0, var1, var2, -909, var6 - -1);
            }

        } catch (RuntimeException var12) {
            throw Class1134.method1067(var12, "ed.E(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ',' + var4 + ')');
        }
    }

    static final void method128(int var0) {
        try {
            Class1134.aClass93_725.clearAll();
            if (var0 != 2) {
                method127((short[]) null, -27, (Class1008[]) null, -4, -64);
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ed.O(" + var0 + ')');
        }
    }

    static final Class1228 method130(int var0, int var1) {
        try {
            if (Class1046.aBoolean579 && ~var1 <= ~Class3_Sub13_Sub4.anInt3054 && ~var1 >= ~Class10117.anInt1416) {
                int var2 = 120 / ((0 - var0) / 32);
                return Class117.aClass44_Sub1Array1609[var1 - Class3_Sub13_Sub4.anInt3054];
            } else {
                return null;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ed.P(" + var0 + ',' + var1 + ')');
        }
    }

    static final void markMinimap(int var0, int var1, int var2, int var3, int var4, Class1034 var5, boolean var6) {
        try {
            int var7 = var3 * var3 + var4 * var4;
            if (-360001 <= ~var7) {
                int var8 = Math.min(var5.scrollbarWidth / 2, var5.scrollbarHeight / 2);
                if (var6) {
                    Class53.anInt865 = -79;
                }

                if (var8 * var8 >= var7) {
                    //	System.out.println("here");
                    //System.out.println("var5: "+ var5 + ", ")
                    //Class38_Sub1.markMinimap(var5, Class1210.hintMapmarkerSprites[var0], var4, var3, var1, var2);
                    Class38_Sub1.markMinimap(var5, Class45.mapmarkerSprites[1], var4, var3, var1, var2);
                } else {
                    //System.out.println("here2");
                    var8 -= 10;
                    int var9 = 2047 & Class3_Sub13_Sub8.anInt3102 + Class1211.cameraYaw;
                    int var11 = Rasterizer.cosineTable[var9];
                    int var10 = Rasterizer.sineTable[var9];
                    var10 = var10 * 256 / (256 + Class164_Sub2.anInt3020);
                    var11 = var11 * 256 / (Class164_Sub2.anInt3020 + 256);
                    int var12 = var4 * var10 - -(var11 * var3) >> 16;
                    int var13 = -(var10 * var3) + var4 * var11 >> 16;
                    double var14 = Math.atan2((double) var12, (double) var13);
                    int var16 = (int) (Math.sin(var14) * (double) var8);
                    int var17 = (int) (Math.cos(var14) * (double) var8);
                /*if(Class1012.aBoolean_617) {
                   ((Class1011)Class3_Sub13_Sub39.aClass3_Sub28_Sub16Array3458[var0]).method648(240, 240, (var5.anInt168 / 2 + var2 + var16) * 16, 16 * (-var17 + var5.anInt193 / 2 + var1), (int)(10430.378D * var14), 4096);
                } else {
                   ((Class1206_2)Class3_Sub13_Sub39.aClass3_Sub28_Sub16Array3458[var0]).method660(-10 + var16 + var5.anInt168 / 2 + var2, -10 + var5.anInt193 / 2 + var1 + -var17, 20, 20, 15, 15, var14, 256);
                }*/
                    if (Class1012.aBoolean_617) {
                        ((Class1011) Class45.mapmarkerSprites[1]).method648(240, 240, (var5.scrollbarWidth / 2 + var2 + var16) * 16, 16 * (-var17 + var5.scrollbarHeight / 2 + var1), (int) (10430.378D * var14), 4096);
                    } else {
                        ((Class1206_2) Class45.mapmarkerSprites[1]).method660(-10 + var16 + var5.scrollbarWidth / 2 + var2, -10 + var5.scrollbarHeight / 2 + var1 + -var17, 20, 20, 15, 15, var14, 256);
                    }
                }
            }
        } catch (RuntimeException var18) {
            throw Class1134.method1067(var18, "hi.D(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + (var5 != null ? "{...}" : "null") + ',' + var6 + ')');
        }
    }
}

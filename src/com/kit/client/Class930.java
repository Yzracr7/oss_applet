package com.kit.client;

final class Class930 {

    protected int fogColorRgb;
    private static Class1008 aClass94_1176 = Class943.create("Loading wordpack )2 ");
    protected int screenColorRgb;
    protected int lightX;
    protected int lightZ;
    protected static Class972[] class972 = new Class972[4];
    protected static Class1008 aClass94_1183 = aClass94_1176;
    protected int fogDepth;
    protected int lightY;
    protected static Class988 referenceCache;
    protected float lightModelAmbient;
    private static Class1008 aClass94_1188 = Class943.create("::rebuild");
    protected float light1Diffuse;
    protected float light0Diffuse;
    protected static int anInt1191;
    private static Class1008 aClass94_1192 = Class943.create("Examine");
    protected static Class3_Sub24_Sub4 aClass3_Sub24_Sub4_1193;
    protected static Class1017_2 aClass130_1194;
    protected static int anInt1195;
    protected static Class1008 examineClass1008 = aClass94_1192;

    static final void handleMusic(boolean var0, int id) {
        try {
            if (-1 == id && !Class83.aBoolean1158) {
                Class1031.method1870(false);
            } else if (id != -1 && (Class963.currentSound != id || !Class1220.method1391(-1)) && Class9.musicVolume != 0 && !Class83.aBoolean1158) {
                Class151.playMusic(true, id, 0, Class75_Sub2.cacheIndex6, false, Class9.musicVolume, 2);
                System.out.println("handleMusic::Class9.musicVolume: " + Class9.musicVolume + ", id: " + id + ", var0: " + var0);
            }

            if (!var0) {
                aClass94_1192 = (Class1008) null;
            }

            Class963.currentSound = id;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "li.B(" + var0 + ',' + id + ')');
        }
    }

    static final void method1428(int var0, int var2) {
        Class163_Sub1.variousSettings[var0] = var2;
        Class419 var3 = (Class419) Class951.aClass130_3679.get((long) var0);
        if (var3 == null) {
            var3 = new Class419(Class1219.currentTimeMillis() - -500L);
            Class951.aClass130_3679.put(var3, (long) var0);
        } else {
            var3.value = 500L + Class1219.currentTimeMillis();
        }
    }

    public static void method1429(byte var0) {
        try {
            aClass3_Sub24_Sub4_1193 = null;
            aClass94_1188 = null;
            class972 = null;
            aClass94_1192 = null;
            aClass94_1183 = null;
            aClass94_1176 = null;
            referenceCache = null;
            aClass130_1194 = null;
            examineClass1008 = null;
            if (var0 != 53) {
                aClass3_Sub24_Sub4_1193 = (Class3_Sub24_Sub4) null;
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "li.C(" + var0 + ')');
        }
    }

    static final Class1019_3 method1430(int var0, int var1) {
        try {
            if (var0 != -28922) {
                return (Class1019_3) null;
            } else {
                Class1019_3 var2 = (Class1019_3) Class80.aClass93_1135.get((long) var1);
                if (var2 != null) {
                    return var2;
                } else {
                    byte[] var3 = Class1027.cacheIndex13.getFile(var1, 0);
                    var2 = new Class1019_3(var3);
                    var2.method697(Class120_Sub30_Sub1.aClass109Array3270, (int[]) null);
                    Class80.aClass93_1135.put(var2, (long) var1);
                    return var2;
                }
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "li.A(" + var0 + ',' + var1 + ')');
        }
    }

    public Class930() {
        try {
            this.screenColorRgb = Class953.defaultLightColorRgb;
            this.light1Diffuse = 1.2F;
            this.lightX = -50;
            this.lightModelAmbient = 1.1523438F;
            this.fogColorRgb = Class953.fogColor;
            this.lightZ = -60;
            this.light0Diffuse = 0.69921875F;
            this.fogDepth = 0;
            this.lightY = -50;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "li.<init>()");
        }
    }

    Class930(ByteBuffer class966) {
        int flag = class966.readUnsignedByte();
        if ((flag & 1) == 0) {
            this.screenColorRgb = Class953.defaultLightColorRgb;
        } else {
            this.screenColorRgb = class966.getInt();
        }
        if ((2 & flag) == 0) {
            this.lightModelAmbient = 1.1523438F;
        } else {
            this.lightModelAmbient = (float) class966.aInteger233() / 256.0F;
        }
        if ((flag & 4) == 0) {
            this.light0Diffuse = 0.69921875F;
        } else {
            this.light0Diffuse = (float) class966.aInteger233() / 256.0F;
        }
        if ((flag & 8) == 0) {
            this.light1Diffuse = 1.2F;
        } else {
            this.light1Diffuse = (float) class966.aInteger233() / 256.0F;
        }
        if ((16 & flag) == 0) {
            this.lightX = -50;
            this.lightY = -50;
            this.lightZ = -60;
        } else {
            this.lightY = class966.aLong_1884();
            this.lightZ = class966.aLong_1884();
            this.lightX = class966.aLong_1884();
        }
        if ((32 & flag) == 0) {
            this.fogColorRgb = Class953.fogColor;
        } else {
            this.fogColorRgb = class966.getInt();
        }
        if ((64 & flag) == 0) {
            this.fogDepth = 0;
        } else {
            this.fogDepth = class966.aInteger233();
        }
    }
}

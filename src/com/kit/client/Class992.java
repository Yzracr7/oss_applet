package com.kit.client;

final class Class992 extends Class1002 {

    protected static int renderY;
    protected int anInt3658;
    protected static int anInt3660 = 0;
    protected int anInt3662;
    protected Class1017_2 aClass130_3663;
    private Class1008 aClass94_3664;
    protected static boolean visibleLevels = true;
    private Class1017_2 aClass130_3666;
    private int anInt3667;

    private final void method615(int var1, ByteBuffer var2, byte var3) {
        try {
            if (var3 > -29) {
                renderY = 70;
            }

            if (~var1 != -2) {
                if (~var1 == -3) {
                    this.anInt3658 = var2.readUnsignedByte();
                } else if (3 != var1) {
                    if (var1 != 4) {
                        if (5 == var1 || 6 == var1) {
                            int var4 = var2.aInteger233();
                            this.aClass130_3663 = new Class1017_2(Class95.method1585(var4));

                            for (int var5 = 0; var5 < var4; ++var5) {
                                int var6 = var2.getInt();
                                Object var7;
                                if (var1 == 5) {
                                    var7 = new Class1030(var2.class_91033());
                                } else {
                                    var7 = new Class1042_2(var2.getInt());
                                }

                                this.aClass130_3663.put((Class1042) var7, (long) var6);
                            }
                        }
                    } else {
                        this.anInt3667 = var2.getInt();
                    }
                } else {
                    this.aClass94_3664 = var2.class_91033();
                }
            } else {
                this.anInt3662 = var2.readUnsignedByte();
            }

        } catch (RuntimeException var8) {
            var8.printStackTrace();
            throw Class1134.method1067(var8, "ml.C(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    final Class1008 method616(int var1, byte var2) {
        try {
            int var3 = 10 / ((var2 - 68) / 50);
            if (null == this.aClass130_3663) {
                return this.aClass94_3664;
            } else {
                Class1030 var4 = (Class1030) this.aClass130_3663.get((long) var1);
                return null == var4 ? this.aClass94_3664 : var4.value;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "ml.S(" + var1 + ',' + var2 + ')');
        }
    }

    final boolean method617(Class1008 var1, int var2) {
        try {
            if (null == this.aClass130_3663) {
                return false;
            } else {
                if (var2 != 8729) {
                    this.method615(-97, (ByteBuffer) null, (byte) -91);
                }

                if (null == this.aClass130_3666) {
                    this.method618(0);
                }

                for (Class3_Sub10 var3 = (Class3_Sub10) this.aClass130_3666.get(var1.method1538()); var3 != null; var3 = (Class3_Sub10) this.aClass130_3666.method1784()) {
                    if (var3.aClass94_2341.equals(var1)) {
                        return true;
                    }
                }

                return false;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ml.F(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    private final void method618(int var1) {
        try {
            this.aClass130_3666 = new Class1017_2(this.aClass130_3663.method1785());
            Class1030 var2 = (Class1030) this.aClass130_3663.getFirst();
            if (var1 == 0) {
                while (var2 != null) {
                    Class3_Sub10 var3 = new Class3_Sub10(var2.value, (int) var2.hash);
                    this.aClass130_3666.put(var3, var2.value.method1538());
                    var2 = (Class1030) this.aClass130_3663.getNext();
                }

            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ml.O(" + var1 + ')');
        }
    }

    static final Class1047[] method619(byte var0, int var1, Class1027 var2) {
        try {
            return Class1013.method2029((byte) -119, var2, var1) ? (var0 <= 52 ? (Class1047[]) null : Class1043.method1281(0)) : null;
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ml.A(" + var0 + ',' + var1 + ',' + (var2 != null ? "{...}" : "null") + ')');
        }
    }

    final int method620(int var1, int var2) {
        try {
            if (this.aClass130_3663 != null) {
                Class1042_2 var3 = (Class1042_2) this.aClass130_3663.get((long) var2);
                return var3 != null ? var3.value : this.anInt3667;
            } else {
                return this.anInt3667;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ml.E(" + var1 + ',' + var2 + ')');
        }
    }

    final boolean method621(int var1, int var2) {
        try {
            if (null != this.aClass130_3663) {
                if (this.aClass130_3666 == null) {
                    this.method622(109);
                }

                if (var1 != -8143) {
                    method619((byte) 68, -100, (Class1027) null);
                }

                Class1042_2 var3 = (Class1042_2) this.aClass130_3666.get((long) var2);
                return var3 != null;
            } else {
                return false;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ml.B(" + var1 + ',' + var2 + ')');
        }
    }

    private final void method622(int var1) {
        try {
            this.aClass130_3666 = new Class1017_2(this.aClass130_3663.method1785());
            int var3 = -48 % ((26 - var1) / 58);

            for (Class1042_2 var2 = (Class1042_2) this.aClass130_3663.getFirst(); null != var2; var2 = (Class1042_2) this.aClass130_3663.getNext()) {
                Class1042_2 var4 = new Class1042_2((int) var2.hash);
                this.aClass130_3666.put(var4, (long) var2.value);
            }

        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "ml.D(" + var1 + ')');
        }
    }

    static final byte[] method623(byte[] var1) {
        ByteBuffer class966 = new ByteBuffer(var1);
        int compressionType = class966.readUnsignedByte();
        int length = class966.getInt();
        if (0 <= length && (-1 == ~Class75.anInt1108 || ~Class75.anInt1108 <= ~length)) {
            if (-1 == ~compressionType) {
                byte[] var8 = new byte[length];
                class966.getOutOurCode(var8, 0, length);
                return var8;
            } else {
                int decompressedLength = class966.getInt();
                if (0 <= decompressedLength && (Class75.anInt1108 == 0 || ~Class75.anInt1108 <= ~decompressedLength)) {
                    byte[] var6 = new byte[decompressedLength];
                    if (compressionType != 1) {
                        Class3_Sub22.aClass49_2505.method1128(var6, class966, false);
                    } else {
                        Class105.method1640(var6, decompressedLength, var1, length, 9);
                    }
                    return var6;
                } else {
                    throw new RuntimeException("RTE");
                }
            }
        } else {
            throw new RuntimeException("RTE");
        }
    }

    final void method625(ByteBuffer var1, int var2) {
        try {
            while (true) {
                int var3 = var1.readUnsignedByte();
                if (-1 == ~var3) {
                    var3 = 68 % ((-84 - var2) / 35);
                    return;
                }

                this.method615(var3, var1, (byte) -84);
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ml.Q(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    static final Class992 list(int id) {
        Class992 var2 = (Class992) Class1210.aClass47_2686.getCS2((long) id, 1400);
        if (var2 != null) {
            return var2;
        } else {
            byte[] var3 = Class45.aClass153_731.getFile(8, id);
            var2 = new Class992();

            if (null != var3) {
                var2.method625(new ByteBuffer(var3), -122);
            }

            Class1210.aClass47_2686.method1097(var2, (long) id, (byte) -115);
            return var2;
        }
    }

    public Class992() {
        this.aClass94_3664 = Class47.aClass94_750;
    }

}

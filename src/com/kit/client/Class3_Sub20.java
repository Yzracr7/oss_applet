package com.kit.client;

final class Class3_Sub20 extends Class1042 {

   static int[] maxStats = new int[25];
   
   //static Class1008 aClass94_2482 = Class1008.createJString(":clan:");
   int anInt2483;
   static int anInt2484 = 0;
   private static Class1008 aClass94_2486 = Class943.create("red:");
   static int anInt2487;
   static int anInt2488 = 0;
   int anInt2489;
   static Class1008 aClass94_2490 = aClass94_2486;
   static Class1008 aClass94_2481 = aClass94_2486;

	/*static final void method388() {
		if (Class3_Sub13_Sub19.aClass94_3220 != null) {
			Class3_Sub10.method138(Class3_Sub13_Sub19.aClass94_3220, 0);
			Class3_Sub13_Sub19.aClass94_3220 = null;
		}
	}*/

   static final void method389(boolean var0) {
      try {
         Class32.method995();
         Class49.minimapLandscape = null;
         Class58.anInt909 = -1;
         Class3_Sub13_Sub30.method313();
         Class3_Sub28_Sub21.aClass47_3801.method1101(2);
         Class10117.aClass136_1413 = new Class136();
         ((Class945) Rasterizer.anClass1226_973).method1618(0);
         Class977.lightsPos = 0;
         Class977.class983s = new Class983[255];
         Class948.method1929();
         Class141.method2043();
         Class65.method1240();
         //Class1006.method1250(var0);
         Class3_Sub13_Sub17.method247((byte)51);

         for(int var1 = 0; 2048 > var1; ++var1) {
            Class946 var2 = Class922.class946List[var1];
            if(null != var2) {
               var2.anObject2796 = null;
            }
         }

         if(Class1012.aBoolean_617) {
            Class141.method2041(104, 104);
            Class1218.method1755();
         }

         Class922.loadFonts(Class1027.cacheIndex13, 0, Class140_Sub6.cacheIndex8);
         //Don't remove below or switching to Hd will crash from compass sprite
         Class14.loadExtraSprites(21, Class140_Sub6.cacheIndex8);
         Class3_Sub26.aClass3_Sub28_Sub16_2560 = null;
         Class3_Sub13_Sub7.aClass3_Sub28_Sub16_3099 = null;
         Class1209.aClass3_Sub28_Sub16_824 = null;
         Class95.aClass3_Sub28_Sub16_1339 = null;
         Class108.aClass3_Sub28_Sub16_1457 = null;
         if(Class922.aInteger_544 == 5) {
            Class922.load_title_screen(Class3_Sub13_Sub25.cacheIndex10);
         }

         if(10 == Class922.aInteger_544) {
            Class3_Sub13_Sub11.constructLoginScreen(false);
         }

         if(Class922.aInteger_544 == 30) {
            Class922.setaInteger_544(25);
         }

      } catch (RuntimeException var3) {
         throw Class1134.method1067(var3, "lb.C(" + var0 + ')');
      }
   }

   static final void method390(boolean var0, int var1, int var2, int var3, byte var4, int var5, int var6) {
      try {
         Class3_Sub28_Sub10.anInt3631 = var3;
         Class3_Sub13_Sub34.anInt3414 = var2;
         Class970.anInt30 = var6;
         Class163_Sub2_Sub1.anInt4021 = var1;
         Class961.anInt1904 = var5;
         if(var0 && Class3_Sub28_Sub10.anInt3631 >= 100) {
            Class1001.renderX = 128 * Class970.anInt30 + 64;
            Class77.renderY = 128 * Class961.anInt1904 + 64;
            Class7.renderZ = Class121.method1736(Class26.plane, 1, Class1001.renderX, Class77.renderY) + -Class3_Sub13_Sub34.anInt3414;
         }

         int var7 = 76 % ((-79 - var4) / 35);
         Class974.anInt1753 = 2;
      } catch (RuntimeException var8) {
         throw Class1134.method1067(var8, "lb.D(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ')');
      }
   }

	public static void method391() {
		aClass94_2490 = null;
		maxStats = null;
		aClass94_2481 = null;
		aClass94_2486 = null;
		// aClass94_2482 = null;
	}

   Class3_Sub20(int var1, int var2) {
      try {
         this.anInt2483 = var2;
         this.anInt2489 = var1;
      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "lb.<init>(" + var1 + ',' + var2 + ')');
      }
   }

   static final void method392(Class1027 skins, Class1027 var1, int var2, Class1027 skeletons) {
      try {
         Class954.aClass153_1860 = var1;
         Class7.skinsFetcher = skins;
         Class131.skeletonsFetcher = skeletons;
      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "lb.E(" + (skins != null?"{...}":"null") + ',' + (var1 != null?"{...}":"null") + ',' + var2 + ',' + (skeletons != null?"{...}":"null") + ')');
      }
   }

}

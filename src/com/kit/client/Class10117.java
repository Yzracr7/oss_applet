package com.kit.client;

import java.util.Random;

final class Class10117 {

	private int groundRgb = 0;
	int anInt1406;
	static int anInt1407;
	int anInt1408;

	static Class1027 aClass153_1410;
	boolean aBoolean1411 = true;
	int anInt1412 = -1;
	static Class136 aClass136_1413 = new Class136();
	int anInt1414 = 128;
	static int anInt1416;
	int anInt1417;
	int anInt1418;
	static Class1008 aClass94_1409 = Class943.create("Loaded input handler");

	private final void calculateHSL(int var1) {
		double var3 = (double) (255 & var1 >> 16) / 256.0D;
		double var5 = (double) (255 & var1 >> 8) / 256.0D;
		double var9 = var3;
		double var7 = (double) (var1 & 255) / 256.0D;
		if (var5 < var3) {
			var9 = var5;
		}

		if (var7 < var9) {
			var9 = var7;
		}

		double var11 = var3;
		double var14 = 0.0D;
		if (var5 > var3) {
			var11 = var5;
		}

		if (var7 > var11) {
			var11 = var7;
		}

		double var16 = 0.0D;
		double var18 = (var11 + var9) / 2.0D;
		if (var9 != var11) {
			if (0.5D > var18) {
				var16 = (var11 - var9) / (var11 + var9);
			}

			if (var11 != var3) {
				if (var5 != var11) {
					if (var7 == var11) {
						var14 = 4.0D + (-var5 + var3) / (-var9 + var11);
					}
				} else {
					var14 = (var7 - var3) / (var11 - var9) + 2.0D;
				}
			} else {
				var14 = (-var7 + var5) / (-var9 + var11);
			}

			if (0.5D <= var18) {
				var16 = (var11 - var9) / (-var9 + (2.0D - var11));
			}
		}

		if (var18 > 0.5D) {
			this.anInt1418 = (int) (var16 * (-var18 + 1.0D) * 512.0D);
		} else {
			this.anInt1418 = (int) (var16 * var18 * 512.0D);
		}

		if (1 > this.anInt1418) {
			this.anInt1418 = 1;
		}

		this.anInt1406 = (int) (var16 * 256.0D);
		this.anInt1417 = (int) (256.0D * var18);
		if (~this.anInt1417 <= -1) {
			if ((Class922.snow ? -255 : 255) < this.anInt1417) {
				this.anInt1417 = 255;
			}
		} else {
			this.anInt1417 = 0;
		}

		var14 /= 6.0D;
		this.anInt1408 = (int) ((double) this.anInt1418 * var14);
		if (-1 >= ~this.anInt1406) {
			if (this.anInt1406 > (Class922.snow ? -255 : 255)) {
				this.anInt1406 = 255;
			}
		} else {
			this.anInt1406 = 0;
		}
	}

	final void decode(ByteBuffer class966) {
		while (true) {
			int opcode = class966.readUnsignedByte();
			if (opcode == 0) {
				return;
			}

			this.decode(class966, opcode);
		}
	}

	static final int method1602(Class1008 var1) {
		if (Class119.aClass131_1624 != null && var1.getLength() != 0) {
			for (int var2 = 0; ~Class119.aClass131_1624.anInt1720 < ~var2; ++var2) {
				if (Class119.aClass131_1624.aClass94Array1721[var2].method1560(Class3_Sub13_Sub16.aClass94_3192, true, Class932.aClass94_4066).method_874(var1)) {
					return var2;
				}
			}

			return -1;
		} else {
			return -1;
		}
	}

	static final int method1603(Random var2, int var1) {
		if (~var1 >= -1) {
			throw new IllegalArgumentException();
		} else if (Class140_Sub6.method2021(var1)) {
			return (int) (((long) var2.nextInt() & 4294967295L) * (long) var1 >> 32);
		} else {
			int var3 = -((int) (4294967296L % (long) var1)) + Integer.MIN_VALUE;

			int var4;
			do {
				var4 = var2.nextInt();
			} while (var3 <= var4);

			return Class3_Sub13_Sub7.method201(var4, var1);
		}
	}

	private final void decode(ByteBuffer class966, int opcode) {
		if (opcode == 1) {
			this.groundRgb = class966.aBoolean183();
			this.calculateHSL(this.groundRgb);
		} else if (opcode == 2) {
			this.anInt1412 = class966.aInteger233();
			if (this.anInt1412 == 65535) {
				this.anInt1412 = -1;
			}
		} else if (opcode == 3) {
			this.anInt1414 = class966.aInteger233();
		} else if (opcode == 4) {
			this.aBoolean1411 = false;
		}
	}

	static final Class10117 list(int id) {
		Class10117 var2 = (Class10117) Class1134.aClass93_725.get((long) id);
		if (var2 != null)
			return var2;
		byte[] var3 = Class3_Sub23.aClass153_2536.getFile(1, id);
		var2 = new Class10117();
		if (Class922.aBoolean_609 && null != var3) {
			var2.decode(new ByteBuffer(var3));
		}

		Class1134.aClass93_725.put(var2, (long) id);
		return var2;
	}

	static final void method1605(Class1008 var1, int var2) {
		Class3_Sub13_Sub1.outputStream.putPacket(188);
		Class3_Sub13_Sub1.outputStream.aInt1033(var2);
		Class3_Sub13_Sub1.outputStream.aInt233(var1.toLong());
	}

	public static void method1606() {
		aClass94_1409 = null;
		aClass153_1410 = null;
		aClass136_1413 = null;
	}
	
	/*public int hue;
	public int saturation;
	public int lightness;
	public int hueVar;
	public int hueDivider;
	public int hslValue;*/
	
	public static int hslToRgb(int color) {
		int r = (int) ((color >> 16 & 0xff) / 256D);
		int g = (int) ((color >> 8 & 0xff) / 256D);
		int b = (int) ((color & 0xff) / 256D);
		double red_val1 = r;
		if (g < red_val1) {
			red_val1 = g;
		}
		if (b < red_val1) {
			red_val1 = b;
		}
		double red_val2 = r;
		if (g > red_val2) {
			red_val2 = g;
		}
		if (b > red_val2) {
			red_val2 = b;
		}
		return /*(alpha << 24) |*/ (r << 16) | (g << 8) | (b << 0);
		/*double hueCalc = 0.0D;
		double satCalc = 0.0D;
		double lightCalc = (red_val1 + red_val2) / 2D;
		if (red_val1 != red_val2) {
			if (lightCalc < 0.5D) {
				satCalc = (red_val2 - red_val1) / (red_val2 + red_val1);
			}
			if (lightCalc >= 0.5D) {
				satCalc = (red_val2 - red_val1) / (2D - red_val2 - red_val1);
			}
			if (r == red_val2) {
				hueCalc = (g - b) / (red_val2 - red_val1);
			} else if (g == red_val2) {
				hueCalc = 2D + (b - r) / (red_val2 - red_val1);
			} else if (b == red_val2) {
				hueCalc = 4D + (r - g) / (red_val2 - red_val1);
			}
		}
		hueCalc /= 6D;
		hue = (int) (hueCalc * 256D);
		saturation = (int) (satCalc * 256D);
		lightness = (int) (lightCalc * 256D);
		if (saturation < 0) {
			saturation = 0;
		} else if (saturation > 255) {
			saturation = 255;
		}
		if (lightness < 0) {
			lightness = 0;
		} else if (lightness > 255) {
			lightness = 255;
		}
		if (lightCalc > 0.5D) {
			hueDivider = (int) ((1.0D - lightCalc) * satCalc * 512D);
		} else {
			hueDivider = (int) (lightCalc * satCalc * 512D);
		}
		if (hueDivider < 1) {
			hueDivider = 1;
		}
		hueVar = (int) (hueCalc * (double) hueDivider);
		int hueOffset = hue;
		int saturationOffset = saturation;
		int lightnessOffset = lightness;
		boolean BOT_RANDOMIZATION = false;
		if (BOT_RANDOMIZATION) {
			hueOffset = (hue + (int) (Math.random() * 16D)) - 8;
			if (hueOffset < 0) {
				hueOffset = 0;
			} else if (hueOffset > 255) {
				hueOffset = 255;
			}
			saturationOffset = (saturation + (int) (Math.random() * 48D)) - 24;
			if (saturationOffset < 0) {
				saturationOffset = 0;
			} else if (saturationOffset > 255) {
				saturationOffset = 255;
			}
			lightnessOffset = (lightness + (int) (Math.random() * 48D)) - 24;
			if (lightnessOffset < 0) {
				lightnessOffset = 0;
			} else if (lightnessOffset > 255) {
				lightnessOffset = 255;
			}
		}
		hslValue = getHSLValue(hueOffset, saturationOffset, lightnessOffset);*/
	}

	private int getHSLValue(int hue, int saturation, int lightness) {
		if (lightness > 179) {
			saturation /= 2;
		}
		if (lightness > 192) {
			saturation /= 2;
		}
		if (lightness > 217) {
			saturation /= 2;
		}
		if (lightness > 243) {
			saturation /= 2;
		}
		return (hue / 4 << 10) + (saturation / 32 << 7) + lightness / 2;
	}


}

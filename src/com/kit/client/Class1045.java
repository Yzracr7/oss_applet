package com.kit.client;

final class Class1045 {

    protected static int anInt869;
    protected int[] anIntArray870;
    protected static Class1008 aClass94_871 = Class943.create("headicons_hint");
    protected static int anInt872;
    protected Class1008[] aClass94Array873;
    protected Class951 aClass3_Sub28_Sub15_874;
    protected int anInt877 = -1;
    protected static Class1027 aClass153_878;

    static final boolean checkIfSelf(Class1008 userName, byte var1) {
        try {
            if (userName != null) {
                for (int var2 = 0; Class8.localPlayerIds > var2; ++var2) {
                    if (userName.method102(Class70.localPlayerNames[var2])) {
                        return true;
                    }
                }

                if (var1 != -82) {
                    aClass94_871 = (Class1008) null; //reset headicon
                }

                if (userName.method102(Class945.thisClass946.username)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "hj.A(" + (userName != null ? "{...}" : "null") + ',' + var1 + ')');
        }
    }

    static final void passContextOptions(short type, int var0, long var1, Class1008 target, int xRelativeToPlayer, Class1008 action, int yRelativeToPlayer) {
        //
        //short 9 is teleports
        //short 32 is spell casting
        //Shot 5 equip in invy
        //Use(invy) short 22

        //var8 138 on active item?

        //System.out.println("target: " + target + ", action: "+ action + " on type " + type + " var0: "+ var0
        //		   + " var1: "+ var1);

        if (Class38_Sub1.drawContextMenu || Class3_Sub13_Sub34.contextOptionsAmount >= 500)
            return;

        //60 - walk
        //42 - object action(blue text) - like chop tree
        //50 smelt furnace
        //1004 - examine OBJ
        //1006 - EXAMINE ITEM
        //1007 examine npc

        //4 npc second option
        //17 talk npc
        //16 attack npc

        //2031 follow player
        //2030 Challenge player
        //2029 trade player
        //30 attack player

        if (Class922.clientSize > 0 && Class922.aInteger_544 == 30) {
                     /* && (Class3_Sub13_Sub34.contextOptionsAmount == 2 ||*/ //&&
            //(ByteBuffer.lastActionString.toLowerCase().contains("walk")
            //		|| ByteBuffer.lastActionString.toLowerCase().contains("chop down"))) {
            int x = Class981.clickX;
            int y = Class38_Sub1.clickY;

            if (x >= (Class922.resizeWidth - 214)
                    && (y >= (Class922.resizeHeight - 345)
                    || (Class922.resizeWidth < 1000 && y >= (Class922.resizeHeight - 380)) //for collapsible
            ) && !Class922.inventoryHidden) {
                //System.out.println("var6: "+ type);
                if (type == 60 || type == 42 || type == 1004 || type == 50 || type == 4 || type == 16 || type == 1007
                        || type == 17 || type == 2030 || type == 2031 || type == 30 || type == 2030) {
                    //Blocked invy
                    return;
                }
            } else if (x >= 497 && x <= 520 && y >= (Class922.resizeHeight - 200)) {
                //Blocked chatbox scrollbar
                return;
            }
            //Below is full invy block
	     		 /* else if (x <= 520 && y >= (client.resizeHeight - 200)) {
	     			 //Blocked chatbox - always? later on maybe unlock to add friend
	     			 return;
	     		 }*/
            //System.out.println("Context(made it thru): " + Class3_Sub13_Sub34.contextOptionsAmount + ", " + Class981.aInteger_514 + ", " + Class38_Sub1.aInteger_513);
        }

        //var5 = itemSlot in invy
        //var1 = itemId in invy
        //var8 might be container uid
        Class1013.aClass94Array2935[Class3_Sub13_Sub34.contextOptionsAmount] = action;
        Class163_Sub2_Sub1.contextOpStrings[Class3_Sub13_Sub34.contextOptionsAmount] = target;
        Class114.anIntArray1578[Class3_Sub13_Sub34.contextOptionsAmount] = ~var0 == 0 ? Class955.anInt3590 : var0;
        Class3_Sub13_Sub7.aShortArray3095[Class3_Sub13_Sub34.contextOptionsAmount] = type;
        Class120_Sub30_Sub1.aLongArray3271[Class3_Sub13_Sub34.contextOptionsAmount] = var1;
        Class117.anIntArray1613[Class3_Sub13_Sub34.contextOptionsAmount] = xRelativeToPlayer;
        Class27.anIntArray512[Class3_Sub13_Sub34.contextOptionsAmount] = yRelativeToPlayer;
        ++Class3_Sub13_Sub34.contextOptionsAmount;
    }

    public static void method1178(byte var0) {
        try {
            int var1 = 103 / ((var0 - -13) / 52);
            aClass94_871 = null;
            aClass153_878 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "hj.B(" + var0 + ')');
        }
    }

}

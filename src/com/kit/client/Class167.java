package com.kit.client;

final class Class167 {

   protected    static Class1008 aClass94_2082 = Class943.create(" <col=ffff00>");
   protected   static Class1008 aClass94_2083 = null;
   protected   static Class1008 aClass94_2084 = Class943.create("ul");
   protected   static int anInt2087 = 0;

   static final void method2261(int var0) {
      try {
         while(true) {
            if(Class1211.incomingPackets.getBitsLeft(Class1017_2.anInt1704) >= 27) {
               int var1 = Class1211.incomingPackets.getBits(15);
               if(32767 != var1) {
                  boolean var2 = false;
                  if(null == Class3_Sub13_Sub24.class1001List[var1]) {
                     var2 = true;
                     Class3_Sub13_Sub24.class1001List[var1] = new Class1001();
                  }

                  Class1001 var3 = Class3_Sub13_Sub24.class1001List[var1];
                  Class15.anIntArray347[Class163.anInt2046++] = var1;
                  var3.anInt2838 = Class1134.loopCycle;
                  if(null != var3.aClass90_3976 && var3.aClass90_3976.method1474(-1)) {
                     Class3_Sub28_Sub8.method574(var3, false);
                  }
                  
                  
                  int var7 = Class1211.incomingPackets.getBits(5);
                  if(15 < var7) {
                      var7 -= 32;
                   }
                 
                  
                  var3.method1987(-1, Class981.list(Class1211.incomingPackets.getBits(14)));
                  

                  int var6 = Class1211.incomingPackets.getBits(1);
                  if(~var6 == -2) {
                     Class21.anIntArray441[Class1006.anInt997++] = var1;
                  }

                  int var4 = Class1211.incomingPackets.getBits(1);
                  int var5 = Class27.possibleSizes[Class1211.incomingPackets.getBits(3)];
                  if(var2) {
                     var3.turnDirection = var3.directionDegrees = var5;
                  }
                  
                  int var8 = Class1211.incomingPackets.getBits(5);
                  if(15 < var8) {
                     var8 -= 32;
                  }

                  var3.setSize(var3.aClass90_3976.size, 2);
                  var3.idleAnimation = var3.aClass90_3976.idleAnim;
                  var3.walkAnimation = var3.aClass90_3976.walkAnim;
                  var3.turn180Animation = var3.aClass90_3976.turn180Animation;
                  var3.turn90CWAnimation = var3.aClass90_3976.turn90CWAnimation;
                  var3.turn90CCAnimation = var3.aClass90_3976.turn90CCAnimation;
                  //var3.anInt2763 = var3.aClass90_3976.anInt1280;
                  var3.anInt2779 = var3.aClass90_3976.anInt1274;
                  if(~var3.anInt2779 == -1) {
                     var3.directionDegrees = 0;
                  }

                  var3.method1967(var3.getSize(), Class945.thisClass946.anIntArray2767[0] + var8, var7 + Class945.thisClass946.anIntArray2755[0], ~var4 == -2);
                  if(var3.aClass90_3976.method1474(-1)) {
                     Class70.method1286(var3.anIntArray2755[0], false, (ObjectDefinition)null, 0, var3, var3.anIntArray2767[0], Class26.plane, (Class946)null);
                  }
                  continue;
               }
            }

            Class1211.incomingPackets.endBitAccess();
            if(var0 <= 0) {
               method2265(-16);
            }

            return;
         }
      } catch (RuntimeException var9) {
         throw Class1134.method1067(var9, "wj.E(" + var0 + ')');
      }
   }

   public static void method2262(byte var0) {
      try {
         aClass94_2083 = null;
         if(var0 > 0) {
            aClass94_2082 = null;
            aClass94_2084 = null;
         }
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "wj.F(" + var0 + ')');
      }
   }

   static final void method2263(int var0, int var1, int var2, int var3, int var4, int var5, int var6) {
      Class113 var7 = new Class113();
      var7.anInt1553 = var1 / 128;
      var7.anInt1547 = var2 / 128;
      var7.anInt1563 = var3 / 128;
      var7.anInt1566 = var4 / 128;
      var7.anInt1554 = var0;
      var7.anInt1562 = var1;
      var7.anInt1545 = var2;
      var7.anInt1560 = var3;
      var7.anInt1550 = var4;
      var7.anInt1544 = var5;
      var7.anInt1548 = var6;
      Class3_Sub28_Sub8.aClass113Array3610[Class990.anInt2249++] = var7;
   }

   static final void method2264(boolean var0) {
      if(var0) {
         Class75_Sub2.class949s = Class166.aClass3_Sub2ArrayArrayArray2065;
         Class1134.activeTileHeightMap = Class956.anIntArrayArrayArray3605;
         Class3_Sub23.aClass3_Sub11ArrayArray2542 = Class3_Sub13_Sub28.aClass3_Sub11ArrayArray3346;
      } else {
         Class75_Sub2.class949s = Class932.aClass3_Sub2ArrayArrayArray4070;
         Class1134.activeTileHeightMap = Class58.anIntArrayArrayArray914;
         Class3_Sub23.aClass3_Sub11ArrayArray2542 = Class922.aClass3_Sub11ArrayArray2199;
      }

      Class1227.anInt2456 = Class75_Sub2.class949s.length;
   }

   static final void method2265(int var0) {
      try {
         Class1048.aClass93_2442.clearAll();
         if(var0 != 0) {
            aClass94_2084 = (Class1008)null;
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "wj.B(" + var0 + ')');
      }
   }

   static final void handleMusicEffect(int var0, int var1, byte var2) {
      try {
         if(Class9.musicVolume != 0 && var1 != -1) {
            Class70.method1285(Class1002.cacheIndex11, false, var1, 0, false, Class9.musicVolume);
            Class83.aBoolean1158 = true;
         }

         if(var2 != -1) {
            aClass94_2084 = (Class1008)null;
         }

      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "wj.D(" + var0 + ',' + var1 + ',' + var2 + ')');
      }
   }

   static final void method2267(int var0, int var1, boolean var2, ByteBuffer var3, int var4, int var5, byte var6, int var7, int var8) {
      try {
         int var9;
         if(-1 >= ~var5 && ~var5 > -105 && var4 >= 0 && -105 < ~var4) {
            if(!var2) {
               Class9.groundArray[var8][var5][var4] = 0;
            }

            while(true) {
               var9 = var3.readUnsignedByte();
               if(-1 == ~var9) {
                  if(!var2) {
                     if(~var8 != -1) {
                        Class1134.activeTileHeightMap[var8][var5][var4] = -240 + Class1134.activeTileHeightMap[var8 - 1][var5][var4];
                     } else {
                        Class1134.activeTileHeightMap[0][var5][var4] = 8 * -Class32.method993(var4 + 556238 + var1, 125, var0 + var5 + 932731);
                     }
                  } else {
                     Class1134.activeTileHeightMap[0][var5][var4] = Class58.anIntArrayArrayArray914[0][var5][var4];
                  }
                  break;
               }

               if(var9 == 1) {
                  int var10 = var3.readUnsignedByte();
                  if(!var2) {
                     if(~var10 == -2) {
                        var10 = 0;
                     }

                     if(-1 == ~var8) {
                        Class1134.activeTileHeightMap[0][var5][var4] = 8 * -var10;
                     } else {
                        Class1134.activeTileHeightMap[var8][var5][var4] = -(var10 * 8) + Class1134.activeTileHeightMap[-1 + var8][var5][var4];
                     }
                  } else {
                     Class1134.activeTileHeightMap[0][var5][var4] = Class58.anIntArrayArrayArray914[0][var5][var4] - -(var10 * 8);
                  }
                  break;
               }

               if(49 >= var9) {
                  Class139.aByteArrayArrayArray1828[var8][var5][var4] = var3.getByte();
                  Class93.aByteArrayArrayArray1328[var8][var5][var4] = (byte)((-2 + var9) / 4);
                  Class979.aByteArrayArrayArray81[var8][var5][var4] = (byte) Class951.method633(-2 + var9 + var7, 3);
               } else if(var9 > 81) {
                  Class3_Sub13_Sub36.aByteArrayArrayArray3430[var8][var5][var4] = (byte)(-81 + var9);
               } else if(!var2) {
                  Class9.groundArray[var8][var5][var4] = (byte)(var9 - 49);
               }
            }
         } else {
            while(true) {
               var9 = var3.readUnsignedByte();
               if(~var9 == -1) {
                  break;
               }

               if(~var9 == -2) {
                  var3.readUnsignedByte();
                  break;
               }

               if(-50 <= ~var9) {
                  var3.readUnsignedByte();
               }
            }
         }

         if(var6 < 58) {
            anInt2087 = 87;
         }

      } catch (RuntimeException var11) {
         throw Class1134.method1067(var11, "wj.A(" + var0 + ',' + var1 + ',' + var2 + ',' + (var3 != null?"{...}":"null") + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ')');
      }
   }

   static final int method2268(int var1, int var2) {
         Class1042_4 var3 = (Class1042_4) Class949.aClass130_2220.get((long)var1);
         if(var3 != null) {
            if(var2 != -1) {
               int var4 = 0;

               for(int var5 = 0; ~var5 > ~var3.anIntArray2551.length; ++var5) {
                  if(~var2 == ~var3.anIntArray2547[var5]) {
                     var4 += var3.anIntArray2551[var5];
                  }
               }
               return var4;
            } else {
               return 0;
            }
         } else {
            return 0;
         }
   }

	static final void processLogout() {
		if (null != Class3_Sub15.worldConnection) {
			Class3_Sub15.worldConnection.close();
			Class3_Sub15.worldConnection = null;
		}

		Class3_Sub13_Sub30.method313();
		Class32.method995();

		for (int var1 = 0; var1 < 4; ++var1) {
			Class930.class972[var1].resetFlags();
		}

		// Class1006.method1250(false);
		System.gc();
		Class1245.method882(-1, 2);
		Class83.aBoolean1158 = false;
		Class963.currentSound = -1;
		Class164_Sub1.method2241((byte) -77, true);
		Class1030.aBoolean2583 = false;
		SpriteDefinition.anInt1152 = 0;
		Class956.anInt3606 = 0;
		Class419.anInt2294 = 0;
		Class131.anInt1716 = 0;

		for (int var1 = 0; Class1244.hintsList.length > var1; ++var1) {
			Class1244.hintsList[var1] = null;
		}

		Class159.anInt2022 = 0;
		Class163.anInt2046 = 0;

		for (int var1 = 0; var1 < 2048; ++var1) {
			Class922.class946List[var1] = null;
			Class65.playerClass966List[var1] = null;
		}

		for (int var1 = 0; -32769 < ~var1; ++var1) {
			Class3_Sub13_Sub24.class1001List[var1] = null;
		}

		for (int var1 = 0; 4 > var1; ++var1) {
			for (int var2 = 0; -105 < ~var2; ++var2) {
				for (int var3 = 0; ~var3 > -105; ++var3) {
					Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[var1][var2][var3] = null;
				}
			}
		}

		Class955.method560(-21556);
		Class3_Sub13_Sub2.resetVarp();
		Class3_Sub13_Sub11.constructLoginScreen(true);
	}
         
}

package com.kit.client;

final class Class166 {

    protected int anInt2063;
    protected byte[] aByteArray2064;
    static Class949[][][] aClass3_Sub2ArrayArrayArray2065;
    protected int anInt2066;
    protected int anInt2067;
    static int[] anIntArray2068 = new int[50];
    protected int anInt2069;
    static Class955[] aClass3_Sub28_Sub5Array2070 = new Class955[14];
    protected int anInt2071;
    protected static Class957[] hintIconSprites;
    protected static int[] anIntArray2073 = new int[5];
    static Class1008 aClass94_2075 = Class943.create("rect_debug=");
    protected byte[] aByteArray2076;
    protected int anInt2077;
    protected int anInt2078;
    protected static int anInt2079 = 0;
    static Class1008 aClass94_2080 = Class943.create("(U2");


    public static void method2255(byte var0) {
        try {
            anIntArray2073 = null;
            aClass3_Sub2ArrayArrayArray2065 = (Class949[][][]) null;
            aClass94_2080 = null;
            aClass94_2075 = null;
            if (var0 >= -126) {
                aClass94_2080 = (Class1008) null;
            }

            anIntArray2068 = null;
            aClass3_Sub28_Sub5Array2070 = null;
            hintIconSprites = null;
            //aClass94_2074 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "wh.B(" + var0 + ')');
        }
    }

    static final boolean method2256(int var0, int var1, int var2, int var3) {
        if (!Class8.method846(var0, var1, var2)) {
            return false;
        } else {
            int var4 = var1 << 7;
            int var5 = var2 << 7;
            return Class3_Sub13_Sub37.method349(var4 + 1, Class1134.activeTileHeightMap[var0][var1][var2] + var3, var5 + 1) && Class3_Sub13_Sub37.method349(var4 + 128 - 1, Class1134.activeTileHeightMap[var0][var1 + 1][var2] + var3, var5 + 1) && Class3_Sub13_Sub37.method349(var4 + 128 - 1, Class1134.activeTileHeightMap[var0][var1 + 1][var2 + 1] + var3, var5 + 128 - 1) && Class3_Sub13_Sub37.method349(var4 + 1, Class1134.activeTileHeightMap[var0][var1][var2 + 1] + var3, var5 + 128 - 1);
        }
    }

    static final void method2257(int var0) {
        try {
            if (var0 < 60) {
                hintIconSprites = (Class957[]) null;
            }

            Class163_Sub2_Sub1.aClass93_4015.clearAll();
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "wh.F(" + var0 + ')');
        }
    }

    static final void method2258(int type, int var1, Class1008 playerName) {
        try {
            Class1008 cleanedName = playerName.method1579(-17).upperCase();
            boolean var4 = false;

            // System.out.println("Class946 option packet: " + type + ", " + var1 + ", " + playerName.toString());


            for (int var5 = var1; ~var5 > ~Class159.anInt2022; ++var5) {

                Class946 otherClass946 = Class922.class946List[Class922.playerIndices[var5]];

                if (null != otherClass946 && null != otherClass946.username && otherClass946.username.method102(cleanedName)) {
                    var4 = true;
                    Class3_Sub28_Sub9.method582(Class945.thisClass946.anIntArray2755[0], 0, 1, false, 0, 2, otherClass946.anIntArray2767[0], 1, 0, 2, otherClass946.anIntArray2755[0], Class945.thisClass946.anIntArray2767[0]);

                    //System.out.println("TYPE: " + type);
                    if (1 == type || type == 4) { //type == 4 cheaphax for trade
                        //challenge request click chatbox text
                        Class3_Sub13_Sub1.outputStream.putPacket(68);
                        Class3_Sub13_Sub1.outputStream.aLittleBitch(type);
                        Class3_Sub13_Sub1.outputStream.aLittleBitch(Class922.playerIndices[var5]);
                    } else if (4 != type) {
                        if (5 != type) {
                            if (~type != -7) {
                                if (~type == -8) {
                                    Class3_Sub13_Sub1.outputStream.putPacket(114);
                                    Class3_Sub13_Sub1.outputStream.aLittleBitch(Class922.playerIndices[var5]);
                                }
                            } else {
                                Class3_Sub13_Sub1.outputStream.putPacket(133);
                                Class3_Sub13_Sub1.outputStream.method_122(Class922.playerIndices[var5]);
                            }
                        } else {
                            Class3_Sub13_Sub1.outputStream.putPacket(4);
                            Class3_Sub13_Sub1.outputStream.method_122(Class922.playerIndices[var5]);
                        }
                    } else {
                        Class3_Sub13_Sub1.outputStream.putPacket(180);
                        Class3_Sub13_Sub1.outputStream.aLittleBitch(Class922.playerIndices[var5]);
                    }
                    break;
                }
            }

            if (!var4) {
                Class966_2.sendMessage(Class922.BLANK_CLASS_1008, Class922.combinejStrings(new Class1008[]{Class983.aClass94_691, cleanedName}), 0);
            }

        } catch (RuntimeException var7) {
            throw Class1134.method1067(var7, "wh.D(" + type + ',' + var1 + ',' + (playerName != null ? "{...}" : "null") + ')');
        }
    }

    static final Class927 method2259() {
        Object var1;
        if (Class1012.aBoolean_617) {
            var1 = new Class928(Class3_Sub15.spriteTrimWidth, Class974.spriteTrimHeight, Class164.spriteXOffsets[0], ByteBuffer.aInteger1259[0], Class1013.spriteWidths[0], Class3_Sub13_Sub6.spriteHeights[0], Class163_Sub1.spritePaletteIndicators[0], Class3_Sub13_Sub38.spritePalette);
        } else {
            var1 = new Class1047(Class3_Sub15.spriteTrimWidth, Class974.spriteTrimHeight, Class164.spriteXOffsets[0], ByteBuffer.aInteger1259[0], Class1013.spriteWidths[0], Class3_Sub13_Sub6.spriteHeights[0], Class163_Sub1.spritePaletteIndicators[0], Class3_Sub13_Sub38.spritePalette);
        }

        Class922.resetSprites();
        return (Class927) var1;
    }

    static final void method2260(int var0, int var1) {
        try {
            Class3_Sub13_Sub34.aClass93_3412.method1522(var1);
            if (var0 == -1045) {
                Class3_Sub13_Sub31.aClass93_3369.method1522(var1);
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wh.E(" + var0 + ',' + var1 + ')');
        }
    }

}

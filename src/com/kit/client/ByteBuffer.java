package com.kit.client;

import java.math.BigInteger;

public class ByteBuffer extends Class1042 {

    static int[] aInteger1259;
    int offset;
    static Class1008[] aClass94Array2596 = null;
    static Class1008 aClass94_2597 = Class943.create("Loaded interfaces");
    static Class1008 newLinejString = Class943.create("<br>");
    static Class3_Sub28_Sub3 aClass3_Sub28_Sub3_2600;
    byte[] buffer;
    static Class1009[] aBoolean_7311 = new Class1009[256];

    final int aInteger233() {
        offset += 2;
        return (buffer[offset - 2] << 8 & '\uff00') + (buffer[offset - 1] & 255);
    }

    final void method_211(int var2) {
        buffer[offset++] = (byte) (var2 >> 24);
        buffer[offset++] = (byte) (var2 >> 16);
        buffer[offset++] = (byte) (var2 >> 8);
        buffer[offset++] = (byte) var2;
    }

    final void method739(int var1, int var2, long var3) {
        try {
            --var2;
            if (~var2 <= -1 && -8 <= ~var2) {
                if (var1 == 0) {
                    for (int var5 = var2 * 8; 0 <= var5; var5 -= 8) {
                        buffer[offset++] = (byte) ((int) (var3 >> var5));
                    }
                }
            } else {
                throw new IllegalArgumentException();
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "wa.OB(" + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

    final void aInt233(long var1) {
        buffer[offset++] = (byte) ((int) (var1 >> 56));
        buffer[offset++] = (byte) ((int) (var1 >> 48));
        buffer[offset++] = (byte) ((int) (var1 >> 40));
        buffer[offset++] = (byte) ((int) (var1 >> 32));
        buffer[offset++] = (byte) ((int) (var1 >> 24));
        buffer[offset++] = (byte) ((int) (var1 >> 16));
        buffer[offset++] = (byte) ((int) (var1 >> 8));
        buffer[offset++] = (byte) ((int) var1);
    }

    final int method741() {
        byte var2 = buffer[offset++];
        int var3;
        for (var3 = 0; 0 > var2; var2 = buffer[offset++]) {
            var3 = (127 & var2 | var3) << 7;
        }

        return var2 | var3;
    }

    final void method742(int var1, int var2) {
        try {
            buffer[-4 + offset + -var2] = (byte) (var2 >> 24);
            buffer[-var2 + offset - 3] = (byte) (var2 >> 16);
            buffer[-2 + offset + -var2] = (byte) (var2 >> 8);
            if (var1 < 78) {
                method771(37);
            }

            buffer[-var2 + offset + -1] = (byte) var2;
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "wa.VB(" + var1 + ',' + var2 + ')');
        }
    }

    final void aM_211(int var2) {
        buffer[offset++] = (byte) (128 - var2);
    }

    static final void method744(boolean var0) {
        try {
            if (!var0) {
                method784(-10, -32, -21);
            }

            ++Class1005.anInt1908;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "wa.HA(" + var0 + ')');
        }
    }

    final void aInt_322(Class1008 var2) {
        offset += var2.method1580(true, buffer, offset, 0, var2.getLength());
        buffer[offset++] = 0;
    }

    static final void method746(byte var0) {
        try {
            Class67.aClass93_1013.clearAll();
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "wa.VC(" + var0 + ')');
        }
    }

    final int getShortA() {
        offset += 2;
        int var2 = (buffer[-2 + offset] << 8 & '\uff00') - -(-128 + buffer[offset + -1] & 255);
        if (var2 > 32767) {
            var2 -= 65536;
        }
        return var2;
    }

    final int getInt() {
        offset += 4;
        return (((buffer[offset - 1]) & 0xff)
                + (((buffer[offset - 2]) & 0xff) << 8)
                + (((buffer[offset - 4]) & 0xff) << 24)
                + (((buffer[offset - 3]) & 0xff) << 16));
    }

    final int aInt12000(byte var1) {
        try {
            offset += 3;
            int var2 = 40 % ((-7 - var1) / 47);
            return ((buffer[offset + -2] & 255) << 8) + ((buffer[-1 + offset] & 255) << 16) + (buffer[-3 + offset] & 255);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.BA(" + var1 + ')');
        }
    }

    final byte aReturn111() {
        return (byte) (128 - buffer[offset++]);
    }

    final Class1008 method750(byte var1) {
        try {
            if (var1 != 78) {
                return (Class1008) null;
            } else if (buffer[offset] != 0) {
                return class_91033();
            } else {
                ++offset;
                return null;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.IB(" + var1 + ')');
        }
    }

    final int aInt2016() {
        return 255 & (buffer[offset++] - 128);
    }

    final void aBlowMe(int var2) {
        buffer[offset++] = (byte) var2;
    }

    final void method753(byte[] var1, int var2, int var3, int var4) {
        try {
            int var5 = var2;
            if (var4 >= 37) {
                while (~var5 > ~(var2 + var3)) {
                    buffer[offset++] = var1[var5];
                    ++var5;
                }
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "wa.QC(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
        }
    }

    final int aInt122() {
        return 128 - buffer[offset++] & 255;
    }

    final long aVar100() {
        long var2 = (long) getInt() & 4294967295L;
        long var4 = 4294967295L & (long) getInt();
        return var4 + (var2 << 32);
    }

    final void method_12(int var1) {
        buffer[offset++] = (byte) var1;
        buffer[offset++] = (byte) (var1 >> 8);
        buffer[offset++] = (byte) (var1 >> 16);
        buffer[offset++] = (byte) (var1 >> 24);
    }

    final int aMethod10() {
        offset += 2;
        return (buffer[offset - 1] - 128 & 255) + ('\uff00' & buffer[offset - 2] << 8);
    }

    final void aRover_107(int var2) {
        buffer[offset++] = (byte) (var2 >> 16);
        buffer[offset++] = (byte) (var2 >> 24);
        buffer[offset++] = (byte) var2;
        buffer[offset++] = (byte) (var2 >> 8);
    }

    ByteBuffer(int var1) {
        try {
            buffer = Class134.method1807(66, var1);
            offset = 0;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.<init>(" + var1 + ')');
        }
    }

    final byte getByte() {
        return buffer[offset++];
    }

    final Class1008 method761(int var1) {
        try {
            byte var2 = buffer[offset++];
            if (var1 < 50) {
                buffer = (byte[]) null;
            }
            if (0 != var2) {
                throw new IllegalStateException("Bad version number in gjstr2");
            } else {
                int var3 = offset;
                while (-1 != ~buffer[offset++]) {
                    ;
                }
                return Class3_Sub13_Sub3.bufferToString(buffer, var3, offset - (var3 - -1));
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "wa.DA(" + var1 + ')');
        }
    }

    final void method762(float var1) {
        int var3 = Float.floatToRawIntBits(var1);
        buffer[offset++] = (byte) var3;
        buffer[offset++] = (byte) (var3 >> 8);
        buffer[offset++] = (byte) (var3 >> 16);
        buffer[offset++] = (byte) (var3 >> 24);
    }

    final byte getByteC() {
        return (byte) (-buffer[offset++] + 0);
    }

    final void getOutOurCode(byte[] buffer, int off, int len) {
        for (int var5 = off; off + len > var5; ++var5) {
            buffer[var5] = this.buffer[offset++];
        }
    }

    final void aLittleBitch(int var1) {
        buffer[offset++] = (byte) (var1 + 128);
        buffer[offset++] = (byte) (var1 >> 8);
    }

    final int aLong_011() {
        offset += 2;
        return (255 & buffer[offset - 2]) + ('\uff00' & buffer[offset - 1] << 8);
    }

    public static void method767(int var0) {
        try {
            aInteger1259 = null;
            aClass94_2597 = null;
            aClass3_Sub28_Sub3_2600 = null;
            aClass94Array2596 = null;
            aBoolean_7311 = null;
            newLinejString = null;
            if (var0 != 0) {
                aInteger1259 = (int[]) null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "wa.IC(" + var0 + ')');
        }
    }

    final void aLong_1017(int var2) {
        if (0 <= var2 && ~var2 > -129) {
            aBlowMe(var2);
        } else if (0 <= var2 && ~var2 > -32769) {
            method_0133('\u8000' + var2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    final void aLong105(int var2) {
        buffer[-1 + -var2 + offset] = (byte) var2;
    }

    final void method770(int[] var1, int var2, int var3, int var4) {
        try {
            int var5 = offset;
            offset = var3;
            int var6 = (-var3 + var4) / 8;
            for (int var7 = 0; var6 > var7; ++var7) {
                int var10 = -957401312;
                int var8 = getInt();
                int var9 = getInt();
                int var12 = 32;
                for (int var11 = -1640531527; var12-- > 0; var8 -= (var9 >>> 5 ^ var9 << 4) + var9 ^ var1[var10 & 3] + var10) {
                    var9 -= var1[(6754 & var10) >>> 11] + var10 ^ var8 + (var8 >>> 5 ^ var8 << 4);
                    var10 -= var11;
                }
                offset -= 8;
                method_211(var8);
                method_211(var9);
            }
            if (var2 <= 102) {
                aInteger1259 = (int[]) null;
            }
            offset = var5;
        } catch (RuntimeException var13) {
            throw Class1134.method1067(var13, "wa.SC(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ')');
        }
    }

    final void method771(int var1) {
        if (~(-128 & var1) != -1) {
            if (-1 != ~(-16384 & var1)) {
                if ((var1 & -2097152) != 0) {
                    if (0 != (-268435456 & var1)) {
                        aBlowMe(var1 >>> 28 | 128);
                    }
                    aBlowMe(128 | var1 >>> 21);
                }
                aBlowMe(128 | var1 >>> 14);
            }
            aBlowMe(var1 >>> 7 | 128);
        }
        aBlowMe(var1 & 127);
    }

    final long method772(int var1, int var2) {
        try {
            --var1;
            if (var2 <= var1 && ~var1 >= -8) {
                long var4 = 0L;
                for (int var3 = var1 * 8; ~var3 <= -1; var3 -= 8) {
                    var4 |= ((long) buffer[offset++] & 255L) << var3;
                }
                return var4;
            } else {
                throw new IllegalArgumentException();
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "wa.VA(" + var1 + ',' + var2 + ')');
        }
    }

    final int method773() {
        int var3 = aNon145();
        int var2;
        for (var2 = 0; 32767 == var3; var2 += 32767) {
            var3 = aNon145();
        }
        var2 += var3;
        return var2;
    }

    final void method774(int var1, int var2, byte[] var3, int var4) {
        try {
            if (var1 == 2) {
                for (int var5 = var4 - (-var2 - -1); ~var4 >= ~var5; --var5) {
                    var3[var5] = buffer[offset++];
                }

            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "wa.HC(" + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ')');
        }
    }

    final void aBool_0113(int var1) {
        buffer[offset++] = (byte) (var1 >> 8);
        buffer[offset++] = (byte) var1;
        buffer[offset++] = (byte) (var1 >> 24);
        buffer[offset++] = (byte) (var1 >> 16);
    }

    final Class1008 class_91033() {
        int var2 = offset;
        while (0 != buffer[offset++]) {
            ;
        }
        return Class3_Sub13_Sub3.bufferToString(buffer, var2, (offset - 1) - var2);
    }

    static final void method777(Class972[] var0, boolean var1, int var2, int var3, int var4, int var5, int var6, byte[] var7) {
        try {
            int var10;
            int var11;
            if (!var1) {
                for (int var9 = 0; 4 > var9; ++var9) {
                    for (var10 = 0; -65 < ~var10; ++var10) {
                        for (var11 = 0; ~var11 > -65; ++var11) {
                            if (-1 > ~(var5 - -var10) && var10 + var5 < 103 && ~(var3 + var11) < -1 && ~(var11 + var3) > -104) {
                                var0[var9].aBoolean_4222[var10 + var5][var3 - -var11] = Class951.method633(var0[var9].aBoolean_4222[var10 + var5][var3 - -var11], -16777217);
                            }
                        }
                    }
                }
            }
            ByteBuffer var20 = new ByteBuffer(var7);
            byte var8;
            if (!var1) {
                var8 = 4;
            } else {
                var8 = 1;
            }
            int var12;
            for (var10 = 0; var8 > var10; ++var10) {
                for (var11 = 0; ~var11 > -65; ++var11) {
                    for (var12 = 0; 64 > var12; ++var12) {
                        Class167.method2267(var2, var6, var1, var20, var12 - -var3, var5 + var11, (byte) 91, 0, var10);
                    }
                }
            }
            int var14;
            int var15;
            int var17;
            boolean var21;
            int var24;
            for (var21 = false; var20.offset < var20.buffer.length; var21 = true) {
                var11 = var20.readUnsignedByte();
                if (var11 != 129) {
                    --var20.offset;
                    break;
                }
                for (var12 = 0; var12 < 4; ++var12) {
                    byte var13 = var20.getByte();
                    if (0 != var13) {
                        if (1 != var13) {
                            if (var13 == 2 && -1 > ~var12) {
                                var15 = var5 + 64;
                                var24 = var3;
                                var17 = var3 + 64;
                                if (~var15 > -1) {
                                    var15 = 0;
                                } else if (104 <= var15) {
                                    var15 = 104;
                                }

                                if (~var3 <= -1) {
                                    if (-105 >= ~var3) {
                                        var24 = 104;
                                    }
                                } else {
                                    var24 = 0;
                                }

                                if (-1 >= ~var17) {
                                    if (~var17 <= -105) {
                                        var17 = 104;
                                    }
                                } else {
                                    var17 = 0;
                                }

                                var14 = var5;
                                if (var5 >= 0) {
                                    if (104 <= var5) {
                                        var14 = 104;
                                    }
                                } else {
                                    var14 = 0;
                                }

                                while (var15 > var14) {
                                    while (~var24 > ~var17) {
                                        Class136.aByteArrayArrayArray1774[var12][var14][var24] = Class136.aByteArrayArrayArray1774[var12 + -1][var14][var24];
                                        ++var24;
                                    }

                                    ++var14;
                                }
                            }
                        } else {
                            for (var14 = 0; ~var14 > -65; var14 += 4) {
                                for (var15 = 0; var15 < 64; var15 += 4) {
                                    byte var16 = var20.getByte();

                                    for (var17 = var14 + var5; 4 + var5 + var14 > var17; ++var17) {
                                        for (int var18 = var3 + var15; ~var18 > ~(4 + var3 + var15); ++var18) {
                                            if (var17 >= 0 && ~var17 > -105 && 0 <= var18 && -105 < ~var18) {
                                                Class136.aByteArrayArrayArray1774[var12][var17][var18] = var16;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        var14 = var5;
                        if (var5 >= 0) {
                            if (~var5 <= -105) {
                                var14 = 104;
                            }
                        } else {
                            var14 = 0;
                        }

                        var24 = var3;
                        if (-1 < ~var3) {
                            var24 = 0;
                        } else if (~var3 <= -105) {
                            var24 = 104;
                        }

                        var15 = 64 + var5;
                        var17 = var3 + 64;
                        if (-1 >= ~var17) {
                            if (var17 >= 104) {
                                var17 = 104;
                            }
                        } else {
                            var17 = 0;
                        }

                        if (-1 < ~var15) {
                            var15 = 0;
                        } else if (var15 >= 104) {
                            var15 = 104;
                        }

                        while (~var14 > ~var15) {
                            while (var24 < var17) {
                                Class136.aByteArrayArrayArray1774[var12][var14][var24] = 0;
                                ++var24;
                            }

                            ++var14;
                        }
                    }
                }
            }

            if (var4 == 4) {
                int var23;
                if (Class1012.aBoolean_617 && !var1) {
                    Class930 var22 = null;

                    while (~var20.offset > ~var20.buffer.length) {
                        var12 = var20.readUnsignedByte();
                        if (var12 != 0) {
                            if (~var12 != -2) {
                                throw new IllegalStateException();
                            }

                            var23 = var20.readUnsignedByte();
                            if (0 < var23) {
                                for (var14 = 0; var23 > var14; ++var14) {
                                    Class983 var25 = new Class983(var20);
                                    if (-32 == ~var25.anInt705) {
                                        Class57 var26 = Class57.method1401(var20.aInteger233());
                                        var25.method1060((byte) -67, var26.anInt896, var26.anInt908, var26.anInt899, var26.anInt907);
                                    }

                                    var25.anInt708 += var3 << 7;
                                    var25.anInt703 += var5 << 7;
                                    var17 = var25.anInt708 >> 7;
                                    var24 = var25.anInt703 >> 7;
                                    if (~var24 <= -1 && 0 <= var17 && ~var24 > -105 && ~var17 > -105) {
                                        var25.aBoolean696 = 0 != (Class9.groundArray[1][var24][var17] & 2);
                                        var25.anInt697 = Class1134.activeTileHeightMap[var25.anInt704][var24][var17] + -var25.anInt697;
                                        Class977.method1264(var25);
                                    }
                                }
                            }
                        } else {
                            var22 = new Class930(var20);
                        }
                    }

                    if (var22 == null) {
                        var22 = new Class930();
                    }

                    for (var12 = 0; -9 < ~var12; ++var12) {
                        for (var23 = 0; -9 < ~var23; ++var23) {
                            var14 = var12 + (var5 >> 3);
                            var15 = (var3 >> 3) + var23;
                            if (0 <= var14 && var14 < 13 && -1 >= ~var15 && ~var15 > -14) {
                                Class115.aClass930ArrayArray1581[var14][var15] = var22;
                            }
                        }
                    }
                }

                if (!var21) {
                    for (var11 = 0; var11 < 4; ++var11) {
                        for (var12 = 0; 16 > var12; ++var12) {
                            for (var23 = 0; var23 < 16; ++var23) {
                                var14 = (var5 >> 2) - -var12;
                                var15 = var23 + (var3 >> 2);
                                if (0 <= var14 && 26 > var14 && 0 <= var15 && var15 < 26) {
                                    Class136.aByteArrayArrayArray1774[var11][var14][var15] = 0;
                                }
                            }
                        }
                    }
                }

            }
        } catch (RuntimeException var19) {
            throw Class1134.method1067(var19, "wa.OA(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + (var7 != null ? "{...}" : "null") + ')');
        }
    }

    final int aNon145() {
        int var2 = buffer[offset] & 255;
        return 128 <= var2 ? aInteger233() - 32768 : readUnsignedByte();
    }

    final void aLong_4502(int var1) {
        buffer[offset++] = (byte) (var1 >> 16);
        buffer[offset++] = (byte) (var1 >> 8);
        buffer[offset++] = (byte) var1;
    }

    final int aLong5882() {
        offset += 4;
        return ((buffer[offset - 2] & 255) << 24) + ((255 & buffer[offset - 1]) << 16) + ('\uff00' & buffer[-4 + offset] << 8) - -(buffer[offset + -3] & 255);
    }

    final int aBoole100() {
        offset += 2;
        return (buffer[offset - 1] << 8 & '\uff00') + (255 & -128 + buffer[offset - 2]);
    }

    final int aBool1002() {
        offset += 4;
        return (255 & buffer[-4 + offset]) + (16711680 & buffer[offset - 2] << 1572599856) + ((255 & buffer[offset + -1]) << -34836040) + ((buffer[-3 + offset] & 255) << 481963272);
    }

    final void aEoor1(int var1) {
        buffer[offset++] = (byte) (var1 >> -702824440);
        buffer[offset++] = (byte) (128 + var1);
    }

    static final Class12 method784(int var0, int var1, int var2) {
        Class949 var3 = Class75_Sub2.class949s[var0][var1][var2];
        return var3 != null && var3.aClass12_2230 != null ? var3.aClass12_2230 : null;
    }

    ByteBuffer(byte[] var1) {
        try {
            offset = 0;
            buffer = var1;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.<init>(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    final void method_12(int var1, byte var2) {
        buffer[offset++] = (byte) var1;
        buffer[offset++] = (byte) (var1 >> -1080682200);
        buffer[offset++] = (byte) (var1 >> 16);
        buffer[offset++] = (byte) (var1 >> 12970328);
    }

    final int gea100() {
        return 255 & 0 + -buffer[offset++];
    }

    final int aLong_1884() {
        offset += 2;
        int var2 = (buffer[-1 + offset] & 255) + ((255 & buffer[offset + -2]) << -2034851416);
        if (~var2 < -32768) {
            var2 -= 65536;
        }
        return var2;
    }

    final int method788(int var1) {
        try {
            offset += 2;
            int var2 = ((buffer[offset - 1] & 255) << 1510012168) - -(buffer[-2 + offset] - 128 & 255);
            if (var1 != -1741292848) {
                aBoolean1056((BigInteger) null, (BigInteger) null, 11);
            }
            if (32767 < var2) {
                var2 -= 65536;
            }
            return var2;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.EC(" + var1 + ')');
        }
    }

    final byte aLong1003() {
        return (byte) (buffer[offset++] - 128);
    }

    final void aInt1033(int var1) {
        buffer[offset++] = (byte) (128 + var1);
    }

    final int method791(byte var1) {
        try {
            offset += 2;
            if (var1 != 10) {
                aInt2016();
            }
            int var2 = (buffer[-2 + offset] & 255) + ('\uff00' & buffer[offset + -1] << 50972264);
            if (var2 > 32767) {
                var2 -= 65536;
            }
            return var2;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.SA(" + var1 + ')');
        }
    }

    static final void method792(int var0) {
        try {
            if (var0 == 9179409) {
                int var1 = Class137.method1817();
                if (0 == var1) {
                    Class923.aByteArrayArrayArray2008 = (byte[][][]) null;
                    Class136.method1816(0, -7);
                } else if (~var1 == -2) {
                    Class3_Sub5.method112((byte) 0, (byte) 55);
                    Class136.method1816(512, -7);
                    Class933.method257((byte) 125);
                } else {
                    Class3_Sub5.method112((byte) (-4 + Class1220.anInt1127 & 255), (byte) 55);
                    Class136.method1816(2, -7);
                }
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "wa.AA(" + var0 + ')');
        }
    }

    final int method793(byte var1, int var2) {
        try {
            if (var1 < 1) {
                return 65;
            } else {
                int var3 = Class99.method1599(var2, offset, buffer);
                method_211(var3);
                return var3;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "wa.QB(" + var1 + ',' + var2 + ')');
        }
    }

    final int aBoolean183() {
        offset += 3;
        return (16711680 & buffer[offset + -3] << -2022440336) + (('\uff00' & buffer[-2 + offset] << -54462168) - -(buffer[offset + -1] & 255));
    }

    static final void method795(byte var0, int var1) {
        try {
            if (var0 != 14) {
                aInteger1259 = (int[]) null;
            }
            Class1048.aClass93_2450.method1522(var1);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.OC(" + var0 + ',' + var1 + ')');
        }
    }

    final void method_122(int var2) {
        buffer[offset++] = (byte) var2;
        buffer[offset++] = (byte) (var2 >> 8);
    }

    final int method_931() {
        int var2 = buffer[offset] & 255;
        return ~var2 > -129 ? -64 + readUnsignedByte() : aInteger233() - '\uc000';
    }

    final int method_152() {
        offset += 4;
        return ((buffer[offset - 3] & 255) << -1597905000) - -(16711680 & buffer[offset + -4] << 861399376) + (((buffer[offset + -1] & 255) << 979767016) - -(255 & buffer[offset + -2]));
    }

    final void aBoolean1056(BigInteger var1, BigInteger var2, int var3) {
        try {
            int var4 = offset;
            offset = 0;
            byte[] var5 = new byte[var4];
            getOutOurCode(var5, 0, var4);
            BigInteger var6 = new BigInteger(var5);
            BigInteger var7 = null;
            var7 = var6.modPow(var1, var2);
            byte[] var8 = var7.toByteArray();
            offset = 0;
            aBlowMe(var8.length);
            method753(var8, 0, var8.length, var3 + 348);
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "wa.KB(" + (var1 != null ? "{...}" : "null") + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    final void method801(float var2) {
        int var3 = Float.floatToRawIntBits(var2);
        buffer[offset++] = (byte) (var3 >> -1164789608);
        buffer[offset++] = (byte) (var3 >> -259929904);
        buffer[offset++] = (byte) (var3 >> 1414718216);
        buffer[offset++] = (byte) var3;
    }

    static final Class1008 getContextOption(int var0, boolean var1) {
        try {
            if (!var1) {
                method746((byte) -33);
            }
            Class1008 js = -1 > ~Class163_Sub2_Sub1.contextOpStrings[var0].getLength() ? Class922.combinejStrings(new Class1008[]{Class1013.aClass94Array2935[var0], Class1016.aClass94_43, Class163_Sub2_Sub1.contextOpStrings[var0]}) : Class1013.aClass94Array2935[var0];
            String old = js.toString();
            if (old.contains("Impact")) {
                old = old.replace("Impact", "Smite");
                js = Class943.create(old);
            }
            return js;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "wa.RB(" + var0 + ',' + var1 + ')');
        }
    }

    final int readUnsignedByte() {
        return buffer[offset++] & 0xff;
    }

    final void method_0133(int var2) {
        buffer[offset++] = (byte) (var2 >> 8);
        buffer[offset++] = (byte) var2;
    }

    public int aMethod_593() {
        if (buffer[offset] >= 0) {
            return aInteger233() & 0x7FFF;
        }
        return getInt() & 0x7FFFFFFF;
    }
    
    public int getUnsignedLEShort() {
		offset += 2;
		return ((buffer[offset - 2] & 0xff) << 8) + (buffer[offset - 1] & 0xff);
	}
    
    public int getSmartA() {
		int value = buffer[offset] & 0xff;
		if (value < 128) {
			return readUnsignedByte() - 64;
		}
		return getUnsignedLEShort() - 49152;
	}
}

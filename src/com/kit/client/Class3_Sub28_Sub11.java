package com.kit.client;

final class Class3_Sub28_Sub11 extends Class1002 {

    protected Class1017_2 aClass130_3636;
    protected static boolean aBoolean3641 = false;
    protected static int anInt3642 = 0;
    protected static Class1008 aClass94_3643 = Class943.create("Loading fonts )2 ");
    protected static int anInt3644 = 0;
    protected static Class1008 aClass94_3645 = Class943.create(" is already on your friend list)3");

    static final int method599(int var0, Class1027 var1) {
        int var2 = 0;
        if (var1.method2144(Class154.anInt1966)) {
            ++var2;
        }
        return var2;
    }

    final int method600(int var1, int var2, byte var3) {
        try {
            if (this.aClass130_3636 != null) {
                if (var3 != -29) {
                    this.method604((Class1008) null, 110);
                }

                Class1042_2 var4 = (Class1042_2) this.aClass130_3636.get((long) var1);
                return null == var4 ? var2 : var4.value;
            } else {
                return var2;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "lk.Q(" + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

    private final void method601(ByteBuffer var1, int var2, byte var3) {
        try {
            if (var3 < -2) {
                if (249 == var2) {
                    int var4 = var1.readUnsignedByte();
                    int var5;
                    if (this.aClass130_3636 == null) {
                        var5 = Class95.method1585(var4);
                        this.aClass130_3636 = new Class1017_2(var5);
                    }

                    for (var5 = 0; var4 > var5; ++var5) {
                        boolean var6 = 1 == var1.readUnsignedByte();
                        int var7 = var1.aBoolean183();
                        Object var8;
                        if (!var6) {
                            var8 = new Class1042_2(var1.getInt());
                        } else {
                            var8 = new Class1030(var1.class_91033());
                        }

                        this.aClass130_3636.put((Class1042) var8, (long) var7);
                    }
                }

            }
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "lk.P(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ')');
        }
    }

    static final void method603(int height, int var1, int width, Class1034 inter, boolean var4) {
        try {
            int width2 = inter.scrollbarWidth;
            int height2 = inter.scrollbarHeight;
            if (var1 != 13987) {
                Class922.method602(-115, 65, (Class1027) null);
            }

            if (-1 != ~inter.aByte304) {
                if (~inter.aByte304 != -2) {
                    if (~inter.aByte304 == -3) {
                        inter.scrollbarWidth = inter.width * width >> 14;
                    } else if (inter.aByte304 == 3) {
                        if (~inter.type != -3) {
                            if (inter.type == 7) {
                                inter.scrollbarWidth = 115 * inter.width + inter.invSpritePadX * (-1 + inter.width);
                            }
                        } else {
                            inter.scrollbarWidth = inter.width * 32 - -((inter.width - 1) * inter.invSpritePadX);
                        }
                    }
                } else {
                    inter.scrollbarWidth = width + -inter.width;
                }
            } else {
                inter.scrollbarWidth = inter.width;
            }

            if (-1 == ~inter.aByte241) {
                inter.scrollbarHeight = inter.height;
            } else if (inter.aByte241 == 1) {
                inter.scrollbarHeight = -inter.height + height;
            } else if (~inter.aByte241 == -3) {
                inter.scrollbarHeight = height * inter.height >> 14;
            } else if (inter.aByte241 == 3) {
                if (~inter.type == -3) {
                    inter.scrollbarHeight = (inter.height + -1) * inter.invSpritePadY + inter.height * 32;
                } else if (~inter.type == -8) {
                    inter.scrollbarHeight = inter.height * 12 + (-1 + inter.height) * inter.invSpritePadY;
                }
            }

            if (-5 == ~inter.aByte304) {
                inter.scrollbarWidth = inter.anInt216 * inter.scrollbarHeight / inter.anInt160;
            }

            if (inter.aByte241 == 4) {
                inter.scrollbarHeight = inter.anInt160 * inter.scrollbarWidth / inter.anInt216;
            }

            if (Class1043.qaoptestEnabled && (-1 != ~Class1034.getInterfaceClickMask(inter).clickMask || ~inter.type == -1)) {

                if (inter.scrollbarHeight < 5 && 5 > inter.scrollbarWidth) {
                    inter.scrollbarHeight = 5;
                    inter.scrollbarWidth = 5;
                } else {
                    if (~inter.scrollbarWidth >= -1) {
                        inter.scrollbarWidth = 5;
                    }

                    if (0 >= inter.scrollbarHeight) {
                        inter.scrollbarHeight = 5;
                    }
                }
            }

            if (1337 == inter.clientCode) {
                Class1223.aClass11_2091 = inter;
            }

            if (var4 && null != inter.anObjectArray235 && (~width2 != ~inter.scrollbarWidth || inter.scrollbarHeight != height2)) {
                Class1048 var7 = new Class1048();
                var7.objectData = inter.anObjectArray235;
                var7.aClass11_2449 = inter;
                Class110.aClass61_1471.insertBack(var7);
            }

        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "lk.E(" + height + ',' + var1 + ',' + width + ',' + (inter != null ? "{...}" : "null") + ',' + var4 + ')');
        }
    }

    final Class1008 method604(Class1008 var1, int var3) {
        if (this.aClass130_3636 == null) {
            return var1;
        } else {
            Class1030 var4 = (Class1030) this.aClass130_3636.get((long) var3);
            return null != var4 ? var4.value : var1;
        }
    }

    public static void method605(int var0) {
        try {
            //aClass94_3637 = null;
            aClass94_3643 = null;
            if (var0 != 221301966) {
                method603(-111, -64, -10, (Class1034) null, false);
            }

            aClass94_3645 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "lk.D(" + var0 + ')');
        }
    }

    static final void method606(int var0, Class3_Sub9 var1, int var2, int var3, int var4, int var5) {
        try {
            if (var5 > 44) {
                if (~var1.anInt2332 != 0 || var1.anIntArray2333 != null) {
                    int var6 = 0;
                    if (var1.anInt2321 < var0) {
                        var6 += -var1.anInt2321 + var0;
                    } else if (var1.anInt2326 > var0) {
                        var6 += var1.anInt2326 - var0;
                    }

                    if (var1.anInt2307 >= var4) {
                        if (var4 < var1.anInt2308) {
                            var6 += -var4 + var1.anInt2308;
                        }
                    } else {
                        var6 += -var1.anInt2307 + var4;
                    }

                    if (0 != var1.anInt2328 && ~var1.anInt2328 <= ~(var6 - 64) && 0 != Class14.areaSoundsVolume && var2 == var1.anInt2314) {
                        var6 -= 64;
                        if (var6 < 0) {
                            var6 = 0;
                        }

                        int var7 = (-var6 + var1.anInt2328) * Class14.areaSoundsVolume / var1.anInt2328;
                        if (var1.aClass3_Sub24_Sub1_2312 == null) {
                            if (-1 >= ~var1.anInt2332) {
                                Class135 var8 = Class135.method1811(Class961.cacheIndex4, var1.anInt2332, 0);
                                if (null != var8) {
                                    Class3_Sub12_Sub1 var9 = var8.method1812().method151(Class27.aClass157_524);
                                    Class3_Sub24_Sub1 var10 = Class3_Sub24_Sub1.method437(var9, 100, var7);
                                    var10.method429(-1);
                                    Class3_Sub26.aClass3_Sub24_Sub2_2563.method457(var10);
                                    var1.aClass3_Sub24_Sub1_2312 = var10;
                                }
                            }
                        } else {
                            var1.aClass3_Sub24_Sub1_2312.method419(var7);
                        }

                        if (null != var1.aClass3_Sub24_Sub1_2315) {
                            var1.aClass3_Sub24_Sub1_2315.method419(var7);
                            if (!var1.aClass3_Sub24_Sub1_2315.method82(0)) {
                                var1.aClass3_Sub24_Sub1_2315 = null;
                            }
                        } else if (var1.anIntArray2333 != null && ~(var1.anInt2316 -= var3) >= -1) {
                            int var13 = (int) ((double) var1.anIntArray2333.length * Math.random());
                            Class135 var14 = Class135.method1811(Class961.cacheIndex4, var1.anIntArray2333[var13], 0);
                            if (null != var14) {
                                Class3_Sub12_Sub1 var15 = var14.method1812().method151(Class27.aClass157_524);
                                Class3_Sub24_Sub1 var11 = Class3_Sub24_Sub1.method437(var15, 100, var7);
                                var11.method429(0);
                                Class3_Sub26.aClass3_Sub24_Sub2_2563.method457(var11);
                                var1.anInt2316 = (int) ((double) (-var1.anInt2310 + var1.anInt2325) * Math.random()) + var1.anInt2310;
                                var1.aClass3_Sub24_Sub1_2315 = var11;
                            }
                        }

                    } else {
                        if (null != var1.aClass3_Sub24_Sub1_2312) {
                            Class3_Sub26.aClass3_Sub24_Sub2_2563.method461(var1.aClass3_Sub24_Sub1_2312);
                            var1.aClass3_Sub24_Sub1_2312 = null;
                        }

                        if (var1.aClass3_Sub24_Sub1_2315 != null) {
                            Class3_Sub26.aClass3_Sub24_Sub2_2563.method461(var1.aClass3_Sub24_Sub1_2315);
                            var1.aClass3_Sub24_Sub1_2315 = null;
                        }

                    }
                }
            }
        } catch (RuntimeException var12) {
            throw Class1134.method1067(var12, "lk.O(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
        }
    }

    final void method608(ByteBuffer var2) {
        while (true) {
            int var3 = var2.readUnsignedByte();
            if (0 == var3) {
                return;
            }

            this.method601(var2, var3, (byte) -5);
        }
    }

}

package com.kit.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

final class Class980 implements MouseListener, MouseMotionListener, FocusListener, MouseWheelListener {

    protected static float[] aFloatArray1919 = new float[4];
    protected static int[] anIntArray1920;
    protected static int anInt1921 = 0;
    protected static int anInt1923;
    protected static int anInt1924 = 0;
    protected static int weigth = 0;
    protected static int anInt1926;
    protected static int anInt1927 = 0;

    private int mouseWheelX;
    private int mouseWheelY;
    //fix clicking on fullscreen

    public final synchronized void mouseMoved(MouseEvent var1) {
        try {
            if (Class1225.class980 != null) {
                Class1225.anInt4045 = 0;
                Class922.mouseX = var1.getX();
                Class922.mouseY = var1.getY();
            }


        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mouseMoved(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    static final void method2087() {
        for (Class990 var1 = (Class990) Class3_Sub13_Sub6.aClass61_3075.getFirst(); null != var1; var1 = (Class990) Class3_Sub13_Sub6.aClass61_3075.getNext()) {
            if (var1.anInt2259 > 0) {
                --var1.anInt2259;
            }

            if (0 != var1.anInt2259) {
                if (~var1.anInt2261 < -1) {
                    --var1.anInt2261;
                }

                if (~var1.anInt2261 == -1 && 1 <= var1.anInt2264 && 1 <= var1.anInt2248 && 102 >= var1.anInt2264 && ~var1.anInt2248 >= -103 && (~var1.id > -1 || Class3_Sub28_Sub10.method590((byte) -34, var1.id, var1.anInt2262))) {
                    Class988.method1048(var1.id, var1.anInt2264, var1.plane, var1.anInt2256, var1.anInt2248, -65, var1.anInt2262, var1.anInt2263);
                    var1.anInt2261 = -1;
                    if (~var1.anInt2254 == ~var1.id && var1.anInt2254 == -1) {
                        var1.unlink();
                    } else if (~var1.id == ~var1.anInt2254 && var1.anInt2256 == var1.anInt2257 && ~var1.anInt2253 == ~var1.anInt2262) {
                        var1.unlink();
                    }
                }
            } else if (-1 < ~var1.anInt2254 || Class3_Sub28_Sub10.method590((byte) -66, var1.anInt2254, var1.anInt2253)) {
                Class988.method1048(var1.anInt2254, var1.anInt2264, var1.plane, var1.anInt2257, var1.anInt2248, -71, var1.anInt2253, var1.anInt2263);
                var1.unlink();
            }
        }

    }

    public static void method2088(boolean var0) {
        try {
            anIntArray1920 = null;
            if (!var0) {
                anIntArray1920 = (int[]) null;
            }

            aFloatArray1919 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ug.C(" + var0 + ')');
        }
    }

    public final synchronized void focusLost(FocusEvent var1) {
        try {
            if (Class1225.class980 != null) {
                Class1211.anInt549 = 0;
            }

        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.focusLost(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    static final void method2089(byte var0) {
        try {
            Class999.aClass93_2982.clearSoftReference();
            if (var0 >= 84) {
                ;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ug.D(" + var0 + ')');
        }
    }

    public final synchronized void mouseDragged(MouseEvent var1) {
        try {

            if (middleMouseButton && Class929.middleMouse) {
                int y = mouseWheelX - var1.getX();
                int k = mouseWheelY - var1.getY();
                mouseWheelDragged(y, -k);
                mouseWheelX = var1.getX();
                mouseWheelY = var1.getY();
                return;
            }
            if (null != Class1225.class980) {
                Class1225.anInt4045 = 0;
                Class922.mouseX = var1.getX();
                Class922.mouseY = var1.getY();
            }


        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mouseDragged(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    private void mouseWheelDragged(int i, int j) {
        if (!middleMouseButton || !Class929.middleMouse)
            return;
        Class1211.cameraYaw += i * 3;
        Class3_Sub9.anInt2309 += (j << 1);
    }

    protected boolean middleMouseButton = false;

    public final synchronized void mouseReleased(MouseEvent var1) {
        try {
            if (null != Class1225.class980) {
                Class1225.anInt4045 = 0;
                Class1211.anInt549 = 0;
                int var2 = var1.getModifiers();
                if (0 == (16 & var2)) {
                    ;
                }

                if (-1 == ~(var2 & 4)) {
                    ;
                }

                if (-1 == ~(var2 & 8)) {
                    ;
                }
                middleMouseButton = false;
                //   leftMouseDown = false;
            }

            //TODO: Adjust for fullscreen
            if (Class922.aInteger_544 == 10) {
                if (var1.getX() >= 277 && var1.getX() <= 492
                        && var1.getY() >= 214 && var1.getY() <= 239) {
                    try {
                        if (Class922.inputBox == 2)
                            Class1005.pressEnter();
                    } catch (AWTException e) {
                        e.printStackTrace();
                    }
                } else if (var1.getX() >= 277 && var1.getX() <= 492
                        && var1.getY() >= 267 && var1.getY() <= 293) {
                    try {
                        if (Class922.inputBox == 1)
                            Class1005.pressEnter();
                    } catch (AWTException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (var1.isPopupTrigger()) {
                var1.consume();
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mouseReleased(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    public final void mouseClicked(MouseEvent var1) {
        try {
            if (var1.isPopupTrigger()) {
                var1.consume();
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mouseClicked(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    public final void focusGained(FocusEvent var1) {
    }

    static final void method2090(int var0) {
        try {
            if (Class1225.class980 != null) {
                Class980 var1 = Class1225.class980;
                synchronized (var1) {
                    Class1225.class980 = null;
                }
            }
            if (var0 == 8) {
                ;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ug.F(" + var0 + ')');
        }
    }

    public final synchronized void mousePressed(MouseEvent var1) {
        try {
            if (Class1225.class980 != null) {
                Class1225.anInt4045 = 0;
                Class922.anInt362 = var1.getX();
                Class3_Sub13_Sub32.anInt3389 = var1.getY();
                Class140_Sub6.clickTime = Class1219.currentTimeMillis();
                if (SwingUtilities.isLeftMouseButton(var1)) {
                    Class922.aInteger_514 = var1.getX();
                    Class922.aInteger_513 = var1.getY();
                }
                if (SwingUtilities.isMiddleMouseButton(var1) && Class929.middleMouse) {
                    middleMouseButton = true;
                    mouseWheelX = var1.getX();
                    mouseWheelY = var1.getY();
                    return;
                }
                if (!var1.isMetaDown()) {
                    Class140_Sub3.anInt2743 = 1;
                    Class1211.anInt549 = 1;
                } else {
                    Class140_Sub3.anInt2743 = 2;
                    Class1211.anInt549 = 2;
                }

                int var2 = var1.getModifiers();
                if ((var2 & 16) == 0) {
                    ;
                }

                if (~(4 & var2) != -1) {
                    ;
                }

                if (-1 != ~(var2 & 8)) {
                    ;
                }
            }
            if (var1.isPopupTrigger()) {
                var1.consume();
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mousePressed(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    public final synchronized void mouseExited(MouseEvent var1) {
        try {
            if (Class1225.class980 != null) {
                Class1225.anInt4045 = 0;
                Class922.mouseX = -1;
                Class922.mouseY = -1;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mouseExited(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    static final void method2091(int var0, int var1) {
        try {
            if (Class1031.anIntArray1838 == null || Class1031.anIntArray1838.length < var0) {
                Class1031.anIntArray1838 = new int[var0];
            }
            if (var1 != 4) {
                anInt1926 = -75;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.E(" + var0 + ',' + var1 + ')');
        }
    }

    public final synchronized void mouseEntered(MouseEvent var1) {
        try {
            if (Class1225.class980 != null) {
                Class1225.anInt4045 = 0;
                Class922.mouseX = var1.getX();
                Class922.mouseY = var1.getY();
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ug.mouseEntered(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    static final void method2092(int var0) {
        Class1042_3 var2 = Class3_Sub24_Sub3.putInterfaceChange(9, var0);
        var2.a();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent data) {
    }
}

package com.kit.client;

import java.io.IOException;

class Class163_Sub1 extends Class163 {

   static Class93 cursorCache = new Class93(2);
   static int[] variousSettings = new int[2500];
   static long[] aLongArray2986 = new long[32];
   static byte[][] spritePaletteIndicators;
   private static Class1008 aClass94_2988 = Class943.create("glow3:");
   static int anInt2989 = 0;
   static Class975 tiles = new Class975();
   static Class1008 aClass94_2991 = aClass94_2988;
   static Class1008 aClass94_2992 = aClass94_2988;
   static int clickX2 = 0;
   static int anInt2994;

   static final void ping(boolean force) {
	   Class58.method1194();
	   if(30 == Class922.aInteger_544 || Class922.aInteger_544 == 25) {
		   Class3_Sub13_Sub23_Sub1.pingTimer++;
		   if(Class3_Sub13_Sub23_Sub1.pingTimer >= 50 || force) {
			  // System.out.println("Ping timer >= 50 || force logout.");
			   Class3_Sub13_Sub23_Sub1.pingTimer = 0;
			   if(!Class3_Sub28_Sub18.errorPinging && Class3_Sub15.worldConnection != null) {
				   Class3_Sub13_Sub1.outputStream.putPacket(202);
				   try {
					   Class3_Sub15.worldConnection.write(Class3_Sub13_Sub1.outputStream.buffer, 0, Class3_Sub13_Sub1.outputStream.offset);
					   Class3_Sub13_Sub1.outputStream.offset = 0;
				   } catch (IOException var3) {
					   Class3_Sub28_Sub18.errorPinging = true;
				   }
			   }	   
			   Class58.method1194();
		   }
	   }
   }

   static final void method2211(int var0) {
      try {
         if(var0 == -48) {
            if(null == Class67.aClass11_1017) {
               if(null == Class56.aClass11_886) {
                  int var1 = Class3_Sub28_Sub11.anInt3644;
                  int var3;
                  int var4;
                  if(!Class38_Sub1.drawContextMenu) {
                     if(~var1 == -2 && 0 < Class3_Sub13_Sub34.contextOptionsAmount) {
                        short var2 = Class3_Sub13_Sub7.aShortArray3095[-1 + Class3_Sub13_Sub34.contextOptionsAmount];
                        if(-26 == ~var2 || var2 == 23 || 48 == var2 || ~var2 == -8 || 13 == var2 || ~var2 == -48 || -6 == ~var2 || var2 == 43 || -36 == ~var2 || ~var2 == -59 || ~var2 == -23 || var2 == 1006) {
                           var3 = Class117.anIntArray1613[-1 + Class3_Sub13_Sub34.contextOptionsAmount];
                           var4 = Class27.anIntArray512[Class3_Sub13_Sub34.contextOptionsAmount + -1];
                           Class1034 var5 = Class7.getInterface(var4);
                           Class971 var6 = Class1034.getInterfaceClickMask(var5);
                           if(var6.method100((byte)-9) || var6.method93(572878952)) {
                        	   //This is what happens when you click an item?
                        	  // System.out.println("RESET DRAG?");
                              Class40.mouseDownTimer = 0;
                              Class72.isDrag = false;
                              if(Class67.aClass11_1017 != null) {
                                 Class20.refreshInterface(Class67.aClass11_1017);
                              }

                              Class67.aClass11_1017 = Class7.getInterface(var4);
                              Class1210.anInt2693 = clickX2;
                              Class1016.anInt40 = Class38_Sub1.clickY2;
                              Class979.oldSlot = var3;
                              Class20.refreshInterface(Class67.aClass11_1017);
                              return;
                           }
                        }
                     }

                     if(-2 == ~var1 && (-2 == ~Class1006.anInt998 && 2 < Class3_Sub13_Sub34.contextOptionsAmount || Class3_Sub13_Sub39.method353(Class3_Sub13_Sub34.contextOptionsAmount + -1, 0))) {
                        var1 = 2;
                     }

                     if(~var1 == -3 && -1 > ~Class3_Sub13_Sub34.contextOptionsAmount || -2 == ~Class992.anInt3660) {
                        Class132.method1801((byte)-105);
                     }

                     if(1 == var1 && Class3_Sub13_Sub34.contextOptionsAmount > 0 || -3 == ~Class992.anInt3660) {
                        Class3_Sub13_Sub8.handleClick(100);
                     }
                  } else {
                     int var11;
                     if(-2 != ~var1) {
                        var3 = Class1017_2.anInt1709;
                        var11 = Class1015.mouseX2;
                        if(~var11 > ~(Class927.anInt1462 - 10) || ~var11 < ~(Class3_Sub28_Sub3.anInt3552 + (Class927.anInt1462 - -10)) || ~(-10 + Class3_Sub13_Sub33.anInt3395) < ~var3 || ~var3 < ~(Class3_Sub28_Sub1.anInt3537 + (Class3_Sub13_Sub33.anInt3395 - -10))) {
                           Class38_Sub1.drawContextMenu = false;
                           Class75.method1340(Class927.anInt1462, Class3_Sub28_Sub3.anInt3552, Class3_Sub13_Sub33.anInt3395, Class3_Sub28_Sub1.anInt3537);
                        }
                     }

                     if(-2 == ~var1) {
                        var11 = Class927.anInt1462;
                        var3 = Class3_Sub13_Sub33.anInt3395;
                        var4 = Class3_Sub28_Sub3.anInt3552;
                        int var12 = clickX2;
                        int var13 = Class38_Sub1.clickY2;
                        int var7 = -1;

                        for(int var8 = 0; var8 < Class3_Sub13_Sub34.contextOptionsAmount; ++var8) {
                           int var9;
                           if(Class1027.aBoolean1951) {
                              var9 = 15 * (Class3_Sub13_Sub34.contextOptionsAmount + -1 + -var8) + 35 + var3;
                           } else {
                              var9 = 15 * (-var8 + (Class3_Sub13_Sub34.contextOptionsAmount - 1)) + var3 + 31;
                           }

                           if(~var12 < ~var11 && ~(var11 - -var4) < ~var12 && var9 + -13 < var13 && ~(3 + var9) < ~var13) {
                              var7 = var8;
                           }
                        }

                        if(~var7 != 0) {
                           Class966_2.method806(2597, var7);
                        }

                        Class38_Sub1.drawContextMenu = false;
                        Class75.method1340(Class927.anInt1462, Class3_Sub28_Sub3.anInt3552, Class3_Sub13_Sub33.anInt3395, Class3_Sub28_Sub1.anInt3537);
                     }
                  }

               }
            }
         }
      } catch (RuntimeException var10) {
         throw Class1134.method1067(var10, "ah.A(" + var0 + ')');
      }
   }

   public static void method2212(boolean var0) {
      try {
         variousSettings = null;
         aClass94_2988 = null;
         cursorCache = null;
         tiles = null;
         aLongArray2986 = null;
         if(var0) {
            method2211(68);
         }

         aClass94_2992 = null;
         spritePaletteIndicators = (byte[][])null;
         aClass94_2991 = null;
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "ah.C(" + var0 + ')');
      }
   }

}

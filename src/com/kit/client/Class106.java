package com.kit.client;

final class Class106 {

    protected static boolean aBoolean1441 = true;
    protected static int anInt1442 = 0;
    protected static Class67 aClass67_1443;
    protected static short aShort1444 = 256;
    protected static int anInt1446 = 0;
    protected int anInt1447;
    protected int anInt1448;
    protected int anInt1449;
    protected int anInt1450;
    protected static boolean aBoolean1451 = false;

    static final void method1642(int var0, Class1008 var1) {
        try {
            if (null != Class951.clanMembers) {
                if (var0 != 3803) {
                    aClass67_1443 = (Class67) null;
                }
                long var3 = var1.toLong();
                int var2 = 0;
                if (var3 != 0L) {
                    while (Class951.clanMembers.length > var2 && ~Class951.clanMembers[var2].hash != ~var3) {
                        ++var2;
                    }

                    if (var2 < Class951.clanMembers.length && null != Class951.clanMembers[var2]) {
                        Class3_Sub13_Sub1.outputStream.putPacket(162);
                        Class3_Sub13_Sub1.outputStream.aInt233(Class951.clanMembers[var2].hash);
                    }
                }
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "od.C(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    static final int method1643(int var0, boolean var1, int var2, int var3) {
        try {
            if (var0 != 10131) {
                method1644((byte) 95);
            }

            Class1042_4 var4 = (Class1042_4) Class949.aClass130_2220.get((long) var2);
            if (null != var4) {
                int var5 = 0;

                for (int var6 = 0; ~var4.anIntArray2547.length < ~var6; ++var6) {
                    if (var4.anIntArray2547[var6] >= 0 && Class3_Sub13_Sub23.anInt3287 > var4.anIntArray2547[var6]) {
                        ItemDefinition var7 = ItemDefinition.getDefinition(var4.anIntArray2547[var6]);
                        if (null != var7.aClass130_798) {
                            Class1042_2 var8 = (Class1042_2) var7.aClass130_798.get((long) var3);
                            if (null != var8) {
                                if (var1) {
                                    var5 += var4.anIntArray2551[var6] * var8.value;
                                } else {
                                    var5 += var8.value;
                                }
                            }
                        }
                    }
                }
                return var5;
            } else {
                return 0;
            }
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "od.B(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

    public static void method1644(byte var0) {
        try {
            aClass67_1443 = null;
            if (var0 != 121) {
                aClass67_1443 = (Class67) null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "od.A(" + var0 + ')');
        }
    }
}

package com.kit.client;

import java.awt.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

final class Class1051 extends Class961 implements MouseWheelListener {

	protected  int rotation = 0;

	final void removeMouseWheel(Component var2) {
		var2.removeMouseWheelListener(this);
	}

	final synchronized int getRotation() {
		int var2 = this.rotation;
		this.rotation = 0;
		return var2;
	}

	public final synchronized void mouseWheelMoved(MouseWheelEvent var1) {
		if (Class1143.mainScreenInterface < -1){
			return;
		}
		if (Class922.mouseX >= 0 && Class922.mouseX <= 516 && Class922.mouseY >= 0 && Class922.mouseY <= 342) {
			int zoom = Class922.cameraZoom + 600;
			zoom += var1.getWheelRotation();
			if (zoom > 607) {
				zoom = 607;
			}
			if (zoom < 599) {
				zoom = 599;
			}
			Class922.cameraZoom = zoom - 600;
		}
		this.rotation += var1.getWheelRotation();
	}

	final void addMouseWheel(Component var1) {
		var1.addMouseWheelListener(this);
	}

}

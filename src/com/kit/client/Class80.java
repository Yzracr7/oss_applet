package com.kit.client;

import java.awt.*;

final class Class80 {

   private Class1042 aClass3_1130;
   static Class93 aClass93_1131 = new Class93(5);
   private int current = 0;
   static Class1008 aClass94_1133 = Class943.create(")4j");
   private Class1017_2 aClass130_1134;
   static Class93 aClass93_1135 = new Class93(4);
   static Class957[] mapIconsArray;
   static int anInt1137 = 2;
   static int[] anIntArray1138;


   final Class1042 getNext() {
         Class1042 var2;
         if(-1 > ~this.current && this.aClass130_1134.method_122[this.current + -1] != this.aClass3_1130) {
            var2 = this.aClass3_1130;
            this.aClass3_1130 = var2.next;
            return var2;
         } else {
            do {
               if(~this.aClass130_1134.method_713 >= ~this.current) {
                  return null;
               }

               var2 = this.aClass130_1134.method_122[this.current++].next;
            } while(var2 == this.aClass130_1134.method_122[-1 + this.current]);

            this.aClass3_1130 = var2.next;
            return var2;
         }
   }

   final Class1042 getFirst() {
	   this.current = 0;
	   return this.getNext();
   }

	public static void method1394() {
		anIntArray1138 = null;
		aClass93_1131 = null;
		aClass94_1133 = null;
		aClass93_1135 = null;
		mapIconsArray = null;
	}

	static final long method1395(int var0, int var1, int var2) {
		Class949 var3 = Class75_Sub2.class949s[var0][var1][var2];
		return var3 != null && var3.aClass19_2233 != null ? var3.aClass19_2233.aLong428 : 0L;
	}

	static final void method1396() {
		int var2 = Class989.canvasDrawY;
		int var1 = Class84.canvasDrawX;
		int var4 = -Class1013.canvasHei + (Class70.anInt1047 - var2);
		int var3 = -var1 + Class3_Sub9.anInt2334 - Class23.canvasWid;
		if (var1 > 0 || var3 > 0 || var2 > 0 || var4 > 0) {
			try {
				Object var5;
				if (null != Class3_Sub13_Sub10.fullscreenFrame) {
					var5 = Class3_Sub13_Sub10.fullscreenFrame;
				} else if (Class3_Sub13_Sub7.resizableFrame == null) {
					var5 = Class38.gameClass942.thisApplet;
				} else {
					var5 = Class3_Sub13_Sub7.resizableFrame;
				}

				int var7 = 0;
				int var6 = 0;
				if (Class3_Sub13_Sub7.resizableFrame == var5) {
					Insets var8 = Class3_Sub13_Sub7.resizableFrame.getInsets();
					var6 = var8.left;
					var7 = var8.top;
				}

				Graphics var11 = ((Container) var5).getGraphics();
				var11.setColor(Color.black);
				if (var1 > 0) {
					var11.fillRect(var6, var7, var1, Class70.anInt1047);
				}

				if (var2 > 0) {
					var11.fillRect(var6, var7, Class3_Sub9.anInt2334, var2);
				}

				if (var3 > 0) {
					var11.fillRect(-var3 + var6 + Class3_Sub9.anInt2334, var7, var3, Class70.anInt1047);
				}

				if (var4 > 0) {
					var11.fillRect(var6, -var4 + var7 + Class70.anInt1047, Class3_Sub9.anInt2334, var4);
				}
			} catch (Exception var9) {
				;
			}
		}
	}
	
	Class80(Class1017_2 var1) {
	   this.aClass130_1134 = var1;
   }

}
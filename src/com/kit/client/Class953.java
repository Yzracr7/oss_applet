package com.kit.client;

import javax.media.opengl.GL;

final class Class953 {

    protected static float[] light0Position = new float[4];
    private static int lightColor = -1;
    protected static int lightX;
    protected static int lightZ;
    protected static int fogColor = 13156520;
    private static float light1Diffuse = -1.0F;
    private static float light0Diffuse = -1.0F;
    static float[] fogColor_int = new float[4];
    private static float lightModelAmbient;
    private static float[] light1Position = new float[4];
    protected static int defaultLightColorRgb = 16777215;
    private static int fogOffset = -1;
    private static int fogColorRGB = -1;

    static final void applyLightPosition() {
        GL gl = Class1012.gl;
        gl.glLightfv(16384, 4611, light0Position, 0);
        gl.glLightfv(16385, 4611, light1Position, 0);
    }

    static final float getLightColor() {
        return light1Diffuse;
    }

    static final void setLightParams(int color, float ambientMod, float l0Diffuse, float l1Diffuse) {
        if (lightColor != color || lightModelAmbient != ambientMod || light1Diffuse != l0Diffuse || light0Diffuse != l1Diffuse) {
            lightColor = color;
            lightModelAmbient = ambientMod;
            light1Diffuse = l0Diffuse;
            light0Diffuse = l1Diffuse;
            GL gl = Class1012.gl;
            float red = (float) (color >> 16 & 255) / 255.0F;
            float green = (float) (color >> 8 & 255) / 255.0F;
            float blue = (float) (color & 255) / 255.0F;
            float[] lightModelAmbientParams = new float[]{ambientMod * red, ambientMod * green, ambientMod * blue, 1.0F};
            gl.glLightModelfv(2899, lightModelAmbientParams, 0);
            float[] var9 = new float[]{l0Diffuse * red, l0Diffuse * green, l0Diffuse * blue, 1.0F};
            gl.glLightfv(16384, 4609, var9, 0);
            float[] var10 = new float[]{-l1Diffuse * red, -l1Diffuse * green, -l1Diffuse * blue, 1.0F};
            gl.glLightfv(16385, 4609, var10, 0);
        }
    }

    public static void nullLoader() {
        light0Position = null;
        light1Position = null;
        fogColor_int = null;
    }

    static final void setFogParams(int fogCol, int fogOff) {
        if (fogColorRGB != fogCol || fogOffset != fogOff) {
            fogColorRGB = fogCol;
            fogOffset = fogOff;
            GL gl = Class1012.gl;
            byte lowestFogStart = 50;
            short baseFogStart = 3584;
            fogColor_int[0] = (float) (fogCol >> 16 & 255) / 255.0F;
            fogColor_int[1] = (float) (fogCol >> 8 & 255) / 255.0F;
            fogColor_int[2] = (float) (fogCol & 255) / 255.0F;
            gl.glFogi(2917, 9729);
            gl.glFogf(2914, 0.95F);
            gl.glHint(3156, 4353);
            int fogStart = baseFogStart - 1075 - fogOff;
            if (fogStart < lowestFogStart) {
                fogStart = lowestFogStart;
            }

            gl.glFogf(2915, (float) fogStart);
            gl.glFogf(2916, (float) (baseFogStart - 256));
            gl.glFogfv(2918, fogColor_int, 0);
        }
    }

    static final void setLightPosition(float x, float y, float z) {
        if (light0Position[0] != x || light0Position[1] != y || light0Position[2] != z) {
            light0Position[0] = x;
            light0Position[1] = y;
            light0Position[2] = z;
            light1Position[0] = -x;
            light1Position[1] = -y;
            light1Position[2] = -z;
            lightX = (int) (x * 256.0F / y);
            lightZ = (int) (z * 256.0F / y);
        }
    }

    static final int method1510() {
        return lightColor;
    }

    static final void method1511() {
        GL var0 = Class1012.gl;
        var0.glColorMaterial(1028, 5634);
        var0.glEnable(2903);
        float[] var1 = new float[]{0.0F, 0.0F, 0.0F, 1.0F};
        var0.glLightfv(16384, 4608, var1, 0);
        var0.glEnable(16384);
        float[] var2 = new float[]{0.0F, 0.0F, 0.0F, 1.0F};
        var0.glLightfv(16385, 4608, var2, 0);
        var0.glEnable(16385);
        lightColor = -1;
        fogColorRGB = -1;
        method1513();
    }

    static final void setFogColor(float[] params) {
        if (params == null) {
            params = fogColor_int;
        }
        GL gl = Class1012.gl;
        gl.glFogfv(2918, params, 0);
    }

    private static final void method1513() {
        setLightParams(defaultLightColorRgb, 1.1523438F, 0.69921875F, 1.2F);
        setLightPosition(-50.0F, -60.0F, -50.0F);
        setFogParams(fogColor, 0);
    }

    static final float method1514() {
        return lightModelAmbient;
    }

}

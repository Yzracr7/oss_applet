package com.kit.client;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

final class Class1005 implements KeyListener, FocusListener {

    protected static boolean showGroundDecorations = true;
    protected static int anInt1908 = 0;
    protected static Class93 aClass93_1911 = new Class93(260);
    protected static int anInt1912;
    protected static Class1008 scrollbarjString = Class943.create("scrollbar");
    protected static Class1008 scrollbarFsjString = Class943.create("scrollbarfs");
    protected static int anInt1914;
    protected static Class1008 nullString = Class943.create("Null");
    protected static Class1027 aClass153_1916;
    protected static Class1008 aClass94_1917 = Class943.create(" <col=00ff80>");
    protected static int anInt1918 = 0;

    final static int[][] keys = {
            {KeyEvent.VK_ESCAPE, -1},
            {KeyEvent.VK_F1, 4},
            {KeyEvent.VK_F2, 5},
            {KeyEvent.VK_F3, 6},
            {KeyEvent.VK_F4, 7},
            {KeyEvent.VK_F5, 1},
            {KeyEvent.VK_F6, -1},
            {KeyEvent.VK_F7, -1},
            {KeyEvent.VK_F8, -1},
            {KeyEvent.VK_F9, -1},
            {KeyEvent.VK_F10, -1},
    };

    public static boolean checkHotkey(KeyEvent var1) {
        int var15 = var1.getKeyCode();

        for (int[] i : keys)
            if (var15 == i[0]) {
                if (i[1] != -1) {
                    run548Script(i[1]);
                    return true;
                }
            }
        return false;
    }


    static final void run548Script(int child) {
        Class1034 var1 = Class7.getInterface(35913757 + child);
        if (null != var1 && var1.anObjectArray314 != null) {
            Class1048 var3 = new Class1048();
            var3.objectData = var1.anObjectArray314;
            var3.aClass11_2449 = var1;
            Class983.method1065(var3);
            if (Class922.clientSize > 0 && Class922.inventoryHidden) {
                Class7.getInterface(Class922.INVY_CONTAINER).hidden = false;
                Class7.getInterface(Class922.INVY_FRAME).hidden = false;
                Class922.inventoryHidden = false;
            }
            Class922.previousFullscreenTab = 35913976 + child;
        }
    }

    static final void runScript(int child) {
        Class1034 var1 = Class7.getInterface(child);
        if (null != var1 && var1.anObjectArray314 != null) {
            Class1048 var3 = new Class1048();
            var3.objectData = var1.anObjectArray314;
            var3.aClass11_2449 = var1;
            Class983.method1065(var3);
        }
    }


    public static final void pressEnter() throws AWTException {
        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_ENTER);
        r.keyRelease(KeyEvent.VK_ENTER);
    }

    public static final void pressSpace() throws AWTException {
        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_SPACE);
        r.keyRelease(KeyEvent.VK_SPACE);
    }

    public static final void pressBackspace() throws AWTException {
        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_BACK_SPACE);
        r.keyRelease(KeyEvent.VK_BACK_SPACE);
    }

    private boolean blockLogin = false;


    public static String getClipboardContents() {
        String result = "";
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable contents = clipboard.getContents(null);
        boolean hasTransferableText = (contents != null)
                && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
        if (hasTransferableText) {
            try {
                result = (String) contents.getTransferData(DataFlavor.stringFlavor);
            } catch (UnsupportedFlavorException ex) {
                System.out.println(ex);
                ex.printStackTrace();
            } catch (IOException ex) {
                System.out.println(ex);
                ex.printStackTrace();
            }
        }
        //	if (myPrivilege >= 2) {
        return result;
        //	} else {
        //		return "";
        //	}
    }


    public final synchronized void keyPressed(KeyEvent var1) {
        int i = var1.getKeyCode();

        if (Class929.newKeys && Class922.aInteger_544 == 30 && checkHotkey(var1))
            return;
            
            /*if ((var1.isControlDown() && var1.getKeyCode() == KeyEvent.VK_V)) {
                //Class7.getInterface(8978433).disabledText =
            	Class47.toAdd = Class1008.create(Class47.fixJString(getClipboardContents().toString()));
    			//Class922.inputString += Class922.getClipboardContents();
    			//Class922.inputTaken = true;
    		}
    		if ((var1.isControlDown() && var1.getKeyCode() == KeyEvent.VK_X)) {
    		   // Class922.setClipboardContents(Class922.inputString);
    		   // Class922.inputString = "";
    			//Class922.inputTaken = true;
    		}*/
        if (var1.getKeyCode() == KeyEvent.VK_TAB) {
            if (Class922.aInteger_544 == 10) {
                //This is also stopped(so we dont get an invalid char) in keyTyped
                try {
                    blockLogin = true;
                    pressEnter();
                } catch (AWTException e) {
                    e.printStackTrace();
                }
                return;
            } else if (Class922.aInteger_544 == 30) {
                if (Class922.lastPmFrom != null) {
                    System.out.println("Responding to last pm from: " + Class922.lastPmFrom);
                    Class986.method66(Class922.lastPmFrom, 1, 1, 36044801);
                    try {
                        pressSpace();
                        pressBackspace();
                    } catch (Exception e) {

                    }
                } else
                    Class966_2.sendMessage((Class1008) null, Class943.create("You don't have any messages to which you can reply to."), 0);
            }
        }

        if (Class922.aInteger_544 == 0 && (var1.getKeyCode() == KeyEvent.VK_SPACE || var1.getKeyCode() == KeyEvent.VK_ENTER)) {
            if (Class1025.loadingStage >= 30 && !Class929.aBoolean_606)
                Class922.closeSelect();
        }


        if (Class922.aInteger_544 == 10 && var1.getKeyCode() == KeyEvent.VK_ENTER) {
            if (blockLogin)
                Class922.inputBox = Class922.inputBox == 1 ? 2 : 1;
            else {
                if (Class922.inputBox == 1)
                    Class922.inputBox = 2;
                else if (Class922.inputBox == 2) {
                    if (Class922.password.toString().length() != 0 && Class922.username.toString().length() != 0) {
                        if (-11 == ~Class922.aInteger_544 && ~Class3_Sub13_Sub31.anInt3375 == -1 && -1 == ~Class3_Sub13_Sub25.loginStage && ~Class969.anInt23 == -1 && -1 == ~Class983.anInt692) {
                            Class131.handleLoginButton(Class922.username, Class922.password, 0, (byte) -38);
                            Class922.inputBox = Class922.inputBox == 1 ? 2 : 1;
                            return;
                        }
                    } else
                        Class922.inputBox = 1;
                }
            }
        }

        blockLogin = false;

        if (Class922.aInteger_544 == 30) {
            //TODO enterInterfaceUp should account for enter Text interface somehow
            boolean enterInterfaceUp = Class7.getInterface(35913809).disabledText.toString().length() > 1
                    && Class7.getInterface(35913808).disabledText.toString().toLowerCase().contains("amount");
            if (enterInterfaceUp) {
                if (var1.getKeyCode() == KeyEvent.VK_B || var1.getKeyCode() == KeyEvent.VK_M || var1.getKeyCode() == KeyEvent.VK_K) {
                    if (Class922.amountModifier.toString().length() == 0) {
                        System.out.println("added modifier " + var1.getKeyChar());
                        Class922.amountModifier = Class943.create("" + var1.getKeyChar());
                    }
                } else if (var1.getKeyCode() == KeyEvent.VK_BACK_SPACE && Class922.amountModifier != Class922.BLANK_CLASS_1008) {
                    Class922.amountModifier = Class922.BLANK_CLASS_1008;
                    System.out.println("removed modifier");
                    return; //return to avoid CS2 removal of final digit
                }
            } else {
                Class922.amountModifier = Class922.BLANK_CLASS_1008; //enter without anything
            }
        }

        if (0 <= i && Class117.keycodes.length > i) {
            i = Class117.keycodes[i];
            if (0 != (i & 128)) {
                i = -1;
            }
        } else {
            i = -1;
        }

        if (-1 >= ~CanvasBuffer.anInt2384 && -1 >= ~i) {
            Class1009.anIntArray2952[CanvasBuffer.anInt2384] = i;
            CanvasBuffer.anInt2384 = 127 & CanvasBuffer.anInt2384 - -1;
            if (CanvasBuffer.anInt2384 == Class974.anInt1744) {
                CanvasBuffer.anInt2384 = -1;
            }
        }

        int var3;
        if (~i <= -1) {
            var3 = 127 & 1 + Class25.anInt491;
            if (var3 != Class3_Sub28_Sub9.anInt3620) {
                Class963.anIntArray1693[Class25.anInt491] = i;
                Class155.anIntArray1978[Class25.anInt491] = -1;
                Class25.anInt491 = var3;
            }
        }

        var3 = var1.getModifiers();
        if (-1 != ~(var3 & 10) || 85 == i || -11 == ~i) {
            var1.consume();
        }
    }

    public static int characterCount = 0;

    public final void keyTyped(KeyEvent var1) {
        if (var1.getKeyChar() == KeyEvent.VK_TAB) {
            if (Class922.aInteger_544 == 10) {
                return;
            }
        }

        if (Class3_Sub13_Sub3.aClass148_3049 != null) {
            int var2 = Class1220.method1386(true, var1);
            if (-1 >= ~var2) {
                int var3 = 1 + Class25.anInt491 & 127;
                if (~Class3_Sub28_Sub9.anInt3620 != ~var3) {
                    Class963.anIntArray1693[Class25.anInt491] = -1;
                    Class155.anIntArray1978[Class25.anInt491] = var2;
                    Class25.anInt491 = var3;
                }
            }
        }

        var1.consume();
    }

    public final synchronized void focusLost(FocusEvent var1) {
        if (null != Class3_Sub13_Sub3.aClass148_3049) {
            CanvasBuffer.anInt2384 = -1;
        }

    }

    public final synchronized void keyReleased(KeyEvent var1) {
        if (var1.getKeyCode() == KeyEvent.VK_TAB) {
            //So we don't get invalid password char
            if (Class922.aInteger_544 == 10) {
                //System.out.println("Returning keyReleased");
                return;
            }
        }
        if (null != Class3_Sub13_Sub3.aClass148_3049) {
            Class3_Sub13_Sub33.anInt3398 = 0;
            int var2 = var1.getKeyCode();
            if (var2 >= 0 && ~Class117.keycodes.length < ~var2) {
                var2 = Class117.keycodes[var2] & -129;
            } else {
                var2 = -1;
            }

            if (CanvasBuffer.anInt2384 >= 0 && ~var2 <= -1) {
                Class1009.anIntArray2952[CanvasBuffer.anInt2384] = ~var2;
                CanvasBuffer.anInt2384 = 127 & 1 + CanvasBuffer.anInt2384;
                if (~Class974.anInt1744 == ~CanvasBuffer.anInt2384) {
                    CanvasBuffer.anInt2384 = -1;
                }
            }
        }

        var1.consume();
    }

    public final void focusGained(FocusEvent var1) {
    }

    public static void method2085(int var0) {
        try {
            aClass93_1911 = null;
            aClass153_1916 = null;
            if (var0 > 81) {
                nullString = null;
                scrollbarjString = null;
                scrollbarFsjString = null;
                aClass94_1917 = null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "uf.A(" + var0 + ')');
        }
    }

    static final void method2086(byte var0) {
        try {
            if (var0 >= 62) {
                int var1 = Class945.thisClass946.y + Class933.anInt3216;
                int var2 = Class945.thisClass946.x - -Class1016.anInt42;
                if (499 < ~(-var1 + Class3_Sub13_Sub13.anInt3155) || ~(-var1 + Class3_Sub13_Sub13.anInt3155) < -501 || ~(Class62.anInt942 + -var2) > 499 || -501 > ~(-var2 + Class62.anInt942)) {
                    Class3_Sub13_Sub13.anInt3155 = var1;
                    Class62.anInt942 = var2;
                }

                if (~Class62.anInt942 != ~var2) {
                    Class62.anInt942 += (-Class62.anInt942 + var2) / 16;
                }

                if (~Class3_Sub13_Sub13.anInt3155 != ~var1) {
                    Class3_Sub13_Sub13.anInt3155 += (-Class3_Sub13_Sub13.anInt3155 + var1) / 16;
                }

                if (Class15.aBoolean346) {
                    for (int var3 = 0; ~Class3_Sub23.anInt2537 < ~var3; ++var3) {
                        int var4 = Class974.anIntArray1755[var3];
                        if (98 != var4) {
                            if (-100 != ~var4) {
                                if (var4 != 96) {
                                    if (var4 == 97) {
                                        Class1211.cameraYaw = Class1211.cameraYaw + 191 & -128;
                                    }
                                } else {
                                    Class1211.cameraYaw = Class1211.cameraYaw - 65 & -128;
                                }
                            } else {
                                Class3_Sub9.anInt2309 = -16 & Class3_Sub9.anInt2309 - 17;
                            }
                        } else {
                            Class3_Sub9.anInt2309 = -16 & Class3_Sub9.anInt2309 + 47;
                        }
                    }
                } else {
                    if (ObjectDefinition.isKeyDown[98]) {
                        Class27.anInt517 += (-Class27.anInt517 + 12) / 2;
                    } else if (!ObjectDefinition.isKeyDown[99]) {
                        Class27.anInt517 /= 2;
                    } else {
                        Class27.anInt517 += (-Class27.anInt517 + -12) / 2;
                    }

                    if (!ObjectDefinition.isKeyDown[96]) {
                        if (ObjectDefinition.isKeyDown[97]) {
                            Class3_Sub5.anInt2281 += (-Class3_Sub5.anInt2281 + 24) / 2;
                        } else {
                            Class3_Sub5.anInt2281 /= 2;
                        }
                    } else {
                        Class3_Sub5.anInt2281 += (-Class3_Sub5.anInt2281 + -24) / 2;
                    }

                    Class3_Sub9.anInt2309 += Class27.anInt517 / 2;
                    Class1211.cameraYaw += Class3_Sub5.anInt2281 / 2;
                }

                Class47.method1098((byte) -94);
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "uf.B(" + var0 + ')');
        }
    }

}

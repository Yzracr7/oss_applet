package com.kit.client;

final class Class21 {

    protected static boolean aBoolean440 = false;
    protected static int[] anIntArray441 = new int[2048];
    protected static int anInt443;
    protected static Class1008 aClass94_444 = Class943.create("<img=1>");
    protected static Class1008 pkerImg = Class943.create("<img=14>");
    protected static Class1008 donatorImg = Class943.create("<img=3>");
    protected static Class1008 extremeDonatorImg = Class943.create("<img=4>");
    protected static Class1008 supportImg = Class943.create("<img=2>");
    protected static Class1008 superDonatorImg = Class943.create("<img=15>");
    protected static Class1008 legendaryDonatorImg = Class943.create("<img=16>");

    public static void method911(int var0) {
        try {
            if (var0 != 26) {
                anIntArray441 = (int[]) null;
            }
            anIntArray441 = null;
            aClass94_444 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "dh.B(" + var0 + ')');
        }
    }

    static final void method912(boolean var0) {
        try {
            Class3_Sub13_Sub1.outputStream.offset = 0;
            Class7.lastPacket = -1;
            Class38_Sub1.drawContextMenu = var0;
            Class1017_2.anInt1704 = 0;
            Class65.mapFlagX = 0;
            Class3_Sub13_Sub34.contextOptionsAmount = 0;
            Class1030.anInt2582 = -1;
            Class161.anInt2028 = 0;
            Class38_Sub1.systemUpdateCycle = 0;
            Class1217.beforeLastPacket = -1;
            Class1211.incomingPackets.offset = 0;
            Class957.timoutCycle = 0;
            Class1008.incomingPacket = -1;

            int var1;
            for (var1 = 0; Class922.class946List.length > var1; ++var1) {
                if (null != Class922.class946List[var1]) {
                    Class922.class946List[var1].anInt2772 = -1;
                }
            }

            for (var1 = 0; ~var1 > ~Class3_Sub13_Sub24.class1001List.length; ++var1) {
                if (Class3_Sub13_Sub24.class1001List[var1] != null) {
                    Class3_Sub13_Sub24.class1001List[var1].anInt2772 = -1;
                }
            }

            Class3_Sub28_Sub9.method580((byte) 80);
            Class974.anInt1753 = 1;
            Class922.setaInteger_544(25);

            for (var1 = 0; var1 < 100; ++var1) {
                Class921.interfacesToRefresh[var1] = true;
            }

            // Class3_Sub13_Sub8.method204();
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "dh.F(" + var0 + ')');
        }
    }

    static final Class118 method913(int var0) {
        try {
            return new Class118_Sub1();
        } catch (Throwable var2) {
            return null;
        }
    }

    static boolean reHideChatbox;

    static final Class1207 overrideInterface(int uid, int var2,
                                             int type) {
        // Sendinterface
        // var2 = window | position
        //System.out.println("open inter: " + uid);
        if ((uid >= 228 && uid <= 248) || (uid >= 64 && uid <= 74)
                || (uid >= 210 && uid <= 214)) {
            Class922.dialogueUp = true;
            if (Class922.clientSize > 0) {
                if (Class922.chatboxHidden) {
                    reHideChatbox = true;
                    Class922.chatboxHidden = false;
                    Class7.getInterface(35914038 - 1).disabledSprite = 1158;
                    Class7.getInterface(Class922.CHATBOX_CONTAINER).hidden = false;
                }
                Class7.getInterface(Class922.CHATBOX_FRAME).hidden = false;
            }

        }
        if (uid == 12 || uid == 102 || uid == 465 || uid == 300 || uid == 374 || uid == 107) {
            //System.out.println("Hide fs");
            // Hides tabicons
            Class922.hideFSTabs = true;
            // If our invy is hidden open it up again because (bank/IKOD/equip
            // screen) is open
            if (Class922.clientSize > 0 && Class922.inventoryHidden) {
                Class7.getInterface(Class922.INVY_CONTAINER).hidden = false;
                Class7.getInterface(Class922.INVY_FRAME).hidden = false;
                Class922.inventoryHidden = false;
            }
        }
        Class1207 var4 = new Class1207();
        var4.type = type;
        var4.uid = uid;
        Class3_Sub13_Sub17.aClass130_3208.put(var4, (long) var2);
        Class3_Sub13_Sub13.method232(uid);
        Class1034 var5 = Class7.getInterface(var2);
        /*
		 * if(var2 == 35913790) { System.out.println("yo");
		 * if(client.clientSize > 0) var5.drawFullscreen = true; }
		 */
        if (var5 != null) {
            Class20.refreshInterface(var5);
        }

        if (null != Class3_Sub13_Sub7.aClass11_3087) {
            Class20.refreshInterface(Class3_Sub13_Sub7.aClass11_3087);
            Class3_Sub13_Sub7.aClass11_3087 = null;
        }

        int var6 = Class3_Sub13_Sub34.contextOptionsAmount;

        int var7;
        for (var7 = 0; var6 > var7; ++var7) {
            if (Class2.method73(Class3_Sub13_Sub7.aShortArray3095[var7])) {
                Class1042_4.method509(1, var7);
            }
        }

        if (1 == Class3_Sub13_Sub34.contextOptionsAmount) {
            Class38_Sub1.drawContextMenu = false;
            Class75.method1340(Class927.anInt1462,
                    Class3_Sub28_Sub3.anInt3552, Class3_Sub13_Sub33.anInt3395,
                    Class3_Sub28_Sub1.anInt3537);
        } else {
            Class75.method1340(Class927.anInt1462,
                    Class3_Sub28_Sub3.anInt3552, Class3_Sub13_Sub33.anInt3395,
                    Class3_Sub28_Sub1.anInt3537);
            var7 = Class922.getBoldClass1019().method682(Class75_Sub4.aClass94_2667);

            for (int var8 = 0; Class3_Sub13_Sub34.contextOptionsAmount > var8; ++var8) {
                int var9 = Class922.getBoldClass1019().method682(
                        ByteBuffer.getContextOption(var8, true));
                if (~var9 < ~var7) {
                    var7 = var9;
                }
            }

            Class3_Sub28_Sub3.anInt3552 = 8 + var7;
            Class3_Sub28_Sub1.anInt3537 = 15
                    * Class3_Sub13_Sub34.contextOptionsAmount
                    + (!Class1027.aBoolean1951 ? 22 : 26);
        }

        if (var5 != null) {
            Class1009.method2104(var5, false, 55);
        }

        Class3_Sub13_Sub12.executeOnLaunchScript(uid);
        if (-1 != Class1143.mainScreenInterface) {
            Class976.method124(1, Class1143.mainScreenInterface);
        }

        return var4;
    }

	/*
	 * static final void method915(Class1008 var0) { int var2 =
	 * Class3_Sub28_Sub8.method576(var0, false); if (-1 != var2) {
	 * Class956
	 * .method565(Class119.aClass131_1624.aShortArray1727[var2],
	 * Class119.aClass131_1624.aShortArray1718[var2]); } }
	 */

    static final Class961 createMouseWheel() {
        try {
            return new Class1051();
        } catch (Throwable var2) {
            return null;
        }
    }

    static final void removeOverrideInterface(Class1207 inter,
                                              boolean discard) {
        // System.out.println("remove: " + inter.uid);
        if ((inter.uid >= 228 && inter.uid <= 248)
                || (inter.uid >= 64 && inter.uid <= 74)
                || (inter.uid >= 210 && inter.uid <= 214)) {
            Class922.dialogueUp = false;
            if (Class922.clientSize > 0) {
                if (reHideChatbox) {
                    reHideChatbox = false;
                    Class922.chatboxHidden = true;
                    Class7.getInterface(35914038 - 1).disabledSprite = 1177;
                    Class7.getInterface(Class922.CHATBOX_CONTAINER).hidden = true;
                    //if(client.aInteger_510 < 525)
                    Class7.getInterface(Class922.CHATBOX_FRAME).hidden = true;
                } else if (Class929.aInteger_510 >= 525)
                    Class7.getInterface(Class922.CHATBOX_FRAME).hidden = true;
            }
        }
        if (inter.uid == 12 || inter.uid == 102 || inter.uid == 465
                || inter.uid == 300 || inter.uid == 374 || inter.uid == 107) {
            //System.out.println("UNHide fs");
            Class922.hideFSTabs = false;
        }
        if (inter.uid == 102 || inter.uid == 602) {
            // IKOD / Clan wars challenge
            Class60.discardInterface2(inter.uid);
            // Class7.getInterface(6684692).inventoryAmounts = null;
            // Class7.getInterface(6684692).inventoryIds = null;
        }
        int var4 = (int) inter.hash;
        int var3 = inter.uid;
        inter.unlink();
        if (discard) {
            Class60.discardInterface(var3);
        }

        Class164_Sub2.method2249((byte) 83, var3);
        Class1034 var5 = Class7.getInterface(var4);
        if (null != var5) {
            Class20.refreshInterface(var5);
        }

        int var6 = Class3_Sub13_Sub34.contextOptionsAmount;

        int var7;
        for (var7 = 0; var6 > var7; ++var7) {
            if (Class2.method73(Class3_Sub13_Sub7.aShortArray3095[var7])) {
                Class1042_4.method509(1, var7);
            }
        }

        if (-2 != ~Class3_Sub13_Sub34.contextOptionsAmount) {
            Class75.method1340(Class927.anInt1462,
                    Class3_Sub28_Sub3.anInt3552, Class3_Sub13_Sub33.anInt3395,
                    Class3_Sub28_Sub1.anInt3537);
            var7 = Class922.getBoldClass1019().method682(Class75_Sub4.aClass94_2667);

            for (int var8 = 0; ~var8 > ~Class3_Sub13_Sub34.contextOptionsAmount; ++var8) {
                int var9 = Class922.getBoldClass1019().method682(
                        ByteBuffer.getContextOption(var8, true));
                if (var7 < var9) {
                    var7 = var9;
                }
            }

            Class3_Sub28_Sub1.anInt3537 = Class3_Sub13_Sub34.contextOptionsAmount
                    * 15 + (Class1027.aBoolean1951 ? 26 : 22);
            Class3_Sub28_Sub3.anInt3552 = var7 + 8;
        } else {
            Class38_Sub1.drawContextMenu = false;
            Class75.method1340(Class927.anInt1462,
                    Class3_Sub28_Sub3.anInt3552, Class3_Sub13_Sub33.anInt3395,
                    Class3_Sub28_Sub1.anInt3537);
        }

        if (-1 != Class1143.mainScreenInterface) {
            Class976.method124(1, Class1143.mainScreenInterface);
        }
    }
}

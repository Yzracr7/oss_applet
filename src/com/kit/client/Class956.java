package com.kit.client;

import java.io.IOException;

abstract class Class956 extends Class1002 {

    protected static int anInt3602;
    protected static int anInt3603;
    protected static boolean removeRoofs = true;
    protected static int[][][] anIntArrayArrayArray3605;
    protected static int anInt3606;
    protected static int[] anIntArray3607 = new int[]{0, 2, 2, 2, 1, 1, 2, 2, 1, 3, 1, 1};
    protected static int anInt3608;

    static final Class1206_2 method562(Class1027 var0, int var1, int var2, byte var3) {
        try {
            return Class922.spriteExists(var0, var1, var2) ? (var3 != 39 ? (Class1206_2) null : Class3_Sub28_Sub9.method578(var3 ^ 84)) : null;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "gf.O(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

    public static void method563(int var0) {
        try {
            anIntArrayArrayArray3605 = (int[][][]) null;
            anIntArray3607 = null;
            if (var0 != 3) {
                anInt3603 = -108;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "gf.Q(" + var0 + ')');
        }
    }

    static final void method564(Class942 var0, int var1) {
        try {
            Class3_Sub28_Sub10.brightness = 3;
            Class25.method957(96, true);
            removeRoofs = true;
            Class3_Sub13_Sub15.isStereo = true;
            Class128.aBoolean1685 = true;
            Class3_Sub28_Sub9.anInt3622 = 0;
            Class3_Sub13_Sub5.fullscreenFrameHeight = 0;
            Class1005.showGroundDecorations = true;
            Class1228.highDetailLights = true;
            Class1034.manyIdleAnimations = true;
            Class14.areaSoundsVolume = 127;
            Class38.aBoolean661 = true;
            Class140_Sub6.aBoolean2910 = true;
            CanvasBuffer.fullscreenFrameWidth = 0;
            Class80.anInt1137 = 2;
            Class120_Sub30_Sub1.manyGroundTextures = true;
            Class989.aBoolean1441 = true;
            Class9.musicVolume = 255;
            Class25.lowMemoryTextures = true;
            Class921.antiAliasing = 0;
            Class995 var2 = null;
            Class1048.soundEffectsVolume = 127;
            if (Class3_Sub24_Sub3.maxMemory >= 96) {
                Class1218.method1758(2);
            } else {
                Class1218.method1758(0);
            }
            Class1008.anInt2148 = var1;
            Class3_Sub20.anInt2488 = 0;
            Class15.aBoolean346 = false;
            Class163_Sub3.aBoolean3004 = true;
            Class1008.safeMode = false;
            Class943.aBoolean1080 = false;
            Class1002.anInt2577 = 0;
            try {
                Class1124 var3 = var0.getPreferences("runescape");
                while (0 == var3.status) {
                    Class3_Sub13_Sub34.sleep(1L);
                }
                if (-2 == ~var3.status) {
                    var2 = (Class995) var3.value;
                    byte[] var4 = new byte[(int) var2.getLength()];

                    int var6;
                    for (int var5 = 0; ~var5 > ~var4.length; var5 += var6) {
                        var6 = var2.read(var4, var5, var4.length - var5);
                        if (var6 == -1) {
                            throw new IOException("EOF");
                        }
                    }
                    Class1220.method1390(new ByteBuffer(var4), -1);
                }
            } catch (Exception var8) {
                System.out.println("error1");
                ;
            }
            try {
                if (var2 != null) {
                    var2.close();
                }
            } catch (Exception var7) {
                System.out.println("error2");
                ;
            }
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "gf.F(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ')');
        }
    }

    static final void renameText(Class1008 text, int uid) {
        Class1042_3 var3 = Class3_Sub24_Sub3.putInterfaceChange(3, uid);
        var3.add();

        var3.stringValue = text;
    }

    abstract Object get();

    abstract boolean isSoftReference();
}

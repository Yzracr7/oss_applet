package com.kit.client;

abstract class Class3_Sub28_Sub10 extends Class1002 {

    protected static int brightness = 3;
    protected static Class1008 aClass94_3626 = Class943.create("Created gameworld");
    protected boolean aBoolean3628;
    protected static Class1008 aClass94_3629 = aClass94_3626;
    protected static int anInt3631;
    volatile boolean aBoolean3632 = true;
    protected boolean aBoolean3635;

    abstract int method586();

    abstract byte[] getBuffer(boolean var1);

    public static void method588() {
        aClass94_3626 = null;
        aClass94_3629 = null;
    }

    static final void method589(int var0, int var1, int var2) {
        Class3_Sub13_Sub21.mouseClickToTileRequested = true;
        Class972.anInt1302 = var0;
        Class49.mouseClickToTileOffX = var1;
        Class3_Sub13_Sub23_Sub1.anInt4039 = var2;
        Class27.clickedTileX = -1;
        Class1006.clickedTileZ = -1;
    }

    static final boolean method590(byte var0, int var1, int var2) {
        try {
            if (11 == var2) {
                var2 = 10;
            }

            int var3 = -59 % ((var0 - 26) / 55);
            if (-6 >= ~var2 && var2 <= 8) {
                var2 = 4;
            }

            ObjectDefinition var4 = ObjectDefinition.getDefinition(var1);
            return var4.method1684(115, var2);
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "il.D(" + var0 + ',' + var1 + ',' + var2 + ')');
        }
    }

}

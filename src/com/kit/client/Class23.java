package com.kit.client;

public final class Class23 {

   static int anInt453 = 0;
   public static int canvasWid;
   static int anInt455;
   static boolean[][] aBooleanArrayArray457;
   private static Class1008 aClass94_458 = Class943.create("Opened title screen");
   static Class1008 aClass94_459 = aClass94_458;


   public static void method937(int var0) {
      try {
         aBooleanArrayArray457 = (boolean[][])null;
         aClass94_458 = null;
         if(var0 != 0) {
            aBooleanArrayArray457 = (boolean[][])((boolean[][])null);
         }

         aClass94_459 = null;
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "dl.A(" + var0 + ')');
      }
   }

	/*static final void method938(int var0, int var1, int var2, int var3, int var4, int var5, int var7, int var8) {
		int var9 = var2 - var7;
		int var10 = var3 - var8;
		int var11 = (-var1 + var0 << 16) / var9;
		int var12 = (-var5 + var4 << 16) / var10;
		Class136.method1814(var1, var3, var2, var12, var7, 0, 0, 127, var11, var8, var5);
	}*/

   static final ByteBuffer getPreferencesBuffer() {
         ByteBuffer var1 = new ByteBuffer(34);
         var1.aBlowMe(11);
         var1.aBlowMe(Class3_Sub28_Sub10.brightness);
         var1.aBlowMe(!Class992.visibleLevels?0:1);
         var1.aBlowMe(Class956.removeRoofs?1:0);
         var1.aBlowMe(Class1005.showGroundDecorations?1:0);
         var1.aBlowMe(Class25.lowMemoryTextures?1:0);
         var1.aBlowMe(!Class1034.manyIdleAnimations?0:1);
         var1.aBlowMe(!Class1228.highDetailLights?0:1);
         var1.aBlowMe(Class120_Sub30_Sub1.manyGroundTextures ?1:0);
         var1.aBlowMe(!Class140_Sub6.aBoolean2910?0:1);
         var1.aBlowMe(Class80.anInt1137);
         var1.aBlowMe(!Class989.aBoolean1441?0:1);
         var1.aBlowMe(Class128.aBoolean1685?1:0);
         var1.aBlowMe(Class38.aBoolean661?1:0);
         var1.aBlowMe(Class3_Sub28_Sub9.anInt3622);
         var1.aBlowMe(!Class3_Sub13_Sub15.isStereo?0:1);
         var1.aBlowMe(Class1048.soundEffectsVolume);
         var1.aBlowMe(Class9.musicVolume);
         var1.aBlowMe(Class14.areaSoundsVolume);
         var1.method_0133(CanvasBuffer.fullscreenFrameWidth);
         var1.method_0133(Class3_Sub13_Sub5.fullscreenFrameHeight);
         var1.aBlowMe(Class1218.method1757());
         var1.method_211(Class1008.anInt2148);
         var1.aBlowMe(Class1002.anInt2577);
         var1.aBlowMe(Class1008.safeMode?1:0);
         var1.aBlowMe(!Class15.aBoolean346?0:1);
         var1.aBlowMe(Class3_Sub20.anInt2488);
         var1.aBlowMe(Class943.aBoolean1080?1:0);
         var1.aBlowMe(Class163_Sub3.aBoolean3004?1:0);
         return var1;
   }

   static final void method940(int var0, int var1) {
      try {
         if(var0 >= 101) {
            Class980 var2 = Class1225.class980;
            synchronized(var2) {
               Class1225.anInt4045 = var1;
            }
         }
      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "dl.D(" + var0 + ',' + var1 + ')');
      }
   }

}

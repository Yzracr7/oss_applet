package com.kit.client;

final class Class38_Sub1 extends Class38 {

    static int[][][] anIntArrayArrayArray2609;
    static Class1008 aClass94_2610 = Class943.create(")1o");
    static String aString2611;
    static int clickY;
    static Class15 aClass15_2613;
    static int clickY2 = 0;
    static boolean drawContextMenu = false;
    static int systemUpdateCycle = 0;
    static int randomSeed;


    static final void markMinimap(Class1034 var0, Class957 sprite, int var2, int var3, int var4, int var6) {
       /*if(null != sprite) {
		   int var9 = var3 * var3 - -(var2 * var2);
           int var7 = 2047 & Class3_Sub13_Sub8.anInt3102 + Class1211.anInt531;
           int var8 = Math.max(var0.anInt168 / 2, var0.anInt193 / 2) - -10;
           if(var8 * var8 >= var9) {
              int var10 = Class978.sineTable[var7];
              var10 = var10 * 256 / (Class164_Sub2.anInt3020 - -256);
              int var11 = Class978.cosineTable[var7];
              var11 = 256 * var11 / (256 + Class164_Sub2.anInt3020);
              int var12 = var10 * var2 - -(var3 * var11) >> 16;
              int var13 = var11 * var2 + -(var3 * var10) >> 16;
              if(!Class1012.aBoolean_617) {
                 ((Class1206_2)sprite).method666(var0.anInt168 / 2 + var6 - -var12 + -(sprite.trimWidth / 2), -(sprite.trimHeight / 2) + var0.anInt193 / 2 + var4 + -var13, client.mapbackSource, client.mapbackDest);
              } else {
                 ((Class1011)sprite).method645(var0.anInt168 / 2 + var6 + var12 - sprite.trimWidth / 2, var0.anInt193 / 2 + var4 - (var13 + sprite.trimHeight / 2), (Class1011)var0.method866((byte)-113, false));
              }

           }
        }*/
        if (null != sprite) {
            int distance = var3 * var3 + var2 * var2;
            int yaw = 0x7ff & Class3_Sub13_Sub8.anInt3102 + Class1211.cameraYaw;
            if (distance <= 6400) {
                int sineYaw = Rasterizer.sineTable[yaw];
                sineYaw = sineYaw * 256 / (Class164_Sub2.anInt3020 + 256);
                int cosineYaw = Rasterizer.cosineTable[yaw];
                cosineYaw = 256 * cosineYaw / (256 + Class164_Sub2.anInt3020);
                int var12 = sineYaw * var2 + (var3 * cosineYaw) >> 16;
                int var13 = cosineYaw * var2 - (var3 * sineYaw) >> 16;

                //TODO - HD MAP ICON SPRITES
                if (!Class1012.aBoolean_617) {
                    ((Class1206_2) sprite).method643((-(sprite.trimWidth / 2) + (136 + var6)
                            - -var12 - 43), -var13
                            + (81 + (var4 - sprite.trimHeight / 2)));
                    //((Class1206_2)sprite).method643(var0.width / 2 + var6 + var12 - (sprite.trimWidth / 2), -(sprite.trimHeight / 2) + var0.height / 2 + var4 - var13);
                   // System.out.println(var0.width / 2 + var6 + var12 - (sprite.trimWidth / 2) + " [Map icon LD] : " + sprite.offsetX);
                    // ((Class1206_2)sprite).method666(var0.anInt168 / 2 + var6 - -var12 + -(sprite.trimWidth / 2), -(sprite.trimHeight / 2) + var0.anInt193 / 2 + var4 + -var13, client.mapbackSource, client.mapbackDest);
                } else {
                   // System.out.println(var0.width / 2 + var6 + var12 - (sprite.trimWidth / 2) + " [Map icon HD] : " + sprite.offsetX);
                    ((Class1011) sprite).method643((-(sprite.trimWidth / 2) + (94 + var6)
                            - -var12 - -4), -var13
                            + (83 + (var4 - sprite.trimHeight / 2) + -4));
                   // ((Class1011) sprite).method645(var0.width / 2 + var6 + var12 - sprite.trimWidth / 2, var0.height / 2 + var4 - (var13 + sprite.trimHeight / 2), (Class1011) var0.method_405((byte) -113, false));
                }
            }
        }
    }

    public static void method1032(boolean var0) {
        try {
            anIntArrayArrayArray2609 = (int[][][]) null;
            aString2611 = null;
            aClass94_2610 = null;
            aClass15_2613 = null;
            if (var0) {
                method1032(true);
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "em.A(" + var0 + ')');
        }
    }

}

package com.kit.client;

import java.awt.Component;

final class ItemDefinition {

	private short[] aShort765;
    private short[] aShort751;
    private int femaleEquipZ;
    private int anInt753 = -1;
    protected int offsetY;
    private int groundModel;
    protected int anInt756 = -1;
    protected int cost = 1;
    protected int anInt758;
    private int maleEquipX = 0;
    protected int femaleModelId;
    protected int anInt762;
    protected int stackable;
    protected int[] anIntArray766;
    protected int anInt767;
    protected int anInt768;
    private int anInt769;
    protected Class1008 name;
    private int maleArmsId;
    private short[] modifiedColors;
    private int anInt773 = -1;
    private short[] originalColors;
    private int maleEquipZ;
    private int anInt776 = -1;
    private int femaleEquipX;
    private int maleEquipY;
    boolean members;
    private int anInt780;
    protected static int[] xpForLevel = new int[99];
    protected int anInt782 = 0;
    protected Class1008[] invyActions;
    private int anInt784;
    private byte[] aByteArray785;
    protected int rotationY;
    protected int itemId;
    protected int anInt788;
    protected int anInt789;
    private int anInt790;
    protected int anInt791;
    protected int offsetX;
    protected int maleModelId;
    private int femaleArmsId;
    protected int anInt795;
    private int anInt796;
    private int anInt797;
    protected Class1017_2 aClass130_798;
    protected int rotationX;
    protected int anInt800;
    protected Class1008[] groundActions;
    private int femaleEquipY;
    private int anInt803;
    protected int[] anIntArray804;
    private int anInt805;
    protected static Class1008 actionColor;
    protected boolean aBoolean807;
    protected static Class1008 actionColor2;
    protected int modelZoom;

    final boolean method1102(boolean var1, boolean var2) {
        try {
            int var3 = this.anInt803;
            int var4 = this.anInt796;
            if (var1) {
                var3 = this.anInt773;
                var4 = this.anInt753;
            }
            if (~var3 == 0) {
                return true;
            } else {
                boolean var5 = true;
                if (!Class1030.cacheIndex7.method2129(0, var3)) {
                    if (!Class922.modelsCache602.method2129(0, var3)) {
                        var5 = false;
                    }
                }

                if (var4 != -1 && !Class1030.cacheIndex7.method2129(0, var4)) {
                    if (!Class922.modelsCache602.method2129(0, var4)) {
                        var5 = false;
                    }
                }
                return var5;
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "h.G(" + var1 + ',' + var2 + ')');
        }
    }

    static final void method1103(Class1027 var0, Class1027 var1, boolean var2) {
        try {
            Class3_Sub13_Sub14.aClass153_3173 = var0;
            Class1221.aClass153_557 = var1;
            if (!var2) {
                ;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "h.B("
                    + (var0 != null ? "{...}" : "null") + ','
                    + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    final Class1008 method1105(int var1, Class1008 var2, int var3) {
        try {
            if (this.aClass130_798 == null) {
                return var2;
            } else {
                if (var1 < 90) {
                    method1111(-111);
                }

                Class1030 var4 = (Class1030) this.aClass130_798.get(var3);
                return null != var4 ? var4.value : var2;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "h.S(" + var1 + ','
                    + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    final ItemDefinition method1106(int var1, int var2) {
        try {
            int var3 = 58 % ((-28 - var2) / 48);
            if (this.anIntArray804 != null && -2 > ~var1) {
                int var4 = -1;

                for (int var5 = 0; 10 > var5; ++var5) {
                    if (this.anIntArray766[var5] <= var1
                            && -1 != ~this.anIntArray766[var5]) {
                        var4 = this.anIntArray804[var5];
                    }
                }

                if (0 != ~var4) {
                    return ItemDefinition.getDefinition(var4);
                }
            }

            return this;
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "h.H(" + var1 + ',' + var2 + ')');
        }
    }

    static final Class1228 method1107(int var0) {
        try {
            if (Class3_Sub13_Sub16.aClass44_Sub1Array3201.length > Class3_Sub6.anInt2291) {
                return Class3_Sub13_Sub16.aClass44_Sub1Array3201[Class3_Sub6.anInt2291++];
            } else {
                if (var0 != 5422) {
                    method1107(-66);
                }

                return null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "h.R(" + var0 + ')');
        }
    }

    final boolean method1108(boolean var2) {
        int var4 = this.maleArmsId;
        int var3 = this.maleModelId;
        int var5 = this.anInt769;
        if (var2) {
            var5 = this.anInt776;
            var3 = this.femaleModelId;
            var4 = this.femaleArmsId;
        }

        if (var3 != -1) {
            boolean var7 = true;
            if (!Class1030.cacheIndex7.method2129(0, var3)) {
                if (!Class922.modelsCache602.method2129(0, var3)) {
                    var7 = false;
                }
            }


            if (var4 != -1 && !Class1030.cacheIndex7.method2129(0, var4)) {
                if (!Class922.modelsCache602.method2129(0, var4)) {
                    var7 = false;
                }

            }

            if (-1 != var5 && !Class1030.cacheIndex7.method2129(0, var5)) {
                if (!Class922.modelsCache602.method2129(0, var5)) {
                    var7 = false;
                }
            }
            return var7;
        } else {
            return true;
        }
    }

    final void method1109(byte var1, ItemDefinition var2, ItemDefinition var3) {
        try {
            this.aByteArray785 = var2.aByteArray785;
            this.maleEquipY = var2.maleEquipY;
            this.aClass130_798 = var2.aClass130_798;
            this.anInt769 = var2.anInt769;
            this.femaleModelId = var2.femaleModelId;
            this.maleEquipZ = var2.maleEquipZ;
            this.invyActions = new Class1008[5];
            this.groundModel = var3.groundModel;
            this.modelZoom = var3.modelZoom;
            this.cost = 0;
            this.anInt782 = var2.anInt782;
            this.anInt773 = var2.anInt773;
            this.originalColors = var2.originalColors;
            this.anInt768 = var3.anInt768;
            this.maleArmsId = var2.maleArmsId;
            this.rotationX = var3.rotationX;
            this.anInt803 = var2.anInt803;
            this.anInt796 = var2.anInt796;
            this.maleEquipX = var2.maleEquipX;
            this.rotationY = var3.rotationY;
            this.offsetY = var3.offsetY;
            this.anInt753 = var2.anInt753;
            this.femaleEquipX = var2.femaleEquipX;
            this.modifiedColors = var2.modifiedColors;
            this.femaleEquipY = var2.femaleEquipY;
            this.femaleEquipZ = var2.femaleEquipZ;
            this.offsetX = var3.offsetX;
            if (var1 != 69) {
                this.cost = 109;
            }

            this.maleModelId = var2.maleModelId;
            this.femaleArmsId = var2.femaleArmsId;
            this.name = var2.name;
            this.aShort751 = var2.aShort751;
            this.aShort765 = var2.aShort765;
            this.groundActions = var2.groundActions;
            this.members = var2.members;
            this.anInt776 = var2.anInt776;
            if (null != var2.invyActions) {
                for (int var4 = 0; -5 < ~var4; ++var4) {
                    this.invyActions[var4] = var2.invyActions[var4];
                }
            }

            this.invyActions[4] = Class922.discardjString;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "h.J(" + var1 + ','
                    + (var2 != null ? "{...}" : "null") + ','
                    + (var3 != null ? "{...}" : "null") + ')');
        }
    }

    public short[] getOriginalColors() {
        return originalColors;
    }

    public short[] getModifiedColors() {
        return modifiedColors;
    }

    final Class960 method1110(int var1, int var2, int var3,
                              Class954 var4, int var5, int var6) {
        try {
            if (var1 < 94) {
                this.groundModel = -67;
            }

            if (this.anIntArray804 != null && -2 > ~var5) {
                int var7 = -1;

                for (int var8 = 0; var8 < 10; ++var8) {
                    if (~var5 <= ~this.anIntArray766[var8]
                            && ~this.anIntArray766[var8] != -1) {
                        var7 = this.anIntArray804[var8];
                    }
                }

                if (~var7 != 0) {
                    return ItemDefinition.getDefinition(var7).method1110(113,
                            var2, var3, var4, 1, var6);
                }
            }

            Class960 var11 = (Class960) Class143.aClass93_1874
                    .get(this.itemId);

            //nothere
            if (var11 == null) {
                Model var12 = Model.get(Class1030.cacheIndex7,
                        this.groundModel, 0);
                if (null == var12) {
                    return null;
                }

                int var9;
                if (null != this.originalColors) {
                    for (var9 = 0; this.originalColors.length > var9; ++var9) {
                        if (null != this.aByteArray785
                                && ~var9 > ~this.aByteArray785.length) {
                            var12.swapColors(
                                    this.originalColors[var9],
                                    Class3_Sub13_Sub38.aShortArray3453[this.aByteArray785[var9] & 255]);
                        } else {
                            var12.swapColors(this.originalColors[var9],
                                    this.modifiedColors[var9]);
                        }
                    }
                }

                if (this.aShort765 != null) {
                    for (var9 = 0; var9 < this.aShort765.length; ++var9) {
                        var12.method1998(this.aShort765[var9],
                                this.aShort751[var9]);
                    }
                }

                var11 = var12.convert(this.anInt784 + 64, 768 + this.anInt790,
                        -50, -10, -50);
                if (~this.anInt805 != -129 || this.anInt780 != 128
                        || ~this.anInt797 != -129) {
                    var11.scale(this.anInt805, this.anInt780, this.anInt797);
                }

                var11.aBoolean2699 = true;
                if (Class1012.aBoolean_617) {
                    ((Class948) var11).method1920(false, false, false, true,
                            false, false, true);
                }

                Class143.aClass93_1874.put(var11, this.itemId);
            }

            if (var4 != null) {
                var11 = var4.method2055(var11, (byte) -88, var2, var3, var6);
            }

            return var11;
        } catch (RuntimeException var10) {
            throw Class1134.method1067(var10, "h.E(" + var1 + ',' + var2 + ','
                    + var3 + ',' + (var4 != null ? "{...}" : "null") + ','
                    + var5 + ',' + var6 + ')');
        }
    }

    public static void method1111(int var0) {
        try {
            xpForLevel = null;
            actionColor2 = null;
            if (var0 == 3327) {
                actionColor = null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "h.P(" + var0 + ')');
        }
    }

    final void method1112(int var1) {
        try {
            if (var1 != 5401) {
                method1103((Class1027) null, (Class1027) null, true);
            }

        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "h.O(" + var1 + ')');
        }
    }

    final void init_decoding(ByteBuffer buff) {
        while (true) {
            int var3 = buff.readUnsignedByte();
            if (0 == var3) {
                return;
            }
            this.readValues(buff, var3);
        }
    }

    private final void readValues(ByteBuffer class966, int opcode) {
        if (~opcode != -2) {
            if (opcode != 2) {
                if (-5 == ~opcode) {
                    this.modelZoom = class966.aInteger233();
                } else if (~opcode == -6) {
                    this.rotationY = class966.aInteger233();
                } else if (~opcode == -7) {
                    this.rotationX = class966.aInteger233();
                } else if (~opcode == -8) {
                    this.offsetX = class966.aInteger233();
                    if (~this.offsetX < -32768) {
                        this.offsetX -= 65536;
                    }
                } else if (~opcode != -9) {
                    if (opcode == 11) {
                        this.stackable = 1;
                    } else if (-13 == ~opcode) {
                        this.cost = class966.getInt();
                    } else if (-17 != ~opcode) {
                        if (23 == opcode) {
                            this.maleModelId = class966.aInteger233();
                            class966.readUnsignedByte();
                        } else if (opcode != 24) {
                            if (~opcode != -26) {
                                if (~opcode != -27) {
                                    if (opcode >= 30 && -36 < ~opcode) {
                                        this.groundActions[-30 + opcode] = class966.class_91033();
                                        if (this.groundActions[opcode + -30]
                                                .method102(Class3_Sub13_Sub3.aClass94_3051)) {
                                            this.groundActions[-30 + opcode] = null;
                                        }
                                    } else if (35 <= opcode && 40 > opcode) {
                                        this.invyActions[-35 + opcode] = class966.class_91033();
                                    } else {
                                        int var5;
                                        int var6;
                                        if (-41 != ~opcode) {
                                            if (-42 == ~opcode) {
                                                var5 = class966.readUnsignedByte();
                                                this.aShort751 = new short[var5];
                                                this.aShort765 = new short[var5];

                                                for (var6 = 0; ~var5 < ~var6; ++var6) {
                                                    this.aShort765[var6] = (short) class966.aInteger233();
                                                    this.aShort751[var6] = (short) class966.aInteger233();
                                                }
                                            } else if (42 == opcode) {
                                                var5 = class966.readUnsignedByte();
                                                this.aByteArray785 = new byte[var5];

                                                for (var6 = 0; ~var6 > ~var5; ++var6) {
                                                    this.aByteArray785[var6] = class966.getByte();
                                                }
                                            } else if (opcode == 65) {
                                                this.aBoolean807 = true;
                                            } else if (-79 != ~opcode) {
                                                if (-80 == ~opcode) {
                                                    this.anInt776 = class966.aInteger233();
                                                } else if (90 == opcode) {
                                                    this.anInt803 = class966.aInteger233();
                                                } else if (opcode == 91) {
                                                    this.anInt773 = class966.aInteger233();
                                                } else if (opcode != 92) {
                                                    if (opcode != 93) {
                                                        if (-96 == ~opcode) {
                                                            this.anInt768 = class966.aInteger233();
                                                        } else if (-97 == ~opcode) {
                                                            this.anInt800 = class966.readUnsignedByte();
                                                        } else if (opcode == 97) {
                                                            this.anInt789 = class966.aInteger233();
                                                        } else if (-99 == ~opcode) {
                                                            this.anInt791 = class966.aInteger233();
                                                        } else if (-101 >= ~opcode && ~opcode > -111) {
                                                            if (null == this.anIntArray804) {
                                                                this.anIntArray804 = new int[10];
                                                                this.anIntArray766 = new int[10];
                                                            }

                                                            this.anIntArray804[-100 + opcode] = class966.aInteger233();
                                                            this.anIntArray766[opcode + -100] = class966.aInteger233();
                                                        } else if (110 != opcode) {
                                                            if (~opcode != -112) {
                                                                if (-113 == ~opcode) {
                                                                    this.anInt797 = class966.aInteger233();
                                                                } else if (-114 != ~opcode) {
                                                                    if (~opcode == -115) {
                                                                        this.anInt790 = 5 * class966.getByte();
                                                                    } else if (opcode == 115) {
                                                                        this.anInt782 = class966.readUnsignedByte();
                                                                    } else if (opcode != 121) {
                                                                        if (opcode == 122) {
                                                                            this.anInt762 = class966.aInteger233();
                                                                        } else if (125 != opcode) {
                                                                            if (~opcode == -127) {
                                                                                this.femaleEquipX = class966
                                                                                        .getByte();
                                                                                this.femaleEquipY = class966
                                                                                        .getByte();
                                                                                this.femaleEquipZ = class966
                                                                                        .getByte();
                                                                            } else if (opcode == 127) {
                                                                                this.anInt767 = class966.readUnsignedByte();
                                                                                this.anInt758 = class966.aInteger233();
                                                                            } else if (~opcode != -129) {
                                                                                if (-130 != ~opcode) {
                                                                                    if (-131 == ~opcode) {
                                                                                        class966.readUnsignedByte();
                                                                                        class966.aInteger233();// some
                                                                                        // cs2
                                                                                        // stuff
                                                                                    } else if (opcode == 132) {
                                                                                        int i = class966.readUnsignedByte();
                                                                                        int[] anIntArray2935 = new int[i];
                                                                                        for (int i_5_ = 0; (i
                                                                                                ^ 0xffffffff) < (i_5_
                                                                                                ^ 0xffffffff); i_5_++)
                                                                                            anIntArray2935[i_5_] = class966
                                                                                                    .aInteger233();// these
                                                                                        // are
                                                                                        // images
                                                                                        // for
                                                                                        // items
                                                                                    } else if (139 == opcode) {
                                                                                        class966.aInteger233();
                                                                                    } else if (140 == opcode) {
                                                                                        class966.aInteger233();
                                                                                    } else if (148 == opcode) {
                                                                                        class966.aInteger233();
                                                                                    } else if (149 == opcode) {
                                                                                        class966.aInteger233();
                                                                                    } else if (249 == opcode) {
                                                                                        var5 = class966.readUnsignedByte();
                                                                                        if (null == this.aClass130_798) {
                                                                                            var6 = Class95
                                                                                                    .method1585(var5);
                                                                                            this.aClass130_798 = new Class1017_2(
                                                                                                    var6);
                                                                                        }

                                                                                        for (var6 = 0; var6 < var5; ++var6) {
                                                                                            boolean var7 = class966
                                                                                                    .readUnsignedByte() == 1;
                                                                                            int var8 = class966
                                                                                                    .aBoolean183();
                                                                                            Object var9;
                                                                                            if (!var7) {
                                                                                                var9 = new Class1042_2(
                                                                                                        class966.getInt());
                                                                                            } else {
                                                                                                var9 = new Class1030(
                                                                                                        class966.class_91033());
                                                                                            }

                                                                                            this.aClass130_798.put(
                                                                                                    (Class1042) var9,
                                                                                                    (long) var8);
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    class966.readUnsignedByte();
                                                                                    class966.aInteger233();// some
                                                                                    // cs2
                                                                                    // stuff
                                                                                }
                                                                            } else {
                                                                                this.anInt788 = class966.readUnsignedByte();
                                                                                this.anInt756 = class966.aInteger233();
                                                                            }
                                                                        } else {
                                                                            this.maleEquipX = class966
                                                                                    .getByte();
                                                                            this.maleEquipY = class966
                                                                                    .getByte();
                                                                            this.maleEquipZ = class966
                                                                                    .getByte();
                                                                        }
                                                                    } else {
                                                                        this.anInt795 = class966.aInteger233();
                                                                    }
                                                                } else {
                                                                    this.anInt784 = class966.getByte();
                                                                }
                                                            } else {
                                                                this.anInt780 = class966.aInteger233();
                                                            }
                                                        } else {
                                                            this.anInt805 = class966.aInteger233();
                                                        }
                                                    } else {
                                                        this.anInt753 = class966.aInteger233();
                                                    }
                                                } else {
                                                    this.anInt796 = class966.aInteger233();
                                                }
                                            } else {
                                                this.anInt769 = class966.aInteger233();
                                            }
                                        } else {
                                            var5 = class966.readUnsignedByte();
                                            this.modifiedColors = new short[var5];
                                            this.originalColors = new short[var5];

                                            for (var6 = 0; var5 > var6; ++var6) {
                                                this.originalColors[var6] = (short) class966
                                                        .aInteger233();
                                                this.modifiedColors[var6] = (short) class966
                                                        .aInteger233();
                                            }
                                        }
                                    }
                                } else {
                                    this.femaleArmsId = class966.aInteger233();
                                }
                            } else {
                                this.femaleModelId = class966.aInteger233();
                                class966.readUnsignedByte();// 464 uses
                            }
                        } else {
                            this.maleArmsId = class966.aInteger233();
                        }
                    } else {
                        this.members = true;
                    }
                } else {
                    this.offsetY = class966.aInteger233();
                    if (this.offsetY > 32767) {
                        this.offsetY -= 65536;
                    }
                }
            } else {
                this.name = class966.class_91033();
            }
        } else {
            this.groundModel = class966.aInteger233();
        }
    }

    final int method1115(int var1, int var3) {
        if (this.aClass130_798 != null) {
            Class1042_2 var5 = (Class1042_2) this.aClass130_798.get(var3);
            return null != var5 ? var5.value : var1;
        } else {
            return var1;
        }
    }

    final Model method1116(boolean var1, byte var2) {
        try {
            int var4 = this.anInt796;
            if (var2 != -109) {
                return null;
            } else {
                int var3 = this.anInt803;
                if (var1) {
                    var4 = this.anInt753;
                    var3 = this.anInt773;
                }

                if (-1 != var3) {

                    Model var5 = Model.get(
                            Class1030.cacheIndex7, var3, 0);
                    if (-1 != var4) {
                        Model var6 = Model.get(
                                Class1030.cacheIndex7, var4, 0);
                        Model[] var7 = new Model[]{var5, var6};
                        var5 = new Model(var7, 2);
                    }

                    int var9;
                    if (this.originalColors != null) {
                        for (var9 = 0; var9 < this.originalColors.length; ++var9) {
                            var5.swapColors(this.originalColors[var9],
                                    this.modifiedColors[var9]);
                        }
                    }

                    if (this.aShort765 != null) {
                        for (var9 = 0; ~this.aShort765.length < ~var9; ++var9) {
                            var5.method1998(this.aShort765[var9],
                                    this.aShort751[var9]);
                        }
                    }

                    return var5;
                } else {
                    return null;
                }
            }
        } catch (RuntimeException var8) {
            throw Class1134.method1067(var8, "h.A(" + var1 + ',' + var2 + ')');
        }
    }

    final Model getBodyModel(boolean var1, int var2) {
        int model = this.maleModelId;
        int arms = this.maleArmsId;
        int var5 = this.anInt769;
        if (var1) {
            var5 = this.anInt776;
            model = this.femaleModelId;
            arms = this.femaleArmsId;
        }

        if (model == -1) {// TODO this
            return null;
        } else {
            //	System.out.println("getbodymodel for item: " + this.itemId);
            Model mainModel = Model.get(Class1030.cacheIndex7, model,
                    0);
            if (0 != ~arms) {
                Model armsModel = Model.get(Class1030.cacheIndex7,
                        arms, 0);
                if (-1 == var5) {
                    Model[] combinedModel = new Model[]{mainModel, armsModel};
                    mainModel = new Model(combinedModel, 2);
                } else {
                    Model var12 = Model.get(
                            Class1030.cacheIndex7, var5, 0);
                    Model[] var9 = new Model[]{mainModel, armsModel,
                            var12};
                    mainModel = new Model(var9, 3);
                }
            }

            if (mainModel == null)
                return null;

            if (!var1
                    && (~this.maleEquipX != -1 || ~this.maleEquipY != -1 || -1 != ~this.maleEquipZ)) {
                mainModel.method2001(this.maleEquipX, this.maleEquipY, this.maleEquipZ);
            }

            if (var1
                    && (~this.femaleEquipX != -1 || ~this.femaleEquipY != -1 || ~this.femaleEquipZ != -1)) {
                mainModel.method2001(this.femaleEquipX, this.femaleEquipY, this.femaleEquipZ);
            }

            int var11;
            if (this.originalColors != null) {
                for (var11 = 0; var11 < this.originalColors.length; ++var11) {
                    mainModel.swapColors(this.originalColors[var11],
                            this.modifiedColors[var11]);
                }
            }

            if (this.aShort765 != null) {
                for (var11 = 0; var11 < this.aShort765.length; ++var11) {
                    mainModel.method1998(this.aShort765[var11],
                            this.aShort751[var11]);
                }
            }

            return mainModel;
        }
    }

    final void method1118(ItemDefinition var1, ItemDefinition var2, boolean var3) {
        try {
            this.name = var1.name;
            this.modelZoom = var2.modelZoom;
            if (var3) {
                this.anInt780 = -70;
            }

            this.originalColors = var2.originalColors;
            this.modifiedColors = var2.modifiedColors;
            this.rotationY = var2.rotationY;
            this.rotationX = var2.rotationX;
            this.aShort751 = var2.aShort751;
            this.groundModel = var2.groundModel;
            this.aByteArray785 = var2.aByteArray785;
            this.anInt768 = var2.anInt768;
            this.cost = var1.cost;
            this.stackable = 1;
            this.offsetY = var2.offsetY;
            this.offsetX = var2.offsetX;
            this.aShort765 = var2.aShort765;
            this.members = var1.members;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "h.N("
                    + (var1 != null ? "{...}" : "null") + ','
                    + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    static final void addMouseHandler(Component var0) {
        var0.addMouseListener(Class1225.class980);
        var0.addMouseMotionListener(Class1225.class980);
        var0.addFocusListener(Class1225.class980);
        var0.addMouseWheelListener(Class1225.class980);
    }

    final Class1049 method1120(int var1) {
        try {
            Model var2 = Model.get(Class1030.cacheIndex7,
                    this.groundModel, 0);
            if (var2 == null) {
                return null;
            } else {
                int var3;
                if (this.originalColors != null) {
                    for (var3 = 0; this.originalColors.length > var3; ++var3) {
                        if (null != this.aByteArray785
                                && ~var3 > ~this.aByteArray785.length) {
                            var2.swapColors(
                                    this.originalColors[var3],
                                    Class3_Sub13_Sub38.aShortArray3453[this.aByteArray785[var3] & 255]);
                        } else {
                            var2.swapColors(this.originalColors[var3],
                                    this.modifiedColors[var3]);
                        }
                    }
                }

                if (this.aShort765 != null) {
                    for (var3 = 0; var3 < this.aShort765.length; ++var3) {
                        var2.method1998(this.aShort765[var3],
                                this.aShort751[var3]);
                    }
                }

                Class1049 var5 = var2.method2000(64 - -this.anInt784,
                        768 - -this.anInt790, -50, -10, -50);
                if (var1 != 18206) {
                    this.method1105(-67, (Class1008) null, -37);
                }

                if (-129 != ~this.anInt805 || -129 != ~this.anInt780
                        || -129 != ~this.anInt797) {
                    var5.scale(this.anInt805, this.anInt780, this.anInt797);
                }

                return var5;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "h.L(" + var1 + ')');
        }
    }

    static final ItemDefinition getDefinition(int itemId) {
        ItemDefinition def = (ItemDefinition) Class3_Sub28_Sub4.itemDefinitions
                .get(itemId);
        if (def != null)
            return def;
        int itemFile = Class922.getItemsConfig(itemId);
        byte[] data = Class164.cacheIndex2.getFile(
                itemFile, itemId);
        if (data == null) {
            data = Class164.cacheIndex2.getFile(32, itemId);
        }
        def = new ItemDefinition();
        def.itemId = itemId;
        if (data != null) {
            def.init_decoding(new ByteBuffer(data));
        }
        if (def.itemId == 21295 || def.itemId == 21284 || def.itemId == 21285 || def.itemId == 21282 || def.itemId == 21289) {
            def.aShort751 = new short[1];
            def.aShort765 = new short[1];
            def.aShort751[0] = 681;
            def.aShort765[0] = 59;
        }
        def.method1112(5401);
        if (0 != ~def.anInt791) {
            def.method1118(getDefinition(def.anInt789),
                    getDefinition(def.anInt791), false);
        }
        if (def.anInt762 != -1) {
            def.method1109((byte) 69, getDefinition(def.anInt795),
                    getDefinition(def.anInt762));
        }

        if (!Class139.membersWorld && def.members) {
            def.name = Class951.aClass94_3691;
            def.anInt782 = 0;
            def.invyActions = Class1244.aClass94Array2119;
            def.aBoolean807 = false;
            def.groundActions = ByteBuffer.aClass94Array2596;
        }
        Class3_Sub28_Sub4.itemDefinitions.put(def, itemId);
        // System.out.println("var2 integer_34: " + var2.integer_34);
        // System.out.println("var2 integer_34: " + var2.integer_34 + " 791: " +
        // var2.anInt791 + ", 762: " + var2.anInt762 + ", 7 ate 9: " +
        // var2.anInt789
        // + ", 7 ate 7: " + var2.itemId);

        if (def.itemId == 290) {
            System.out.println("Model ID:" + def.groundModel + "");
            System.out.println("Model Zoom:" + def.modelZoom + "");
            System.out.println("Model RotationY:" + def.rotationY + "");
            System.out.println("Model RotationX:" + def.rotationX + "");
            for (int i = 0; i < def.modifiedColors.length; i++) {
                System.out.println("Model Original Colors:" + def.originalColors[i] + "");
            }

        }
        if (def.itemId == 4151 || itemId == 1123) {
            if (def.modifiedColors != null) {
                for (int i = 0; i < def.modifiedColors.length; i++)
                    System.out.println("orignal colors: " + i + ", "
                            + def.modifiedColors[i]);
            }
            if (def.originalColors != null) {
                for (int i = 0; i < def.originalColors.length; i++)
                    System.out.println("orignal colors: " + i + ", "
                            + def.originalColors[i]);
            }
            // System.out.println("810:" + def.modelZoom);
            // System.out.println("786:" + def.rotationY);
            // System.out.println("799:" + def.anInt799);

            // System.out.println("797:" + def.anInt797); //128 - must be
            // important?
            /*
             * System.out.println("788:" + def.anInt788);
			 * System.out.println("756:" + def.anInt756);
			 * System.out.println("760:" + def.anInt760);
			 * System.out.println("778:" + def.anInt778);
			 * System.out.println("775:" + def.anInt775);
			 * System.out.println("795:" + def.anInt795);
			 * System.out.println("784:" + def.anInt784);
			 * System.out.println("780:" + def.anInt780);
			 * System.out.println("805:" + def.anInt805);
			 * System.out.println("753:" + def.anInt753);
			 * System.out.println("796:" + def.anInt796);
			 * System.out.println("769:" + def.anInt769);
			 * System.out.println("794:" + def.anInt794);
			 * System.out.println("771:" + def.anInt771);
			 * System.out.println("754:" + def.offsetY);
			 * System.out.println("755:" + def.groundModel);
			 */
            // System.out.println("790:" + def.anInt790);
            // System.out.println("782:" + def.anInt782);
            // System.out.println("762:" + def.anInt762);
            // System.out.println("777:" + def.anInt777);
            // System.out.println("802:" + def.anInt802);
            // System.out.println("752:" + def.anInt752);
            // System.out.println("767:" + def.anInt767);
            // System.out.println("758:" + def.anInt758);
            /*
             * System.out.println("761:" + def.femaleModelId);
			 * System.out.println("776:" + def.anInt776);
			 * System.out.println("803:" + def.anInt803);
			 * System.out.println("773:" + def.anInt773);
			 * System.out.println("768:" + def.anInt768);
			 * System.out.println("800:" + def.anInt800);
			 * System.out.println("789:" + def.anInt789);
			 * System.out.println("791:" + def.anInt791);
			 */

			/*
             * if(def.itemId == 4151) System.out.println("793:" +
			 * def.maleModelId); if(def.itemId == 4151)
			 * System.out.println("794:" + def.anInt794); if(def.itemId == 4151
			 * && def.aShortArray751 != null) { for(int i = 0; i <
			 * def.aShortArray751.length; i ++) System.out.println("array751 " +
			 * i + " : " + def.aShortArray751[i]); } if(def.itemId == 4151 &&
			 * def.aShortArray765 != null) { for(int i = 0; i <
			 * def.aShortArray765.length; i ++) System.out.println("array765 " +
			 * i + " : " + def.aShortArray765[i]); } if(def.itemId == 4151)
			 * System.out.println("775:" + def.anInt776); if(def.itemId == 4151)
			 * System.out.println("802:" + def.anInt802); if(def.itemId == 4151)
			 * System.out.println("752:" + def.anInt752); if(def.itemId == 4151)
			 * System.out.println("792:" + def.offsetX); if(def.itemId == 4151
			 * && def.aShortArray772 != null) { for(int i = 0; i <
			 * def.aShortArray772.length; i ++) System.out.println("array772 " +
			 * i + " : " + def.aShortArray772[i]); }
			 */

        }
        return def;
    }

    public ItemDefinition() {
        this.name = Class40.nullString;
        this.maleEquipZ = 0;
        this.anInt784 = 0;
        this.anInt769 = -1;
        this.anInt796 = -1;
        this.anInt791 = -1;
        this.femaleEquipX = 0;
        this.anInt780 = 128;
        this.anInt767 = -1;
        this.anInt758 = -1;
        this.anInt768 = 0;
        this.anInt762 = -1;
        this.anInt795 = -1;
        this.femaleModelId = -1;
        this.maleArmsId = -1;
        this.offsetY = 0;
        this.rotationY = 0;
        this.rotationX = 0;
        this.anInt800 = 0;
        this.stackable = 0;
        this.anInt789 = -1;
        this.femaleArmsId = -1;
        this.anInt788 = -1;
        this.anInt797 = 128;
        this.members = false;
        this.femaleEquipZ = 0;
        this.offsetX = 0;
        this.anInt803 = -1;
        this.femaleEquipY = 0;
        this.maleModelId = -1;
        this.groundActions = new Class1008[]{null, null,
                Class3_Sub13_Sub33.takeClass1008, null, null};
        this.anInt805 = 128;
        this.anInt790 = 0;
        this.maleEquipY = 0;
        this.invyActions = new Class1008[]{null, null, null, null,
                Class140_Sub3.dropjString};
        this.modelZoom = 2000;
        this.aBoolean807 = false;
    }

    static {
        int var0 = 0;

        for (int var1 = 0; var1 < 99; ++var1) {
            int var2 = 1 + var1;
            int var3 = (int) (Math.pow(2.0D, var2 / 7.0D) * 300.0D + var2);
            var0 += var3;
            xpForLevel[var1] = var0 / 4;
        }

        actionColor2 = Class943.create("green:");
        actionColor = Class943.create("green:");
    }
}

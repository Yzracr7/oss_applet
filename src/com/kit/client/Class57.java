package com.kit.client;
final class Class57 {
	
   int anInt896 = 0;
   static int[] anIntArray898 = new int[2500];
   int anInt899 = 2048;
   static int anInt902 = 100;
   static int[] anIntArray904 = new int[200];
   static int anInt906;
   int anInt907 = 0;
   int anInt908 = 2048;

	final void decode(ByteBuffer class966) {
		while (true) {
			int configCode = class966.readUnsignedByte();
			if (configCode == 0) {
				return;
			}

			this.decode(class966, configCode);
		}
	}

	private final void decode(ByteBuffer class966, int configCode) {
		if (configCode == 1) {
			this.anInt896 = class966.readUnsignedByte();
		} else if (configCode == 2) {
			this.anInt908 = class966.aInteger233();
		} else if (configCode == 3) {
			this.anInt899 = class966.aInteger233();
		} else if (configCode == 4) {
			this.anInt907 = class966.aLong_1884();
		}
	}

	static final Class57 method1401(int var1) {
		Class57 var2 = (Class57) Class128.aClass93_1683.get((long) var1);
		if (var2 != null) {
			return var2;
		} else {
			byte[] var3 = Class1004.aClass153_737.getFile(31, var1);
			var2 = new Class57();
			if (var3 != null) {
				var2.decode(new ByteBuffer(var3));
			}

			Class128.aClass93_1683.put(var2, (long) var1);
			return var2;
		}
	}

	public static void method1192() {
		anIntArray898 = null;
		anIntArray904 = null;
		Class922.compassSprite = null;
	}

}

package com.kit.client;

final class Class1227 extends Class1042 {

    protected int id;
    protected int[] anIntArray2455;
    protected static int anInt2456;
    protected static float aFloat2457;
    protected int[][] skins;
    protected int amount;
    protected boolean[] aBooleanArray2463;
    protected int[] types;
    protected static Class1008 loading_completion = Class943.create("");

    public static void method380(int var0) {
        try {
            loading_completion = null;
            if (var0 != -29113) {
                method381();
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "jm.B(" + var0 + ')');
        }
    }

    static final void method381() {
        if (-1 != Class1143.mainScreenInterface) {
            Class1098.method1160(Class1143.mainScreenInterface);
        }
        for (int var1 = 0; var1 < Class3_Sub28_Sub3.anInt3557; ++var1) {
            if (Class921.interfacesToRefresh[var1]) {
                Class163_Sub1_Sub1.aBooleanArray4008[var1] = true;
            }
            Class1017_2.aBooleanArray1712[var1] = Class921.interfacesToRefresh[var1];
            Class921.interfacesToRefresh[var1] = false;
        }
        Class53.anInt865 = -1;
        Class99.aClass11_1402 = null;
        Class3_Sub23.anInt2535 = Class1134.loopCycle;
        if (Class1012.aBoolean_617) {
            Class986.aBoolean47 = true;
        }
        Class1002.anInt2567 = -1;
        if (Class1143.mainScreenInterface != -1) {
            Class3_Sub28_Sub3.anInt3557 = 0;
            Class8.method841(true);
        }
        if (Class1012.aBoolean_617) {
            Class920.method_233();
        } else {
            Class1023.initDefaultBounds();
        }
        Class989.anInt1446 = 0;
    }

    Class1227(int var1, byte[] var2) {
        try {
            this.id = var1;
            ByteBuffer var3 = new ByteBuffer(var2);
            this.amount = var3.readUnsignedByte();
            this.skins = new int[this.amount][];
            this.types = new int[this.amount];
            this.aBooleanArray2463 = new boolean[this.amount];
            this.anIntArray2455 = new int[this.amount];

            int var4;
            for (var4 = 0; ~var4 > ~this.amount; ++var4) {
                this.types[var4] = var3.readUnsignedByte();
            }
            for (var4 = 0; var4 < this.amount; ++var4) {
                this.anIntArray2455[var4] = 65535;//var3.aInteger233();
            }
            for (var4 = 0; var4 < this.amount; ++var4) {
                this.skins[var4] = new int[var3.readUnsignedByte()];
            }
            for (var4 = 0; this.amount > var4; ++var4) {
                for (int var5 = 0; ~var5 > ~this.skins[var4].length; ++var5) {
                    this.skins[var4][var5] = var3.readUnsignedByte();
                }
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "jm.<init>(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ')');
        }
    }
}

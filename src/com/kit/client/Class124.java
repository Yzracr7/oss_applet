package com.kit.client;

final class Class124 {

   static Class1017_2 aClass130_1659 = new Class1017_2(512);
   static Class1027 aClass153_1661;


   public static void method1744(boolean var0) {
      try {
         aClass130_1659 = null;
         aClass153_1661 = null;
         if(!var0) {
            aClass130_1659 = (Class1017_2)null;
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "rb.A(" + var0 + ')');
      }
   }

   static final void method1745(int var0) {
      try {
         for(int var1 = var0; ~var1 > -105; ++var1) {
            for(int var2 = 0; 104 > var2; ++var2) {
               Class163_Sub1_Sub1.anIntArrayArray4010[var1][var2] = 0;
            }
         }

      } catch (RuntimeException var3) {
         throw Class1134.method1067(var3, "rb.B(" + var0 + ')');
      }
   }

   static final void method1746(boolean var0) {
	   Class75_Sub4.method1352(Class1013.canvasHei, var0, -1, Class1143.mainScreenInterface, Class23.canvasWid);
   }

   static final Class3_Sub28_Sub3 method1747(ByteBuffer var0, boolean var1) {
      try {
         Class3_Sub28_Sub3 var2 = new Class3_Sub28_Sub3(var0.class_91033(), var0.class_91033(), var0.aInteger233(), var0.aInteger233(), var0.getInt(), var0.readUnsignedByte() == 1, var0.readUnsignedByte());
         int var3 = var0.readUnsignedByte();

         for(int var4 = 0; ~var4 > ~var3; ++var4) {
            var2.aClass61_3560.insertBack(new Class3_Sub21(var0.aInteger233(), var0.aInteger233(), var0.aInteger233(), var0.aInteger233()));
         }

         var2.method538();
         return var2;
      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "rb.D(" + (var0 != null?"{...}":"null") + ',' + var1 + ')');
      }
   }

}

package com.kit.client;

final class Class933 extends CanvasBuffer {

    static Class1027 aClass153_3214;
    static int[][] anIntArrayArray3215 = new int[][]{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1}, {1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, {0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1}, {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1}, {1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1}};
    static int anInt3216 = 0;
    private int anInt3217 = '\u8000';
    static int[] anIntArray3218 = new int[]{1, 4};


    static final void method255(int var0, int var1) {
        Class1042_3 var3 = Class3_Sub24_Sub3.putInterfaceChange(1, var0);
        var3.add();
        var3.intValue = var1;
    }

    public Class933() {
        super(3, false);
    }

    final int[][] getColorOutput(int var1, int var2) {
        try {
            if (var1 != -1) {
                aClass153_3214 = (Class1027) null;
            }

            int[][] var3 = this.aClass97_2376.method1594((byte) 4, var2);
            if (this.aClass97_2376.aBoolean1379) {
                int[] var4 = this.method152(1, var2, 32755);
                int[] var5 = this.method152(2, var2, 32755);
                int[] var8 = var3[2];
                int[] var7 = var3[1];
                int[] var6 = var3[0];

                for (int var9 = 0; Class113.anInt1559 > var9; ++var9) {
                    int var10 = (var4[var9] * 255 & 1046259) >> 12;
                    int var11 = var5[var9] * this.anInt3217 >> 12;
                    int var12 = var11 * Class75_Sub2.anIntArray2639[var10] >> 12;
                    int var13 = Class3_Sub13_Sub17.anIntArray3212[var10] * var11 >> 12;
                    int var14 = (var12 >> 12) + var9 & Class922.anInt396;
                    int var15 = Class3_Sub20.anInt2487 & var2 - -(var13 >> 12);
                    int[][] var16 = this.method162(var15, 0, (byte) -117);
                    var6[var9] = var16[0][var14];
                    var7[var9] = var16[1][var14];
                    var8[var9] = var16[2][var14];
                }
            }

            return var3;
        } catch (RuntimeException var17) {
            throw Class1134.method1067(var17, "ke.T(" + var1 + ',' + var2 + ')');
        }
    }

    final int[] getMonochromeOutput(int var1, byte var2) {
        try {
            int var3 = -89 / ((30 - var2) / 36);
            int[] var4 = this.aClass114_2382.method1709(-16409, var1);
            if (this.aClass114_2382.aBoolean1580) {
                int[] var5 = this.method152(1, var1, 32755);
                int[] var6 = this.method152(2, var1, 32755);

                for (int var7 = 0; var7 < Class113.anInt1559; ++var7) {
                    int var9 = this.anInt3217 * var6[var7] >> 12;
                    int var8 = (var5[var7] & 4087) >> 4;
                    int var10 = Class75_Sub2.anIntArray2639[var8] * var9 >> 12;
                    int var11 = Class3_Sub13_Sub17.anIntArray3212[var8] * var9 >> 12;
                    int var12 = Class922.anInt396 & (var10 >> 12) + var7;
                    int var13 = Class3_Sub20.anInt2487 & (var11 >> 12) + var1;
                    int[] var14 = this.method152(0, var13, 32755);
                    var4[var7] = var14[var12];
                }
            }

            return var4;
        } catch (RuntimeException var15) {
            throw Class1134.method1067(var15, "ke.D(" + var1 + ',' + var2 + ')');
        }
    }

    final void decode(int var1, ByteBuffer var2, boolean var3) {
        try {
            if (~var1 != -1) {
                if (var1 == 1) {
                    this.aBoolean2375 = ~var2.readUnsignedByte() == -2;
                }
            } else {
                this.anInt3217 = var2.aInteger233() << 4;
            }

            if (!var3) {
                anInt3216 = -7;
            }

        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "ke.A(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    static final void method256(int var0, int var1, int var2, int var4) {

        Class1042_3 var5 = Class3_Sub24_Sub3.putInterfaceChange(4, var2);
        var5.add();
        var5.intValue2 = var4;
        var5.intValue3 = var0;
        var5.intValue = var1;
    }

    static final void method257(byte var0) {
        try {
            int var1 = 0;
            if (var0 <= 122) {
                renderPlainTile((Class1015) null, 69, 54, -87, 72, -85, 88, 37, true);
            }

            for (int var2 = 0; -105 < ~var2; ++var2) {
                for (int var3 = 0; ~var3 > -105; ++var3) {
                    if (Class1013.removeRoof((byte) -106, true, var2, var3, Class75_Sub2.class949s, var1)) {
                        ++var1;
                    }

                    if (var1 >= 512) {
                        return;
                    }
                }
            }

        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ke.C(" + var0 + ')');
        }
    }

    public static void method258(int var0) {
        try {
            anIntArray3218 = null;
            anIntArrayArray3215 = (int[][]) null;
            int var1 = -20 % ((-31 - var0) / 39);
            aClass153_3214 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ke.B(" + var0 + ')');
        }
    }

    static final void renderPlainTile(Class1015 class1015, int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8) {
        int var9;
        int var10 = var9 = (var6 << 7) - Class1210.renderX;
        int var11;
        int var12 = var11 = (var7 << 7) - Class3_Sub13_Sub30.renderZ;
        int var13;
        int var14 = var13 = var10 + 128;
        int var15;
        int var16 = var15 = var12 + 128;
        int var17 = Class1134.activeTileHeightMap[var1][var6][var7] - Class992.renderY;
        int var18 = Class1134.activeTileHeightMap[var1][var6 + 1][var7] - Class992.renderY;
        int var19 = Class1134.activeTileHeightMap[var1][var6 + 1][var7 + 1] - Class992.renderY;
        int var20 = Class1134.activeTileHeightMap[var1][var6][var7 + 1] - Class992.renderY;
        int var21 = var12 * var4 + var10 * var5 >> 16;
        var12 = var12 * var5 - var10 * var4 >> 16;
        var10 = var21;
        var21 = var17 * var3 - var12 * var2 >> 16;
        var12 = var17 * var2 + var12 * var3 >> 16;
        var17 = var21;
        if (var12 >= 50) {
            var21 = var11 * var4 + var14 * var5 >> 16;
            var11 = var11 * var5 - var14 * var4 >> 16;
            var14 = var21;
            var21 = var18 * var3 - var11 * var2 >> 16;
            var11 = var18 * var2 + var11 * var3 >> 16;
            var18 = var21;
            if (var11 >= 50) {
                var21 = var16 * var4 + var13 * var5 >> 16;
                var16 = var16 * var5 - var13 * var4 >> 16;
                var13 = var21;
                var21 = var19 * var3 - var16 * var2 >> 16;
                var16 = var19 * var2 + var16 * var3 >> 16;
                var19 = var21;
                if (var16 >= 50) {
                    var21 = var15 * var4 + var9 * var5 >> 16;
                    var15 = var15 * var5 - var9 * var4 >> 16;
                    var9 = var21;
                    var21 = var20 * var3 - var15 * var2 >> 16;
                    var15 = var20 * var2 + var15 * var3 >> 16;
                    if (var15 >= 50) {
                        //Below we replaced << 9 with 9
                        //This is buffer fix for LOW DEFINITION fullscreen. High-def already has this fixed.
                        int var22 = Rasterizer.centerX + (var10 << Class922.log_view_dist) / var12;
                        int var23 = Rasterizer.centerY + (var17 << Class922.log_view_dist) / var12;
                        int var24 = Rasterizer.centerX + (var14 << Class922.log_view_dist) / var11;
                        int var25 = Rasterizer.centerY + (var18 << Class922.log_view_dist) / var11;
                        int var26 = Rasterizer.centerX + (var13 << Class922.log_view_dist) / var16;
                        int var27 = Rasterizer.centerY + (var19 << Class922.log_view_dist) / var16;
                        int var28 = Rasterizer.centerX + (var9 << Class922.log_view_dist) / var15;
                        int var29 = Rasterizer.centerY + (var21 << Class922.log_view_dist) / var15;
                        Rasterizer.alpha = 0;
                        int var30;
                        if ((var26 - var28) * (var25 - var29) - (var27 - var29) * (var24 - var28) > 0) {
                            if (Class3_Sub13_Sub21.mouseClickToTileRequested && Class3_Sub13_Sub4.method185(Class49.mouseClickToTileOffX + Rasterizer.centerX, Class3_Sub13_Sub23_Sub1.anInt4039 + Rasterizer.centerY, var27, var29, var25, var26, var28, var24)) {
                                Class27.clickedTileX = var6;
                                Class1006.clickedTileZ = var7;
                            }

                            if (!Class1012.aBoolean_617 && !var8) {
                                Rasterizer.boundsLeft = false;
                                if (var26 < 0 || var28 < 0 || var24 < 0 || var26 > Rasterizer.endX || var28 > Rasterizer.endX || var24 > Rasterizer.endX) {
                                    Rasterizer.boundsLeft = true;
                                }

                                if (class1015.texture == -1) {
                                    if (class1015.color3 != 12345678) {
                                        Rasterizer.method1154(var27, var29, var25, var26, var28, var24, class1015.color3, class1015.color4, class1015.color2);
                                    }
                                } else if (Class120_Sub30_Sub1.manyGroundTextures) {
                                    if (class1015.flat) {
                                        Rasterizer.drawTexturedTriangle(var27, var29, var25, var26, var28, var24, class1015.color3, class1015.color4, class1015.color2, var10, var14, var9, var17, var18, var21, var12, var11, var15, class1015.texture);
                                    } else {
                                        Rasterizer.drawTexturedTriangle(var27, var29, var25, var26, var28, var24, class1015.color3, class1015.color4, class1015.color2, var13, var9, var14, var19, var21, var18, var16, var15, var11, class1015.texture);
                                    }
                                } else {
                                    var30 = Rasterizer.anClass1226_973.getColorPaletteIndex(class1015.texture);
                                    Rasterizer.method1154(var27, var29, var25, var26, var28, var24, Class1126.method935(var30, class1015.color3), Class1126.method935(var30, class1015.color4), Class1126.method935(var30, class1015.color2));
                                }
                            }
                        }

                        if ((var22 - var24) * (var29 - var25) - (var23 - var25) * (var28 - var24) > 0) {
                            if (Class3_Sub13_Sub21.mouseClickToTileRequested && Class3_Sub13_Sub4.method185(Class49.mouseClickToTileOffX + Rasterizer.centerX, Class3_Sub13_Sub23_Sub1.anInt4039 + Rasterizer.centerY, var23, var25, var29, var22, var24, var28)) {
                                Class27.clickedTileX = var6;
                                Class1006.clickedTileZ = var7;
                            }

                            if (!Class1012.aBoolean_617 && !var8) {
                                Rasterizer.boundsLeft = false;
                                if (var22 < 0 || var24 < 0 || var28 < 0 || var22 > Rasterizer.endX || var24 > Rasterizer.endX || var28 > Rasterizer.endX) {
                                    Rasterizer.boundsLeft = true;
                                }
                                if (class1015.texture == -1) {
                                    if (class1015.color1 != 12345678) {
                                        Rasterizer.method1154(var23, var25, var29, var22, var24, var28, class1015.color1, class1015.color2, class1015.color4);
                                    }
                                } else if (Class120_Sub30_Sub1.manyGroundTextures) {
                                    Rasterizer.drawTexturedTriangle(var23, var25, var29, var22, var24, var28, class1015.color1, class1015.color2, class1015.color4, var10, var14, var9, var17, var18, var21, var12, var11, var15, class1015.texture);
                                } else {
                                    var30 = Rasterizer.anClass1226_973.getColorPaletteIndex(class1015.texture);
                                    Rasterizer.method1154(var23, var25, var29, var22, var24, var28, Class1126.method935(var30, class1015.color1), Class1126.method935(var30, class1015.color2), Class1126.method935(var30, class1015.color4));
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    final void method158(int var1) {
        try {
            if (var1 != 16251) {
                method255(33, 78);
            }

            Class8.method844();
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ke.P(" + var1 + ')');
        }
    }

    static final void renderShapedTile(Class997 class997, int var1, int var2, int var3, int var4, int var5, int var6, boolean bool) {
        int var8 = class997.xVertices.length;

        int i_12_;
        int i_13_;
        int i_14_;
        int i_15_;
        int i_16_;
        for (i_12_ = 0; i_12_ < var8; ++i_12_) {
            i_13_ = class997.xVertices[i_12_] - Class1210.renderX;
            i_14_ = class997.anIntArray615[i_12_] - Class992.renderY;
            i_15_ = class997.anIntArray618[i_12_] - Class3_Sub13_Sub30.renderZ;
            i_16_ = i_15_ * var3 + i_13_ * var4 >> 16;
            i_15_ = i_15_ * var4 - i_13_ * var3 >> 16;
            i_13_ = i_16_;
            i_16_ = i_14_ * var2 - i_15_ * var1 >> 16;
            i_15_ = i_14_ * var1 + i_15_ * var2 >> 16;
            if (i_15_ < 50) {
                return;
            }

            if (class997.anIntArray1621 != null) {
                Class997.anIntArray1640[i_12_] = i_13_;
                Class997.anIntArray1632[i_12_] = i_16_;
                Class997.anIntArray1623[i_12_] = i_15_;
            }

            Class997.anIntArray1630[i_12_] = Rasterizer.centerX + (i_13_ << Class922.log_view_dist) / i_15_;
            Class997.anIntArray1636[i_12_] = Rasterizer.centerY + (i_16_ << Class922.log_view_dist) / i_15_;
        }


        Rasterizer.alpha = 0;
        var8 = class997.anIntArray1634.length;
        for (i_12_ = 0; i_12_ < var8; ++i_12_) {
            i_13_ = class997.anIntArray1634[i_12_];
            i_14_ = class997.anIntArray1633[i_12_];
            i_15_ = class997.anIntArray1641[i_12_];
            i_16_ = Class997.anIntArray1630[i_13_];
            int i_17_ = Class997.anIntArray1630[i_14_];
            int i_18_ = Class997.anIntArray1630[i_15_];
            int i_19_ = Class997.anIntArray1636[i_13_];
            int i_20_ = Class997.anIntArray1636[i_14_];
            int i_21_ = Class997.anIntArray1636[i_15_];
            if ((i_16_ - i_17_) * (i_21_ - i_20_) - (i_19_ - i_20_) * (i_18_ - i_17_) > 0) {
                if (Class3_Sub13_Sub21.mouseClickToTileRequested && Class3_Sub13_Sub4.method185(Class49.mouseClickToTileOffX + Rasterizer.centerX, Class3_Sub13_Sub23_Sub1.anInt4039 + Rasterizer.centerY, i_19_, i_20_, i_21_, i_16_, i_17_, i_18_)) {
                    Class27.clickedTileX = var5;
                    Class1006.clickedTileZ = var6;
                }

                if (!Class1012.aBoolean_617 && !bool) {
                    Rasterizer.boundsLeft = false;
                    if (i_16_ < 0 || i_17_ < 0 || i_18_ < 0 || i_16_ > Rasterizer.endX || i_17_ > Rasterizer.endX || i_18_ > Rasterizer.endX) {
                        Rasterizer.boundsLeft = true;
                    }
                    if (class997.anIntArray1621 == null || class997.anIntArray1621[i_12_] == -1) {
                        if (class997.anIntArray1627[i_12_] != 12345678) {
                            Rasterizer.method1154(i_19_, i_20_, i_21_, i_16_, i_17_, i_18_, class997.anIntArray1627[i_12_], class997.anIntArray1625[i_12_], class997.anIntArray1624[i_12_]);
                        }
                    } else if (Class120_Sub30_Sub1.manyGroundTextures) {
                        if (class997.aBoolean1629) {
                            Rasterizer.drawTexturedTriangle(i_19_, i_20_, i_21_, i_16_, i_17_, i_18_, class997.anIntArray1627[i_12_], class997.anIntArray1625[i_12_], class997.anIntArray1624[i_12_], Class997.anIntArray1640[0], Class997.anIntArray1640[1], Class997.anIntArray1640[3], Class997.anIntArray1632[0],
                                    Class997.anIntArray1632[1], Class997.anIntArray1632[3], Class997.anIntArray1623[0], Class997.anIntArray1623[1], Class997.anIntArray1623[3], class997.anIntArray1621[i_12_]);
                        } else {
                            Rasterizer.drawTexturedTriangle(i_19_, i_20_, i_21_, i_16_, i_17_, i_18_, class997.anIntArray1627[i_12_], class997.anIntArray1625[i_12_], class997.anIntArray1624[i_12_], Class997.anIntArray1640[i_13_], Class997.anIntArray1640[i_14_], Class997.anIntArray1640[i_15_],
                                    Class997.anIntArray1632[i_13_], Class997.anIntArray1632[i_14_], Class997.anIntArray1632[i_15_], Class997.anIntArray1623[i_13_], Class997.anIntArray1623[i_14_], Class997.anIntArray1623[i_15_], class997.anIntArray1621[i_12_]);
                        }
                    } else {
                        final int i_22_ = Rasterizer.anClass1226_973.getColorPaletteIndex(class997.anIntArray1621[i_12_]);
                        Rasterizer.method1154(i_19_, i_20_, i_21_, i_16_, i_17_, i_18_, Class1126.method935(i_22_, class997.anIntArray1627[i_12_]), Class1126.method935(i_22_, class997.anIntArray1625[i_12_]), Class1126.method935(i_22_, class997.anIntArray1624[i_12_]));
                    }
                }
            }
        }

    }

}

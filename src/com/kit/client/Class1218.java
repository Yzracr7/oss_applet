package com.kit.client;

import javax.media.opengl.GL;
import java.nio.FloatBuffer;

final class Class1218 extends Class1126 {

    private static int particleAmount;

    Class1218() {
        new Class17();
        new Class975();
    }

    static final void method1755() {
        GL var0 = Class1012.gl;
        if (var0.isExtensionAvailable("GL_ARB_point_parameters")) {
            float[] var1 = new float[]{1.0F, 0.0F, 5.0E-4F};
            var0.glPointParameterfvARB(33065, var1, 0);
            FloatBuffer var2 = FloatBuffer.allocate(1);
            var0.glGetFloatv(33063, var2);
            float var3 = var2.get(0);
            if (var3 > 1024.0F) {
                var3 = 1024.0F;
            }

            var0.glPointParameterfARB(33062, 1.0F);
            var0.glPointParameterfARB(33063, var3);
        }

        if (var0.isExtensionAvailable("GL_ARB_point_sprite")) {
            ;
        }

    }

    static final void method1756() {
    }

    static final int method1757() {
        return particleAmount;
    }

    static final void method1758(int var0) {
        particleAmount = var0;
    }

    final void method1759() {
    }

    static {
        new Class128(8);
        particleAmount = 2;
        new ByteBuffer(131056);
    }
}

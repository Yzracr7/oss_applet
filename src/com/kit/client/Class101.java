package com.kit.client;

final class Class101 {

    protected static boolean aBoolean1419 = false;
    protected static Class1027 aClass153_1420;
    protected static Class3_Sub24_Sub4 aClass3_Sub24_Sub4_1421;
    protected static Class1046 cacheFile;
    protected static Class1027 soundCacheIndex;
    protected static Class975 aClass61_1424 = new Class975();
    protected static int anInt1425 = 0;
    protected static int[] landscapeArray;
    protected static int anInt1427;
    protected static Class1027 aClass153_1428;
    protected static short[][] aShortArrayArray1429;

    static final void method1607(int var0, int var1, boolean var2, int[] var3, int[] var4) {
        try {
            if (var2) {
                method1608((byte) 106);
            }
            if (~var0 > ~var1) {
                int var5 = (var1 + var0) / 2;
                int var6 = var0;
                int var7 = var3[var5];
                var3[var5] = var3[var1];
                var3[var1] = var7;
                int var8 = var4[var5];
                var4[var5] = var4[var1];
                var4[var1] = var8;

                for (int var9 = var0; ~var1 < ~var9; ++var9) {
                    if (~var3[var9] < ~((var9 & 1) + var7)) {
                        int var10 = var3[var9];
                        var3[var9] = var3[var6];
                        var3[var6] = var10;
                        int var11 = var4[var9];
                        var4[var9] = var4[var6];
                        var4[var6++] = var11;
                    }
                }
                var3[var1] = var3[var6];
                var3[var6] = var7;
                var4[var1] = var4[var6];
                var4[var6] = var8;
                method1607(var0, var6 - 1, var2, var3, var4);
                method1607(var6 + 1, var1, var2, var3, var4);
            }
        } catch (RuntimeException var12) {
            throw Class1134.method1067(var12, "nj.C(" + var0 + ',' + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + (var4 != null ? "{...}" : "null") + ')');
        }
    }

    public static void method1608(byte var0) {
        try {
            aClass153_1428 = null;
            aClass61_1424 = null;
            aShortArrayArray1429 = (short[][]) null;
            landscapeArray = null;
            aClass3_Sub24_Sub4_1421 = null;
            cacheFile = null;
            soundCacheIndex = null;
            if (var0 != 110) {
                method1607(46, 78, true, (int[]) null, (int[]) null);
            }
            aClass153_1420 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "nj.A(" + var0 + ')');
        }
    }

    static final void method1609() {
        Class2.anInterface5Array70 = null;
        Class88.method1455();
    }
}

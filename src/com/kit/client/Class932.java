package com.kit.client;

import java.awt.*;

final class Class932 extends Class3_Sub28_Sub10 {

    protected byte padding;
    protected static Class1008 aClass94_4066 = Class943.create("<br>");
    protected int blockPosition;
    protected Class1009 worker;
    protected static boolean aBoolean4068 = true;
    protected ByteBuffer class966;
    protected static Class949[][][] aClass3_Sub2ArrayArrayArray4070;
    protected static int anInt4073;
    protected static Class1008 aClass94_4071 = Class943.create(" from your friend list first)3");

    final int method586() {
        return (class966 == null ? 0 : class966.offset * 100 / (class966.buffer.length - padding));
    }

    static final Class989[] gatherDisplayModesInformation(Class942 var1) {
        if (!var1.fullscreenImpExist()) {
            return new Class989[0];
        } else {
            Class1124 node = var1.getDisplayModesInformation();
            while (node.status == 0) {
                System.out.println("stuck2");
                Class3_Sub13_Sub34.sleep(10L);
            }
            if (node.status == 2) {
                return new Class989[0];
            } else {
                int[] data = (int[]) node.value;
                Class989[] information = new Class989[data.length >> 2];
                for (int id = 0; id < information.length; ++id) {
                    Class989 instance = new Class989();
                    information[id] = instance;
                    instance.width = data[id << 2];
                    instance.height = data[(id << 2) + 1];
                    instance.bitDepth = data[(id << 2) + 2];
                    instance.refreshRate = data[(id << 2) + 3];
                }
                return information;
            }
        }
    }

    public static void method597(byte var0) {
        try {
            aClass3_Sub2ArrayArrayArray4070 = (Class949[][][]) null;
            aClass94_4066 = null;
            if (var0 < 91) {
                aClass3_Sub2ArrayArrayArray4070 = (Class949[][][]) ((Class949[][][]) null);
            }
            aClass94_4071 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "pm.O(" + var0 + ')');
        }
    }


    public final static int FULLSCREEN = 3;
    public final static int RESIZABLE = 2;
    public final static int FIXED = 1;

    static final void method598(boolean replaceCanvas, int displayMode, boolean close, int var3, int width, int height) {
        if (close) {
            Class1012.close();
        }
        if (displayMode == FULLSCREEN && Class3_Sub13_Sub10.fullscreenFrame == null) {
        } else {
            Object activeFrame;
            activeFrame = Class38.gameClass942.thisApplet;
            Class3_Sub9.anInt2334 = ((Container) activeFrame).getSize().width;
            Class70.anInt1047 = ((Container) activeFrame).getSize().height;
            if (-3 >= ~displayMode) {
                Class23.canvasWid = Class3_Sub9.anInt2334;
                Class1013.canvasHei = Class70.anInt1047;
                Class84.canvasDrawX = 0;
                Class989.canvasDrawY = 0;
            } else {
                if (Class922.clientSize == 0) {
                    Class989.canvasDrawY = 0;
                    Class84.canvasDrawX = (Class3_Sub9.anInt2334 + -765) / 2;
                    Class23.canvasWid = 765;
                    Class1013.canvasHei = 503;
                }
            }
            if ((!Class1012.aBoolean_617) && Class922.clientSize > 0 && Class922.aInteger_544 == 30) {
                Class84.canvasDrawX = 0;
                Class23.canvasWid = Class922.resizeWidth;
                Class1013.canvasHei = Class922.resizeHeight;
            }
            if (Class922.clientSize > 0) {
                Class922.modZoom();
            }
            if (!replaceCanvas) {
                if (Class1012.aBoolean_617) {
                    Class1012.changeCanvasHeight(Class23.canvasWid, Class1013.canvasHei);
                }
                Class1143.canvas.setSize(Class23.canvasWid, Class1013.canvasHei);
                if (Class3_Sub13_Sub7.resizableFrame == activeFrame) {
                } else {
                    Class1143.canvas.setLocation(Class84.canvasDrawX, Class989.canvasDrawY);
                }
            } else {
                Class163_Sub1_Sub1.method2215(Class1143.canvas, -9320);
                Class1017_2.method1783(Class1143.canvas);
                if (null != Class38.mouseWheelHandler) {
                    Class38.mouseWheelHandler.removeMouseWheel(Class1143.canvas);
                }
                Class1015.aClass9221671.addCanvas();
                Class3_Sub13_Sub4.addKeyboardHandler(Class1143.canvas);
                ItemDefinition.addMouseHandler(Class1143.canvas);
                if (Class38.mouseWheelHandler != null) {
                    Class38.mouseWheelHandler.addMouseWheel(Class1143.canvas);
                }
            }

            if (0 == displayMode && -1 > ~var3) {
                Class1012.turnOff(Class1143.canvas);
            }

            if (close && displayMode > 0) {
                Class1143.canvas.setIgnoreRepaint(true);
                if (!Class1021.initOpenGL || Class922.dontEnterGL) {
                    Class32.method995();
                    Class164_Sub1.aClass158_3009 = null;
                    Class164_Sub1.aClass158_3009 = Class3_Sub13_Sub23_Sub1.createGraphicsBuffer(Class1143.canvas, Class922.resizeWidth, Class922.resizeHeight);
                    Class1023.resetPixels();
                    if (5 != Class922.aInteger_544) {
                        Class922.drawTextOnScreen(Class3_Sub13_Sub23.loadingString, false);
                    } else {
                        Class922.secondLoadingBar(true, Class922.getBoldClass1019());
                    }
                    try {
                        Graphics var11 = Class1143.canvas.getGraphics();
                        Class164_Sub1.aClass158_3009.drawGraphics(var11, 0, 0);
                    } catch (Exception var9) {
                        ;
                    }
                    Class80.method1396();
                    if (-1 != ~var3) {
                        Class164_Sub1.aClass158_3009 = null;
                    } else {
                        Class164_Sub1.aClass158_3009 = Class3_Sub13_Sub23_Sub1.createGraphicsBuffer(Class1143.canvas, Class922.resizeWidth, Class922.resizeHeight);
                    }
                    Class1124 var13 = Class38.gameClass942.method1444(Class1015.aClass9221671.getClass());

                    while (var13.status == 0) {
                        Class3_Sub13_Sub34.sleep(100L);
                    }
                    if (1 == var13.status && !Class922.dontEnterGL) {
                        Class1021.initOpenGL = true;
                    }
                }
                if (Class1021.initOpenGL && !Class922.dontEnterGL) {
                    System.out.println("Attempting to run open-gl high definition..");
                    Class1012.startToolkit(Class1143.canvas, 2 * Class921.antiAliasing);
                    Class922.log_view_dist = 9;
                } else {
                    if (Class922.clientSize > 0) {
                        Class922.log_view_dist = 10;
                    } else {
                        Class922.log_view_dist = 9;
                    }
                }
            }
            if (!Class1012.aBoolean_617 && 0 < displayMode) {
                method598(true, 0, true, var3, -1, -1);
            } else {
                if (~displayMode < -1 && -1 == ~var3) {
                    Class17.aThread409.setPriority(5);
                    Class164_Sub1.aClass158_3009 = null;
                    Class1049.method1935();
                    ((Class945) Rasterizer.anClass1226_973).method1619(200, -1);
                    if (Class989.aBoolean1441) {
                        Rasterizer.method1137(0.7F);
                    }
                    CanvasBuffer.method165(-7878);
                } else if (0 == displayMode && var3 > 0) {
                    Class17.aThread409.setPriority(1);
                    Class164_Sub1.aClass158_3009 = Class3_Sub13_Sub23_Sub1.createGraphicsBuffer(Class1143.canvas, Class922.resizeWidth, Class922.resizeHeight);
                    Class1049.method1938();
                    Class1218.method1756();
                    ((Class945) Rasterizer.anClass1226_973).method1619(20, -1);
                    if (Class989.aBoolean1441) {
                        if (1 == Class3_Sub28_Sub10.brightness) {
                            Rasterizer.method1137(0.9F);
                        }
                        if (2 == Class3_Sub28_Sub10.brightness) {
                            Rasterizer.method1137(0.8F);
                        }
                        if (3 == Class3_Sub28_Sub10.brightness) {
                            Rasterizer.method1137(0.7F);
                        }
                        if (4 == Class3_Sub28_Sub10.brightness) {
                            Rasterizer.method1137(0.6F);
                        }
                    }
                    Class3_Sub11.method144();
                    CanvasBuffer.method165(-7878);
                }
                Class47.aBoolean742 = !Class1001.visibleLevels();
                if (close && !Class922.dontEnterGL) {
                    Class3_Sub20.method389(false);
                }
                if (~displayMode <= -3) {
                    Class3_Sub15.aBoolean2427 = true;
                } else {
                    Class3_Sub15.aBoolean2427 = false;
                }
                if (-1 != Class1143.mainScreenInterface) {
                    Class124.method1746(true);
                }
                for (int var12 = 0; var12 < 100; ++var12) {
                    Class921.interfacesToRefresh[var12] = true;
                }
                if (Class1012.aBoolean_617 && Class922.justChangedSize) {
                    Class922.forceRefresh();
                    Class922.justChangedSize = false;
                }
                Class3_Sub13_Sub10.fullRedraw = true;
            }
        }
        Class922.dontEnterGL = false;
    }

    final byte[] getBuffer(boolean var1) {
        try {
            if (!aBoolean3632 && ~class966.offset <= ~(-padding + class966.buffer.length)) {
                if (var1) {
                    method586();
                }
                return class966.buffer;
            } else {
                throw new RuntimeException("RTE");
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "pm.E(" + var1 + ')');
        }
    }
}

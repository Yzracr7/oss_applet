package com.kit.client;

final class Class3_Sub13_Sub31 extends CanvasBuffer {

    static Class93 aClass93_3369 = new Class93(64);
    protected static Class1008 old_markers_string = Class943.create("hitmarks");
    protected static Class1008 new_markers_string = Class943.create("new_markers");
    protected static Class1008 new_markers_string2dark = Class943.create("new_markers2dark");
    protected static Class1008 new_markers_string2hq = Class943.create("new_markers2");
    protected static Class1008 soaking_string = Class943.create("soaking");
    protected static Class1008 healmarkString = Class943.create("healmark");
    protected static Class1008 crit_font_string = Class943.create("crit_full");
    protected static Class1008 hit_font_string = Class943.create("hit_full");
    private static Class1008 aClass94_3371 = Class943.create("Sat");
    private static Class1008 aClass94_3372 = Class943.create("Mon");
    protected static Class957[] pkIconSprites;
    private static Class1008 aClass94_3374 = Class943.create("Fri");
    protected static int anInt3375 = 0;
    protected static int anInt3377 = 7759444;
    private static Class1008 aClass94_3378 = Class943.create("Wed");
    private static Class1008 aClass94_3379 = Class943.create("Thu");
    private static Class1008 aClass94_3380 = Class943.create("Tue");
    private static Class1008 aClass94_3381 = Class943.create("Sun");
    protected static Class1008 aClass94_3382 = Class943.create("(U0a )2 in: ");
    protected static Class1008[] aClass94Array3376 = new Class1008[]{aClass94_3381, aClass94_3372, aClass94_3380, aClass94_3378, aClass94_3379, aClass94_3374, aClass94_3371};

    public static void method317(int var0) {
        try {
            aClass94Array3376 = null;
            aClass94_3378 = null;
            aClass94_3374 = null;
            aClass94_3382 = null;
            aClass94_3381 = null;
            aClass94_3372 = null;
            aClass94_3380 = null;
            if (var0 != 7759444) {
                method317(72);
            }
            aClass93_3369 = null;
            aClass94_3379 = null;
            pkIconSprites = null;
            old_markers_string = null;
            aClass94_3371 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "rl.C(" + var0 + ')');
        }
    }

    final int[] getMonochromeOutput(int var1, byte var2) {
        try {
            int var3 = -96 / ((var2 - 30) / 36);
            return Class945.anIntArray2125;
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "rl.D(" + var1 + ',' + var2 + ')');
        }
    }

    public Class3_Sub13_Sub31() {
        super(0, true);
    }

    static final void method318(int var0) {
        try {
            Class990 var1 = (Class990) Class3_Sub13_Sub6.aClass61_3075.getFirst();
            if (var0 != 7759444) {
                aClass94_3379 = (Class1008) null;
            }
            for (; null != var1; var1 = (Class990) Class3_Sub13_Sub6.aClass61_3075.getNext()) {
                if (var1.anInt2259 != -1) {
                    var1.unlink();
                } else {
                    var1.anInt2261 = 0;
                    Class132.method1798(56, var1);
                }
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "rl.B(" + var0 + ')');
        }
    }
}

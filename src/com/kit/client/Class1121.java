package com.kit.client;
import java.lang.ref.SoftReference;

final class Class1121 extends Class956 {

   private SoftReference softReference;

   final Object get() {
      return this.softReference.get();
   }

   final boolean isSoftReference() {
      return true;
   }

   Class1121(Object var1) {
      this.softReference = new SoftReference(var1);
   }
}

package com.kit.client;

/* Class20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class1044 implements Runnable {
    
	public final void run() {
		try {
			for (;;) {
				Class1028 class3_sub10;
				synchronized (Class922.updateServerList) {
					class3_sub10 = (Class1028) Class922.updateServerList.getFirst();
				}
				if (class3_sub10 == null) {
					Class3_Sub13_Sub34.sleep(100L);
					synchronized (Class922.anObject821) {
						if (Class922.anInt1465 <= 1) {
							Class922.anInt1465 = 0;
							Class922.anObject821.notifyAll();
						} else {
							Class922.anInt1465--;
							continue;
						}
						break;
					}
				}
				do {
					if (class3_sub10.anInt1246 != 0) {
						if (class3_sub10.anInt1246 != 1)
							break;
						class3_sub10.buffer = class3_sub10.class988.get((int) class3_sub10.hash);
						synchronized (Class922.updateServerList) {
							Class922.aClass60_2164.insertBack(class3_sub10);
							break;
						}
					}
					class3_sub10.class988.method1050((int)class3_sub10.hash, class3_sub10.buffer.length, class3_sub10.buffer);
					synchronized (Class922.updateServerList) {
						class3_sub10.unlink();
					}
				} while (false);
				synchronized (Class922.anObject821) {
					if (Class922.anInt1465 <= 1) {
						Class922.anInt1465 = 0;
						Class922.anObject821.notifyAll();
						break;
					}
					Class922.anInt1465 = 600;
				}
			}
		} catch (Exception exception) {
		}
    }
}

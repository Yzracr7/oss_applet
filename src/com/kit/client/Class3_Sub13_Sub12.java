package com.kit.client;

import java.util.zip.CRC32;

final class Class3_Sub13_Sub12 extends CanvasBuffer {

    private static Class1008 aClass94_3141 = Class943.create("Loaded config");
    protected static Class1008 aClass94_3142 = aClass94_3141;
    protected static CRC32 aCRC32_3143 = new CRC32();
    protected static Class1008 aClass94_3145 = Class943.create(")1 ");

    static final void method223(boolean var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        try {
            if (~var4 == ~var7) {
                Class1007.method1460(var1, var3, (byte) -113, var6, var7, var2, var5);
            } else {
                if (!var0) {
                    executeOnLaunchScript(87);
                }
                if (~(var2 - var7) <= ~Class101.anInt1425 && var7 + var2 <= Class3_Sub28_Sub18.anInt3765 && ~Class159.anInt2020 >= ~(var3 - var4) && Class57.anInt902 >= var3 + var4) {
                    Class161.method2200(var6, var2, var3, var5, var7, 95, var4, var1);
                } else {
                    Class3_Sub13_Sub34.method329(var7, var6, var5, var1, (byte) -54, var3, var2, var4);
                }
            }
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "fn.C(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ')');
        }
    }

    public Class3_Sub13_Sub12() {
        super(1, true);
    }

    static final void drawOldScroller(int scrollPosition, int maxScrollVertical, int x, int y, int barHeight) {
        Class1031.scrollBarSprite[0].drawSprite(x, y);
        Class1031.scrollBarSprite[1].drawSprite(x, -16 + barHeight + y);
        int var6 = barHeight * (barHeight + -32) / maxScrollVertical;
        if (var6 < 8) {
            var6 = 8;
        }

        int var7 = scrollPosition * (-var6 + -32 + barHeight) / (maxScrollVertical + -barHeight);
        if (Class1012.aBoolean_617) {
            Class920.method_574(x, 16 + y, 16, -32 + barHeight, Class3_Sub23.anInt2530);
            Class920.method_574(x, 16 + y + var7, 16, var6, Class25.anInt486);
            Class920.method_203(x, var7 + (y - -16), var6, Class3_Sub13_Sub31.anInt3377);
            Class920.method_203(x + 1, var7 + 16 + y, var6, Class3_Sub13_Sub31.anInt3377);
            Class920.method_433(x, var7 + 16 + y, 16, Class3_Sub13_Sub31.anInt3377);
            Class920.method_433(x, var7 + y + 17, 16, Class3_Sub13_Sub31.anInt3377);
            Class920.method_203(15 + x, y + (16 - -var7), var6, Class949.anInt2243);
            Class920.method_203(14 + x, 17 + (y - -var7), -1 + var6, Class949.anInt2243);
            Class920.method_433(x, var6 + 15 + y + var7, 16, Class949.anInt2243);
            Class920.method_433(x + 1, y + 14 - -var7 + var6, 15, Class949.anInt2243);
        } else {
            Class1023.fillRect(x, 16 + y, 16, -32 + barHeight, Class3_Sub23.anInt2530);
            Class1023.fillRect(x, var7 + (y - -16), 16, var6, Class25.anInt486);
            Class1023.drawVerticalLine(x, var7 + y + 16, var6, Class3_Sub13_Sub31.anInt3377);
            Class1023.drawVerticalLine(x + 1, var7 + 16 + y, var6, Class3_Sub13_Sub31.anInt3377);
            Class1023.drawHorizontalLine(x, y + (16 - -var7), 16, Class3_Sub13_Sub31.anInt3377);
            Class1023.drawHorizontalLine(x, 17 + y + var7, 16, Class3_Sub13_Sub31.anInt3377);
            Class1023.drawVerticalLine(x - -15, var7 + 16 + y, var6, Class949.anInt2243);
            Class1023.drawVerticalLine(14 + x, y - -17 - -var7, -1 + var6, Class949.anInt2243);
            Class1023.drawHorizontalLine(x, var6 + 15 + y + var7, 16, Class949.anInt2243);
            Class1023.drawHorizontalLine(1 + x, var6 + y - (-14 + -var7), 15, Class949.anInt2243);
        }

    }

    static final void method225(int var0, Class1034 inter) {
        //NEW SCROLLER
        try {
            Class1034 var2 = Class1223.method2273(inter);
            if (var0 != 14) {
                resetVariables(true);
            }

            int width;
            int height;
            if (null == var2) {
                height = Class1013.canvasHei;
                width = Class23.canvasWid;
            } else {
                height = var2.scrollbarHeight;
                width = var2.scrollbarWidth;
            }

            //Draw new scroller - both?
            Class3_Sub28_Sub11.method603(height, 13987, width, inter, false);
            Class62.method1224(inter, height, width);
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "fn.Q(" + var0 + ',' + (inter != null ? "{...}" : "null") + ')');
        }
    }

    static final void executeOnLaunchScript(int var0) {
        if (var0 != -1) {
            //  115 interface id: 35913761
            //  115 interface id: 35913979
            if (Class970.method_948(var0)) {
                Class1034[] var2 = Class1031.interfaceCache[var0];

                for (int var3 = 0; var3 < var2.length; ++var3) {
                    Class1034 var4 = var2[var3];
                    if (null != var4.onLaunchListener) {
                        Class1048 var5 = new Class1048();
                        var5.objectData = var4.onLaunchListener;
                        var5.interfaceId = var0;
                        var5.aClass11_2449 = var4;
                        Object objects[] = var5.objectData;

                        //   int scriptId = ((Integer) objects[0]).intValue();
                        //if(scriptId == 115) {
                        //	   if(client.clientSize == false)
                        //		   client.sendGameframe(client.gameFrame, false);
                        //	   System.out.println("(GFS)115 interface id: " + var4.uid + ", var0: " + var0);
                        //  }
                        Class951.launchClientscript(2000000, var5);
                    }
                }

            }
        }
    }
   
 /*  static final void refreshMainScreen() {
       jInterface[] var2 = SceneGraphNode.interfaceCache[548];

	   for(int var3 = 0; var3 < var2.length; ++var3) {
		   jInterface var4 = var2[var3];
		   if(null != var4.onLaunchListener) {
			   InterfaceListener var5 = new InterfaceListener();
			   var5.objectData = var4.onLaunchListener;
			   var5.interfaceId = 548;
			   var5.aClass11_2449 = var4;
			   Object objects[] =var5.objectData;
			   
			   int scriptId = ((Integer) objects[0]).intValue();
			   if(scriptId == 115) {
				   System.out.println("115 interface id: " + var4.uid + ", var0: 548");
			   }
			   Class951.method1104(2000000, var5);
		   }
		   if(Class7.getInterface(var4.uid) != null)
			   Class20.refreshInterface(Class7.getInterface(var4.uid));
	   }
   }*/

    public static void resetVariables(boolean var0) {
        try {
            aClass94_3142 = null;
            aClass94_3145 = null;
            aCRC32_3143 = null;
            if (!var0) {
                aClass94_3142 = (Class1008) null;
            }

            aClass94_3141 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "fn.O(" + var0 + ')');
        }
    }

    final int[] getMonochromeOutput(int var1, byte var2) {
        try {
            int var3 = -42 / ((30 - var2) / 36);
            int[] var10 = this.aClass114_2382.method1709(-16409, var1);
            if (this.aClass114_2382.aBoolean1580) {
                int[][] var4 = this.method162(var1, 0, (byte) -126);
                int[] var5 = var4[0];
                int[] var7 = var4[2];
                int[] var6 = var4[1];

                for (int var8 = 0; ~var8 > ~Class113.anInt1559; ++var8) {
                    var10[var8] = (var7[var8] + var5[var8] + var6[var8]) / 3;
                }
            }

            return var10;
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "fn.D(" + var1 + ',' + var2 + ')');
        }
    }

    static final int method228(Class1027 var0, Class1027 var1) {
        int var3 = 0;
        if (var0.method2144(Class75_Sub2.fontP12Id)) {
            ++var3;
        }
        if (var0.method2144(Class3_Sub13_Sub11.fontB12Id)) {
            ++var3;
        }
        if (var1.method2144(Class75_Sub2.fontP12Id)) {
            ++var3;
        }
        if (var1.method2144(Class3_Sub13_Sub11.fontB12Id)) {
            ++var3;
        }
        try {
            if (var0.method2144(Class1025.fontP11Id)) {
                ++var3;
            }
            if (var1.method2144(Class1025.fontP11Id)) {
                ++var3;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return var3;
    }

}

package com.kit.client;

/*final class Class1015 {

   static int[] intArguments;
   int anInt1663;
   int anInt1664;
   static int anInt1665;
   static Class1245 aClass13_1666 = new Class1245();
   int anInt1667;
   static int anInt1668 = -1;
   int texture;
   static client aClass9221671;
   static int anInt1672 = 0;
   int minimapColor;
   boolean aBoolean1674 = true;
   int anInt1675;
   static int anInt1676 = 0;
   public boolean textured;
   
   public static void method1751() {
	   aClass13_1666 = null;
	   aClass9221671 = null;
	   intArguments = null;
   }

   Class1015(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7) {
	   this.anInt1667 = var2;
	   this.anInt1664 = var3;
	   this.anInt1675 = var1;
	   this.minimapColor = var6;
	   this.aBoolean1674 = var7;
	   this.anInt1663 = var4;
	   this.texture = var5;
	   this.textured = texture != -1;
   }

}*/


final class Class1015 {

    static int[] intArguments;
    int color4;
    int color3;
    static int anInt1665;
    static Class1245 aClass13_1666 = new Class1245();
    int color2;
    static int anInt1668 = -1;
    int texture;
    static Class922 aClass9221671;
    static int anInt1672 = 0;
    int minimapColor;
    boolean flat = true;
    int color1;
    static int mouseX2 = 0;
    boolean textured = false;

    public static void method1751() {
        aClass13_1666 = null;
        aClass9221671 = null;
        intArguments = null;
    }

    Class1015(int color1, int color2, int color3, int color4, int texture, int minimapColor, boolean flat) {
        this.color1 = color1; //1675
        this.color2 = color2;//1667
        this.color3 = color3;//1664
        this.color4 = color4;//1663
        this.minimapColor = minimapColor;
        this.flat = flat;
        this.texture = texture;
        this.textured = texture != -1;
    }

}
/*

final class Class1015 {

   static int[] intArguments;
   int anInt1663;
   int anInt1664;
   static int anInt1665;
   static Class1245 aClass13_1666 = new Class1245();
   int anInt1667;
   static int anInt1668 = -1;
   int anInt1670;
   static client aClass9221671;
   static int anInt1672 = 0;
   int minimapColor;
   boolean aBoolean1674 = true;
   int anInt1675;
   static int anInt1676 = 0;

   public static void method1751() {
	   aClass13_1666 = null;
	   aClass9221671 = null;
	   intArguments = null;
   }

   Class1015(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7) {
	   this.anInt1667 = var2;
	   this.anInt1664 = var3;
	   this.anInt1675 = var1;
	   this.minimapColor = var6;
	   this.aBoolean1674 = var7;
	   this.anInt1663 = var4;
	   this.anInt1670 = var5;
   }

}*/

package com.kit.client;

final class Class132 {

   static int anInt1734 = 0;
   static Class1027 aClass153_1735;
   static int anInt1736;
   static int anInt1737 = 1;
   static Class1008 yellowColor = Class943.create("<col=ffff00>");
   static Class1008[] aClass94Array1739 = new Class1008[1000];
   static int anInt1740 = 0;
   static int anInt1741;


   static final void method1798(int var0, Class990 var1) {
      try {
         long var2 = 0L;
         int var4 = -1;
         if(var0 <= 17) {
            anInt1740 = -43;
         }

         int var5 = 0;
         if(-1 == ~var1.anInt2263) {
            var2 = Class157.method2174(var1.plane, var1.anInt2264, var1.anInt2248);
         }

         int var6 = 0;
         if(-2 == ~var1.anInt2263) {
            var2 = Class80.method1395(var1.plane, var1.anInt2264, var1.anInt2248);
         }

         if(var1.anInt2263 == 2) {
            var2 = Class955.method557(var1.plane, var1.anInt2264, var1.anInt2248);
         }

         if(~var1.anInt2263 == -4) {
            var2 = Class949.method104(var1.plane, var1.anInt2264, var1.anInt2248);
         }

         if(var2 != 0L) {
            var4 = Integer.MAX_VALUE & (int)(var2 >>> 32);
            var6 = (int)var2 >> 20 & 3;
            var5 = ((int)var2 & 516214) >> 14;
         }

         var1.anInt2254 = var4;
         var1.anInt2253 = var5;
         var1.anInt2257 = var6;
      } catch (RuntimeException var7) {
         throw Class1134.method1067(var7, "sf.B(" + var0 + ',' + (var1 != null?"{...}":"null") + ')');
      }
   }

   static final void method1799(byte var0, Class1027 var1) {
      try {
         Class3_Sub13_Sub7.aClass153_3098 = var1;
         int var2 = 113 / ((1 - var0) / 63);
      } catch (RuntimeException var3) {
         throw Class1134.method1067(var3, "sf.C(" + var0 + ',' + (var1 != null?"{...}":"null") + ')');
      }
   }

   public static void method1800(byte var0) {
      try {
         aClass94Array1739 = null;
         yellowColor = null;
         aClass153_1735 = null;
         if(var0 <= 52) {
            aClass94Array1739 = (Class1008[])null;
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "sf.A(" + var0 + ')');
      }
   }

   static final void method1801(byte var0) {
      try {
         if(var0 >= -94) {
            method1799((byte)-90, (Class1027)null);
         }

         int var1 = Class922.getBoldClass1019().method682(Class75_Sub4.aClass94_2667);

         int var2;
         int var3;
         for(var2 = 0; Class3_Sub13_Sub34.contextOptionsAmount > var2; ++var2) {
            var3 = Class922.getBoldClass1019().method682(ByteBuffer.getContextOption(var2, true));
            if(var3 > var1) {
               var1 = var3;
            }
         }

         var2 = 15 * Class3_Sub13_Sub34.contextOptionsAmount + 21;
         int var4 = Class38_Sub1.clickY;
         var1 += 8;
         var3 = Class981.clickX + -(var1 / 2);
         if(~(var4 + var2) < ~Class1013.canvasHei) {
            var4 = Class1013.canvasHei + -var2;
         }

         if(Class23.canvasWid < var3 + var1) {
            var3 = -var1 + Class23.canvasWid;
         }

         if(-1 < ~var3) {
            var3 = 0;
         }

         if(~var4 > -1) {
            var4 = 0;
         }

         if(-2 == ~Class992.anInt3660) {
            if(~Class981.clickX == ~Class3_Sub13_Sub39.anInt3460 && ~Class1223.anInt2099 == ~Class38_Sub1.clickY) {
               Class3_Sub28_Sub1.anInt3537 = Class3_Sub13_Sub34.contextOptionsAmount * 15 - -(!Class1027.aBoolean1951?22:26);
               Class992.anInt3660 = 0;
               Class3_Sub13_Sub33.anInt3395 = var4;
               Class927.anInt1462 = var3;
               Class38_Sub1.drawContextMenu = true;
               Class3_Sub28_Sub3.anInt3552 = var1;
            }
         } else if(~Class981.clickX == ~Class163_Sub1.clickX2 && ~Class38_Sub1.clickY == ~Class38_Sub1.clickY2) {
            Class927.anInt1462 = var3;
            Class992.anInt3660 = 0;
            Class3_Sub28_Sub3.anInt3552 = var1;
            Class3_Sub13_Sub33.anInt3395 = var4;
            Class3_Sub28_Sub1.anInt3537 = (Class1027.aBoolean1951?26:22) + Class3_Sub13_Sub34.contextOptionsAmount * 15;
            Class38_Sub1.drawContextMenu = true;
         } else {
            Class1223.anInt2099 = Class38_Sub1.clickY2;
            Class3_Sub13_Sub39.anInt3460 = Class163_Sub1.clickX2;
            Class992.anInt3660 = 1;
         }

      } catch (RuntimeException var5) {
         throw Class1134.method1067(var5, "sf.D(" + var0 + ')');
      }
   }

}

package com.kit.client;

import java.util.Calendar;

final class Class3_Sub28_Sub9 extends Class1002 {

    protected int anInt3614;
    protected static Calendar aCalendar3616 = Calendar.getInstance();
    protected int anInt3617;
    protected Class1008 aClass94_3619;
    protected static int anInt3620 = 0;
    protected static Class1008 aClass94_3621 = null;
    protected static int anInt3622 = 0;
    protected static int anInt3623 = 0;
    protected static int anInt3624;


    static final Class1206_2 method578(int var0) {
        try {
            int var1 = Class3_Sub13_Sub6.spriteHeights[0] * Class1013.spriteWidths[0];
            byte[] var2 = Class163_Sub1.spritePaletteIndicators[0];
            if (var0 != 115) {
                anInt3624 = 112;
            }

            Object var3;
            if (Class120_Sub30_Sub1.spriteHaveAlpha[0]) {
                byte[] var4 = Class163_Sub3.spriteAlphas[0];
                int[] var5 = new int[var1];

                for (int var6 = 0; var6 < var1; ++var6) {
                    var5[var6] = Class3_Sub13_Sub29.method308(Class951.method633(var4[var6] << 24, -16777216), Class3_Sub13_Sub38.spritePalette[Class951.method633(255, var2[var6])]);
                }

                var3 = new Class12062(Class3_Sub15.spriteTrimWidth, Class974.spriteTrimHeight, Class164.spriteXOffsets[0], ByteBuffer.aInteger1259[0], Class1013.spriteWidths[0], Class3_Sub13_Sub6.spriteHeights[0], var5);
            } else {
                int[] var8 = new int[var1];

                for (int var9 = 0; var9 < var1; ++var9) {
                    var8[var9] = Class3_Sub13_Sub38.spritePalette[Class951.method633(var2[var9], 255)];
                }

                var3 = new Class1206_2(Class3_Sub15.spriteTrimWidth, Class974.spriteTrimHeight, Class164.spriteXOffsets[0], ByteBuffer.aInteger1259[0], Class1013.spriteWidths[0], Class3_Sub13_Sub6.spriteHeights[0], var8);
            }

            Class922.resetSprites();
            return (Class1206_2) var3;
        } catch (RuntimeException var7) {
            throw Class1134.method1067(var7, "hn.P(" + var0 + ')');
        }
    }

    private final void method579(int var1, ByteBuffer var2, int var3) {
        try {
            if (~var1 != -2) {
                if (var1 == 2) {
                    this.anInt3614 = var2.getInt();
                } else if (-6 == ~var1) {
                    this.aClass94_3619 = var2.class_91033();
                }
            } else {
                this.anInt3617 = var2.readUnsignedByte();
            }

            if (var3 != 0) {
                method582(5, 31, 114, true, -67, 14, -33, -115, -101, -61, -25, -121);
            }

        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "hn.A(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
        }
    }

    static final void method580(byte var0) {
        try {
            if (var0 != 80) {
                method582(88, 85, -8, true, 72, 12, 29, 96, 6, 57, -13, 15);
            }

            Class949.aClass130_2220 = new Class1017_2(32);
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "hn.B(" + var0 + ')');
        }
    }

    static final boolean method582(int var0, int var1, int var2, boolean var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
        try {
            return Class945.thisClass946.getSize() != var5 ? (Class945.thisClass946.getSize() <= 2 ? Class999.method2191(var6, var4, var11, -1001, var10, var9, var2, var1, var3, var8, var0, var7) : Class1098.method1166(var10, (byte) 34, var7, var9, var1, Class945.thisClass946.getSize(), var6, var8, var4, var11, var2, var3, var0)) : Class2.method76(var7, var8, var4, var0, var10, var3, var2, var1, var6, var9, 127, var11);
        } catch (RuntimeException var13) {
            throw Class1134.method1067(var13, "hn.O(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ',' + var9 + ',' + var10 + ',' + var11 + ')');
        }
    }

    final void method583(int var1, ByteBuffer var2) {
        try {
            if (var1 == 207) {
                while (true) {
                    int var3 = var2.readUnsignedByte();
                    if (var3 == 0) {
                        return;
                    }

                    this.method579(var3, var2, 0);
                }
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "hn.C(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ')');
        }
    }

    public static void method584(int var0) {
        try {
            aCalendar3616 = null;
            if (var0 != 0) {
                method580((byte) -90);
            }

            aClass94_3621 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "hn.F(" + var0 + ')');
        }
    }

    final boolean method585(int var1) {
        try {
            if (var1 != 0) {
                aClass94_3621 = (Class1008) null;
            }

            return this.anInt3617 == 115;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "hn.E(" + var1 + ')');
        }
    }

}

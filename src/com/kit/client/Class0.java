package com.kit.client;

final class Class0 extends Class1042 {

    protected  byte aByte2472;
    protected  Class1008 aClass94_2473;
    protected static Class1009 cacheIndex14;
    protected  Class1008 name;
    protected  static Class1008 aClass94_2477 = Class943.create("<col=ffffff>");
    protected  int anInt2478;

    static final Class75_Sub2 method384(ByteBuffer var0) {
        return new Class75_Sub2(var0.aLong_1884(), var0.aLong_1884(), var0.aLong_1884(), var0.aLong_1884(), var0.aBoolean183(), var0.aBoolean183(), var0.readUnsignedByte());
    }

    static final boolean method_745(int z) {
        byte offsetX = 0;
        byte offsetY = 0;
        int var5;
        int pixelId;
        if (null == Class27.landscapeAsSprite) {
            if (!Class1012.aBoolean_617 && (Class49.minimapLandscape != null)) {
                Class27.landscapeAsSprite = (Class1206_2) Class49.minimapLandscape;
            } else {
                Class27.landscapeAsSprite = new Class1206_2(512, 512);
            }
            int[] pixels = Class27.landscapeAsSprite.pixels;
            int pixelAmount = pixels.length;

            for (int pixel = 0; pixel < pixelAmount; ++pixel) {
                pixels[pixel] = 1;
            }
            for (int y = offsetY + 1; -1 + -offsetY + 104 > y; ++y) {
                pixelId = 4 * 512 * (offsetY + 103 + -y) + 24628;

                for (int x = 1 + offsetX; x < (-offsetX + 104 + -1); ++x) {
                    if (z >= Class9.groundArray.length || x > 104 || y > 104)
                        continue;
                    if (-1 == ~(Class9.groundArray[z][x][y] & 24)) {
                        if (Class929.hdMinimap) {
                            Class1007.pushMinimapPixelsHD(pixels, pixelId, 512, z, x, y);
                        } else {
                            Class1007.pushMinimapPixelsLD(pixels, pixelId, 512, z, x, y);
                        }
                         // Class1007.calcMinimapHD(pixels, pixelId, z, x, y);
                    }

                    if (~z > -4 && 0 != (Class9.groundArray[1 + z][x][y] & 8)) {
                        if (Class929.hdMinimap) {
                            Class1007.pushMinimapPixelsHD(pixels, pixelId, 512, z + 1, x, y);
                        } else {
                            Class1007.pushMinimapPixelsLD(pixels, pixelId, 512, z + 1, x, y);
                        }
                        //	  Class1007.calcMinimapHD(pixels, pixelId, z + 1, x, y);
                    }

                    pixelId += 4;
                }
            }

            Class980.anInt1924 = 0;

            for (int x = 0; 104 > x; ++x) {
                for (int y = 0; 104 > y; ++y) {
                    long var20 = Class949.method104(Class26.plane, x + offsetX, y - -offsetY);
                    if (~var20 != -1L) {
                        ObjectDefinition obj = ObjectDefinition.getDefinition((int) (var20 >>> 32) & Integer.MAX_VALUE);
                        int var11 = obj.minimapSprite;
                        int var12;
                        if (null != obj.anIntArray1524) {
                            for (var12 = 0; ~obj.anIntArray1524.length < ~var12; ++var12) {
                                if (-1 != obj.anIntArray1524[var12]) {
                                    ObjectDefinition var13 = ObjectDefinition.getDefinition(obj.anIntArray1524[var12]);
                                    if (0 <= var13.minimapSprite) {
                                        var11 = var13.minimapSprite;
                                        break;
                                    }
                                }
                            }
                        }
                        if (~var11 <= -1) {
                            int var21 = offsetY + y;
                            var12 = offsetX + x;
                            if (22 != var11 && var11 != 29 && 34 != var11 && -37 != ~var11 && 46 != var11 && var11 != 47 && var11 != 48) {
                                //                      	if(Class26.plane > Class120_Sub30_Sub1.aClass61ArrayArrayArray3273.length)
                                //                 		   return false;
                                int[][] var14 = Class930.class972[Class26.plane].aBoolean_4222;

                                for (int var15 = 0; -11 < ~var15; ++var15) {
                                    int var16 = (int) (Math.random() * 4.0D);
                                    if (var16 == 0 && var12 > 0 && ~(x - 3) > ~var12 && ~(var14[var12 - 1][var21] & 19661064) == -1) {
                                        --var12;
                                    }

                                    if (var16 == 1 && -104 < ~var12 && ~(3 + x) < ~var12 && (var14[var12 - -1][var21] & 19661184) == 0) {
                                        ++var12;
                                    }

                                    if (2 == var16 && var21 > 0 && ~(-3 + y) > ~var21 && 0 == (var14[var12][-1 + var21] & 19661058)) {
                                        --var21;
                                    }

                                    if (3 == var16 && var21 < 103 && ~var21 > ~(y - -3) && 0 == (19661088 & var14[var12][1 + var21])) {
                                        ++var21;
                                    }
                                }
                            }

                            Class951.anIntArray3693[Class980.anInt1924] = obj.objectId;
                            Class84.anIntArray1163[Class980.anInt1924] = -offsetX + var12;
                            Class1225.anIntArray4050[Class980.anInt1924] = var21 + -offsetY;
                            ++Class980.anInt1924;
                        }
                    }
                }
            }
        }

        Class27.landscapeAsSprite.initDefault();
        int var18 = (238 + (int) (20.0D * Math.random()) - 10 << 8) + (-10 + (int) (Math.random() * 20.0D) + 238 << 16) + (228 - -((int) (20.0D * Math.random())));
        var5 = (int) (Math.random() * 20.0D) + 238 + -10 << 16;

        for (int y = 1; y < 103; ++y) {
            for (int x = 1; x < 103; ++x) {
                if (z < Class9.groundArray.length && 0 == (Class9.groundArray[z][x + offsetX][y + offsetY] & 24) && !Class1008.method1529(offsetY, x, var18, y, var5, offsetX, z, true)) {
                    if (Class1012.aBoolean_617) {
                        Class1023.pixels = null;
                    } else {
                        Class164_Sub1.aClass158_3009.initCanvas();
                    }

                }

                if (z + 1 < Class9.groundArray.length && ~(Class9.groundArray[1 + z][x + offsetX][y + offsetY] & 8) != -1 && !Class1008.method1529(offsetY, x, var18, y, var5, offsetX, 1 + z, true)) {
                    if (!Class1012.aBoolean_617) {
                        Class164_Sub1.aClass158_3009.initCanvas();
                    } else {
                        Class1023.pixels = null;
                    }

                }
            }
        }

        if (Class1012.aBoolean_617) {
            int[] pixels = Class27.landscapeAsSprite.pixels;
            int pixelAmount = pixels.length;

            for (int pixel = 0; pixelAmount > pixel; ++pixel) {
                if (pixels[pixel] == 0) {
                    pixels[pixel] = 1;
                }
            }

            Class49.minimapLandscape = new Class1011(Class27.landscapeAsSprite);
        } else {
            Class49.minimapLandscape = Class27.landscapeAsSprite;
        }

        if (!Class1012.aBoolean_617) {
            Class164_Sub1.aClass158_3009.initCanvas();
        } else {
            Class1023.pixels = null;
        }

        Class27.landscapeAsSprite = null;
        return true;
    }

    static final void method386(int var0, int var1) {
        try {
            Class1207.aClass93_2604.method1522(var0);
            if (var1 <= -109) {
                Class27.aClass93_511.method1522(var0);
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "kl.D(" + var0 + ',' + var1 + ')');
        }
    }

    public static void method387(int var0) {
        try {
            int var1 = 41 % ((var0 - -70) / 32);
            aClass94_2477 = null;
            cacheIndex14 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "kl.A(" + var0 + ')');
        }
    }

}

package com.kit.client;

import java.awt.*;
import java.io.IOException;

final class Class979 {

    protected static int js5Stage = 0;
    protected static byte[][][] aByteArrayArrayArray81;
    protected static Class975 aClass61_82 = new Class975();
    protected static short aShort83 = 32767;
    protected static int oldSlot = 0;
    protected static int anInt87 = 0;
    protected static Class1034 aClass11_88 = null;

    private static final Class1008[] cmd = new Class1008[]{
            Class943.create(":lcsdaon:"), Class943.create(":lcaodfff:"),
            Class943.create(":newsdmenu:"), Class943.create(":oldmdfenu:"),
            Class943.create(":newhsdits:"), Class943.create(":olddfhits:"),
            Class943.create(":newhsdealthbars:"),
            Class943.create(":oldhesdsdalthbars:"), Class943.create(":634mdfarkers:"),
            Class943.create(":554msdarkers:"), Class943.create(":oldmarkers:"),
            Class943.create(":orsdbson:"), Class943.create(":orbsoff:"),
            Class943.create(":nesdwcursors:"), Class943.create(":oldcusdrsors:"),
            Class943.create(":twesdeningon:"), Class943.create(":tweensdingoff:"),
            Class943.create(":hdsdosdn:"), Class943.create(":hdosdff:"),
            Class943.create(":censosdron:"), Class943.create(":censosdroff:"),
            Class943.create(":newcursorson:"),
            Class943.create(":newcursorsoff:"), Class943.create(":2frame:"),
            Class943.create(":1frsame:"), Class943.create(":0frame:"),
            Class943.create(":fsson:"), Class943.create(":fsosff:"),
            Class943.create(":fsson2:"), Class943.create(":fsosff2:")};

    static final int method823(int var0, int var1, int var3) {
        return (8 & Class9.groundArray[var3][var1][var0]) == 0 ? (~var3 < -1
                && -1 != ~(Class9.groundArray[1][var1][var0] & 2) ? -1 + var3
                : var3) : 0;
    }

    static final void method824(long[] var0, Object[] var1, int var2) {
        try {
            int var3 = 38 % ((var2 - 28) / 52);
            Class134.method1809(var0.length - 1, var0, 122, 0, var1);
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ac.E("
                    + (var0 != null ? "{...}" : "null") + ','
                    + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
        }
    }

    static final void method825(byte var0, int var1) {
        try {
            int var2 = -51 % ((26 - var0) / 33);
            Class1042_3 var3 = Class3_Sub24_Sub3.putInterfaceChange(1,
                    var1);
            var3.a();
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "ac.D(" + var0 + ',' + var1 + ')');
        }
    }

    static final int method826(Class1008 var0, int var1) {
        try {
            if (var1 != -1) {
                method826((Class1008) null, 65);
            }

            if (var0 != null) {
                for (int var2 = 0; Class8.localPlayerIds > var2; ++var2) {
                    if (var0.method102(Class70.localPlayerNames[var2])) {
                        return var2;
                    }
                }

                return -1;
            } else {
                return -1;
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "ac.B("
                    + (var0 != null ? "{...}" : "null") + ',' + var1 + ')');
        }
    }

    public static long lastPing = 0;

    static final boolean parsePackets() throws IOException {
        if (Class3_Sub15.worldConnection == null) {
            return false;
        }
        int var1 = Class3_Sub15.worldConnection.available();
        if (0 == var1) {
            return false;
        }
        if (Class1008.incomingPacket == -1) {
            var1--;
            Class3_Sub15.worldConnection.read(
                    Class1211.incomingPackets.buffer, 0, 1);
            Class1211.incomingPackets.offset = 0;
            Class1008.incomingPacket = Class1211.incomingPackets.getPacket();
            Class1017_2.anInt1704 = Class75_Sub4.anIntArray2668[Class1008.incomingPacket];
        }
        if (Class1017_2.anInt1704 == -1) {
            if (0 >= var1) {
                return false;
            }

            Class3_Sub15.worldConnection.read(
                    Class1211.incomingPackets.buffer, 0, 1);
            --var1;
            Class1017_2.anInt1704 = Class1211.incomingPackets.buffer[0] & 255;
        }

        if (-2 == Class1017_2.anInt1704) {
            if (var1 <= 1) {
                return false;
            }

            var1 -= 2;
            Class3_Sub15.worldConnection.read(
                    Class1211.incomingPackets.buffer, 0, 2);
            Class1211.incomingPackets.offset = 0;
            Class1017_2.anInt1704 = Class1211.incomingPackets.aInteger233();
        }

        if (Class1017_2.anInt1704 > var1) {
            return false;
        }
        Class1211.incomingPackets.offset = 0;
        Class3_Sub15.worldConnection.read(Class1211.incomingPackets.buffer, 0,
                Class1017_2.anInt1704);
        Class1217.beforeLastPacket = Class7.lastPacket;
        Class7.lastPacket = Class1030.anInt2582;
        Class1030.anInt2582 = Class1008.incomingPacket;
        Class957.timoutCycle = 0;
        int var20;
        int var5;
        Class1008 var24;
        long var4;
        int var3;
        Class1008 var58;
        boolean var31;
        long var2;
        int var10;
        int var11;
        long var29;
        long var36;
        int var21;
        int var30;
        int var6;
        int var8;
        boolean var32;
        Class1008 var41;
        // System.out.println("incoming: " + Class1008.incomingPacket);
        if (Class1008.incomingPacket == 174 || Class1008.incomingPacket == 90) { // 90
            // =
            // player
            // update
            // 174 = npc udpate
            lastPing = System.currentTimeMillis();
        }
        if (Class1008.incomingPacket == 245) { // configs
            int id = Class1211.incomingPackets.aMethod10();
            byte value = Class1211.incomingPackets.getByte();
            int newintvalue = 0;
            if (id == 554) { // total xp on xp counter
                int intvalue = value;
                if (intvalue >= 2147483647)
                    newintvalue = 2147483647;
            }
            Class3_Sub13_Sub23.method281(value,
                    newintvalue > 0 ? (byte) newintvalue : id);
            Class1008.incomingPacket = -1;
            return true;
            /*
             * } else if (156 == Class1008.incomingPacket) { int type =
			 * Class1211.aClass3_Sub30_Sub1_532.getByte(); int index =
			 * Class1211.aClass3_Sub30_Sub1_532.getByte();
			 * 
			 * Class1025 hint = new Class1025(); hint.type = type; //10 = player,
			 * 1 npc hint.iconIndex = type == 10 ? 1 : 0; //0 = full yellow for
			 * npcs, 1 = seethru for players hint.index = index; //theoretically
			 * it is the most recent player
			 * 
			 * hint.anInt1346 = 64; hint.anInt1350 = 64;
			 * System.out.println("added hint at index: " + index);
			 * //Class1211.aClass3_Sub30_Sub1_532.offset += 3;
			 * Class1244.hintsList[0] = hint; Class1008.incomingPacket
			 * = -1; return true; } else if (217 == Class1008.incomingPacket) {
			 * //Remove hint int index =
			 * Class1211.aClass3_Sub30_Sub1_532.getByte(); //index in list
			 * System.out.println("Removed hint at index: " + index);
			 * Class1244.hintsList[index] = null;
			 * Class1008.incomingPacket = -1; return true;
			 */
        } else if (Class1008.incomingPacket == 69) {
            // clientscript packet
            Class1008 args = Class1211.incomingPackets.class_91033();
            Object[] var71 = new Object[args.getLength() - -1];

            for (int i = args.getLength() + -1; -1 >= ~i; --i) {
                if (115 == args.method1569(i, (byte) -45)) {
                    var71[1 + i] = Class1211.incomingPackets.class_91033();
                } else {
                    var71[1 + i] = new Integer(
                            Class1211.incomingPackets.getInt());
                }
            }

            var71[0] = new Integer(Class1211.incomingPackets.getInt());
            Class1048 var66 = new Class1048();
            var66.objectData = var71;
            Class983.method1065(var66);

            Class1008.incomingPacket = -1;
            return true;

            // TODO: CUSTOM PACKETS GO HERE. EG. GAMEFRAME CHANGER
        } else if (Class1008.incomingPacket == 108) { // packet 108
            // regular message
            Class1008 message = Class1211.incomingPackets.class_91033();
            for (Class1008 cmds : cmd) {
                if (message.method_874(cmds)) {
                    if (message.method_874(cmd[0])) {
                        Class922.leftClickAttack = true;
                        //client.newKeys = true;
                    } else if (message.method_874(cmd[1])) {
                        Class922.leftClickAttack = false;
                        //client.newKeys = false;
                    } else if (message.method_874(cmd[2])) {
                        Class929.newMenus = true;
                    } else if (message.method_874(cmd[3])) {
                        Class929.newMenus = false;
                    } else if (message.method_874(cmd[4])) {
                        Class929.newHits = true;
                    } else if (message.method_874(cmd[5])) {
                        Class929.newHits = false;
                    } else if (message.method_874(cmd[6])) {
                        Class929.newHealthbars = true;
                    } else if (message.method_874(cmd[7])) {
                        Class929.newHealthbars = false;
                    } else if (message.method_874(cmd[8])) {
                        Class929.aInteger_511 = 634;
                    } else if (message.method_874(cmd[9])) {
                        Class929.aInteger_511 = 554;
                    } else if (message.method_874(cmd[10])) {
                        Class929.aInteger_511 = 464;
                    } else if (message.method_874(cmd[11])) {
                        if (Class929.orbsToggled == false) {
                            Class929.orbsToggled = true;
                            // Class955.canvasReplaceRecommended = true;
                            System.gc();
                        }
                    } else if (message.method_874(cmd[12])) {
                        if (Class929.orbsToggled == true) {
                            Class929.orbsToggled = false;
                            // Class955.canvasReplaceRecommended = true;
                            System.gc();
                        }
                    } else if (message.method_874(cmd[13])) {
                        Class929.newCursors = true;
                        Class122.setCursor(0);
                    } else if (message.method_874(cmd[14])) {
                        Class929.newCursors = false;
                        Class122.getFrame().setCursor(
                                new Cursor(Cursor.DEFAULT_CURSOR));
                    } else if (message.method_874(cmd[15])) {
                        Class3_Sub26.forceTweeningEnabled = true;
                    } else if (message.method_874(cmd[16])) {
                        Class3_Sub26.forceTweeningEnabled = false;
                    } else if (message.method_874(cmd[17])) {
                        //HD
                        if (Class59.hdEnabled == false) {
                            //if (client.clientSize > 0) {
                            //	client.changeSize(1);
                            //} else {
                            Class59.hdEnabled = true;
                            Class1031.setLowDefinition(false, 1, -1, -1);
                            Class922.forceRefresh();
                            System.gc();
                            //}
                        }
                    } else if (message.method_874(cmd[18])) {
                        if (Class59.hdEnabled == true) {
                            Class59.hdEnabled = false;
                            Class1031.setLowDefinition(false, 0, -1, -1);
                            Class922.forceRefresh();
                            System.gc();
                        }
                    } else if (message.method_874(cmd[19])) {
                        Class922.censorOn = true;
                    } else if (message.method_874(cmd[20])) {
                        Class922.censorOn = false;
                    } else if (message.method_874(cmd[21])) {
                        Class929.newCursors = true;
                    } else if (message.method_874(cmd[22])) {
                        Class929.newCursors = false;
                    } else if (message.method_874(cmd[23])) {
                        // if (client.aInteger_510 != 562 && client.clientSize == 0)
                        // {// &&
                        // !Class1012.aBoolean_617)
                        // {
                        // client.setDefaults();
                        // client.sendGameframe(562, false);
                        // Class955.canvasReplaceRecommended = true;
                        Class922.forceRefresh();
                        // client.revertToFixed();
                        System.gc();
                        if (Class922.clientSize > 0 && !Class922.chatboxHidden) {
                            Class7.getInterface(Class922.CHATBOX_FRAME).hidden = true;
                        }
                        if (Class922.clientSize == 0) {
                            Class7.getInterface(Class922.XP_COUNTER).y = Class7.getInterface(Class922.XP_COUNTER).originalY;
                        } else {
                            Class922.reloadFullscreenInterfaces();
                        }
                        // }
                    } else if (message.method_874(cmd[24])) {
                        // if (client.aInteger_510 != 525 && client.clientSize == 0)
                        // {// &&
                        // !Class1012.aBoolean_617)
                        // {
                        // client.setDefaults();
                        // client.sendGameframe(525, false);
                        // Class955.canvasReplaceRecommended = true;
                        Class922.forceRefresh();
                        System.gc();
                        Class929.aInteger_510 = 525;
                        if (Class922.clientSize > 0 && !Class922.chatboxHidden) {
                            Class7.getInterface(Class922.CHATBOX_FRAME).hidden = true;
                        }
                        if (Class922.clientSize == 0) {
                            Class7.getInterface(Class922.XP_COUNTER).y = Class7.getInterface(Class922.XP_COUNTER).originalY;
                        } else {
                            Class922.reloadFullscreenInterfaces();
                        }
                        // }
                    } else if (message.method_874(cmd[25])) {
                        // if (client.aInteger_510 != 474 && client.clientSize == 0)
                        // {// &&
                        // !Class1012.aBoolean_617)
                        // {
                        // client.setDefaults();
                        // client.sendGameframe(474, false);
                        // Class955.canvasReplaceRecommended = true;
                        Class922.forceRefresh();
                        System.gc();
                        Class929.aInteger_510 = 474;
                        if (Class922.clientSize > 0 && !Class922.chatboxHidden) {
                            Class7.getInterface(Class922.CHATBOX_FRAME).hidden = false;
                        }
                        if (Class922.clientSize == 0) {
                            Class7.getInterface(Class922.XP_COUNTER).y = Class7.getInterface(Class922.XP_COUNTER).originalY;
                        } else {
                            Class922.reloadFullscreenInterfaces();
                        }
                        // }
                    } else if (message.method_874(cmd[26])) {
                        if (Class922.clientSize != 1) {
                            Class922.changeSize(1);
                        }
                    } else if (message.method_874(cmd[27])) {
                        if (Class922.clientSize > 0) {
                            Class922.changeSize(0);
                        }
                    } else if (message.method_874(cmd[28])) {
                        if (Class922.clientSize != 2) {
                            Class922.changeSize(2);
                        }
                    } else if (message.method_874(cmd[29])) {
                        if (Class922.clientSize > 0) {
                            Class922.changeSize(0);
                        }
                    }

                    // Class944.changeSetting("client_load", client.clientLoad);
                    Class944.changeSetting("hd_enabled", Class922.hdOnLogin);
                    Class944.changeSetting("start_size", Class922.clientSize);
                    Class944.changeSetting("orbs_toggled", Class929.orbsToggled);
                    Class944.changeSetting("tweening_enabled",
                            Class3_Sub26.forceTweeningEnabled);
                    Class944.changeSetting("new_menus", Class929.newMenus);
                    Class944.changeSetting("new_health_bars",
                            Class929.newHealthbars);
                    Class944.changeSetting("new_hits", Class929.newHits);
                    Class944.changeSetting("new_cursors", Class929.newCursors);
                    Class944.changeSetting("censor", Class922.censorOn);
                    Class944.changeSetting("gameframe", Class929.aInteger_510);
                    Class944.changeSetting("hitmarkers", Class929.aInteger_511);
                    Class944.changeSetting("left_click_attack", Class922.leftClickAttack);
                    Class944.save();
                    System.out.println("Saved settings.");
                    Class1008.incomingPacket = -1;
                    return true;
                }
            }
            if (message.method1550((byte) -60, Class117.tradeReqString)) {
                var24 = message.substring(
                        message.method1551(Class155.semiColon), 0);
                var4 = var24.toLong();
                var31 = false;

                for (var30 = 0; ~var30 > ~Class955.ignoreListCount; ++var30) {
                    if (~Class114.ignoreList[var30] == ~var4) {
                        var31 = true;
                        break;
                    }
                }

                if (!var31 && ~Class1228.anInt2622 == -1) {
                    Class966_2.sendMessage(var24, Class3_Sub6.tradeTextString,
                            4);
                }
            } else if (message.method1550((byte) -47,
                    Class1046.duelReqString)) {
                var24 = message.substring(
                        message.method1551(Class155.semiColon), 0);
                var4 = var24.toLong();
                var31 = false;

                for (var30 = 0; ~Class955.ignoreListCount < ~var30; ++var30) {
                    if (Class114.ignoreList[var30] == var4) {
                        var31 = true;
                        break;
                    }
                }

                // if(!var31 && Class1228.anInt2622 == 0) { //only diff is 0
                // was -1
                // System.out.println("message: " + message);
                // var41 = message.substring(message.getLength() + -9, 1 +
                // message.method1551(Class155.aClass94_1970));
                // Class966_2.method805(var24, var41, 8);
                // }

                if (!var31 && ~Class1228.anInt2622 == /* 0 */-1) {
                    // var41 = message.substring(message.getLength() + -9, 1 +
                    // message.method1551(Class155.semiColon));
                    Class966_2.sendMessage(var24, /* var41 */
                            Class3_Sub6.duelTextString, /* 4 */8);
                }

				/*
                 * } else if(message.method1550((byte)-98,
				 * Class3_Sub13_Sub26.aClass94_3330)) { var31 = false; var24 =
				 * message
				 * .method1557(message.method1551(Class155.aClass94_1970), 0);
				 * var4 = var24.toLong();
				 * 
				 * for(var30 = 0; ~Class955.anInt3591 < ~var30; ++var30)
				 * { if(var4 == Class114.aLongArray1574[var30]) { var31 = true;
				 * break; } }
				 * 
				 * if(!var31 && ~Class1228.anInt2622 == -1) {
				 * Class966_2.method805(var24, Class921.aClass94_3672, 10);
				 * } } else if(message.method1550((byte)-128,
				 * Class3_Sub20.aClass94_2482)) { var24 =
				 * message.method1557(message
				 * .method1551(Class3_Sub20.aClass94_2482), 0);
				 */
                // Class966_2.method805(Class921.aClass94_3672, var24, 11);
            } else if (message.method1550((byte) -29, Class1001.tradeString)) {
                var24 = message.substring(message.method1551(Class1001.tradeString),
                        0);
                if (0 == Class1228.anInt2622) {
                    Class966_2.sendMessage(Class922.BLANK_CLASS_1008, var24, 12);
                }
                /*
                 * } else if(message.method1550((byte)-80,
				 * Class143.aClass94_1877)) { var24 =
				 * message.method1557(message.
				 * method1551(Class143.aClass94_1877), 0);
				 * if(Class1228.anInt2622 == 0) {
				 * Class966_2.method805(Class921.aClass94_3672, var24, 13);
				 * }
				 */
            } else if (message.method1550((byte) -47,
                    Class1046.clanWarReqString)) {
                var24 = message.substring(
                        message.method1551(Class155.semiColon), 0);
                var4 = var24.toLong();
                var31 = false;

                for (var30 = 0; ~Class955.ignoreListCount < ~var30; ++var30) {
                    if (Class114.ignoreList[var30] == var4) {
                        var31 = true;
                        break;
                    }
                }

                if (!var31) { //&& ~Class1228.anInt2622 == /* 0 */-1) {
                    int chatType = 8;
                    Class966_2.sendMessage(var24, /* var41 */
                            Class3_Sub6.clanWarTextString, /* 4 */chatType);
                }


            } else if (message.method1550((byte) -42, Class27.aClass94_514)) {
                var31 = false;
                var24 = message.substring(
                        message.method1551(Class155.semiColon), 0);
                var4 = var24.toLong();

                for (var30 = 0; Class955.ignoreListCount > var30; ++var30) {
                    if (var4 == Class114.ignoreList[var30]) {
                        var31 = true;
                        break;
                    }
                }

                if (!var31 && -1 == ~Class1228.anInt2622) {
                    Class966_2.sendMessage(var24, Class922.BLANK_CLASS_1008, 14);
                }
            } else if (message.method1550((byte) -41, Class1039.aClass94_965)) {
                var24 = message.substring(
                        message.method1551(Class155.semiColon), 0);
                var31 = false;
                var4 = var24.toLong();
                for (var30 = 0; ~Class955.ignoreListCount < ~var30; ++var30) {
                    if (~Class114.ignoreList[var30] == ~var4) {
                        var31 = true;
                        break;
                    }
                }

                if (!var31 && 0 == Class1228.anInt2622) {
                    Class966_2.sendMessage(var24, Class922.BLANK_CLASS_1008, 15);
                }
				/*
				 * } else if(message.method1550((byte)-110,
				 * Class3_Sub13_Sub30.aClass94_3368)) { var24 =
				 * message.method1557
				 * (message.method1551(Class155.aClass94_1970), 0); var4 =
				 * var24.toLong(); var31 = false;
				 * 
				 * for(var30 = 0; ~Class955.anInt3591 < ~var30; ++var30)
				 * { if(~var4 == ~Class114.aLongArray1574[var30]) { var31 =
				 * true; break; } }
				 * 
				 * if(!var31 && Class1228.anInt2622 == 0) {
				 * Class966_2.method805(var24, Class921.aClass94_3672, 16);
				 * } } else if(message.method1550((byte)-41,
				 * Class1008.aClass94_2155)) { var24 =
				 * message.method1557(message.method1551
				 * (Class155.aClass94_1970), 0); var31 = false; var4 =
				 * var24.toLong();
				 * 
				 * for(var30 = 0; ~Class955.anInt3591 < ~var30; ++var30)
				 * { if(~Class114.aLongArray1574[var30] == ~var4) { var31 =
				 * true; break; } }
				 * 
				 * if(!var31 && Class1228.anInt2622 == 0) { var41 =
				 * message.method1557(message.getLength() - 9, 1 +
				 * message.method1551(Class155.aClass94_1970));
				 * Class966_2.method805(var24, var41, 21); }
				 */
            } else {
                Class966_2.sendMessage(Class922.BLANK_CLASS_1008, message, 0);
            }

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 123) {
            var20 = Class1211.incomingPackets.aLong_011();
            var58 = Class1211.incomingPackets.class_91033();
            Class3_Sub13_Sub27.method295(var58, (byte) 40, var20);

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 230) {
            Class107.anInt1452 = Class1211.incomingPackets.aInt2016();
            Class65.anInt990 = Class1211.incomingPackets.aInt122();

            while (~Class1017_2.anInt1704 < ~Class1211.incomingPackets.offset) {
                Class1008.incomingPacket = Class1211.incomingPackets
                        .readUnsignedByte();
                Class39.method1038((byte) -82);
            }

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 68) {
            Class1008.incomingPacket = -1;
            Class65.mapFlagX = 0;
            return true;
        } else if (Class1008.incomingPacket == 220) {
            var20 = Class1211.incomingPackets.method_152();
            var3 = Class1211.incomingPackets.aLong_011();
            Class3_Sub13_Sub33.method327(var3, var20, (byte) 68);

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 81) {
            var2 = Class1211.incomingPackets.aVar100();
            Class1211.incomingPackets.getByte();
            var4 = Class1211.incomingPackets.aVar100();
            var29 = (long) Class1211.incomingPackets.aInteger233();
            var36 = (long) Class1211.incomingPackets.aBoolean183();
            var10 = Class1211.incomingPackets.readUnsignedByte();
            boolean var63 = false;
            var11 = Class1211.incomingPackets.aInteger233();
            long var55 = (var29 << 32) + var36;
            int var54 = 0;

            label1521:
            while (true) {
                if (100 > var54) {
                    if (~var55 != ~Class163_Sub2_Sub1.aLongArray4017[var54]) {
                        ++var54;
                        continue;
                    }

                    var63 = true;
                    break;
                }

                if (1 >= var10) {
                    for (var54 = 0; ~Class955.ignoreListCount < ~var54; ++var54) {
                        if (~Class114.ignoreList[var54] == ~var2) {
                            var63 = true;
                            break label1521;
                        }
                    }
                }
                break;
            }

            if (!var63 && 0 == Class1228.anInt2622) {
                Class163_Sub2_Sub1.aLongArray4017[Class980.anInt1921] = var55;
                Class980.anInt1921 = (1 + Class980.anInt1921) % 100;
                Class1008 var61 = Class1030.method733(var11).method555(28021,
                        Class1211.incomingPackets);
				
/*				boolean hideRank = false;
				//Looping through players to see if rank hidden
				for(int i = 0; i < Class159.anInt2022; ++i) {
					Class946 var23 = client.class946List[client.playerIndices[i]];
					if(null != var23 && 
							var23.username.toString().trim().equalsIgnoreCase(Class988.longToString(var2).toString().trim())) {
							if(var23.rankHidden) {
								hideRank = true;
							}
					}
				}*/

                if (var10 != 2/* && 3 != var10 */) {
                    if (var10 != 1) {
						/*if (var10 == 3)
							Class1143.appendChatMessage(20, var61, Class988
									.longToString(var4).upperCase(), client
									.combinejStrings(new Class1008[] {
											Class21.pkerImg,
											Class988.longToString(var2)
													.upperCase() }));
						else if (var10 == 4)
							Class1143.appendChatMessage(20, var61, Class988
									.longToString(var4).upperCase(), client
									.combinejStrings(new Class1008[] {
											Class21.donatorImg,
											Class988.longToString(var2)
													.upperCase() }));
						else if (var10 == 5)
							Class1143.appendChatMessage(20, var61, Class988
									.longToString(var4).upperCase(), client
									.combinejStrings(new Class1008[] {
											Class21.extremeDonatorImg,
											Class988.longToString(var2)
													.upperCase() }));
						else if (var10 == 7)
							Class1143.appendChatMessage(20, var61, Class988
									.longToString(var4).upperCase(), client
									.combinejStrings(new Class1008[] {
											Class21.superDonatorImg,
											Class988.longToString(var2)
													.upperCase() }));
						else if (var10 == 8)
							Class1143.appendChatMessage(20, var61, Class988
									.longToString(var4).upperCase(), client
									.combinejStrings(new Class1008[] {
											Class21.legendaryDonatorImg,
											Class988.longToString(var2)
													.upperCase() }));
						else if (var10 == 6)
							Class1143.appendChatMessage(20, var61, Class988
									.longToString(var4).upperCase(), client
									.combinejStrings(new Class1008[] {
											Class21.supportImg,
											Class988.longToString(var2)
													.upperCase() }));
						else*/
                        Class1143.appendChatMessage(20, var61, Class988
                                .longToString(var4).upperCase(), Class988
                                .longToString(var2).upperCase());
                    } else {
                        Class1143.appendChatMessage(20, var61, Class988
                                .longToString(var4).upperCase(), Class922
                                .combinejStrings(new Class1008[]{
                                        Class32.aClass94_592,
                                        Class988.longToString(var2)
                                                .upperCase()}));
                    }
                } else {
                    Class1143.appendChatMessage(
                            20,
                            var61,
                            Class988.longToString(var4).upperCase(),
                            Class922.combinejStrings(new Class1008[]{
                                    Class21.aClass94_444,
                                    Class988.longToString(var2).upperCase()}));
                }
            }

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 55) {
            // TODO: real clan members
            Class167.anInt2087 = Class3_Sub13_Sub17.anInt3213;
            var2 = Class1211.incomingPackets.aVar100();
            if (~var2 == -1L) {
                Class161.aClass94_2035 = null;
                Class1008.incomingPacket = -1;
                Class1034.aClass94_251 = null;
                Class951.clanMembers = null;
                Class1002.anInt2572 = 0;
                return true;
            } else {
                var4 = Class1211.incomingPackets.aVar100();
                Class1034.aClass94_251 = Class988.longToString(var4);
                Class161.aClass94_2035 = Class988.longToString(var2);
                Class946.aByte3953 = Class1211.incomingPackets.getByte();
                var6 = Class1211.incomingPackets.readUnsignedByte();
                if (255 == var6) {
                    Class1008.incomingPacket = -1;
                    return true;
                } else {
                    Class1002.anInt2572 = var6;
                    Class0[] var7 = new Class0[100];

                    for (var8 = 0; ~Class1002.anInt2572 < ~var8; ++var8) {
                        var7[var8] = new Class0();
                        var7[var8].hash = Class1211.incomingPackets
                                .aVar100();
                        var7[var8].name = Class988
                                .longToString(var7[var8].hash);
                        var7[var8].anInt2478 = Class1211.incomingPackets
                                .aInteger233();
                        var7[var8].aByte2472 = Class1211.incomingPackets
                                .getByte();
                        var7[var8].aClass94_2473 = Class1211.incomingPackets
                                .class_91033();
                        if (~Class3_Sub13_Sub16.aLong3202 == ~var7[var8].hash) {
                            Class972.aByte1308 = var7[var8].aByte2472;
                        }
                    }

                    var32 = false;
                    var10 = Class1002.anInt2572;

                    while (-1 > ~var10) {
                        var32 = true;
                        --var10;

                        for (var11 = 0; ~var10 < ~var11; ++var11) {
                            if (-1 > ~var7[var11].name
                                    .contains(var7[var11 - -1].name)) {
                                var32 = false;
                                Class0 var9 = var7[var11];
                                var7[var11] = var7[1 + var11];
                                var7[var11 + 1] = var9;
                            }
                        }

                        if (var32) {
                            break;
                        }
                    }

                    Class951.clanMembers = var7;
                    Class1008.incomingPacket = -1;
                    return true;
                }
            }
        } else if (Class1008.incomingPacket == 164) {
            var20 = Class1211.incomingPackets.aLong5882();
            Class136.aClass64_1778 = Class38.gameClass942.method1449(var20);
            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 90) {
            Class163_Sub3.method2228((byte) -122);// TODO player update
            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 48) {
            // Change text on interface? not used
            var24 = Class1211.incomingPackets.class_91033();
            var21 = Class1211.incomingPackets.aBoole100();
            Class3_Sub13_Sub27.method295(var24, (byte) 40, var21);

            Class1008.incomingPacket = -1;
            return true;
        } else if (156 == Class1008.incomingPacket) {
			/*
			 * Class3_Sub13_Sub8.publicChatStatus =
			 * Class1211.aClass3_Sub30_Sub1_532 .readUnsignedByte();
			 * Class1217.privateChatStatus =
			 * Class1211.aClass3_Sub30_Sub1_532 .readUnsignedByte();
			 * Class45.tradeChatStatus =
			 * Class1211.aClass3_Sub30_Sub1_532.readUnsignedByte();
			 * System.out.println("pub: " + Class3_Sub13_Sub8.publicChatStatus);
			 * System.out.println("priv: " + Class1217.privateChatStatus);
			 * System.out.println("trade: " + Class45.tradeChatStatus);
			 */
            byte type = Class1211.incomingPackets.getByte();
            byte index = Class1211.incomingPackets.getByte();
            byte flash = Class1211.incomingPackets.getByte();

            if (type == 69) {
                Class1244.hintsList[0] = null;
                System.out.println("removed hint: " + index);
                Class922.hintFlash = false;
            } else {
                Class1025 hint = new Class1025();
                hint.type = type; // 10 = player, 1 npc
                hint.iconIndex = type == 10 ? 1 : 0; // 0 = full yellow for
                // npcs, 1 = seethru for
                // players
                hint.index = index; // theoretically it is the most recent
                // player
                if (flash == 1)
                    Class922.hintFlash = true;

                hint.anInt1346 = 64;
                hint.anInt1350 = 64;
                System.out.println("added hint at index: " + index);
                Class1244.hintsList[0] = hint;
            }
            // Class1211.aClass3_Sub30_Sub1_532.offset += 3;
            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 72) {
            Class1008 var56 = Class1211.incomingPackets.class_91033();
            var21 = Class1211.incomingPackets.aInt122();
            var3 = Class1211.incomingPackets.gea100();
            var20 = -1;
            if (1 <= var21 && ~var21 >= -9) {
                if (var56.method102(Class1209.nullString)) {
                    var56 = null;
                }
                Class972.aClass94Array1299[-1 + var21] = var56;
                Class3_Sub13_Sub26.anIntArray3328[var21 + -1] = var20;
                Class1.aBooleanArray54[var21 + -1] = ~var3 == -1;
            }

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 142) {
            // Make sure we're dealing with interface 548?
            var20 = Class1211.incomingPackets.aInt122();
            var21 = Class1211.incomingPackets.aLong5882();
            // System.out.println("142 doe: var20: " + var20 + ", var21:" +
            // var21);
            Class3_Sub13_Sub19.toggleInterfaceComponent(var21, var20);

            Class1008.incomingPacket = -1;
            return true;
        } else if (Class1008.incomingPacket == 77) {// TODO this
            // DNOTE: Idk wtf this is. Maybe going fullscreen? It changes
            // mainScreenInterface and refreshes the interface
            var20 = Class1211.incomingPackets.aBoole100();
            // var3 = Class1211.aClass3_Sub30_Sub1_532.aInt2016();
            // if(-3 == ~var3) {
            // Class7.method834((byte)-86);
            // }

            Class1143.mainScreenInterface = var20;
            Class3_Sub13_Sub13.method232(var20);
            Class124.method1746(false);
            Class3_Sub13_Sub12
                    .executeOnLaunchScript(Class1143.mainScreenInterface);

            for (var5 = 0; -101 < ~var5; ++var5) {
                Class921.interfacesToRefresh[var5] = true;
            }

            Class1008.incomingPacket = -1;
            return true;
        } else if (-70 != ~Class1008.incomingPacket) {
            if (141 == Class1008.incomingPacket) {
                var2 = Class1211.incomingPackets.aVar100();
                var21 = Class1211.incomingPackets.aInteger233();
                Class1008 var56 = Class1030.method733(var21).method555(28021,
                        Class1211.incomingPackets);
                Class1143.appendChatMessage(19, var56, (Class1008) null, Class988
                        .longToString(var2).upperCase());
                Class1008.incomingPacket = -1;
                return true;
            } else if (-170 != ~Class1008.incomingPacket) {
                if (89 == Class1008.incomingPacket) {
                    Class3_Sub13_Sub2.resetVarp();
                    Class966_2.method819(false);
                    Class1017.anInt641 += 32;
                    Class1008.incomingPacket = -1;
                    return true;
                } else if (-126 == ~Class1008.incomingPacket) {
                    var3 = Class1211.incomingPackets.readUnsignedByte();
                    var21 = Class1211.incomingPackets.readUnsignedByte();
                    var5 = Class1211.incomingPackets.aInteger233();
                    var6 = Class1211.incomingPackets.readUnsignedByte();
                    var30 = Class1211.incomingPackets.readUnsignedByte();
                    Class164_Sub1.method2238(var5, var21, var6, var3,
                            (byte) -21, var30);

                    Class1008.incomingPacket = -1;
                    return true;
                } else if (63 == Class1008.incomingPacket) {
                    var20 = Class1211.incomingPackets.method_152();
                    var3 = Class1211.incomingPackets.method791((byte) 10);
                    Class131.method1790(var20, var3);
                    Class1008.incomingPacket = -1;
                    return true;
                } else {
                    Class971 var38;
                    Class971 var47;
                    if (-10 == ~Class1008.incomingPacket) {
                        var20 = Class1211.incomingPackets.aBoole100();
                        var3 = Class1211.incomingPackets.aBool1002();
                        var5 = Class1211.incomingPackets.aLong_011();
                        if (-65536 == ~var5) {
                            var5 = -1;
                        }

                        var6 = Class1211.incomingPackets.aMethod10();
                        if (-65536 == ~var6) {
                            var6 = -1;
                        }

                        for (var30 = var6; var5 >= var30; ++var30) {
                            var36 = (long) var30 + ((long) var3 << 32);
                            var47 = (Class971) Class124.aClass130_1659
                                    .get(var36);
                            if (null != var47) {
                                var38 = new Class971(var47.clickMask,
                                        var20);
                                var47.unlink();
                            } else if (0 != ~var30) {
                                var38 = new Class971(0, var20);
                            } else {
                                var38 = new Class971(
                                        Class7.getInterface(var3).clickMask.clickMask,
                                        var20);
                            }

                            Class124.aClass130_1659.put(var38, var36);
                        }

                        Class1008.incomingPacket = -1;
                        return true;
                    } else {
                        int var33;
                        if (Class1008.incomingPacket == 56) {
                            var20 = Class1211.incomingPackets.aInteger233();
                            var3 = Class1211.incomingPackets.aLong_011();
                            var21 = Class1211.incomingPackets.aLong5882();
                            var5 = Class1211.incomingPackets.aBoole100();
                            if (~(var21 >> 30) == -1) {
                                Class954 var53;
                                if (var21 >> 29 != 0) {
                                    var6 = '\uffff' & var21;
                                    Class1001 var62 = Class3_Sub13_Sub24.class1001List[var6];
                                    if (null != var62) {
                                        if (-65536 == ~var5) {
                                            var5 = -1;
                                        }

                                        var32 = true;
                                        if (0 != ~var5
                                                && -1 != var62.anInt2842
                                                && ~Class954
                                                .list(Class1211
                                                        .list(var5).anInt542).anInt1857 > ~Class954
                                                .list(Class1211
                                                        .list(var62.anInt2842).anInt542).anInt1857) {
                                            var32 = false;
                                        }

                                        if (var32) {
                                            var62.anInt2761 = 0;
                                            var62.anInt2842 = var5;
                                            var62.anInt2759 = Class1134.loopCycle
                                                    - -var20;
                                            var62.anInt2805 = 0;
                                            if (var62.anInt2759 > Class1134.loopCycle) {
                                                var62.anInt2805 = -1;
                                            }

                                            var62.anInt2799 = var3;
                                            var62.anInt2826 = 1;
                                            if (~var62.anInt2842 != 0
                                                    && Class1134.loopCycle == var62.anInt2759) {
                                                var33 = Class1211
                                                        .list(var62.anInt2842).anInt542;
                                                if (~var33 != 0) {
                                                    var53 = Class954
                                                            .list(var33);
                                                    if (var53 != null
                                                            && null != var53.frames) {
                                                        Class1007.method1470(
                                                                var62.x,
                                                                var53,
                                                                183921384,
                                                                var62.y,
                                                                false, 0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else if (-1 != ~(var21 >> 28)) {
                                    var6 = var21 & '\uffff';
                                    Class946 var60;
                                    if (~Class971.anInt2211 == ~var6) {
                                        var60 = Class945.thisClass946;
                                    } else {
                                        var60 = Class922.class946List[var6];
                                    }

                                    if (null != var60) {
                                        if (var5 == '\uffff') {
                                            var5 = -1;
                                        }

                                        var32 = true;
                                        if (var5 != -1
                                                && ~var60.anInt2842 != 0
                                                && ~Class954
                                                .list(Class1211
                                                        .list(var5).anInt542).anInt1857 > ~Class954
                                                .list(Class1211
                                                        .list(var60.anInt2842).anInt542).anInt1857) {
                                            var32 = false;
                                        }

                                        if (var32) {
                                            var60.anInt2759 = var20
                                                    + Class1134.loopCycle;
                                            var60.anInt2799 = var3;
                                            var60.anInt2842 = var5;
                                            if (~var60.anInt2842 == -65536) {
                                                var60.anInt2842 = -1;
                                            }

                                            var60.anInt2826 = 1;
                                            var60.anInt2761 = 0;
                                            var60.anInt2805 = 0;
                                            if (var60.anInt2759 > Class1134.loopCycle) {
                                                var60.anInt2805 = -1;
                                            }

                                            if (0 != ~var60.anInt2842
                                                    && ~var60.anInt2759 == ~Class1134.loopCycle) {
                                                var33 = Class1211
                                                        .list(var60.anInt2842).anInt542;
                                                if (0 != ~var33) {
                                                    var53 = Class954
                                                            .list(var33);
                                                    if (null != var53
                                                            && null != var53.frames) {
                                                        Class1007.method1470(
                                                                var60.x,
                                                                var53,
                                                                183921384,
                                                                var60.y,
                                                                var60 == Class945.thisClass946,
                                                                0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                var6 = 3 & var21 >> 28;
                                var30 = ((var21 & 268434277) >> 14)
                                        + -Class131.anInt1716;
                                var8 = (var21 & 16383) + -SpriteDefinition.anInt1152;
                                if (~var30 <= -1 && var8 >= 0 && 104 > var30
                                        && ~var8 > -105) {
                                    var8 = var8 * 128 - -64;
                                    var30 = 128 * var30 + 64;
                                    Class1212 var50 = new Class1212(
                                            var5, var6, var30, var8, -var3
                                            + Class121.method1736(var6,
                                            1, var30, var8),
                                            var20, Class1134.loopCycle);
                                    Class3_Sub13_Sub15.aClass61_3177
                                            .insertBack(new Class3_Sub28_Sub2(
                                                    var50));
                                }
                            }

                            Class1008.incomingPacket = -1;
                            return true;
                        } else if (999 != Class1008.incomingPacket) {
                            if (Class1008.incomingPacket == 190) {
                                Class966_2.method819(false);
                                var21 = Class1211.incomingPackets
                                        .aInt122();
                                var3 = Class1211.incomingPackets.aBool1002();
                                var20 = Class1211.incomingPackets
                                        .readUnsignedByte();
                                Class974.currentExp[var21] = var3;
                                Class3_Sub13_Sub15.currentStats[var21] = var20;
                                Class3_Sub20.maxStats[var21] = 1;

                                for (var5 = 0; 98 > var5; ++var5) {
                                    if (ItemDefinition.xpForLevel[var5] <= var3) {
                                        Class3_Sub20.maxStats[var21] = var5 + 2;
                                    }
                                }

                                Class3_Sub28_Sub19.anIntArray3780[Class951
                                        .method633(31, Class49.anInt815++)] = var21;
                                Class1008.incomingPacket = -1;
                                return true;
                            } else if (Class1008.incomingPacket != 104
                                    && 121 != Class1008.incomingPacket
                                    && -98 != ~Class1008.incomingPacket
                                    && ~Class1008.incomingPacket != -15
                                    && ~Class1008.incomingPacket != -203
                                    && ~Class1008.incomingPacket != -136
                                    && Class1008.incomingPacket != 186
                                    && Class1008.incomingPacket != 218
                                    && Class1008.incomingPacket != 39
                                    && Class1008.incomingPacket != 112
                                    && -21 != ~Class1008.incomingPacket
                                    && 16 != Class1008.incomingPacket
                                    && 17 != Class1008.incomingPacket) {
                                if (Class1008.incomingPacket == 137) {
                                    var3 = Class1211.incomingPackets
                                            .getInt();
                                    Class1207 var67 = (Class1207) Class3_Sub13_Sub17.aClass130_3208
                                            .get((long) var3);
                                    if (null != var67) {
                                        Class21.removeOverrideInterface(var67,
                                                true);
                                    }

                                    if (null != Class3_Sub13_Sub7.aClass11_3087) {
                                        Class20.refreshInterface(Class3_Sub13_Sub7.aClass11_3087);
                                        Class3_Sub13_Sub7.aClass11_3087 = null;
                                    }

                                    Class1008.incomingPacket = -1;
                                    return true;
                                } else if (Class1008.incomingPacket != 187) {
									/*
									 * if(Class1008.anInt2147 == 132) { var20 =
									 * Class1211
									 * .aClass3_Sub30_Sub1_532.aInteger233();
									 * var21 =
									 * Class1211.aClass3_Sub30_Sub1_532
									 * .aBoole100(); var5 =
									 * Class1211.aClass3_Sub30_Sub1_532
									 * .aBoole100(); var6 =
									 * Class1211.aClass3_Sub30_Sub1_532
									 * .getInt(); Class153.method2143(var21,
									 * var6, var5, var20);
									 * 
									 * Class1008.anInt2147 = -1; return true; }
									 * else
									 */
                                    if (112 == Class1008.incomingPacket) {
                                        Class65.anInt990 = Class1211.incomingPackets
                                                .readUnsignedByte();
                                        Class107.anInt1452 = Class1211.incomingPackets
                                                .gea100();

                                        for (var20 = Class65.anInt990; var20 < 8 + Class65.anInt990; ++var20) {
                                            for (var3 = Class107.anInt1452; ~var3 > ~(8 + Class107.anInt1452); ++var3) {
                                                if (null != Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var20][var3]) {
                                                    Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[Class26.plane][var20][var3] = null;
                                                    Class128.spawnGroundItem(
                                                            var3, var20);
                                                }
                                            }
                                        }

                                        for (Class990 var68 = (Class990) Class3_Sub13_Sub6.aClass61_3075
                                                .getFirst(); null != var68; var68 = (Class990) Class3_Sub13_Sub6.aClass61_3075
                                                .getNext()) {
                                            if (~var68.anInt2264 <= ~Class65.anInt990
                                                    && 8 + Class65.anInt990 > var68.anInt2264
                                                    && var68.anInt2248 >= Class107.anInt1452
                                                    && ~var68.anInt2248 > ~(8 + Class107.anInt1452)
                                                    && var68.plane == Class26.plane) {
                                                var68.anInt2259 = 0;
                                            }
                                        }

                                        Class1008.incomingPacket = -1;
                                        return true;
                                    } else if (Class1008.incomingPacket == 144) {
                                        var20 = Class1211.incomingPackets
                                                .method_152();
                                        Class1034 var65 = Class7
                                                .getInterface(var20);

                                        for (var21 = 0; var65.inventoryIds.length > var21; ++var21) {
                                            var65.inventoryIds[var21] = -1;
                                            var65.inventoryIds[var21] = 0;
                                        }

                                        Class20.refreshInterface(var65);
                                        Class1008.incomingPacket = -1;
                                        return true;
                                    } else if (-131 == ~Class1008.incomingPacket) {
                                        var20 = Class1211.incomingPackets
                                                .aBool1002();
                                        var21 = Class1211.incomingPackets
                                                .aMethod10();
                                        if (var21 == '\uffff') {
                                            var21 = -1;
                                        }

                                        Class933.method256(-1, 1,
                                                var20, var21);

                                        Class1008.incomingPacket = -1;
                                        return true;
                                    } else if (-193 == ~Class1008.incomingPacket) {
                                        Class161.anInt2028 = Class1211.incomingPackets
                                                .readUnsignedByte();
                                        Class1008.incomingPacket = -1;
                                        return true;
                                    } else if (~Class1008.incomingPacket == -14) {
                                        // Play anim on player?
                                        var20 = Class1211.incomingPackets
                                                .aInt122();
                                        var3 = Class1211.incomingPackets
                                                .aInt2016();
                                        var21 = Class1211.incomingPackets
                                                .readUnsignedByte();
                                        Class26.plane = var3 >> 1;
                                        Class945.thisClass946
                                                .method1981(var20,
                                                        ~(var3 & 1) == -2,
                                                        var21);
                                        Class1008.incomingPacket = -1;
                                        return true;
                                    } else {
                                        int var12;
                                        Class1008 var57;
                                        Class1008 var64;
                                        if (-101 == ~Class1008.incomingPacket) {
											/*
											 * var2 =
											 * Class1211.aClass3_Sub30_Sub1_532
											 * .aVar100(); var21 =
											 * Class1211.
											 * aClass3_Sub30_Sub1_532
											 * .aInteger233(); var5 =
											 * Class1211
											 * .aClass3_Sub30_Sub1_532
											 * .readUnsignedByte();
											 */
                                            // Add friend to friends list
                                            Class1008 name = Class1211.incomingPackets
                                                    .class_91033();
                                            var21 = Class1211.incomingPackets
                                                    .aInteger233(); // world
                                            var5 = Class1211.incomingPackets
                                                    .readUnsignedByte();
                                            var2 = name.toLong();
                                            var31 = true;
                                            if (var2 < 0L) {
                                                var2 &= Long.MAX_VALUE;
                                                var31 = false;
                                            }

                                            var41 = Class922.BLANK_CLASS_1008;
                                            if (-1 > ~var21) {
                                                var41 = Class1211.incomingPackets
                                                        .class_91033();
                                            }
                                            for (var33 = 0; var33 < Class8.localPlayerIds; ++var33) {
                                                if (var2 == Class1209.friendsList[var33]) {
                                                    if (~var21 != ~Class973.anIntArray882[var33]) {
                                                        Class973.anIntArray882[var33] = var21;
                                                        if (var21 > 0) { // online
                                                            Class966_2
                                                                    .sendMessage(
                                                                            Class922.BLANK_CLASS_1008,
                                                                            Class922.combinejStrings(new Class1008[]{
                                                                                    name,
                                                                                    Class1028.hasLoggedInString}),
                                                                            5);
                                                        }

                                                        if (var21 == 0) { // logged
                                                            // out(or
                                                            // just
                                                            // adding
                                                            // to
                                                            // list)
                                                            Class966_2
                                                                    .sendMessage(
                                                                            Class922.BLANK_CLASS_1008,
                                                                            Class922.combinejStrings(new Class1008[]{
                                                                                    name,
                                                                                    Class1209.hasLoggedOutString}),
                                                                            5);
                                                        }
                                                    }

                                                    Class1002.aClass94Array2566[var33] = var41;
                                                    Class57.anIntArray904[var33] = var5;
                                                    name = null;
                                                    Class1042.aBooleanArray73[var33] = var31;
                                                    break;
                                                }
                                            }

                                            boolean var45 = false;
                                            if (null != name
                                                    && Class8.localPlayerIds < 200) {
                                                Class1209.friendsList[Class8.localPlayerIds] = var2;
                                                Class70.localPlayerNames[Class8.localPlayerIds] = name;
                                                Class973.anIntArray882[Class8.localPlayerIds] = var21;
                                                Class1002.aClass94Array2566[Class8.localPlayerIds] = var41;
                                                Class57.anIntArray904[Class8.localPlayerIds] = var5;
                                                Class1042.aBooleanArray73[Class8.localPlayerIds] = var31;
                                                ++Class8.localPlayerIds;
                                            }

                                            Class110.anInt1472 = Class3_Sub13_Sub17.anInt3213;
                                            var10 = Class8.localPlayerIds;

                                            while (~var10 < -1) {
                                                --var10;
                                                var45 = true;

                                                for (var11 = 0; var11 < var10; ++var11) {
                                                    if (~Class973.anIntArray882[var11] != ~Class1048.anInt2451
                                                            && ~Class1048.anInt2451 == ~Class973.anIntArray882[var11
                                                            - -1]
                                                            || Class973.anIntArray882[var11] == 0
                                                            && Class973.anIntArray882[var11
                                                            - -1] != 0) {
                                                        var45 = false;
                                                        var12 = Class973.anIntArray882[var11];
                                                        Class973.anIntArray882[var11] = Class973.anIntArray882[var11
                                                                - -1];
                                                        Class973.anIntArray882[1 + var11] = var12;
                                                        var64 = Class1002.aClass94Array2566[var11];
                                                        Class1002.aClass94Array2566[var11] = Class1002.aClass94Array2566[var11 + 1];
                                                        Class1002.aClass94Array2566[var11
                                                                - -1] = var64;
                                                        var57 = Class70.localPlayerNames[var11];
                                                        Class70.localPlayerNames[var11] = Class70.localPlayerNames[var11 + 1];
                                                        Class70.localPlayerNames[var11
                                                                - -1] = var57;
                                                        long var15 = Class1209.friendsList[var11];
                                                        Class1209.friendsList[var11] = Class1209.friendsList[var11
                                                                - -1];
                                                        Class1209.friendsList[var11
                                                                - -1] = var15;
                                                        int var17 = Class57.anIntArray904[var11];
                                                        Class57.anIntArray904[var11] = Class57.anIntArray904[var11
                                                                - -1];
                                                        Class57.anIntArray904[1 + var11] = var17;
                                                        boolean var18 = Class1042.aBooleanArray73[var11];
                                                        Class1042.aBooleanArray73[var11] = Class1042.aBooleanArray73[var11
                                                                - -1];
                                                        Class1042.aBooleanArray73[var11
                                                                - -1] = var18;
                                                    }
                                                }

                                                if (var45) {
                                                    break;
                                                }
                                            }

                                            Class1008.incomingPacket = -1;
                                            return true;
                                        } else if (-161 == ~Class1008.incomingPacket) {
                                            if (0 != Class1017_2.anInt1704) {
                                                Class3_Sub13_Sub28.aClass94_3353 = Class1211.incomingPackets
                                                        .class_91033();
                                            } else {
                                                Class3_Sub13_Sub28.aClass94_3353 = Class56.aClass94_891;
                                            }

                                            Class1008.incomingPacket = -1;
                                            return true;
                                        } else if (128 != Class1008.incomingPacket) {
                                            if (~Class1008.incomingPacket == -155) {
                                                var3 = Class1211.incomingPackets
                                                        .readUnsignedByte();
                                                var21 = Class1211.incomingPackets
                                                        .readUnsignedByte();
                                                var5 = Class1211.incomingPackets
                                                        .aInteger233();
                                                var6 = Class1211.incomingPackets
                                                        .readUnsignedByte();
                                                var30 = Class1211.incomingPackets
                                                        .readUnsignedByte();
                                                Class3_Sub20.method390(true,
                                                        var6, var5, var30,
                                                        (byte) -124, var21,
                                                        var3);

                                                Class1008.incomingPacket = -1;
                                                return true;
                                            } else if (247 == Class1008.incomingPacket) {
                                                var2 = Class1211.incomingPackets
                                                        .aVar100();
                                                var4 = (long) Class1211.incomingPackets
                                                        .aInteger233();
                                                var29 = (long) Class1211.incomingPackets
                                                        .aBoolean183();
                                                var8 = Class1211.incomingPackets
                                                        .readUnsignedByte();
                                                var33 = Class1211.incomingPackets
                                                        .aInteger233();
                                                boolean var49 = false;
                                                long var51 = (var4 << -737495840)
                                                        - -var29;
                                                int var59 = 0;

                                                label1603:
                                                while (true) {
                                                    if (100 > var59) {
                                                        if (~var51 != ~Class163_Sub2_Sub1.aLongArray4017[var59]) {
                                                            ++var59;
                                                            continue;
                                                        }

                                                        var49 = true;
                                                        break;
                                                    }

                                                    if (var8 <= 1) {
                                                        for (var59 = 0; ~var59 > ~Class955.ignoreListCount; ++var59) {
                                                            if (var2 == Class114.ignoreList[var59]) {
                                                                var49 = true;
                                                                break label1603;
                                                            }
                                                        }
                                                    }
                                                    break;
                                                }

                                                if (!var49
                                                        && ~Class1228.anInt2622 == -1) {
                                                    Class163_Sub2_Sub1.aLongArray4017[Class980.anInt1921] = var51;
                                                    Class980.anInt1921 = (1 + Class980.anInt1921) % 100;
                                                    var64 = Class1030
                                                            .method733(var33)
                                                            .method555(
                                                                    28021,
                                                                    Class1211.incomingPackets);
                                                    if (-3 == ~var8) {
                                                        Class1143.appendChatMessage(
                                                                18,
                                                                var64,
                                                                (Class1008) null,
                                                                Class922.combinejStrings(new Class1008[]{
                                                                        Class21.aClass94_444,
                                                                        Class988
                                                                                .longToString(
                                                                                        var2)
                                                                                .upperCase()}));
                                                    } else if (1 == var8) {
                                                        Class1143.appendChatMessage(
                                                                18,
                                                                var64,
                                                                (Class1008) null,
                                                                Class922.combinejStrings(new Class1008[]{
                                                                        Class32.aClass94_592,
                                                                        Class988
                                                                                .longToString(
                                                                                        var2)
                                                                                .upperCase()}));
                                                    } else {
														/*if (var8 == 3)
															Class1143.appendChatMessage(
																	18,
																	var64,
																	(Class1008) null,
																	client.combinejStrings(new Class1008[] {
																			Class21.pkerImg,
																			Class988
																					.longToString(
																							var2)
																					.upperCase() }));
														else if (var8 == 4)
															Class1143.appendChatMessage(
																	18,
																	var64,
																	(Class1008) null,
																	client.combinejStrings(new Class1008[] {
																			Class21.donatorImg,
																			Class988
																					.longToString(
																							var2)
																					.upperCase() }));
														else if (var8 == 5)
															Class1143.appendChatMessage(
																	18,
																	var64,
																	(Class1008) null,
																	client.combinejStrings(new Class1008[] {
																			Class21.extremeDonatorImg,
																			Class988
																					.longToString(
																							var2)
																					.upperCase() }));
														else if (var8 == 7)
															Class1143.appendChatMessage(
																	18,
																	var64,
																	(Class1008) null,
																	client.combinejStrings(new Class1008[] {
																			Class21.superDonatorImg,
																			Class988
																					.longToString(
																							var2)
																					.upperCase() }));
														else if (var8 == 8)
															Class1143.appendChatMessage(
																	18,
																	var64,
																	(Class1008) null,
																	client.combinejStrings(new Class1008[] {
																			Class21.legendaryDonatorImg,
																			Class988
																					.longToString(
																							var2)
																					.upperCase() }));
														else if (var8 == 6)
															Class1143.appendChatMessage(
																	18,
																	var64,
																	(Class1008) null,
																	client.combinejStrings(new Class1008[] {
																			Class21.supportImg,
																			Class988
																					.longToString(
																							var2)
																					.upperCase() }));
														else*/
                                                        Class1143.appendChatMessage(
                                                                18,
                                                                var64,
                                                                (Class1008) null,
                                                                Class988
                                                                        .longToString(
                                                                                var2)
                                                                        .upperCase());
                                                    }
                                                }

                                                Class1008.incomingPacket = -1;
                                                return true;
                                            } else {
                                                Class1207 var26;
                                                if (~Class1008.incomingPacket != -177) {
                                                    if (Class1008.incomingPacket != 27) {
                                                        if (Class1008.incomingPacket == 2) {
                                                            var20 = Class1211.incomingPackets
                                                                    .aLong5882();
                                                            var21 = Class1211.incomingPackets
                                                                    .aBoole100();
                                                            Class1220.method1385(
                                                                    var21,
                                                                    var20);

                                                            Class1008.incomingPacket = -1;
                                                            return true;
                                                        } else if (Class1008.incomingPacket == 30) {
                                                            Class38_Sub1.systemUpdateCycle = Class1211.incomingPackets
                                                                    .aInteger233() * 30;
                                                            Class1008.incomingPacket = -1;
                                                            Class140_Sub6.anInt2905 = Class3_Sub13_Sub17.anInt3213;
                                                            return true;
                                                        }/*
														 * else
														 * if(~Class1008.anInt2147
														 * == -115) {
														 * Class3_Sub13_Sub29
														 * .method305
														 * (Class38.gameClass942
														 * , Class1211.
														 * aClass3_Sub30_Sub1_532
														 * ,
														 * Class1017_2.anInt1704,
														 * (byte)-126);
														 * Class1008.anInt2147 =
														 * -1; return true; }
														 * else
														 */ else if (9 == Class1008.incomingPacket) {
                                                            var21 = Class1211.incomingPackets
                                                                    .aLong5882();
                                                            var3 = Class1211.incomingPackets
                                                                    .aLong_011();
                                                            Class933
                                                                    .method255(
                                                                            var21,
                                                                            var3);
                                                            Class1008.incomingPacket = -1;
                                                            return true;
                                                        } else if (Class1008.incomingPacket == 163) {
                                                            Class966_2
                                                                    .method819(false);
                                                            Class9.energy = Class1211.incomingPackets
                                                                    .readUnsignedByte();
                                                            Class140_Sub6.anInt2905 = Class3_Sub13_Sub17.anInt3213;
                                                            Class1008.incomingPacket = -1;
                                                            return true;
                                                        } else if (~Class1008.incomingPacket == -210) {
                                                            if (-1 != Class1143.mainScreenInterface) {
                                                                Class976
                                                                        .method124(
                                                                                0,
                                                                                Class1143.mainScreenInterface);
                                                            }

                                                            Class1008.incomingPacket = -1;
                                                            return true;
                                                        } else if (~Class1008.incomingPacket != -192) {
                                                            if (-103 == ~Class1008.incomingPacket) {
                                                                var20 = Class1211.incomingPackets
                                                                        .aLong_011();
                                                                var3 = Class1211.incomingPackets
                                                                        .aInt122();
                                                                var21 = Class1211.incomingPackets
                                                                        .aInteger233();
                                                                Class1001 var39 = Class3_Sub13_Sub24.class1001List[var20];
                                                                if (null != var39) {
                                                                    Class1017_2
                                                                            .method1772(
                                                                                    var3,
                                                                                    var21,
                                                                                    39,
                                                                                    var39);
                                                                }

                                                                Class1008.incomingPacket = -1;
                                                                return true;
                                                            } else if (Class1008.incomingPacket != 159) {
                                                                if (Class1008.incomingPacket == 23) {
                                                                    // incoming
                                                                    // pm
                                                                    Class1008 name = Class1211.incomingPackets
                                                                            .class_91033();
                                                                    var58 = Class1019
                                                                            .method686(Class32
                                                                                    .method992(
                                                                                            Class1211.incomingPackets)
                                                                                    .method1536(
                                                                                            121));
                                                                    Class966_2
                                                                            .sendMessage(
                                                                                    name.upperCase(),
                                                                                    var58,
                                                                                    6);
                                                                    Class1008.incomingPacket = -1;
                                                                    return true;
                                                                } else if (-43 != ~Class1008.incomingPacket) {
                                                                    if (-112 == ~Class1008.incomingPacket) {
                                                                        var3 = Class1211.incomingPackets
                                                                                .method_152();
                                                                        var21 = Class1211.incomingPackets
                                                                                .aBoole100();
                                                                        var5 = Class1211.incomingPackets
                                                                                .aLong_011();
                                                                        var6 = Class1211.incomingPackets
                                                                                .aBoole100();
                                                                        Class933
                                                                                .method256(
                                                                                        var21,
                                                                                        7,
                                                                                        var3,
                                                                                        var5 << -311274832
                                                                                                | var6);
                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (37 == Class1008.incomingPacket) {
                                                                        int id = Class1211.incomingPackets
                                                                                .aMethod10();
                                                                        int value = Class1211.incomingPackets
                                                                                .aBool1002();
                                                                        Class3_Sub13_Sub23
                                                                                .method281(
                                                                                        value,
                                                                                        id);
                                                                        // Class163.method2209(value,
                                                                        // id);
                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (Class1008.incomingPacket == 238) {
                                                                        // send
                                                                        // interface
                                                                        var3 = Class1211.incomingPackets
                                                                                .aLong5882(); // window
                                                                        // |
                                                                        // position
                                                                        var5 = Class1211.incomingPackets
                                                                                .aInteger233(); // interface
                                                                        var20 = Class1211.incomingPackets
                                                                                .gea100(); // walkable
                                                                        var26 = (Class1207) Class3_Sub13_Sub17.aClass130_3208
                                                                                .get((long) var3);
                                                                        if (null != var26) {
                                                                            Class21.removeOverrideInterface(
                                                                                    var26,
                                                                                    var26.uid != var5);
                                                                        }

                                                                        // System.out.println("var3: "
                                                                        // +
                                                                        // var3
                                                                        // +
                                                                        // ", var5: "
                                                                        // +
                                                                        // var5
                                                                        // +
                                                                        // ", var20: "
                                                                        // +
                                                                        // var20
                                                                        // +
                                                                        // " , var26: "
                                                                        // +
                                                                        // var26);

                                                                        // var5
                                                                        // = uid

                                                                        Class21.overrideInterface(
                                                                                var5,
                                                                                var3,
                                                                                var20);

                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (~Class1008.incomingPacket == -132) {
                                                                        for (var20 = 0; var20 < Class922.class946List.length; ++var20) {
                                                                            if (Class922.class946List[var20] != null) {
                                                                                Class922.class946List[var20].currentAnimationId = -1;
                                                                            }
                                                                        }

                                                                        for (var20 = 0; ~Class3_Sub13_Sub24.class1001List.length < ~var20; ++var20) {
                                                                            if (null != Class3_Sub13_Sub24.class1001List[var20]) {
                                                                                Class3_Sub13_Sub24.class1001List[var20].currentAnimationId = -1;
                                                                            }
                                                                        }

                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (~Class1008.incomingPacket == -218
                                                                            || Class1008.incomingPacket == 150) {
                                                                        try {
                                                                            var20 = Class1211.incomingPackets
                                                                                    .readUnsignedByte();
                                                                            Class1025 hint = new Class1025();
                                                                            System.out
                                                                                    .println("testing hint - packet "
                                                                                            + Class1008.incomingPacket);
                                                                            var3 = var20 >> 340093798;
                                                                            hint.type = var20 & 63;
                                                                            hint.iconIndex = Class1211.incomingPackets
                                                                                    .readUnsignedByte();
                                                                            if (~hint.iconIndex <= -1
                                                                                    && ~hint.iconIndex > ~Class166.hintIconSprites.length) {
                                                                                if (~hint.type != -2
                                                                                        && 10 != hint.type) {
                                                                                    if (-3 >= ~hint.type
                                                                                            && 6 >= hint.type) {

                                                                                        // 2-6
                                                                                        // --
                                                                                        // offset
                                                                                        if (hint.type == 2) {
                                                                                            hint.anInt1346 = 64;
                                                                                            hint.anInt1350 = 64;
                                                                                        }

                                                                                        if (-4 == ~hint.type) {
                                                                                            hint.anInt1346 = 0;
                                                                                            hint.anInt1350 = 64;
                                                                                        }

                                                                                        if (4 == hint.type) {
                                                                                            hint.anInt1346 = 128;
                                                                                            hint.anInt1350 = 64;
                                                                                        }

                                                                                        if (5 == hint.type) {
                                                                                            hint.anInt1346 = 64;
                                                                                            hint.anInt1350 = 0;
                                                                                        }

                                                                                        if (-7 == ~hint.type) {
                                                                                            hint.anInt1346 = 64;
                                                                                            hint.anInt1350 = 128;
                                                                                        }

                                                                                        hint.type = 2;

                                                                                        // x,
                                                                                        // y,
                                                                                        // z
                                                                                        hint.anInt1356 = Class1211.incomingPackets
                                                                                                .aInteger233();
                                                                                        hint.anInt1347 = Class1211.incomingPackets
                                                                                                .aInteger233();
                                                                                        hint.anInt1353 = Class1211.incomingPackets
                                                                                                .readUnsignedByte();
                                                                                    }
                                                                                } else {
                                                                                    hint.index = Class1211.incomingPackets
                                                                                            .aInteger233();
                                                                                    Class1211.incomingPackets.offset += 3;
                                                                                }

                                                                                hint.anInt1355 = Class1211.incomingPackets
                                                                                        .aInteger233();
                                                                                if (hint.anInt1355 == '\uffff') {
                                                                                    hint.anInt1355 = -1;
                                                                                }

                                                                                // Class1244.hintsList[var3]
                                                                                // =
                                                                                // var48;
                                                                            }
                                                                        } catch (Exception e) {
                                                                            System.out
                                                                                    .println("Exception on hinticon: "
                                                                                            + e);
                                                                        }

                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (75 == Class1008.incomingPacket) {// TODO
                                                                        // -
                                                                        // fix
                                                                        // /
                                                                        // 8
                                                                        // shit
                                                                        Class955.ignoreListCount = Class1017_2.anInt1704 / 8;
                                                                        for (var20 = 0; ~var20 > ~Class955.ignoreListCount; ++var20) {
                                                                            Class1008 name = Class1211.incomingPackets
                                                                                    .class_91033();
                                                                            Class114.ignoreList[var20] = name
                                                                                    .toLong();
                                                                            Class3_Sub13_Sub27.aClass94Array3341[var20] = name;
                                                                        }
                                                                        Class110.anInt1472 = Class3_Sub13_Sub17.anInt3213;
                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (Class1008.incomingPacket == 174) {
                                                                        // npc
                                                                        // update
                                                                        Class3_Sub13_Sub14
                                                                                .method237(8169);
                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (201 == Class1008.incomingPacket) {
                                                                        var3 = Class1211.incomingPackets
                                                                                .aBool1002();
                                                                        var21 = Class1211.incomingPackets
                                                                                .aLong_1884();
                                                                        var5 = Class1211.incomingPackets
                                                                                .getShortA();
                                                                        Class1223
                                                                                .method2271(
                                                                                        var21,
                                                                                        var3,
                                                                                        var5);

                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (Class1008.incomingPacket == 235) {
                                                                        var20 = Class1211.incomingPackets
                                                                                .aInt122();
                                                                        var3 = var20 >> -518983614;
                                                                        var21 = 3 & var20;
                                                                        var5 = Class75.anIntArray1107[var3];
                                                                        var6 = Class1211.incomingPackets
                                                                                .aInteger233();
                                                                        var30 = Class1211.incomingPackets
                                                                                .getInt();
                                                                        if ('\uffff' == var6) {
                                                                            var6 = -1;
                                                                        }

                                                                        var10 = 16383 & var30;
                                                                        var33 = 16383 & var30 >> 2070792462;
                                                                        var33 -= Class131.anInt1716;
                                                                        var10 -= SpriteDefinition.anInt1152;
                                                                        var8 = 3 & var30 >> 765199868;
                                                                        Class1209.method1131(
                                                                                var8,
                                                                                110,
                                                                                var21,
                                                                                var3,
                                                                                var10,
                                                                                var5,
                                                                                var33,
                                                                                var6);
                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (Class1008.incomingPacket == 50) {
                                                                        // PM --
                                                                        // the
                                                                        // "To: "
                                                                        // side
                                                                        // of it
                                                                        Class1008 name = Class1211.incomingPackets
                                                                                .class_91033();
                                                                        var4 = (long) Class1211.incomingPackets
                                                                                .aInteger233();
                                                                        var29 = (long) Class1211.incomingPackets
                                                                                .aBoolean183();
                                                                        var8 = Class1211.incomingPackets
                                                                                .readUnsignedByte();
                                                                        boolean var42 = false;
                                                                        long var35 = var29
                                                                                + (var4 << -1802335520);
                                                                        var12 = 0;

                                                                        label1651:
                                                                        while (true) {
                                                                            if (-101 >= ~var12) {
                                                                                if (var8 <= 1) {
                                                                                    if ((!Class3_Sub15.aBoolean2433 || Class121.aBoolean1641)
                                                                                            && !Class3_Sub13_Sub14.aBoolean3166) {
                                                                                        for (var12 = 0; var12 < Class955.ignoreListCount; ++var12) {
                                                                                            if (~name
                                                                                                    .toLong() == ~Class114.ignoreList[var12]) {
                                                                                                var42 = true;
                                                                                                break label1651;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        var42 = true;
                                                                                    }
                                                                                }
                                                                                break;
                                                                            }

                                                                            if (~var35 == ~Class163_Sub2_Sub1.aLongArray4017[var12]) {
                                                                                var42 = true;
                                                                                break;
                                                                            }

                                                                            ++var12;
                                                                        }

                                                                        if (!var42
                                                                                && -1 == ~Class1228.anInt2622) {
                                                                            // Receive
                                                                            // PM
                                                                            // -
                                                                            // var52
                                                                            // =
                                                                            // msg
                                                                            Class163_Sub2_Sub1.aLongArray4017[Class980.anInt1921] = var35;
                                                                            Class980.anInt1921 = (Class980.anInt1921 - -1) % 100;
                                                                            Class1008 var52 = Class1019
                                                                                    .method686(Class32
                                                                                            .method992(
                                                                                                    Class1211.incomingPackets)
                                                                                            .method1536(
                                                                                                    96));
                                                                            Class922.lastPmFrom = name;

                                                                            if (var8 != 2) {
                                                                                if (var8 != 1) {
																					/*if (var8 == 3)
																						Class966_2
																								.sendMessage(
																										client.combinejStrings(new Class1008[] {
																												Class21.pkerImg,
																												integer_34
																													 * .
																													 * upperCase
																													 * (
																													 * )
																													 }),
																										var52,
																										7);
																					else if (var8 == 4)
																						Class966_2
																								.sendMessage(
																										client.combinejStrings(new Class1008[] {
																												Class21.donatorImg,
																												integer_34
																													 * .
																													 * upperCase
																													 * (
																													 * )
																													 }),
																										var52,
																										7);
																					else if (var8 == 5)
																						Class966_2
																								.sendMessage(
																										client.combinejStrings(new Class1008[] {
																												Class21.extremeDonatorImg,
																												integer_34
																													 * .
																													 * upperCase
																													 * (
																													 * )
																													 }),
																										var52,
																										7);
																					else if (var8 == 7)
																						Class966_2
																								.sendMessage(
																										client.combinejStrings(new Class1008[] {
																												Class21.superDonatorImg,
																												integer_34
																													 * .
																													 * upperCase
																													 * (
																													 * )
																													 }),
																										var52,
																										7);
																					else if (var8 == 8)
																						Class966_2
																								.sendMessage(
																										client.combinejStrings(new Class1008[] {
																												Class21.legendaryDonatorImg,
																												integer_34
																													 * .
																													 * upperCase
																													 * (
																													 * )
																													 }),
																										var52,
																										7);
																					else if (var8 == 6)
																						Class966_2
																								.sendMessage(
																										client.combinejStrings(new Class1008[] {
																												Class21.supportImg,
																												integer_34
																													 * .
																													 * upperCase
																													 * (
																													 * )
																													 }),
																										var52,
																										7);
																					else*/
                                                                                    Class966_2
                                                                                            .sendMessage(
                                                                                                    name/*
																											 * .
																											 * upperCase
																											 * (
																											 * )
																											 */,
                                                                                                    var52,
                                                                                                    3);
                                                                                } else {
                                                                                    Class966_2
                                                                                            .sendMessage(
                                                                                                    Class922.combinejStrings(new Class1008[]{
                                                                                                            Class32.aClass94_592,
                                                                                                            name /*
																												 * .
																												 * upperCase
																												 * (
																												 * )
																												 */}),
                                                                                                    var52,
                                                                                                    7);
                                                                                }
                                                                            } else {
                                                                                Class966_2
                                                                                        .sendMessage(
                                                                                                Class922.combinejStrings(new Class1008[]{
                                                                                                        Class21.aClass94_444,
                                                                                                        name /*
																											 * .
																											 * upperCase
																											 * (
																											 * )
																											 */}),
                                                                                                var52,
                                                                                                7);
                                                                            }
                                                                            // TODO:
                                                                            // Click
                                                                            // here
                                                                            // to
                                                                            // quickly
                                                                            // reply...
                                                                            // TODO:
                                                                            // Disable
                                                                            // warnings
                                                                            if (Class122.unfocused)
                                                                                Class122
                                                                                        .getTray()
                                                                                        .displayMessage(
                                                                                                "PM Received from "
                                                                                                        + name,
                                                                                                var52.toString(),
                                                                                                TrayIcon.MessageType.INFO);
                                                                            // JOptionPane.showMessageDialog(null,
                                                                            // var52.toString(),
                                                                            // "PM From "
                                                                            // +
                                                                            // integer_34.upperCase(),
                                                                            // JOptionPane.PLAIN_MESSAGE);
                                                                        }

                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    } else if (-55 != ~Class1008.incomingPacket) {
                                                                        if (Class1008.incomingPacket == 222) {
                                                                            Class39.createRegion(true);
                                                                            Class1008.incomingPacket = -1;
                                                                            return true;
                                                                        } else if (~Class1008.incomingPacket != -173) {
                                                                            if (8 == Class1008.incomingPacket) {
                                                                                // player
                                                                                // chathead
                                                                                var3 = Class1211.incomingPackets
                                                                                        .aBool1002();
                                                                                var21 = 0;
                                                                                if (Class945.thisClass946.class1098 != null) {
                                                                                    var21 = Class945.thisClass946.class1098
                                                                                            .method1163(-24861);
                                                                                    // returns
                                                                                    // player
                                                                                    // appearance(or
                                                                                    // npc)
                                                                                }
                                                                                // var4
                                                                                // =
                                                                                // apperance
                                                                                // data,
                                                                                // also
                                                                                // NPC
                                                                                // ID
                                                                                // for
                                                                                // npc
																				/*
																				 * System
																				 * .
																				 * out
																				 * .
																				 * println
																				 * (
																				 * "var3(inter child id): "
																				 * +
																				 * var3
																				 * +
																				 * ", var21(appearance data): "
																				 * +
																				 * var21
																				 * )
																				 * ;
																				 * int
																				 * zoom
																				 * =
																				 * Class7
																				 * .
																				 * getInterface
																				 * (
																				 * var3
																				 * )
																				 * .
																				 * zoom
																				 * ;
																				 * System
																				 * .
																				 * out
																				 * .
																				 * println
																				 * (
																				 * "zoom: "
																				 * +
																				 * Class7
																				 * .
																				 * getInterface
																				 * (
																				 * var3
																				 * )
																				 * .
																				 * zoom
																				 * )
																				 * ;
																				 * if
																				 * (
																				 * zoom
																				 * ==
																				 * 2000
																				 * )
																				 * {
																				 * Class7
																				 * .
																				 * getInterface
																				 * (
																				 * var3
																				 * )
																				 * .
																				 * zoom
																				 * =
																				 * 796
																				 * ;
																				 * Class20
																				 * .
																				 * refreshInterface
																				 * (
																				 * Class7
																				 * .
																				 * getInterface
																				 * (
																				 * var3
																				 * )
																				 * )
																				 * ;
																				 * }
																				 */
                                                                                Class933
                                                                                        .method256(
                                                                                                -1,
                                                                                                3,
                                                                                                var3,
                                                                                                var21);
                                                                                Class1008.incomingPacket = -1;
                                                                                return true;
                                                                            } else if (Class1008.incomingPacket == 47) {
                                                                                //Modify interface text
                                                                                var20 = Class1211.incomingPackets
                                                                                        .aLong5882();
                                                                                var24 = Class1211.incomingPackets
                                                                                        .class_91033();

                                                                                Class956
                                                                                        .renameText(
                                                                                                var24,
                                                                                                var20);
                                                                                if (var20 >= Class922.CLAN_MEMBERS_START && var20 < Class922.CLAN_MEMBERS_START + Class922.CLAN_MEMBERS_SIZE) {
                                                                                    String name = var24.toString().replaceAll("\\<.*?\\>", "").trim();
                                                                                    if (name.length() <= 0 || name == null) {
                                                                                        Class1209.clanMembersList[var20 - Class922.CLAN_MEMBERS_START] = null;
                                                                                    } else
                                                                                        Class1209.clanMembersList[var20 - Class922.CLAN_MEMBERS_START] = name;
                                                                                    //System.out.println("var20: " + var20 + ", var24: " + var24 + ", integer_34: " + integer_34 + ", id: " + (var20 - 40108065));
                                                                                }

                                                                                Class1008.incomingPacket = -1;
                                                                                return true;
                                                                            } else {
                                                                                Class1034 var25;
                                                                                if (Class1008.incomingPacket != 120) {
                                                                                    if (Class1008.incomingPacket == 24) {
                                                                                        Class955
                                                                                                .method560(-21556);
                                                                                        Class1008.incomingPacket = -1;
                                                                                        return true;
                                                                                    } else if (Class1008.incomingPacket == 167) {
                                                                                        Class167.processLogout();
                                                                                        Class1008.incomingPacket = -1;
                                                                                        return false;
                                                                                    } else if (116 == Class1008.incomingPacket) {
                                                                                        var20 = Class1211.incomingPackets
                                                                                                .readUnsignedByte();
                                                                                        if (-1 != ~Class1211.incomingPackets
                                                                                                .readUnsignedByte()) {
                                                                                            --Class1211.incomingPackets.offset;
                                                                                            Class3_Sub13_Sub33.aClass133Array3393[var20] = new Class974(
                                                                                                    Class1211.incomingPackets);
                                                                                        } else {
                                                                                            Class3_Sub13_Sub33.aClass133Array3393[var20] = new Class974();
                                                                                        }

                                                                                        Class1008.incomingPacket = -1;
                                                                                        Class121.anInt1642 = Class3_Sub13_Sub17.anInt3213;
                                                                                        return true;
                                                                                    } else if (207 == Class1008.incomingPacket) {
                                                                                        // chathead
                                                                                        // npc
                                                                                        var20 = Class1211.incomingPackets
                                                                                                .aBoole100(); // npc id
                                                                                        var3 = Class1211.incomingPackets
                                                                                                .getInt(); // inter/child
                                                                                      //  if (Class929.aInteger_510
                                                                                            //    >
                                                                                           //     474)
                                                                                         //   Class7.getInterface(var3).zoom
                                                                                            //        =
                                                                                            //        2000;
                                                                                     if (Class7
                                                                                                .getInterface(var3).zoom == 2000
                                                                                                && var20/* npcId */ >= 5500) {
                                                                                            Class7.getInterface(var3).zoom = 796;
                                                                                            Class20.refreshInterface(Class7
                                                                                                    .getInterface(var3));
                                                                                        }
                                                                                        if (~var20 == -65536) {
                                                                                            var20 = -1;
                                                                                        }
                                                                                        System.out.println("[RECIEVING NPC CHAT HEAD] " + var20 + " " + var3 + " ");

                                                                                        Class933
                                                                                                .method256(
                                                                                                        -1,
                                                                                                        2,
                                                                                                        var3,
                                                                                                        var20);

                                                                                        Class1008.incomingPacket = -1;
                                                                                        return true;
                                                                                    } else if (Class1008.incomingPacket == 221) {// TODO
                                                                                        // map
                                                                                        // region
                                                                                        Class39.createRegion(false);
                                                                                        Class1008.incomingPacket = -1;
                                                                                        return true;
                                                                                    } else if (254 != Class1008.incomingPacket) {
                                                                                        if (Class1008.incomingPacket == 152) {
                                                                                            Class1025.anInt1357 = Class1211.incomingPackets
                                                                                                    .readUnsignedByte();
                                                                                            Class110.anInt1472 = Class3_Sub13_Sub17.anInt3213;
                                                                                            Class1008.incomingPacket = -1;
                                                                                            return true;
                                                                                        } else if (Class1008.incomingPacket != 196) {
                                                                                            if (114 != Class1008.incomingPacket) {
                                                                                                if (Class1008.incomingPacket != 92) {
																									/*
																									 * if
																									 * (
																									 * ~
																									 * Class1008
																									 * .
																									 * anInt2147
																									 * ==
																									 * -
																									 * 143
																									 * )
																									 * {
																									 * Class1030
																									 * .
																									 * method734
																									 * (
																									 * 0
																									 * ,
																									 * Class1211
																									 * .
																									 * aClass3_Sub30_Sub1_532
																									 * .
																									 * class_91033
																									 * (
																									 * )
																									 * )
																									 * ;
																									 * Class1008
																									 * .
																									 * anInt2147
																									 * =
																									 * -
																									 * 1
																									 * ;
																									 * return
																									 * true
																									 * ;
																									 * }
																									 * else
																									 */
                                                                                                    if (Class1008.incomingPacket != 132) {

                                                                                                        if (4 == Class1008.incomingPacket) {
                                                                                                            // Music
                                                                                                            // packet
                                                                                                            // var20
                                                                                                            // =
                                                                                                            // Class1211.aClass3_Sub30_Sub1_532
                                                                                                            // .aBoole100();
                                                                                                            var20 = Class1211.incomingPackets
                                                                                                                    .getInt();
                                                                                                            var20 = 0;
                                                                                                            System.out
                                                                                                                    .println("music id: "
                                                                                                                            + var20);
                                                                                                            if (var20 == '\uffff') {
                                                                                                                var20 = -1;
                                                                                                            }

                                                                                                            Class930.handleMusic(
                                                                                                                    true,
                                                                                                                    var20);
                                                                                                            Class1008.incomingPacket = -1;
                                                                                                            return true;
                                                                                                        } else if (Class1008.incomingPacket != 208) {
                                                                                                            Class49.method1125(
                                                                                                                    "T1 - "
                                                                                                                            + Class1008.incomingPacket
                                                                                                                            + ","
                                                                                                                            + Class7.lastPacket
                                                                                                                            + ","
                                                                                                                            + Class1217.beforeLastPacket
                                                                                                                            + " - "
                                                                                                                            + Class1017_2.anInt1704,
                                                                                                                    (Throwable) null);
                                                                                                            // Class167.processLogout();
                                                                                                            Class1008.incomingPacket = -1;
                                                                                                            return true;
                                                                                                        } else {
                                                                                                            // 208
                                                                                                            // -
                                                                                                            // music
                                                                                                            // effect
                                                                                                            var20 = Class1211.incomingPackets
                                                                                                                    .aInt12000((byte) -118);
                                                                                                            var3 = Class1211.incomingPackets
                                                                                                                    .aLong_011();
                                                                                                            if (var3 == '\uffff') {
                                                                                                                var3 = -1;
                                                                                                            }

                                                                                                            Class167.handleMusicEffect(
                                                                                                                    var20,
                                                                                                                    var3,
                                                                                                                    (byte) -1);
                                                                                                            Class1008.incomingPacket = -1;
                                                                                                            return true;
                                                                                                        }
                                                                                                    } else {
                                                                                                        Class107.anInt1452 = Class1211.incomingPackets
                                                                                                                .getByte();
                                                                                                        Class65.anInt990 = Class1211.incomingPackets
                                                                                                                .getByte();
                                                                                                        Class1008.incomingPacket = -1;
                                                                                                        return true;
                                                                                                    }
                                                                                                } else {
                                                                                                    // PACKET
                                                                                                    // ==
                                                                                                    // 92
                                                                                                    // -
                                                                                                    // send
                                                                                                    // items
                                                                                                    var20 = Class1211.incomingPackets
                                                                                                            .getInt();
                                                                                                    var3 = Class1211.incomingPackets
                                                                                                            .aInteger233();
                                                                                                    if (var20 == 786468
                                                                                                            && Class922.resetBankScroller) {
                                                                                                        if (Class7.getInterface(786441) != null)
                                                                                                            Class7.getInterface(786441).scrollPosition = 0;
                                                                                                        Class922.newScrollerToReset = 786526;
                                                                                                        Class922.resetBankScroller = false;
                                                                                                    }
                                                                                                    if (~var20 > 69999) {
                                                                                                        var3 += '\u8000';
                                                                                                    }
                                                                                                    if (0 <= var20) {
                                                                                                        var25 = Class7
                                                                                                                .getInterface(var20);
                                                                                                    } else {
                                                                                                        var25 = null;
                                                                                                    }
                                                                                                    if (var25 != null) {
                                                                                                        for (var5 = 0; var25.inventoryIds.length > var5; ++var5) {
                                                                                                            var25.inventoryIds[var5] = 0;
                                                                                                            var25.inventoryAmounts[var5] = 0;
                                                                                                        }
                                                                                                    }
                                                                                                    Class10.method852(
                                                                                                            (byte) 114,
                                                                                                            var3);
                                                                                                    var5 = Class1211.incomingPackets
                                                                                                            .aInteger233();
                                                                                                    for (var6 = 0; var5 > var6; ++var6) {
                                                                                                        var30 = Class1211.incomingPackets
                                                                                                                .gea100();
                                                                                                        if (255 == var30) {
                                                                                                            var30 = Class1211.incomingPackets
                                                                                                                    .getInt();
                                                                                                        }
                                                                                                        var8 = Class1211.incomingPackets
                                                                                                                .aLong_011();
                                                                                                        if (null != var25
                                                                                                                && ~var6 > ~var25.inventoryIds.length) {
                                                                                                            var25.inventoryIds[var6] = var8;
                                                                                                            var25.inventoryAmounts[var6] = var30 == 0 ? -1
                                                                                                                    : var30;
                                                                                                        }

                                                                                                        Class1223
                                                                                                                .method2277(
                                                                                                                        -1
                                                                                                                                + var8,
                                                                                                                        var6,
                                                                                                                        var30,
                                                                                                                        var3,
                                                                                                                        (byte) 41);
                                                                                                    }

                                                                                                    if (var25 != null) {
                                                                                                        Class20.refreshInterface(var25);
                                                                                                    }

                                                                                                    Class966_2
                                                                                                            .method819(false);
                                                                                                    Class3_Sub28_Sub4.anIntArray3565[Class951
                                                                                                            .method633(
                                                                                                                    Class62.anInt944++,
                                                                                                                    31)] = Class951
                                                                                                            .method633(
                                                                                                                    32767,
                                                                                                                    var3);
                                                                                                    Class1008.incomingPacket = -1;
                                                                                                    return true;
                                                                                                }
                                                                                            } else {
                                                                                                var20 = Class1211.incomingPackets
                                                                                                        .aBool1002();
                                                                                                var21 = Class1211.incomingPackets
                                                                                                        .aLong_1884();
                                                                                                var3 = Class1211.incomingPackets
                                                                                                        .aBool1002();
                                                                                                if ('\uffff' == var21) {
                                                                                                    var21 = -1;
                                                                                                }
                                                                                                Class1034 var34 = Class7
                                                                                                        .getInterface(var3);
                                                                                                ItemDefinition var43;
                                                                                                if (var34.scriptedInterface) {
                                                                                                    Class140_Sub6
                                                                                                            .method2026(
                                                                                                                    (byte) 122,
                                                                                                                    var3,
                                                                                                                    var20,
                                                                                                                    var21);
                                                                                                    var43 = ItemDefinition
                                                                                                            .getDefinition(var21);
                                                                                                    Class1027.method2143(
                                                                                                            var43.modelZoom,
                                                                                                            var3,
                                                                                                            var43.rotationX,
                                                                                                            var43.rotationY);
                                                                                                    Class84.method1420(
                                                                                                            var3,
                                                                                                            var43.anInt768,
                                                                                                            var43.offsetY,
                                                                                                            var43.offsetX);
                                                                                                } else {
                                                                                                    if (-1 == var21) {
                                                                                                        var34.mediaTypeDisabled = 0;
                                                                                                        Class1008.incomingPacket = -1;
                                                                                                        return true;
                                                                                                    }

                                                                                                    var43 = ItemDefinition
                                                                                                            .getDefinition(var21);
                                                                                                    var34.rotateX = var43.rotationY;
                                                                                                    var34.zoom = 100
                                                                                                            * var43.modelZoom
                                                                                                            / var20;
                                                                                                    var34.mediaTypeDisabled = 4;
                                                                                                    var34.mediaIdDisabled = var21;
                                                                                                    var34.rotateY = var43.rotationX;
                                                                                                    Class20.refreshInterface(var34);
                                                                                                }

                                                                                                Class1008.incomingPacket = -1;
                                                                                                return true;
                                                                                            }
                                                                                        } else {
                                                                                            var2 = Class1211.incomingPackets
                                                                                                    .aVar100();
                                                                                            var21 = Class1211.incomingPackets
                                                                                                    .aInteger233();
                                                                                            byte var28 = Class1211.incomingPackets
                                                                                                    .getByte();
                                                                                            var31 = false;
                                                                                            if (-1L != ~(Long.MIN_VALUE & var2)) {
                                                                                                var31 = true;
                                                                                            }

                                                                                            if (!var31) {
                                                                                                var41 = Class1211.incomingPackets
                                                                                                        .class_91033();
                                                                                                Class0 var40 = new Class0();
                                                                                                var40.hash = var2;
                                                                                                var40.name = Class988
                                                                                                        .longToString(var40.hash);
                                                                                                var40.aByte2472 = var28;
                                                                                                var40.aClass94_2473 = var41;
                                                                                                var40.anInt2478 = var21;

                                                                                                for (var33 = -1
                                                                                                        + Class1002.anInt2572; ~var33 <= -1; --var33) {
                                                                                                    var10 = Class951.clanMembers[var33].name
                                                                                                            .contains(var40.name);
                                                                                                    if (-1 == ~var10) {
                                                                                                        Class951.clanMembers[var33].anInt2478 = var21;
                                                                                                        Class951.clanMembers[var33].aByte2472 = var28;
                                                                                                        Class951.clanMembers[var33].aClass94_2473 = var41;
                                                                                                        if (~var2 == ~Class3_Sub13_Sub16.aLong3202) {
                                                                                                            Class972.aByte1308 = var28;
                                                                                                        }

                                                                                                        Class167.anInt2087 = Class3_Sub13_Sub17.anInt3213;
                                                                                                        Class1008.incomingPacket = -1;
                                                                                                        return true;
                                                                                                    }

                                                                                                    if (var10 < 0) {
                                                                                                        break;
                                                                                                    }
                                                                                                }

                                                                                                if (Class951.clanMembers.length <= Class1002.anInt2572) {
                                                                                                    Class1008.incomingPacket = -1;
                                                                                                    return true;
                                                                                                }

                                                                                                for (var10 = Class1002.anInt2572
                                                                                                        + -1; ~var33 > ~var10; --var10) {
                                                                                                    Class951.clanMembers[1 + var10] = Class951.clanMembers[var10];
                                                                                                }

                                                                                                if (-1 == ~Class1002.anInt2572) {
                                                                                                    Class951.clanMembers = new Class0[100];
                                                                                                }

                                                                                                Class951.clanMembers[1 + var33] = var40;
                                                                                                if (Class3_Sub13_Sub16.aLong3202 == var2) {
                                                                                                    Class972.aByte1308 = var28;
                                                                                                }

                                                                                                ++Class1002.anInt2572;
                                                                                            } else {
                                                                                                if (~Class1002.anInt2572 == -1) {
                                                                                                    Class1008.incomingPacket = -1;
                                                                                                    return true;
                                                                                                }

                                                                                                boolean var37 = false;
                                                                                                var2 &= Long.MAX_VALUE;

                                                                                                for (var30 = 0; ~Class1002.anInt2572 < ~var30
                                                                                                        && (var2 != Class951.clanMembers[var30].hash || ~var21 != ~Class951.clanMembers[var30].anInt2478); ++var30) {
                                                                                                    ;
                                                                                                }

                                                                                                if (var30 < Class1002.anInt2572) {
                                                                                                    while (~(-1 + Class1002.anInt2572) < ~var30) {
                                                                                                        Class951.clanMembers[var30] = Class951.clanMembers[1 + var30];
                                                                                                        ++var30;
                                                                                                    }

                                                                                                    --Class1002.anInt2572;
                                                                                                    Class951.clanMembers[Class1002.anInt2572] = null;
                                                                                                }
                                                                                            }

                                                                                            Class1008.incomingPacket = -1;
                                                                                            Class167.anInt2087 = Class3_Sub13_Sub17.anInt3213;
                                                                                            return true;
                                                                                        }
                                                                                    } else {
                                                                                        var3 = Class1211.incomingPackets
                                                                                                .aLong_011();
                                                                                        if (var3 == '\uffff') {
                                                                                            var3 = -1;
                                                                                        }

                                                                                        var21 = Class1211.incomingPackets
                                                                                                .getInt();
                                                                                        var5 = Class1211.incomingPackets
                                                                                                .aMethod10();
                                                                                        var6 = Class1211.incomingPackets
                                                                                                .aLong5882();
                                                                                        if (~var5 == -65536) {
                                                                                            var5 = -1;
                                                                                        }

                                                                                        for (var30 = var5; ~var3 <= ~var30; ++var30) {
                                                                                            var36 = ((long) var21 << -1381724512)
                                                                                                    - -((long) var30);
                                                                                            var47 = (Class971) Class124.aClass130_1659
                                                                                                    .get(var36);
                                                                                            if (var47 == null) {
                                                                                                if (-1 == var30) {
                                                                                                    var38 = new Class971(
                                                                                                            var6,
                                                                                                            Class7.getInterface(var21).clickMask.anInt2202);
                                                                                                } else {
                                                                                                    var38 = new Class971(
                                                                                                            var6,
                                                                                                            -1);
                                                                                                }
                                                                                            } else {
                                                                                                var38 = new Class971(
                                                                                                        var6,
                                                                                                        var47.anInt2202);
                                                                                                var47.unlink();
                                                                                            }

                                                                                            Class124.aClass130_1659
                                                                                                    .put(var38,
                                                                                                            var36);
                                                                                        }

                                                                                        Class1008.incomingPacket = -1;
                                                                                        return true;
                                                                                    }
                                                                                } else {
                                                                                    var20 = Class1211.incomingPackets
                                                                                            .getInt();
                                                                                    var3 = Class1211.incomingPackets
                                                                                            .aInteger233();
                                                                                    if (69999 < ~var20) {
                                                                                        var3 += '\u8000';
                                                                                    }

                                                                                    if (var20 < 0) {
                                                                                        var25 = null;
                                                                                    } else {
                                                                                        var25 = Class7
                                                                                                .getInterface(var20);
                                                                                    }

                                                                                    for (; ~Class1211.incomingPackets.offset > ~Class1017_2.anInt1704; Class1223
                                                                                            .method2277(
                                                                                                    var6
                                                                                                            + -1,
                                                                                                    var5,
                                                                                                    var30,
                                                                                                    var3,
                                                                                                    (byte) 46)) {
                                                                                        var5 = Class1211.incomingPackets
                                                                                                .aNon145();
                                                                                        var6 = Class1211.incomingPackets
                                                                                                .aInteger233();
                                                                                        var30 = 0;
                                                                                        if (var6 != 0) {
                                                                                            var30 = Class1211.incomingPackets
                                                                                                    .readUnsignedByte();
                                                                                            if (-256 == ~var30) {
                                                                                                var30 = Class1211.incomingPackets
                                                                                                        .getInt();
                                                                                            }
                                                                                        }

                                                                                        if (var25 != null
                                                                                                && ~var5 <= -1
                                                                                                && ~var25.inventoryIds.length < ~var5) {
                                                                                            var25.inventoryIds[var5] = var6;
                                                                                            var25.inventoryAmounts[var5] = var30;
                                                                                        }
                                                                                    }

                                                                                    if (var25 != null) {
                                                                                        Class20.refreshInterface(var25);
                                                                                    }

                                                                                    Class966_2
                                                                                            .method819(false);
                                                                                    Class3_Sub28_Sub4.anIntArray3565[Class951
                                                                                            .method633(
                                                                                                    Class62.anInt944++,
                                                                                                    31)] = Class951
                                                                                            .method633(
                                                                                                    32767,
                                                                                                    var3);
                                                                                    Class1008.incomingPacket = -1;
                                                                                    return true;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            var20 = Class1211.incomingPackets
                                                                                    .aInteger233();
                                                                            var3 = Class1211.incomingPackets
                                                                                    .readUnsignedByte();
                                                                            if (-65536 == ~var20) {
                                                                                var20 = -1;
                                                                            }

                                                                            var21 = Class1211.incomingPackets
                                                                                    .aInteger233();
                                                                            Class3_Sub13_Sub6
                                                                                    .method199(
                                                                                            var3,
                                                                                            var20,
                                                                                            var21,
                                                                                            -799);
                                                                            Class1008.incomingPacket = -1;
                                                                            return true;
                                                                        }
                                                                    } else {
                                                                        var2 = Class1211.incomingPackets
                                                                                .aVar100();
                                                                        Class1211.incomingPackets
                                                                                .getByte();
                                                                        var4 = Class1211.incomingPackets
                                                                                .aVar100();
                                                                        var29 = (long) Class1211.incomingPackets
                                                                                .aInteger233();
                                                                        var36 = (long) Class1211.incomingPackets
                                                                                .aBoolean183();
                                                                        long var44 = (var29 << -164903776)
                                                                                + var36;
                                                                        var10 = Class1211.incomingPackets
                                                                                .readUnsignedByte();
                                                                        boolean var13 = false;
                                                                        int var14 = 0;

                                                                        label1774:
                                                                        while (true) {
                                                                            if (var14 >= 100) {
                                                                                if (1 >= var10) {
                                                                                    if ((!Class3_Sub15.aBoolean2433 || Class121.aBoolean1641)
                                                                                            && !Class3_Sub13_Sub14.aBoolean3166) {
                                                                                        for (var14 = 0; Class955.ignoreListCount > var14; ++var14) {
                                                                                            if (Class114.ignoreList[var14] == var2) {
                                                                                                var13 = true;
                                                                                                break label1774;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        var13 = true;
                                                                                    }
                                                                                }
                                                                                break;
                                                                            }

                                                                            if (Class163_Sub2_Sub1.aLongArray4017[var14] == var44) {
                                                                                var13 = true;
                                                                                break;
                                                                            }

                                                                            ++var14;
                                                                        }

                                                                        if (!var13
                                                                                && 0 == Class1228.anInt2622) {
                                                                            Class163_Sub2_Sub1.aLongArray4017[Class980.anInt1921] = var44;
                                                                            Class980.anInt1921 = (Class980.anInt1921 + 1) % 100;
                                                                            var57 = Class1019
                                                                                    .method686(Class32
                                                                                            .method992(
                                                                                                    Class1211.incomingPackets)
                                                                                            .method1536(
                                                                                                    116));
                                                                            if (var10 != 2) {
                                                                                if (var10 == 1) {
                                                                                    Class3_Sub13_Sub11
                                                                                            .method221(
                                                                                                    -1,
                                                                                                    var57,
                                                                                                    Class922.combinejStrings(new Class1008[]{
                                                                                                            Class32.aClass94_592,
                                                                                                            Class988
                                                                                                                    .longToString(
                                                                                                                            var2)
                                                                                                                    .upperCase()}),
                                                                                                    Class988
                                                                                                            .longToString(
                                                                                                                    var4)
                                                                                                            .upperCase(),
                                                                                                    9);
                                                                                } else {
																					/*if (var10 == 3)
																						Class3_Sub13_Sub11
																								.method221(
																										-1,
																										var57,
																										client.combinejStrings(new Class1008[] {
																												Class21.pkerImg,
																												Class988
																														.longToString(
																																var2)
																														.upperCase() }),
																										Class988
																												.longToString(
																														var4)
																												.upperCase(),
																										9);
																					else if (var10 == 4)
																						Class3_Sub13_Sub11
																								.method221(
																										-1,
																										var57,
																										client.combinejStrings(new Class1008[] {
																												Class21.donatorImg,
																												Class988
																														.longToString(
																																var2)
																														.upperCase() }),
																										Class988
																												.longToString(
																														var4)
																												.upperCase(),
																										9);
																					else if (var10 == 5)
																						Class3_Sub13_Sub11
																								.method221(
																										-1,
																										var57,
																										client.combinejStrings(new Class1008[] {
																												Class21.extremeDonatorImg,
																												Class988
																														.longToString(
																																var2)
																														.upperCase() }),
																										Class988
																												.longToString(
																														var4)
																												.upperCase(),
																										9);
																					else if (var10 == 7)
																						Class3_Sub13_Sub11
																								.method221(
																										-1,
																										var57,
																										client.combinejStrings(new Class1008[] {
																												Class21.superDonatorImg,
																												Class988
																														.longToString(
																																var2)
																														.upperCase() }),
																										Class988
																												.longToString(
																														var4)
																												.upperCase(),
																										9);
																					else if (var10 == 8)
																						Class3_Sub13_Sub11
																								.method221(
																										-1,
																										var57,
																										client.combinejStrings(new Class1008[] {
																												Class21.legendaryDonatorImg,
																												Class988
																														.longToString(
																																var2)
																														.upperCase() }),
																										Class988
																												.longToString(
																														var4)
																												.upperCase(),
																										9);
																					else if (var10 == 6)
																						Class3_Sub13_Sub11
																								.method221(
																										-1,
																										var57,
																										client.combinejStrings(new Class1008[] {
																												Class21.supportImg,
																												Class988
																														.longToString(
																																var2)
																														.upperCase() }),
																										Class988
																												.longToString(
																														var4)
																												.upperCase(),
																										9);
																					else*/
                                                                                    Class3_Sub13_Sub11
                                                                                            .method221(
                                                                                                    -1,
                                                                                                    var57,
                                                                                                    Class988
                                                                                                            .longToString(
                                                                                                                    var2)
                                                                                                            .upperCase(),
                                                                                                    Class988
                                                                                                            .longToString(
                                                                                                                    var4)
                                                                                                            .upperCase(),
                                                                                                    9);
                                                                                }
                                                                            } else {
                                                                                Class3_Sub13_Sub11
                                                                                        .method221(
                                                                                                -1,
                                                                                                var57,
                                                                                                Class922.combinejStrings(new Class1008[]{
                                                                                                        Class21.aClass94_444,
                                                                                                        Class988
                                                                                                                .longToString(
                                                                                                                        var2)
                                                                                                                .upperCase()}),
                                                                                                Class988
                                                                                                        .longToString(
                                                                                                                var4)
                                                                                                        .upperCase(),
                                                                                                9);
                                                                            }
                                                                        }

                                                                        Class1008.incomingPacket = -1;
                                                                        return true;
                                                                    }
                                                                } else {
                                                                    if (null != Class3_Sub13_Sub10.fullscreenFrame) {
                                                                        System.out
                                                                                .println("settinnng69");
                                                                        Class1031
                                                                                .setLowDefinition(
                                                                                        false,
                                                                                        Class1002.anInt2577,
                                                                                        -1,
                                                                                        -1);
                                                                    }

                                                                    byte[] var22 = new byte[Class1017_2.anInt1704];
                                                                    Class1211.incomingPackets
                                                                            .getBytesIsaac(
                                                                                    var22,
                                                                                    0,
                                                                                    Class1017_2.anInt1704);
                                                                    var24 = Class3_Sub13_Sub3
                                                                            .bufferToString(
                                                                                    var22,
                                                                                    0,
                                                                                    Class1017_2.anInt1704);
																	/*
																	 * if(null
																	 * ==
																	 * Class3_Sub13_Sub7
																	 * .
																	 * aFrame3092
																	 * && (3 ==
																	 * Class942
																	 * .
																	 * anInt1214
																	 * ||
																	 * !Class942
																	 * .osName.
																	 * startsWith
																	 * ("win")
																	 * ||
																	 * Class106
																	 * .
																	 * aBoolean1451
																	 * )) {
																	 * Class99
																	 * .method1596
																	 * (var24,
																	 * true); }
																	 * else {
																	 * Class3_Sub13_Sub24
																	 * .
																	 * aClass94_3295
																	 * = var24;
																	 * Class1008
																	 * .aBoolean2154
																	 * = true;
																	 * Class15
																	 * .aClass64_351
																	 * =
																	 * Class38.
																	 * gameClass942
																	 * .
																	 * method1452
																	 * (new
																	 * String
																	 * (var24
																	 * .method1568
																	 * (0),
																	 * "ISO-8859-1"
																	 * ), true);
																	 * }
																	 */

                                                                    Class1008.incomingPacket = -1;
                                                                    return true;
                                                                }
                                                            } else {
                                                                Class966_2
                                                                        .method819(false);
                                                                Class980.weigth = Class1211.incomingPackets
                                                                        .aLong_1884();
                                                                Class140_Sub6.anInt2905 = Class3_Sub13_Sub17.anInt3213;
                                                                Class1008.incomingPacket = -1;
                                                                return true;
                                                            }
                                                        } else {
                                                            var20 = Class1211.incomingPackets
                                                                    .aLong_011();
                                                            Class3_Sub28_Sub1
                                                                    .method532(var20);
                                                            Class3_Sub28_Sub4.anIntArray3565[Class951
                                                                    .method633(
                                                                            31,
                                                                            Class62.anInt944++)] = Class951
                                                                    .method633(
                                                                            var20,
                                                                            32767);
                                                            Class1008.incomingPacket = -1;
                                                            return true;
                                                        }
                                                    } else {
                                                        var3 = Class1211.incomingPackets
                                                                .readUnsignedByte();
                                                        var21 = Class1211.incomingPackets
                                                                .readUnsignedByte();
                                                        var5 = Class1211.incomingPackets
                                                                .readUnsignedByte();
                                                        var6 = Class1211.incomingPackets
                                                                .readUnsignedByte();
                                                        var30 = Class1211.incomingPackets
                                                                .aInteger233();
                                                        Class104.aBooleanArray2169[var3] = true;
                                                        Class3_Sub13_Sub32.anIntArray3383[var3] = var21;
                                                        Class166.anIntArray2073[var3] = var5;
                                                        Class3_Sub13_Sub29.anIntArray3359[var3] = var6;
                                                        Class163_Sub1_Sub1.anIntArray4009[var3] = var30;

                                                        Class1008.incomingPacket = -1;
                                                        return true;
                                                    }
                                                } else {
                                                    var20 = Class1211.incomingPackets
                                                            .aLong5882();
                                                    var21 = Class1211.incomingPackets
                                                            .aLong5882();
                                                    Class1207 var23 = (Class1207) Class3_Sub13_Sub17.aClass130_3208
                                                            .get((long) var20);
                                                    var26 = (Class1207) Class3_Sub13_Sub17.aClass130_3208
                                                            .get((long) var21);
                                                    if (null != var26) {
                                                        Class21.removeOverrideInterface(
                                                                var26,
                                                                null == var23
                                                                        || var26.uid != var23.uid);
                                                    }

                                                    if (null != var23) {
                                                        var23.unlink();
                                                        Class3_Sub13_Sub17.aClass130_3208
                                                                .put(var23,
                                                                        (long) var21);
                                                    }

                                                    Class1034 var27 = Class7
                                                            .getInterface(var20);
                                                    if (var27 != null) {
                                                        Class20.refreshInterface(var27);
                                                    }

                                                    var27 = Class7
                                                            .getInterface(var21);
                                                    if (null != var27) {
                                                        Class20.refreshInterface(var27);
                                                        Class1009
                                                                .method2104(
                                                                        var27,
                                                                        true,
                                                                        48);
                                                    }

                                                    if (0 != ~Class1143.mainScreenInterface) {
                                                        Class976
                                                                .method124(
                                                                        1,
                                                                        Class1143.mainScreenInterface);
                                                    }

                                                    Class1008.incomingPacket = -1;
                                                    return true;
                                                }
                                            }
                                        } else {
                                            for (var20 = 0; ~Class163_Sub1.variousSettings.length < ~var20; ++var20) {
                                                if (~Class57.anIntArray898[var20] != ~Class163_Sub1.variousSettings[var20]) {
                                                    Class163_Sub1.variousSettings[var20] = Class57.anIntArray898[var20];
                                                    Class1004.method1087(98,
                                                            var20);
                                                    Class1134.anIntArray726[Class951
                                                            .method633(
                                                                    Class1017.anInt641++,
                                                                    31)] = var20;
                                                }
                                            }

                                            Class1008.incomingPacket = -1;
                                            return true;
                                        }
                                    }
                                } else {
                                    var20 = Class1211.incomingPackets
                                            .aLong_011();
                                    var21 = Class1211.incomingPackets
                                            .aInteger233();
                                    Class1211.cameraYaw = var20;
                                    Class3_Sub9.anInt2309 = var21;
                                    if (-3 == ~Class974.anInt1753) {
                                        Class139.renderPitch = Class3_Sub9.anInt2309;
                                        Class3_Sub13_Sub25.renderYaw = Class1211.cameraYaw;
                                    }

                                    Class47.method1098((byte) -117);

                                    Class1008.incomingPacket = -1;
                                    return true;
                                }
                            } else {
                                Class39.method1038((byte) -99);
                                Class1008.incomingPacket = -1;
                                return true;
                            }
                        } else {
							/*
							 * var20 =
							 * Class1211.aClass3_Sub30_Sub1_532.method_152();
							 * var21 =
							 * Class1211.aClass3_Sub30_Sub1_532.aInteger233();
							 * var5 =
							 * Class1211.aClass3_Sub30_Sub1_532.aMethod10
							 * (); Class114.method1708(var5 + (var21 << 16),
							 * var20);
							 */
                            Class1008.incomingPacket = -1;
                            return true;
                        }
                    }
                }
            } else {
                Class162.method2204(Class1211.incomingPackets);
                Class1008.incomingPacket = -1;
                return true;
            }
        } else {
            var3 = Class1211.incomingPackets.getInt();
            var21 = Class1211.incomingPackets.aMethod10();
            Class933.method255(var21, var3);

            Class1008.incomingPacket = -1;
            return true;
        }
    }

    public static void method828(int var0) {
        try {
            aByteArrayArrayArray81 = (byte[][][]) null;
            // aClass94_85 = null;
            if (var0 > -88) {
                method828(-84);
            }

            aClass61_82 = null;
            aClass11_88 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ac.A(" + var0 + ')');
        }
    }

    static final void method829(int var0) {
        try {
            Class20.refreshInterface(Class56.aClass11_886);
            ++Class75_Sub3.anInt2658;
            if (Class21.aBoolean440 && Class85.aBoolean1167) {
                int var1 = Class1015.mouseX2;
                var1 -= Class144.anInt1881;
                if (Class3_Sub13_Sub13.anInt3156 > var1) {
                    var1 = Class3_Sub13_Sub13.anInt3156;
                }

                int var2 = Class1017_2.anInt1709;
                if (~(Class3_Sub13_Sub13.anInt3156 + aClass11_88.scrollbarWidth) > ~(var1 - -Class56.aClass11_886.scrollbarWidth)) {
                    var1 = -Class56.aClass11_886.scrollbarWidth
                            + Class3_Sub13_Sub13.anInt3156
                            + aClass11_88.scrollbarWidth;
                }

                var2 -= Class95.anInt1336;
                if (~var2 > ~Class134.anInt1761) {
                    var2 = Class134.anInt1761;
                }

                if (Class134.anInt1761 - -aClass11_88.scrollbarHeight < var2
                        - -Class56.aClass11_886.scrollbarHeight) {
                    var2 = Class134.anInt1761 + aClass11_88.scrollbarHeight
                            + -Class56.aClass11_886.scrollbarHeight;
                }

                if (var0 != -1) {
                    aClass61_82 = (Class975) null;
                }

                int var4 = var2 - Class949.anInt2218;
                int var3 = var1 + -Class3_Sub15.anInt2421;
                int var6 = var1 + -Class3_Sub13_Sub13.anInt3156
                        + aClass11_88.anInt247;
                int var7 = aClass11_88.scrollPosition + -Class134.anInt1761
                        + var2;
                int var5 = Class56.aClass11_886.anInt214;
                if (~Class75_Sub3.anInt2658 < ~Class56.aClass11_886.anInt179
                        && (~var5 > ~var3 || ~(-var5) < ~var3 || var4 > var5 || var4 < -var5)) {
                    Class1001.aBoolean3975 = true;
                }

                Class1048 var8;
                if (Class56.aClass11_886.anObjectArray295 != null
                        && Class1001.aBoolean3975) {
                    var8 = new Class1048();
                    var8.aClass11_2449 = Class56.aClass11_886;
                    var8.objectData = Class56.aClass11_886.anObjectArray295;
                    var8.anInt2447 = var6;
                    var8.anInt2441 = var7;
                    Class983.method1065(var8);
                }

                if (0 == Class3_Sub13_Sub5.anInt3069) {
                    if (Class1001.aBoolean3975) {
                        if (Class56.aClass11_886.anObjectArray229 != null) {
                            var8 = new Class1048();
                            var8.anInt2441 = var7;
                            var8.aClass11_2438 = Class27.aClass11_526;
                            var8.anInt2447 = var6;
                            var8.objectData = Class56.aClass11_886.anObjectArray229;
                            var8.aClass11_2449 = Class56.aClass11_886;
                            Class983.method1065(var8);
                        }

                        if (Class27.aClass11_526 != null
                                && Class922.method42(Class56.aClass11_886) != null) {
                            Class3_Sub13_Sub1.outputStream.putPacket(79);
                            Class3_Sub13_Sub1.outputStream
                                    .aRover_107(Class56.aClass11_886.uid);
                            Class3_Sub13_Sub1.outputStream
                                    .method_122(Class27.aClass11_526.anInt191);
                            Class3_Sub13_Sub1.outputStream
                                    .method_211(Class27.aClass11_526.uid);
                            Class3_Sub13_Sub1.outputStream
                                    .method_122(Class56.aClass11_886.anInt191);
                        }
                    } else if ((-2 == ~Class1006.anInt998 || Class3_Sub13_Sub39
                            .method353(-1
                                            + Class3_Sub13_Sub34.contextOptionsAmount,
                                    ~var0))
                            && Class3_Sub13_Sub34.contextOptionsAmount > 2) {
                        Class132.method1801((byte) -97);
                    } else if (~Class3_Sub13_Sub34.contextOptionsAmount < -1) {
                        Class3_Sub13_Sub8.handleClick(96);
                    }

                    Class56.aClass11_886 = null;
                }

            } else {
                if (-2 > ~Class75_Sub3.anInt2658) {
                    Class56.aClass11_886 = null;
                }

            }
        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "ac.F(" + var0 + ')');
        }
    }

}

package com.kit.client;

final class Class986 {

   static Class1008[] aClass94Array45;
   static short aShort46 = 256;
   static boolean aBoolean47 = false;
   static int anInt48 = 2;
   static int[] anIntArray49;
   final static Class1008 GREENJ_STRING = Class943.create("<col=80ff00>");
   //static Class1008 aClass94_51 = Class1008.createJString("; Expires=");


	static final short[] arrayCopy(short[] var1) {
		if (null != var1) {
			short[] var2 = new short[var1.length];
			Class967.arrayCopy(var1, 0, var2, 0, var1.length);
			return var2;
		} else {
			return null;
		}
	}

   static final void method66(Class1008 var0, int var1, int var2, int var4) {
	  // System.out.println("var0: " + var0 + ", var1: "+ var1 + ", var2: "+ var2 + ", var4: " + var4);
         Class1034 var5 = Class957.method638(var4, var1);
         if(null != var5) {
            if(var5.anObjectArray314 != null) {
               Class1048 var6 = new Class1048();
               var6.objectData = var5.anObjectArray314;
               var6.aClass11_2449 = var5;
               var6.aClass94_2439 = var0;
               var6.anInt2445 = var2;
               Class983.method1065(var6);
            }

            boolean var8 = true;
            if(0 < var5.clientCode) {
               var8 = Class3_Sub28_Sub19.method715(205, var5);
            }

            if(var8) {
               if(Class1034.getInterfaceClickMask(var5).method92(var2 - 1)) {
                  if(1 == var2) {
                     Class3_Sub13_Sub1.outputStream.putPacket(113);
                     Class3_Sub13_Sub1.outputStream.method_211(var4);
                     Class3_Sub13_Sub1.outputStream.method_0133(var1);
                  }

                     if(-3 == ~var2) {
                        Class3_Sub13_Sub1.outputStream.putPacket(37);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(~var2 == -4) {
                        Class3_Sub13_Sub1.outputStream.putPacket(134);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(var2 == 4) {
                        Class3_Sub13_Sub1.outputStream.putPacket(137);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(~var2 == -6) {
                        Class3_Sub13_Sub1.outputStream.putPacket(140);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(6 == var2) {
                        Class3_Sub13_Sub1.outputStream.putPacket(210);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(-8 == ~var2) {
                        Class3_Sub13_Sub1.outputStream.putPacket(148);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(-9 == ~var2) {
                        Class3_Sub13_Sub1.outputStream.putPacket(104);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(-10 == ~var2) {
                        Class3_Sub13_Sub1.outputStream.putPacket(9);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

                     if(~var2 == -11) {
                    	 //Examine item - on inter?
                        Class3_Sub13_Sub1.outputStream.putPacket(28);
                        Class3_Sub13_Sub1.outputStream.method_211(var4);
                        Class3_Sub13_Sub1.outputStream.method_0133(var1);
                     }

               }
            }
         }
   }

   public static void method67(boolean var0) {
      try {
         //aClass94_50 = null;
         aClass94Array45 = null;
         if(!var0) {
            aBoolean47 = true;
         }

         anIntArray49 = null;
         //aClass94_51 = null;
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "vg.B(" + var0 + ')');
      }
   }

   static final void method68(int var0, int var1, Class991 var2) {
      try {
         if(~Class1134.loopCycle <= ~var2.anInt2800) {
            if(var2.anInt2790 >= Class1134.loopCycle) {
               Class1223.method2270(var2, (byte)-56); //idk
            } else {
               Class973.method1180((byte)-22, var2); //movement anims
            }
         } else {
            Class1212.method1950(var2, true);//idk
         }
         

         if(-129 < ~var2.y || var2.x < 128 || var2.y >= 13184 || var2.x >= 13184) {
            var2.currentAnimationId = -1;
            var2.anInt2842 = -1;
            var2.anInt2800 = 0;
            var2.anInt2790 = 0;
            var2.y = 128 * var2.anIntArray2767[0] - -(64 * var2.getSize());
            var2.x = var2.anIntArray2755[0] * 128 + var2.getSize() * 64;
            var2.method1973(var1 + -2395);
         }

         if(var1 == 2279) {
            if(var2 == Class945.thisClass946 && (var2.y < 1536 || -1537 < ~var2.x || -11777 >= ~var2.y || var2.x >= 11776)) {
               var2.anInt2842 = -1;
               var2.anInt2800 = 0;
               var2.anInt2790 = 0;
               var2.currentAnimationId = -1;
               var2.y = var2.anIntArray2767[0] * 128 + var2.getSize() * 64;
               var2.x = 128 * var2.anIntArray2755[0] + 64 * var2.getSize();
               var2.method1973(-98);
            }

            Class17.method904(65536, var2);
            Class922.method900(var2, -11973);
         }
      } catch (RuntimeException var4) {
         throw Class1134.method1067(var4, "vg.C(" + var0 + ',' + var1 + ',' + (var2 != null?"{...}":"null") + ')');
      }
   }

}

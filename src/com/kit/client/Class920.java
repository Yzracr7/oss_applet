package com.kit.client;

import javax.media.opengl.GL;
import java.nio.IntBuffer;

final class Class920 {

    protected static Class1011 aClass3_Sub28_Sub16_Sub1_447 = null;
    protected static int startY = 0;
    protected static int startX = 0;
    protected static int endX = 0;
    protected static int endY = 0;

    static final void method921() {
        aClass3_Sub28_Sub16_Sub1_447 = null;
    }

    static final void method_433(int x, int y, int height, int color) {
        Class1012.init2dStuff();
        float var4 = (float) x + 0.3F;
        float var5 = var4 + (float) height;
        float var6 = (float) Class1012.canvasHeight - ((float) y + 0.3F);
        GL var7 = Class1012.gl;
        var7.glBegin(1);
        var7.glColor3ub((byte) (color >> 16), (byte) (color >> 8), (byte) color);
        var7.glVertex2f(var4, var6);
        var7.glVertex2f(var5, var6);
        var7.glEnd();
    }

    public static void method923() {
        aClass3_Sub28_Sub16_Sub1_447 = null;
    }

    static final void method_203(int var0, int var1, int var2, int var3) {
        Class1012.init2dStuff();
        float var4 = (float) var0 + 0.3F;
        float var5 = (float) Class1012.canvasHeight - ((float) var1 + 0.3F);
        float var6 = var5 - (float) var2;
        GL var7 = Class1012.gl;
        var7.glBegin(1);
        var7.glColor3ub((byte) (var3 >> 16), (byte) (var3 >> 8), (byte) var3);
        var7.glVertex2f(var4, var5);
        var7.glVertex2f(var4, var6);
        var7.glEnd();
    }

    static final void method_233() {
        startX = 0;
        startY = 0;
        endX = Class1012.canvasWidth;
        endY = Class1012.canvasHeight;
        GL var0 = Class1012.gl;
        var0.glDisable(3089);
        method921();
    }

    static final void method926(int[] var0, int var1, int var2, int var3, int var4) {
        Class1012.init2dStuff();
        GL var5 = Class1012.gl;
        var5.glRasterPos2i(var1, Class1012.canvasHeight - var2);
        var5.glPixelZoom(1.0F, -1.0F);
        var5.glDisable(3042);
        var5.glDisable(3008);
        var5.glDrawPixels(var3, var4, '\u80e1', Class1012.isBigEndianOrder ? '\u8367' : 5121, IntBuffer.wrap(var0));
        var5.glEnable(3008);
        var5.glEnable(3042);
        var5.glPixelZoom(1.0F, 1.0F);
    }

    static final void method_475(int var0, int var1, int var2, int var3, int var4) {
        Class1012.init2dStuff();
        float var5 = (float) var0 + 0.3F;
        float var6 = var5 + (float) (var2 - 1);
        float var7 = (float) Class1012.canvasHeight - ((float) var1 + 0.3F);
        float var8 = var7 - (float) (var3 - 1);
        GL var9 = Class1012.gl;
        var9.glBegin(2);
        var9.glColor3ub((byte) (var4 >> 16), (byte) (var4 >> 8), (byte) var4);
        var9.glVertex2f(var5, var7);
        var9.glVertex2f(var5, var8);
        var9.glVertex2f(var6, var8);
        var9.glVertex2f(var6, var7);
        var9.glEnd();
    }

    static final void method_321(int var0, int var1, int var2, int var3, int var4, int var5) {
        Class1012.init2dStuff();
        float var6 = (float) var0 + 0.3F;
        float var7 = var6 + (float) (var2 - 1);
        float var8 = (float) Class1012.canvasHeight - ((float) var1 + 0.3F);
        float var9 = var8 - (float) (var3 - 1);
        GL var10 = Class1012.gl;
        var10.glBegin(2);
        var10.glColor4ub((byte) (var4 >> 16), (byte) (var4 >> 8), (byte) var4, var5 > 255 ? -1 : (byte) var5);
        var10.glVertex2f(var6, var8);
        var10.glVertex2f(var6, var9);
        var10.glVertex2f(var7, var9);
        var10.glVertex2f(var7, var8);
        var10.glEnd();
    }

    static final void method_452(int x, int y, int width, int height, int color, int startAlpha, int lines) {
        Class1012.init2dStuff();
        float var6 = (float) x;
        float var7 = var6 + (float) width;
        float var8 = (float) (Class1012.canvasHeight - y);
        float var9 = var8 - (float) height;
        GL var10 = Class1012.gl;
        for (int i = lines; 0 < i; i--) {
            i = i - 3;
            var8 = (float) (Class1012.canvasHeight - (y + i));
            var9 = var8 - (float) height;
            var10.glBegin(6);
            var10.glColor4ub((byte) (color >> 16), (byte) (color >> 8), (byte) color, startAlpha > 255 ? -1 : (byte) (startAlpha + (i / 2)));
            var10.glVertex2f(var6, var8);
            var10.glVertex2f(var6, var9);
            var10.glVertex2f(var7, var9);
            var10.glVertex2f(var7, var8);
            var10.glEnd();
        }
    }

    static final void method_844(int var0, int var1, int var2, int var3, int var4, int var5) {
        int var6 = var2 - var0;
        int var7 = var3 - var1;
        int var8 = var6 >= 0 ? var6 : -var6;
        int var9 = var7 >= 0 ? var7 : -var7;
        int var10 = var8;
        if (var8 < var9) {
            var10 = var9;
        }

        if (var10 != 0) {
            int var11 = (var6 << 16) / var10;
            int var12 = (var7 << 16) / var10;
            if (var12 <= var11) {
                var11 = -var11;
            } else {
                var12 = -var12;
            }

            int var13 = var5 * var12 >> 17;
            int var14 = var5 * var12 + 1 >> 17;
            int var15 = var5 * var11 >> 17;
            int var16 = var5 * var11 + 1 >> 17;
            int var17 = var0 + var13;
            int var18 = var0 - var14;
            int var19 = var0 + var6 - var14;
            int var20 = var0 + var6 + var13;
            int var21 = var1 + var15;
            int var22 = var1 - var16;
            int var23 = var1 + var7 - var16;
            int var24 = var1 + var7 + var15;
            Class1012.init2dStuff();
            GL var25 = Class1012.gl;
            var25.glColor3ub((byte) (var4 >> 16), (byte) (var4 >> 8), (byte) var4);
            var25.glBegin(6);
            if (var12 <= var11) {
                var25.glVertex2f((float) var20, (float) (Class1012.canvasHeight - var24));
                var25.glVertex2f((float) var19, (float) (Class1012.canvasHeight - var23));
                var25.glVertex2f((float) var18, (float) (Class1012.canvasHeight - var22));
                var25.glVertex2f((float) var17, (float) (Class1012.canvasHeight - var21));
            } else {
                var25.glVertex2f((float) var17, (float) (Class1012.canvasHeight - var21));
                var25.glVertex2f((float) var18, (float) (Class1012.canvasHeight - var22));
                var25.glVertex2f((float) var19, (float) (Class1012.canvasHeight - var23));
                var25.glVertex2f((float) var20, (float) (Class1012.canvasHeight - var24));
            }

            var25.glEnd();
        }
    }

    static final void method_973(int var0, int var1, int var2, int var3, int var4, int var5) {
        Class1012.init2dStuff();
        float var6 = (float) var0;
        float var7 = var6 + (float) var2;
        float var8 = (float) (Class1012.canvasHeight - var1);
        float var9 = var8 - (float) var3;
        GL var10 = Class1012.gl;
        var10.glBegin(6);
        var10.glColor4ub((byte) (var4 >> 16), (byte) (var4 >> 8), (byte) var4, var5 > 255 ? -1 : (byte) var5);
        var10.glVertex2f(var6, var8);
        var10.glVertex2f(var6, var9);
        var10.glVertex2f(var7, var9);
        var10.glVertex2f(var7, var8);
        var10.glEnd();
    }

    static final void method_974(int var0, int var1, int var2, int var3) {
        if (startX < var0) {
            startX = var0;
        }
        if (startY < var1) {
            startY = var1;
        }
        if (endX > var2) {
            endX = var2;
        }
        if (endY > var3) {
            endY = var3;
        }
        GL var4 = Class1012.gl;
        var4.glEnable(3089);
        if (startX <= endX && startY <= endY) {
            var4.glScissor(startX, Class1012.canvasHeight - endY, endX - startX, endY - startY);
        } else {
            var4.glScissor(0, 0, 0, 0);
        }
        method921();
    }

    static final void resetPixels() {
        Class1012.gl.glClear(106640);
    }

    static final void method_844(int var0, int var1, int var2, int var3, int var4) {
        Class1012.init2dStuff();
        float var5 = (float) var0 + 0.3F;
        float var6 = (float) var2 + 0.3F;
        float var7 = (float) Class1012.canvasHeight - ((float) var1 + 0.3F);
        float var8 = (float) Class1012.canvasHeight - ((float) var3 + 0.3F);
        GL var9 = Class1012.gl;
        var9.glBegin(2);
        var9.glColor3ub((byte) (var4 >> 16), (byte) (var4 >> 8), (byte) var4);
        var9.glVertex2f(var5, var7);
        var9.glVertex2f(var6, var8);
        var9.glEnd();
    }

    static final void method_235(int x, int y, int width, int color, int alpha) {
        Class1012.init2dStuff();
        float x1 = (float) x + 0.3F;
        float x2 = (float) width + 0.3F;
        float y1 = (float) Class1012.canvasHeight - ((float) y + 0.3F);
        GL var9 = Class1012.gl;
        var9.glBegin(3);
        var9.glColor4ub((byte) (color >> 16), (byte) (color >> 8), (byte) color, alpha > 255 ? -1 : (byte) alpha);
        var9.glVertex2f(x1, y1);
        var9.glVertex2f(x2, y1);//y2);
        var9.glEnd();
    }

    protected static boolean debugged = false;

    static final void method_574(int var0, int var1, int var2, int var3, int var4) {
        Class1012.init2dStuff();
        float var5 = (float) var0;
        float var6 = var5 + (float) var2;
        float var7 = (float) (Class1012.canvasHeight - var1);
        float var8 = var7 - (float) var3;
        GL var9 = Class1012.gl;
        var9.glBegin(6);
        var9.glColor3ub((byte) (var4 >> 16), (byte) (var4 >> 8), (byte) var4);
        var9.glVertex2f(var5, var7);
        var9.glVertex2f(var5, var8);
        var9.glVertex2f(var6, var8);
        var9.glVertex2f(var6, var7);
        var9.glEnd();
    }

    static final void method_576(int var0, int var1, int var2, int var3) {
        if (var0 < 0) {
            var0 = 0;
        }
        if (var1 < 0) {
            var1 = 0;
        }
        if (var2 > Class1012.canvasWidth) {
            var2 = Class1012.canvasWidth;
        }
        if (var3 > Class1012.canvasHeight) {
            var3 = Class1012.canvasHeight;
        }
        startX = var0;
        startY = var1;
        endX = var2;
        endY = var3;
        GL var4 = Class1012.gl;
        var4.glEnable(3089);
        if (startX <= endX && startY <= endY) {
            var4.glScissor(startX, Class1012.canvasHeight - endY, endX - startX, endY - startY);
        } else {
            var4.glScissor(0, 0, 0, 0);
        }
        method921();
    }

    static final void method936(Class1011 var0) {
        if (var0.height != endY - startY) {
            throw new IllegalArgumentException();
        } else {
            aClass3_Sub28_Sub16_Sub1_447 = var0;
        }
    }
}

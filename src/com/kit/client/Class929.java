package com.kit.client;

public class Class929 {

    protected static int typeFace = 2; // 0 = small, 1 = regular, 2 = bold

    static boolean disable_gl_texture_binding = false;

    protected static boolean aBoolean_605 = false;
    protected static String aString1089 = aBoolean_605 ? "35.196.52.173" : "localhost";
    protected static String aString1088 = "Smite";
    protected static String aString1087 = "smite_cache";
    protected static String aString1086 = System.getProperty("user.home") + "/" + aString1087 + "/";
    protected static boolean aBoolean_607;
    protected static boolean aBoolean_608;

    protected static boolean newHits = false, newMenus = false, newHealthbars = false, newKeys = true, orbsToggled = false,
            middleMouse = true, hdMinimap = false, newCursors = false;

    protected static boolean aBoolean_606 = true;
    protected static int aInteger_510 = 464;
    protected static int aInteger_511 = 464;
    protected static int aInteger_509 = 9179409;

    protected static final Class1008[] aString1090 = new Class1008[]{Class943.create("::lo33wmemon"),
            Class943.create("::low22memoff"), Class943.create("::zoo333m0"), Class943.create("::zo33om1"),
            Class943.create("::zoo22m2"), Class943.create("::zoom223"), Class943.create("::full232screen"),
            Class943.create("::562frlllllllllame"), Class943.create("::530sdfsdfsdfframe"),
            Class943.create("::474sfsdfsdfframe"), Class943.create("::fullsc33reenon"), Class943.create("::fullscre33enoff"),
            Class943.create("::vot22e"), Class943.create("::h11s"), Class943.create("::hig3hsc44ores"),
            Class943.create("::hisc22ores"), Class943.create("::don33ate"), Class943.create("::for33um"),
            Class943.create("::for224ums"), Class943.create("::top44ic"), Class943.create("::threa22d"), Class943.create("::f334ov"),
            Class943.create("::textures"), Class943.create("::packsprit44e")};
}

package com.kit.client;

final class Class1225 extends Class956 {

    protected static Class93 npcDefinitions = new Class93(64);
    private Object value;
    protected static volatile int anInt4045 = 0;
    protected static Class1017_2 aClass130_4046 = new Class1017_2(16);
    protected static Class980 class980 = new Class980();
    protected static Class1027 aClass153_4048;
    protected static Class1008 aClass94_4049 = Class943.create("");
    protected static int[] anIntArray4050 = new int[1000];
    protected static Class93 aClass93_4051 = new Class93(30);

    static final void method569(int var0, int var1) {
        try {
            if (var0 < -78) {
                Class1042_3 var2 = Class3_Sub24_Sub3.putInterfaceChange(7, var1);
                var2.a();
            }
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "he.C(" + var0 + ',' + var1 + ')');
        }
    }

    final Object get() {
        return this.value;
    }

    public static void method570(int var0) {
        try {
            aClass94_4049 = null;
            anIntArray4050 = null;
            class980 = null;
            aClass130_4046 = null;
            npcDefinitions = null;
            aClass153_4048 = null;
            if (var0 > -101) {
                method570(-94);
            }
            aClass93_4051 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "he.D(" + var0 + ')');
        }
    }

    final boolean isSoftReference() {
        return false;
    }

    Class1225(Object var1) {
        this.value = var1;
    }

}

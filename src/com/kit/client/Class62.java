package com.kit.client;

final class Class62 {

   static int anInt942;
   int[][] groupFileNames;
   static int anInt944 = 0;
   int[] groupCrcs;
   byte[][][] groupFileBuffers;
   static Class1008 aClass94_946 = Class943.create(")2");
   private int groupLength;
   private static Class1008 aClass94_948 = Class943.create("You can(Wt add yourself to your own friend list)3");
   Class1043 groupClass1043;
   static int anInt950;
   static int anInt952;
   int[] groupIds;
   int[] groupFileAmount;
   int[] groupNames;
   int[] groupFileCount;
   static Class1008 aClass94_957 = aClass94_948;
   int[] groupVersions;
   int[][] groupFileIds;
   int anInt960;
   int rev;
   Class1043[] groupFileClass1043;
   static int anInt963;
   int anInt964;


   public static void method1223(int var0) {
      try {
         aClass94_946 = null;
         if(var0 == 0) {
            aClass94_948 = null;
            aClass94_957 = null;
         }
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "ii.A(" + var0 + ')');
      }
   }

   static final void method1224(Class1034 var0, int var2, int var3) {
	   //inter, height, width
	   
         if(0 != var0.aByte162) {
            if(var0.aByte162 != 1) {
               if(2 == var0.aByte162) {
                  var0.newScrollerPos = var2 - var0.scrollbarHeight - var0.y;
               } else if(var0.aByte162 != 3) {
                  if(4 == var0.aByte162) {
                     var0.newScrollerPos = (var2 * var0.y >> 14) + (-var0.scrollbarHeight + var2) / 2;
                  } else {
                     var0.newScrollerPos = -(var2 * var0.y >> 14) + -var0.scrollbarHeight + var2;
                  }
               } else {
                  var0.newScrollerPos = var0.y * var2 >> 14;
               }
            } else {
               var0.newScrollerPos = (var2 - var0.scrollbarHeight) / 2 + var0.y;
            }
         } else {
            var0.newScrollerPos = var0.y;
         }

         if(0 == var0.aByte273) {
            var0.anInt306 = var0.x;
         } else if(~var0.aByte273 != -2) {
            if(var0.aByte273 == 2) {
               var0.anInt306 = -var0.x + -var0.scrollbarWidth + var3;
            } else if(3 != var0.aByte273) {
               if(4 != var0.aByte273) {
                  var0.anInt306 = -(var3 * var0.x >> 14) + var3 + -var0.scrollbarWidth;
               } else {
                  var0.anInt306 = (var0.x * var3 >> 14) + (var3 - var0.scrollbarWidth) / 2;
               }
            } else {
               var0.anInt306 = var0.x * var3 >> 14;
            }
         } else {
            var0.anInt306 = var0.x + (var3 - var0.scrollbarWidth) / 2;
         }

         if(Class1043.qaoptestEnabled && (Class1034.getInterfaceClickMask(var0).clickMask != 0 || ~var0.type == -1)) {
            if(~var0.newScrollerPos > -1) {
               var0.newScrollerPos = 0;
            } else if(var0.scrollbarHeight + var0.newScrollerPos > var2) {
               var0.newScrollerPos = var2 + -var0.scrollbarHeight;
            }

            if(0 > var0.anInt306) {
               var0.anInt306 = 0;
            } else if(var3 < var0.anInt306 - -var0.scrollbarWidth) {
               var0.anInt306 = var3 + -var0.scrollbarWidth;
            }
         }

   }

	static final void method1225() {
		Class980 var1 = Class1225.class980;
		synchronized (var1) {
			Class3_Sub13_Sub5.anInt3069 = Class1211.anInt549;
			Class1015.mouseX2 = Class922.mouseX;
			Class1017_2.anInt1709 = Class922.mouseY;
			Class3_Sub28_Sub11.anInt3644 = Class140_Sub3.anInt2743;
			Class163_Sub1.clickX2 = Class922.anInt362;
			++Class1225.anInt4045;
			Class38_Sub1.clickY2 = Class3_Sub13_Sub32.anInt3389;
			Class75.aLong1102 = Class140_Sub6.clickTime;
			Class140_Sub3.anInt2743 = 0;
		}
	}

	private final void method1226(byte[] var2) {
		ByteBuffer class966 = new ByteBuffer(Class992.method623(var2));
		int protocol = class966.readUnsignedByte();
		if (protocol >= 6) {
			rev = class966.getInt();
		} else {
			rev = 0;
		}
		int groupNamed = class966.readUnsignedByte();
		int offset = 0;
		groupLength = protocol >= 7 ? class966.aMethod_593() : class966.aInteger233();
		int var7 = -1;
		groupIds = new int[groupLength];
		int var8;
		for (var8 = 0; groupLength > var8; ++var8) {
			groupIds[var8] = offset += protocol >= 7 ? class966.aMethod_593() : class966.aInteger233();
			if (groupIds[var8] > var7) {
				var7 = groupIds[var8];
			}
		}
		anInt960 = var7 + 1;
		groupVersions = new int[anInt960];
		groupFileIds = new int[anInt960][];
		groupCrcs = new int[anInt960];
		groupFileBuffers = new byte[anInt960][][];
		groupFileAmount = new int[anInt960];
		groupFileCount = new int[anInt960];
		if (groupNamed != 0) {
			groupNames = new int[anInt960];
			//for (int id = 0; id < anInt960; ++id) {
			//	groupNames[id] = -1;
			//}
			for (int id = 0; id < groupLength; ++id) {
				groupNames[groupIds[id]] = class966.getInt();
			}
			groupClass1043 = new Class1043(groupNames);
		}
		for (int id = 0; groupLength > id; ++id) {
			groupCrcs[groupIds[id]] = class966.getInt();
		}
		for (int id = 0; groupLength > id; ++id) {
			groupVersions[groupIds[id]] = class966.getInt();
		}
		for (int id = 0; groupLength > id; ++id) {
			groupFileCount[groupIds[id]] = protocol >= 7 ? class966.aMethod_593() : class966.aInteger233();
		}
		for (int groupIdx = 0; groupIdx < groupLength; ++groupIdx) {
			offset = 0;
			int groupId = groupIds[groupIdx];
			int fileCount = groupFileCount[groupId];
			int maxFileId = -1;
			groupFileIds[groupId] = new int[fileCount];
			for (int fileIdx = 0; fileCount > fileIdx; ++fileIdx) {
				int fileId = groupFileIds[groupId][fileIdx] = offset += protocol >= 7 ? class966.aMethod_593() : class966.aInteger233();
				if (fileId > maxFileId) {
					maxFileId = fileId;
				}
			}
			groupFileBuffers[groupId] = new byte[maxFileId + 1][];
			groupFileAmount[groupId] = maxFileId + 1;
			//if (~(1 + maxFileId) == ~fileCount) {
			//	groupFileIds[groupId] = null;
			//}
		}
		if (groupNamed != 0) {
			groupFileClass1043 = new Class1043[var7 + 1];
			groupFileNames = new int[1 + var7][];
			for (int groupIdx = 0; groupIdx < groupLength; ++groupIdx) {
				int groupId = groupIds[groupIdx];
				int fileCount = groupFileCount[groupId];
				groupFileNames[groupId] = new int[groupFileBuffers[groupId].length];
				// for(int fileIdx = 0; fileIdx < groupFileAmount[groupId];
				// ++fileIdx) {
				// groupFileNames[groupId][fileIdx] = -1;
				// }
				for (int fileIdx = 0; fileIdx < fileCount; ++fileIdx) {
					int fileId;
					if (null != groupFileIds[groupId]) {
						fileId = groupFileIds[groupId][fileIdx];
					} else {
						fileId = fileIdx;
					}
					groupFileNames[groupId][fileId] = class966.getInt();
				}
				groupFileClass1043[groupId] = new Class1043(groupFileNames[groupId]);
			}
		}
	}

	Class62(byte[] var1, int var2) {
		anInt964 = Class38.method1026(var1, var1.length, false);
		if (var2 == anInt964) {
			method1226(var1);
		} else {
			throw new RuntimeException("RTE");
		}
	}

}

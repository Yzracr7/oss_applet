package com.kit.client;

import java.awt.*;
import java.awt.image.*;
import java.util.Hashtable;

public final class Class931 extends Class923 {

    private Component component;

    final void drawGraphics(Graphics graphics, int x, int y) {
        graphics.drawImage(this.image, x, y, this.component);
    }

    final void init(Component component, int height, int width, boolean isRasterPremultiplied) {
        this.pixels = new int[height * width + 1];
        this.height = width;
        this.width = height;
        DataBufferInt databufferint = new DataBufferInt(this.pixels, this.pixels.length);
        DirectColorModel directcolormodel = new DirectColorModel(32, 16711680, '\uff00', 255);
        WritableRaster writableraster = Raster.createWritableRaster(directcolormodel.createCompatibleSampleModel(this.width, this.height), databufferint, (Point) null);
        this.image = new BufferedImage(directcolormodel, writableraster, isRasterPremultiplied, new Hashtable());
        this.component = component;
        this.initCanvas();
    }

    final void clip(Graphics graphics, int x, int y, int w, int h) {
        Shape shape = graphics.getClip();
        graphics.clipRect(x, y, w, h);
        graphics.drawImage(this.image, 0, 0, this.component);
        graphics.setClip(shape);
    }
}

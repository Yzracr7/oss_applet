package com.kit.client;

import java.io.*;
import java.util.Hashtable;


/**
 * The settings class will manage all settings stored by the client. The client
 * will refer to, change, and remove settings as it needs to. Adding buffer new client
 * setting is extremely easy and changing them is even easier due to the fact that
 * the Class944 class will remove old or nulled settings still stored in the settings
 * file, and it verifies each setting to make sure it's buffer legitimate setting (to check
 * if you still require the usage of that setting).
 *
 * @author Galkon
 */
public class Class944 {

    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();

        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            is.close();
            throw new IOException("Could not completely read file "
                    + file.getName());
        }

        is.close();
        return bytes;
    }


    /**
     * The settings stored created and stored by the client.
     */
    public static Hashtable<String, Object> settings = new Hashtable<String, Object>();


    /**
     * The values of the settings.
     */

    public static Object[][] settingValues = {
            {"client_load", 474}, {"hd_enabled", false}, {"tweening_enabled", false},
            {"hitmarkers", /*634*/-1}, {"gameframe", 474/*562*/}, {"new_health_bars", false},
            {"orbs_toggled", true}, {"new_hits", false}, {"new_menus", false},
            {"hd_minimap", false}, {"new_cursors", false}, {"fullscreen", false},
            {"censor", false}, {"lowmem", false}, {"start_size", 0}, {"left_click_attack", true},
    };

    /**
     * Creates buffer setting for the specified integer_34 and object.
     *
     * @param name
     * @param object
     */
    public static void createSetting(String name, Object object) {
        if (!verify(name)) {
            return;
        }
        settings.put(name, object);
    }


    /**
     * Returns the object for the specified integer_34.
     *
     * @param name
     * @return
     */
    public static Object getSetting(String name) {
        if (!verify(name)) {
            return null;
        }
        return settings.get(name);
    }


    /**
     * Changes the specified setting by removing it then re-adding the new values.
     *
     * @param name
     * @param object
     */
    public static void changeSetting(String name, Object object) {
        if (!verify(name)) {
            return;
        }
        settings.remove(name);
        settings.put(name, object);
        save();
    }


    /**
     * Toggles the specified setting (only works for booleans).
     *
     * @param name
     */
    public static void toggleSetting(String name) {
        if (settings.get(name) instanceof Boolean) {
            changeSetting(name, !(Boolean) settings.get(name));
        }
    }


    /**
     * Verifies that the specified integer_34 is buffer defined setting.
     *
     * @param name
     * @return
     */
    public static boolean verify(String name) {
        for (int index = 0; index < settingValues.length; index++) {
            String nameValue = (String) settingValues[index][0];
            if (nameValue.equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Creates the default settings.
     */
    public static void createDefaults() {
        for (int index = 0; index < settingValues.length; index++) {
            createSetting((String) settingValues[index][0], settingValues[index][1]);
        }
        save();
    }


    /**
     * Loads and creates the settings from the settings file.
     */
    public static void load() {
        try {
            if (!new File(getWorkingDirectory() + FILE).exists()) {
                createDefaults();
                System.out.println("Created default settings.");
                return;
            }
            RandomAccessFile in = new RandomAccessFile(getWorkingDirectory() + FILE, "rw");
            int size = in.readShort();
            if (size != settingValues.length) {
                createDefaults();
            }
            for (int index = 0; index < size; index++) {
                String name = in.readUTF();
                Object object = null;
                int type = in.readByte();
                if (type == 0) {
                    object = in.readBoolean();
                } else if (type == 1) {
                    object = in.readInt();
                } else if (type == 2) {
                    object = in.readUTF();
                }
                if (size == settingValues.length) {
                    createSetting(name, object);
                } else {
                    changeSetting(name, object);
                }
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Saves the settings to the settings file.
     */
    public static void save() {
        try {
            //	try {
            RandomAccessFile out = new RandomAccessFile(getWorkingDirectory() + FILE, "rw");
            // 	} catch (FileNotFoundException f){
            // 		System.out.println("Class944 file not found.");
            // 	}
            out.writeShort(settings.size());
            for (int index = 0; index < settingValues.length; index++) {
                String name = (String) settingValues[index][0];
                Object object = settings.get(name);
                if (object == null) {
                    object = settingValues[index][1];
                }
                out.writeUTF(name);
                if (object instanceof Boolean) {
                    out.writeByte(0);
                    out.writeBoolean((Boolean) object);
                } else if (object instanceof Integer) {
                    out.writeByte(1);
                    out.writeInt((Integer) object);
                } else if (object instanceof String) {
                    out.writeByte(2);
                    out.writeUTF((String) object);
                }
            }
            out.close();
            // System.out.println("Class944 saved.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * The settings file integer_34.
     */
    public final static String FILE = "settings.txt";


    public final static String getWorkingDirectory() {
        return Class942.getCacheDir() + "/";
    }
}
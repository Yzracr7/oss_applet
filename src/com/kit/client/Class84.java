package com.kit.client;

public final class Class84 {

   static int[][] anIntArrayArray1160 = new int[104][104];
   static int[] anIntArray1161;
  // static Class975 aClass61_1162 = new Class975();
   static int[] anIntArray1163 = new int[1000];
   public static int canvasDrawX = 0;
   static int anInt1165 = -1;


	static final void method1417() {
		if (~Class922.aInteger_544 == -11 && Class1012.aBoolean_617) {
			Class922.setaInteger_544(25);
		}

		if (~Class922.aInteger_544 == -31) {
			Class922.setaInteger_544(25);
		}
	}

   /*static final void method1418(Class1027 var1) {
         Class163_Sub2_Sub1.aClass109_Sub1Array4027 = Class992.method619((byte)65, Class1001.anInt4001, var1);
         Class1098.anIntArray861 = new int[256];

         int var2;
         for(var2 = 0; -4 < ~var2; ++var2) {
            int var4 = (Class131.anIntArray1729[1 + var2] & 16711680) >> 16;
            float var3 = (float)((Class131.anIntArray1729[var2] & 16711680) >> 16);
            float var6 = (float)(Class131.anIntArray1729[var2] >> 8 & 255);
            float var9 = (float)(Class131.anIntArray1729[var2] & 255);
            float var5 = ((float)var4 - var3) / 64.0F;
            int var7 = (Class131.anIntArray1729[var2 + 1] & '\uff00') >> 8;
            float var8 = (-var6 + (float)var7) / 64.0F;
            int var10 = Class131.anIntArray1729[var2 + 1] & 255;
            float var11 = ((float)var10 - var9) / 64.0F;

            for(int var12 = 0; -65 < ~var12; ++var12) {
               Class1098.anIntArray861[var12 + 64 * var2] = Class3_Sub13_Sub29.method308((int)var9, Class3_Sub13_Sub29.method308((int)var6 << 8, (int)var3 << 16));
               var6 += var8;
               var9 += var11;
               var3 += var5;
            }
         }

         for(var2 = 192; var2 < 255; ++var2) {
            Class1098.anIntArray861[var2] = Class131.anIntArray1729[3];
         }

         Class161.anIntArray2026 = new int['\u8000'];
         Class986.anIntArray49 = new int['\u8000'];
         Class3_Sub13_Sub10.method215((byte)-89, (LDIndexedSprite)null);
         Class966_2.anIntArray3805 = new int['\u8000'];
         Class1126.anIntArray1681 = new int['\u8000'];
         Class97.aClass3_Sub28_Sub16_Sub2_1381 = new Class1206_2(128, 254);
   }*/

   static final void drawContextMenu(int var0) {
	   if (!Class929.newMenus) {
		   int var2 = Class3_Sub13_Sub33.anInt3395;
	         int var3 = Class3_Sub28_Sub3.anInt3552;
	         int var1 = Class927.anInt1462;
	         int var5 = 6116423;
	         int var4 = Class3_Sub28_Sub1.anInt3537;
	         if(!Class1012.aBoolean_617) {
	            Class1023.fillRect(var1, var2, var3, var4, var5);
	            Class1023.fillRect(1 + var1, 1 + var2, var3 + -2, 16, 0);
	            Class1023.drawRect(1 + var1, var2 + 18, -2 + var3, -19 + var4, 0);
	         } else {
	            Class920.method_574(var1, var2, var3, var4, var5);
	            Class920.method_574(1 + var1, 1 + var2, var3 + -2, 16, 0);
	            Class920.method_475(1 + var1, var2 + 18, var3 + -2, -19 + var4, 0);
	         }
	         Class922.getBoldClass1019().method681(Class75_Sub4.aClass94_2667, var1 - -3, var2 + 14, var5, -1);
	         int var7 = Class1017_2.anInt1709;
	         int var6 = Class1015.mouseX2;
	         if(var0 >= -113) {
	            anIntArrayArray1160 = (int[][])((int[][])null);
	         }
	         Class1008 selectedAction = null;
	         for(int var8 = 0; ~Class3_Sub13_Sub34.contextOptionsAmount < ~var8; ++var8) {
	            int var9 = (-var8 + -1 + Class3_Sub13_Sub34.contextOptionsAmount) * 15 + var2 - -31;
	            int var10 = 16777215;
	            if(~var1 > ~var6 && ~var6 > ~(var1 - -var3) && -13 + var9 < var7 && ~var7 > ~(3 + var9)) {
	               var10 = 16776960;
	            }
	            Class1008 action = ByteBuffer.getContextOption(var8, true);
	            if(var10 == 16776960)
	            	selectedAction = action;
	            Class922.getBoldClass1019().method681(action, var1 - -3, var9, var10, 0);
	         }
	         if(selectedAction != null) {
	        		if(selectedAction.toString() != "") {
	        			Class922.detectCursor(selectedAction.toString());
	        		}
	       	// System.out.println("selectedAction: " + selectedAction.toString());
	         }
	         Class1043.method1282(Class927.anInt1462, Class3_Sub13_Sub33.anInt3395, Class3_Sub28_Sub1.anInt3537, Class3_Sub28_Sub3.anInt3552);
	   } else {
		   int menuWidth = Class3_Sub28_Sub3.anInt3552;
		   	 int menuOffsetY = Class3_Sub13_Sub33.anInt3395;
		     int menuOffsetX = Class927.anInt1462;
	         int color = 6116423;
	         int menuHeight = Class3_Sub28_Sub1.anInt3537;
	 		 int yPos = menuOffsetY, xPos = menuOffsetX;
	 		 if(!Class1012.aBoolean_617) {
	 			Class1023.fillRect(xPos + 2, yPos, menuWidth - 4, 1, 0x6d6a5b);
				Class1023.fillRect(xPos + 1, yPos + 1, menuWidth - 2, 1, 0x6d6a5b);
				Class1023.fillRect(xPos, yPos + 2, menuWidth, menuHeight - 2, 0x6d6a5b);
				Class1023.fillRect(xPos + 1, yPos + 3, menuWidth - 2, menuHeight - 4, 0x2b2622);
				Class1023.fillRect(xPos + 3, yPos + 1, menuWidth - 6, 1, 0x322e22);
				Class1023.fillRect(xPos + 3, yPos + 2, menuWidth - 6, 1, 0x2a291b);
				Class1023.fillRect(xPos + 3, yPos + 3, menuWidth - 6, 1, 0x2a261b);
				Class1023.fillRect(xPos + 3, yPos + 4, menuWidth - 6, 1, 0x252116);
				Class1023.fillRect(xPos + 2, yPos + 5, menuWidth - 4, 1, 0x211e15);
				Class1023.fillRect(xPos + 2, yPos + 6, menuWidth - 4, 1, 0x1e1b12);
				Class1023.fillRect(xPos + 2, yPos + 7, menuWidth - 4, 1, 0x1a170e);
				Class1023.fillRect(xPos + 2, yPos + 8, menuWidth - 4, 1, 0x15120b);
				Class1023.fillRect(xPos + 2, yPos + 9, menuWidth - 4, 1, 0x15120b);
				Class1023.fillRect(xPos + 2, yPos + 10, menuWidth - 4, 1, 0x100d08);
				Class1023.fillRect(xPos + 2, yPos + 11, menuWidth - 4, 1, 0x090a04);
				Class1023.fillRect(xPos + 2, yPos + 12, menuWidth - 4, 1, 0x080703);
				Class1023.fillRect(xPos + 2, yPos + 13, menuWidth - 4, 1, 0x090a04);
				Class1023.fillRect(xPos + 2, yPos + 14, menuWidth - 4, 1, 0x070802);
				Class1023.fillRect(xPos + 2, yPos + 15, menuWidth - 4, 1, 0x090a04);
				Class1023.fillRect(xPos + 2, yPos + 16, menuWidth - 4, 1, 0x070802);
				Class1023.fillRect(xPos + 2, yPos + 17, menuWidth - 4, 1, 0x090a04);
				Class1023.fillRect(xPos + 2, yPos + 1, 1, 1, 0x2b271c);
				Class1023.fillRect(xPos + 1, yPos + 2, 2, 1, 0x2b271c);
				Class1023.fillRect(xPos + menuWidth - 3, yPos + 1, 1, 1, 0x2b271c);
				Class1023.fillRect(xPos + menuWidth - 3, yPos + 2, 2, 1, 0x2b271c);
				Class1023.fillRect(xPos + 2, yPos + 19, menuWidth - 4, menuHeight - 21, 0x524a3d);
				Class1023.fillRect(xPos + 3, yPos + 20, menuWidth - 6, menuHeight - 23, 0x2b271c);
				Class1023.fillRect(xPos, yPos + menuHeight - 1, 1, 1, 0x2d2822);
				Class1023.fillRect(xPos, yPos + menuHeight - 2, 1, 1, 0x524a3d);
				Class1023.fillRect(xPos + 1, yPos + menuHeight - 1, 1, 1, 0x524a3d);
				Class1023.fillRect(xPos + 1, yPos + menuHeight - 3, 1, 1, 0x6e675f);
				Class1023.fillRect(xPos + 1, yPos + menuHeight - 2, 2, 1, 0x6e675f);
				Class1023.fillRect(xPos + 2, yPos + menuHeight - 3, 1, 1, 0x2e2a1f);
				Class1023.fillRect(xPos + menuWidth - 1, yPos + menuHeight - 1, 1, 1, 0x2d2822);
				Class1023.fillRect(xPos + menuWidth - 1, yPos + menuHeight - 2, 1, 1, 0x524a3d);
				Class1023.fillRect(xPos + menuWidth - 2, yPos + menuHeight - 1, 1, 1, 0x524a3d);
				Class1023.fillRect(xPos + menuWidth - 2, yPos + menuHeight - 3, 1, 1, 0x6e675f);
				Class1023.fillRect(xPos + menuWidth - 3, yPos + menuHeight - 2, 2, 1, 0x6e675f);
				Class1023.fillRect(xPos + menuWidth - 3, yPos + menuHeight - 3, 1, 1, 0x2e2a1f);
	 		 } else {
		 			Class920.method_574(xPos + 2, yPos, menuWidth - 4, 1, 0x6d6a5b);
					Class920.method_574(xPos + 1, yPos + 1, menuWidth - 2, 1, 0x6d6a5b);
					Class920.method_574(xPos, yPos + 2, menuWidth, menuHeight - 2, 0x6d6a5b);
					Class920.method_574(xPos + 1, yPos + 3, menuWidth - 2, menuHeight - 4, 0x2b2622);
					Class920.method_574(xPos + 3, yPos + 1, menuWidth - 6, 1, 0x322e22);
					Class920.method_574(xPos + 3, yPos + 2, menuWidth - 6, 1, 0x2a291b);
					Class920.method_574(xPos + 3, yPos + 3, menuWidth - 6, 1, 0x2a261b);
					Class920.method_574(xPos + 3, yPos + 4, menuWidth - 6, 1, 0x252116);
					Class920.method_574(xPos + 2, yPos + 5, menuWidth - 4, 1, 0x211e15);
					Class920.method_574(xPos + 2, yPos + 6, menuWidth - 4, 1, 0x1e1b12);
					Class920.method_574(xPos + 2, yPos + 7, menuWidth - 4, 1, 0x1a170e);
					Class920.method_574(xPos + 2, yPos + 8, menuWidth - 4, 1, 0x15120b);
					Class920.method_574(xPos + 2, yPos + 9, menuWidth - 4, 1, 0x15120b);
					Class920.method_574(xPos + 2, yPos + 10, menuWidth - 4, 1, 0x100d08);
					Class920.method_574(xPos + 2, yPos + 11, menuWidth - 4, 1, 0x090a04);
					Class920.method_574(xPos + 2, yPos + 12, menuWidth - 4, 1, 0x080703);
					Class920.method_574(xPos + 2, yPos + 13, menuWidth - 4, 1, 0x090a04);
					Class920.method_574(xPos + 2, yPos + 14, menuWidth - 4, 1, 0x070802);
					Class920.method_574(xPos + 2, yPos + 15, menuWidth - 4, 1, 0x090a04);
					Class920.method_574(xPos + 2, yPos + 16, menuWidth - 4, 1, 0x070802);
					Class920.method_574(xPos + 2, yPos + 17, menuWidth - 4, 1, 0x090a04);
					Class920.method_574(xPos + 2, yPos + 1, 1, 1, 0x2b271c);
					Class920.method_574(xPos + 1, yPos + 2, 2, 1, 0x2b271c);
					Class920.method_574(xPos + menuWidth - 3, yPos + 1, 1, 1, 0x2b271c);
					Class920.method_574(xPos + menuWidth - 3, yPos + 2, 2, 1, 0x2b271c);
					Class920.method_574(xPos + 2, yPos + 19, menuWidth - 4, menuHeight - 21, 0x524a3d);
					Class920.method_574(xPos + 3, yPos + 20, menuWidth - 6, menuHeight - 23, 0x2b271c);
					Class920.method_574(xPos, yPos + menuHeight - 1, 1, 1, 0x2d2822);
					Class920.method_574(xPos, yPos + menuHeight - 2, 1, 1, 0x524a3d);
					Class920.method_574(xPos + 1, yPos + menuHeight - 1, 1, 1, 0x524a3d);
					Class920.method_574(xPos + 1, yPos + menuHeight - 3, 1, 1, 0x6e675f);
					Class920.method_574(xPos + 1, yPos + menuHeight - 2, 2, 1, 0x6e675f);
					Class920.method_574(xPos + 2, yPos + menuHeight - 3, 1, 1, 0x2e2a1f);
					Class920.method_574(xPos + menuWidth - 1, yPos + menuHeight - 1, 1, 1, 0x2d2822);
					Class920.method_574(xPos + menuWidth - 1, yPos + menuHeight - 2, 1, 1, 0x524a3d);
					Class920.method_574(xPos + menuWidth - 2, yPos + menuHeight - 1, 1, 1, 0x524a3d);
					Class920.method_574(xPos + menuWidth - 2, yPos + menuHeight - 3, 1, 1, 0x6e675f);
					Class920.method_574(xPos + menuWidth - 3, yPos + menuHeight - 2, 2, 1, 0x6e675f);
					Class920.method_574(xPos + menuWidth - 3, yPos + menuHeight - 3, 1, 1, 0x2e2a1f);
	 		 }
	 		 //Choose option text
	 		Class922.getBoldClass1019().method681(Class75_Sub4.aClass94_2667,  3 + menuOffsetX, menuOffsetY - -14, color, -1);
	         int var7 = Class1017_2.anInt1709;
	         int var6 = Class1015.mouseX2;
	         //Loop through actions and draw rectangles

	            Class1008 selectedAction = null;
	         for(int var8 = 0; ~Class3_Sub13_Sub34.contextOptionsAmount < ~var8; ++var8) {
	        	int var9 = (-var8 + -1 + Class3_Sub13_Sub34.contextOptionsAmount) * 15 + menuOffsetY - -31;
	            int var10 = 0xc6b895;
	            if(~menuOffsetX > ~var6 && ~var6 > ~(menuOffsetX - -menuWidth) && -13 + var9 < var7 && ~var7 > ~(3 + var9)) {
	            	if(!Class1012.aBoolean_617) {
	            		if (ByteBuffer.getContextOption(var8, true).method102(Class943.create("Cancel"))) {
	            			Class1023.fillRect(xPos + 3, var9 - 10, menuWidth - 6, 12, 0x6f695d);
	            		} else {
	            			Class1023.fillRect(xPos + 3, var9 - 11, menuWidth - 6, 14, 0x6f695d);
	            		}
	            	} else {
	 	               if (ByteBuffer.getContextOption(var8, true).method102(Class943.create("Cancel"))) {
	 	            	   	Class920.method_574(xPos + 3, var9 - 10, menuWidth - 6, 12, 0x6f695d);
		        		} else {
		        			Class920.method_574(xPos + 3, var9 - 11, menuWidth - 6, 14, 0x6f695d);
		        		}
	            	}
	            	var10 = 0xeee5c6;
	            }
	            Class1008 action = ByteBuffer.getContextOption(var8, true);
		           // System.out.println("action: " + action + " var10: "+ var10);
		            if(var10 == 15656390)
		            	selectedAction = action;
	            Class922.getBoldClass1019().method681(action, menuOffsetX + 3, var9, var10, 0);
	         }
	         if(selectedAction != null) {
	        		if(selectedAction.toString() != "") {
	        			Class922.detectCursor(selectedAction.toString());
	        		}
	        	// System.out.println("selectedAction: " + selectedAction.toString());
	         }
	         Class1043.method1282(Class927.anInt1462, Class3_Sub13_Sub33.anInt3395, Class3_Sub28_Sub1.anInt3537, Class3_Sub28_Sub3.anInt3552);
	   }
   }

   static final void method1420(int var0, int var1, int var2, int var3) {
		Class1042_3 var5 = Class3_Sub24_Sub3.putInterfaceChange(10, var0);
		var5.add();
		var5.intValue2 = var2;
		var5.intValue = var3;
		var5.intValue3 = var1;
   }

	static final int method1421() {
		return ((Class3_Sub13_Sub15.isStereo ? 1 : 0) << 19) + (((Class38.aBoolean661 ? 1 : 0) << 16) + ((!Class128.aBoolean1685 ? 0 : 1) << 15) + ((!Class989.aBoolean1441 ? 0 : 1) << 13) + ((Class140_Sub6.aBoolean2910 ? 1 : 0) << 10) + ((Class120_Sub30_Sub1.manyGroundTextures ? 1 : 0) << 9) + ((Class1034.manyIdleAnimations ? 1 : 0) << 7) + ((!Class25.lowMemoryTextures ? 0 : 1) << 6) + ((Class1005.showGroundDecorations ? 1 : 0) << 5) + (((!Class992.visibleLevels ? 0 : 1) << 3) + (Class3_Sub28_Sub10.brightness & 7) - (-((!Class956.removeRoofs ? 0 : 1) << 4) + -((Class1228.highDetailLights ? 1 : 0) << 8)) - (-(Class80.anInt1137 << 11 & 6144) + -((-1 == ~Class1048.soundEffectsVolume ? 0 : 1) << 20) - (((~Class9.musicVolume != -1 ? 1 : 0) << 21) + ((~Class14.areaSoundsVolume == -1 ? 0 : 1) << 22)))) - -(Class1218.method1757() << 23));
	}

   public static void method1422(byte var0) {
      try {
         anIntArrayArray1160 = (int[][])null;
         //anIntArray1161 = null;
         //aClass61_1162 = null;
         anIntArray1163 = null;
         if(var0 != 24) {
            method1420(-74, 65, 51, 91);
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "lf.B(" + var0 + ')');
      }
   }

}

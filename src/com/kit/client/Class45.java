package com.kit.client;

final class Class45 {

   static int[] anIntArray729 = new int[4096];
   static float aFloat730;
   static Class1027 aClass153_731;
   static boolean aBoolean732 = false;
   static int mapFlagY = 0;
   static int tradeChatStatus = 0;
   static int anInt735;
   static Class957[] mapmarkerSprites;


   static final void method1080(int var0, int var1, byte var2, Class946 var3) {
         int var4;
         int var5;
         int var7;

         if((var0 & 0x100) != 0) {
            var4 = Class1211.incomingPackets.aInt122();
            var5 = Class1211.incomingPackets.aInt122();
            int icon = Class1211.incomingPackets.aInt2016();
            int focus = Class1211.incomingPackets.aInt2016();
            var3.updateHitData(var5, -8, Class1134.loopCycle, var4, icon, focus);
            var3.anInt2781 = 300 + Class1134.loopCycle;
            int currentHp = Class1211.incomingPackets.aInt2016();
            int maxHp = Class1211.incomingPackets.aInt2016();
            var3.hpRatio = (currentHp * 255) / maxHp;
         }
         
         if((0x200 & var0) != 0) {
             var4 = Class1211.incomingPackets.aInteger233();
             if(var4 == '\uffff') {
                var4 = -1;
             }

             var5 = Class1211.incomingPackets.method_152();
             boolean var21 = true;
             if(~var4 != 0 && 0 != ~var3.anInt2842 && Class954.list(Class1211.list(var4).anInt542).anInt1857 < Class954.list(Class1211.list(var3.anInt2842).anInt542).anInt1857) {
                var21 = false;
             }

             if(var21) {
                var3.anInt2759 = (var5 & '\uffff') + Class1134.loopCycle;
                var3.anInt2761 = 0;
                var3.anInt2805 = 0;
                var3.anInt2842 = var4;
                if(~var3.anInt2759 < ~Class1134.loopCycle) {
                   var3.anInt2805 = -1;
                }

                var3.anInt2799 = var5 >> 16;
                var3.anInt2826 = 1;
                if(~var3.anInt2842 != 0 && Class1134.loopCycle == var3.anInt2759) {
                   var7 = Class1211.list(var3.anInt2842).anInt542;
                   if(0 != ~var7) {
                      Class954 var24 = Class954.list(var7);
                      if(null != var24 && var24.frames != null) {
                         Class1007.method1470(var3.x, var24, 183921384, var3.y, var3 == Class945.thisClass946, 0);
                      }
                   }
                }
             }
          }
         
         if((0x8 & var0) != 0) {
             var3.anInt2772 = Class1211.incomingPackets.aLong_011();
             if(-65536 == ~var3.anInt2772) {
                var3.anInt2772 = -1;
             }
          }
         

         if((var0 & 0x4) != 0) {
        	 var3.anInt2786 = Class1211.incomingPackets.aBoole100();
        	 var3.anInt2762 = Class1211.incomingPackets.aBoole100();
         }
         

         if((0x400 & var0) != 0) {
            var3.anInt2784 = Class1211.incomingPackets.aInt122();
            var3.anInt2835 = Class1211.incomingPackets.aInt122();
            var3.anInt2823 = Class1211.incomingPackets.aInt122();
            var3.anInt2798 = Class1211.incomingPackets.aInt2016();
            var3.anInt2800 = Class1211.incomingPackets.aMethod10() + Class1134.loopCycle;
            var3.anInt2790 = Class1211.incomingPackets.aInteger233() - -Class1134.loopCycle;
            var3.anInt2840 = Class1211.incomingPackets.gea100();
            var3.walkQueueLocationIndex = 1;
            var3.anInt2811 = 0;
         }
         
         if((var0 & 0x80) != 0) {
             var3.aClass94_2825 = Class1211.incomingPackets.class_91033();
             if(~var3.aClass94_2825.method1569(0, (byte)-45) == -127) {
                var3.aClass94_2825 = var3.aClass94_2825.method1556(1, (byte)-74);
                Class966_2.sendMessage(var3.method1980(0), var3.aClass94_2825, 2);
             } else if(var3 == Class945.thisClass946) {
                Class966_2.sendMessage(var3.method1980(0), var3.aClass94_2825, 2);
             }

             var3.anInt2753 = 0;
             var3.anInt2837 = 0;
             var3.anInt2814 = 150;
          }

         if((var0 & 0x20) != 0) {
            var4 = Class1211.incomingPackets.aLong_011();
            if(-65536 == ~var4) {
               var4 = -1;
            }

            var5 = Class1211.incomingPackets.gea100();
            Class921.method628(0, var5, var4, var3);
         }
         
         if(0 != (var0 & 0x1)) {
             var4 = Class1211.incomingPackets.aBoole100();
             var5 = Class1211.incomingPackets.aInt2016();
             int var6 = Class1211.incomingPackets.readUnsignedByte();
             var7 = Class1211.incomingPackets.offset;
             if(null != var3.username && var3.class1098 != null) {
                long var9 = var3.username.toLong();
                boolean var11 = false;
                if(var5 <= 1) {
                   if((Class3_Sub15.aBoolean2433 && !Class121.aBoolean1641 || Class3_Sub13_Sub14.aBoolean3166)) {
                      var11 = true;
                   } else {
                      for(int var12 = 0; var12 < Class955.ignoreListCount; ++var12) {
                         if(Class114.ignoreList[var12] == var9) {
                            var11 = true;
                            break;
                         }
                      }
                   }
                }

                if(!var11 && 0 == Class1228.anInt2622) {
                   Class161.aClass3_Sub30_2030.offset = 0;
                   Class1211.incomingPackets.method774(2, var6, Class161.aClass3_Sub30_2030.buffer, 0);
                   Class161.aClass3_Sub30_2030.offset = 0;
                   int var13 = -1;
                   Class1008 var25;
                   var25 = Class1019.method686(Class32.method992(Class161.aClass3_Sub30_2030).method1536(78));

                   var3.aClass94_2825 = var25.method1564();
                   var3.anInt2753 = var4 & 255;
                   var3.anInt2814 = 150;
                   var3.anInt2837 = var4 >> 8;
                      
                   if(-3 != ~var5) {
                      if(~var5 != -2) {
                    	  
                    	  
                    	  //All non-staff
                    	  if(!var3.rankHidden) {
	                    	  if(var5 == 3)
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.pkerImg, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
	                    	 else if (var5 == 4) //donator
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.donatorImg, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
	                    	 else if (var5 == 5) //edonator
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.extremeDonatorImg, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
	                    	 else if (var5 == 6) //support
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.supportImg, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
	                    	 else if (var5 == 7) //super
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.superDonatorImg, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
	                    	 else if (var5 == 8) //legendary
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.legendaryDonatorImg, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
	                    	 else //normal player
	                    		 Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
                    	  } else {
                    		  	Class1143.appendChatMessage(2, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
                    	  }
                    	  
                    	  } else { //PMOD
                         Class1143.appendChatMessage(1, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class32.aClass94_592, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(0)}));
                      }
                   } else { //JMOD
                      Class1143.appendChatMessage(1, var25, (Class1008)null, Class922.combinejStrings(new Class1008[]{Class21.aClass94_444, var3.title.toString().length() > 0 ? var3.title : Class922.BLANK_CLASS_1008, var3.method1980(var2 + 79)}));
                   }
                }
             }

             Class1211.incomingPackets.offset = var7 + var6;
          }

         if((var0 & 0x2) != 0) {
            var4 = Class1211.incomingPackets.aInt2016();
            var5 = Class1211.incomingPackets.aInt122();
            int icon = Class1211.incomingPackets.aInt2016();
            int focus = Class1211.incomingPackets.aInt2016();
            var3.updateHitData(var5, var2 + 71, Class1134.loopCycle, var4, icon, focus);
            //(entity.currentHealth * 30) / entity.maxHealth;
            var3.anInt2781 = 300 + Class1134.loopCycle;
            int currentHp = Class1211.incomingPackets.aInt2016();
            int maxHp = Class1211.incomingPackets.aInt2016();
            var3.hpRatio = (currentHp * 255) / maxHp;
         }       
            
         if(0 != (0x40 & var0)) {
        	 var4 = Class1211.incomingPackets.aInt2016();
        	 byte[] var16 = new byte[var4];
        	 ByteBuffer var19 = new ByteBuffer(var16);
        	 Class1211.incomingPackets.getOutOurCode(var16, 0, var4);
        	 Class65.playerClass966List[var1] = var19;
        	 var3.decodeAppearance(var19);
         }
         
         if((2048 & var0) != 0) {
             var4 = Class1211.incomingPackets.gea100();
             int[] var18 = new int[var4];
             int[] var17 = new int[var4];
             int[] var20 = new int[var4];

             for(int var22 = 0; ~var4 < ~var22; ++var22) {
                int var23 = Class1211.incomingPackets.aLong_011();
                if('\uffff' == var23) {
                   var23 = -1;
                }

                var18[var22] = var23;
                var17[var22] = Class1211.incomingPackets.aInt2016();
                var20[var22] = Class1211.incomingPackets.aInteger233();
             }

             Class75_Sub1.method1342(var17, var18, var3, (byte)-113, var20);
          }

   }

   public static void method1081(byte var0) {
      try {
         mapmarkerSprites = null;
         anIntArray729 = null;
         if(var0 <= 63) {
            decodeSprites((byte[])null);
         }

         aClass153_731 = null;
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "gk.E(" + var0 + ')');
      }
   }

   static final void decodeSprites(byte[] data) {
	   ByteBuffer class966 = new ByteBuffer(data);
	   class966.offset = data.length - 2;
	   Class95.spriteAmount = class966.aInteger233();
	   Class3_Sub13_Sub6.spriteHeights = new int[Class95.spriteAmount];
	   Class1013.spriteWidths = new int[Class95.spriteAmount];
	   Class164.spriteXOffsets = new int[Class95.spriteAmount];
	   Class120_Sub30_Sub1.spriteHaveAlpha = new boolean[Class95.spriteAmount];
	   Class163_Sub3.spriteAlphas = new byte[Class95.spriteAmount][];
	   ByteBuffer.aInteger1259 = new int[Class95.spriteAmount];
	   Class163_Sub1.spritePaletteIndicators = new byte[Class95.spriteAmount][];
	   class966.offset = data.length - (8 * Class95.spriteAmount) - 7;
	   Class3_Sub15.spriteTrimWidth = class966.aInteger233();
	   Class974.spriteTrimHeight = class966.aInteger233();
	   int paletteSize = (class966.readUnsignedByte() & 0xff) - -1;

	   int var4;
	   for(var4 = 0; var4 < Class95.spriteAmount; ++var4) {
		   Class164.spriteXOffsets[var4] = class966.aInteger233();
	   }

	   for(var4 = 0; var4 < Class95.spriteAmount; ++var4) {
		   ByteBuffer.aInteger1259[var4] = class966.aInteger233();
	   }

	   for(var4 = 0; Class95.spriteAmount > var4; ++var4) {
		   Class1013.spriteWidths[var4] = class966.aInteger233();
	   }
	   
	   for(var4 = 0; var4 < Class95.spriteAmount; ++var4) {
		   Class3_Sub13_Sub6.spriteHeights[var4] = class966.aInteger233();
	   }

	   class966.offset = -(8 * Class95.spriteAmount) + data.length + -7 + 3 + -(paletteSize * 3);
	   Class3_Sub13_Sub38.spritePalette = new int[paletteSize];

	   for(var4 = 1; ~var4 > ~paletteSize; ++var4) {
		   Class3_Sub13_Sub38.spritePalette[var4] = class966.aBoolean183();
		   if(0 == Class3_Sub13_Sub38.spritePalette[var4]) {
			   Class3_Sub13_Sub38.spritePalette[var4] = 1;
		   }
	   }

	   class966.offset = 0;
	   
	   for(var4 = 0; var4 < Class95.spriteAmount; ++var4) {
		   int var5 = Class1013.spriteWidths[var4];
		   int var6 = Class3_Sub13_Sub6.spriteHeights[var4];
		   int var7 = var5 * var6;
		   byte[] var8 = new byte[var7];
		   boolean var10 = false;
		   Class163_Sub1.spritePaletteIndicators[var4] = var8;
		   byte[] var9 = new byte[var7];
		   Class163_Sub3.spriteAlphas[var4] = var9;
		   int var11 = class966.readUnsignedByte();
		   int var12;
		   if(-1 != ~(1 & var11)) {
			   int var13;
			   for(var12 = 0; ~var12 > ~var5; ++var12) {
				   for(var13 = 0; var13 < var6; ++var13) {
					   var8[var12 + var13 * var5] = class966.getByte();
				   }
			   }

			   if(-1 != ~(var11 & 2)) {
				   for(var12 = 0; ~var12 > ~var5; ++var12) {
					   for(var13 = 0; var13 < var6; ++var13) {
						   byte var14 = var9[var5 * var13 + var12] = class966.getByte();
						   var10 |= -1 != var14;
					   }
				   }
			   }
		   } else {
			   for(var12 = 0; ~var7 < ~var12; ++var12) {
				   var8[var12] = class966.getByte();
			   }

			   if((2 & var11) != 0) {
				   for(var12 = 0; ~var12 > ~var7; ++var12) {
					   byte var16 = var9[var12] = class966.getByte();
					   var10 |= var16 != -1;
				   }
			   }
		   }

		   Class120_Sub30_Sub1.spriteHaveAlpha[var4] = var10;
	   }
   }

   static final void method1083(byte var0) {
      try {
         Class3_Sub13_Sub9.anIntArray3107 = Class1016.method62(true, 14585, 8, 2048, 4, 0.4F, 8, 35);
         int var1 = -5 / ((var0 - 45) / 59);
      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "gk.C(" + var0 + ')');
      }
   }

	static final void method1084(Class1002 var0, Class1002 var1) {
		if (var1.previousSub != null) {
			var1.unlinkSub();
		}

		var1.previousSub = var0;
		var1.nextSub = var0.nextSub;
		var1.previousSub.nextSub = var1;
		var1.nextSub.previousSub = var1;
	}

}

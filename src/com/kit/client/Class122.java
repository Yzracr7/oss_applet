package com.kit.client;

import com.kit.Engine;
import com.kit.res.Resources;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class Class122 extends Applet {

    private static final long serialVersionUID = -6988977112927509734L;
    private static String OS = System.getProperty("os.name").toLowerCase();

    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

    public static Properties props = new Properties();
    public static JFrame frame;
    private static JPanel jp = new JPanel();

    private static TrayIcon trayIcon;

    public static void main(String[] param) {
        new Class122("1");
    }

    public static TrayIcon getTray() {
        return trayIcon;
    }

    public static JFrame getFrame() {
        return frame;
    }

    public static JPanel getPanel() {
        return jp;
    }

    public Class122(String wld) {
        try {
            addTrayIcon();
            props.put("worldid", wld);
            props.put("members", "1");
            props.put("modewhat", "0");
            props.put("modewhere", "0");
            props.put("safemode", "0");
            props.put("game", "0");
            props.put("js", "1");
            props.put("lang", "0");
            props.put("affid", "0");
            props.put("lowmem", "1");
            props.put("settings", "kKmok3kJqOeN6D3mDdihco3oPeYN2KFy6W5--vZUbNA");
            Class942 sn = new Class942(this, 32, "runescape", Class922.aInteger_512);
            Class922.providesignlink(sn);
            Class922 localclient = new Class922();
            Class944.load();
            localclient.init();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    public void addTrayIcon() {
        if (SystemTray.isSupported()) {
           /* SystemTray tray = SystemTray.getSystemTray();
            Image image = Resources.get[0].getImage();
            final TrayIcon trayIcon = new TrayIcon(image);
            trayIcon.setImageAutoSize(true);
            trayIcon.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    //Double click
                    System.out.println("Out click tray icon");
                }
            });

            try {
                tray.add(trayIcon);
                Class122.trayIcon = trayIcon;
                Class122.trayIcon.setToolTip("OSKit (http://smite.io)");
            } catch (AWTException e) {
                System.err.println("TrayIcon could not be added.");
            }*/
        }
    }

    public static boolean unfocused = false;

    public void frame__windowStateChanged(WindowEvent e) {
        // minimized
        if ((e.getNewState() & Frame.ICONIFIED) == Frame.ICONIFIED) {
            unfocused = true;
        }
        // maximized
        else if ((e.getNewState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH) {
            unfocused = false;
        }
    }

    public static void changeFrameToResizable() {
        frame.setPreferredSize(new java.awt.Dimension(765 + 17, 503 + 60));
        frame.setMinimumSize(new java.awt.Dimension(765 + 17, 503 + 60));
        frame.setSize(frame.getWidth(), frame.getHeight());
        Class922.resizeWidth = frame.getWidth();
        Class922.resizeHeight = frame.getHeight();
    }

    public static void changeFrameToFixed() {
        Class922.resizeWidth = 765;
        Class922.resizeHeight = 503;
        frame.setLocationRelativeTo(null);
        frame.setPreferredSize(new java.awt.Dimension(765 + 5, 503 + 31));
        frame.setMinimumSize(new java.awt.Dimension(765 + 5, 503 + 31));
        frame.setSize(765, 503);
    }

    static int currentCursor = -1;

    public static void setCursor(int id) {
     /*   if (!Class929.newCursors)
            return;
        if (currentCursor != id)
            currentCursor = id;
        else
            return;
        try {
            Image image = Toolkit
                    .getDefaultToolkit()
                    .createImage(
                            Class987.ReadFile((Class942.getCacheDir()
                                    + File.separator + "cursors/Cursor_" + id + ".png")));
            frame.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
                    image, new Point(0, 0), null));
        } catch (Exception e) {
            //	System.out
            //			.println("[System Error]: Cursor cache cannot be located: "
            //					+ id);
        }*/
    }

    public String getParameter(String paramString) {
        return ((String) props.get(paramString));
    }

    public URL getDocumentBase() {
        return getCodeBase();
    }

    public URL getCodeBase() {
        try {
            return new URL("http://127.0.0.1");
        } catch (MalformedURLException localException) {
            localException.printStackTrace();
            return null;
        }
    }

    public void initMenubar() {
        JMenu fileMenu = new JMenu("File");
        String[] mainButtons = new String[]{"Forums", "-", "Exit"};
        for (String name : mainButtons) {
            JMenuItem menuItem = new JMenuItem(name);
            if (name.equalsIgnoreCase("-")) {
                fileMenu.addSeparator();
            } else if (name.equalsIgnoreCase("Forums")) {
                JMenu forumsMenu = new JMenu("Forums");
                fileMenu.add(forumsMenu);
                JMenuItem runeServer = new JMenuItem("Rune-Server");
                runeServer.addActionListener(new MenuActionListener());
                forumsMenu.add(runeServer);
            } else {
                menuItem.addActionListener(new MenuActionListener());
                fileMenu.add(menuItem);
            }
        }

        JMenuBar menuBar = new JMenuBar();
        JMenuBar jmenubar = new JMenuBar();

        frame.add(jmenubar);
        menuBar.add(fileMenu);
        frame.getContentPane().add(menuBar, BorderLayout.NORTH);
    }

    class MenuActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println("Selected: " + e.getActionCommand());

        }
    }

    public static void setDefaultCursor() {
       // if (Class122.getFrame().getCursor() != null && Class122.getFrame().getCursor().getType() != Cursor.DEFAULT_CURSOR)
        //    Class122.getFrame().setCursor(Cursor.getDefaultCursor());
    }

    public static void goFullscreen() {
        frame.dispose();
        try {
            GraphicsEnvironment env =
                    GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice dev = env.getDefaultScreenDevice();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setBackground(Color.darkGray);
            frame.setResizable(false);
            frame.setUndecorated(true);
            frame.add(Class122.getPanel());
            frame.pack();
            dev.setFullScreenWindow(frame);

            //GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(frame);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rebuildFrame() {

    }

    public static void resize() {
        try {
            System.out.println("w: " + frame.getWidth() + " h: "
                    + frame.getHeight());
            if (Class922.clientSize > 0) {
                Class922.resizeWidth = frame.getWidth() + (Class922.clientSize == 2 ? 16 : 0);
                Class922.resizeHeight = frame.getHeight() + (Class922.clientSize == 2 ? 38 : 0);
                if (Class922.aInteger_544 == 30) {
                    Class922.reloadFullscreenInterfaces();
                    if (Class1012.aBoolean_617) {
                        System.setProperty(
                                "sun.awt.noerasebackground",
                                "false");
                        if (Class38.gameClass942.thisApplet == null) {
                            System.out.println("[FATAL]: applet was null(1)");
                            return;
                        }
                        Class932.method598(false, 2, false,
                                0, Class922.resizeWidth,
                                Class922.resizeHeight);
                    } else {
                        Class922.set = true;
                        Class922.dontEnterGL = true;
                        System.setProperty(
                                "sun.awt.noerasebackground", "true");
                        if (Class38.gameClass942.thisApplet == null) {
                            System.out.println("[FATAL]: applet was null(2)");
                            return;
                        }
                        Class932.method598(false, 2, false,
                                0, Class922.resizeWidth,
                                Class922.resizeHeight);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Exception while resizing: " + e);
            e.printStackTrace();
        }
    }
}
package com.kit.client;

final class Class921 extends Class1002 {

    protected static boolean aBoolean3668 = false;
    protected static byte[][] aByteArrayArray3669;
    protected static int antiAliasing;
    protected static boolean[] interfacesToRefresh = new boolean[100];
    protected Class1013 aClass140_Sub7_3676;

    Class921(Class1013 var1) {
        try {
            this.aClass140_Sub7_3676 = var1;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "pa.<init>(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    public static void method627(byte var0) {
        try {
            if (var0 <= -112) {
                Class922.username = null;
                Class922.password = null;
                aByteArrayArray3669 = (byte[][]) null;
                interfacesToRefresh = null;
            }
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "pa.A(" + var0 + ')');
        }
    }

    static final void method628(int var0, int var1, int var2, Class946 var3) {
        if (~var2 == ~var3.currentAnimationId && 0 != ~var2) {
            Class954 var4 = Class954.list(var2);
            int var5 = var4.anInt1845;
            if (1 == var5) {
                var3.animationDelay = var1;
                var3.anInt2760 = 0;
                var3.anInt2776 = 1;
                var3.anInt2832 = 0;
                var3.anInt2773 = 0;
                Class1007.method1470(var3.x, var4, var0 + 183921384, var3.y, Class945.thisClass946 == var3, var3.anInt2832);
            }

            if (var5 == 2) {
                var3.anInt2773 = 0;
            }
        } else if (-1 == var2 || var3.currentAnimationId == -1 || Class954.list(var2).anInt1857 >= Class954.list(var3.currentAnimationId).anInt1857) {
            var3.anInt2776 = 1;
            var3.anInt2832 = 0;
            var3.animationDelay = var1;
            var3.anInt2811 = var3.walkQueueLocationIndex;
            var3.anInt2773 = 0;
            var3.anInt2760 = 0;
            var3.currentAnimationId = var2;
            if (var3.currentAnimationId != -1) {
                Class1007.method1470(var3.x, Class954.list(var3.currentAnimationId), 183921384, var3.y, var3 == Class945.thisClass946, var3.anInt2832);
            }
        }
    }
}

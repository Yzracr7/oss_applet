package com.kit.client;

final class Class954 {

    protected int anInt1845 = 2;
    protected boolean tween = false;
    protected static volatile long lastCanvasReplace = 0L;
    protected boolean aBoolean1848 = false;
    protected int anInt1849 = -1;
    protected int anInt1850 = -1;
    protected int[] frames;
    protected static Class1027 aClass153_1852;
    protected int anInt1854 = -1;
    protected boolean[] aBooleanArray1855;
    protected static Class927 aClass109_1856;
    protected int anInt1857 = 5;
    protected boolean aBoolean1859 = false;
    protected static Class1027 aClass153_1860;
    protected int anInt1861 = 99;
    protected static int anInt1862 = 0;
    protected int id;
    protected int anInt1865 = -1;
    protected int anInt1866 = -1;
    protected int[][] anIntArrayArray1867;
    protected static Class25[] aClass25Array1868;
    protected int[] cycles;
    private int[] anIntArray1870;
    protected static int[] anIntArray1871 = new int[2];
    protected boolean aBoolean1872 = false;

    static final Class3_Sub11 method2052(Class1017_2 var0, Class1223 var2) {
        long var3 = (long) ((var2.anInt2095 - -1 << 16) + var2.anInt2090) + (((long) var2.anInt2100 << 56) - -((long) var2.anInt2094 << 32));

        Class3_Sub11 var5 = (Class3_Sub11) var0.get(var3);
        if (null == var5) {
            var5 = new Class3_Sub11(var2.anInt2095, (float) var2.anInt2090, true, false, var2.anInt2094);
            var0.put(var5, var3);
        }

        return var5;
    }

    final void method2053(ByteBuffer var1) {
        while (true) {
            int var3 = var1.readUnsignedByte();
            if (var3 == 0) {
                return;
            }
            this.method2060(var3, var1);
        }
    }

    final Class960 method2054(int var1, int var2, int var3, Class960 var4, int var5, int var6) {
        try {
            int var7 = this.cycles[var2];
            var2 = this.frames[var2];
            Class955 var8 = Class955.list(var2 >> 16, var1 + -19749, false);
            var2 &= '\uffff';
            if (var8 == null) {
                return var4.method1890(true, true, true);
            } else {
                var5 &= 3;
                Class955 var9 = null;
                if ((this.tween || Class3_Sub26.forceTweeningEnabled) && ~var3 != 0 && this.frames.length > var3) {
                    var3 = this.frames[var3];
                    var9 = Class955.list(var3 >> 16, 0, false);
                    var3 &= '\uffff';
                }

                Class960 var10;
                if (var9 != null) {
                    var10 = var4.method1890(!var8.method559(var2) & !var9.method559(var3), !var8.method561(var2) & !var9.method561(var3), !this.aBoolean1848);
                } else {
                    var10 = var4.method1890(!var8.method559(var2), !var8.method561(var2), !this.aBoolean1848);
                }

                if (Class1012.aBoolean_617 && this.aBoolean1848) {
                    if (-2 != ~var5) {
                        if (2 != var5) {
                            if (~var5 == -4) {
                                ((Class948) var10).method1925();
                            }
                        } else {
                            ((Class948) var10).method1911();
                        }
                    } else {
                        ((Class948) var10).method1902();
                    }
                } else if (var5 != 1) {
                    if (2 != var5) {
                        if (3 == var5) {
                            var10.rotate90();
                        }
                    } else {
                        var10.rotate180();
                    }
                } else {
                    var10.rotate270();
                }

                var10.method1880(var8, var2, var9, var3, -1 + var6, var7, this.aBoolean1848);
                if (Class1012.aBoolean_617 && this.aBoolean1848) {
                    if (1 != var5) {
                        if (var5 == 2) {
                            ((Class948) var10).method1911();
                        } else if (var5 == 3) {
                            ((Class948) var10).method1902();
                        }
                    } else {
                        ((Class948) var10).method1925();
                    }
                } else if (~var5 != -2) {
                    if (-3 != ~var5) {
                        if (var5 == 3) {
                            var10.rotate270();
                        }
                    } else {
                        var10.rotate180();
                    }
                } else {
                    var10.rotate90();
                }

                return var10;
            }
        } catch (RuntimeException var11) {
            throw Class1134.method1067(var11, "tk.D(" + var1 + ',' + var2 + ',' + var3 + ',' + (var4 != null ? "{...}" : "null") + ',' + var5 + ',' + var6 + ')');
        }
    }

    final Class960 method2055(Class960 var1, byte var2, int var3, int var4, int var5) {
        try {
            int var7 = this.frames[var5];
            int var6 = this.cycles[var5];
            Class955 var8 = Class955.list(var7 >> 16, 0, false);
            var7 &= '\uffff';
            if (null == var8) {
                return var1.method1894(true, true, true);
            } else {
                Class955 var9 = null;
                if ((this.tween || Class3_Sub26.forceTweeningEnabled) && 0 != ~var3 && ~var3 > ~this.frames.length) {
                    var3 = this.frames[var3];
                    var9 = Class955.list(var3 >> 16, 0, false);
                    var3 &= '\uffff';
                }

                Class955 var10 = null;
                int var12 = 47 % ((var2 - 66) / 45);
                Class955 var11 = null;
                int var13 = 0;
                int var14 = 0;
                if (null != this.anIntArray1870) {
                    if (~this.anIntArray1870.length < ~var5) {
                        var13 = this.anIntArray1870[var5];
                        if (~var13 != -65536) {
                            var10 = Class955.list(var13 >> 16, 0, false);
                            var13 &= '\uffff';
                        }
                    }

                    if ((this.tween || Class3_Sub26.forceTweeningEnabled) && -1 != var3 && this.anIntArray1870.length > var3) {
                        var14 = this.anIntArray1870[var3];
                        if (~var14 != -65536) {
                            var11 = Class955.list(var14 >> 16, 0, false);
                            var14 &= '\uffff';
                        }
                    }
                }

                boolean var15 = !var8.method559(var7);
                boolean var16 = !var8.method561(var7);
                if (var10 != null) {
                    var15 &= !var10.method559(var13);
                    var16 &= !var10.method561(var13);
                }

                if (null != var9) {
                    var15 &= !var9.method559(var3);
                    var16 &= !var9.method561(var3);
                }

                if (null != var11) {
                    var15 &= !var11.method559(var14);
                    var16 &= !var11.method561(var14);
                }

                Class960 var17 = var1.method1894(var15, var16, !this.aBoolean1848);
                var17.method1880(var8, var7, var9, var3, var4 - 1, var6, this.aBoolean1848);
                if (null != var10) {
                    var17.method1880(var10, var13, var11, var14, var4 + -1, var6, this.aBoolean1848);
                }

                return var17;
            }
        } catch (RuntimeException var18) {
            throw Class1134.method1067(var18, "tk.E(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
        }
    }

    final Class960 method2056(int var1, int var2, int var3, int var4, Class960 var5, int var6) {
        try {
            int var7 = this.cycles[var2];
            var2 = this.frames[var2];
            Class955 var8 = Class955.list(var2 >> 16, var6 + -3, false);
            var2 &= '\uffff';
            if (null == var8) {
                return var5.method1894(true, true, true);
            } else {
                var4 &= var6;
                Class955 var9 = null;
                if ((this.tween || Class3_Sub26.forceTweeningEnabled) && ~var1 != 0 && ~var1 > ~this.frames.length) {
                    var1 = this.frames[var1];
                    var9 = Class955.list(var1 >> 16, var6 + -3, false);
                    var1 &= '\uffff';
                }

                Class960 var10;
                if (null == var9) {
                    var10 = var5.method1894(!var8.method559(var2), !var8.method561(var2), !this.aBoolean1848);
                } else {
                    var10 = var5.method1894(!var8.method559(var2) & !var9.method559(var1), !var8.method561(var2) & !var9.method561(var1), !this.aBoolean1848);
                }

                if (this.aBoolean1848 && Class1012.aBoolean_617) {
                    if (1 != var4) {
                        if (-3 == ~var4) {
                            ((Class948) var10).method1911();
                        } else if (-4 == ~var4) {
                            ((Class948) var10).method1925();
                        }
                    } else {
                        ((Class948) var10).method1902();
                    }
                } else if (var4 == 1) {
                    var10.rotate270();
                } else if (var4 == 2) {
                    var10.rotate180();
                } else if (var4 == 3) {
                    var10.rotate90();
                }

                var10.method1880(var8, var2, var9, var1, var3 + -1, var7, this.aBoolean1848);
                if (this.aBoolean1848 && Class1012.aBoolean_617) {
                    if (~var4 == -2) {
                        ((Class948) var10).method1925();
                    } else if (-3 == ~var4) {
                        ((Class948) var10).method1911();
                    } else if (~var4 == -4) {
                        ((Class948) var10).method1902();
                    }
                } else if (1 != var4) {
                    if (var4 == 2) {
                        var10.rotate180();
                    } else if (3 == var4) {
                        var10.rotate270();
                    }
                } else {
                    var10.rotate90();
                }

                return var10;
            }
        } catch (RuntimeException var11) {
            throw Class1134.method1067(var11, "tk.B(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + (var5 != null ? "{...}" : "null") + ',' + var6 + ')');
        }
    }

    public static void method2057(byte var0) {
        try {
            aClass25Array1868 = null;
            aClass109_1856 = null;
            aClass153_1860 = null;
            aClass153_1852 = null;
            if (var0 != -108) {
                aClass153_1852 = (Class1027) null;
            }

            anIntArray1871 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "tk.F(" + var0 + ')');
        }
    }

    final void postDecode() {
        if (-1 == this.anInt1866) {
            if (null == this.aBooleanArray1855) {
                this.anInt1866 = 0;
            } else {
                this.anInt1866 = 2;
            }
        }

        if (-1 == this.anInt1850) {
            if (null != this.aBooleanArray1855) {
                this.anInt1850 = 2;
            } else {
                this.anInt1850 = 0;
            }
        }
    }

    final Class960 method2059(Class960 var5, int nextFrame, int cycle, int frame) {
        int time = this.cycles[frame];
        frame = this.frames[frame];
        Class955 var7 = Class955.list(frame >> 16, 0, false);
        frame &= '\uffff';
        if (var7 == null) {
            return var5.method1882(true, true, true);
        } else {
            Class955 var9 = null;
            if ((this.tween || Class3_Sub26.forceTweeningEnabled) && ~nextFrame != 0 && ~this.frames.length < ~nextFrame) {
                nextFrame = this.frames[nextFrame];
                var9 = Class955.list(nextFrame >> 16, 0, false);
                nextFrame &= '\uffff';
            }

            Class960 var10;
            if (null == var9) {
                var10 = var5.method1882(!var7.method559(frame), !var7.method561(frame), !this.aBoolean1848);
            } else {
                var10 = var5.method1882(!var7.method559(frame) & !var9.method559(nextFrame), !var7.method561(frame) & !var9.method561(nextFrame), !this.aBoolean1848);
            }

            var10.method1880(var7, frame, var9, nextFrame, cycle + -1, time, this.aBoolean1848);
            return var10;
        }
    }

    private final void method2060(int arg0, ByteBuffer arg2) {
        if (arg0 == 1) {
            int frameCount = arg2.aInteger233();
            cycles = new int[frameCount];
            for (int i_23_ = 0; i_23_ < frameCount; i_23_++)
                cycles[i_23_] = arg2.aInteger233();
            frames = new int[frameCount];
            for (int i_24_ = 0; frameCount > i_24_; i_24_++)
                frames[i_24_] = arg2.aInteger233();
            for (int i_25_ = 0; frameCount > i_25_; i_25_++)
                frames[i_25_] = ((arg2.aInteger233() << 96142256) + frames[i_25_]);
        } else if ((arg0 ^ 0xffffffff) == -3)
            anInt1865 = arg2.aInteger233();
        else if (arg0 == 3) {
            this.aBooleanArray1855 = new boolean[256];
            int len = arg2.readUnsignedByte();
            for (int var5 = 0; var5 < len; ++var5) {
                this.aBooleanArray1855[arg2.readUnsignedByte()] = true;
            }
        } else if ((arg0 ^ 0xffffffff) == -5)
            aBoolean1859 = true;
        else if ((arg0 ^ 0xffffffff) != -6) {
            if (arg0 == 6)
                anInt1854 = arg2.aInteger233();
            else if ((arg0 ^ 0xffffffff) == -8)
                anInt1849 = arg2.aInteger233();
            else if ((arg0 ^ 0xffffffff) != -9) {
                if (arg0 == 9)
                    anInt1866 = arg2.readUnsignedByte();
                else if (arg0 != 10) {
                    if ((arg0 ^ 0xffffffff) == -12)
                        anInt1845 = arg2.readUnsignedByte();
                    else if (arg0 == 12) {
                        int len = arg2.readUnsignedByte();
                        this.anIntArray1870 = new int[len];
                        for (int var5 = 0; ~len < ~var5; ++var5) {
                            this.anIntArray1870[var5] = arg2.aInteger233();
                        }
                        for (int var5 = 0; ~len < ~var5; ++var5) {
                            this.anIntArray1870[var5] += arg2.aInteger233() << 16;
                        }
                    } else if ((arg0 ^ 0xffffffff) == -14) {
                        int len = arg2.readUnsignedByte();
                        int[] anIntArray3199 = new int[len];
                        for (int i_29_ = 0; len > i_29_; i_29_++)
                            anIntArray3199[i_29_] = arg2.aBoolean183();

                    } else if (arg0 == 14) {
                        aBoolean1848 = true;
                    } else if (15 == arg0) {
                        tween = true;
                    } else if (16 == arg0) {
                        aBoolean1872 = true;
                    } else if (arg0 == 18) {
                        boolean aBoolean409 = true;
                    } else if (arg0 == 19) {
                        int[] anIntArray416 = null;
                        if (anIntArray416 == null) {
                            anIntArray416 = (new int[anIntArrayArray1867.length]);
                            for (int i_15_ = 0; (i_15_ < anIntArrayArray1867.length); i_15_++)
                                anIntArray416[i_15_] = 255;
                        }
                        anIntArray416[arg2.readUnsignedByte()] = arg2.readUnsignedByte();
                    } else if (arg0 == 20) {
                        int[] anIntArray415 = null;
                        int[] anIntArray400 = null;
                        if (anIntArray415 == null || anIntArray400 == null) {
                            anIntArray415 = (new int[anIntArrayArray1867.length]);
                            anIntArray400 = (new int[anIntArrayArray1867.length]);
                            for (int i_16_ = 0; (anIntArrayArray1867.length > i_16_); i_16_++) {
                                anIntArray415[i_16_] = 256;
                                anIntArray400[i_16_] = 256;
                            }
                        }
                        int i_17_ = arg2.readUnsignedByte();
                        anIntArray415[i_17_] = arg2.aInteger233();
                        anIntArray400[i_17_] = arg2.aInteger233();
                    }
                } else
                    anInt1850 = arg2.readUnsignedByte();
            } else
                anInt1861 = arg2.readUnsignedByte();
        } else
            anInt1857 = arg2.readUnsignedByte();
    }

    static final Class954 list(int var0) {

        Class954 var2 = (Class954) SpriteDefinition.aClass93_1146.get((long) var0);
        if (var2 != null)
            return var2;
        int anim_config = Class922.getAnimationConfig(var0);
        byte[] data = aClass153_1860.getFile(anim_config, var0);

        var2 = new Class954();
        var2.id = var0;
        if (data != null) {
            var2.method2053(new ByteBuffer(data));
        }
        var2.postDecode();
        SpriteDefinition.aClass93_1146.put(var2, (long) var0);
        return var2;
    }

    static final void resetLocalVariables(boolean var0) {
        try {
            Class3_Sub26.anInt2556 = 0;
            Class991.aBoolean2774 = true;
            Class927.aLong1465 = 0L;
            Class989.aClass67_1443.anInt1018 = 0;
            Class3_Sub13_Sub6.focus = true;
            CanvasBuffer.method153(112);
            Class1217.beforeLastPacket = -1;
            Class7.lastPacket = -1;
            Class1008.incomingPacket = -1;
            Class159.logoutCycle = 0;
            Class38_Sub1.systemUpdateCycle = 0;
            Class3_Sub13_Sub1.outputStream.offset = 0;
            Class1030.anInt2582 = -1;
            Class957.timoutCycle = 0;
            Class1211.incomingPackets.offset = 0;

            int var1;
            for (var1 = 0; ~var1 > ~Class1244.hintsList.length; ++var1) {
                Class1244.hintsList[var1] = null;
            }

            Class3_Sub13_Sub34.contextOptionsAmount = 0;
            Class38_Sub1.drawContextMenu = false;
            Class23.method940(119, 0);

            for (var1 = 0; -101 < ~var1; ++var1) {
                Class1030.chatMessages[var1] = null;
            }

            Class164_Sub1.anInt3012 = 0;
            Class933.anInt3216 = (int) (Math.random() * 100.0D) + -50;
            Class45.mapFlagY = 0;
            Class1211.cameraYaw = 2047 & (int) (Math.random() * 20.0D) - 10;
            Class58.anInt909 = -1;
            Class159.anInt2022 = 0;
            Class161.anInt2028 = 0;
            Class1016.anInt42 = (int) (110.0D * Math.random()) + -55;
            Class1031.aBoolean1837 = false;
            Class164_Sub2.anInt3020 = -20 + (int) (30.0D * Math.random());
            Class113.anInt1552 = 0;
            Class65.mapFlagX = 0;
            Class3_Sub13_Sub8.anInt3102 = -60 + (int) (Math.random() * 120.0D);
            Class3_Sub13_Sub9.anInt3114 = 0;
            Class1030.anInt2589 = (int) (80.0D * Math.random()) - 40;
            Class163.anInt2046 = 0;

            for (var1 = 0; 2048 > var1; ++var1) {
                Class922.class946List[var1] = null;
                Class65.playerClass966List[var1] = null;
            }

            for (var1 = 0; var1 < '\u8000'; ++var1) {
                Class3_Sub13_Sub24.class1001List[var1] = null;
            }

            Class945.thisClass946 = Class922.class946List[2047] = new Class946();
            Class3_Sub13_Sub30.aClass61_3364.clear();
            Class3_Sub13_Sub15.aClass61_3177.clear();
            if (null != Class120_Sub30_Sub1.aClass61ArrayArrayArray3273) {
                for (var1 = 0; 4 > var1; ++var1) {
                    for (int var2 = 0; ~var2 > -105; ++var2) {
                        for (int var3 = 0; ~var3 > -105; ++var3) {
                            Class120_Sub30_Sub1.aClass61ArrayArrayArray3273[var1][var2][var3] = null;
                        }
                    }
                }
            }

            Class3_Sub13_Sub6.aClass61_3075 = new Class975();
            Class1025.anInt1357 = 0;
            Class8.localPlayerIds = 0;
            Class3_Sub13_Sub2.resetVarp();
            Class949.method103();
            Class75.anInt1105 = 0;
            Class163_Sub2_Sub1.anInt4014 = 0;
            Class157.anInt1996 = 0;
            Class3_Sub13_Sub34.anInt3414 = 0;
            Class961.anInt1904 = 0;
            Class970.anInt30 = 0;
            Class1211.anInt529 = 0;
            Class980.anInt1923 = 0;
            Class3_Sub28_Sub10.anInt3631 = 0;
            Class163_Sub2_Sub1.anInt4021 = 0;
            for (var1 = 0; ~Class981.anIntArray1277.length < ~var1; ++var1) {
                Class981.anIntArray1277[var1] = -1;
            }
            if (Class1143.mainScreenInterface != -1) {
                Class60.discardInterface(Class1143.mainScreenInterface);
            }
            for (Class1207 var7 = (Class1207) Class3_Sub13_Sub17.aClass130_3208.getFirst(); var7 != null; var7 = (Class1207) Class3_Sub13_Sub17.aClass130_3208.getNext()) {
                Class21.removeOverrideInterface(var7, true);
            }
            Class1143.mainScreenInterface = -1;
            Class3_Sub13_Sub17.aClass130_3208 = new Class1017_2(8);
            Class419.method122();
            Class3_Sub13_Sub7.aClass11_3087 = null;
            Class38_Sub1.drawContextMenu = false;
            Class3_Sub13_Sub34.contextOptionsAmount = 0;
            Class77.aClass52_1112.setAppearance(new int[]{0, 0, 0, 0, 0}, -1, false, (int[]) null);

            for (var1 = 0; 8 > var1; ++var1) {
                Class972.aClass94Array1299[var1] = null;
                Class1.aBooleanArray54[var1] = false;
                Class3_Sub13_Sub26.anIntArray3328[var1] = -1;
            }
            Class3_Sub28_Sub9.method580((byte) 80);
            Class3_Sub13_Sub4.aBoolean3064 = true;

            for (var1 = 0; var1 < 100; ++var1) {
                Class921.interfacesToRefresh[var1] = true;
            }
            Class1002.anInt2572 = 0;
            Class951.clanMembers = null;
            Class1034.aClass94_251 = null;

            for (var1 = 0; 6 > var1; ++var1) {
                Class3_Sub13_Sub33.aClass133Array3393[var1] = new Class974();
            }
            for (var1 = 0; -26 < ~var1; ++var1) {
                Class3_Sub13_Sub15.currentStats[var1] = 0;
                Class3_Sub20.maxStats[var1] = 0;
                Class974.currentExp[var1] = 0;
            }
            if (Class1012.aBoolean_617) {
                Class3_Sub13_Sub14.method236((byte) 64);
            }
            Class932.aBoolean4068 = var0;
            Class3_Sub13_Sub28.aClass94_3353 = Class56.aClass94_891;
            Class943.aBoolean1084 = false;
            Class3_Sub13_Sub38.aShortArray3455 = Class3_Sub13_Sub9.aShortArray3110 = Class136.aShortArray1779 = Class3_Sub13_Sub38.aShortArray3453 = new short[256];
            CanvasBuffer.method165(-7878);
            Class1027.aBoolean1951 = false;
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "tk.A(" + var0 + ')');
        }
    }
}

package com.kit.client;

abstract class Class151 {

   //static Class1008 aClass94_1932 = Class1008.createJString(")4a=");
   static Class1034 aClass11_1933;
   static float[] aFloatArray1934 = new float[]{0.073F, 0.169F, 0.24F, 1.0F};
   static Class1008 aClass94_1935 = Class943.create("::clientdrop");
   static Class8 aClass8_1936;


   public static void method2093(int var0) {
      try {
         aClass8_1936 = null;
         //aClass94_1932 = null;
         aFloatArray1934 = null;
         aClass94_1935 = null;
         aClass11_1933 = null;
         if(var0 != 1) {
            method2096(-83, 44, -77, 121L);
         }

      } catch (RuntimeException var2) {
         throw Class1134.method1067(var2, "v.R(" + var0 + ')');
      }
   }

   abstract Class62 method2094(int var1);

   abstract void method2095(int var1, int var2);

   static final boolean method2096(int var0, int var1, int var2, long var3) {
      Class949 var5 = Class75_Sub2.class949s[var0][var1][var2];
      if(var5 == null) {
         return false;
      } else if(var5.aClass70_2234 != null && var5.aClass70_2234.aLong1048 == var3) {
         return true;
      } else if(var5.aClass19_2233 != null && var5.aClass19_2233.aLong428 == var3) {
         return true;
      } else if(var5.aClass12_2230 != null && var5.aClass12_2230.aLong328 == var3) {
         return true;
      } else {
         for(int var6 = 0; var6 < var5.anInt2223; ++var6) {
            if(var5.aClass25Array2221[var6].aLong498 == var3) {
               return true;
            }
         }

         return false;
      }
   }

   abstract int method2097(int var1, int var2);

   abstract byte[] method2098(int var1, int var2);

   static final void playMusic(boolean var0, int var1, int var2, Class1027 cacheIndex, boolean var4, int var5, int var6) {
     System.out.println("PlayMusic: " + var0 + ", " + var1 + ", " + var2 + ", " + var4 + ", " + var5 + ", " + var6);
	  //PlayMusic: true, 0, 0, false, 255, 2 
     try {
         Class101.soundCacheIndex = cacheIndex;
         Class132.anInt1741 = var2;
         Class3_Sub13_Sub39.soundId = var1;
         Class3_Sub9.aBoolean2311 = var4;
         Class10.anInt154 = 1;
         Class1211.anInt546 = var6;
         if(!var0) {
            method2096(-8, 46, 45, -6L);
         }

         Class3_Sub13_Sub36.musicVol = var5;
      } catch (RuntimeException var8) {
         throw Class1134.method1067(var8, "v.Q(" + var0 + ',' + var1 + ',' + var2 + ',' + (cacheIndex != null?"{...}":"null") + ',' + var4 + ',' + var5 + ',' + var6 + ')');
      }
   }

}

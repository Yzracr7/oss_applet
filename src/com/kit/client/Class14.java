package com.kit.client;

final class Class14 {

    protected  static boolean aBoolean337;
    protected  static Class1008 aClass94_339 = Class943.create("1");
    protected  static int areaSoundsVolume = 127;
    protected  static Class1008 aClass94_341 = Class943.create(")3");

    private static final Class1047[] method885(int var1, Class1027 var2, int var3) {
        return !Class922.spriteExists(var2, var3, var1) ? null : Class1043.method1281(0);
    }

    public static void method886(byte var0) {
        try {
            if (var0 < 26) {
                aClass94_339 = (Class1008) null;
            }

            aClass94_339 = null;
            aClass94_341 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "cg.B(" + var0 + ')');
        }
    }

    static final void loadExtraSprites(int var0, Class1027 var1) {
        try {
            Class922.infinitySymbol = Class922.method602(0, Class922.infinitySymbolId, var1);

    	/* client.fishCursor = InterfaceChangeNode.buffer(client.fishCursorId, var1, true);
    	 client.treeCursor = InterfaceChangeNode.buffer(client.treeCursorId, var1, true);
    	 client.prayCursor = InterfaceChangeNode.buffer(client.prayCursorId, var1, true);
    	 client.mineCursor = InterfaceChangeNode.buffer(client.mineCursorId, var1, true);
    	 client.eatCursor = InterfaceChangeNode.buffer(client.eatCursorId, var1, true);
    	 client.drinkCursor = InterfaceChangeNode.buffer(client.drinkCursorId, var1, true);
    	 client.wearCursor = InterfaceChangeNode.buffer(client.wearCursorId, var1, true);
    	 client.talkCursor = InterfaceChangeNode.buffer(client.talkCursorId, var1, true);
    	 client.useCursor = InterfaceChangeNode.buffer(client.useCursorId, var1, true);
    	 client.takeCursor = InterfaceChangeNode.buffer(client.takeCursorId, var1, true);
    	 client.pointer = InterfaceChangeNode.buffer(client.pointerId, var1, true);
    	 
    	 
    	 client.attackCursor = InterfaceChangeNode.buffer(client.attackCursorId, var1, true);
    	 client.climbUpCursor = InterfaceChangeNode.buffer(client.climbUpCursorId, var1, true);
    	 client.climbDownCursor = InterfaceChangeNode.buffer(client.climbDownCursorId, var1, true);
    	 client.stealCursor = InterfaceChangeNode.buffer(client.stealCursorId, var1, true);
    	 client.doorCursor = InterfaceChangeNode.buffer(client.doorCursorId, var1, true);
    	 client.enterCursor = InterfaceChangeNode.buffer(client.enterCursorId, var1, true);
    	 client.magicCursor = InterfaceChangeNode.buffer(client.magicCursorId, var1, true);
    	 client.searchCursor = InterfaceChangeNode.buffer(client.searchCursorId, var1, true);
    	 client.chooseCursor = InterfaceChangeNode.buffer(client.chooseCursorId, var1, true);
    	 client.withdrawCursor = InterfaceChangeNode.buffer(client.withdrawCursorId, var1, true);
    	 client.slashCursor = InterfaceChangeNode.buffer(client.slashCursorId, var1, true);
    	 client.craftCursor = InterfaceChangeNode.buffer(client.craftCursorId, var1, true);
    	 client.acceptCursor = InterfaceChangeNode.buffer(client.acceptCursorId, var1, true);
    	 client.declineCursor = InterfaceChangeNode.buffer(client.declineCursorId, var1, true);
    	 client.smeltCursor = InterfaceChangeNode.buffer(client.smeltCursorId, var1, true);
    	 client.cleanCursor = InterfaceChangeNode.buffer(client.cleanCursorId, var1, true);
    	 */
            Class945.aClass3_Sub28_Sub16_Sub2Array2140 = new Class1206_2[86];
            for (int i = 0; i < 86; i++) {
                Class945.aClass3_Sub28_Sub16_Sub2Array2140[i] = Class40.method1043(0, var1, var0 + -3199, 1246 + i);
            }
            //Above is 'mapfunctions' ?
            Class75_Sub3.old_hitmarkers = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.old_markers_id, var1);
            Class75_Sub3.new_hitmarkers = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.new_markers_id, var1);
            Class75_Sub3.new_hitmarkers2hq = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.new_markers_id2hq, var1);
            Class75_Sub3.new_hitmarkers2dark = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.new_markers_id2dark, var1);
            Class75_Sub3.crit_font = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.crit_font_id, var1);
            Class75_Sub3.hit_font = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.hit_font_id, var1);
            Class75_Sub3.soaking = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.soakingId, var1);
            Class75_Sub3.healmark = Class140_Sub6.method2027(0, Class3_Sub13_Sub23_Sub1.healmarkId, var1);
            Class1006.aClass3_Sub28_Sub16Array996 = Class140_Sub6.method2027(0, Class922.anInt2195, var1);
            Class3_Sub13_Sub31.pkIconSprites = Class140_Sub6.method2027(0, Class1002.pkIconsId, var1);
            Class1001.headIconSprites = Class140_Sub6.method2027(0, Class922.headIconsId, var1);

            Class922.mapscenes = Class3_Sub13_Sub23_Sub1.method28622222(0, Class922.mapscenesId, var1); //hd mapscenes fix - mikey
            Class922.mapbackSprte = Class1042_3.a(Class922.mapbackId, var1, true);//Class3_Sub28_Sub11.method602(0, client.mapbackId, (byte)-18, var1);
            //Class922.xpCounterHover = InterfaceChangeNode.buffer(Class922.mapbackIdNew, var1, true);

            Class922.mapbackFsSprite = Class1042_3.a(Class922.mapbackFsId, var1, true);
            Class922.chatbuttonFsSprite = Class1042_3.a(Class922.chatbuttonFsId, var1, true);
            Class922.tabstonesFsSprite = Class1042_3.a(Class922.tabstonesFsId, var1, true);
            //orbs
            Class922.orbBg = Class1042_3.a(Class922.orbBgId, var1, true);
            Class922.hpFill = Class1042_3.a(Class922.hpFillId, var1, true);
            Class922.poisonFill = Class1042_3.a(Class922.poisonFillId, var1, true);
            Class922.hpIcon = Class1042_3.a(Class922.hpIconId, var1, true);
            Class922.prayFill = Class1042_3.a(Class922.prayFillId, var1, true);
            Class922.prayIcon = Class1042_3.a(Class922.prayIconId, var1, true);
            Class922.runFill = Class1042_3.a(Class922.runFillId, var1, true);
            Class922.runIcon = Class1042_3.a(Class922.runIconId, var1, true);
            Class922.orbDrain = Class1042_3.a(Class922.orbDrainId, var1, true);
            Class922.runFillOn = Class1042_3.a(Class922.runFillOnId, var1, true);
            Class922.runOnIcon = Class1042_3.a(Class922.runOnIconId, var1, true);

            Class922.worldmap0 = Class1042_3.a(Class922.worldmap0Id, var1, true);
            Class922.worldmap1 = Class1042_3.a(Class922.worldmap1Id, var1, true);
            Class922.worldmap2 = Class1042_3.a(Class922.worldmap2Id, var1, true);

            Class922.chatbuttonselected = Class1042_3.a(Class922.chatbuttonselectedId, var1, true);

            Class922.redStone0 = Class1042_3.a(Class922.redStoneId, var1, true);
            Class922.redStone1 = Class1042_3.a(Class922.redStone1Id, var1, true);
            Class922.redStone2 = Class1042_3.a(Class922.redStone2Id, var1, true);
            Class922.redStone3 = Class1042_3.a(Class922.redStone3Id, var1, true);
            Class922.redStone4 = Class1042_3.a(Class922.redStone4Id, var1, true);
            Class922.redStone5 = Class1042_3.a(Class922.redStone5Id, var1, true);

            Class922.aboveCompass = Class1042_3.a(Class922.aboveCompassId, var1, true);

            Class922.attackIcon = Class1042_3.a(Class922.attackIconId, var1, true);
            Class922.skillsIcon = Class1042_3.a(Class922.skillsIconId, var1, true);
            Class922.questIcon = Class1042_3.a(Class922.questIconId, var1, true);
            Class922.inventoryIcon = Class1042_3.a(Class922.inventoryIconId, var1, true);
            Class922.equipmentIcon = Class1042_3.a(Class922.equipmentIconId, var1, true);
            Class922.prayerIcon = Class1042_3.a(Class922.prayerIconId, var1, true);
            Class922.magicIcon = Class1042_3.a(Class922.magicIconId, var1, true);
            Class922.clanChatIcon = Class1042_3.a(Class922.clanChatIconId, var1, true);
            Class922.friendsIcon = Class1042_3.a(Class922.friendsIconId, var1, true);
            Class922.ignoreIcon = Class1042_3.a(Class922.ignoreIconId, var1, true);
            Class922.logoutIcon = Class1042_3.a(Class922.logoutIconId, var1, true);
            Class922.settingsIcon = Class1042_3.a(Class922.settingsIconId, var1, true);
            Class922.emotesIcon = Class1042_3.a(Class922.emotesIconId, var1, true);
            Class922.musicIcon = Class1042_3.a(Class922.musicIconId, var1, true);

            Class922.xpButton = Class1042_3.a(Class922.xpButtonId, var1, true);
            Class922.xp_counter = Class1042_3.a(Class922.xp_counter_id, var1, true);
            Class922.xp_counter_2 = Class1042_3.a(Class922.xp_counter_2_id, var1, true);
            Class922.xpCounterActive = Class1042_3.a(Class922.xpCounterActiveId, var1, true);
            Class922.xpCounterActive_2 = Class1042_3.a(Class922.xpCounterActive2Id, var1, true);
            Class922.xpCounterHover = Class1042_3.a(Class922.xpCounterHoverId, var1, true);

            Class922.compassBg = Class1042_3.a(Class922.compassBgId, var1, true);

            //orbs
            //Class922.worldmap0 = InterfaceChangeNode.buffer(Class922.orbBgIdNew, var1, true);
            //Class922.worldmap1 = InterfaceChangeNode.buffer(Class922.orbBgOnIdNew, var1, true);
            Class922.orbBgOSHover = Class1042_3.a(Class922.orbBgOSHoverId, var1, true);
            Class922.hpFillNew = Class1042_3.a(Class922.hpFillIdNew, var1, true);
            //client.hpIconNew = InterfaceChangeNode.buffer(client.hpIconIdNew, var1, true);
            Class922.osrsChannelBackground = Class1042_3.a(Class922.osrsChannelBackgroundID, var1, true);
            Class922.prayIconNew = Class1042_3.a(Class922.prayIconIdNew, var1, true);
            Class922.runFillNew = Class1042_3.a(Class922.runFillIdNew, var1, true);
            Class922.orbDrainNew = Class1042_3.a(Class922.orbDrainIdNew, var1, true);
            Class922.orb_drain_hd = Class3_Sub13_Sub23_Sub1.method286(0, Class922.orb_drain_hd_id, var1);
            Class922.randomSprite = Class3_Sub13_Sub23_Sub1.method286(0, Class922.orb_drain_hd_id, var1);

            Class922.bountyTeleSprite = Class3_Sub13_Sub23_Sub1.method286(0, Class922.bountyTeleSpriteId, var1);

            //Class922.xpButton = InterfaceChangeNode.buffer(Class922.xpButtonId, var1, true);
            Class922.restIcon = Class1042_3.a(Class922.restIconId, var1, true);

            Class922.forgottenPasswordLogin = Class1042_3.a(Class922.forgottenPasswordLoginId, var1, true);
            Class922.rememberUsernameLogin = Class1042_3.a(Class922.rememberUsernameLoginId, var1, true);
            Class922.hideUsernamelogin = Class1042_3.a(Class922.hideUsernameLoginId, var1, true);
            Class922.rememberToggleOff = Class1042_3.a(Class922.rememberToggleOffId, var1, true);
            Class922.rememberToggleOffHover = Class1042_3.a(Class922.rememberToggleOffHoverId, var1, true);
            Class922.blockIcon = Class1042_3.a(Class922.blockIconId, var1, true);

            Class922.musicToggle = Class3_Sub13_Sub23_Sub1.method286(0, Class922.musicToggleId, var1);
            Class922.hd_sprites = Class3_Sub13_Sub23_Sub1.method286(0, Class922.hd_sprites_id, var1);
            Class922.sd_sprites = Class3_Sub13_Sub23_Sub1.method286(0, Class922.sd_sprites_id, var1);

            //Class922.worldmap0 = Class3_Sub13_Sub23_Sub1.method286(0, Class922.worldmap0id, var1);
            //Class922.worldmap1 = Class3_Sub13_Sub23_Sub1.method286(0, Class922.worldmap1id, var1);

            Class166.hintIconSprites = Class140_Sub6.method2027(0, Class3_Sub13_Sub29.hintIconsId, var1);
            Class1210.hintMapmarkerSprites = Class140_Sub6.method2027(0, Class1210.hintMapmarkersId, var1);
            // Class45.mapmarkerSprites =  Class3_Sub13_Sub23_Sub1.method286(0, Class3_Sub13_Sub4.mapmarkersId, var1); //Class140_Sub6.method2027(0, Class3_Sub13_Sub4.mapmarkersId, var1);

            Class45.mapmarkerSprites = Class3_Sub13_Sub23_Sub1.method286(0, Class3_Sub13_Sub4.mapmarkersId, var1);

            Class1210.hintMapmarkerSprites = Class140_Sub6.method2027(0, Class1210.hintMapmarkersId, var1);
            //client.mapscenes = Class3_Sub13_Sub23_Sub1.method286(0, client.mapscenesId, var1);

            Class139.aClass3_Sub28_Sub16Array1825 = Class3_Sub13_Sub23_Sub1.method286(0, Class75_Sub1.anInt2633, var1);
            Class80.mapIconsArray = Class3_Sub13_Sub23_Sub1.method286(0, Class40.mapIconIds, var1);
            Class1031.scrollBarSprite = Class85.method1424(var1, (byte) -12, 0, Class3_Sub15.scrollbarSpriteId);
            Class1031.scrollBarFsSprite = Class85.method1424(var1, (byte) -12, 0, Class3_Sub15.scrollbarFsSpriteId);
            Class120_Sub30_Sub1.aClass109Array3270 = Class85.method1424(var1, (byte) -12, 0, Class3_Sub28_Sub18.anInt3757);
            Class922.getSmallClass1019().method697(Class120_Sub30_Sub1.aClass109Array3270, (int[]) null);
            Class922.getRegularClass1019().method697(Class120_Sub30_Sub1.aClass109Array3270, (int[]) null);
            Class922.getBoldClass1019().method697(Class120_Sub30_Sub1.aClass109Array3270, (int[]) null);
            if (Class1012.aBoolean_617) {
                Class141.aClass109_Sub1Array1843 = method885(Class45.anInt735, var1, 0);
                for (int var2 = 0; Class141.aClass109_Sub1Array1843.length > var2; ++var2) {
                    Class141.aClass109_Sub1Array1843[var2].method1675();
                }
            }
            Class1206_2 compass_sprite = Class40.method1043(100, var1, var0 + -3199, Class93.compassId);
            compass_sprite.resize();
            if (Class1012.aBoolean_617) {
                Class922.compassSprite = new Class1011(compass_sprite);
            } else {
                Class922.compassSprite = compass_sprite;
            }

         /*Class1206_2[] var3 = Class157.method2176(0, 32767, IntegerNode.anInt2471, var1);

         for(var4 = 0; ~var4 > ~var3.length; ++var4) {
            var3[var4].resize();
         }

         if(!Class1012.aBoolean_617) {
            Class3_Sub13_Sub39.aClass3_Sub28_Sub16Array3458 = var3;
         } else {
            Class3_Sub13_Sub39.aClass3_Sub28_Sub16Array3458 = new Class957[var3.length];

            for(var4 = 0; var4 < var3.length; ++var4) {
               Class3_Sub13_Sub39.aClass3_Sub28_Sub16Array3458[var4] = new Class1011(var3[var4]);
            }
         }*/

            int var5 = (int) ((double) var0 * Math.random()) - 10;
            int var4 = (int) (21.0D * Math.random()) - 10;
            int var6 = -10 + (int) (21.0D * Math.random());
            int var7 = -20 + (int) (Math.random() * 41.0D);

            for (int var8 = 0; var8 < Class945.aClass3_Sub28_Sub16_Sub2Array2140.length; ++var8) {
                Class945.aClass3_Sub28_Sub16_Sub2Array2140[var8].method669(var4 + var7, var7 + var5, var7 + var6);
            }

            if (!Class1012.aBoolean_617) {
                Class991.aClass3_Sub28_Sub16Array2839 = Class945.aClass3_Sub28_Sub16_Sub2Array2140;
            } else {
                Class991.aClass3_Sub28_Sub16Array2839 = new Class957[Class945.aClass3_Sub28_Sub16_Sub2Array2140.length];

                for (int var8 = 0; Class945.aClass3_Sub28_Sub16_Sub2Array2140.length > var8; ++var8) {
                    Class991.aClass3_Sub28_Sub16Array2839[var8] = new Class1011(Class945.aClass3_Sub28_Sub16_Sub2Array2140[var8]);
                }
            }
            if (src == null)
                src = new int[151];
            if (dest == null)
                dest = new int[151];
            if (!tinged)
                ting();
            if (Class922.mapbackSource == null /*&& !Class1012.aBoolean_617*/) {
                //Minimap shape for 464-474
                Class1047 sprite = (Class1047) Class922.mapbackSprte;
                //sprite.method1675();
                Class922.mapbackSource = new int[sprite.height];
                Class922.mapbackDest = new int[sprite.height];
                Class922.compassSource = new int[33];
                Class922.compassDest = new int[33];
                for (int id = 5; id < sprite.height; id++) {
                    int i_44_ = 999;
                    int i_45_ = 0;
                    for (int i_46_ = 20; i_46_ < 172; i_46_++) {
                        if (sprite.indicators[i_46_ + sprite.width * id] != 0 || i_46_ <= 34 && id <= 34) {
                            if (i_44_ != 999) {
                                i_45_ = i_46_;
                                break;
                            }
                        } else if (i_44_ == 999)
                            i_44_ = i_46_;
                    }
                    Class922.mapbackSource[id - 5] = i_44_ - 25;
                    Class922.mapbackDest[id - 5] = i_45_ - i_44_;
                }
                for (int id = 0; id < 33; id++) {
                    int i_40_ = 999;
                    int i_41_ = 0;
                    for (int i_42_ = 0; i_42_ < 34; i_42_++) {
                        if (sprite.indicators[(sprite.width * id) + i_42_] != 0) {
                            if (i_40_ != 999) {
                                i_41_ = i_42_;
                                break;
                            }
                        } else if (i_40_ == 999)
                            i_40_ = i_42_ - 5;
                    }
                    Class922.compassSource[id] = i_40_;
                    Class922.compassDest[id] = i_41_ - i_40_;
                }
            }


        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "cg.A(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    public static boolean tinged = false;

    static int[] src = new int[151];
    static int[] dest = new int[151];

    public static void ting() {
        tinged = true;
        src[0] = 4 + 18;
        src[1] = 4 + 30;
        src[2] = 4 + 38;
        src[3] = 4 + 46;
        src[4] = 4 + 52;
        src[5] = 4 + 56;
        src[6] = 4 + 62;
        src[7] = 4 + 66;
        src[8] = 4 + 70;
        src[9] = 4 + 74;
        src[10] = 4 + 78;
        src[11] = 4 + 80;
        src[12] = 4 + 84;
        src[13] = 4 + 86;
        src[14] = 4 + 90;
        src[15] = 4 + 92;
        src[16] = 4 + 94;
        src[17] = 4 + 98;
        src[18] = 4 + 100;
        src[19] = 4 + 102;
        src[20] = 4 + 104;
        src[21] = 4 + 106;
        src[22] = 4 + 108;
        src[23] = 4 + 110;
        src[24] = 4 + 112;
        src[25] = 4 + 114;
        src[26] = 4 + 116;
        src[27] = 4 + 118;
        src[28] = 4 + 118;
        src[29] = 4 + 120;
        src[30] = 4 + 122;
        src[31] = 4 + 124;
        src[32] = 4 + 124;
        src[33] = 4 + 126;
        src[34] = 4 + 128;
        src[35] = 4 + 128;
        src[36] = 4 + 130;
        src[37] = 4 + 132;
        src[38] = 4 + 132;
        src[39] = 4 + 134;
        src[40] = 4 + 134;
        src[41] = 4 + 136;
        src[42] = 4 + 136;
        src[43] = 4 + 138;
        src[44] = 4 + 138;
        src[45] = 4 + 140;
        src[46] = 4 + 140;
        src[47] = 4 + 140;
        src[48] = 4 + 142;
        src[49] = 4 + 142;
        src[50] = 4 + 144;
        src[51] = 4 + 144;
        src[52] = 4 + 144;
        src[53] = 4 + 146;
        src[54] = 4 + 146;
        src[55] = 4 + 146;
        src[56] = 4 + 146;
        src[57] = 4 + 148;
        src[58] = 4 + 148;
        src[59] = 4 + 148;
        src[60] = 4 + 148;
        src[61] = 4 + 148;
        src[62] = 4 + 150;
        src[63] = 4 + 150;
        src[64] = 4 + 150;
        src[65] = 4 + 150;
        src[66] = 4 + 150;
        src[67] = 4 + 150;
        src[68] = 4 + 152;
        src[69] = 4 + 152;
        src[70] = 4 + 152;
        src[71] = 4 + 152;
        src[72] = 4 + 152;
        src[73] = 4 + 152;
        src[74] = 4 + 152;
        src[75] = 4 + 152;

        src[150 - 0] = 4 + 18;
        src[150 - 1] = 4 + 30;
        src[150 - 2] = 4 + 38;
        src[150 - 3] = 4 + 46;
        src[150 - 4] = 4 + 52;
        src[150 - 5] = 4 + 56;
        src[150 - 6] = 4 + 62;
        src[150 - 7] = 4 + 66;
        src[150 - 8] = 4 + 70;
        src[150 - 9] = 4 + 74;
        src[150 - 10] = 4 + 78;
        src[150 - 11] = 4 + 80;
        src[150 - 12] = 4 + 84;
        src[150 - 13] = 4 + 86;
        src[150 - 14] = 4 + 90;
        src[150 - 15] = 4 + 92;
        src[150 - 16] = 4 + 94;
        src[150 - 17] = 4 + 98;
        src[150 - 18] = 4 + 100;
        src[150 - 19] = 4 + 102;
        src[150 - 20] = 4 + 104;
        src[150 - 21] = 4 + 106;
        src[150 - 22] = 4 + 108;
        src[150 - 23] = 4 + 110;
        src[150 - 24] = 4 + 112;
        src[150 - 25] = 4 + 114;
        src[150 - 26] = 4 + 116;
        src[150 - 27] = 4 + 118;
        src[150 - 28] = 4 + 118;
        src[150 - 29] = 4 + 120;
        src[150 - 30] = 4 + 122;
        src[150 - 31] = 4 + 124;
        src[150 - 32] = 4 + 124;
        src[150 - 33] = 4 + 126;
        src[150 - 34] = 4 + 128;
        src[150 - 35] = 4 + 128;
        src[150 - 36] = 4 + 130;
        src[150 - 37] = 4 + 132;
        src[150 - 38] = 4 + 132;
        src[150 - 39] = 4 + 134;
        src[150 - 40] = 4 + 134;
        src[150 - 41] = 4 + 136;
        src[150 - 42] = 4 + 136;
        src[150 - 43] = 4 + 138;
        src[150 - 44] = 4 + 138;
        src[150 - 45] = 4 + 140;
        src[150 - 46] = 4 + 140;
        src[150 - 47] = 4 + 140;
        src[150 - 48] = 4 + 142;
        src[150 - 49] = 4 + 142;
        src[150 - 50] = 4 + 144;
        src[150 - 51] = 4 + 144;
        src[150 - 52] = 4 + 144;
        src[150 - 53] = 4 + 146;
        src[150 - 54] = 4 + 146;
        src[150 - 55] = 4 + 146;
        src[150 - 56] = 4 + 146;
        src[150 - 57] = 4 + 148;
        src[150 - 58] = 4 + 148;
        src[150 - 59] = 4 + 148;
        src[150 - 60] = 4 + 148;
        src[150 - 61] = 4 + 148;
        src[150 - 62] = 4 + 150;
        src[150 - 63] = 4 + 150;
        src[150 - 64] = 4 + 150;
        src[150 - 65] = 4 + 150;
        src[150 - 66] = 4 + 150;
        src[150 - 67] = 4 + 150;
        src[150 - 68] = 4 + 152;
        src[150 - 69] = 4 + 152;
        src[150 - 70] = 4 + 152;
        src[150 - 71] = 4 + 152;
        src[150 - 72] = 4 + 152;
        src[150 - 73] = 4 + 152;
        src[150 - 74] = 4 + 152;
        src[150 - 75] = 4 + 152;

        dest[0] = 67;
        dest[1] = 61;
        dest[2] = 57;
        dest[3] = 53;
        dest[4] = 50;
        dest[5] = 48;
        dest[6] = 45;
        dest[7] = 43;
        dest[8] = 41;
        dest[9] = 39;
        dest[10] = 37;
        dest[11] = 36;
        dest[12] = 34;
        dest[13] = 33;
        dest[14] = 31;
        dest[15] = 30;
        dest[16] = 29;
        dest[17] = 28;
        dest[18] = 27;
        dest[19] = 26;
        dest[20] = 25;
        dest[21] = 24;
        dest[22] = 23;
        dest[23] = 22;
        dest[24] = 21;
        dest[25] = 20;
        dest[26] = 19;
        dest[27] = 18;
        dest[28] = 17;
        dest[29] = 17;
        dest[30] = 16;
        dest[31] = 15;
        dest[32] = 14;
        dest[33] = 14;
        dest[34] = 13;
        dest[35] = 12;
        dest[36] = 12;
        dest[37] = 11;
        dest[38] = 10;
        dest[39] = 10;
        dest[40] = 9;
        dest[41] = 9;
        dest[42] = 8;
        dest[43] = 8;
        dest[44] = 7;
        dest[45] = 7;
        dest[46] = 6;
        dest[47] = 6;
        dest[48] = 6;
        dest[49] = 5;
        dest[50] = 5;
        dest[51] = 4;
        dest[52] = 4;
        dest[53] = 4;
        dest[54] = 3;
        dest[55] = 3;
        dest[56] = 3;
        dest[57] = 3;
        dest[58] = 2;
        dest[59] = 2;
        dest[60] = 2;
        dest[61] = 2;
        dest[62] = 1;
        dest[63] = 1;
        dest[64] = 1;
        dest[65] = 1;
        dest[66] = 1;
        dest[67] = 1;
        dest[68] = 0;
        dest[69] = 0;
        dest[70] = 0;
        dest[71] = 0;
        dest[72] = 0;
        dest[73] = 0;
        dest[74] = 0;
        dest[75] = 0;
        dest[150 - 0] = 67;
        dest[150 - 1] = 61;
        dest[150 - 2] = 57;
        dest[150 - 3] = 53;
        dest[150 - 4] = 50;
        dest[150 - 5] = 48;
        dest[150 - 6] = 45;
        dest[150 - 7] = 43;
        dest[150 - 8] = 41;
        dest[150 - 9] = 39;
        dest[150 - 10] = 37;
        dest[150 - 11] = 36;
        dest[150 - 12] = 34;
        dest[150 - 13] = 33;
        dest[150 - 14] = 31;
        dest[150 - 15] = 30;
        dest[150 - 16] = 29;
        dest[150 - 17] = 28;
        dest[150 - 18] = 27;
        dest[150 - 19] = 26;
        dest[150 - 20] = 25;
        dest[150 - 21] = 24;
        dest[150 - 22] = 23;
        dest[150 - 23] = 22;
        dest[150 - 24] = 21;
        dest[150 - 25] = 20;
        dest[150 - 26] = 19;
        dest[150 - 27] = 18;
        dest[150 - 28] = 17;
        dest[150 - 29] = 17;
        dest[150 - 30] = 16;
        dest[150 - 31] = 15;
        dest[150 - 32] = 14;
        dest[150 - 33] = 14;
        dest[150 - 34] = 13;
        dest[150 - 35] = 12;
        dest[150 - 36] = 12;
        dest[150 - 37] = 11;
        dest[150 - 38] = 10;
        dest[150 - 39] = 10;
        dest[150 - 40] = 9;
        dest[150 - 41] = 9;
        dest[150 - 42] = 8;
        dest[150 - 43] = 8;
        dest[150 - 44] = 7;
        dest[150 - 45] = 7;
        dest[150 - 46] = 6;
        dest[150 - 47] = 6;
        dest[150 - 48] = 6;
        dest[150 - 49] = 5;
        dest[150 - 50] = 5;
        dest[150 - 51] = 4;
        dest[150 - 52] = 4;
        dest[150 - 53] = 4;
        dest[150 - 54] = 3;
        dest[150 - 55] = 3;
        dest[150 - 56] = 3;
        dest[150 - 57] = 3;
        dest[150 - 58] = 2;
        dest[150 - 59] = 2;
        dest[150 - 60] = 2;
        dest[150 - 61] = 2;
        dest[150 - 62] = 1;
        dest[150 - 63] = 1;
        dest[150 - 64] = 1;
        dest[150 - 65] = 1;
        dest[150 - 66] = 1;
        dest[150 - 67] = 1;
        dest[150 - 68] = 0;
        dest[150 - 69] = 0;
        dest[150 - 70] = 0;
        dest[150 - 71] = 0;
        dest[150 - 72] = 0;
        dest[150 - 73] = 0;
        dest[150 - 74] = 0;
        dest[150 - 75] = 0;
    }

}

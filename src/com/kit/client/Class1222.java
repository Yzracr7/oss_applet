package com.kit.client;

public class Class1222 {

    static final void method132(byte var0) {
        try {
            for (int var1 = 0; var1 < Class113.anInt1552; ++var1) {
                --Class1008.anIntArray2157[var1];
                if (~Class1008.anIntArray2157[var1] > 9) {
                    --Class113.anInt1552;

                    //Move all up one
                    for (int var2 = var1; var2 < Class113.anInt1552; ++var2) {
                        Class1042_4.anIntArray2550[var2] = Class1042_4.anIntArray2550[var2 + 1];
                        Class945.aClass135Array2131[var2] = Class945.aClass135Array2131[var2 + 1];
                        Class166.anIntArray2068[var2] = Class166.anIntArray2068[1 + var2];
                        Class1008.anIntArray2157[var2] = Class1008.anIntArray2157[1 + var2];
                        Class3_Sub13_Sub6.anIntArray3083[var2] = Class3_Sub13_Sub6.anIntArray3083[var2 + 1];
                    }

                    --var1;
                } else {
                    Class135 var11 = Class945.aClass135Array2131[var1];
                    if (null == var11) {
                        var11 = Class135.method1811(Class961.cacheIndex4, Class1042_4.anIntArray2550[var1], 0);
                        if (null == var11) {
                            continue;
                        }

                        Class1008.anIntArray2157[var1] += var11.method1813();
                        Class945.aClass135Array2131[var1] = var11;
                    }

                    if (0 > Class1008.anIntArray2157[var1]) {
                        int var3;
                        if (~Class3_Sub13_Sub6.anIntArray3083[var1] != -1) {
                            int var4 = 128 * (255 & Class3_Sub13_Sub6.anIntArray3083[var1]);
                            int var7 = Class3_Sub13_Sub6.anIntArray3083[var1] >> 8 & 255;
                            int var5 = 255 & Class3_Sub13_Sub6.anIntArray3083[var1] >> 16;
                            int var8 = -Class945.thisClass946.x + 64 + 128 * var7;
                            if (var8 < 0) {
                                var8 = -var8;
                            }

                            int var6 = -Class945.thisClass946.y + 64 + var5 * 128;
                            if (0 > var6) {
                                var6 = -var6;
                            }

                            int var9 = -128 + var6 + var8;
                            if (~var4 > ~var9) {
                                Class1008.anIntArray2157[var1] = -100;
                                continue;
                            }

                            if (~var9 > -1) {
                                var9 = 0;
                            }

                            var3 = Class14.areaSoundsVolume * (var4 + -var9) / var4;
                        } else {
                            var3 = Class1048.soundEffectsVolume;
                        }

                        if (-1 > ~var3) {
                            System.out.println("var3(sound): " + var3);
                            Class3_Sub12_Sub1 var12 = var11.method1812().method151(Class27.aClass157_524);
                            Class3_Sub24_Sub1 var13 = Class3_Sub24_Sub1.method437(var12, 100, var3);
                            var13.method429(Class166.anIntArray2068[var1] + -1);
                            Class3_Sub26.aClass3_Sub24_Sub2_2563.method457(var13);
                        }

                        Class1008.anIntArray2157[var1] = -100;
                    }
                }
            }

            if (var0 != -92) {
                Class976.method126(true, 36, 42, 14, 51);
            }

            if (Class83.aBoolean1158 && !Class1220.method1391(-1)) {
                if (0 != Class9.musicVolume && Class963.currentSound != -1) {
                    System.out.println("3_music: " + Class963.currentSound);
                    Class70.method1285(Class75_Sub2.cacheIndex6, false, Class963.currentSound, 0, false, Class9.musicVolume);
                }

                Class83.aBoolean1158 = false;
            } else if (-1 != ~Class9.musicVolume && ~Class963.currentSound != 0 && !Class1220.method1391(var0 + 91)) {
                Class3_Sub13_Sub1.outputStream.putPacket(137);
                Class3_Sub13_Sub1.outputStream.method_211(Class963.currentSound);
                Class963.currentSound = -1;
            }

        } catch (RuntimeException var10) {
            throw Class1134.method1067(var10, "ed.C(" + var0 + ')');
        }
    }

    public static void method131(int var0) {
        try {
            Class976.aClass94_2304 = null;
            Class976.aClass94_2306 = null;
            if(var0 >= -104) {
                method132((byte)-28);
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ed.Q(" + var0 + ')');
        }
    }

    static final int method129(int var0, int var1, int var2, int var3) {
        try {
            if(var1 != 2) {
                method131(14);
            }

            if(-244 <= ~var0) {
                if(~var0 < -218) {
                    var2 >>= 3;
                } else if(var0 <= 192) {
                    if(179 < var0) {
                        var2 >>= 1;
                    }
                } else {
                    var2 >>= 2;
                }
            } else {
                var2 >>= 4;
            }

            return (var0 >> 1) + (var2 >> 5 << 7) + (var3 >> 2 << 10);
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "ed.D(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

}

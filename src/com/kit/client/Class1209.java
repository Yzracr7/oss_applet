package com.kit.client;

final class Class1209 {

    protected static int anInt820 = 0;
    protected int y;
    protected static Class1008 hasLoggedOutString = Class943.create(" has logged out)3");
    ;
    protected int size;
    protected static Class957 aClass3_Sub28_Sub16_824;
    protected static Class1008 aClass94_825 = Class943.create("purple:");
    ;
    protected static long[] friendsList = new long[200];
    protected static String[] clanMembersList = new String[Class922.CLAN_MEMBERS_SIZE];
    protected static int anInt828 = 0;
    protected static Class1008 nullString = Class943.create("null");
    protected int z;
    protected int x;
    protected static Class1008 aClass94_833 = Class943.create("purple:");
    ;

    static final void method1131(int plane, int var1, int var2, int var3, int localY, int var5, int localX, int var7) {
        try {
            if (localX >= 0 && localY >= 0 && localX < 104 && localY < 103) {
                int var9;
                if (-1 == ~var5) {
                    Class70 tile = Class154.getGroundTile(plane, localX, localY);
                    if (tile != null) {
                        var9 = Integer.MAX_VALUE & (int) (tile.aLong1048 >>> 32);
                        if (-3 == ~var3) {
                            tile.aClass140_1049 = new Class140_Sub3(var9, 2, 4 + var2, plane, localX, localY, var7, false, tile.aClass140_1049);
                            tile.aClass140_1052 = new Class140_Sub3(var9, 2, 3 & 1 + var2, plane, localX, localY, var7, false, tile.aClass140_1052);
                        } else {
                            tile.aClass140_1049 = new Class140_Sub3(var9, var3, var2, plane, localX, localY, var7, false, tile.aClass140_1049);
                        }
                    } else {
                    }
                }

                if (-2 == ~var5) {
                    Class19 var12 = Class1134.getGroundTile(plane, localX, localY);
                    if (null != var12) {
                        var9 = (int) (var12.aLong428 >>> 32) & Integer.MAX_VALUE;
                        if (-5 != ~var3 && -6 != ~var3) {
                            if (~var3 != -7) {
                                if (7 == var3) {
                                    var12.aClass140_429 = new Class140_Sub3(var9, 4, (var2 - -2 & 3) - -4, plane, localX, localY, var7, false, var12.aClass140_429);
                                } else if (var3 == 8) {
                                    var12.aClass140_429 = new Class140_Sub3(var9, 4, 4 + var2, plane, localX, localY, var7, false, var12.aClass140_429);
                                    var12.aClass140_423 = new Class140_Sub3(var9, 4, (2 + var2 & 3) + 4, plane, localX, localY, var7, false, var12.aClass140_423);
                                }
                            } else {
                                var12.aClass140_429 = new Class140_Sub3(var9, 4, var2 - -4, plane, localX, localY, var7, false, var12.aClass140_429);
                            }
                        } else {
                            var12.aClass140_429 = new Class140_Sub3(var9, 4, var2, plane, localX, localY, var7, false, var12.aClass140_429);
                        }
                    }
                }

                if (-3 == ~var5) {
                    if (~var3 == -12) {
                        var3 = 10;
                    }

                    Class25 var11 = Class75.method1336(plane, localX, localY);
                    if (var11 != null) {
                        var11.aClass140_479 = new Class140_Sub3((int) (var11.aLong498 >>> 32) & Integer.MAX_VALUE, var3, var2, plane, localX, localY, var7, false, var11.aClass140_479);
                    }
                }

                if (~var5 == -4) {
                    Class12 var13 = ByteBuffer.method784(plane, localX, localY);
                    if (null != var13) {
                        var13.aClass140_320 = new Class140_Sub3(Integer.MAX_VALUE & (int) (var13.aLong328 >>> 32), 22, var2, plane, localX, localY, var7, false, var13.aClass140_320);
                    }
                }
            }

            if (var1 <= 104) {
                method1132(-79);
            }

        } catch (RuntimeException var10) {
            throw Class1134.method1067(var10, "hd.B(" + plane + ',' + var1 + ',' + var2 + ',' + var3 + ',' + localY + ',' + var5 + ',' + localX + ',' + var7 + ')');
        }
    }

    static final void method1132(int var0) {
        try {
            Class128.aClass93_1683.clearSoftReference();
            if (var0 != 103) {
                method1132(14);
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "hd.A(" + var0 + ')');
        }
    }

    public static void method1133(byte var0) {
        try {
            aClass94_833 = null;
            hasLoggedOutString = null;
            nullString = null;
            if (var0 != 81) {
                aClass94_833 = (Class1008) null;
            }

            aClass94_825 = null;
            friendsList = null;
            aClass3_Sub28_Sub16_824 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "hd.C(" + var0 + ')');
        }
    }

    public Class1209() {
    }

    Class1209(Class1209 var1) {
        try {
            this.size = var1.size;
            this.x = var1.x;
            this.y = var1.y;
            this.z = var1.z;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "hd.<init>(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }
}

package com.kit.client;

final class Class27 {

    protected static int[] possibleSizes = new int[]{768, 1024, 1280, 512, 1536, 256, 0, 1792};
    protected static Class93 aClass93_511 = new Class93(30);
    protected static int[] anIntArray512 = new int[500];
    protected static Class1008 aClass94_514 = Class943.create(":duelstake:");
    protected static int clickedTileX = -1;
    protected static Class1008 aClass94_516 = Class943.create("unzap");
    protected static int anInt517 = 0;
    protected static Class1206_2 landscapeAsSprite;
    private static Class1008 aClass94_519 = Class943.create("skill: ");
    private static Class1008 aClass94_521 = Class943.create("scroll:");
    protected static Class1008 aClass94_522 = aClass94_519;
    protected static Class1008 aClass94_523 = aClass94_521;
    protected static Class157 aClass157_524;
    protected static Class1034 aClass11_526 = null;
    private static Class1008 aClass94_527 = Class943.create("level: ");
    protected static Class1008 aClass94_528 = aClass94_521;
    protected static Class1008 aClass94_525 = aClass94_527;

    static final int method961() {
        return (Class23.anInt453 == 0 ? 0 : Class2.anInterface5Array70[Class23.anInt453].method24());
    }

    public static void method962(byte var0) {
        try {
            aClass94_528 = null;
            aClass94_523 = null;
            aClass94_525 = null;
            anIntArray512 = null;
            aClass94_527 = null;
            landscapeAsSprite = null;
            if (var0 > -44) {
                aClass94_522 = (Class1008) null;
            }

            aClass11_526 = null;
            possibleSizes = null;
            aClass94_522 = null;
            aClass94_521 = null;
            aClass94_514 = null;
            aClass94_516 = null;
            aClass94_519 = null;
            aClass157_524 = null;
            aClass93_511 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "ef.A(" + var0 + ')');
        }
    }
}

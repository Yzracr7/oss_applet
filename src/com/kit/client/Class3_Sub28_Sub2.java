package com.kit.client;

final class Class3_Sub28_Sub2 extends Class1002 {

    protected static Class1008 aClass94_3541 = Class943.create("yellow:");
    protected static Class1008 aClass94_3543 = Class943.create("Loading config )2 ");
    protected static Class1008 loadingModelsString = Class943.create("Loading integer_233 )2 ");
    protected static Class1008 aClass94_3544 = aClass94_3541;
    protected Class1212 aClass140_Sub2_3545;
    protected static Class1008 aClass94_3546 = aClass94_3543;
    protected static Class1008 aClass94_3548 = aClass94_3541;

    public static void method534(int var0) {
        try {
            aClass94_3546 = null;
            aClass94_3548 = null;
            aClass94_3543 = null;
            loadingModelsString = null;
            int var1 = 101 % ((-29 - var0) / 45);
            aClass94_3544 = null;
            aClass94_3541 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "bk.B(" + var0 + ')');
        }
    }

    static final void method535(byte var0, int var1) {
        try {
            Class151.aFloatArray1934[0] = (float) Class951.method633(255, var1 >> 16) / 255.0F;
            Class151.aFloatArray1934[1] = (float) Class951.method633(var1 >> 8, 255) / 255.0F;
            Class151.aFloatArray1934[2] = (float) Class951.method633(255, var1) / 255.0F;
            Class1042_2.method383(3);
            Class1042_2.method383(4);
            if (var0 != 56) {
                method535((byte) 127, 99);
            }

        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "bk.A(" + var0 + ',' + var1 + ')');
        }
    }

    static final Class75_Sub3 method536(byte var0, ByteBuffer var1) {
        try {
            if (var0 != 54) {
                method534(117);
            }

            return new Class75_Sub3(var1.aLong_1884(), var1.aLong_1884(), var1.aLong_1884(), var1.aLong_1884(), var1.aLong_1884(), var1.aLong_1884(), var1.aLong_1884(), var1.aLong_1884(), var1.aBoolean183(), var1.readUnsignedByte());
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "bk.C(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ')');
        }
    }

    Class3_Sub28_Sub2(Class1212 var1) {
        try {
            this.aClass140_Sub2_3545 = var1;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "bk.<init>(" + (var1 != null ? "{...}" : "null") + ')');
        }
    }

}

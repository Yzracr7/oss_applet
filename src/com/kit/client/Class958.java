package com.kit.client;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.Attributes;

/**
 * Runs buffer jar application from any url. Usage is 'java JarRunner url [args..]'
 * where url is the url of the jar file and args is optional arguments to be
 * passed to the application's main method.
 */

public class Class958 {

    public static String clientURL;

    public static void main(String[] args) {
        System.setProperty("http.agent", "Chrome");
        URL url = null;
        try {
            url = new URL(getClientLink());
        } catch (Exception e) {
            fatal("Invalid URL: " + args[0]);
        }
        Class959 cl = new Class959(url);
        String name = null;
        try {
            name = cl.getMainClassName();
        } catch (IOException e) {
            System.err.println("I/O error while loading JAR file:");
            e.printStackTrace();
            System.exit(1);
        }
        if (name == null) {
            fatal("Specified jar file does not contain buffer 'Main-Class'"
                    + " manifest attribute");
        }
        String[] newArgs = new String[clientURL.length() - 1];
        try {
            cl.invokeClass(name, newArgs);
        } catch (ClassNotFoundException e) {
            fatal("Class not found: " + name);
        } catch (NoSuchMethodException e) {
            fatal("Class does not define buffer 'main' method: " + name);
        } catch (InvocationTargetException e) {
            e.getTargetException().printStackTrace();
            System.exit(1);
        }
    }

    public static String getClientLink() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        URL url = new URL("http://os-ps.org/config/config.xml");
        DocumentBuilder db = factory.newDocumentBuilder();
        Document doc = db.parse(url.openStream());
        NodeList nList = doc.getElementsByTagName("data");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                clientURL = eElement
                        .getElementsByTagName("alora_client_url")
                        .item(0)
                        .getTextContent();
            }
        }
        return clientURL;
    }

    private static void fatal(String s) {
        System.err.println(s);
        System.exit(1);
    }

    private static void usage() {
        fatal("Usage: java JarRunner url [args..]");
    }
}

/**
 * A class ex for loading jar files, both local and remote.
 */

class Class959 extends URLClassLoader {
    private URL url;

    /**
     * Creates buffer new Class959 for the specified url.
     *
     * @param url the url of the jar file
     */
    public Class959(URL url) {
        super(new URL[]{url});
        this.url = url;
    }

    /**
     * Returns the integer_34 of the jar file main class, or null if no "Main-Class"
     * manifest attributes was defined.
     */
    public String getMainClassName() throws IOException {
        URL u = new URL("jar", "", url + "!/");
        JarURLConnection uc = (JarURLConnection) u.openConnection();
        Attributes attr = uc.getMainAttributes();
        return attr != null ? attr.getValue(Attributes.Name.MAIN_CLASS) : null;
    }

    /**
     * Invokes the application in this jar file given the integer_34 of the main class
     * and an array of arguments. The class must define buffer static method "main"
     * which takes an array of String arguemtns and is of return type "void".
     *
     * @param name the integer_34 of the main class
     * @param args the arguments for the application
     * @throws ClassNotFoundException    if the specified class could not be found
     * @throws NoSuchMethodException     if the specified class does not contain buffer "main" method
     * @throws InvocationTargetException if the application raised an exception
     */
    public void invokeClass(String name, String[] args)
            throws ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException {
        Class c = loadClass(name);
        Method m = c.getMethod("main", new Class[]{args.getClass()});
        m.setAccessible(true);
        int mods = m.getModifiers();
        if (m.getReturnType() != void.class || !Modifier.isStatic(mods)
                || !Modifier.isPublic(mods)) {
            throw new NoSuchMethodException("main");
        }
        try {
            m.invoke(null, new Object[]{args});
        } catch (IllegalAccessException e) {
            // This should not happen, as we have disabled access checks
        }
    }

}
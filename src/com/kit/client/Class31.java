package com.kit.client;
import javax.media.opengl.GL;

final class Class31 {

   static int anInt580 = 0;
   private static Class975 aClass61_581 = new Class975();
   static int anInt582 = 0;
   private static long lastGcTime = 0L;
   static int anInt584 = 0;
   static int anInt585 = 0;
   private static Class975 aClass61_586 = new Class975();
   private static Class975 aClass61_587 = new Class975();
   private static Class975 aClass61_588 = new Class975();
   private static int[] anIntArray589 = new int[1000];

   static final synchronized void method985(int var0, int var1, int var2) {
      if(var2 == anInt582) {
         Class1042_2 var3 = new Class1042_2(var1);
         var3.hash = (long)var0;
         aClass61_587.insertBack(var3);
      }
   }

   static final synchronized void method986(int var0, int var1) {
      if(var1 == anInt582) {
         Class1042_2 var2 = new Class1042_2();
         var2.hash = (long)var0;
         aClass61_588.insertBack(var2);
      }
   }

   public static void method987() {
      aClass61_581 = null;
      aClass61_586 = null;
      aClass61_587 = null;
      aClass61_588 = null;
      anIntArray589 = null;
   }

   static final synchronized void method988() {
      ++anInt582;
      aClass61_581.clear();
      aClass61_586.clear();
      aClass61_587.clear();
      aClass61_588.clear();
      anInt585 = 0;
      anInt584 = 0;
      anInt580 = 0;
   }

   static final synchronized void method989(int var0, int var1, int var2) {
      if(var2 == anInt582) {
         Class1042_2 var3 = new Class1042_2(var1);
         var3.hash = (long)var0;
         aClass61_581.insertBack(var3);
      }
   }

   static final synchronized void method990() {
      GL var0 = Class1012.gl;
      int var1 = 0;

      while(true) {
         Class1042_2 var2 = (Class1042_2)aClass61_581.popFront();
         if(var2 == null) {
            if(var1 > 0) {
               var0.glDeleteBuffersARB(var1, anIntArray589, 0);
               var1 = 0;
            }

            while(true) {
               var2 = (Class1042_2)aClass61_586.popFront();
               if(var2 == null) {
                  while(true) {
                     var2 = (Class1042_2)aClass61_587.popFront();
                     if(var2 == null) {
                        if(var1 > 0) {
                           var0.glDeleteTextures(var1, anIntArray589, 0);
                        }

                        while(true) {
                           var2 = (Class1042_2)aClass61_588.popFront();
                           if(var2 == null) {
                              if(anInt585 + anInt584 + anInt580 > 100663296 && Class1219.currentTimeMillis() > lastGcTime + 60000L) {
                                 System.gc();
                                 lastGcTime = Class1219.currentTimeMillis();
                              }

                              return;
                           }

                           int var3 = (int)var2.hash;
                           var0.glDeleteLists(var3, 1);
                        }
                     }

                     anIntArray589[var1++] = (int)var2.hash;
                     anInt580 -= var2.value;
                     if(var1 == 1000) {
                        var0.glDeleteTextures(var1, anIntArray589, 0);
                        var1 = 0;
                     }
                  }
               }

               anIntArray589[var1++] = (int)var2.hash;
               anInt584 -= var2.value;
               if(var1 == 1000) {
                  var0.glDeleteTextures(var1, anIntArray589, 0);
                  var1 = 0;
               }
            }
         }

         anIntArray589[var1++] = (int)var2.hash;
         anInt585 -= var2.value;
         if(var1 == 1000) {
            var0.glDeleteBuffersARB(var1, anIntArray589, 0);
            var1 = 0;
         }
      }
   }

   static final synchronized void method991(int var0, int var1, int var2) {
      if(var2 == anInt582) {
         Class1042_2 var3 = new Class1042_2(var1);
         var3.hash = (long)var0;
         aClass61_586.insertBack(var3);
      }
   }

}

package com.kit.client;

import java.text.NumberFormat;

import com.kit.client.Class922.GameState;

final class Class47 {

    static boolean aBoolean742 = false;
    static Class93 aClass93_743 = new Class93(20);
    private Class1002 aClass3_Sub28_744 = new Class1002();
    private Class1017_2 aClass130_745;
    private int anInt746;
    private Class1245 aClass13_747 = new Class1245();
    static Class1027 aClass153_748;
    private int anInt749;
    static Class1008 aClass94_750 = Class943.create("null");


	/*static final boolean method1088() {
        if (Class3_Sub28_Sub11.aBoolean3641) {
			try {
				Class8.aClass94_106.method1577(-1857, Class38.gameClass942.thisApplet);
				return true;
			} catch (Throwable var2) {
				;
			}
		}
		return false;
	}*/

    static final Class955 method1089(Class1027 skeletons, boolean var1, Class1027 skins, int var4) {
        boolean var5 = true;
        int[] var6 = skeletons.getChildEntries(var4);

        for (int var7 = 0; var7 < var6.length; ++var7) {
            byte[] var8 = skeletons.method2140(var6[var7], var4, 0);
            if (var8 == null) {
                var5 = false;
            } else {
                int var9 = (255 & var8[0]) << 8 | var8[1] & 255;
                byte[] var10;
                if (!var1) {
                    var10 = skins.method2140(0, var9, 0);
                } else {
                    var10 = skins.method2140(var9, 0, 0);
                }

                if (null == var10) {
                    var5 = false;
                }
            }
        }

        if (!var5) {
            return null;
        } else {
            try {
                return new Class955(skeletons, skins, var4, var1);
            } catch (Exception var11) {
                return null;
            }
        }
    }

    static final Class1008 createBlankJString(int var1) {
        Class1008 var2 = new Class1008();
        var2.pos = 0;
        var2.buf = new byte[var1];
        return var2;
    }

    static final void method1091(boolean var0) {
        byte var2;
        byte[][] var3;
        if (Class1012.aBoolean_617 && var0) {
            var2 = 1;
            var3 = Class921.aByteArrayArray3669;
        } else {
            var2 = 4;
            var3 = Class164_Sub2.aByteArrayArray3027;
        }

        int var4 = var3.length;

        int var5;
        int var6;
        int var7;
        byte[] var8;
        for (var5 = 0; ~var4 < ~var5; ++var5) {
            var6 = -Class131.anInt1716 + 64 * (Class3_Sub24_Sub3.anIntArray3494[var5] >> 8);
            var7 = -SpriteDefinition.anInt1152 + 64 * (255 & Class3_Sub24_Sub3.anIntArray3494[var5]);
            var8 = var3[var5];
            if (null != var8) {
                Class58.method1194();
                ByteBuffer.method777(Class930.class972, var0, -48 + 8 * Class956.anInt3606, var7, 4, var6, (Class419.anInt2294 + -6) * 8, var8);
            }
        }

        var5 = 0;

        for (; ~var4 < ~var5; ++var5) {
            var6 = -Class131.anInt1716 + 64 * (Class3_Sub24_Sub3.anIntArray3494[var5] >> 8);
            var7 = -SpriteDefinition.anInt1152 + 64 * (255 & Class3_Sub24_Sub3.anIntArray3494[var5]);
            var8 = var3[var5];
            if (var8 == null && ~Class419.anInt2294 > -801) {
                Class58.method1194();

                for (int var9 = 0; var9 < var2; ++var9) {
                    Class12.method870(var9, (byte) 102, var7, var6, 64, 64);
                }
            }
        }
    }

    final Class1002 getCS2(long var1, int var3) {
        try {
            if (var3 != 1400) {
                this.anInt749 = -79;
            }

            Class1002 var4 = (Class1002) this.aClass130_745.get(var1);
            if (null != var4) {
                this.aClass13_747.insertLast(var4);
            }

            return var4;
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "gn.N(" + var1 + ',' + var3 + ')');
        }
    }

    static final void method1093() {
        for (int var1 = 0; var1 < 100; ++var1) {
            Class921.interfacesToRefresh[var1] = true;
        }
    }

    final Class1042 method1094(int var1) {
        try {
            return var1 != 0 ? (Class1042) null : this.aClass130_745.getFirst();
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "gn.A(" + var1 + ')');
        }
    }

    static final void drawInterface(int x, int var1, int var2, Class1034[] var3, int width, int var5, int y, int height, byte var8, int var9, int actualInter) {
        int var222 = 0;
        int var33 = 550;
        if (actualInter == -1 || actualInter == 381 || actualInter == 548 || actualInter == 599

                || actualInter == 378 || actualInter == 650 || actualInter == 17 //welcome screen + box
                && Class922.clientSize > 0) { //wildy inter / gameframe / gwd killcount
            width = Class922.resizeWidth;
            height = Class922.resizeHeight;
            x = 0;
            y = 0;
            //Setting the clipping
        }


        if (Class1012.aBoolean_617) {
            Class920.method_576(x, y, width, height);
        } else {
            Class1023.clipRect(x, y, width, height);
            Rasterizer.method1134();
        }


        //TODO: original bank arrays for ids and amounts
        for (int var10 = 0; ~var3.length < ~var10; ++var10) {
            Class1034 var11 = var3[var10];

            //if(var11 != null) {
            if (var11.uid == Class922.newScrollerToReset) {
                if (var10 == 1 || var10 == 2)
                    var11.newScrollerPos = 16;
                else if (var10 == 3) {
                    //System.out.println("before offset: " + var11.newScrollerPos);
                    var11.newScrollerPos = var11.uid == 17957003 ? 120 : 27;
                    Class922.newScrollerToReset = -1;
                }
                //System.out.println("index: " + var10 + ", offset" + var11.newScrollerPos);
            }

            //Redstones 464
            if (var11.disabledSprite >= 305 && var11.disabledSprite <= 307)
                continue;

            if (Class922.hideFSTabs && Class922.clientSize > 0
                    && (
                    (var11.disabledSprite >= 1065 && var11.disabledSprite <= 1069) //redstones
                            || (var11.disabledSprite >= 898 && var11.disabledSprite <= 909) //tabstones
                            || var11.disabledSprite == 895 //clan chat
                            || var11.disabledSprite == 168 //combat options
                            || var11.disabledSprite == 911 //sideicons
                            || var11.parent == Class922.NEW_BOTTOM_CLICKAREA
                            || var11.parent == Class922.NEW_TOP_CLICKAREA))
                continue;
            else if (Class922.clientSize > 0 && Class922.inventoryHidden && (var11.disabledSprite >= 1065 && var11.disabledSprite <= 1069))
                continue; //redstones when invy hidden

            if (var11 != null && (~var11.parent == ~var5 || var5 == -1412584499 && var11 == Class56.aClass11_886)) {
                if (actualInter == 601) {

                }
                int var12;
                if (0 != ~var9) {
                    var12 = var9;
                } else {
                    Class155.anIntArray1969[Class3_Sub28_Sub3.anInt3557] = var2 + var11.anInt306;
                    Class946.anIntArray3954[Class3_Sub28_Sub3.anInt3557] = var11.newScrollerPos - -var1;
                    Class3_Sub28_Sub18.anIntArray3768[Class3_Sub28_Sub3.anInt3557] = var11.scrollbarWidth;
                    Class991.anIntArray2794[Class3_Sub28_Sub3.anInt3557] = var11.scrollbarHeight;
                    var12 = Class3_Sub28_Sub3.anInt3557++;
                }

                var11.anInt204 = Class1134.loopCycle;
                var11.anInt292 = var12;
                if (!var11.scriptedInterface || !Class1034.isComponentHidden(var11)) {
                    if (0 < var11.clientCode) {
                        Class2.method81((byte) -128, var11);
                    }


                    int var14 = var1 + var11.newScrollerPos;
                    int var15 = var11.alpha;
                    int var13 = var11.anInt306 - -var2;
                    if (Class1043.qaoptestEnabled && (0 != Class1034.getInterfaceClickMask(var11).clickMask || var11.type == 0) && var15 > 127) {
                        var15 = 127;
                    }


                    //  if(var11.type == 0) {//container
                    //	  if(var11.uid == client.NEW_TOP_CLICKAREA || var11.uid == client.NEW_BOTTOM_CLICKAREA) {
                    //  		  client.tabstonesFsSprite.drawSprite(var11.x, var11.y);
                    //	  }
                    //  }
                    int var17;
                    int var16;
                    if (var11 == Class56.aClass11_886) {
                        if (var5 != -1412584499 && !var11.aBoolean200) {
                            Class956.anInt3602 = var2;
                            Class943.anInt1082 = var1;
                            Class1031.aClass11Array1836 = var3;
                            continue;
                        }

                        if (Class1001.aBoolean3975 && Class85.aBoolean1167) {
                            var17 = Class1017_2.anInt1709;
                            var16 = Class1015.mouseX2;
                            var17 -= Class95.anInt1336;
                            if (var17 < Class134.anInt1761) {
                                var17 = Class134.anInt1761;
                            }

                            if (~(var17 + var11.scrollbarHeight) < ~(Class979.aClass11_88.scrollbarHeight + Class134.anInt1761)) {
                                var17 = -var11.scrollbarHeight + Class979.aClass11_88.scrollbarHeight + Class134.anInt1761;
                            }

                            var14 = var17;
                            var16 -= Class144.anInt1881;
                            if (Class3_Sub13_Sub13.anInt3156 > var16) {
                                var16 = Class3_Sub13_Sub13.anInt3156;
                            }

                            if (~(Class979.aClass11_88.scrollbarWidth + Class3_Sub13_Sub13.anInt3156) > ~(var11.scrollbarWidth + var16)) {
                                var16 = -var11.scrollbarWidth + Class979.aClass11_88.scrollbarWidth + Class3_Sub13_Sub13.anInt3156;
                            }

                            var13 = var16;
                        }

                        if (!var11.aBoolean200) {
                            var15 = 128;
                        }
                    }

                    int var19;
                    int var18;
                    int var21;
                    int var20;
                    if (2 != var11.type) {
                        var17 = ~var14 < ~y ? var14 : y;
                        var16 = ~x > ~var13 ? var13 : x;
                        var20 = var11.scrollbarWidth + var13;
                        var21 = var14 - -var11.scrollbarHeight;
                        if (~var11.type == -10) {
                            ++var21;
                            ++var20;
                        }

                        var19 = height <= var21 ? height : var21;
                        var18 = ~var20 <= ~width ? width : var20;
                    } else {
                        var19 = height;
                        var18 = width;
                        var17 = y;
                        var16 = x;
                    }

                    if (!var11.scriptedInterface || ~var18 < ~var16 && ~var17 > ~var19) {
                        int var23;
                        int var22;
                        int var25;
                        int var24;
                        int var26;
                        int var29;
                        int var28;
                        int var47;
                        if (~var11.clientCode != -1) {
                            if (~var11.clientCode == -1338 || var11.clientCode == 1403 && Class1012.aBoolean_617) {
                                Class1223.aClass11_2091 = var11;
                                Class1002.anInt2567 = var14;
                                Class53.anInt865 = var13;
                                Class3_Sub13_Sub36.method338(var11.scrollbarHeight, ~var11.clientCode == -1404, var13, var11.scrollbarWidth, var14);
                                if (Class1012.aBoolean_617) {
                                    Class920.method_576(x, y, width, height);
                                } else {
                                    Class1023.clipRect(x, y, width, height);
                                }
                                continue;
                            }

                            if (var11.clientCode == 1337) { //game drawing
                                System.out.println("test");

                            }

                            if (var11.clientCode == 1338) {
                                if (Class929.aInteger_510 < 498) {

                                    Class976.method_946(var11, var12, var14, var13);

                                    if (Class1012.aBoolean_617) {
                                        Class920.method_576(x, y, width, height);
                                    } else {
                                        Class1023.clipRect(x, y, width, height);
                                    }

                                    if (0 != Class161.anInt2028 && 3 != Class161.anInt2028 || Class38_Sub1.drawContextMenu || var16 > Class981.clickX || ~Class38_Sub1.clickY > ~var17 || ~Class981.clickX <= ~var18 || ~var19 >= ~Class38_Sub1.clickY) {
                                        continue;
                                    }

                                    var20 = Class981.clickX - var13 - 25;
                                    var21 = -var14 + Class38_Sub1.clickY;
                                    var22 = Class922.mapbackSource[var21];
                                    if (~var20 > ~var22 || ~var20 < ~(var22 + Class922.mapbackDest[var21])) {
                                        continue;
                                    }

                                    var20 += 15;

                                    var21 -= var11.scrollbarHeight / 2;
                                    var23 = 2047 & Class1211.cameraYaw + Class3_Sub13_Sub8.anInt3102;
                                    var20 -= var11.scrollbarWidth / 2;
                                    var24 = Rasterizer.sineTable[var23];
                                    var25 = Rasterizer.cosineTable[var23];
                                    var24 = (Class164_Sub2.anInt3020 + 256) * var24 >> 8;
                                    var25 = (Class164_Sub2.anInt3020 + 256) * var25 >> 8;
                                    var47 = -(var24 * var20) + var25 * var21 >> 11;
                                    var26 = var21 * var24 + (var20 * var25) >> 11;
                                    var28 = Class945.thisClass946.y + var26 >> 7;
                                    var29 = -var47 + Class945.thisClass946.x >> 7;

                                    if (Class1031.aBoolean1837 && 0 != (Class164.anInt2051 & 64)) {
                                        Class1034 var53 = Class957.method638(Class1045.anInt872, Class1034.anInt278);
                                        if (null != var53) {
                                            Class1045.passContextOptions((short) 11, Class144.anInt1887, 1L, Class131.aClass94_1724, var28, Class3_Sub28_Sub9.aClass94_3621, var29);
                                        } else {
                                            Class25.method958((byte) -91);
                                        }
                                        continue;
                                    }

                                    Class1045.passContextOptions((short) 60, -1, 1L, Class922.BLANK_CLASS_1008, var28, Class3_Sub13_Sub28.aClass94_3353, var29);
                                }
                                continue;
                            }
                            /** Draw on gameframe here **/
                            if (Class922.aInteger_544 == GameState.LOGGED_IN) {
                                if (Class922.clientSize == 0) {
                                    Class922.drawRedstones();
                                    Class976.drawSideIcons();
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("All")), 32, 495, 16777215, 0);
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("Report")), 459, 495, 16777215, 0);
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("Game")), 99, 490, 16777215, 0);
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("Public")), 165, 490, 16777215, 0);
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("Private")), 231, 490, 16777215, 0);
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("Clan")), 297, 490, 16777215, 0);
                                    Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("Trade")), 363, 490, 16777215, 0);
                                    Class922.compassBg.drawSprite(539, 0);
                                    Class922.worldmap0.drawSprite(712, 127);
                                    Class922.aboveCompass.drawSprite(516, 1);
                                    if (Class922.mouseX >= 714 && Class922.mouseX <= 744 && Class922.mouseY >= 129 && Class922.mouseY <= 155) {
                                        Class922.worldmap2.drawSprite(716, 131);
                                    } else {
                                        Class922.worldmap1.drawSprite(716, 131);
                                    }
                                    Class976.method_943(var33, var222 - 35);
                                    Class976.method_944(var33, var222);
                                } else {
                                    if (Class976.xpCounterActive()) {
                                        int i = Class976.xpCounter >= Integer.MAX_VALUE
                                                ? Class922.getSmallClass1019().getTextWidth("Lots!") - Class922.getSmallClass1019().getTextWidth("Lots!") / 2
                                                : Class922.getSmallClass1019().getTextWidth(Integer.toString(Class976.xpCounter))
                                                - Class922.getSmallClass1019().getTextWidth(Integer.toString(Class976.xpCounter)) / 2;
                                        Class922.xp_counter.drawTransparentSprite(Class922.resizeWidth - 350, var2 + 3, 130);
                                        Class922.xp_counter_2.drawSprite(Class922.resizeWidth - 322, var2 + 5);
                                        Class922.getSmallClass1019().drawText(Class943.create(Class47.fixJString("" + NumberFormat.getInstance().format(Class976.xpCounter))), Class976.xpCounter == 0 ? Class922.resizeWidth - 235 - i - Class976.digits : Class922.resizeWidth - 239 - i - Class976.digits, 21, 16777215, 0);
                                    }
                                    Class922.osrsChannelBackground.drawSprite(1, Class922.resizeHeight - 62);
                                    Class922.mapbackFsSprite.drawSprite(Class922.resizeWidth - 192/* - 32*/, var2);
                                    Class922.worldmap0.drawSprite(Class922.resizeWidth - 15, 141);
                                    Class922.worldmap1.drawSprite(Class922.resizeWidth - 19, 144);
                                }
                            }
                            if (var11.clientCode == 6969) { //New gameframe map
                                if (Class929.aInteger_510 >= 498 || Class922.clientSize > 0) { //525 + 562
                                    //   System.out.println(var14+":"+var15+":"+var13);
                                    //System.out.println(x+":"+y+":"+width+":"+height+":"+client.mapbackSprte.width+":"+client.mapbackSprte.height);
                                        /*if(Class1012.aBoolean_617) {
                                Class920.method_576(var13, var14, var13 + client.mapbackSprte.anInt1461, var14 + client.mapbackSprte.anInt1468);
                             } else {
                                Class1023.method_576(var13, var14, var13 + client.mapbackSprte.anInt1461, var14 + client.mapbackSprte.anInt1468);
                             }

                            if(2 != Class161.anInt2028 && 5 != Class161.anInt2028) {
                                if(Class1012.aBoolean_617) {
                                    Class920.method_574(var13 + 97, var14 + 82 - 4, 3, 3, 16777215);
                                 } else {
                                    Class1023.method_574(var13 + 97, var14 + 82 - 4, 3, 3, 16777215);
                                 }
                            } else {
                            	  if(Class1012.aBoolean_617) {
                            		  Class920.method926(client.mapbackSource, var13 + 25, var14 + 5, 0, 0);//var13 + 97, var14 + 82 - 4, 3, 3, 16777215);
                            	  } else {
                            		  Class1023.method1332(var13 + 25, var14 + 5, 0, client.mapbackSource, client.mapbackDest);
                            	  }
                            }

                            if(Class1017_2.aBooleanArray1712[var12])
                            	client.mapbackSprte.method1667(var13, var14);

                            if(Class1012.aBoolean_617) {
                                Class920.method_576(x, y, width, height);
                             } else {
                                Class1023.method_576(x, y, width, height);
                             }*/
                                    // if(!var11.method855(-30721)) {
                                    //   continue;
                                    //}


                                    Class976.method_946(var11, var12, var14, var13);
                                    if (Class1012.aBoolean_617) {
                                        Class920.method_576(x, y, width, height);
                                    } else {
                                        Class1023.clipRect(x, y, width, height);
                                    }

                                    if (0 != Class161.anInt2028 && 3 != Class161.anInt2028 || Class38_Sub1.drawContextMenu || var16 > Class981.clickX || ~Class38_Sub1.clickY > ~var17 || ~Class981.clickX <= ~var18 || ~var19 >= ~Class38_Sub1.clickY) {
                                        continue;
                                    }

                                    var20 = Class981.clickX - var13 - 25;
                                    var21 = -var14 + Class38_Sub1.clickY - 3;

                                    if (var21 >= Class14.dest.length)
                                        var21 = Class14.dest.length - 1;
                                    if (var21 <= 0)
                                        var21 = 0;
                                    //     var22 = client.mapbackSource[var21];
                                    var22 = Class14.dest[var21];
                                    if (~var20 > ~var22 || ~var20 < ~(var22 + /*client.mapbackDest[var21]*/ Class14.src[var21])) {
                                        continue;
                                    }

                                    var20 += 15;

                                    var21 -= var11.scrollbarHeight / 2;
                                    var23 = 2047 & Class1211.cameraYaw + Class3_Sub13_Sub8.anInt3102;
                                    var20 -= var11.scrollbarWidth / 2;
                                    var24 = Rasterizer.sineTable[var23];
                                    var25 = Rasterizer.cosineTable[var23];
                                    var24 = (Class164_Sub2.anInt3020 + 256) * var24 >> 8;
                                    var25 = (Class164_Sub2.anInt3020 + 256) * var25 >> 8;
                                    var47 = -(var24 * var20) + var25 * var21 >> 11;
                                    var26 = var21 * var24 + (var20 * var25) >> 11;
                                    var28 = Class945.thisClass946.y + var26 >> 7;
                                    var29 = -var47 + Class945.thisClass946.x >> 7;
                                    if (Class1031.aBoolean1837 && 0 != (Class164.anInt2051 & 64)) {
                                        Class1034 var53 = Class957.method638(Class1045.anInt872, Class1034.anInt278);
                                        if (null != var53) {
                                            Class1045.passContextOptions((short) 11, Class144.anInt1887, 1L, Class131.aClass94_1724, var28, Class3_Sub28_Sub9.aClass94_3621, var29);
                                        } else {
                                            Class25.method958((byte) -91);
                                        }
                                        continue;
                                    }
                                    Class1045.passContextOptions((short) 60, -1, 1L, Class922.BLANK_CLASS_1008, var28, Class3_Sub13_Sub28.aClass94_3353, var29);
                                }
                                continue;
                                //end 6969
                            }

								/*if(var11.clientCode == 1339) {
                           if(var11.method855(-30721)) {
                              Class972.method1493(var13, var14, var11, var12, (byte)59);
                              if(!Class1012.aBoolean_617) {
                                 Class1023.method_576(x, y, width, height);
                              } else {
                                 Class920.method_576(x, y, width, height);
                              }
                           }
                           continue;
                        }*/

								/*if(var11.clientCode == 1400) {
                           ByteBuffer.method799(var13, 64, var14, var11.anInt193, var11.scrollbarWidth);
                           Class921.aBooleanArray3674[var12] = true;
                           Class163_Sub1_Sub1.aBooleanArray4008[var12] = true;
                           if(Class1012.aBoolean_617) {
                              Class920.method_576(x, y, width, height);
                           } else {
                              Class1023.method_576(x, y, width, height);
                           }
                           continue;
                        }*/

								/* if(-1402 == ~var11.clientCode) {
                           Class1.method72(var13, var11.anInt193, var11.scrollbarWidth, 19481, var14);
                           Class921.aBooleanArray3674[var12] = true;
                           Class163_Sub1_Sub1.aBooleanArray4008[var12] = true;
                           if(!Class1012.aBoolean_617) {
                              Class1023.method_576(x, y, width, height);
                           } else {
                              Class920.method_576(x, y, width, height);
                           }
                           continue;
                        }*/

								/*if(1402 == var11.clientCode) {
                           if(!Class1012.aBoolean_617) {
                              Class963.method1768(var13, 95, var14);
                              Class921.aBooleanArray3674[var12] = true;
                              Class163_Sub1_Sub1.aBooleanArray4008[var12] = true;
                           }
                           continue;
                        }*/

                            //FPS below
                            if (~var11.clientCode == -1406) {
                                    /*    if(!Class20.aBoolean438) {
                              continue;
                           }

                           var20 = var11.scrollbarWidth + var13;
                           var21 = 15 + var14;
                           client.getRegularClass1019().method699(Class1008.createJString("Fps: " + Class72.method1298(Class954.anInt1862)), var20, var21, 16776960, 0);
                           //client.getRegularClass1019().method688(new Class1008[]{Class3_Sub13_Sub16.aClass94_3196, Class72.method1298(Class954.anInt1862)}, (byte)-69), var20, var21, 16776960, -1);
                           var21 += 15;
                           Runtime var57 = Runtime.getRuntime();
                           var23 = (int)((var57.totalMemory() + -var57.freeMemory()) / 1024L);
                           var24 = 16776960;
                           if(~var23 < -65537) {
                              var24 = 16711680;
                           }
                           client.getRegularClass1019().method699(Class1008.createJString("Mem: " + Class72.method1298(var23) + "K"), var20, var21, var24, 0);
//                           client.getRegularClass1019().method688(client.method903(new Class1008[]{Class3_Sub28_Sub10_Sub1.aClass94_4057, Class72.method1298(var23), Class151_Sub1.aClass94_2951}, (byte)-128), var20, var21, var24, -1);
                           var21 += 15;
                     //      if(Class1012.aBoolean_617) {
                              var24 = 16776960;
                              var25 = (Class31.anInt580 + Class31.anInt585 + Class31.anInt584) / 1024;
                              if(65536 < var25) {
                                 var24 = 16711680;
                              }
                              client.getRegularClass1019().method699(Class1008.createJString("Cache: " + Class72.method1298(var25) + " (" + Class1009.aClass94_2951 +")"), var20, var21, var24, 0);
             //                 client.getRegularClass1019().method688(client.method903(new Class1008[]{Class118.aClass94_1622, Class72.method1298(var25), Class151_Sub1.aClass94_2951}, (byte)-97), var20, var21, var24, -1);
                              var21 += 15;
                      //     }

/*                           var24 = 16776960;
                           var25 = 0;
                           var47 = 0;
                           var26 = 0;

                           for(var28 = 0; var28 < client.aInteger_512; ++var28) {
                              var25 += ByteBuffer.aClass151_Sub1Array2601[var28].method2108((byte)1);
                              var26 += ByteBuffer.aClass151_Sub1Array2601[var28].method2102(0);
                              var47 += ByteBuffer.aClass151_Sub1Array2601[var28].method2106(1);
                           }

                           var29 = 10000 * var26 / var25;
                           var28 = var47 * 100 / var25;
                           Class1008 var55 = client.method903(new Class1008[]{Class20.aClass94_436, Class3_Sub23.method407(0, true, 2, (long)var29, 2), Class3_Sub21.aClass94_2498, Class72.method1298(var28), Class10.aClass94_148}, (byte)-113);
                           client.getSmallClass1019().method688(var55, var20, var21, var24, -1);
                           var21 += 12;
                           Class3_Sub28_Sub14.aBooleanArray3674[var12] = true;
                           Class163_Sub1_Sub1.aBooleanArray4008[var12] = true;
                           continue;
                        }*/
                            }
                            if (-1407 == ~var11.clientCode) {
                                Class1244.anInt2115 = var14;
                                Class957.aClass11_3708 = var11;
                                Class3_Sub13_Sub23_Sub1.anInt4041 = var13;
                                continue;
                            }
                        }

                        if (!Class38_Sub1.drawContextMenu) {
                            if (~var11.type == -1 && var11.aBoolean219 && Class981.clickX >= var16 && ~Class38_Sub1.clickY <= ~var17 && Class981.clickX < var18 && ~var19 < ~Class38_Sub1.clickY && !Class1043.qaoptestEnabled) {
                                Class3_Sub13_Sub34.contextOptionsAmount = 1;
                                Class114.anIntArray1578[0] = Class955.anInt3590;
                                Class1013.aClass94Array2935[0] = Class161.aClass94_2031;
                                Class163_Sub2_Sub1.contextOpStrings[0] = Class922.BLANK_CLASS_1008;
                                Class3_Sub13_Sub7.aShortArray3095[0] = 1005;
                            }

                            if (var16 <= Class981.clickX && ~var17 >= ~Class38_Sub1.clickY && var18 > Class981.clickX && ~var19 < ~Class38_Sub1.clickY) {
                                Class3_Sub24_Sub4.createItemContext(var11, Class38_Sub1.clickY + -var14, -var13 + Class981.clickX);
                            }
                        }

                        if (-1 == ~var11.type) {
                            if (!var11.scriptedInterface && Class1034.isComponentHidden(var11) && Class107.aClass11_1453 != var11) {
                                continue;
                            }

                            if (!var11.scriptedInterface) {
                                if (~(-var11.scrollbarHeight + var11.maxScrollVertical) > ~var11.scrollPosition) {
                                    var11.scrollPosition = -var11.scrollbarHeight + var11.maxScrollVertical;
                                }

                                if (0 > var11.scrollPosition) {
                                    var11.scrollPosition = 0;
                                }
                            }

                            drawInterface(var16, -var11.scrollPosition + var14, -var11.anInt247 + var13, var3, var18, var11.uid, var17, var19, (byte) 87, var12, actualInter);
                            if (null != var11.aClass11Array262) {
                                drawInterface(var16, -var11.scrollPosition + var14, -var11.anInt247 + var13, var11.aClass11Array262, var18, var11.uid, var17, var19, (byte) 52, var12, actualInter);
                            }

                            Class1207 var36 = (Class1207) Class3_Sub13_Sub17.aClass130_3208.get((long) var11.uid);
                            if (var36 != null) {
                                if (var36.type == 0 && !Class38_Sub1.drawContextMenu && Class981.clickX >= var16 && ~var17 >= ~Class38_Sub1.clickY && ~var18 < ~Class981.clickX && Class38_Sub1.clickY < var19 && !Class1043.qaoptestEnabled) {
                                    Class1013.aClass94Array2935[0] = Class161.aClass94_2031;
                                    Class3_Sub13_Sub34.contextOptionsAmount = 1;
                                    Class114.anIntArray1578[0] = Class955.anInt3590;
                                    Class3_Sub13_Sub7.aShortArray3095[0] = 1005;
                                    Class163_Sub2_Sub1.contextOpStrings[0] = Class922.BLANK_CLASS_1008;
                                }

                                Class3_Sub13_Sub1.method171(-101, var36.uid, var16, var18, var13, var12, var19, var17, var14);
                            }

                            if (Class1012.aBoolean_617) {
                                Class920.method_576(x, y, width, height);
                            } else {
                                Class1023.clipRect(x, y, width, height);
                                Rasterizer.method1134();
                            }
                        }


                        //System.out.println("hash: " + Class1017_2.aBooleanArray1712[var12] + ", cs2: " + Class951.anInt3689);
                        if (Class1017_2.aBooleanArray1712[var12] || ~Class951.anInt3689 < -2) {

                            if (-1 == ~var11.type && !var11.scriptedInterface && var11.maxScrollVertical > var11.scrollbarHeight) {

                                Class3_Sub13_Sub12.drawOldScroller(var11.scrollPosition, var11.maxScrollVertical, var11.scrollbarWidth + var13, var14, var11.scrollbarHeight);
                            }

                            //TODO:
                            // if(client.clientSize > 0) {
                            //   Class1023.drawAlphaGradient2(16, 210, 505, 130, 0, 0, 70);
                            //}
                            //Actually what we should do is change when drawing the chatbox sprite

                            if (var11.type != 1) {

                                boolean var39;
                                boolean var46;
                                if (-3 == ~var11.type) {
                                    var20 = 0;

                                    int bankTabHeaderMove = -2;
                                    //TODO: search function
                                    //if(client.searchString.contains(item)) show else

                                    //  String chatboxText = Class7.getInterface(8978433).disabledText.toString();

                                    if (/*var11.uid == 786468 ||*/ var11.parent == Class922.BANK_TAB_HEADER_ITEMS) {
                                        bankTabHeaderMove = -1;
                                    }


                                    for (var21 = 0; ~var21 > ~var11.height; ++var21) {

                                        for (var22 = 0; var11.width > var22; ++var22) {
												/*  if(var11.uid == 786439) {
                                         Class993 itemDef = Class993.getDefinition(var11.inventoryIds[var20]-1);
                                         if(var11.inventoryIds[var20]-1 >= 995 && var11.inventoryIds[var20]-1 <= 1000)
                                       	  System.out.println("integer_34: " + itemDef.integer_34);
                                         if(chatboxText.toLowerCase().contains("coin") && !itemDef.integer_34.toString().toLowerCase().contains("coin"))
                                         	continue;
                                     }*/
                                            var24 = var14 + var21 * (32 - -var11.invSpritePadY);
                                            var23 = (var11.invSpritePadX + 32) * var22 + var13 + (bankTabHeaderMove > -2 && var11.parent == Class922.BANK_TAB_HEADER_ITEMS ? bankTabHeaderMove : 0);
                                            bankTabHeaderMove++;
                                            if (var20 < 20) {
                                                var24 += var11.spriteY[var20];
                                                var23 += var11.spriteX[var20];
                                            }


                                            if (var11.inventoryIds[var20] <= 0) {
                                                if (null != var11.spriteId && var20 < 20) {
                                                    Class957 var58 = var11.getSprite(true, var20);
                                                    if (null == var58) {
                                                        if (Class1021.aBoolean6) {
                                                            Class20.refreshInterface(var11);
                                                        }
                                                    } else {
                                                        var58.method643(var23, var24);
                                                    }
                                                }
                                            } else {
                                                var39 = false;
                                                var46 = false;
                                                var47 = var11.inventoryIds[var20] - 1;
                                                if (x < 32 + var23 && ~var23 > ~width && ~y > ~(var24 - -32) && ~var24 > ~height || var11 == Class67.aClass11_1017 && ~Class979.oldSlot == ~var20) {
                                                    Class957 var54;

														/* if(var11.uid == 786439) {
                                              Class993 itemDef = Class993.getDefinition(var47);
                                              if(var47 >= 995 && var47 <= 1000)
                                            	  System.out.println("integer_34: " + itemDef.integer_34);
                                              if(chatboxText.toLowerCase().contains("coin") && !itemDef.integer_34.toString().toLowerCase().contains("coin"))
                                              	continue;
                                          }*/
                                                    int actualAmount = (var11.parent == Class922.BANK_TAB_HEADER_ITEMS/*|| var11.uid == 786468*/) ? -1
                                                            : var11.inventoryAmounts[var20];

                                                    if (var11.parent == Class922.BANK_TAB_HEADER_ITEMS && var11.inventoryIds[var20] >= 995 && var11.inventoryIds[var20] <= 1000) {
                                                        actualAmount = var11.inventoryAmounts[var20] * -1;
                                                        //  System.out.println("amount for item: "
                                                        //	  +var11.inventoryAmounts[var20] + ", actual:" + actualAmount);
                                                    }
                                                    if (-2 == ~Class164_Sub1.anInt3012 && Class110.anInt1473 == var20 && ~var11.uid == ~Class3_Sub28_Sub18.anInt3764) {
                                                        var54 = Class114.getItemSprite(2, var47, var11.aBoolean227, actualAmount, 0, 65536);
                                                    } else {
                                                        //Tab header loaded here
                                                        var54 = Class114.getItemSprite(1, var47, var11.aBoolean227, actualAmount, 3153952, 65536);
                                                    }
                                                    if (Rasterizer.aBoolean837) {
                                                        Class921.interfacesToRefresh[var12] = true;
                                                    }

                                                    if (null == var54) {
                                                        Class20.refreshInterface(var11);
                                                    } else if (Class67.aClass11_1017 == var11 && var20 == Class979.oldSlot) {
                                                        var25 = Class1015.mouseX2 - Class1210.anInt2693; //switch timer
                                                        var26 = -Class1016.anInt40 + Class1017_2.anInt1709;
                                                        //System.out.println("var25: " + var25 + ", var26: " + var26);


                                                        //The two below check if our mouse has only been down 3ms seconds or less?
                                        	  /* if(var25 != 0 && var25 <= 3 && var25 >= -3) {
                                            	var25 = 0;
                                            }
                                            if(var26 != 0 && var26 <= 3 && var26 >= -3) {
                                            	var26 = 0;
                                            }*/
                                                        //  TODO: reimport all switching shit from =original pargon or 530
                                                        //  		then add sitatusions fix
                                        	  /*if(var25 != 0 || var26 != 0) {
                                            	 System.out.println("1676: " + Class1015.anInt1676 + ", 2693: " + Class1210.anInt2693);
                                            	 var25 = 0;
                                            	 var26 = 0;
                                             }*/
                                        	  /*  if(-15 < ~var26 && 15 > ~var26) {
                                                var26 = 0;
                                             }

                                             if(var25 < 15 && ~var25 < 15) {
                                            	 //switch fix
                                                var25 = 0;
                                             }

                                             if(15 > Class40.anInt677) {
                                                var25 = 0;
                                                var26 = 0;
                                             }*/
                                        	  /*  if(-6 < ~var26 && 4 > ~var26) {
	                                                 var26 = 0;
	                                              }*/
                                        	  /*   if(var25 < 5 && ~var25 < 4) {
	                                                 var25 = 0;
	                                              }
	                                              if(10 > Class40.yDragChange) {
	                                                 var25 = 0;
	                                                 var26 = 0;
	                                              }*/
                                                        //if(var25 < 10 && ~var25 < 10) {
                                                        //       var25 = 0;
                                                        //    }
                                                        /**SWITCHING?? NANDO SAYS SO**/
                                                        //If the mouse has been down for less than 20 seconds its buffer click.
                                                        //0, 0 means click(equip)


                                                        //Lower means faster to show drag
                                                        if (10 > Class40.mouseDownTimer) {
                                                            var25 = 0;
                                                            var26 = 0;
                                                        }

                                                        // System.out.println("var24: "  + var24 + ", var26: " + var26 + ", combined: " + (var24 + var26) + ", mouseY: " + client.mouseY);

                                                        var54.drawTransparentSprite(var23 + var25, var24 - -var26, 128);
                                                        if (var5 != -1) {
                                                            Class1034 var51 = var3[var5 & '\uffff'];
                                                            int var31;
                                                            int var30;
                                                            if (Class1012.aBoolean_617) {
                                                                var31 = Class920.endY;
                                                                var30 = Class920.startY;
                                                            } else {
                                                                var30 = Class1023.startY;
                                                                var31 = Class1023.endY;
                                                            }

                                                            int var32;
                                                            if (~var30 < ~(var26 + var24) && -1 > ~var51.scrollPosition) {
                                                                //	System.out.println("switch? " + var11.disabledText + ", var51.anInt208: " + var51.scrollPosition);
                                                                var32 = Class989.anInt1446 * (-var26 + var30 + -var24) / 3;
                                                                if (~var32 < ~(Class989.anInt1446 * 10)) {
                                                                    var32 = 10 * Class989.anInt1446;
                                                                }

                                                                if (var32 > var51.scrollPosition) {
                                                                    var32 = var51.scrollPosition;
                                                                }


                                                                boolean dontScroll = false;
                                                                if (var11.uid == Class922.BANK_ITEMS) {
                                                                    int fullscreenYAdjust;
                                                                    if (Class922.resizeWidth < 1000)
                                                                        fullscreenYAdjust = Class922.resizeHeight / 2 - (275);
                                                                    else
                                                                        fullscreenYAdjust = Class922.resizeHeight / 2 - (275);

                                                                    if (Class922.clientSize == 0 && Class922.mouseY < 100)
                                                                        dontScroll = true;
                                                                    else if (Class922.clientSize > 0 && Class922.mouseY < fullscreenYAdjust + 100)
                                                                        dontScroll = true;
                                                                }

                                                                if (!dontScroll)
                                                                    var51.scrollPosition -= var32;
                                                                Class1016.anInt40 += var32;
                                                                Class20.refreshInterface(var51);
                                                            }// else {
                                                            //   	System.out.println("didn't make it.... " + var11.disabledText + ", var51.anInt208: " + var51.anInt208);
                                                            //}

                                                            if (var31 < 32 + var26 + var24 && var51.scrollPosition < -var51.scrollbarHeight + var51.maxScrollVertical) {
                                                                var32 = (-var31 + 32 + (var24 - -var26)) * Class989.anInt1446 / 3;
                                                                if (var32 > Class989.anInt1446 * 10) {
                                                                    var32 = 10 * Class989.anInt1446;
                                                                }

                                                                if (-var51.scrollPosition + var51.maxScrollVertical + -var51.scrollbarHeight < var32) {
                                                                    var32 = var51.maxScrollVertical + -var51.scrollbarHeight + -var51.scrollPosition;
                                                                }

                                                                var51.scrollPosition += var32;
                                                                Class1016.anInt40 -= var32;
                                                                Class20.refreshInterface(var51);
                                                            }
                                                        }
                                                    } else if (var11 == Class151.aClass11_1933 && var20 == Class1005.anInt1918) {
                                                        var54.drawTransparentSprite(var23, var24, 128);
                                                    } else {
                                                        var54.method643(var23, var24);
                                                    }
                                                    int amount = var11.inventoryAmounts[var20];
                                                    if (amount == -1) {
                                                        Class922.infinitySymbol.method643(var23, var24);
                                                    }
                                                }
                                            }

                                            ++var20;
                                        }
                                    }
                                } else if (3 == var11.type) {

                                    if (!Class1143.method609(var11)) {
                                        var20 = var11.disabledColor;
                                        if (var11 == Class107.aClass11_1453 && 0 != var11.disabledMouseOverColor) {
                                            var20 = var11.disabledMouseOverColor;
                                        }
                                    } else {
                                        var20 = var11.enabledColor;
                                        if (Class107.aClass11_1453 == var11 && 0 != var11.enabledMouseOverColor) {
                                            var20 = var11.enabledMouseOverColor;
                                        }
                                    }

                                    if (0 != var15) {
                                        if (var11.filled) {
                                            boolean skillGuide = false;
                                            if (var11.uid == Class922.SKILL_GUIDE_BG && (Class922.clientSize > 0 || Class922.startSize > 0))
                                                skillGuide = true;
                                            if (!Class1012.aBoolean_617) {
                                                //x, y, width, height, col, alpha
                                                //System.out.println("var11.scrollbarWidth " + var11.scrollbarWidth + ", h: " + var11.scrollbarHeight + ", var20: "  + var20 + ", var15:" + var15 + " final var15: " + (256 - (var15 & 255)));;
                                                Class1023.fillRectAlpha(skillGuide ? 0 : var13, skillGuide ? 0 : var14, var11.scrollbarWidth, var11.scrollbarHeight, var20, 256 - (var15 & 255));
                                            } else {
                                                Class920.method_973(skillGuide ? 0 : var13, skillGuide ? 0 : var14, var11.scrollbarWidth, var11.scrollbarHeight, var20, 256 - (var15 & 255));
                                            }
                                        } else if (Class1012.aBoolean_617) {
                                            Class920.method_321(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, var20, 256 - (var15 & 255));
                                        } else {
                                            Class1023.drawRectAlpha(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, var20, 256 - (var15 & 255));
                                        }
                                    } else if (var11.filled) {
                                        if (Class1012.aBoolean_617) {
                                            Class920.method_574(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, var20);
                                        } else {
                                            Class1023.fillRect(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, var20);
                                        }
                                    } else if (!Class1012.aBoolean_617) {
                                        Class1023.drawRect(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, var20);
                                    } else {
                                        Class920.method_475(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, var20);
                                    }
                                } else {

                                    Class1019 var34;
                                    if (-5 == ~var11.type) {
                                        var34 = var11.method868(Class120_Sub30_Sub1.aClass109Array3270, 0);
                                        if (var34 != null) {
                                            Class1008 var45 = var11.disabledText;
                                            boolean pm = false;
                                            if (var11.uid == 35913809) {//Enter string text
                                                var45 = Class943.create(
                                                        ((Class922.clientSize > 0 && Class929.aInteger_510 >= 525) ? "<col=7FA9FF><shad=0>" : "")
                                                                + (Class922.amountModifier.toString().length() > 0 ? (var11.disabledText.toString().substring(0, var11.disabledText.toString().length() - 1) + Class922.amountModifier + "(Z")
                                                                : fixJString(var11.disabledText.toString())));
                                            } else if (var11.uid == 35913808) {
                                                //Header for enter string
                                                if (Class922.clientSize > 0 && Class929.aInteger_510 >= 525) {
                                                    var45 = Class943.create(fixJString((Class922.clientSize > 0 && Class929.aInteger_510 >= 525 ? "<col=ffffff><shad=0>" : "") + var11.disabledText));
                                                }
                                            } else if ((var11.disabledText.toString().startsWith("From ")
                                                    || var11.disabledText.toString().startsWith("To ")
                                                    || var11.disabledText.toString().contains(" has logged out.")
                                                    || var11.disabledText.toString().contains(" has logged in."))
                                                    && !var11.disabledText.toString().contains("<col=800000>")
                                                    && !var11.disabledText.toString().contains("<col=0000ff>")
                                                    && Class922.clientSize > 0) {
                                                pm = true;
                                                //System.out.println("var11: " + var11.disabledText.toString() + ", uid: "+ var11.uid);
                                                //35913796 - 802?
                                            } else if (var11.uid == 39387147) { //pw input
                                                int length = var11.disabledText.toString().length();
                                                Class922.password = var11.disabledText;
                                                //System.out.println("disabledText password: " + var11.disabledText.toString());
                                                //TODO: | flashing
                                                String text = "Password:";
                                                for (int i = 0; i < length; i++) {
                                                    if (i > 100)
                                                        break;
                                                    text = text + "(Z";
                                                }

                                                text = text + ((Class922.inputBox == 2 && Class1134.loopCycle % 40 < 20) ? Class922.aInteger_544 == GameState.RECONNECTING ? "" : "<col=FDFF00>|" : "");
                                                var45 = Class943.create(text);
                                            } else if (var11.uid == 39387146) {
                                                Class922.tabID = 3;
                                                /**Login screen**/
                                                Class922.forgottenPasswordLogin.drawSprite(286, 346);
                                                Class922.rememberUsernameLogin.drawSprite(287, 283);
                                                Class922.hideUsernamelogin.drawSprite(424, 283);
                                                if (Class922.mouseX >= 268 && Class922.mouseX <= 282 && Class922.mouseY >= 283 && Class922.mouseY <= 295) {
                                                    Class922.rememberToggleOffHover.drawSprite(267, 279);
                                                } else {
                                                    Class922.rememberToggleOff.drawSprite(267, 279);
                                                }
                                                if (Class922.mouseX >= 398 && Class922.mouseX <= 415 && Class922.mouseY >= 283 && Class922.mouseY <= 295) {
                                                    Class922.rememberToggleOffHover.drawSprite(404, 279);
                                                } else {
                                                    Class922.rememberToggleOff.drawSprite(404, 279);
                                                }
                                                String text = "Login:" + var11.disabledText.toString();
                                                Class922.username = var11.disabledText;
                                                if (text.length() > 100)
                                                    text = text.substring(0, 100);
                                                text = text + ((Class922.inputBox == 1 && Class1134.loopCycle % 40 < 20) ? Class922.aInteger_544 == GameState.RECONNECTING ? "" : "<col=FDFF00>|" : "");
                                                var45 = Class943.create(text);
                                            } else if (var11.uid == 8978433) { //chatbox
                                                String u = var11.disabledText.toString().replaceAll(": <col=0000ff>*", "");
                                                String output = Class922.username.toString().substring(0, 1).toUpperCase() + Class922.username.toString().substring(1);
                                                String t = u.replaceAll(output, "");
                                                Class1008 flash = (Class1134.loopCycle % 40 < 20) ? Class922.BLANK_CLASS_1008 : Class922.FLASH_CLASS_1008;
                                                Class1008 text = var11.disabledText.substring(var11.disabledText.toString().length() - 1, 0);
                                                Class1008[] textAll = null;

                                                boolean newChatbox = Class922.clientSize > 0 && Class929.aInteger_510 >= 525;
                                                textAll = new Class1008[]{
                                                        newChatbox ? Class3_Sub26.whitejString : Class922.BLANK_CLASS_1008,
                                                        Class945.thisClass946.title == null ? Class922.BLANK_CLASS_1008 : Class945.thisClass946.title,
                                                        newChatbox ? Class943.create("<shad=0><col=ffffff>" + fixJString(text.toString().replace("<col=0000ff>", "<col=7FA9FF>"))) : Class943.create(fixJString(text.toString())),
                                                        //toAdd != null ? toAdd : client.BLANK_CLASS_1008,
                                                        (t.length() - 1) > 0 ? flash : Class922.BLANK_CLASS_1008};
                                                var45 = Class922.combinejStrings(textAll);

                                            } else if (var11.parent == 8978434) { //chatbox messages(100)
                                                if (Class922.clientSize > 0 && Class929.aInteger_510 >= 525) {
                                                    String text = var11.disabledText.toString();
                                                    text = text.replace("<col=0000ff>", "<col=7FA9FF>");
                                                    if (text.contains("</col>")) {
                                                        boolean bracket = text.contains("</col>]");
                                                        //text = text.replace(bracket ? "</col>]" : "</col>", bracket ? "</col><shad=1><col=ffffff>]" : "</col><col=ffffff><shad=1>");
                                                        text = text.replace(bracket ? "</col>]" : "</col>", bracket ? "</col><shad=0><col=ffffff>]" : "</col><col=ffffff><shad=1>");
                                                        if (bracket)
                                                            text = text.replace("[<col", "<col=ffffff><shad=0>[<col");
                                                    } else
                                                        text = "<shad=1><col=ffffff>" + text;


                                                    if (text.contains("<col=0000CC>"))
                                                        text = text.replace("<col=0000CC>", "<col=7FA9FF>");
                                                    if (text.contains("<col=8B0000>"))
                                                        text = text.replace("<col=8B0000>", "<col=FF5256>");
                                                    if (text.startsWith("<col="))
                                                        text = "<shad=0>" + text;
                                                    else {
                                                        //text not starting with any color
                                                        text = "<col=ffffff>" + text;
                                                    }
                                                    // if(client.clientSize > 0) {
                                                    // var11.shaded = true;
                                                    //System.out.println("textall: " + var45.toString());
                                                    //System.out.println("text: " + text);
                                                    //   }


                                                    var45 = Class943.create(fixJString(text));
                                                }

                                            }

                                            if (!Class1143.method609(var11)) {
                                                var21 = var11.disabledColor;
                                                if (Class107.aClass11_1453 == var11 && var11.disabledMouseOverColor != 0) {
                                                    var21 = var11.disabledMouseOverColor;
                                                }
                                            } else {
                                                var21 = var11.enabledColor;
                                                if (Class107.aClass11_1453 == var11 && var11.enabledMouseOverColor != 0) {
                                                    var21 = var11.enabledMouseOverColor;
                                                }

                                                if (~var11.enabledText.getLength() < -1) {
                                                    var45 = var11.enabledText;
                                                }
                                            }

                                            if (var11.scriptedInterface && 0 != ~var11.anInt192) {
                                                ItemDefinition itemDef = ItemDefinition.getDefinition(var11.anInt192);
                                                var45 = itemDef.name;
                                                if (var45 == null) {
                                                    var45 = Class1209.nullString;
                                                }

                                                if ((-2 == ~itemDef.stackable || -2 != ~var11.anInt271) && var11.anInt271 != -1) {
                                                    var45 = Class922.combinejStrings(new Class1008[]{Class3_Sub13_Sub2.aClass94_3042, var45, Class976.aClass94_2306, Class1017.method1013((byte) -125, var11.anInt271)});
                                                }
                                            }

                                            if (Class3_Sub13_Sub7.aClass11_3087 == var11) {
                                                var21 = var11.disabledColor;
                                                var45 = Class949.aClass94_2216;
                                            }


                                            if (!var11.scriptedInterface) {
                                                var45 = Class943.method1303(var11, var45, 0);
                                            }
                                            int changeX = (var11.parent == Class922.WILDY_CONTAINER && Class922.clientSize > 0) ? (Class922.resizeWidth - 258)
                                                    : -1;
                                            int changeY = pm == true ? (var14 + /*(Class7.getInterface(client.CHATBOX_CONTAINER).hidden ?*/
                                                    (Class922.chatboxHidden ? Class922.resizeHeight - 400 : Class922.resizeHeight - 540))
                                                    : (var11.parent == Class922.WILDY_CONTAINER && Class922.clientSize > 0) ? 185 : -1;

                                            var34.method676(var45,
                                                    changeX != -1 ? changeX : var13,
                                                    changeY != -1 ? changeY : var14,
                                                    var11.scrollbarWidth, var11.scrollbarHeight, var21, !var11.shaded ? -1 : 0, var11.horizontalAlignment, var11.verticalAlignment, var11.verticalSpacing);
                                        } else if (Class1021.aBoolean6) {
                                            Class20.refreshInterface(var11);
                                        }
                                    } else if (5 != var11.type) {
                                        ItemDefinition itemDef2;
                                        if (var11.type == 6) {
                                            boolean var41 = Class1143.method609(var11);
                                            Class960 var38 = null;
                                            if (var41) {
                                                var21 = var11.enabledAnim;
                                            } else {
                                                var21 = var11.disabledAnim;
                                            }

                                            var23 = 0;
                                            if (~var11.anInt192 != 0) {
                                                itemDef2 = ItemDefinition.getDefinition(var11.anInt192);
                                                if (itemDef2 != null) {
                                                    itemDef2 = itemDef2.method1106(var11.anInt271, 78);
                                                    Class954 var52 = ~var21 == 0 ? null : Class954.list(var21);
                                                    //Getting the anim for this item?
                                                    var38 = itemDef2.method1110(100, var11.anInt260, var11.anInt267, var52, 1, var11.anInt283);
                                                    if (var38 == null) {
                                                        Class20.refreshInterface(var11);
                                                    } else {
                                                        var23 = -var38.method1871() / 2;
                                                    }
                                                }
                                            } else if (5 != var11.mediaTypeDisabled) {
                                                if (0 == ~var21) {
                                                    var38 = var11.method865(-1, (Class954) null, -1, 0, var41, Class945.thisClass946.class1098);
                                                    if (null == var38 && Class1021.aBoolean6) {
                                                        Class20.refreshInterface(var11);
                                                    }
                                                } else {
                                                    Class954 var48 = Class954.list(var21);
                                                    var38 = var11.method865(var11.anInt260, var48, var11.anInt283, var11.anInt267, var41, Class945.thisClass946.class1098);
                                                    if (null == var38 && Class1021.aBoolean6) {
                                                        Class20.refreshInterface(var11);
                                                    }
                                                }
                                            } else if (-1 == var11.mediaIdDisabled) {
                                                var38 = Class77.aClass52_1112.getPlayerModel((Class145[]) null, -1, (Class954) null, (Class954) null, 0, -1, 100, 0, true, -1, -1);
                                            } else {
                                                var24 = 2047 & var11.mediaIdDisabled;
                                                if (~var24 == ~Class971.anInt2211) {
                                                    var24 = 2047;
                                                }

                                                Class946 var49 = Class922.class946List[var24];
                                                Class954 var56 = var21 == -1 ? null : Class954.list(var21);
                                                if (null != var49 && ~((int) var49.username.toLong() << -1033903957) == ~(-2048 & var11.mediaIdDisabled)) {
                                                    var38 = var49.class1098.getPlayerModel((Class145[]) null, -1, (Class954) null, var56, 0, -1, -126, 0, true, var11.anInt283, 0);
                                                }
                                            }

                                            if (var38 != null) {
                                                if (~var11.anInt184 < -1) {
                                                    var24 = (var11.scrollbarWidth << -873624568) / var11.anInt184;
                                                } else {
                                                    var24 = 256;
                                                }

                                                if (var11.anInt312 <= 0) {
                                                    var25 = 256;
                                                } else {
                                                    var25 = (var11.scrollbarHeight << 991611304) / var11.anInt312;
                                                }

                                                var26 = var13 - -(var11.scrollbarWidth / 2) - -(var24 * var11.translateX >> -1758325176);
                                                var47 = var11.scrollbarHeight / 2 + var14 + (var25 * var11.translateY >> -1056321176);
                                                if (Class1012.aBoolean_617) {
                                                    if (var11.aBoolean181) {
                                                        Class1012.method1855(var26, var47, var11.zoom, var11.aShort293, var24, var25);
                                                    } else {
                                                        Class1012.method1821(var26, var47, var24, var25);
                                                        Class1012.method1825((float) var11.aShort169, 1.5F * (float) var11.aShort293);
                                                    }

                                                    Class1012.method1846();
                                                    Class1012.toggleFog(true);
                                                    Class1012.toggleFog(false);
                                                    Class3_Sub13_Sub33.method324(Class3_Sub28_Sub10.brightness);
                                                    if (Class986.aBoolean47) {
                                                        Class920.method_233();
                                                        Class1012.method1841();
                                                        Class920.method_576(x, y, width, height);
                                                        Class986.aBoolean47 = false;
                                                    }

                                                    if (var11.depthBufferEnabled) {
                                                        Class1012.disableDepthBuffer();
                                                    }

                                                    var28 = Rasterizer.sineTable[var11.rotateX] * var11.zoom >> -215429808;
                                                    var29 = var11.zoom * Rasterizer.cosineTable[var11.rotateX] >> -957182768;
                                                    if (var11.scriptedInterface) {
                                                        var38.method1893(0, var11.rotateY, var11.rotateZ, var11.rotateX, var11.anInt258, var11.anInt264 + var28 + var23, var11.anInt264 + var29, -1L);
                                                    } else {
                                                        var38.method1893(0, var11.rotateY, 0, var11.rotateX, 0, var28, var29, -1L);
                                                    }

                                                    if (var11.depthBufferEnabled) {
                                                        Class1012.enableDepthBuffer();
                                                    }
                                                } else {
                                                    Rasterizer.method1145(var26, var47);
                                                    var28 = Rasterizer.sineTable[var11.rotateX] * var11.zoom >> 428930352;
                                                    var29 = var11.zoom * Rasterizer.cosineTable[var11.rotateX] >> 1430420816;
                                                    if (!var11.scriptedInterface) {
                                                        var38.method1893(0, var11.rotateY, 0, var11.rotateX, 0, var28, var29, -1L);
                                                    } else if (var11.aBoolean181) {
                                                        ((Class1049) var38).method1946(0, var11.rotateY, var11.rotateZ, var11.rotateX, var11.anInt258, var11.anInt264 + var23 + var28, var29 + var11.anInt264, var11.zoom);
                                                    } else {
                                                        var38.method1893(0, var11.rotateY, var11.rotateZ, var11.rotateX, var11.anInt258, var11.anInt264 + (var28 - -var23), var29 + var11.anInt264, -1L);
                                                    }

                                                    Rasterizer.method1141();
                                                }
                                            }
                                        } else {
                                            if (-8 == ~var11.type) {
                                                var34 = var11.method868(Class120_Sub30_Sub1.aClass109Array3270, 0);
                                                if (var34 == null) {
                                                    if (Class1021.aBoolean6) {
                                                        Class20.refreshInterface(var11);
                                                    }
                                                    continue;
                                                }

                                                var21 = 0;


                                                //Search here? == not the best spot, make buffer new list of inventoryids/amounts
                                                //var42.integer_34 filter
                                                //But then withdrawing items would be tricky.
                                                //Maybe search server-side would be better

                                                for (var22 = 0; ~var11.height < ~var22; ++var22) {
                                                    for (var23 = 0; var23 < var11.width; ++var23) {
                                                        if (0 < var11.inventoryIds[var21]) {
                                                            itemDef2 = ItemDefinition.getDefinition(var11.inventoryIds[var21] + -1);

                                                            Class1008 var40;
                                                            if (1 != itemDef2.stackable && 1 == var11.inventoryAmounts[var21]) {
                                                                var40 = Class922.combinejStrings(new Class1008[]{Class3_Sub13_Sub2.aClass94_3042, itemDef2.name, Class1030.aClass94_2584});
                                                            } else {
                                                                var40 = Class922.combinejStrings(new Class1008[]{Class3_Sub13_Sub2.aClass94_3042, itemDef2.name, Class976.aClass94_2306, Class1017.method1013((byte) -100, var11.inventoryAmounts[var21])});
                                                            }

                                                            var26 = var13 + var23 * (var11.invSpritePadX + 115);
                                                            var47 = (var11.invSpritePadY + 12) * var22 + var14;
                                                            if (~var11.horizontalAlignment != -1) {
                                                                if (-2 == ~var11.horizontalAlignment) {

                                                                    var34.drawText(var40, 57 + var26, var47, var11.disabledColor, !var11.shaded ? -1 : 0);
                                                                } else {

                                                                    var34.method688(var40, -1 + var26 + 115, var47, var11.disabledColor, !var11.shaded ? -1 : 0);
                                                                }
                                                            } else {

                                                                var34.method681(var40, var26, var47, var11.disabledColor, var11.shaded ? 0 : -1);
                                                            }
                                                        }

                                                        ++var21;
                                                    }
                                                }
                                            }

                                            //Type 8: Yellow box
                                            if (var11.type == 8 && Class20.aClass11_439 == var11 && ~Class75.anInt1109 == ~Class3_Sub13_Sub26.anInt3323) {
                                                var21 = 0;
                                                var20 = 0;
                                                Class1008 var43 = var11.disabledText;

                                                Class1019 var35 = Class922.getRegularClass1019();
                                                var43 = Class943.method1303(var11, var43, 0);

                                                Class1008 var44;
                                                while (-1 > ~var43.getLength()) {
                                                    var25 = var43.method1551(ByteBuffer.newLinejString);
                                                    if (var25 != -1) {
                                                        var44 = var43.substring(var25, 0);
                                                        var43 = var43.method1556(var25 + 4, (byte) -74);
                                                    } else {
                                                        var44 = var43;
                                                        var43 = Class922.BLANK_CLASS_1008;
                                                    }

                                                    var26 = var35.method682(var44);
                                                    var21 += var35.anInt3727 - -1;
                                                    if (~var20 > ~var26) {
                                                        var20 = var26;
                                                    }
                                                }

                                                var26 = var14 - -var11.scrollbarHeight - -5;
                                                var20 += 6;
                                                var21 += 7;
                                                if (~(var26 - -var21) < ~height) {
                                                    var26 = -var21 + height;
                                                }

                                                var25 = -var20 + -5 + var13 - -var11.scrollbarWidth;
                                                if (var25 < 5 + var13) {
                                                    var25 = 5 + var13;
                                                }

                                                if (~(var20 + var25) < ~width) {
                                                    var25 = -var20 + width;
                                                }

                                                if (Class1012.aBoolean_617) {
                                                    Class920.method_574(var25, var26, var20, var21, 16777120);
                                                    Class920.method_475(var25, var26, var20, var21, 0);
                                                } else {
                                                    Class1023.fillRect(var25, var26, var20, var21, 16777120);
                                                    Class1023.drawRect(var25, var26, var20, var21, 0);
                                                }

                                                var43 = var11.disabledText;
                                                var47 = 2 + (var26 - -var35.anInt3727);


                                                for (var43 = Class943.method1303(var11, var43, 0); ~var43.getLength() < -1; var47 += var35.anInt3727 + 1) {
                                                    //var28 = skillID
                                                    var28 = var43.method1551(ByteBuffer.newLinejString);
                                                    if (0 == ~var28) {
                                                        var44 = var43;
                                                        var43 = Class922.BLANK_CLASS_1008;
                                                    } else {
                                                        var44 = var43.substring(var28, 0);
                                                        var43 = var43.method1556(4 + var28, (byte) -74);
                                                    }

                                                    var35.method681(var44, 3 + var25, var47, 0, -1);
                                                }
                                            }

                                            if (var11.type == 9) {

                                                if (var11.aBoolean167) {
                                                    var20 = var13;
                                                    var22 = var13 + var11.scrollbarWidth;
                                                    var21 = var14 + var11.scrollbarHeight;
                                                    var23 = var14;
                                                } else {
                                                    var20 = var13;
                                                    var21 = var14;
                                                    var23 = var14 + var11.scrollbarHeight;
                                                    var22 = var13 + var11.scrollbarWidth;
                                                }
                                                //var11.anInt250 = 1;

                                                if (var11.thickness == 1) {
                                                    if (var11.uid == 8978432 && Class922.clientSize > 0 && Class929.aInteger_510 >= 525) {
                                                        if (!Class1012.aBoolean_617) {
                                                            Class1023.drawAlphaHorizontalLine2(2, var21, var22 + 12, /*var23,*/ /*var11.disabledColor*/0x66625E, 130);
                                                        } else {
                                                            Class920.method_235(1, var21, var22, /*var23,*/ /*var11.disabledColor*/ 0x66625E, 130);
                                                        }
                                                    } else {
                                                        if (!Class1012.aBoolean_617) {
                                                            Class1023.drawDiagonalLine(var20, var21, var22, var23, var11.disabledColor);
                                                        } else {
                                                            Class920.method_844(var20, var21, var22, var23, var11.disabledColor);
                                                        }
                                                    }
                                                } else if (!Class1012.aBoolean_617) {
                                                    Class1023.drawDiagonalLine(var20, var21, var22, var23, var11.disabledColor, var11.thickness);
                                                } else {
                                                    Class920.method_844(var20, var21, var22, var23, var11.disabledColor, var11.thickness);
                                                }
                                            }
                                        }
                                    } else {
                                        Class957 var37;
                                        if (!var11.scriptedInterface) {
                                            //old sprites?
                                            var37 = var11.method_405((byte) -113, Class1143.method609(var11));
                                            if (null != var37) {
                                                //Below is chatbox sprite
                                                if ((var11.uid == 35913811 || var11.uid == 35913802) && Class922.clientSize > 0) {
                                                    //TODO: Line at top, scrollbar..
                                                    if (var11.uid == 35913802 && Class922.clientSize > 0 && Class929.aInteger_510 >= 525) {
                                                        if (Class922.dialogueUp) {
                                                            var37.drawTransparentSprite(var13, var14, var11.alpha);
                                                        } else {
                                                            int y2 = Class922.resizeHeight - 199;
                                                            if (Class1012.aBoolean_617) {
                                                                //for(int i = 130; 0 < i; i--) {
                                                                //	i = i - 3;
                                                                //Class920.method_973(7, y2 + i, 505, 3 + 1, 0, 15 + (i / 2));
                                                                Class920.method_235(9, y2 - 1, 516, 0x636363, 130);
                                                                Class920.method_452(9, y2, 508, 3 + 1, 0, 15, 130);
                                                                //higher # = less transparent

                                                                //}
                                                            } else {
                                                                Class1023.drawAlphaHorizontalLine2(9, y2 - 1, 405, 0x7C7C7D, 256); //top line
                                                                //Class1023.drawAlphaGradient2(9, y2, 505, 130, 0, 0, 130);
                                                                Class1023.drawVerticalGradient(9, y2, 505, 130, 0x00000000, 0x5A000000);
                                                                //	Class1023.drawAlphaHorizontalLine2(7, y2 + 120, 405, 0x6d6a57, 256); //Make old alpha
                                                            }
                                                        }

                                                    } else {
                                                        var37.drawTransparentSprite(var13, var14, var11.alpha);
                                                    }
                                                } else {
                                                    if (var11.uid == Class922.DUEL_SPRITE && Class922.clientSize > 0) {
                                                        var37.method643(Class922.resizeWidth - 245, 160);
                                                    } else {
                                                        var37.method643(var13, var14);
                                                    }
                                                }
                                            } else if (Class1021.aBoolean6) {
                                                Class20.refreshInterface(var11);
                                            }
                                        } else {

                                            if (var11.anInt192 != -1) {
                                                var37 = Class114.getItemSprite(var11.outline, var11.anInt192, var11.aBoolean227, var11.anInt271, var11.shadow, 65536);
                                            } else {
                                                var37 = var11.method_405((byte) -113, false);
                                                //Also loads scrollbar sprite
                                            }

                                            if (var37 == null) {
                                                if (Class1021.aBoolean6) {
                                                    Class20.refreshInterface(var11);
                                                }
                                            } else {
                                                var21 = var37.trimWidth;
                                                var22 = var37.trimHeight;
                                                if (!var11.aBoolean186) {
                                                    var23 = var21 == 0 ? 4096 : var11.scrollbarWidth * 4096 / var21;
                                                    //if(var11.uid == 8978435)
                                                    //  System.out.println("var23: "+ var23);
                                                    if (-1 == ~var11.rotatino) {
                                                        if (0 != var15) {
                                                            //transparent new sprite
                                                            var37.method642(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight, -(255 & var15) + 256);
                                                        } else if (~var21 == ~var11.scrollbarWidth && ~var22 == ~var11.scrollbarHeight) {
                                                            //usually here for NEW sprites
                                                            if (var11.uid == Class922.MULTI_SPRITE && Class922.clientSize > 0) {
                                                                var37.method643(Class922.resizeWidth - 45, 160);
                                                            } else if (var11.parent == Class922.WILDY_CONTAINER && Class922.clientSize > 0) {
                                                                var37.method643(Class922.resizeWidth - 245, 160);
                                                            } else {
                                                                //scrollbar pieces
                                                                //corners for borders
																	/*                                  			  if(var11.disabledSprite >= 824 && var11.disabledSprite <= 830) {
                                        				  switch(var11.disabledSprite) {
                                        				  case 824: //top left
                                        					  client.borderSprites[5].drawSprite(var13 + (Class1012.aBoolean_617 ? 0 : 31), var14);
                                        					  break;
                                        				  case 825: //top right
                                        					  client.borderSprites[4].drawSprite(var13 + (Class1012.aBoolean_617 ? 0 : 31), var14);
                                        					  break;
                                        				  case 826: //bottom left
                                        					  client.borderSprites[7].drawSprite(var13 + (Class1012.aBoolean_617 ? 0 : 31), var14);
                                        					  break;
                                        				  case 827: //bottom right
                                        					  client.borderSprites[6].drawSprite(var13 + (Class1012.aBoolean_617 ? 0 : 31), var14);
                                        					  break;
                                        				  case 829: //Left connector
                                        					  client.borderSprites[9].drawSprite(var13 + (Class1012.aBoolean_617 ? 0 : 9), var14 + 9);
                                        					  break;
                                        				  case 830: //Right connector
                                        					  client.borderSprites[8].drawSprite(var13 + (Class1012.aBoolean_617 ? 0 : 31), var14 + 9);
                                        					  break;
                                        				  }
                                        			} else*/
                                                                var37.method643(var13, var14);
                                                            }
                                                        } else {
                                                            //Here for scrollbar

                                                            if (var11.uid == 8978435 && Class922.clientSize > 0 && Class929.aInteger_510 >= 525) {

                                                                int sprite = -1;
                                                                switch (var11.disabledSprite) {
                                                                    case 773: //up
                                                                        sprite = 0;
                                                                        break;
                                                                    case 788: //down
                                                                        sprite = 1;
                                                                        break;

                                                                    case 789://bartop
                                                                        sprite = 2;
                                                                        break;
                                                                    case 790://barmid
                                                                        sprite = 3;
                                                                        break;
                                                                    case 791: //bar bottom
                                                                        sprite = 4;
                                                                        break;
                                                                    //	case 792: //bar bg
                                                                    //	sprite = -1;
                                                                    //	break;
                                                                }
                                                                if (sprite != -1) {
                                                                    //if(var11.disabledSprite == 790)
                                                                    //	var37.method643(var13, var14);
                                                                    //	else
                                                                    Class1031.scrollBarFsSprite[sprite].drawSprite(var13, var14);

                                                                    //if(var11.disabledSprite == 790)
                                                                    //	System.out.println("newpos: " + var11.newScrollerPos + " wholeinter: " + var11.aByte273 + ", wholeinter2: " + var11.anInt306);
                                                                    //newpos max = 88, min = 16
                                                                } //else
                                                                //System.out.println("ds: " + var11.disabledSprite);
                                                                //SceneGraphNode.scrollBarSprite[1].drawSprite(x, -16 + barHeight + y);
                                                                // var37.method643(var13, var14);
                                                            } else {
                                                                if (var11.disabledSprite == 1226)
                                                                    Class922.bountyTeleSprite[0].drawSprite(var11.anInt306 - -var2 + (Class1012.aBoolean_617 ? 0 : 20), var1 + var11.newScrollerPos);
                                                                else
                                                                    var37.method639(var13, var14, var11.scrollbarWidth, var11.scrollbarHeight);
                                                            }

                                                        }
                                                    } else {
                                                        var37.method640(var14 + var11.scrollbarHeight / 2, var11.rotatino, var23, var13 + var11.scrollbarWidth / 2);
                                                    }
                                                } else {
                                                    var23 = (var21 + -1 + var11.scrollbarWidth) / var21;
                                                    var24 = (var11.scrollbarHeight - 1 - -var22) / var22;
                                                    if (Class1012.aBoolean_617) {
                                                        Class920.method_974(var13, var14, var11.scrollbarWidth + var13, var11.scrollbarHeight + var14);
                                                        var39 = Class140_Sub6.method2021(var37.width);
                                                        var46 = Class140_Sub6.method2021(var37.height);
                                                        Class1011 var27 = (Class1011) var37;
                                                        if (var39 && var46) {
                                                            if (var15 != 0) {
                                                                var27.method646(var13, var14, -(255 & var15) + 256, var23, var24);
                                                            } else {
                                                                var27.method649(var13, var14, var23, var24);
                                                            }
                                                        } else if (var39) {
                                                            for (var28 = 0; ~var24 < ~var28; ++var28) {
                                                                if (~var15 == -1) {
                                                                    var27.method649(var13, var28 * var22 + var14, var23, 1);
                                                                } else {
                                                                    var27.method646(var13, var14 + var28 * var22, 256 + -(var15 & 255), var23, 1);
                                                                }
                                                            }
                                                        } else if (!var46) {
                                                            for (var28 = 0; ~var28 > ~var23; ++var28) {
                                                                for (var29 = 0; ~var24 < ~var29; ++var29) {
                                                                    if (var15 != 0) {
                                                                        var37.drawTransparentSprite(var28 * var21 + var13, var22 * var29 + var14, -(255 & var15) + 256);
                                                                    } else {
                                                                        var37.method643(var13 - -(var21 * var28), var22 * var29 + var14);
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            for (var28 = 0; ~var23 < ~var28; ++var28) {
                                                                if (~var15 != -1) {
                                                                    var27.method646(var21 * var28 + var13, var14, -(var15 & 255) + 256, 1, var24);
                                                                } else {
                                                                    var27.method649(var21 * var28 + var13, var14, 1, var24);
                                                                }
                                                            }
                                                        }

                                                        Class920.method_576(x, y, width, height);
                                                    } else {
                                                        Class1023.method1326(var13, var14, var13 - -var11.scrollbarWidth, var14 - -var11.scrollbarHeight);

                                                        for (var25 = 0; var25 < var23; ++var25) {
                                                            for (var26 = 0; ~var26 > ~var24; ++var26) {
                                                                if (var11.rotatino == 0) {
                                                                    if (0 == var15) {
                                                                        //Sprites that are 'grid'
																			/*     	  if(var11.disabledSprite >= 820 && var11.disabledSprite <= 823 || var11.disabledSprite == 828) {
                                            				  switch(var11.disabledSprite) {
                                            				  case 820: //top
                                            				  case 828:
                                            					  client.borderSprites[1].drawSprite(var25 * var21 + var13 + (Class1012.aBoolean_617 ? 0 : 17), var22 * var26 + var14 + 13);
                                            					  break;
                                            				  case 821: //left
                                            					  client.borderSprites[2].drawFlippedSprite(var25 * var21 + var13 + (Class1012.aBoolean_617 ? 0 : 12), var22 * var26 + var14 -1);
                                            					  break;
                                            				  case 822://bottom
                                            					  client.borderSprites[1].drawFlippedSprite(var25 * var21 + var13 + (Class1012.aBoolean_617 ? 0 : -2), var22 * var26 + var14 + 13);
                                            					  //client.borderSprites[0].drawSprite(var25 * var21 + var13 + (Class1012.aBoolean_617 ? 0 : 5), var22 * var26 + var14 + 13);
                                            					  break;
                                            				 case 823: //right
                                            					 client.borderSprites[2].drawSprite(var25 * var21 + var13 + (Class1012.aBoolean_617 ? 0 : 19), var22 * var26 + var14 + 1);
                                            					//  break;
                                            				  }
                                                    	  } else */
                                                                        var37.method643(var25 * var21 + var13, var22 * var26 + var14);
                                                                    } else {
                                                                        var37.drawTransparentSprite(var25 * var21 + var13, var14 + var26 * var22, 256 - (255 & var15));
                                                                    }
                                                                } else {
                                                                    var37.method640(var14 - -(var22 * var26) + var22 / 2, var11.rotatino, 4096, var25 * var21 + var13 + var21 / 2);
                                                                }
                                                            }
                                                        }

                                                        Class1023.clipRect(x, y, width, height);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (var8 <= 31) {
            aClass153_748 = (Class1027) null;
        }
    }

    //  public static Class1008 toAdd;
    public final static String fixJString(String text) {
        //BELOW FIXES FUCKED UP JSTRING THINGS


        if (text.contains("("))
            text = text.replace("(", "(X");
        if (text.contains(")"))
            text = text.replace(")", "(Y");
        if (text.contains("*"))
            text = text.replace("*", "(Z");

        if (text.contains(","))
            text = text.replace(",", ")1");

        if (text.contains("`"))
            text = text.replace("`", ")e");

        if (text.contains("+"))
            text = text.replace("+", ")0");
        if (text.contains("-"))
            text = text.replace("-", ")2");


        //)2 = minus
        //)0 = plus
        //+u = � uppercase
        //-I = � lowcaser
        //+f = �
        //+c = �
        //+9 = same as above but straight
        //-9 = �

        return text;
    }

    public static void method1096(byte var0) {
        try {
            aClass153_748 = null;
            if (var0 < 84) {
                method1091(true);
            }

            aClass94_750 = null;
            aClass93_743 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "gn.G(" + var0 + ')');
        }
    }

    final void method1097(Class1002 var1, long var2, byte var4) {
        try {
            if (-1 == ~this.anInt749) {
                Class1002 var5 = this.aClass13_747.method877();
                var5.unlink();
                var5.unlinkSub();
                if (this.aClass3_Sub28_744 == var5) {
                    var5 = this.aClass13_747.method877();
                    var5.unlink();
                    var5.unlinkSub();
                }
            } else {
                --this.anInt749;
            }

            this.aClass130_745.put(var1, var2);
            int var7 = -76 % ((var4 - -5) / 35);
            this.aClass13_747.insertLast(var1);
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "gn.L(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var4 + ')');
        }
    }

    static final void method1098(byte var0) {
        try {
            if (-129 < ~Class3_Sub9.anInt2309) {
                Class3_Sub9.anInt2309 = 128;
            }

            if (-384 > ~Class3_Sub9.anInt2309) {
                Class3_Sub9.anInt2309 = 383;
            }

            Class1211.cameraYaw &= 2047;
            if (var0 >= -31) {
                aClass153_748 = (Class1027) null;
            }

            int var1 = Class3_Sub13_Sub13.anInt3155 >> -971224825;
            int var2 = Class62.anInt942 >> -694284537;
            int var3 = Class121.method1736(Class26.plane, 1, Class3_Sub13_Sub13.anInt3155, Class62.anInt942);
            int var4 = 0;
            int var5;
            if (-4 > ~var1 && 3 < var2 && 100 > var1 && var2 < 100) {
                for (var5 = -4 + var1; var1 - -4 >= var5; ++var5) {
                    //System.out.println("waw");
                    for (int var6 = -4 + var2; var6 <= 4 + var2; ++var6) {
                        int var7 = Class26.plane;
                        if (3 > var7 && 2 == (2 & Class9.groundArray[1][var5][var6])) {
                            ++var7;
                        }

                        if (var7 >= Class136.aByteArrayArrayArray1774.length)
                            continue;
                        int var8 = (255 & Class136.aByteArrayArrayArray1774[var7][var5][var6]) * 8 - Class1134.activeTileHeightMap[var7][var5][var6] + var3;
                        if (var8 > var4) {
                            var4 = var8;
                        }
                    }
                }
            }

            var5 = 192 * var4;
            if (-98049 > ~var5) {
                var5 = 98048;
            }

            if (-32769 < ~var5) {
                var5 = '\u8000';
            }

            if (~Class75_Sub4.anInt2670 <= ~var5) {
                if (~var5 > ~Class75_Sub4.anInt2670) {
                    Class75_Sub4.anInt2670 += (var5 - Class75_Sub4.anInt2670) / 80;
                }
            } else {
                Class75_Sub4.anInt2670 += (-Class75_Sub4.anInt2670 + var5) / 24;
            }

        } catch (RuntimeException var9) {
            throw Class1134.method1067(var9, "gn.D(" + var0 + ')');
        }
    }


    final Class1042 method1099(int var1) {
        try {
            if (var1 != -1) {
                aClass93_743 = (Class93) null;
            }

            return this.aClass130_745.getNext();
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "gn.F(" + var1 + ')');
        }
    }

    static final int method1100(int var0, int var2) {
        if (0 == ~var2) {
            return 12345678;
        } else {

            var0 = var0 * (127 & var2) >> 2137332647;
            if (2 <= var0) {
                if (126 < var0) {
                    var0 = 126;
                }
            } else {
                var0 = 2;
            }

            return var0 + ('\uff80' & var2);
        }
    }

    final void method1101(int var1) {
        try {
            this.aClass13_747.clear();
            this.aClass130_745.clear();
            this.aClass3_Sub28_744 = new Class1002();
            if (var1 != 2) {
                this.getCS2(-36L, 52);
            }

            this.anInt749 = this.anInt746;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "gn.E(" + var1 + ')');
        }
    }

    Class47(int var1) {
        try {
            int var2 = 1;

            for (this.anInt749 = var1; ~var1 < ~(var2 - -var2); var2 += var2) {
                ;
            }

            this.anInt746 = var1;
            this.aClass130_745 = new Class1017_2(var2);
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "gn.<init>(" + var1 + ')');
        }
    }

}

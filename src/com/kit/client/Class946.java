package com.kit.client;

public final class Class946 extends Class991 {

    protected static int[] anIntArray3951 = new int[4];
    protected int anInt3952 = -1;
    protected static byte aByte3953;
    protected static int[] anIntArray3954 = new int[100];
    protected int headIconId = -1;
    protected int team = 0;
    private int anInt3958 = 0;
    protected static int[] anIntArray3959 = new int[2];
    protected int combatLevel = 0;
    protected static Class1008 tweeningOnMessage = Class943.create("Tweening enabled(Q");
    protected static Class1008 orbsOnMessage = Class943.create("Orbs enabled(Q");
    protected Class1098 class1098;
    protected int anInt3963 = -1;
    protected int anInt3965 = 0;
    protected int anInt3966 = -1;
    protected Class1008 username, title;
    boolean aBoolean3968 = false;
    boolean rankHidden = false;
    protected int anInt3969 = 0;
    protected int anInt3970 = -1;
    protected int pkIconId = -1;
    protected int anInt3973 = -1;
    protected int anInt3974 = 0;

    final int getSize() {
        if (null != this.class1098 && 0 != ~this.class1098.npcId) {
            return Class981.list(this.class1098.npcId).size;
        } else {
            return super.getSize();
        }
    }

    private int weapon;
/*   public int getWeapon() {
       return weapon;
   }*/

    final void decodeAppearance(ByteBuffer class966) {
        class966.offset = 0;
        int var3 = class966.readUnsignedByte();
        int npcId = -1;
        int var4 = 1 & var3;
        int var7 = super.getSize();
        this.setSize(1 + (var3 >> 3 & 7), 2);
        this.anInt3958 = 3 & var3 >> 6;
        this.y += (-var7 + this.getSize()) * 64;
        this.x += 64 * (this.getSize() + -var7);
        this.pkIconId = class966.getByte(); //
        this.headIconId = class966.getByte();
        this.team = 0;

        int var11;
        int var12;
        int var13;
        int var14;
        int[] equipmentIds = new int[12];

        for (int i = 0; i < 12; ++i) {
            int equipment = class966.readUnsignedByte();
            if (equipment == 0) {
                equipmentIds[i] = 0;
            } else {
                int var121 = class966.readUnsignedByte();
                equipmentIds[i] = (equipment << 8) + var121;
                if (i == 0 && equipmentIds[0] == 65535) {
                    npcId = class966.aInteger233();
                    //this.team = class966.readUnsignedByte();
                    break;
                }

                if (equipmentIds[i] >= 512) {
                    int t = ItemDefinition.getDefinition(equipmentIds[i] - 512).anInt782;
                    if (t != 0) {
                        this.team = t;
                    }
                }
            }
            if (i == 3)
                weapon = equipmentIds[i] == 0 ? 0 : equipmentIds[i] - 512;
        }
        int[] colors = new int[5];

        for (var11 = 0; var11 < 5; ++var11) {
            var12 = class966.readUnsignedByte();
            if (-1 < ~var12 || var12 >= Class15.aShortArrayArray344[var11].length) {
                var12 = 0;
            }

            colors[var11] = var12;
        }

        idleAnimation = class966.aInteger233();
        if (idleAnimation == 65535)
            idleAnimation = -1;
        turnAnimation = class966.aInteger233();
        if (turnAnimation == 65535)
            turnAnimation = -1;
        walkAnimation = class966.aInteger233();
        if (walkAnimation == 65535)
            walkAnimation = -1;
        turn180Animation = class966.aInteger233();
        if (turn180Animation == 65535)
            turn180Animation = -1;
        turn90CWAnimation = class966.aInteger233();
        if (turn90CWAnimation == 65535)
            turn90CWAnimation = -1;
        turn90CCAnimation = class966.aInteger233();
        if (turn90CCAnimation == 65535)
            turn90CCAnimation = -1;
        runAnimation = class966.aInteger233();
        if (runAnimation == 65535)
            runAnimation = -1;
        this.username = class966.class_91033();
        this.title = class966.class_91033();
        if (this.title.toString().length() > 0) {
            this.title = Class922.combinejStrings(new Class1008[]{this.title, Class922.spaceString});
        }

        this.rankHidden = class966.aInteger233() == 1;
        this.combatLevel = class966.readUnsignedByte();
        this.anInt3974 = class966.aInteger233();
        this.anInt3965 = this.combatLevel;
        this.anInt3970 = -1;

        var13 = this.anInt3969;
        this.anInt3969 = 0;
        if (0 == this.anInt3969) {
            Class162.method2203(this, 8);
        } else {
            int var15 = this.anInt3966;
            int var16 = this.anInt3963;
            int var17 = this.anInt3973;
            var14 = this.anInt3952;
            this.anInt3952 = class966.aInteger233();
            this.anInt3966 = class966.aInteger233();
            this.anInt3963 = class966.aInteger233();
            this.anInt3973 = class966.aInteger233();
            if (this.anInt3969 != var13 || ~this.anInt3952 != ~var14 || ~this.anInt3966 != ~var15 || var16 != this.anInt3963 || ~this.anInt3973 != ~var17) {
                Class1002.method518(this);
            }
        }
        if (null == this.class1098) {
            this.class1098 = new Class1098();
        }

        var14 = this.class1098.npcId;
        this.class1098.setAppearance(colors, npcId, ~var4 == -2, equipmentIds);
        if (~var14 != ~npcId) {
            this.y = 128 * this.anIntArray2767[0] + this.getSize() * 64;
            this.x = 128 * this.anIntArray2755[0] - -(64 * this.getSize());
        }

        if (this.aClass127_Sub1_2801 != null) {
            this.aClass127_Sub1_2801.method1759();
        }
    }

    protected static boolean resting = false;

    final void render(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, long var9, int var11, Class1218 var12) {
        try {
            if (this.class1098 != null) {
                if (this.currentAnimationId == 11786 && (this.currentMoveAnimation == 0 || this.currentMoveAnimation == -1))
                    resting = true;
                else
                    resting = false;

                Class954 currentAnim = this.currentAnimationId != -1 && 0 == this.animationDelay ? Class954.list(this.currentAnimationId) : null;

                Class954 currentMoveAnim = 0 != ~this.currentMoveAnimation && !this.aBoolean3968 && (this.currentMoveAnimation != idleAnimation || currentAnim == null) ? Class954.list(this.currentMoveAnimation) : null;
                Class960 playerModel = this.class1098.getPlayerModel(this.aClass145Array2809, this.anInt2776, currentMoveAnim, currentAnim, this.anInt2802, this.anInt2793, -120, this.anInt2760, true, this.anInt2832, this.anInt2813);
                int var16 = Class118.method1727();
                if (Class1012.aBoolean_617 && Class3_Sub24_Sub3.maxMemory < 96 && ~var16 < -51) {
                    Class971.method90(1);
                }

                int var17;
                if (0 != Class3_Sub13_Sub13.modeWhat && var16 < 50) {
                    for (var17 = 50 - var16; Class56.anInt893 < var17; ++Class56.anInt893) {
                        Class3_Sub6.aByteArrayArray2287[Class56.anInt893] = new byte[102400];
                    }

                    while (Class56.anInt893 > var17) {
                        --Class56.anInt893;
                        Class3_Sub6.aByteArrayArray2287[Class56.anInt893] = null;
                    }
                }

                if (playerModel != null) {
                    this.anInt2820 = playerModel.method1871();
                    Class960 var23;
                    if (Class1012.aBoolean_617) {
                        if (Class140_Sub6.aBoolean2910 && (-1 == this.class1098.npcId || Class981.list(this.class1098.npcId).aBoolean1249)) {
                            var23 = Class140_Sub3.method1957(160, this.aBoolean2810, currentMoveAnim == null ? currentAnim : currentMoveAnim, this.y, 0, this.x, 0, 1, playerModel, var1, null != currentMoveAnim ? this.anInt2813 : this.anInt2832, this.anInt2831, 240, (byte) -49);
                            float var18 = Class1012.method1852();
                            float var19 = Class1012.method1839();
                            Class1012.disableDepthBuffer();
                            Class1012.method1825(var18, -150.0F + var19);
                            var23.render(0, var2, var3, var4, var5, var6, var7, var8, -1L, var11, (Class1218) null);
                            Class1012.enableDepthBuffer();
                            Class1012.method1825(var18, var19);
                        }
                    }

                    if (Class945.thisClass946 == this) {
                        for (var17 = Class1244.hintsList.length + -1; -1 >= ~var17; --var17) {
                            Class1025 var27 = Class1244.hintsList[var17];
                            if (var27 != null && ~var27.anInt1355 != 0) {
                                int var21;
                                int var20;
                                if (-2 == ~var27.type && 0 <= var27.index && ~Class3_Sub13_Sub24.class1001List.length < ~var27.index) {
                                    Class1001 var24 = Class3_Sub13_Sub24.class1001List[var27.index];
                                    if (null != var24) {
                                        var20 = var24.y / 32 - Class945.thisClass946.y / 32;
                                        var21 = -(Class945.thisClass946.x / 32) + var24.x / 32;
                                        this.method1979((Class1218) null, var21, playerModel, var20, var6, var11, 2047, var1, var8, var5, var4, var2, var27.anInt1355, var3, var7);
                                    }
                                }

                                if (var27.type == 2) {
                                    int var29 = 4 * (-Class131.anInt1716 + var27.anInt1356) + 2 + -(Class945.thisClass946.y / 32);
                                    var20 = 2 + (4 * (var27.anInt1347 - SpriteDefinition.anInt1152) - Class945.thisClass946.x / 32);
                                    this.method1979((Class1218) null, var20, playerModel, var29, var6, var11, 2047, var1, var8, var5, var4, var2, var27.anInt1355, var3, var7);
                                }

                                if (-11 == ~var27.type && var27.index >= 0 && ~Class922.class946List.length < ~var27.index) {
                                    Class946 var28 = Class922.class946List[var27.index];
                                    if (null != var28) {
                                        var20 = -(Class945.thisClass946.y / 32) + var28.y / 32;
                                        var21 = var28.x / 32 + -(Class945.thisClass946.x / 32);
                                        this.method1979((Class1218) null, var21, playerModel, var20, var6, var11, 2047, var1, var8, var5, var4, var2, var27.anInt1355, var3, var7);
                                    }
                                }
                            }
                        }
                    }

                    this.method1971(playerModel);
                    this.method1969(playerModel, var1);
                    var23 = null;
                    if (!this.aBoolean3968 && 0 != ~this.anInt2842 && this.anInt2805 != -1) {
                        Class1211 var26 = Class1211.list(this.anInt2842);
                        var23 = var26.method966(this.anInt2826, (byte) -30, this.anInt2805, this.anInt2761);
                        if (var23 != null) {
                            var23.move(0, -this.anInt2799, 0);
                            if (var26.aBoolean536) {
                                if (Class3_Sub13_Sub16.anInt3198 != 0) {
                                    var23.method1896(Class3_Sub13_Sub16.anInt3198);
                                }

                                if (0 != Class3_Sub28_Sub9.anInt3623) {
                                    var23.method1886(Class3_Sub28_Sub9.anInt3623);
                                }

                                if (Class3_Sub13_Sub9.anInt3111 != 0) {
                                    var23.move(0, Class3_Sub13_Sub9.anInt3111, 0);
                                }
                            }
                        }
                    }

                    Class960 var25 = null;
                    if (!this.aBoolean3968 && this.anObject2796 != null) {
                        if (Class1134.loopCycle >= this.anInt2778) {
                            this.anObject2796 = null;
                        }

                        if (~this.anInt2797 >= ~Class1134.loopCycle && this.anInt2778 > Class1134.loopCycle) {
                            if (!(this.anObject2796 instanceof Class140_Sub3)) {
                                var25 = (Class960) this.anObject2796;
                            } else {
                                var25 = (Class960) ((Class140_Sub3) this.anObject2796).method1963(3);
                            }

                            var25.move(this.anInt2782 + -this.y, this.anInt2812 + -this.anInt2831, this.anInt2833 + -this.x);
                            if (-513 != ~this.turnDirection) {
                                if (~this.turnDirection != -1025) {
                                    if (-1537 == ~this.turnDirection) {
                                        var25.rotate90();
                                    }
                                } else {
                                    var25.rotate180();
                                }
                            } else {
                                var25.rotate270();
                            }
                        }
                    }

                    if (Class1012.aBoolean_617) {
                        playerModel.aBoolean2699 = true;
                        playerModel.render(var1, var2, var3, var4, var5, var6, var7, var8, var9, var11, this.aClass127_Sub1_2801);
                        if (var23 != null) {
                            var23.aBoolean2699 = true;
                            var23.render(var1, var2, var3, var4, var5, var6, var7, var8, var9, var11, this.aClass127_Sub1_2801);
                        }
                    } else {
                        if (null != var23) {
                            playerModel = ((Class1049) playerModel).method1943(var23);
                        }

                        if (var25 != null) {
                            playerModel = ((Class1049) playerModel).method1943(var25);
                        }

                        playerModel.aBoolean2699 = true;
                        playerModel.render(var1, var2, var3, var4, var5, var6, var7, var8, var9, var11, this.aClass127_Sub1_2801);
                    }

                    if (null != var25) {
                        if (this.turnDirection == 512) {
                            var25.rotate90();
                        } else if (1024 == this.turnDirection) {
                            var25.rotate180();
                        } else if (1536 == this.turnDirection) {
                            var25.rotate270();
                        }

                        var25.move(-this.anInt2782 + this.y, -this.anInt2812 + this.anInt2831, -this.anInt2833 + this.x);
                    }

                }
            }
        } catch (RuntimeException var22) {
            throw Class1134.method1067(var22, "e.IA(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ',' + var9 + ',' + var11 + ',' + (var12 != null ? "{...}" : "null") + ')');
        }
    }

    private final void method1979(Class1218 var1, int var2, Class960 var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15) {
        try {
            int var16 = var4 * var4 - -(var2 * var2);
            if (-17 >= ~var16 && -360001 <= ~var16) {
                int var17 = (int) (325.949D * Math.atan2((double) var4, (double) var2)) & var7;
                Class960 var18 = Class128.method1763(true, var17, this.x, var13, this.y, var3, this.anInt2831);
                if (var18 != null) {
                    if (Class1012.aBoolean_617) {
                        float var19 = Class1012.method1852();
                        float var20 = Class1012.method1839();
                        Class1012.disableDepthBuffer();
                        Class1012.method1825(var19, var20 - 150.0F);
                        var18.render(0, var12, var14, var11, var10, var5, var15, var9, -1L, var6, var1);
                        Class1012.enableDepthBuffer();
                        Class1012.method1825(var19, var20);
                    } else {
                        var18.render(0, var12, var14, var11, var10, var5, var15, var9, -1L, var6, var1);
                    }
                }

            }
        } catch (RuntimeException var21) {
            throw Class1134.method1067(var21, "e.N(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ',' + var5 + ',' + var6 + ',' + var7 + ',' + var8 + ',' + var9 + ',' + var10 + ',' + var11 + ',' + var12 + ',' + var13 + ',' + var14 + ',' + var15 + ')');
        }
    }

    final boolean method1966(byte var1) {
        try {
            if (var1 != 17) {
                anIntArray3954 = (int[]) null;
            }

            return this.class1098 != null;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "e.L(" + var1 + ')');
        }
    }

    final Class1008 method1980(int var1) {
        try {
            Class1008 var2 = this.username;
            if (var1 != 0) {
                this.render(-63, 126, 58, -9, -74, -119, -45, -114, -62L, -76, (Class1218) null);
            }

            if (Class966_2.aClass94Array3802 != null) {
                var2 = Class922.combinejStrings(new Class1008[]{Class966_2.aClass94Array3802[this.anInt3958], var2});
            }

            if (null != Class986.aClass94Array45) {
                var2 = Class922.combinejStrings(new Class1008[]{var2, Class986.aClass94Array45[this.anInt3958]});
            }

            return var2;
        } catch (RuntimeException var3) {
            throw Class1134.method1067(var3, "e.Q(" + var1 + ')');
        }
    }

    final void method1867(int var1, int var2, int var3, int var4, int var5) {
    }

    final void method1981(int var2, boolean var3, int var4) {
        super.method1967(this.getSize(), var2, var4, var3);
    }

    protected final void finalize() {
    }

    public static void method1982(byte var0) {
        try {
            //aClass94_3971 = null;
            anIntArray3951 = null;
            tweeningOnMessage = null;
            anIntArray3959 = null;
            if (var0 <= 116) {
                method1982((byte) -48);
            }

            anIntArray3954 = null;
            //aClass94_3964 = null;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "e.R(" + var0 + ')');
        }
    }

    final int method1871() {
        try {
            return this.anInt2820;
        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "e.MA()");
        }
    }

}

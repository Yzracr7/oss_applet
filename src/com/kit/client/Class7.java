package com.kit.client;

public final class Class7 implements Class416 {

	static Class1027 skinsFetcher;
	static int anInt2161 = -1;
	static int renderZ;
	static Class1008 aClass94_2164 = Class943.create("mapedge");
	static int lastPacket = 0;
	static short[] aShortArray2167 = new short[] { (short) 30, (short) 6, (short) 31, (short) 29, (short) 10, (short) 44, (short) 37, (short) 57 };
	static Class1008 aClass94_2168 = Class943.create("<br>");

	static final void method831(String var1) {
		System.out.println("Error: " + Class1042_3.a("%0a", "\n", 105, var1));
	}

	public static final Class1034 getInterface(int var1) {
		int var2 = var1 >> 16;
		int var3 = 0xffff & var1;
		try {
			if (Class1031.interfaceCache[var2] == null || null == Class1031.interfaceCache[var2][var3]) {
				boolean var4 = Class970.method_948(var2);
				if (!var4) {
					return null;
				}
			}
			return Class1031.interfaceCache[var2][var3];
		} catch (Exception e) {
			return null;
		}
	}
	
	
	static final void discardInterface(int var1) {
		if (-1 != var1) {
			if (Class1017_2.interfaceLoaded[var1]) {
				Class3_Sub13_Sub29.interfaceFetcher.method2128(var1);
				if (null != Class1031.interfaceCache[var1]) {
					boolean var2 = true;

					for (int var3 = 0; Class1031.interfaceCache[var1].length > var3; ++var3) {
						if (Class1031.interfaceCache[var1][var3] != null) {
							if (~Class1031.interfaceCache[var1][var3].type == -3) {
								var2 = false;
							} else {
								Class1031.interfaceCache[var1][var3] = null;
							}
						}
					}

					if (var2) {
						Class1031.interfaceCache[var1] = null;
					}

					Class1017_2.interfaceLoaded[var1] = false;
				}
			}
		}
	}
	
	public static void method833(byte var0) {
		try {
			aShortArray2167 = null;
			skinsFetcher = null;
			aClass94_2164 = null;
			aClass94_2168 = null;
			int var1 = 124 / ((var0 - 28) / 41);
		} catch (RuntimeException var2) {
			throw Class1134.method1067(var2, "af.E(" + var0 + ')');
		}
	}

	public final Class1008 method20(int var1, int[] var2, int var3, long var4) {
		try {
			if (var1 != 0) {
				if (var1 != 1 && ~var1 != -11) {
					return var1 != 6 && var1 != 7 && 11 != var1 ? (var3 != 4936 ? (Class1008) null : null) : Class992.list(var2[0]).method616((int) var4, (byte) -69);
				} else {
					ItemDefinition var8 = ItemDefinition.getDefinition((int) var4);
					return var8.name;
				}
			} else {
				Class992 var6 = Class992.list(var2[0]);
				return var6.method616((int) var4, (byte) 120);
			}
		} catch (RuntimeException var7) {
			throw Class1134.method1067(var7, "af.A(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ',' + var4 + ')');
		}
	}

	static final void method834(byte var0) {
		try {
			// Class1006.method1250(false);
			System.gc();
			Class922.setaInteger_544(25);
			if (var0 >= -80) {
				lastPacket = -89;
			}

		} catch (RuntimeException var2) {
			throw Class1134.method1067(var2, "af.D(" + var0 + ')');
		}
	}

	static final boolean method835(int var0, int var1, int var2, int var3, int var4, int var5, Class1031 var6, int var7, long var8) {
		if (var6 == null) {
			return true;
		} else {
			int var10 = var1 * 128 + 64 * var4;
			int var11 = var2 * 128 + 64 * var5;
			return Class56.method1189(var0, var1, var2, var4, var5, var10, var11, var3, var6, var7, false, var8);
		}
	}

}

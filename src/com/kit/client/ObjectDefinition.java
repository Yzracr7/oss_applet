package com.kit.client;

final class ObjectDefinition {

    protected int integer_0 = 1;
    protected int integer_1 = 2;
    protected int integer_2 = 5;
    protected int integer_3 = 14;
    protected int integer_4 = 15;
    protected int integer_5 = 17;
    protected int integer_6 = 18;
    protected int integer_7 = 19;
    protected int integer_8 = 21;
    protected int integer_9 = 22;
    protected int integer_10 = 23;
    protected int integer_11 = 24;
    protected int integer_12 = 27;
    protected int integer_13 = 28;
    protected short[] aShortArray1476;
    protected short[] aShortArray1477;
    protected int anInt1478;
    protected int anInt1479;
    protected int sizeX = 1;
    protected int anInt1481;
    protected int minimapSprite;
    protected boolean aBoolean1483 = false;
    protected int anInt1484;
    protected int sizeY = 1;
    protected boolean projectileCliped;
    protected int anInt1488;
    protected int shading;
    protected static boolean[] isKeyDown = new boolean[112];
    protected boolean aBoolean1491;
    protected boolean aBoolean1492;
    protected int anInt1493;
    protected int lightness = 0;
    protected int anInt1496;
    protected static int[][][] anIntArrayArrayArray1497 = new int[4][64][64];
    protected boolean aBoolean1498;
    protected Class1008[] interactions;
    protected short aShort1500;
    protected Class1017_2 aClass130_1501;
    protected boolean aBoolean1502 = false;
    protected boolean aBoolean1503;
    protected Class1008 integer_34;
    protected byte aByte1505;
    protected short[] aShortArray1495;
    protected short[] aShortArray1506;
    protected boolean aBoolean1507;
    protected boolean aBoolean1510;
    protected int anInt1511;
    protected int anInt1512;
    protected byte[] aByteArray1513;
    protected static int worldId = 1;
    protected int anInt1515;
    protected int anInt1516;
    protected int anInt1517;
    protected int anInt1518;
    protected int[] integer_910;
    protected int[] integer_233;
    protected int[] anIntArray1524;
    protected int[] anIntArray1539;
    protected int anInt1520;
    protected static int anInt1521 = 0;
    protected int anInt1522;
    protected boolean aBoolean1525;
    protected int anInt1526;
    protected int objectId;
    protected int anInt1528;
    protected int isInteractive;
    protected boolean aBoolean1530;
    protected int animationId;
    protected int anInt1532;
    protected int anInt1533;
    protected int anInt1534;
    protected static short aShort1535 = 320;
    protected boolean aBoolean1536;
    protected boolean aBoolean1537;
    protected int clipType;
    protected int anInt1540;
    protected boolean delayShading;
    protected boolean aBoolean1542;

    final boolean method1684(int var1, int var2) {
        try {
            if (integer_910 != null) {
                for (int var7 = 0; ~integer_910.length < ~var7; ++var7) {
                    if (~var2 == ~integer_910[var7]) {
                        return Class922.getModelJs5(integer_233[var7] & '\uffff').method2129(0, integer_233[var7] & '\uffff');
                    }
                }
                return true;
            } else if (null == integer_233) {
                return true;
            } else if (-11 != ~var2) {
                return true;
            } else {
                boolean var4 = true;
                for (int var5 = 0; integer_233.length > var5; ++var5) {
                    var4 &= Class922.getModelJs5('\uffff' & integer_233[var5]).method2129(0, '\uffff' & integer_233[var5]);
                }
                return var4;
            }
        } catch (RuntimeException var6) {
            throw Class1134.method1067(var6, "pb.H(" + var1 + ',' + var2 + ')');
        }
    }

    final ObjectDefinition method1685() {
        int var2 = -1;
        if (anInt1526 != -1) {
            var2 = Class981.method1484(64835055, anInt1526);
        } else if (anInt1532 != -1) {
            var2 = Class163_Sub1.variousSettings[anInt1532];
        }

        if (~var2 <= -1 && ~var2 > ~(anIntArray1524.length - 1) && ~anIntArray1524[var2] != 0) {
            return ObjectDefinition.getDefinition(anIntArray1524[var2]);
        } else {
            int var3 = anIntArray1524[-1 + anIntArray1524.length];
            return ~var3 == 0 ? null : ObjectDefinition.getDefinition(var3);
        }
    }

    private final Model method1686(int var1, int var2, int var3) {
        try {
            Model var4 = null;
            boolean var5 = aBoolean1536;
            if (-3 == ~var2 && 3 < var1) {
                var5 = !var5;
            }

            int var6;
            int var7;
            if (null == integer_910) {
                if (-11 != ~var2) {
                    return null;
                }

                if (integer_233 == null) {
                    return null;
                }

                var6 = integer_233.length;

                for (var7 = 0; ~var7 > ~var6; ++var7) {
                    int var8 = integer_233[var7];
                    if (var5) {
                        var8 += 65536;
                    }

                    var4 = (Model) Class99.aClass93_1401.get((long) var8);
                    if (var4 == null) {
                        var4 = Model.get(Class922.getModelJs5(var8 & '\uffff'), var8 & '\uffff', 0);
                        if (var4 == null) {
                            return null;
                        }

                        if (var5) {
                            var4.method2002();
                        }

                        Class99.aClass93_1401.put(var4, (long) var8);
                    }

                    if (1 < var6) {
                        Class164.aClass140_Sub5Array2058[var7] = var4;
                    }
                }

                if (~var6 < -2) {
                    var4 = new Model(Class164.aClass140_Sub5Array2058, var6);
                }
            } else {
                var6 = -1;

                for (var7 = 0; integer_910.length > var7; ++var7) {
                    if (var2 == integer_910[var7]) {
                        var6 = var7;
                        break;
                    }
                }

                if (~var6 == 0) {
                    return null;
                }

                var7 = integer_233[var6];
                if (var5) {
                    var7 += 65536;
                }

                var4 = (Model) Class99.aClass93_1401.get((long) var7);
                if (null == var4) {
                    var4 = Model.get(Class922.getModelJs5(var7 & '\uffff'), var7 & '\uffff', 0);
                    if (null == var4) {
                        return null;
                    }

                    if (var5) {
                        var4.method2002();
                    }

                    Class99.aClass93_1401.put(var4, (long) var7);
                }
            }

            boolean var11;
            if (128 == anInt1479 && ~anInt1488 == -129 && 128 == anInt1481) {
                var11 = false;
            } else {
                var11 = true;
            }

            boolean var12;
            if (anInt1496 == 0 && -1 == ~anInt1511 && 0 == anInt1534) {
                var12 = false;
            } else {
                var12 = true;
            }

            Model var13 = new Model(var4, var3 == ~var1 && !var11 && !var12, aShortArray1477 == null, null == aShortArray1476, true);
            if (~var2 == -5 && var1 > 3) {
                var13.method2011(256);
                var13.method2001(45, 0, -45);
            }

            var1 &= 3;
            if (-2 != ~var1) {
                if (-3 != ~var1) {
                    if (3 == var1) {
                        var13.method2018();
                    }
                } else {
                    var13.method1989();
                }
            } else {
                var13.method1991();
            }

            int var9;
            if (null != aShortArray1477) {
                for (var9 = 0; aShortArray1477.length > var9; ++var9) {
                    if (null != aByteArray1513 && aByteArray1513.length > var9) {
                        var13.swapColors(aShortArray1477[var9], Class3_Sub13_Sub9.aShortArray3110[255 & aByteArray1513[var9]]);
                    } else {
                        var13.swapColors(aShortArray1477[var9], aShortArray1506[var9]);
                    }
                }
            }

            if (aShortArray1476 != null) {
                for (var9 = 0; aShortArray1476.length > var9; ++var9) {
                    var13.method1998(aShortArray1476[var9], aShortArray1495[var9]);
                }
            }

            if (var11) {
                var13.scale(anInt1479, anInt1488, anInt1481);
            }

            if (var12) {
                var13.method2001(anInt1496, anInt1511, anInt1534);
            }

            return var13;
        } catch (RuntimeException var10) {
            throw Class1134.method1067(var10, "pb.O(" + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

    public static void method1687(int var0) {
        try {
            anIntArrayArrayArray1497 = (int[][][]) null;
            isKeyDown = null;
            if (var0 != -11) {
                anInt1521 = -96;
            }

        } catch (RuntimeException var2) {
            throw Class1134.method1067(var2, "pb.B(" + var0 + ')');
        }
    }

    static final Class72 method1688(int var0, int var1, int var2) {
        Class949 var3 = Class75_Sub2.class949s[var0][var1][var2];
        if (var3 == null) {
            return null;
        } else {
            Class72 var4 = var3.aClass72_2245;
            var3.aClass72_2245 = null;
            return var4;
        }
    }

    final void method1689() {
        if (isInteractive == -1) {
            isInteractive = 0;
            if (null != integer_233 && (null == integer_910 || -11 == ~integer_910[0])) {
                isInteractive = 1;
                for (int var2 = 0; var2 < 5; ++var2) {
                    if (interactions[var2] != null) {
                        isInteractive = 1;
                        break;
                    }
                }
            }
        }

        if (-1 == anInt1540) {
            anInt1540 = clipType != 0 ? 1 : 0;
        }

    }

    final boolean method1690(int var1) {
        try {
            if (anIntArray1524 != null) {
                if (var1 != 28933) {
                    method1696(34, 54, (int[][]) ((int[][]) null), 55, 80, (int[][]) ((int[][]) null), true, (Class1047) null, (byte) 127, true, -38);
                }

                for (int var2 = 0; ~var2 > ~anIntArray1524.length; ++var2) {
                    if (0 != ~anIntArray1524[var2]) {
                        ObjectDefinition var3 = ObjectDefinition.getDefinition(anIntArray1524[var2]);
                        if (0 != ~var3.anInt1512 || var3.anIntArray1539 != null) {
                            return true;
                        }
                    }
                }

                return false;
            } else {
                return anInt1512 != -1 || anIntArray1539 != null;
            }
        } catch (RuntimeException var4) {
            throw Class1134.method1067(var4, "pb.F(" + var1 + ')');
        }
    }

    final int method1691(int var1, int var2, byte var3) {
        try {
            if (var3 <= 76) {
                return -40;
            } else if (aClass130_1501 == null) {
                return var1;
            } else {
                Class1042_2 var4 = (Class1042_2) aClass130_1501.get((long) var2);
                return var4 != null ? var4.value : var1;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "pb.N(" + var1 + ',' + var2 + ',' + var3 + ')');
        }
    }

    final void method_203(ByteBuffer var2) {
        for (; ; ) {
            int var3 = var2.readUnsignedByte();
            if (var3 == 0) {
                break;
            }
            readValues(var2, var3);
        }
    }

    private final void readValues(ByteBuffer class966, int var_013) {
        int len;
        int var5;
        if (var_013 == integer_0) {
            len = class966.readUnsignedByte();
            if (~len < -1) {
                if (integer_233 != null && !Class47.aBoolean742) {
                    class966.offset += len * 3;
                } else {
                    integer_910 = new int[len];
                    integer_233 = new int[len];

                    for (var5 = 0; len > var5; ++var5) {
                        integer_233[var5] = class966.aInteger233();
                        integer_910[var5] = class966.readUnsignedByte();
                    }
                }
            }
        } else if (var_013 == integer_1) {
            integer_34 = class966.class_91033();
        } else if (var_013 == integer_2) {
            len = class966.readUnsignedByte();
            if (~len < -1) {
                if (null != integer_233 && !Class47.aBoolean742) {
                    class966.offset += len * 2;
                } else {
                    integer_233 = new int[len];
                    integer_910 = null;

                    for (var5 = 0; len > var5; ++var5) {
                        integer_233[var5] = class966.aInteger233();
                    }
                }
            }
        } else if (var_013 == integer_3) {
            sizeX = class966.readUnsignedByte();
        } else if (var_013 == integer_4) {
            sizeY = class966.readUnsignedByte();
        } else if (var_013 == integer_5) {
            clipType = 0;
            projectileCliped = false;
        } else if (var_013 == integer_6) {
            projectileCliped = false;
        } else if (var_013 == integer_7) {
            isInteractive = class966.readUnsignedByte();
        } else if (var_013 == integer_8) {
            aByte1505 = 1;
        } else if (var_013 == integer_9) {
            delayShading = true;
        } else if (var_013 == integer_10) {
            aBoolean1542 = true;
        } else if (var_013 == integer_11) {
            animationId = class966.aInteger233();
            if (~animationId == -65536) {
                animationId = -1;
            }
        } else if (var_013 == integer_12) {
            clipType = 1;
        } else if (var_013 == integer_13) {
            anInt1528 = class966.readUnsignedByte();
        } else if (var_013 == 29) {
            lightness = class966.getByte();
        } else if (var_013 == 39) {
            shading = class966.getByte() * 5;
        } else if (var_013 >= 30 && var_013 < 35) {
            interactions[var_013 - 30] = class966.class_91033();
            if (interactions[-30 + var_013].method102(Class3_Sub13_Sub3.aClass94_3051)) {
                interactions[-30 + var_013] = null;
            }
        } else if (var_013 == 40) {
            len = class966.readUnsignedByte();
            aShortArray1477 = new short[len];
            aShortArray1506 = new short[len];
            for (var5 = 0; var5 < len; ++var5) {
                aShortArray1477[var5] = (short) class966.aInteger233();
                aShortArray1506[var5] = (short) class966.aInteger233();
            }
        } else if (var_013 == 41) {
            len = class966.readUnsignedByte();
            aShortArray1495 = new short[len];
            aShortArray1476 = new short[len];
            for (var5 = 0; ~var5 > ~len; ++var5) {
                aShortArray1476[var5] = (short) class966.aInteger233();
                aShortArray1495[var5] = (short) class966.aInteger233();
            }
        } else if (var_013 == 42) {
            len = class966.readUnsignedByte();
            aByteArray1513 = new byte[len];
            for (var5 = 0; ~var5 > ~len; ++var5) {
                aByteArray1513[var5] = class966.getByte();
            }
        } else if (var_013 == 62) {
            aBoolean1536 = true;
        } else if (var_013 == 64) {
            aBoolean1525 = false;
        } else if (var_013 == 65) {
            anInt1479 = class966.aInteger233();
        } else if (var_013 == 66) {
            anInt1488 = class966.aInteger233();
        } else if (var_013 == 67) {
            anInt1481 = class966.aInteger233();
        } else if (var_013 == 68) {
            anInt1516 = class966.aInteger233();
        } else if (var_013 == 69) {
            anInt1533 = class966.readUnsignedByte();
        } else if (var_013 == 70) {
            anInt1496 = class966.aLong_1884();
        } else if (var_013 == 71) {
            anInt1511 = class966.aLong_1884();
        } else if (var_013 == 72) {
            anInt1534 = class966.aLong_1884();
        } else if (var_013 == 73) {
            aBoolean1483 = true;
        } else if (var_013 == 74) {
            aBoolean1498 = true;
        } else if (var_013 == 75) {
            anInt1540 = class966.readUnsignedByte();
        } else if (var_013 == 77 || var_013 == 92) {
            len = -1;
            anInt1526 = class966.aInteger233();
            if ('\uffff' == anInt1526) {
                anInt1526 = -1;
            }
            anInt1532 = class966.aInteger233();
            if ('\uffff' == anInt1532) {
                anInt1532 = -1;
            }
            if (92 == var_013) {
                len = class966.aInteger233();
                if (len == '\uffff') {
                    len = -1;
                }
            }
            var5 = class966.readUnsignedByte();
            anIntArray1524 = new int[var5 - -2];

            for (int var6 = 0; var5 >= var6; ++var6) {
                anIntArray1524[var6] = class966.aInteger233();
                if ('\uffff' == anIntArray1524[var6]) {
                    anIntArray1524[var6] = -1;
                }
            }
            anIntArray1524[1 + var5] = len;
        } else if (var_013 == 78) {
            anInt1512 = class966.aInteger233();
            anInt1484 = class966.readUnsignedByte();
        } else if (var_013 == 79) {
            anInt1518 = class966.aInteger233();
            anInt1515 = class966.aInteger233();
            anInt1484 = class966.readUnsignedByte();
            len = class966.readUnsignedByte();
            anIntArray1539 = new int[len];
            for (var5 = 0; ~len < ~var5; ++var5) {
                anIntArray1539[var5] = class966.aInteger233();
            }
        } else {
            if (81 != var_013) {
                if (~var_013 != -83) {
                    if (var_013 != 88) {
                        if (-90 != ~var_013) {
                            if (90 != var_013) {
                                if (var_013 == 91) {
                                    this.aBoolean1491 = true;
                                } else if (-94 == ~var_013) {
                                    this.aByte1505 = 3;
                                    this.aShort1500 = (short) class966.aLong_1884();
                                } else if (var_013 != 94) {
                                    if (~var_013 == -96) {
                                        this.aByte1505 = 5;
                                    } else if (~var_013 != -97) {
                                        if (~var_013 == -98) {
                                            this.aBoolean1537 = true;
                                        } else if (var_013 == 98) {
                                            this.aBoolean1510 = true;
                                        } else if (~var_013 != -100) {
                                            if (-101 == ~var_013) {
                                                this.anInt1520 = class966.getByte();
                                                this.anInt1522 = class966.aLong_1884();
                                            } else if (~var_013 != -102) {
                                                if (var_013 != 102) {
                                                    if (249 == var_013) {
                                                        len = class966.readUnsignedByte();
                                                        if (null == aClass130_1501) {
                                                            var5 = Class95.method1585(len);
                                                            aClass130_1501 = new Class1017_2(var5);
                                                        }
                                                        for (var5 = 0; len > var5; ++var5) {
                                                            boolean var10 = -2 == ~class966.readUnsignedByte();
                                                            int var7 = class966.aBoolean183();
                                                            Object var8;
                                                            if (!var10) {
                                                                var8 = new Class1042_2(class966.getInt());
                                                            } else {
                                                                var8 = new Class1030(class966.class_91033());
                                                            }
                                                            aClass130_1501.put((Class1042) var8, (long) var7);
                                                        }
                                                    }
                                                } else {
                                                    this.anInt1516 = class966.aLong_1884();
                                                }
                                            } else {
                                                this.anInt1478 = class966.getByte();
                                            }
                                        } else {
                                            this.anInt1493 = class966.getByte();
                                            this.anInt1517 = class966.aLong_1884();
                                        }
                                    } else {
                                        this.aBoolean1507 = true;
                                    }
                                } else {
                                    this.aByte1505 = 4;
                                }
                            } else {
                                this.aBoolean1502 = true;
                            }
                        } else {
                            this.aBoolean1492 = false;
                        }
                    } else {
                        this.aBoolean1503 = false;
                    }
                } else {
                    this.minimapSprite = class966.aLong_1884();
                    this.aBoolean1530 = true;
                }
            } else {
                this.aByte1505 = 2;
                this.aShort1500 = (short) (256 * class966.getByte());
            }
        }
    }


    final boolean method1694(boolean var1) {
        if (null == integer_233) {
            return true;
        } else {
            boolean var2 = true;
            for (int var3 = 0; integer_233.length > var3; ++var3) {
                Class1027 model_cache = Class922.getModelJs5(0xffff & integer_233[var3]);
                var2 &= model_cache.method2129(0, 0xffff & integer_233[var3]);
            }
            return var2;
        }
    }

    private final Class948 method1695(int var1, boolean var2, boolean var3, int var4) {
        try {
            int var6 = lightness + 64;
            int var7 = 5 * shading + 768;
            Class948 var5 = null;
            int var8;
            int var12;
            if (integer_910 != null) {
                var8 = -1;

                int var9;
                for (var9 = 0; ~var9 > ~integer_910.length; ++var9) {
                    if (~integer_910[var9] == ~var4) {
                        var8 = var9;
                        break;
                    }
                }

                if (0 == ~var8) {
                    return null;
                }

                var9 = integer_233[var8];
                if (var2) {
                    var9 += 65536;
                }

                var5 = (Class948) Class99.aClass93_1401.get((long) var9);
                if (null == var5) {
                    Model var10 = Model.get(Class1043.aClass153_1043, '\uffff' & var9, 0);
                    if (null == var10) {
                        return null;
                    }

                    var5 = new Class948(var10, var6, var7, var2);
                    Class99.aClass93_1401.put(var5, (long) var9);
                }
            } else {
                if (var4 != 10) {
                    return null;
                }

                if (integer_233 == null) {
                    return null;
                }

                var8 = integer_233.length;
                if (-1 == ~var8) {
                    return null;
                }

                long var16 = 0L;

                for (int var11 = 0; ~var8 < ~var11; ++var11) {
                    var16 = (long) integer_233[var11] + var16 * 67783L;
                }

                if (var2) {
                    var16 = ~var16;
                }

                var5 = (Class948) Class99.aClass93_1401.get(var16);
                if (null == var5) {
                    Model var17 = null;

                    for (var12 = 0; ~var8 < ~var12; ++var12) {
                        var17 = Model.get(Class922.getModelJs5(integer_233[var12] & '\uffff'), integer_233[var12] & '\uffff', 0);
                        if (null == var17) {
                            return null;
                        }

                        if (var8 > 1) {
                            Class164.aClass140_Sub5Array2058[var12] = var17;
                        }
                    }

                    if (1 < var8) {
                        var17 = new Model(Class164.aClass140_Sub5Array2058, var8);
                    }

                    var5 = new Class948(var17, var6, var7, var2);
                    Class99.aClass93_1401.put(var5, var16);
                }
            }

            boolean var14 = aBoolean1536;
            if (~var4 == -3 && var1 > 3) {
                var14 = !var14;
            }

            boolean var15 = 128 == anInt1488 && -1 == ~anInt1511;
            boolean var18 = -1 == ~var1 && 128 == anInt1479 && ~anInt1481 == -129 && ~anInt1496 == -1 && anInt1534 == 0 && !var14;
            Class948 var19 = var5.method1926(var18, var15, aShortArray1477 == null, true, ~var5.method1903() == ~var6, -1 == ~var1 && !var14, var3, ~var7 == ~var5.method1924(), true, !var14, aShortArray1476 == null);
            if (var14) {
                var19.method1931();
            }

            if (var4 == 4 && 3 < var1) {
                var19.method1932(256);
                var19.move(45, 0, -45);
            }

            var1 &= 3;
            if (1 == var1) {
                var19.method1925();
            } else if (~var1 != -3) {
                if (~var1 == -4) {
                    var19.method1902();
                }
            } else {
                var19.method1911();
            }

            if (null != aShortArray1477) {
                for (var12 = 0; ~aShortArray1477.length < ~var12; ++var12) {
                    var19.method1918(aShortArray1477[var12], aShortArray1506[var12]);
                }
            }

            if (null != aShortArray1476) {
                for (var12 = 0; ~aShortArray1476.length < ~var12; ++var12) {
                    var19.method1916(aShortArray1476[var12], aShortArray1495[var12]);
                }
            }

            if (anInt1479 != 128 || -129 != ~anInt1488 || ~anInt1481 != -129) {
                var19.scale(anInt1479, anInt1488, anInt1481);
            }

            if (-1 != ~anInt1496 || anInt1511 != 0 || 0 != anInt1534) {
                var19.move(anInt1496, anInt1511, anInt1534);
            }

            if (var6 != var19.method1903()) {
                var19.method1914(var6);
            }

            if (var19.method1924() != var7) {
                var19.method1909(var7);
            }

            return var19;
        } catch (RuntimeException var13) {
            throw Class1134.method1067(var13, "pb.L(" + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ')');
        }
    }

    final Class136 method1696(int var1, int var2, int[][] var3, int var4, int var5, int[][] var6, boolean var7, Class1047 var8, byte var9, boolean var10, int var11) {
        try {
            if (var9 >= -5) {
                return (Class136) null;
            } else {
                long var12;
                if (!Class1012.aBoolean_617) {
                    if (integer_910 != null) {
                        var12 = (long) ((var4 << 3) + ((objectId << 10) - -var1));
                    } else {
                        var12 = (long) ((objectId << 10) + var1);
                    }

                    boolean var20;
                    if (var7 && delayShading) {
                        var12 |= Long.MIN_VALUE;
                        var20 = true;
                    } else {
                        var20 = false;
                    }

                    Object var22 = (Class1031) Class1225.aClass93_4051.get(var12);
                    if (null == var22) {
                        Model var21 = method1686(var1, var4, -1);
                        if (var21 == null) {
                            Class10117.aClass136_1413.aClass140_1777 = null;
                            return Class10117.aClass136_1413;
                        }

                        var21.method2010();
                        if (-11 == ~var4 && var1 > 3) {
                            var21.method2011(256);
                        }

                        if (!var20) {
                            var22 = new Class1049(var21, 64 - -lightness, 5 * shading + 768, -50, -10, -50);
                        } else {
                            var21.aShort2879 = (short) (64 + lightness);
                            var22 = var21;
                            var21.aShort2876 = (short) (768 + 5 * shading);
                            var21.calculateNormals();
                        }

                        Class1225.aClass93_4051.put(var22, var12);
                    }

                    if (var20) {
                        var22 = ((Model) var22).method2004();
                    }

                    if (0 != aByte1505) {
                        if (!(var22 instanceof Class1049)) {
                            if (var22 instanceof Model) {
                                var22 = ((Model) var22).method1999(aByte1505, aShort1500, var3, var6, var2, var5, var11, true, false);
                            }
                        } else {
                            var22 = ((Class1049) var22).method1941(aByte1505, aShort1500, var3, var6, var2, var5, var11, true);
                        }
                    }

                    Class10117.aClass136_1413.aClass140_1777 = (Class1031) var22;
                    return Class10117.aClass136_1413;
                } else {
                    if (null != integer_910) {
                        var12 = (long) (var1 + (objectId << 10) - -(var4 << 3));
                    } else {
                        var12 = (long) ((objectId << 10) + var1);
                    }

                    Class136 var16 = (Class136) Class1225.aClass93_4051.get(var12);
                    Class948 var14;
                    Class1047 var15;
                    if (null == var16) {
                        var14 = method1695(var1, false, true, var4);
                        if (null == var14) {
                            Class10117.aClass136_1413.aClass140_1777 = null;
                            Class10117.aClass136_1413.aClass109_Sub1_1770 = null;
                            return Class10117.aClass136_1413;
                        }

                        if (~var4 == -11 && var1 > 3) {
                            var14.method1876(256);
                        }

                        if (var10) {
                            var15 = var14.method1933(var8);
                        } else {
                            var15 = null;
                        }

                        var16 = new Class136();
                        var16.aClass140_1777 = var14;
                        var16.aClass109_Sub1_1770 = var15;
                        Class1225.aClass93_4051.put(var16, var12);
                    } else {
                        var14 = (Class948) var16.aClass140_1777;
                        var15 = var16.aClass109_Sub1_1770;
                    }

                    boolean var17 = delayShading & var7;
                    Class948 var18 = var14.method1926(3 != aByte1505, ~aByte1505 == -1, true, true, true, true, !var17, true, true, true, true);
                    if (~aByte1505 != -1) {
                        var18.method1919(aByte1505, aShort1500, var14, var3, var6, var2, var5, var11);
                    }

                    var18.method1920(~isInteractive == -1 && !aBoolean1510, true, true, true, -1 == ~isInteractive, true, false);
                    Class10117.aClass136_1413.aClass140_1777 = var18;
                    var18.aBoolean3809 = var17;
                    Class10117.aClass136_1413.aClass109_Sub1_1770 = var15;
                    return Class10117.aClass136_1413;
                }
            }
        } catch (RuntimeException var19) {
            throw Class1134.method1067(var19, "pb.A(" + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ',' + var5 + ',' + (var6 != null ? "{...}" : "null") + ',' + var7 + ',' + (var8 != null ? "{...}" : "null") + ',' + var9 + ',' + var10 + ',' + var11 + ')');
        }
    }

    final Class136 method1697(int var1, int var2, Class1047 var3, int var4, Class954 var5, int var6, int[][] var7, boolean var8, int var9, int var10, int[][] var11, int var12, int var13, int var14) {
        try {
            if (var10 != 8308) {
                method1694(false);
            }

            long var15;
            if (Class1012.aBoolean_617) {
                if (integer_910 != null) {
                    var15 = (long) ((var13 << 3) + ((objectId << 10) - -var6));
                } else {
                    var15 = (long) (var6 + (objectId << 10));
                }

                Class948 var23 = (Class948) Class154.aClass93_1965.get(var15);
                if (var23 == null) {
                    var23 = method1695(var6, true, true, var13);
                    if (null == var23) {
                        return null;
                    }

                    var23.method1908();
                    var23.method1920(false, false, false, true, false, false, true);
                    Class154.aClass93_1965.put(var23, var15);
                }

                boolean var19 = false;
                Class948 var22 = var23;
                if (null != var5) {
                    var22 = (Class948) var5.method2056(var12, var9, var14, var6, var23, 3);
                    var19 = true;
                }

                if (~var13 == -11 && 3 < var6) {
                    if (!var19) {
                        var22 = (Class948) var22.method1890(true, true, true);
                        var19 = true;
                    }

                    var22.method1876(256);
                }

                if (var8) {
                    Class10117.aClass136_1413.aClass109_Sub1_1770 = var22.method1933(var3);
                } else {
                    Class10117.aClass136_1413.aClass109_Sub1_1770 = null;
                }

                if (aByte1505 != 0) {
                    if (!var19) {
                        var19 = true;
                        var22 = (Class948) var22.method1890(true, true, true);
                    }

                    var22.method1919(aByte1505, aShort1500, var23, var7, var11, var2, var4, var1);
                }

                Class10117.aClass136_1413.aClass140_1777 = var22;
                return Class10117.aClass136_1413;
            } else {
                if (integer_910 == null) {
                    var15 = (long) ((objectId << 10) + var6);
                } else {
                    var15 = (long) (var6 + (objectId << 10) + (var13 << 3));
                }

                Class1049 var17 = (Class1049) Class154.aClass93_1965.get(var15);
                if (var17 == null) {
                    Model var18 = method1686(var6, var13, -1);
                    if (var18 == null) {
                        return null;
                    }

                    var17 = new Class1049(var18, 64 + lightness, shading * 5 + 768, -50, -10, -50);
                    Class154.aClass93_1965.put(var17, var15);
                }

                boolean var21 = false;
                if (var5 != null) {
                    var21 = true;
                    var17 = (Class1049) var5.method2054(19749, var9, var12, var17, var6, var14);
                }

                if (-11 == ~var13 && var6 > 3) {
                    if (!var21) {
                        var21 = true;
                        var17 = (Class1049) var17.method1890(true, true, true);
                    }

                    var17.method1876(256);
                }

                if (aByte1505 != 0) {
                    if (!var21) {
                        var17 = (Class1049) var17.method1890(true, true, true);
                        var21 = true;
                    }

                    var17 = var17.method1941(aByte1505, aShort1500, var7, var11, var2, var4, var1, false);
                }

                Class10117.aClass136_1413.aClass140_1777 = var17;
                return Class10117.aClass136_1413;
            }
        } catch (RuntimeException var20) {
            throw Class1134.method1067(var20, "pb.M(" + var1 + ',' + var2 + ',' + (var3 != null ? "{...}" : "null") + ',' + var4 + ',' + (var5 != null ? "{...}" : "null") + ',' + var6 + ',' + (var7 != null ? "{...}" : "null") + ',' + var8 + ',' + var9 + ',' + var10 + ',' + (var11 != null ? "{...}" : "null") + ',' + var12 + ',' + var13 + ',' + var14 + ')');
        }
    }

    final Class1008 method1698(Class1008 var1, int var2, int var3) {
        try {
            if (var2 != -23085) {
                method1688(108, -11, 57);
            }

            if (null != aClass130_1501) {
                Class1030 var4 = (Class1030) aClass130_1501.get((long) var3);
                return var4 == null ? var1 : var4.value;
            } else {
                return var1;
            }
        } catch (RuntimeException var5) {
            throw Class1134.method1067(var5, "pb.E(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ',' + var3 + ')');
        }
    }

    static final ObjectDefinition getDefinition(int objectId) {
        ObjectDefinition def = (ObjectDefinition) Class969.aClass93_21.get((long) objectId);
        if (def != null)
            return def;
        byte[] data = Class85.aClass153_1171.getFile(6, objectId);
        def = new ObjectDefinition();
        def.objectId = objectId;
        if (data != null) {
            def.method_203(new ByteBuffer(data));
        }
        def.method1689();
        if (!Class14.aBoolean337 && def.aBoolean1491) {
            def.interactions = null;
        }
        if (def.aBoolean1498) {
            def.clipType = 0;
            def.projectileCliped = false;
        }
        
        //bushtree
        
        /*
         * MAY ALSO POSSIBLY BE:
         * protected short[] aShortArray1476; x
    	 * protected short[] aShortArray1477;
    	 * 
    	 * or
    	 * 
    	 * protected short[] aShortArray1495;
    	 * protected short[] aShortArray1506;
    	 * 
    	 * 
    	 * protected int[] integer_910;
    protected int[] integer_233;
    protected int[] anIntArray1524;
    protected int[] anIntArray1539;
         */
        
        if (def.objectId == 1088) {
            def.aShortArray1476 = new short[1];
            def.aShortArray1495 = new short[1];
            def.aShortArray1476[0] = 681;
            //System.out.println("def.aShortArray1476[0]: " + def.aShortArray1476[0]);
            def.aShortArray1495[0] = 59;
           
        }
        if (def.integer_233 != null && def.integer_233[0] == 15551) {
            int id = 26860;
            if (ObjectDefinition.getDefinition(id).integer_233 != null)
                System.out.println("id: " + id + " model: " + ObjectDefinition.getDefinition(id).integer_233[0]);
            if (ObjectDefinition.getDefinition(26858).integer_233 != null)
                System.out.println("id: " + 26858 + " model: " + ObjectDefinition.getDefinition(26858).integer_233[0]);
            if (ObjectDefinition.getDefinition(26862).integer_233 != null)
                System.out.println("id: " + 26862 + " model: " + ObjectDefinition.getDefinition(26862).integer_233[0]);

        }
        Class969.aClass93_21.put(def, (long) objectId);
        if (Class922.debug_object_models) {
            if (def.integer_233 != null) {
                def.interactions = new Class1008[5];
                def.interactions[0] = Class943.create("modelId: " + def.integer_233[0]);
            }
        }
        return def;
    }

    public ObjectDefinition() {
        integer_34 = Class3_Sub13_Sub13.aClass94_3150;
        aBoolean1503 = true;
        anInt1493 = -1;
        anInt1515 = 0;
        anInt1516 = -1;
        aByte1505 = 0;
        aBoolean1491 = false;
        anInt1517 = -1;
        anInt1496 = 0;
        anInt1518 = 0;
        minimapSprite = -1;
        aBoolean1510 = false;
        anInt1520 = -1;
        aShort1500 = -1;
        anInt1481 = 128;
        interactions = new Class1008[5];
        anInt1479 = 128;
        aBoolean1492 = true;
        anInt1488 = 128;
        aBoolean1498 = false;
        isInteractive = -1;
        aBoolean1530 = false;
        aBoolean1525 = true;
        anInt1532 = -1;
        anInt1522 = -1;
        anInt1533 = 0;
        projectileCliped = true;
        anInt1534 = 0;
        anInt1478 = 0;
        anInt1528 = 16;
        aBoolean1537 = false;
        anInt1511 = 0;
        anInt1484 = 0;
        shading = 0;
        animationId = -1;
        aBoolean1507 = false;
        anInt1512 = -1;
        clipType = 2;
        aBoolean1536 = false;
        anInt1526 = -1;
        anInt1540 = -1;
        delayShading = false;
        aBoolean1542 = false;
    }

}

package com.kit.config;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;

public class XMLConfigData {

    public static double getXMLConfigData() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        URL url = new URL("http://smite.io/game_assets/gamepacks/config.xml");
        DocumentBuilder db = factory.newDocumentBuilder();
        Document doc = db.parse(url.openStream());
        NodeList nList = doc.getElementsByTagName("data");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                cacheVersion = Double.parseDouble(eElement
                        .getElementsByTagName("cache_version")
                        .item(0)
                        .getTextContent());
                hsData = String.format(eElement
                        .getElementsByTagName("hs_data")
                        .item(0)
                        .getTextContent());
                query = String.format(eElement
                        .getElementsByTagName("hs_query")
                        .item(0)
                        .getTextContent());
                query = String.format(eElement
                        .getElementsByTagName("hs_query")
                        .item(0)
                        .getTextContent());
                discord_invite = String.format(eElement
                        .getElementsByTagName("invite")
                        .item(0)
                        .getTextContent());
            }
            System.out.println("Successfully grabbed xml data from webhost.");
        }
        return cacheVersion;
    }

    public static double cacheVersion;
    public static String hsData;
    public static String query;
    public static String discord_invite;
}

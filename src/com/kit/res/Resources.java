package com.kit.res;

import javax.swing.*;

public class Resources {

    boolean collected_resources = false;

    public final String[] SKILL_NAMES = {"null", "Attack", "Strength", "Defence", "Ranged", "Prayer", "Magic", "Runecrafting", "Hitpoints", "Agility", "Herblore", "Theiving", "Crafting", "Fletching", "Slayer", "Hunter", "Mining", "Smithing", "Fishing", "Cooking", "Firemaking", "Woodcutting", "Farming", "Construction"};
    public final String BASE_URL_PATH = "http://smite.io/game_assets/oskit/";
    public final String[] URL_PATHS = {
            "osk_logo.png", "osb_settings_h.png", "osb_settings.png", "osb_camera_h.png", "osb_camera.png", //1-5/
            "osb_discord_h.png", "osb_discord.png", "osb_links.png", "osb_links_h.png", "titleBar_close_h.png", //6-10
            "titleBar_close.png", "titleBar_maximize_h.png", "titleBar_maximize.png", "titleBar_minimize_h.png", "titleBar_minimize.png", //11-15
            "statsbar.png", "titleBar_expand.png", "titleBar_collapse2.png", "osb_news_h.png", "osb_news.png", // 16-20
            "osb_world_h.png", "osb_world.png", "osb_highscores_h.png", "osb_highscores.png", // 21-24
            "osb_tracker_h.png", "osb_tracker.png", "osb_ge_h.png", "osb_ge.png", "osb_kills_h.png", "osb_kills.png", // 24-30
            "osb_farming_h.png", "osb_farming.png", "osb_calc_h.png", "osb_calc.png", "osb_timer_h.png", "osb_timer.png",
            "osb_notes_h.png", "osb_notes.png", "osb_twitch_h.png", "osb_twitch.png", "osk_opengl.png", "osk_opengl_h.png", "osk_opengl_on.png"
    };

    public ImageIcon[] icons = new ImageIcon[URL_PATHS.length];

    public boolean hasCollected_resources() {
        return collected_resources;
    }

    public void setCollected_resources(boolean collected_resources) {
        this.collected_resources = collected_resources;
    }
}

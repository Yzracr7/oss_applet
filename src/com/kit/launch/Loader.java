package com.kit.launch;

import com.kit.Engine;
import com.kit.gui.gui.Application;
import com.kit.config.XMLConfigData;
import demo.ClientNoOSKitLaunch;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;

public class Loader {

    JFrame loader_frame;
    Engine engine;

    public Loader(boolean developer_no_oskit) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    System.setProperty("http.agent", "Chrome");
                    try {
                        if (developer_no_oskit){
                            ClientNoOSKitLaunch.launch();
                            return;
                        }
                        engine = new Engine();
                        if (!engine.getResources().hasCollected_resources()) {
                            loader_frame = new JFrame();
                            if (System.getProperty("os.name").contains("ac")) {
                                loader_frame.setSize(320, 100);
                            } else if (System.getProperty("os.name").contains("indow")) {
                                loader_frame.setSize(328, 109);
                            }
                            loader_frame.setTitle("Smite");
                            loader_frame.setResizable(false);
                            loader_frame.setLocationRelativeTo(null);
                            JPanel down = new JPanel();
                            down.setLayout(null);
                            down.setBackground(Color.black);
                            loader_frame.add(down, BorderLayout.CENTER);
                            loader_frame.setVisible(true);
                            method_890(0, "Checking for updates");
                            method_890(25, "Locating Media");
                            double newest = XMLConfigData.getXMLConfigData();
                            System.out.println("OSKit Version ~ " + newest);
                            method_394(engine);
                            engine.getResources().setCollected_resources(true);
                            loader_frame.dispose();
                            new Application(engine);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void method_890(int i, String s) {
        Graphics graphics = loader_frame.getContentPane().getGraphics();

        while (graphics == null) {
            graphics = loader_frame.getContentPane().getGraphics();
            try {
                loader_frame.getContentPane().repaint();
            } catch (Exception _ex) {
            }
            try {
                Thread.sleep(1000L);
            } catch (Exception _ex) {
            }
        }
        int minusX = 223;
        int minusY = 253;

        java.awt.Font font = new java.awt.Font("Helvetica", 1, 13);
        FontMetrics fontmetrics = getLoader_frame().getContentPane().getFontMetrics(font);
        loader_frame.getContentPane().getFontMetrics(new java.awt.Font("Helvetica", 0, 13));
        graphics.setColor(Color.black);
        graphics.fillRect(0, 0, loader_frame.getWidth(), loader_frame.getHeight());

        int j = 585 / 2 - 18;
        graphics.setColor(Color.decode("0x980000"));
        graphics.drawRect(767 / 2 - 152 - minusX, j - minusY, 304, 34);
        graphics.fillRect(767 / 2 - 150 - minusX, j + 2 - minusY, i * 3, 30);
        graphics.setColor(Color.black);
        graphics.fillRect((767 / 2 - 150) + i * 3 - minusX, j + 2 - minusY, 300 - i * 3, 30);
        graphics.setFont(font);
        graphics.setColor(Color.white);
        int byte1 = 20;
        int centerX = 450 / 2, centerY = 200 / 2;
        graphics.drawString(s, (444 - fontmetrics.stringWidth(s)) / 2 - 59, (centerY + 4 - 41) - byte1);
    }

    public JFrame getLoader_frame() {
        return loader_frame;
    }

    public void method_394(Engine engine) {
        try {
            for (int i = 0; i < engine.getResources().URL_PATHS.length; i++) {
                method_890((100 - engine.getResources().URL_PATHS.length) + (i), "Fetching assets");
                URL url = new URL(engine.getResources().BASE_URL_PATH + "" + engine.getResources().URL_PATHS[i]);
                BufferedImage img = ImageIO.read(url);
                engine.getResources().icons[i] = new ImageIcon(img);
            }
            System.out.println("OSKit has loaded successfully loaded its resources...");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static URLClassLoader classLoader;
    private static String MAIN_CLASS = "com.Class122";
    private final static String JAR_URL = "http://smite.io/gamepacks/game.jar";

    private static int size; // size of download in bytes
    private static int downloaded; // number of bytes downloaded

    private static Applet grabJAR() throws ClassNotFoundException,
            InstantiationException, IllegalAccessException, IOException {
        final URL url = new URL(JAR_URL);
        final InputStream is = url.openStream();
        final byte[] b = new byte[2048];
        int length;
        final HttpURLConnection connection = (HttpURLConnection) url
                .openConnection();

        // Specify what portion of file to download.
        connection.setRequestProperty("Range", "bytes=" + downloaded + "-");

        // Connect to server.
        connection.connect();

        // Make sure response code is in the 200 range.
        if ((connection.getResponseCode() / 100) != 2) {
            Logger.getGlobal().info("Unable to find file");
            return null;
        }

        // set content length.
        size = connection.getContentLength();
        while ((length = is.read(b)) != -1) {
            downloaded += length;
        }
        is.close();
        classLoader = new URLClassLoader(new URL[]{(url)});
        final Applet client = (Applet) classLoader.loadClass(MAIN_CLASS)
                .newInstance();
        client.init();
        client.start();
        return client;
    }
}

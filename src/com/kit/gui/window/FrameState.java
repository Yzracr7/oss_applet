package com.kit.gui.window;

public enum FrameState {
    FIXED, RESIZABLE, FULLSCREEN
}

package com.kit.gui.gui;

import com.kit.Engine;
import com.kit.gui.window.FrameState;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.awt.event.*;

public class Settings extends JFrame {

    boolean resizing = false;
    Point point = new Point();

    JFrame settings_frame;
    JLabel settings_title;
    JPanel settings_content;
    JPanel settings_buttons;

    JPanel titleBar = new JPanel();
    JLabel titleBar_icon = new JLabel();
    JButton titleBar_closeButton = new JButton();
    JButton titleBar_maxButton = new JButton();
    JButton titleBar_minButton = new JButton();

    Point applet_position_coords;

    FrameState state = FrameState.FIXED;
    int resizing_side = -1;

    public Settings(Engine e, boolean visible) {
        this.settings_frame = new JFrame();
        this.settings_title = new JLabel("Settings");
        this.settings_content = new JPanel();
        this.settings_buttons = new JPanel();

        GraphicsEnvironment env =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        UIDefaults uid = UIManager.getDefaults();
        ColorUIResource thumbColor = new ColorUIResource(new Color(34, 34, 34));
        uid.put("ScrollBar.thumb", thumbColor);
        uid.put("ScrollBar.track", thumbColor);
        uid.put("scrollbar", thumbColor);
        uid.put("ScrollBar.border", BorderFactory.createEmptyBorder());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        settings_frame.setLocationRelativeTo(null);
        settings_content = new JPanel();
        settings_content.setBackground(new Color(34, 34, 34));
        settings_content.setBorder(new EmptyBorder(5, 5, 5, 5));
        settings_frame.setContentPane(settings_content);
        settings_content.setLayout(null);
        settings_frame.setUndecorated(true);
        settings_frame.setIconImage(e.getResources().icons[0].getImage());
        settings_frame.setTitle("Settings");
        settings_frame.setResizable(false);
        settings_frame.setBounds(50, 50, 573, 336);
        settings_frame.setSize(573, 336);
        settings_frame.setMaximumSize(env.getMaximumWindowBounds().getSize());
        settings_frame.setMinimumSize(new Dimension(573, 336));
        settings_frame.setVisible(visible);

        titleBar.setOpaque(false);
        titleBar.setBounds(0, 0, settings_frame.getWidth(), 27);
        titleBar.setLayout(null);
        titleBar.setVisible(true);

        settings_title.setHorizontalAlignment(SwingConstants.CENTER);
        settings_title.setForeground(Color.LIGHT_GRAY);
        settings_title.setFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 11));
        settings_title.setBounds(10, 1, 80, 20);
        titleBar.add(settings_title);

        titleBar_icon.setIcon(new ImageIcon(e.getResources().icons[0].getImage().getScaledInstance(16, 16, 500)));
        titleBar_icon.setBounds(5, 0, 36, 24);
        titleBar.add(titleBar_icon);

        titleBar_closeButton.setRolloverIcon(new ImageIcon(e.getResources().icons[9].getImage()));
        titleBar_closeButton.setIcon(new ImageIcon(e.getResources().icons[10].getImage()));
        titleBar_closeButton.setFocusPainted(false);
        titleBar_closeButton.setContentAreaFilled(false);
        titleBar_closeButton.setBorderPainted(false);
        titleBar_closeButton.setBorder(null);
        titleBar_closeButton.setBounds(settings_frame.getWidth() - 39, 0, 30, 18);
        titleBar_closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                settings_frame.dispose();
            }
        });
        titleBar.add(titleBar_closeButton);

        titleBar_maxButton.setRolloverIcon(new ImageIcon(e.getResources().icons[11].getImage()));
        titleBar_maxButton.setIcon(new ImageIcon(e.getResources().icons[12].getImage()));
        titleBar_maxButton.setFocusPainted(false);
        titleBar_maxButton.setContentAreaFilled(false);
        titleBar_maxButton.setBorderPainted(false);
        titleBar_maxButton.setBorder(null);
        titleBar_maxButton.setBounds(titleBar_closeButton.getX() - 27, 0, 30, 18);
        titleBar_maxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (settings_frame.getMaximumSize() != settings_frame.getSize() && state != FrameState.RESIZABLE) {
                    settings_frame.setMaximizedBounds(env.getMaximumWindowBounds());
                    settings_frame.setExtendedState(settings_frame.getExtendedState() | settings_frame.MAXIMIZED_BOTH);
                    state = FrameState.RESIZABLE;
                } else {
                    state = FrameState.FIXED;
                    refreshWindow();
                }
                refreshAssets();
            }
        });
        titleBar.add(titleBar_maxButton);

        titleBar_minButton.setRolloverIcon(new ImageIcon(e.getResources().icons[13].getImage()));
        titleBar_minButton.setIcon(new ImageIcon(e.getResources().icons[14].getImage()));
        titleBar_minButton.setFocusPainted(false);
        titleBar_minButton.setContentAreaFilled(false);
        titleBar_minButton.setBorderPainted(false);
        titleBar_minButton.setBorder(null);
        titleBar_minButton.setBounds(titleBar_maxButton.getX() - 20, 0, 30, 18);
        titleBar_minButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                settings_frame.setState(JFrame.ICONIFIED);
            }
        });
        titleBar.add(titleBar_minButton);

        //Below is listeners don't touch

        titleBar.addMouseListener(new MouseListener() {
            public void mouseReleased(MouseEvent e) {
                applet_position_coords = null;
            }

            public void mousePressed(MouseEvent e) {
                applet_position_coords = e.getPoint();
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseClicked(MouseEvent e) {
            }
        });
        titleBar.addMouseMotionListener(new MouseMotionListener() {
            public void mouseMoved(MouseEvent e) {
            }

            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                settings_frame.setLocation(currCoords.x - applet_position_coords.x, currCoords.y - applet_position_coords.y);
            }
        });

        settings_frame.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, new Color(34, 34, 34)));
        settings_frame.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                resizing = !settings_frame.getCursor().equals(Cursor.getDefaultCursor());
                if (!e.isMetaDown()) {
                    point.x = e.getX();
                    point.y = e.getY();
                    if (point.x > settings_frame.getContentPane().getWidth() - 5) {
                        resizing_side = 1;
                    } else if (point.y > settings_frame.getContentPane().getHeight() - 5) {
                        resizing_side = 2;
                    }
                }
            }
        });

        settings_frame.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                if (resizing && resizing_side != -1) {
                    Point pt = e.getPoint();
                    if (resizing_side == 1) {
                        settings_frame.setSize(settings_frame.getWidth() + pt.x - point.x, settings_frame.getHeight());
                    } else if (resizing_side == 2) {
                        settings_frame.setSize(settings_frame.getWidth(), settings_frame.getHeight() + ((pt.y - point.y) / 30));
                    }
                    point.x = pt.x;
                    refreshAssets();
                }
            }

            public void mouseMoved(MouseEvent me) {
                Point cursorLocation = me.getPoint();
                int xPos = cursorLocation.x;
                int yPos = cursorLocation.y;
                if (xPos > settings_frame.getContentPane().getWidth() - 5)
                    settings_frame.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
                else if (yPos > settings_frame.getContentPane().getHeight() - 5)
                    settings_frame.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
                else if (xPos > settings_frame.getContentPane().getWidth() - 5 && yPos > settings_frame.getContentPane().getHeight() - 5)
                    settings_frame.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
                else if (!settings_frame.getCursor().equals(Cursor.getDefaultCursor()))
                    settings_frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });
        settings_content.add(titleBar);
        refreshWindow();
    }

    public void refreshWindow() {
        switch (state) {
            case FIXED:
                settings_frame.setSize(573, 336);
                break;
        }
    }

    public void refreshAssets() {
        titleBar.setBounds(0, 0, settings_frame.getWidth() + 10, 24);
        titleBar_icon.setBounds(5, 0, 36, 24);
        titleBar_closeButton.setBounds(settings_frame.getWidth() - 40, 0, 30, 18);
        titleBar_maxButton.setBounds(titleBar_closeButton.getX() - 27, 0, 30, 18);
        titleBar_minButton.setBounds(titleBar_maxButton.getX() - 20, 0, 30, 18);
    }
}

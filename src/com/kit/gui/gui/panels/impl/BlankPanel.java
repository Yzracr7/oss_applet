package com.kit.gui.gui.panels.impl;

import com.kit.Engine;

import javax.swing.*;

public class BlankPanel extends JPanel {

    Engine engine;

    JTextField stat_entryName;
    JLabel stat_name;


    public BlankPanel(Engine e) {
        this.engine = e;
        this.stat_entryName = new JTextField();
        this.stat_name = new JLabel("Stat: Sailing");

        this.setOpaque(false);
        this.setLayout(null);
        this.setVisible(true);
    }
}

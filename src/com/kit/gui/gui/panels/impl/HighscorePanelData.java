package com.kit.gui.gui.panels.impl;

import javax.swing.*;

public class HighscorePanelData {

    protected double aBoolean_92[] = new double[23];
    protected static String aString_0122 = "overall_xp";
    protected static String aString_0123 = "attack_xp";
    protected static String aString_0125 = "strength_xp";
    protected static String aString_0124 = "defence_xp";
    protected static String aString_0127 = "ranged_xp";
    protected static String aString_0128 = "prayer_xp";
    protected static String aString_0129 = "magic_xp";
    protected static String aString_0143 = "runecrafting_xp";
    protected static String aString_0126 = "hitpoints_xp";
    protected static String aString_0139 = "agility_xp";
    protected static String aString_0138 = "herblore_xp";
    protected static String aString_0140 = "thieving_xp";
    protected static String aString_0135 = "crafting_xp";
    protected static String aString_0132 = "fletching_xp";
    protected static String aString_0141 = "slayer_xp";
    protected static String aString_0144 = "hunter_xp";

    protected static String aString_0137 = "mining_xp";
    protected static String aString_0136 = "smithing_xp";
    protected static String aString_0133 = "fishing_xp";
    protected static String aString_0130 = "cooking_xp";
    protected static String aString_0134 = "firemaking_xp";
    protected static String aString_0131 = "woodcutting_xp";
    protected static String aString_0142 = "farming_xp";
    protected static String aString_0145 = "construction_xp";

    protected static String[] aString_Comp = new String[25];

    public static void main(String[] args) {
       /* try {
            Class.forName(Class1021.aString_0223);
            String url = Class947.hsData;
            Connection conn = DriverManager.getConnection(url, Class1006.aString_0147, Class1027.aString_0146);
            Statement stmt = conn.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery(Class947.query + args[0] + "'");
            while (rs.next()) {
                String ov = rs.getString(aString_0122);
                aString_Comp[0] = ov;
                String att = rs.getString(aString_0123);
                aString_Comp[1] = att;
                String str = rs.getString(aString_0125);
                aString_Comp[2] = str;
                String def = rs.getString(aString_0124);
                aString_Comp[3] = def;
                String range = rs.getString(aString_0127);
                aString_Comp[4] = range;
                String prayer = rs.getString(aString_0128);
                aString_Comp[5] = prayer;
                String cc = rs.getString(aString_0129);
                aString_Comp[6] = cc;
                String qy = rs.getString(aString_0143);
                aString_Comp[7] = qy;
                String qe = rs.getString(aString_0126);
                aString_Comp[8] = qe;
                String ue = rs.getString(aString_0139);
                aString_Comp[9] = ue;
                String mq = rs.getString(aString_0138);
                aString_Comp[10] = mq;
                String ih = rs.getString(aString_0140);
                aString_Comp[11] = ih;
                String cb = rs.getString(aString_0135);
                aString_Comp[12] = cb;
                String cn = rs.getString(aString_0132);
                aString_Comp[13] = cn;
                String nm = rs.getString(aString_0141);
                aString_Comp[14] = nm;
                String july = rs.getString(aString_0144);
                aString_Comp[15] = july;
                String sept = rs.getString(aString_0137);
                aString_Comp[16] = sept;
                String jl = rs.getString(aString_0136);
                aString_Comp[17] = jl;
                String qg = rs.getString(aString_0133);
                aString_Comp[18] = qg;
                String gd = rs.getString(aString_0130);
                aString_Comp[19] = gd;
                String id = rs.getString(aString_0134);
                aString_Comp[20] = id;
                String xo = rs.getString(aString_0131);
                aString_Comp[21] = xo;
                String vd = rs.getString(aString_0142);
                aString_Comp[22] = vd;
                String va = rs.getString(aString_0145);
                aString_Comp[23] = va;
                Class926.method_313(false, new JLabel[]{new JLabel("" + ov), method_902(Integer.parseInt(att)), method_902(Integer.parseInt(str)), method_902(Integer.parseInt(def)), method_902(Integer.parseInt(range)), method_902(Integer.parseInt(prayer)), method_902(Integer.parseInt(cc)), method_902(Integer.parseInt(qy)), method_902(Integer.parseInt(qe)), method_902(Integer.parseInt(ue)), method_902(Integer.parseInt(mq)), method_902(Integer.parseInt(ih)), method_902(Integer.parseInt(cb)), method_902(Integer.parseInt(cn)), method_902(Integer.parseInt(nm)), method_902(Integer.parseInt(july)), method_902(Integer.parseInt(sept)), method_902(Integer.parseInt(jl)), method_902(Integer.parseInt(qg)), method_902(Integer.parseInt(gd)), method_902(Integer.parseInt(id)), method_902(Integer.parseInt(xo)), method_902(Integer.parseInt(vd)), method_902(Integer.parseInt(va))});
            }
            conn.close();
        } catch (Exception e) {
            System.err.println("exception");
            System.err.println(e.getMessage());
        }*/
    }

    public static int method_903(int x) {
        double io = x;
        int re = 0;
        int qq = 0;
        for (int ty = 1; ty < 100; ty++) {
            re += Math.floor((double) ty + 300.0 * Math.pow(2.0, (double) ty / 7.0));
            qq = (int) Math.floor(re / 4);
            if ((qq - 1) >= io) {
                return ty;
            }
        }
        return 99;
    }

    public static JLabel method_902(int x) {
        double io = x;
        int re = 0;
        int qq = 0;
        for (int ty = 1; ty < 100; ty++) {
            re += Math.floor((double) ty + 300.0 * Math.pow(2.0, (double) ty / 7.0));
            qq = (int) Math.floor(re / 4);
            if ((qq - 1) >= io) {
                return new JLabel("" + ty);
            }
        }
        return new JLabel("" + 99);
    }
}

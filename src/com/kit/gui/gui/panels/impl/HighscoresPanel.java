package com.kit.gui.gui.panels.impl;

import com.kit.Engine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HighscoresPanel extends JPanel {

    Engine engine;

    JTextField stat_entryName;
    JLabel stat_name;

    public HighscoresPanel(Engine e) {
        this.engine = e;
        this.stat_entryName = new JTextField();
        this.stat_name = new JLabel("Stat: Sailing");

        this.setOpaque(false);
        this.setLayout(null);
        this.setVisible(true);

        JLabel hiscores_background = new JLabel("");
        hiscores_background.setIcon(new ImageIcon(e.getResources().icons[15].getImage()));
        hiscores_background.setBounds(2, 12, 223, 451);
        this.add(hiscores_background);

        stat_entryName.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("Action performed.. Please wait... [arg] " + arg0);
                stat_name.setText("Please wait...");
                //HighscorePanelData.main(new String[]{arg0.getActionCommand()});
            }
        });
        stat_entryName.setCaretColor(Color.LIGHT_GRAY);
        stat_entryName.setSelectedTextColor(Color.LIGHT_GRAY);
        stat_entryName.setSelectionColor(new Color(234, 109, 34));
        stat_entryName.setForeground(Color.LIGHT_GRAY);
        stat_entryName.setFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 12));
        stat_entryName.setOpaque(false);
        stat_entryName.setBorder(null);
        stat_entryName.setBounds(11, 18, 183, 20);
        stat_entryName.setColumns(10);
        this.add(stat_entryName);
        }
}

package com.kit.gui.gui.panels;

import com.kit.Engine;
import com.kit.gui.gui.panels.impl.BlankPanel;
import com.kit.gui.gui.panels.impl.HighscoresPanel;

import javax.swing.*;
import java.awt.event.InputEvent;

public class TabSwitchActionEvent {

    Engine engine;
    HighscoresPanel highscores;
    BlankPanel blank;

    private static final int OPTION_MASK = InputEvent.ALT_MASK | InputEvent.SHIFT_MASK;

    public TabSwitchActionEvent(Engine e) {
        this.engine = e;
        this.highscores = new HighscoresPanel(e);
        this.blank = new BlankPanel(e);
    }

    public void jActionPerformed(int button, java.awt.event.ActionEvent evt) {
        System.out.println("Retrieving Button Panel Action [" + engine + "] : [" + button + "]");
        if (evt != null && (evt.getModifiers() & OPTION_MASK) == OPTION_MASK) {
            System.out.println("[Action Button Event] [ALT] [SHIFT] " + evt);
            return;
        }
        if (engine != null) {
            switch (button) {
                case HISCORES_TAB:
                    blank.setVisible(false);
                    highscores.setVisible(true);
                    engine.getApp().setWidget_panel(highscores);
                    break;
                case CALCULATOR_TAB:
                    highscores.setVisible(false);
                    blank.setVisible(true);
                    engine.getApp().setWidget_panel(blank);
                    System.out.println("Calculator here");
                    break;
            }
            engine.getApp().refreshAssets();
        }
    }

    public static final int HISCORES_TAB = 0;
    public static final int CALCULATOR_TAB = 1;
}
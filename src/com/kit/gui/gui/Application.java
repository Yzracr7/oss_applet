package com.kit.gui.gui;

import com.kit.Engine;
import com.kit.client.Class1012;
import com.kit.client.Class122;
import com.kit.gui.gui.panels.TabSwitchActionEvent;
import com.kit.gui.window.FrameState;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.awt.event.*;

public class Application extends JFrame {
    FrameState state = FrameState.FIXED;

    Point point = new Point();
    Engine engine;
    Settings settings;
    Class122 game;
    TabSwitchActionEvent tabs;

    int resizing_side = -1;
    boolean panels_expanded;

    JPanel game_window;
    JPanel widget_panel;
    JPanel button_bar;
    JFrame application_frame;
    JLabel application_title;
    JPanel application_content;
    JPanel application_buttons;

    Point applet_position_coords;

    JPanel title_bar = new JPanel();
    JLabel titleBar_icon = new JLabel();

    JButton titleBar_closeButton = new JButton();
    JButton titleBar_maxButton = new JButton();
    JButton titleBar_minButton = new JButton();
    JButton titleBar_settingsButton = new JButton();
    JButton titleBar_collapseButton = new JButton();
    JButton titleBar_linksButton = new JButton();
    JButton titleBar_discordButton = new JButton();
    JButton titleBar_cameraButton = new JButton();
    JButton titleBar_openGLButton = new JButton();

    JButton hiscores_button = new JButton();
    JButton calculator_button = new JButton();

    void initiateGameWindow() {
        if (Class1012.aBoolean_617) {
            titleBar_openGLButton.setIcon(new ImageIcon(engine.getResources().icons[42].getImage()));
        } else {
            titleBar_openGLButton.setIcon(new ImageIcon(engine.getResources().icons[40].getImage()));
        }
        System.out.println("Initializing smite old school");
        game = new Class122("1");
        game.setBounds(0, 0, 765, 503);
        game_window.add(game);
        game.start();
        game.setLayout(null);
        game_window.setVisible(true);
    }

    public Application(Engine e) {
        this.engine = e;
        this.tabs = new TabSwitchActionEvent(e);
        this.game_window = new JPanel();
        this.widget_panel = new JPanel();
        this.button_bar = new JPanel();
        this.application_frame = new JFrame();
        this.application_title = new JLabel("OSKit : http://smite.io");
        this.application_content = new JPanel();
        this.application_buttons = new JPanel();

        e.setApp(this);
        tabs.jActionPerformed(0, null);
        GraphicsEnvironment env =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        UIDefaults uid = UIManager.getDefaults();
        ColorUIResource thumbColor = new ColorUIResource(new Color(34, 34, 34));
        uid.put("ScrollBar.thumb", thumbColor);
        uid.put("ScrollBar.track", thumbColor);
        uid.put("scrollbar", thumbColor);
        uid.put("ScrollBar.border", BorderFactory.createEmptyBorder());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        application_frame.setLocationRelativeTo(null);
        application_content = new JPanel();
        application_content.setBackground(new Color(34, 34, 34));
        application_content.setBorder(new EmptyBorder(5, 5, 5, 5));
        application_frame.setContentPane(application_content);
        application_content.setLayout(null);
        application_frame.setUndecorated(true);
        application_frame.setIconImage(e.getResources().icons[0].getImage());
        application_frame.setTitle("OSKit");
        application_frame.setResizable(false);
        application_frame.setBounds(100, 100, 1037, 540);
        application_frame.setSize(773, 540);
        application_frame.setMaximumSize(env.getMaximumWindowBounds().getSize());
        application_frame.setMinimumSize(new Dimension(773, 540));

        title_bar.setOpaque(false);
        title_bar.setBounds(0, 0, application_frame.getWidth(), 27);
        title_bar.setLayout(null);
        title_bar.setVisible(true);

        application_title.setHorizontalAlignment(SwingConstants.CENTER);
        application_title.setForeground(Color.LIGHT_GRAY);
        application_title.setFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 11));
        application_title.setBounds(10, 0, 150, 25);
        title_bar.add(application_title);

        titleBar_icon.setIcon(new ImageIcon(e.getResources().icons[0].getImage().getScaledInstance(16, 16, 500)));
        titleBar_icon.setBounds(5, 0, 36, 24);
        title_bar.add(titleBar_icon);

        titleBar_closeButton.setRolloverIcon(new ImageIcon(e.getResources().icons[9].getImage()));
        titleBar_closeButton.setIcon(new ImageIcon(e.getResources().icons[10].getImage()));
        titleBar_closeButton.setFocusPainted(false);
        titleBar_closeButton.setContentAreaFilled(false);
        titleBar_closeButton.setBorderPainted(false);
        titleBar_closeButton.setBorder(null);
        titleBar_closeButton.setBounds(application_frame.getWidth() - 39, 0, 30, 18);
        titleBar_closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String ObjButtons[] = {"Yes", "No"};
                int PromptResult = JOptionPane.showOptionDialog(null,
                        "Are you sure you want to exit?", "Exit OSKit",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
                        ObjButtons, ObjButtons[1]);
                JOptionPane.getRootFrame().setBackground(Color.darkGray);
                JOptionPane.getFrameForComponent(getComponent(0)).setBackground(Color.darkGray);
                if (PromptResult == 0) {
                    System.exit(0);
                }
            }
        });
        title_bar.add(titleBar_closeButton);

        titleBar_maxButton.setRolloverIcon(new ImageIcon(e.getResources().icons[11].getImage()));
        titleBar_maxButton.setIcon(new ImageIcon(e.getResources().icons[12].getImage()));
        titleBar_maxButton.setFocusPainted(false);
        titleBar_maxButton.setContentAreaFilled(false);
        titleBar_maxButton.setBorderPainted(false);
        titleBar_maxButton.setBorder(null);
        titleBar_maxButton.setBounds(titleBar_closeButton.getX() - 27, 0, 30, 18);
        titleBar_maxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (application_frame.getMaximumSize() != application_frame.getSize() && state != FrameState.RESIZABLE) {
                    application_frame.setMaximizedBounds(env.getMaximumWindowBounds());
                    application_frame.setExtendedState(application_frame.getExtendedState() | application_frame.MAXIMIZED_BOTH);
                    state = FrameState.RESIZABLE;
                } else {
                    state = FrameState.FIXED;
                    refreshWindow();
                }
                refreshAssets();
            }
        });
        title_bar.add(titleBar_maxButton);

        titleBar_minButton.setRolloverIcon(new ImageIcon(e.getResources().icons[13].getImage()));
        titleBar_minButton.setIcon(new ImageIcon(e.getResources().icons[14].getImage()));
        titleBar_minButton.setFocusPainted(false);
        titleBar_minButton.setContentAreaFilled(false);
        titleBar_minButton.setBorderPainted(false);
        titleBar_minButton.setBorder(null);
        titleBar_minButton.setBounds(titleBar_maxButton.getX() - 20, 0, 30, 18);
        titleBar_minButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                application_frame.setState(JFrame.ICONIFIED);
            }
        });
        title_bar.add(titleBar_minButton);

        titleBar_settingsButton
                .setRolloverSelectedIcon(null);
        titleBar_settingsButton
                .setSelectedIcon(null);
        titleBar_settingsButton.setFocusPainted(false);
        titleBar_settingsButton.setRolloverIcon(new ImageIcon(e.getResources().icons[1].getImage()));
        titleBar_settingsButton.setIcon(new ImageIcon(e.getResources().icons[2].getImage()));
        titleBar_settingsButton.setContentAreaFilled(false);
        titleBar_settingsButton.setBorderPainted(false);
        titleBar_settingsButton.setBorder(null);
        titleBar_settingsButton.setBounds(titleBar_minButton.getX() - 35, 3, 30, 18);
        titleBar_settingsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                settings_open = true;
                showSettings();
            }
        });
        title_bar.add(titleBar_settingsButton);

        titleBar_collapseButton.setFocusPainted(false);
        titleBar_collapseButton.setIcon(new ImageIcon(e.getResources().icons[16].getImage()));
        //titleBar_settingsButton.setRolloverIcon(new ImageIcon(e.getResources().icons[1].getImage()));
        titleBar_collapseButton.setContentAreaFilled(false);
        titleBar_collapseButton.setBorderPainted(false);
        titleBar_collapseButton.setBorder(null);
        titleBar_collapseButton.setBounds(titleBar_settingsButton.getX() - 15, 3, 16, 16);
        titleBar_collapseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                panels_expanded = !panels_expanded;
                button_bar.setVisible(panels_expanded);
                widget_panel.setVisible(panels_expanded);
                if (!panels_expanded) {
                    application_frame.setSize(773, 540);
                } else {
                    application_frame.setSize(1037, 540);
                }
                application_frame.setMinimumSize(new Dimension(panels_expanded ? 1037 : 773, 540));
                refreshWindow();
                refreshAssets();
            }
        });
        title_bar.add(titleBar_collapseButton);

        titleBar_linksButton.setIcon(new ImageIcon(e.getResources().icons[7].getImage()));
        titleBar_linksButton.setRolloverIcon(new ImageIcon(e.getResources().icons[8].getImage()));
        titleBar_linksButton.setFocusPainted(false);
        titleBar_linksButton.setContentAreaFilled(false);
        titleBar_linksButton.setBorderPainted(false);
        titleBar_linksButton.setBorder(null);
        titleBar_linksButton.setBounds(titleBar_collapseButton.getX() - 25, 0, 21, 26);
        title_bar.add(titleBar_linksButton);

        titleBar_discordButton.setRolloverIcon(new ImageIcon(e.getResources().icons[5].getImage()));
        titleBar_discordButton.setIcon(new ImageIcon(e.getResources().icons[6].getImage()));
        titleBar_discordButton.setFocusPainted(false);
        titleBar_discordButton.setContentAreaFilled(false);
        titleBar_discordButton.setBorderPainted(false);
        titleBar_discordButton.setBorder(null);
        titleBar_discordButton.setBounds(titleBar_linksButton.getX() - 25, 0, 21, 26);
        title_bar.add(titleBar_discordButton);

        titleBar_cameraButton.setRolloverIcon(new ImageIcon(e.getResources().icons[3].getImage()));
        titleBar_cameraButton.setIcon(new ImageIcon(e.getResources().icons[4].getImage()));
        titleBar_cameraButton.setFocusPainted(false);
        titleBar_cameraButton.setContentAreaFilled(false);
        titleBar_cameraButton.setBorderPainted(false);
        titleBar_cameraButton.setBorder(null);
        titleBar_cameraButton.setBounds(titleBar_discordButton.getX() - 27, 2, 25, 24);
        title_bar.add(titleBar_cameraButton);

        titleBar_openGLButton.setRolloverIcon(new ImageIcon(e.getResources().icons[41].getImage()));
        titleBar_openGLButton.setIcon(new ImageIcon(e.getResources().icons[40].getImage()));
        titleBar_openGLButton.setFocusPainted(false);
        titleBar_openGLButton.setContentAreaFilled(false);
        titleBar_openGLButton.setBorderPainted(false);
        titleBar_openGLButton.setBorder(null);
        titleBar_openGLButton.setBounds(titleBar_cameraButton.getX() - 25, 2, 25, 24);
        title_bar.add(titleBar_openGLButton);

        // GAME PANEL
        game_window.setOpaque(false);
        game_window.setForeground(Color.black);
        game_window.setBounds(4, 26, 765, 513);

        //BUTTON PANEL
        button_bar.setOpaque(false);
        button_bar.setLayout(null);
        button_bar.setBounds(application_frame.getWidth() - 42, 26, 50, 503);
        button_bar.setVisible(panels_expanded);

        //WIDGET PANEL
        widget_panel.setOpaque(false);
        widget_panel.setLayout(null);
        widget_panel.setBounds(button_bar.getX() - 227, 26, 230, 503);
        widget_panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(44, 44, 44)));
        widget_panel.setVisible(panels_expanded);

        hiscores_button.setRolloverIcon(new ImageIcon(e.getResources().icons[22].getImage()));
        hiscores_button.setIcon(new ImageIcon(e.getResources().icons[23].getImage()));
        hiscores_button.setFocusPainted(false);
        hiscores_button.setContentAreaFilled(false);
        hiscores_button.setBorderPainted(false);
        hiscores_button.setBorder(null);
        hiscores_button.setBounds(3, 0, 31, 30);
        hiscores_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                tabs.jActionPerformed(0, arg0);
            }
        });
        button_bar.add(hiscores_button);

        calculator_button.setRolloverIcon(new ImageIcon(e.getResources().icons[32].getImage()));
        calculator_button.setIcon(new ImageIcon(e.getResources().icons[33].getImage()));
        calculator_button.setFocusPainted(false);
        calculator_button.setContentAreaFilled(false);
        calculator_button.setBorderPainted(false);
        calculator_button.setBorder(null);
        calculator_button.setBounds(3, 30, 31, 30);
        calculator_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                tabs.jActionPerformed(1, arg0);
            }
        });
        button_bar.add(calculator_button);

        //Below is listeners don't touch

        title_bar.addMouseListener(new MouseListener() {
            public void mouseReleased(MouseEvent e) {
                applet_position_coords = null;
            }

            public void mousePressed(MouseEvent e) {
                applet_position_coords = e.getPoint();
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseClicked(MouseEvent e) {
            }
        });
        title_bar.addMouseMotionListener(new MouseMotionListener() {
            public void mouseMoved(MouseEvent e) {
            }

            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                application_frame.setLocation(currCoords.x - applet_position_coords.x, currCoords.y - applet_position_coords.y);
            }
        });

        application_frame.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, new Color(34, 34, 34)));
        application_frame.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                resizing = !application_frame.getCursor().equals(Cursor.getDefaultCursor());
                if (!e.isMetaDown()) {
                    point.x = e.getX();
                    point.y = e.getY();
                    if (point.x > application_frame.getContentPane().getWidth() - 5) {
                        resizing_side = 1;
                    } else if (point.y > application_frame.getContentPane().getHeight() - 5) {
                        resizing_side = 2;
                    }
                }
            }
        });

        application_frame.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                if (resizing && resizing_side != -1) {
                    Point pt = e.getPoint();
                    if (resizing_side == 1) {
                        application_frame.setSize(application_frame.getWidth() + pt.x - point.x, application_frame.getHeight());
                    } else if (resizing_side == 2) {
                        application_frame.setSize(application_frame.getWidth(), application_frame.getHeight() + ((pt.y - point.y) / 30));
                    }
                    point.x = pt.x;
                    refreshAssets();
                }
            }

            public void mouseMoved(MouseEvent me) {
                Point cursorLocation = me.getPoint();
                int xPos = cursorLocation.x;
                int yPos = cursorLocation.y;
                if (xPos > application_frame.getContentPane().getWidth() - 5)
                    application_frame.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
                else if (yPos > application_frame.getContentPane().getHeight() - 5)
                    application_frame.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
                else if (xPos > application_frame.getContentPane().getWidth() - 5 && yPos > application_frame.getContentPane().getHeight() - 5)
                    application_frame.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
                else if (!application_frame.getCursor().equals(Cursor.getDefaultCursor()))
                    application_frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });
        application_content.add(title_bar);
        application_frame.add(game_window);
        application_frame.add(widget_panel);
        application_frame.add(button_bar);
        application_frame.setVisible(true);
        refreshWindow();
        initiateGameWindow();
    }

    public void showSettings() {
        System.out.println("Opening settings console");
        settings = new Settings(engine, true);
        setSettings_open(true);
    }

    public void refreshWindow() {
        switch (state) {
            case FIXED:
                application_frame.setSize(773, 540);
                break;
            case RESIZABLE:
                // application_frame.setSize(773, 532);
                break;
            case FULLSCREEN:
                // application_frame.setSize(773, 532);
                break;
        }
    }

    public void refreshAssets() {
        game_window.setBounds(((application_frame.getWidth()) / 2 - (game_window.getWidth() / 2) - 4) - (panels_expanded ? 130 : 0), 26, 765, 503);
        button_bar.setBounds(application_frame.getWidth() - 42, 26, 50, 503);
        widget_panel.setBounds(button_bar.getX() - 227, 26, 230, 503);

        title_bar.setBounds(0, 0, application_frame.getWidth() + 10, 24);
        titleBar_icon.setBounds(5, 0, 36, 24);
        titleBar_closeButton.setBounds(application_frame.getWidth() - 40, 0, 30, 18);
        titleBar_maxButton.setBounds(titleBar_closeButton.getX() - 27, 0, 30, 18);
        titleBar_minButton.setBounds(titleBar_maxButton.getX() - 20, 0, 30, 18);
        titleBar_settingsButton.setBounds(titleBar_minButton.getX() - 35, 3, 30, 18);
        titleBar_collapseButton.setBounds(titleBar_settingsButton.getX() - 15, 3, 16, 16);
        titleBar_linksButton.setBounds(titleBar_collapseButton.getX() - 25, 0, 21, 26);
        titleBar_discordButton.setBounds(titleBar_linksButton.getX() - 25, 0, 21, 26);
        titleBar_cameraButton.setBounds(titleBar_discordButton.getX() - 27, 2, 25, 24);
        titleBar_openGLButton.setBounds(titleBar_cameraButton.getX() - 25, 2, 25, 24);

        if (Class1012.aBoolean_617) {
            titleBar_openGLButton.setIcon(new ImageIcon(engine.getResources().icons[42].getImage()));
        } else {
            titleBar_openGLButton.setIcon(new ImageIcon(engine.getResources().icons[40].getImage()));
        }
        if (!panels_expanded) {
            titleBar_collapseButton.setIcon(new ImageIcon(engine.getResources().icons[16].getImage()));
        } else {
            titleBar_collapseButton.setIcon(new ImageIcon(engine.getResources().icons[17].getImage()));
        }
    }

    boolean resizing = false;
    static boolean settings_open = false;

    public static boolean isSettings_open() {
        return settings_open;
    }

    public void setSettings_open(boolean settings_open) {
        this.settings_open = settings_open;
    }

    public JPanel getWidget_panel() {
        return widget_panel;
    }

    public void setWidget_panel(JPanel widget_panel) {
        this.widget_panel = widget_panel;
        application_frame.add(widget_panel);
    }

}

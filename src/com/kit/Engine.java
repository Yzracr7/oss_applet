package com.kit;

import com.kit.client.Class122;
import com.kit.gui.gui.Application;
import com.kit.gui.gui.Console;
import com.kit.res.Resources;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Engine {

    Resources resources;
    Application app;
    Console cons;
    Class122 game;

    public Engine() {
        System.out.println("Starting game engine");
        this.cons = new Console();
        this.resources = new Resources();
        main_process_loop();
    }

    public void main_process_loop() {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(10);
        service.scheduleAtFixedRate(() -> {
            total_engine_ticks++;
            // System.out.println("ticking");
        }, 0, 35, TimeUnit.MILLISECONDS);
    }

    public Application getApp() {
        return app;
    }

    public void setApp(Application app) {
        this.app = app;
    }

    public Console getCons() {
        return cons;
    }

    public Resources getResources() {
        return resources;
    }

    boolean has_game_launched;
    int total_engine_ticks = 0;

}

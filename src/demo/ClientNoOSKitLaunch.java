package demo;

import javax.swing.*;

import com.kit.client.Class122;

public class ClientNoOSKitLaunch {

    static Class122 game;
    static JFrame loader_frame;

    public static void launch() {
        loader_frame = new JFrame();
        loader_frame.setVisible(true);
        loader_frame.setBounds(100, 100, 765, 503);
        game = new Class122("1");
        game.setBounds(0, 0, 765, 503);
        game.start();
        game.setLayout(null);

        loader_frame.add(game);
    }
}
